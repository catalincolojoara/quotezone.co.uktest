/*
 * Tooltip plugin
 *
 * This module is a plugin for jquery framework.
 * Example to use:
 * - give to the item on which you want the tooltip's the name 'helpIcon'.
 *   e.g. <img src="test.gif" name="helpIcon" ...
 * - put inside the element an attribute named: 'tooltip' using the following format:
 * tooltip="INFO:cssHeader= INFO:cssContent= INFO:textHeader= INFO:textContent= INFO:fadeInSpeed=0 INFO:fadeOutSpeed=0 INFO:tooltipDelay=0"
 * where:
 * cssHeader  			- class name for header css.
 * cssContent 			- class name for content css.
 * textHeader 			- text to put in the header.
 * textContent			- text to put on content.
 * fadeInSpeed			- speed of fade when show (0 means instant show). You can use slow, def or fast instead of numbers. Default: 'def'
 * fadeOutSpeed			- speed of fade when hide (0 means instant hide). You can use slow, def or fast instead of numbers. Default: 'def'
 * INFO:tooltipDelay	- delay until tooltip is shown (0 means no delay) Default: '1000 ms'
 * If you don't specify some option, default ones are use.
 * full example:
 * <img src="test.gif" name="helpIcon" tooltip="INFO:cssHeader=header INFO:cssContent=content INFO:textHeader=testing INFO:textContent=testing INFO:fadeInSpeed=1000 INFO:fadeOutSpeed=1000 INFO:tooltipDelay=1000">
 * In this plugin, i use an array with help text, so in the textContent we put only array index where we can find the help text.
 * Also, this plugin do not solve the problem with select lists in IE6.
 * This module do not support different header and content text. We made this difference from the help array.
 * This version is a light one, some options are not implemented here.
 *
 * @author Ando Ciupav Cristian, 2009
 * @package jquery.tooltip
 * @version 1.1
 *
 */


jQuery.fn.tooltip = function(information)
{
	var userValues = jQuery.extend(
	{
		cssHeader:	 '',
		cssContent:  '',
		textHeader:  '',
		textContent: 'Error, no tooltip content found!!!',
		text:		 '',
		fadeInType:  'def',
		fadeOutType: 'def',
		delay:       '400',
		show:        0,
		timeoutHandler: null
	}, information);
	return this.each(function()
	{
		if (userValues.show == 0)
		{
			$(this).css('cursor','default');
			clearTimeout(this.timeoutHandler);
			$('#tooltipDiv').fadeOut(userValues.fadeOutType);
		}
		else
		{
			clearTimeout(this.timeoutHandler);
			$('#tooltipDiv').stop(true, true);
			$(this).css('cursor','help');
			var infoArray = readInfoFromAttributeString(userValues.text);
//			getAttribute('cssHeader',infoArray) ? $('#tooltipDiv th').addClass(getAttribute('cssHeader',infoArray)) : setCssHeader();
//			getAttribute('cssContent',infoArray) ? ('#tooltipDiv td').addClass(getAttribute('cssContent',infoArray)) : setCssContent();
			setCssDiv();
//			userValues.textHeader = getAttribute('textHeader',infoArray) ? getAttribute('textHeader',infoArray) : userValues.textHeader;
			userValues.textContent = getAttribute('textContent',infoArray) ? helpText[parseInt(getAttribute('textContent',infoArray))] : userValues.textContent;
			userValues.fadeInType = getAttribute('fadeInSpeed',infoArray) ? getAttribute('fadeInSpeed',infoArray) : userValues.fadeInType;
			userValues.fadeOutType = getAttribute('fadeOutSpeed',infoArray) ? getAttribute('fadeOutSpeed',infoArray) : userValues.fadeOutType;
			userValues.delay = getAttribute('tooltipDelay',infoArray) ? getAttribute('tooltipDelay',infoArray) : userValues.delay;
			$('#tooltipDivContent').html(userValues.textContent);
			readInfoFromAttributeString(userValues.textContent);
			this.timeoutHandler = setTimeout(function()
			{
				$('#tooltipDiv').fadeIn(userValues.fadeInType);
			}, userValues.delay);
		}
		return true;
	}
	);
	function setCssDiv()
	{
		$('#tooltipDiv').css('width','170px');
		$('#tooltipDiv').css('position','absolute');
		$('#tooltipDiv').css('z-index','99');
		$('#tooltipDiv').css('color','#FFFFFF');
		$('#tooltipDiv').css('background','#28568F');
		$('#tooltipDiv').css('display','none');
		$('#tooltipDiv').css('font-size','11px');
		$('#tooltipDiv').css('opacity','0.90');
		$('#tooltipDiv').css('filter',"alpha(opacity=90)");
	}
	function setCssHeader()
	{
		$('#tooltipDiv').css('color','#FFFFFF');
		$('#tooltipDiv').css('background','#28568F');
		$('#tooltipDiv').css('text-weight','bold');
	}
	function setCssContent()
	{
		$('#tooltipDiv').css('color','#FFFFFF');
		$('#tooltipDiv').css('background','#28568F');
	}
	function readInfoFromAttributeString(content)
	{
	   var temp = content;
	   var infoArray = new Array();
	   var counter = 0;
	   var check = true;
	   while(true)
	   {
	      tempUpper = temp.toUpperCase();
	      index = tempUpper.indexOf("INFO:", 1);
	      if (index == -1)
	         break;
	      infoArray[counter++] = temp.substr(5, index-5);
	      temp = temp.substr(index);
	   }
	   infoArray[counter++] = temp.substr(5);
	   return infoArray;
	}
	function getAttribute(atributeName, infoArray)
	{
	   regExpr = new RegExp("/"+atributeName+"/", "i");
	   for(var i=0; i<infoArray.length; i++)
	   {
	      if (infoArray[i].match( atributeName ))
	      {
	         value = infoArray[i].substr(atributeName.length+1);
	         if (value == 'undefined')
	            return null;
	         return value;
	      }
	   }
	   return null;
	}
};

$().ready( function(){
	$('img[name=helpIcon]').mouseover(
		function()
		{
			$(this).tooltip({text: $(this).attr('tooltip'), show: 1});
		}
	);
	$('img[name=helpIcon]').mouseout(function()
		{
			$(this).tooltip({show: 0});
		}
	);
    $().mousemove(
      function(event)
      {
         //if ($('#tooltipDiv').is(':visible'))
         {
            //$('#tooltipIframe').css('left', event.pageX+20);
            //$('#tooltipIframe').css('top', event.pageY-20);
            $('#tooltipDiv').css('left', event.pageX+20);
            $('#tooltipDiv').css('top', event.pageY);
         }
      }
    );

});


jQuery.currentRow = 0;

jQuery.fn.statictip = function(information)
{
	var userValues = jQuery.extend(
	{
		text: '',
		show: 0
	}, information);
   var row = jQuery(this).attr('row');
   return this.each(function()
   {
      if (userValues.show == 1)
      {
      	var pos = jQuery('input[name=mobile_telephone_number]').position();
      	//var pos = jQuery('input[name=mobile_telephone]').position();
      	var top = jQuery(this).position();
         //var width = jQuery(this).width()+10;
         var width = jQuery('input[name=mobile_telephone_number]').width()+10;
         //var width = jQuery('input[name=mobile_telephone]').width()+10;
	      var objName = jQuery(this).attr('name');
         setCssDiv();
         jQuery('#tooltipDivStatic').css('left', pos.left + width);
         jQuery('#tooltipDivStatic').css('top', top.top+5);
         jQuery('td[name=tiptextInsideDiv]').html(userValues.text);
         if (row == jQuery.currentRow)
            jQuery('#tooltipDivStatic').stop();
	      else
	         jQuery('#tooltipDivStatic').stop(true, true);
         jQuery('#tooltipDivStatic').fadeIn('def');
	      jQuery.currentRow = row;
      }
      else
      {
         jQuery('#tooltipDivStatic').fadeOut('def');
      }
   });
   function setCssDiv()
	{
		jQuery('#tooltipDivStatic').css('width','220px');
		jQuery('#tooltipDivStatic').css('text-align','left');
		jQuery('#tooltipDivStatic').css('position','absolute');
		jQuery('#tooltipDivStatic').css('z-index','99');
		jQuery('#tooltipDivStatic').css('color','#FFFFFF');
		jQuery('#tooltipDivStatic').css('font-size','10px');
		jQuery('#tooltipDivStatic').css('opacity','0.85');
		jQuery('#tooltipDivStatic').css('filter',"alpha(opacity=85)");
	}
};


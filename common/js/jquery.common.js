$().ready( function(){
	$('img[name=helpIcon]').mouseover(
		function()
		{
			$(this).tooltip({text: $(this).attr('tooltip'), show: 1});
		}
	);
	$('img[name=helpIcon]').mouseout(function()
		{
			$(this).tooltip({show: 0});
		}
	);
    $().mousemove(
      function(event)
      {
         $('#tooltipDiv').css('left', event.pageX+20);
         $('#tooltipDiv').css('top', event.pageY);
      }
    );
      ////////////////////////////////////////////////////////////////////////////////////////////////
      //   FROM HERE IT'S THE CODE NEEDED FOR TOOLTIP STATIC FROM THE FILE jquery.statictip.js
      jQuery('td[name=contentInformationCell] > input[tooltip=ON]').focus(
            function()
            {
               jQuery(this).statictip({text: staticText[jQuery(this).attr('tiptext')], show: 1});
            }
         );
      jQuery('td[name=contentInformationCell] > input[tooltip=ON]').blur(
         function()
         {
            jQuery(this).statictip({show: 0});
         }
      );
      jQuery('td[name=contentInformationCell] > select[tooltip=ON]').focus(
            function()
            {
               jQuery(this).statictip({text: staticText[jQuery(this).attr('tiptext')], show: 1});
            }
         );
      jQuery('td[name=contentInformationCell] > select[tooltip=ON]').blur(
         function()
         {
            jQuery(this).statictip({show: 0});
         }
      );

      jQuery('td[name=contentInformationCell] > select[tooltip=ON]').focus(
            function()
            {
               jQuery(this).statictip({text: staticText[jQuery(this).attr('tiptext')], show: 1});
            }
         );
      jQuery('td[name=contentInformationCell] > select[tooltip=ON]').blur(
         function()
         {
            jQuery(this).statictip({show: 0});
         }
      );

      jQuery('td[name=tooltipStaticCorner]').css('background-image','url(images/static-tooltip2.gif)');
      jQuery('td[name=tooltipStaticCorner]').css('background-repeat','no-repeat');
      ////////////////////////////////////////////////////////////////////////////////////////////////

});

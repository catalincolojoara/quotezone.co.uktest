<?php
/*******************************************************************************************************************************/
/*                                                                                                                             */
/*  Insurers rotation script                                                                                                   */ 
/*  -> this script is used to rotate the insurers from a specific panel, so at some point only a few insurers will get a lead. */
/*  -> the insurers max limit is sent as parameter.                                                                            */
/*                                                                                                                             */
/*  (C) 2010 Sturza Ciprian (cipi@acrux.biz)                                                                                   */
/*                                                                                                                             */
/*******************************************************************************************************************************/

include_once "../modules/globals.inc";
include_once "QuoteUser.php";
include_once "Log.php";
include_once "QuoteStatus.php";

$objQuoteStatus = new CQuoteStatus($dbHandle);
$objLog         = new CLog($dbHandle);
$objQuoteUser   = new CQuoteUser($dbHandle);


function RotateInsurers($quoteTypeID='', $getQuote=array(), $requestSites=array(), $insurersLimit=5, $customerFirstName='', $customerSurname='', $customerDateOfBirth='')
{
   global $objQuoteStatus;
   global $objQuoteUser;
   global $objLog;

   if(empty($requestSites))
      return false;

   if(empty($getQuote))
      return false;

   if(! preg_match("/^\d+$/", $quoteTypeID))
      return false;

   if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$customerFirstName))
      return false;

   if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$customerSurname))
      return false;

   if(! preg_match("/\d{2}\/\d{2}\/\d{4}/i",$customerDateOfBirth))
      return false;


   $insurersList = array();
   foreach($requestSites as $siteID => $included)
      array_push($insurersList, $siteID);

   $requestSites = array();
   $skippedSites = array();

   // rebuild request_sites from database with the companies that received the lead if the user had a quote before (we take the last quote)
   if( $userDetails = $objQuoteUser->GetUserByFNameSNameDateOfBirth($customerFirstName,$customerSurname,$customerDateOfBirth))
   {
      $quoteUserID = $userDetails['id'];

      $startDate = "01/".date("m/Y");
      $endDate   = "31/".date("m/Y");

      $userQuotes = array();
      if($userQuotes = $objLog->GetLogsByQuoteType($quoteUserID,'',$startDate,$endDate,'','',$quoteTypeID))
      {
         $maxLogID = 0;
         foreach($userQuotes as $index => $logDetails)
            if($logDetails['id'] > $maxLogID)
               $maxLogID = $logDetails['id'];

         $lastCompaniesSent = array();
         if($lastCompaniesSent = $objQuoteStatus->GetAllQuoteStatusByLogID($maxLogID))
         {
            foreach($lastCompaniesSent as $index => $quoteStatusDetails)
            {
               $siteID = $quoteStatusDetails['site_id'];

               // we take the skipped companies only if those still filter
               if($quoteStatusDetails['status'] == "SKIPPED" && !$getQuote[$siteID])
                  array_push($skippedSites, $siteID);

               // we take the success companies only if those still get the quote
               if($quoteStatusDetails['status'] == "SUCCESS" && $getQuote[$siteID])
                  $requestSites[$siteID] = 1;
            }
         }
      }
   } 

   // get the max site ID that received a lead last quote
   if($lastQuoteDetails = $objLog->GetLastLog($quoteTypeID))
   {
      $lastLogID = $lastQuoteDetails['id'];
      $lastCompaniesSent = array();
      $maxSiteID = 0;
      if($lastCompaniesSent = $objQuoteStatus->GetAllQuoteStatusByLogID($lastLogID))
      {
         foreach($lastCompaniesSent as $index => $quoteStatusDetails)
         {
            $siteID = $quoteStatusDetails['site_id'];

            if($quoteStatusDetails['status'] == "SUCCESS")
               if($siteID > $maxSiteID)
                  $maxSiteID = $siteID;
         }
      }

   }

   sort($insurersList);

   $insurersPriorityList = array();
   $insurersPriorityList = $insurersList;

   // recreating the insurers list starting with the first siteID after the last one that received a lead last time (max site ID)
   foreach($insurersList as $index => $siteID)
   {
      array_shift($insurersPriorityList);
      array_push($insurersPriorityList, $siteID);

      if($siteID == $maxSiteID)
         break;
   }

   // rotation algorithm
   $insurersCount = count($insurersPriorityList); 

   for($i=0; $i<$insurersCount; $i++)
   {
      if(count($requestSites) >= $insurersLimit)
         break;

      // we need to keep the skipped sites too, but no need to count them
      if($getQuote[$insurersPriorityList[$i]])
         $requestSites[$insurersPriorityList[$i]] = 1;
      else
         array_push($skippedSites, $insurersPriorityList[$i]);

   }

   // here we add the skipped sites too
   foreach($skippedSites as $index => $siteID)
      $requestSites[$siteID] = 1;

   return $requestSites;
}

?>

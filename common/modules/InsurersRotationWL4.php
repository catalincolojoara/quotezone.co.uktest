<?php
/*******************************************************************************************************************************/
/*                                                                                                                             */
/*  Insurers rotation script                                                                                                   */ 
/*  -> this script is used to rotate the insurers from a specific panel, so at some point only a few insurers will get a lead. */
/*  -> the insurers max limit is sent as parameter.                                                                            */
/*                                                                                                                             */
/*  (C) 2010 Sturza Ciprian (cipi@acrux.biz)                                                                                   */
/*                                                                                                                             */
/*******************************************************************************************************************************/

include_once "modules/globals.inc";
include_once "QuoteUser.php";
include_once "Log.php";
include_once "QuoteStatus.php";

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: RotateInsurers
//
// [DESCRIPTION]:   Rotates the insurers who gets the lead
//                  The function checks the last quote made on that lead system and decides
//                  which company received the last lead or it was in the position to receive it,
//                  Then it will re-build the request site list to include the sites that succeed 
//                  that last company. 
//                  The number of companies who should receive the lead is sent as param.
//
// [CREATED BY]:    Ciprian STURZA (cipi@acrux.biz) 2010-07-07
//
//////////////////////////////////////////////////////////////////////////////FE

function RotateInsurers($quoteTypeID='', $getQuote=array(), $requestSites=array(), $insurersLimit=5, $customerFirstName='', $customerSurname='', $customerDateOfBirth='')
{
   global $objWs;

   $dbh            = $objWs->GetDbHandle();
   $objQuoteStatus = new CQuoteStatus($dbh);
   $objLog         = new CLog($dbh);
   $objQuoteUser   = new CQuoteUser($dbh);

   $startTime = date('g:i:s');
   
   if(empty($requestSites))
      return false;

   if(empty($getQuote))
      return false;

   if(! preg_match("/^\d+$/", $quoteTypeID))
      return false;

   if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$customerFirstName))
      return false;

   if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$customerSurname))
      return false;

   if(! preg_match("/\d{2}\/\d{2}\/\d{4}/i",$customerDateOfBirth))
      return false;

   $insurersList = array();
   foreach($requestSites as $siteID => $included)
      array_push($insurersList, $siteID);

   $requestSites = array();
   $skippedSites = array();

   $_SESSION["_DUPLICATE_LEAD_"] = 0;
   /*
   // rebuild request_sites from database with the companies that received the lead if the user had a quote before (we take the last quote)
   if( $userDetails = $objQuoteUser->GetUserByFNameSNameDateOfBirth($customerFirstName,$customerSurname,$customerDateOfBirth))
   {
      $quoteUserID = $userDetails['id'];

      //check if the user made a quote today and don`t send details anymore

      //we need to check if the details are created in the last 48 H and if so we need to remove those companies from the details (no data will be sent to this companies, they will be acced to filter)
      //$towDaysInPast = mktime("0","0","0",date("m"),date("d"),date("Y")) - (86400 * 2);
      //updated in the ticket to be just the quote date
      $startDateToday = date("d/m/Y");
      $endDateToday   = date("d/m/Y");

      $userQuotes             = array();
      $companiesToRemoveArray = array();
      if($userQuotes = $objLog->GetLogsByQuoteType($quoteUserID,'',$startDateToday,$endDateToday,'','',$quoteTypeID))
      {
         $_SESSION["_DUPLICATE_LEAD_"] = 1;
         $_SESSION['REAL_USER_IP']     = $_SESSION['USER_IP'];
         $_SESSION['USER_IP']          = "86.125.114.56";
      }

      //check if the user made a quote today and don`t send details anymore
      $startDate = "01/".date("m/Y");
      $endDate   = "31/".date("m/Y");

      $userQuotes = array();
      if($userQuotes = $objLog->GetLogsByQuoteType($quoteUserID,'',$startDate,$endDate,'','',$quoteTypeID))
      {
         $maxLogID = 0;
         foreach($userQuotes as $index => $logDetails)
            if($logDetails['id'] > $maxLogID)
               $maxLogID = $logDetails['id'];

         $lastCompaniesSent = array();
         if($lastCompaniesSent = $objQuoteStatus->GetAllQuoteStatusByLogID($maxLogID))
         {
            foreach($lastCompaniesSent as $index => $quoteStatusDetails)
            {
               $siteID = $quoteStatusDetails['site_id'];

               // we take the skipped companies only if those still filter
               if($quoteStatusDetails['status'] == "SKIPPED" && !$getQuote[$siteID])
                  array_push($skippedSites, $siteID);

               // we take the success companies only if those still get the quote
               if($quoteStatusDetails['status'] == "SUCCESS" && $getQuote[$siteID])
               {
                  $requestSites[$siteID] = 1;                  
               }
            }
         }
      }
   }
   */

   // get the max site ID that received a lead last quote(at least a success one)
   $sqlCMD = "SELECT MAX(lsq.log_id) AS maxLogID FROM (select s.* from sites s where s.quote_type_id = $quoteTypeID) cmdSites LEFT JOIN lead_success_quotes lsq ON cmdSites.id=lsq.site_id";
   
   if($dbh->Exec($sqlCMD))
   {
      if($dbh->FetchRows())
      {
         $lastLogID = $dbh->GetFieldValue("maxLogID");

         $lastQuotesSites = array();
         $lastCompaniesSent = array();
         $maxSiteID = 0;
         if($lastCompaniesSent = $objQuoteStatus->GetAllQuoteStatusByLogID($lastLogID))
         {
            foreach($lastCompaniesSent as $index => $quoteStatusDetails)
            {
               $siteID = $quoteStatusDetails['site_id'];

               $lastQuotesSites[$siteID] = $quoteStatusDetails['status'];

               if($siteID > $maxSiteID && in_array($siteID, $insurersList))
                  $maxSiteID = $siteID;
            }
         }
      }
   }

   sort($insurersList);

   if($maxSiteID == 0)
      $maxSiteID = $insurersList[0];

   $insurersPriorityList = array();
   $insurersPriorityList = $insurersList;

   // recreating the insurers list starting with the first siteID after the last one that received a lead last time (max site ID)
   $found     = true;
   $endOfList = 0;
   while(1)
   {
      if(current($insurersList) < $maxSiteID && $found)
      {
         if(! next($insurersList))
         {
            reset($insurersList);
            $nextSite = current($insurersList);
            break;
         }

         continue;
      }

      $found = false;

      $siteID = current($insurersList);

      if($siteID == $maxSiteID)
         $endOfList ++;

      // if we get a second time the end of the list, that means there is no siteID not marked on the last quote, so we start from the first siteID
      if($endOfList == 2)
      {
         $nextSite = $insurersList[0];
         break;
      }

      if(array_key_exists($siteID, $lastQuotesSites))
      {
         if(! next($insurersList))
            reset($insurersList);

         continue;
      }

      $nextSite = $siteID;
      break;
   }

   reset($insurersList);
   $insurersPriorityList = array();

   foreach($insurersList as $siteID)
   {
      if($siteID < $nextSite)
         continue;

      array_push($insurersPriorityList, $siteID);
   }

   foreach($insurersList as $siteID)
   {
      if(in_array($siteID, $insurersPriorityList))
         continue;

      array_push($insurersPriorityList, $siteID);
   }

   // rotation algorithm
   $insurersCount = count($insurersPriorityList); 

   for($i=0; $i<$insurersCount; $i++)
   {
      if(count($requestSites) >= $insurersLimit)
         break;

      // we need to keep the skipped sites too, but no need to count them
      if($getQuote[$insurersPriorityList[$i]])
         $requestSites[$insurersPriorityList[$i]] = 1;
      else
         array_push($skippedSites, $insurersPriorityList[$i]);
   }

   // here we add the skipped sites too
   foreach($skippedSites as $index => $siteID)
      $requestSites[$siteID] = 1;

   $endTime = date('g:i:s');

   return $requestSites;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SkipInsurers
//
// [DESCRIPTION]:   This function is used to skip some insurers.
//                  There is a mai insurer who has the first priority to receive
//                  the lead, but if this skips, the second prio site will get it. 
//
// [CREATED BY]:    George VULCAN (george@acrux.biz) 2011-09-15
//
//////////////////////////////////////////////////////////////////////////////FE

// This function is used to skip some insurers 
function SkipInsurers($requestSites=array(),$getQuote=array(),$insurersArray=array())
{
   if(empty($requestSites))
      return false;

   if(empty($getQuote))
      return false;

   if(empty($insurersArray))
      return false;
    
   $insurerToQuote = 0;
   foreach($insurersArray as $index=>$siteID)
   {
      if($getQuote[$siteID])
      {
         $insurerPosition = $index;
         $insurerToQuote  = $siteID;
         
         break; 
      }   
   }
   
   $skipInsurerList = array();
   $counter         = 0;   
   
   if(! $insurerToQuote)
      return $skipInsurerList;
            
   foreach($insurersArray as $index=>$siteID)
   {
      if($siteID != $insurerToQuote && $getQuote[$siteID])
      {
         $skipInsurerList[$counter]['site_id'] = $siteID;
         $skipInsurerList[$counter]['error']   = "";
         $counter++;
      }
   }
   
   return $skipInsurerList;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: RotateInsurersAndGroups
//
// [DESCRIPTION]:   Rotates the insurers groups who gets the lead
//                  The function checks the last quote made on that lead system and decides
//                  which company group received the last lead or it was in the position to receive it.
//                  Then it will re-build the request site list to include the sites from the
//                  group that succeed that last company group.
//                  The number of groups/companies who should receive the lead is sent as param.
//
// [CREATED BY]:    Ciprian STURZA (cipi@acrux.biz) 2011-11-10
//
//////////////////////////////////////////////////////////////////////////////FE
function RotateInsurersAndGroups($quoteTypeID='', $getQuote=array(), $requestSites=array(), $insurersLimit=5, $customerFirstName='', $customerSurname='', $customerDateOfBirth='')
{
   global $objQuoteStatus;
   global $objQuoteUser;
   global $objLog;
   global $dbh;

   if(empty($requestSites))
      return false;

   if(empty($getQuote))
      return false;

   if(! preg_match("/^\d+$/", $quoteTypeID))
      return false;

   if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$customerFirstName))
      return false;

   if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$customerSurname))
      return false;

   if(! preg_match("/\d{2}\/\d{2}\/\d{4}/i",$customerDateOfBirth))
      return false;

   $insurersList = array();
   foreach($requestSites as $siteID => $included)
      array_push($insurersList, $siteID);

   $requestSites = array();
   $skippedSites = array();

   // rebuild request_sites from database with the companies that received the lead if the user had a quote before (we take the last quote)
//   if( $userDetails = $objQuoteUser->GetUserByFNameSNameDateOfBirth($customerFirstName,$customerSurname,$customerDateOfBirth))
//   {
//      $quoteUserID = $userDetails['id'];
//
//      $startDate = "01/".date("m/Y");
//      $endDate   = "31/".date("m/Y");
//
//      $userQuotes = array();
//      if($userQuotes = $objLog->GetLogsByQuoteType($quoteUserID,'',$startDate,$endDate,'','',$quoteTypeID))
//      {
//         $maxLogID = 0;
//         foreach($userQuotes as $index => $logDetails)
//            if($logDetails['id'] > $maxLogID)
//               $maxLogID = $logDetails['id'];
//
//         $lastCompaniesSent = array();
//         if($lastCompaniesSent = $objQuoteStatus->GetAllQuoteStatusByLogID($maxLogID))
//         {
//            foreach($lastCompaniesSent as $index => $quoteStatusDetails)
//            {
//               $siteID = $quoteStatusDetails['site_id'];
//
//               // we take the skipped companies only if those still filter
//               if($quoteStatusDetails['status'] == "SKIPPED" && !$getQuote[$siteID])
//                  array_push($skippedSites, $siteID);
//
//               // we take the success companies only if those still get the quote
//               if($quoteStatusDetails['status'] == "SUCCESS" && $getQuote[$siteID])
//                  $requestSites[$siteID] = 1;
//            }
//         }
//      }
//   }

   $sqlCMD = "SELECT id, broker_id FROM ".SQL_SITES." WHERE quote_type_id='$quoteTypeID' ORDER BY id";

   if(! $dbh->Exec($sqlCMD))
   {
     print $dbh->GetError();
     return false;
   }

   if(! $dbh->GetRows())
   {
      print GetErrorString("SITES_NOT_FOUND");
      return false;
   }

   while($dbh->MoveNext())
   {
      $siteID   = $dbh->GetFieldValue("id");
      $brokerID = $dbh->GetFieldValue("broker_id");

      if($brokerID == 0)
         $brokerID = $siteID;

      if(in_array( $siteID,$insurersList)) 
         $groupsSites[$brokerID][$siteID] = 1;

      $siteDetails[$siteID]['broker_id'] = $brokerID;
   } 


   // get the max group ID that received a lead last quote
   $lastQuotesSites = array();

   $maxBrokerID = 0;

   if($lastQuoteDetails = $objLog->GetLastLog($quoteTypeID))
   {
      $lastLogID = $lastQuoteDetails['id'];
      $lastCompaniesSent = array();

      if($lastCompaniesSent = $objQuoteStatus->GetAllQuoteStatusByLogID($lastLogID))
      {
         foreach($lastCompaniesSent as $index => $quoteStatusDetails)
         {
            $siteID = $quoteStatusDetails['site_id'];

            $lastQuotesSites[$siteID] = $quoteStatusDetails['status'];

            
            if($siteDetails[$siteID]['broker_id'] > $maxBrokerID)
               $maxBrokerID = $siteDetails[$siteID]['broker_id'];
         }
      }
   }

   sort($insurersList);
   ksort($groupsSites);

   // if no quote was found or for the last quote wasn't found any success quote, 
   // we set the max group to be the last group from the ordered list of groups
   if($maxBrokerID == 0)
   {
      end($groupsSites);
      $maxBrokerID = key($groupsSites);
      reset($groupsSites);
   } 

   // recreating the insurers list starting with the first siteID after the last one that received a lead last time (max site ID)
   $found = false;
   foreach($groupsSites as $groupID => $sites)
   {
      // init next group as first one from the list
      if($nextGroup == "")
         $nextGroup = $groupID;

      if($found)
      {
         $nextGroup = $groupID;
         break;
      }
  
      if($groupID == $maxBrokerID)
         $found = true;
   }

   reset($insurersList);
   $groupsPriorityList = array();

   foreach($groupsSites as $groupID => $sites)
   {
      if($groupID < $nextGroup)
         continue;

      array_push($groupsPriorityList, $groupID);
   }

   foreach($groupsSites as $groupID => $sites)
   {
      if(in_array($groupID, $groupsPriorityList))
         continue;

      array_push($groupsPriorityList, $groupID);
   }

   // rotation algorithm
   $groupsCount = count($groupsPriorityList); 

   for($i=0; $i<$groupsCount; $i++)
   {
      if(count($requestSites) >= $insurersLimit)
         break;

      foreach($groupsSites[$groupsPriorityList[$i]] as $siteID => $included)
      {
         if($getQuote[$siteID])
            $requestSites[$siteID] = 1;
         else
            array_push($skippedSites, $siteID);   
      }
   }

   // here we add the skipped sites too
   foreach($skippedSites as $index => $siteID)
      $requestSites[$siteID] = 1;

   return $requestSites;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: RotateGroups
//
// [DESCRIPTION]:   Rotates the 2 groups; same algorithm as for insurers rotation. 
//
// [CREATED BY]:    Ciprian STURZA (cipi@acrux.biz) 2011-09-30
//
//////////////////////////////////////////////////////////////////////////////FE
function RotateGroups($broker1Array=array(), $broker2Array=array(), $quoteTypeID='', $getQuote=array(), $requestSites=array(), $customerFirstName='', $customerSurname='', $customerDateOfBirth='')
{
   global $objQuoteStatus;
   global $objQuoteUser;
   global $objLog;

   if(empty($requestSites))
      return false;

   if(empty($getQuote))
      return false;

   if(empty($broker1Array))
      return false;

   if(empty($broker2Array))
      return false;

   if(! preg_match("/^\d+$/", $quoteTypeID))
      return false;

   if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$customerFirstName))
      return false;

   if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$customerSurname))
      return false;

   if(! preg_match("/\d{2}\/\d{2}\/\d{4}/i",$customerDateOfBirth))
      return false;

   // this is the next group/broker who should receive the lead
   $brokerFound = "";

   // rebuild request_sites from database with the companies that received the lead if the user had a quote before (we take the last quote)
//   if( $userDetails = $objQuoteUser->GetUserByFNameSNameDateOfBirth($customerFirstName,$customerSurname,$customerDateOfBirth))
//   {
//      $quoteUserID = $userDetails['id'];
//
//      $startDate = "01/".date("m/Y");
//      $endDate   = "31/".date("m/Y");
//
//      $userQuotes = array();
//
//      // checking for other quotes made by the same user in the current month
//      if($userQuotes = $objLog->GetLogsByQuoteType($quoteUserID,'',$startDate,$endDate,'','',$quoteTypeID))
//      {
//         $maxLogID = 0;
//         foreach($userQuotes as $index => $logDetails)
//            if($logDetails['id'] > $maxLogID)
//               $maxLogID = $logDetails['id'];
//
//         $lastCompaniesSent = array();
//
//         // if this user already has a lead, we should send the lead to the same group/broker
//         if($lastCompaniesSent = $objQuoteStatus->GetAllQuoteStatusByLogID($maxLogID))
//         {
//            foreach($lastCompaniesSent as $index => $quoteStatusDetails)
//            {
//               $siteID = $quoteStatusDetails['site_id'];
//
//               if($quoteStatusDetails['status'] == "SUCCESS")
//               {
//                  if(array_key_exists($siteID, $broker1Array))
//                     $brokerFound = "1";
//
//                  if(array_key_exists($siteID, $broker2Array))
//                     $brokerFound = "2";
//               }
//
//               if($brokerFound != "")
//                  break;
//
//            }
//         }
//      }
//   }

   if($brokerFound == "")
   {
      // get the last group/broker that received a lead last quote
      if($lastQuoteDetails = $objLog->GetLastLog($quoteTypeID))
      {
         $lastLogID = $lastQuoteDetails['id'];
         $lastCompaniesSent = array();

         if($lastCompaniesSent = $objQuoteStatus->GetAllQuoteStatusByLogID($lastLogID))
         {
            foreach($lastCompaniesSent as $index => $quoteStatusDetails)
            {
               $siteID = $quoteStatusDetails['site_id'];

               if($quoteStatusDetails['status'] == "SUCCESS")
               {
                  // if the last lead was sent to broker1, we will send the next lead to broker2
                  if(array_key_exists($siteID, $broker1Array))
                     $brokerFound = "2";

                  // if the last lead was sent to broker2, we will send the next lead to broker1
                  if(array_key_exists($siteID, $broker2Array))
                     $brokerFound = "1";
               }
            
               if($brokerFound != "")
                  break;
            }
         }
      }
   }
 
   if($brokerFound == "2")
   {
      $mainBrokerArray   = $broker2Array;
      $secondBrokerArray = $broker1Array;
   }
   else
   {
      $mainBrokerArray   = $broker1Array;
      $secondBrokerArray = $broker2Array;
   }

   $companyFound = "";

   foreach($mainBrokerArray as $siteID => $name)
   {
      if($getQuote[$siteID])
      {
         $companyFound = $siteID;
         break; 
      }
   }

   if($companyFound != "")
   {
      foreach($secondBrokerArray as $siteID => $name)
         unset($requestSites[$siteID]);
   }
  

   return $requestSites;
}

?>

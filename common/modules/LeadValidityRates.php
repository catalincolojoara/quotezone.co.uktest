<?php

/*****************************************************************************/
/*                                                                           */
/*  CUser class interface                                                    */
/*                                                                           */
/*  (C) 2004 Eugen Savin (seugen@abc.ro)                                     */
/*                                                                           */
/*****************************************************************************/

define("USER_INCLUDED", "1");
define("SQL_LEAD_VALIDITY_RATE", "lead_validity_rates");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CUser
//
// [DESCRIPTION]:  CUser class interface
//
// [FUNCTIONS]:    int        AddValidityRate($quoteTypeID='', $validityRate='')
//                 bool       UpdateValidityRate($id="", $quoteTypeID='', $validityRate='')
//                 array      GetAllValidityRates(&$arrayResult);
//                 array      GetAllValidityRatesByDate($date)
//                 bool       GetUnicity($userId,$days)
//                 Close();
//                 GetError();
//                 float      make_seed();
//
// [CREATED BY]:   Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:     - Ciprian Sturza (cipi@acrux.biz) 2007-01-31
//                   New functions was added
//
//////////////////////////////////////////////////////////////////////////////PE

class CLeadValidityRate
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CValidityRate
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    IN: $dbh=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CLeadValidityRate($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertValidityRate
//
// [DESCRIPTION]:   Check if all fields are OK and set the error string if needed
//
// [PARAMETERS]     IN: $quoteUserID, $validityRate
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel (cipi@acrux.biz) 2010-11-05
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertValidityRate($quoteTypeID, $validityRate)
{
   $this->strERR = "";

   if(! preg_match("/^[0-9]+$/", $quoteTypeID))
   {
      $this->strERR .= GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
   }

   if(! preg_match("/^[0-9\.]+$/", $validityRate))
   {
      $this->strERR .= GetErrorString("INVALID_VALIDITY_RATE_FIELD");
   }

   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddValidityRate
//
// [DESCRIPTION]:   Add new entry to the lead_validity_rates table
//
// [PARAMETERS]:    IN : $quoteTypeID='', $validityRate=''
//
// [RETURN VALUE]:  userID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - Ciprian Sturza (cipi@acrux.biz) 2007-01-31
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddValidityRate($quoteTypeID='', $validityRate='')
{
   if(! $this->AssertValidityRate($quoteTypeID, $validityRate) )
      return 0;

   $sqlCmd= "INSERT INTO ".SQL_LEAD_VALIDITY_RATE." (quote_type_id,validity_rate,cr_date) VALUES ('$quoteTypeID','$validityRate',now())";

   if(! $this->dbh->Exec($sqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $sqlCmd= "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $validityRateID = $this->dbh->GetFieldValue("id");

   return $validityRateID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateValidityRate
//
// [DESCRIPTION]:   Update lead_validity_rates table
//
// [PARAMETERS]:    IN: $id=0, $quoteTypeID='', validityRate=''
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2010-11-05
//
// [MODIFIED]:      [short description]
//
//////////////////////////////////////////////////////////////////////////////FE
function UpdateValidityRate($id=0, $quoteTypeID='', $validityRate='')
{
   $this->strERR = "";

   if(! preg_match("/^\d+$/", $id))
      $this->strERR .= GetErrorString("INVALID_VALIDITY_RATE_ID_FIELD")."\n";

   if(! empty($this->strERR))
      return false;
  
   if(! $this->AssertValidityRate($quoteTypeID, $validityRate) )
      return 0;

   $sqlCmd= "UPDATE ".SQL_LEAD_VALIDITY_RATE." SET quote_type_id='$quoteTypeID',validity_rate='$validityRate' WHERE id='$id'";

   if(! $this->dbh->Exec($sqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllValidityRates
//
// [DESCRIPTION]:   Read data from lead_validity_rates table and put it into an array variable
//
// [PARAMETERS]:    -
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllValidityRates()
{
   $sqlCmd= "SELECT id,quote_type_id, validity_rate, cr_date FROM ".SQL_LEAD_VALIDITY_RATE."";

   if(! $this->dbh->Exec($sqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERS_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("id")]["quote_type_id"]  = $this->dbh->GetFieldValue("quote_type_id");
      $arrayResult[$this->dbh->GetFieldValue("id")]["validity_rate"]  = $this->dbh->GetFieldValue("validity_rate");
      $arrayResult[$this->dbh->GetFieldValue("id")]["cr_date"]        = $this->dbh->GetFieldValue("cr_date");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllValidityRates
//
// [DESCRIPTION]:   Read data from lead_validity_rates table and put it into an array variable
//
// [PARAMETERS]:    -
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      Ciprian Sturza (cipi@acrux.biz) 2011-08-15
//                  - quoteTypeID added in order to return the rate only for a specified system
//
//                  - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetValidityRatesByDate($date="",$quoteTypeID="")
{
   if(! preg_match("/^\d{4,4}\-\d{2,2}\-\d{2,2}$/", $date))
   {
      $this->strERR = GetErrorString("INVALID_VALIDITY_RATE_DATE");
      return false;
   }

   if($quoteTypeID != "")
   {
      if(! preg_match("/^\d+$/", $quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID");
         return false;
      }
   }

   $sqlCmd = "SELECT id, quote_type_id, validity_rate, cr_date FROM (SELECT * from ".SQL_LEAD_VALIDITY_RATE." WHERE cr_date <='$date' ORDER BY cr_date DESC) as tbl";

   if($quoteTypeID != "")
      $sqlCmd .=" WHERE quote_type_id='$quoteTypeID'";

   $sqlCmd .=" group by quote_type_id";


   if(! $this->dbh->Exec($sqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     print $this->strERR;
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERS_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("quote_type_id")]['id']            = $this->dbh->GetFieldValue("id");
      $arrayResult[$this->dbh->GetFieldValue("quote_type_id")]['cr_date']       = $this->dbh->GetFieldValue("cr_date");
      $arrayResult[$this->dbh->GetFieldValue("quote_type_id")]['validity_rate'] = $this->dbh->GetFieldValue("validity_rate");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllValidityRatesByDate
//
// [DESCRIPTION]:   Read data from lead_validity_rates table and put it into an array variable
//
// [PARAMETERS]:    date
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Ciprian Sturza (cipi@acrux.biz) 2011-08-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllValidityRatesByDate($date)
{
   if(! preg_match("/^\d{4,4}\-\d{2,2}\-\d{2,2}$/", $date))
   {
      $this->strERR = GetErrorString("INVALID_VALIDITY_RATE_DATE");
      return false;
   }

   $sqlCmd= "SELECT * FROM ".SQL_LEAD_VALIDITY_RATE." WHERE cr_date ='$date'";

   if(! $this->dbh->Exec($sqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERS_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("quote_type_id")]['id']            = $this->dbh->GetFieldValue("id");
      $arrayResult[$this->dbh->GetFieldValue("quote_type_id")]['cr_date']       = $this->dbh->GetFieldValue("cr_date");
      $arrayResult[$this->dbh->GetFieldValue("quote_type_id")]['validity_rate'] = $this->dbh->GetFieldValue("validity_rate");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllValidityRates
//
// [DESCRIPTION]:   Read data from lead_validity_rates table and put it into an array variable
//
// [PARAMETERS]:    $quoteTypeID, $date
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAcceptanceRateByDate($quoteTypeID, $date)
{
   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID");
      return false;
   }

   if(! preg_match("/^\d{4,4}\-\d{2,2}\-\d{2,2}$/", $date))
   {
      $this->strERR = GetErrorString("INVALID_VALIDITY_RATE_DATE");
      return false;
   }

   $sqlCmd = "SELECT qs.site_id, (count(distinct if(status='SUCCESS',quote_user_id,NULL)) / count(distinct quote_user_id)*100)  as acceptance_rate from logs l, quote_status qs, qzquotes qzq  where l.id=qs.log_id and l.id=qzq.log_id and qzq.quote_type_id=$quoteTypeID and l.date between  DATE_SUB('$date', INTERVAL 30 DAY) and '$date' group by qs.site_id ";

   if(! $this->dbh->Exec($sqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERS_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("site_id")]            = $this->dbh->GetFieldValue("acceptance_rate");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUnicity()
//
// [DESCRIPTION]:   Find if the user id is unique for the specific period
//
// [PARAMETERS]:    -
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Adrian Axente (adi@acrux.biz) 2011-12-08
//////////////////////////////////////////////////////////////////////////////FE
function GetUnicity($userId,$quoteTypeId,$days)
{ 
   if(! empty($days))
   {
      if(! preg_match("/^\d+$/", $days))
      {
         $this->strERR = GetErrorString("INVALID_DAYS");
         return false;
      }
   }
   else
      $days = 30;

   if(! preg_match("/^\d+$/", $quoteTypeId))
   {
       $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID");
       return false;
   }

   if(! preg_match("/^\d+$/", $userId))
   {
       $this->strERR = GetErrorString("INVALID_USER_ID");
       return false;
   }

   $sql = "SELECT count(*) AS number from logs l, qzquotes qzq WHERE l.id=qzq.log_id AND qzq.quote_type_id='$quoteTypeId' AND l.quote_user_id='$userId' and l.date>=date(now()-interval $days day);";

   $unique = false;
   if($this->dbh->Exec($sql,true))
       $this->dbh->FetchRows();
         if($this->dbh->GetFieldValue("number") == 1) //at this point he has a quote !!!!! THE ONE RUNNING NOW
            $unique = true;

   return $unique;
}

function GetUnicityAllTime($userId,$quoteTypeId)
{
   if(! preg_match("/^\d+$/", $quoteTypeId))
   {
       $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID");
       return false;
   }

   if(! preg_match("/^\d+$/", $userId))
   {
       $this->strERR = GetErrorString("INVALID_USER_ID");
       return false;
   }

   $sql = "SELECT count(*) AS number from logs l, qzquotes qzq WHERE l.id=qzq.log_id AND qzq.quote_type_id='$quoteTypeId' AND l.quote_user_id='$userId';";

   $unique = false;
   if($this->dbh->Exec($sql,true))
       $this->dbh->FetchRows();
         if($this->dbh->GetFieldValue("number") == 1) //at this point he has a quote !!!!! THE ONE RUNNING NOW
            $unique = true;

   return $unique;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

// Make random number
function make_seed()
{
   list($usec, $sec) = explode(' ', microtime());
   return (float) $sec + ((float) $usec * 100000000);
}

} // end of CUser class
?>

<?php
/*******************************************************************************************************************************/
/*                                                                                                                             */
/*  Site succes leads limit script                                                                                             */
/*  -> this script is used to calculate the number of success leads per company per specified period.                          */
/*  -> We take only unique users                                                                                               */
/*                                                                                                                             */
/*  (C) 2011 Sturza Ciprian (cipi@acrux.biz)                                                                                   */
/*                                                                                                                             */
/*******************************************************************************************************************************/

//include_once "../modules/globals.inc";
//include_once "MySQL.php";

function GetSiteLeadsPerPeriod($siteID='',$period='')
{
   global $dbHandle;

   if(! empty($siteID))
   {
      if(! preg_match("/^\d+$/", $siteID))
      {
         print GetErrorString("INVALID_SITE_ID_FIELD");
         return false;
      }
   }

   if(empty($period))
   {
      print GetErrorString("INVALID_PERIOD_FIELD");
      return false;
   }

   $sqlCMD = "SELECT COUNT(DISTINCT quote_user_id) AS leads FROM lead_success_quotes WHERE site_id='$siteID'";


   switch($period)
   {
      case '30min':
                  $sqlCMD .= " AND concat(date,' ',time) >= DATE_SUB(now(),INTERVAL 30 MINUTE)";
                  break;

      case 'hour':
                  $currentDayDate = date("Y-m-d");
                  $currentDayHour = date("H");
                  $sqlCMD .= " AND date = '$currentDayDate' AND time like '$currentDayHour%'";
                  break;

      case '2hours':
                  $sqlCMD .= " AND concat(date,' ',time) >= DATE_SUB(now(),INTERVAL 120 MINUTE)";
                  break;

      case '3hours':
                  $sqlCMD .= " AND concat(date,' ',time) >= DATE_SUB(now(),INTERVAL 180 MINUTE)";
                  break;

      case 'day':
                  $currentDayDate = date("Y-m-d");
                  $sqlCMD .= " AND date = '$currentDayDate'";
                  break;

      case 'week':
                  $currentDate = date("Y-m-d");
                  $currentYear = date("Y");
                  $currentWeek = date("W");
                  $sqlCMD .= " AND YEAR(date) = '$currentYear' AND WEEK(date,3) = '$currentWeek'";
                  break;

      case 'month':
                  $currentMonthDate = date("Y-m")."-01";
                  $sqlCMD .= " AND date>='$currentMonthDate'";
                  break;

      case 'all':
                  break;
   }

   if(! $dbHandle->Exec($sqlCMD))
   {
     print $dbHandle->GetError();
     return false;
   }

   if(! $dbHandle->FetchRows())
   {
      print GetErrorString("QUOTE_STATUS_ID_NOT_FOUND");
      return false;
   }

   return $dbHandle->GetFieldValue("leads");
}

?>

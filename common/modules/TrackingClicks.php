<?php

// We use this part for tracking. QZ - this is for Quotezone. SYSTEM - system from where the call was made (e.g. home)
   if (empty($_POST))
   {
      if (array_key_exists('source', $_GET))
      {
         $_SESSION['tracking']['source'] = $_GET['source'];
         if (array_key_exists('system', $_GET))
            $_SESSION['tracking']['system'] = $_GET['system'];
         else
            $_SESSION['tracking']['system'] = "NONE";
      }
      else
      {
         if (! array_key_exists('source', $_SESSION['tracking']))
            $_SESSION['tracking']['source'] = "";
         if (! array_key_exists('system', $_SESSION['tracking']))
            $_SESSION['tracking']['system'] = "";
      }
      $_SESSION['tracking']['remoteIP'] = $_SERVER['REMOTE_ADDR'];
      $_SESSION['tracking']['userBrowserClient'] = $_SERVER['HTTP_USER_AGENT'];
      $_SESSION['tracking']['date'] = date("Y-m-d");
      $_SESSION['tracking']['time'] = date("H:i:s");
      $_SESSION['tracking']['quote_type_id'] = $quoteTypeID;
      if (! array_key_exists('session_id', $_SESSION['tracking']))
      {
         $_SESSION['tracking']['session_id'] = 0;
      }
      if (array_key_exists('ACTIVE STEP', $_SESSION))
      {
         $_SESSION['tracking']['step'] = $_SESSION['ACTIVE STEP'];
         if ($_SESSION['tracking']['step'] == '1')
            $_SESSION['tracking']['unique_step_id'] = 0;
      }
      else
      {
         $_SESSION['tracking']['step'] = "1";
      }
      if (! array_key_exists('unique_step_id', $_SESSION['tracking']))
         $_SESSION['tracking']['unique_step_id'] = 0;
      if (array_key_exists('_QZ_QUOTE_DETAILS_', $_SESSION))
         $_SESSION['tracking']['log_id'] = $_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id'];
      else
         $_SESSION['tracking']['log_id'] = 0;

      include_once "../../common/modules/Tracking.php";
      $tracking = new Tracking($quoteTypeID);
      $tracking->setTrackingInformation($_SESSION['tracking']);
      $_SESSION['tracking']['unique_step_id'] = $tracking->getUniqueStepId();
      if ($_SESSION['tracking']['session_id'] == 0)
         $_SESSION['tracking']['session_id'] = $_SESSION['tracking']['unique_step_id'];
      unset($tracking);
   }

?>
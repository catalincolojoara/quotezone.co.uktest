<?php

/*****************************************************************************/
/*                                                                           */
/*  CLogLeadsQuoteTimes class interface                                      */
/*                                                                           */
/*  (C) 2012 Alexandru Furtuna (alex@acrux.biz)                              */
/*                                                                           */
/*****************************************************************************/
include_once "globals.inc";
include_once "MySQL.php";

//////////////////////////////////////////////////////////////////////////////PE
//
// [CLASS NAME]:   CLogLeadsQuoteTimes
//
// [DESCRIPTION]:  CLogLeadsQuoteTimes class interface
//
// [FUNCTIONS]:    void Initialise();
//                 bolean 
//
//
// [CREATED BY]:   Alexandru Furtuna (alex@acrux.biz) 2012-05-10
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CLogLeadsQuoteTimes
{

   var $objMySql;            // CMySQL class object
   var $strERR;              // last Log error string
   var $closeDB;             // close db variable

   /*
   CREATE TABLE `leads_quote_times` (
      `id` bigint(20) unsigned NOT NULL auto_increment,
      `quote_type_id` mediumint(8) unsigned NOT NULL default '0',
      `log_id` bigint(20) unsigned NOT NULL default '0',
      `load_time` varchar(128) NOT NULL default '',
      `server_ip` varchar(32) NOT NULL default '',
      `date` date default '0000-00-00',
      PRIMARY KEY  (`id`),
      KEY `log_id` (`log_id`),
      KEY `qt_key` (`quote_type_id`)
   ) ENGINE=MyISAM

   //new table for more details
   CREATE TABLE `leads_quote_times_details` (
      `id` bigint(20) unsigned NOT NULL auto_increment,
      `log_id` bigint(20) unsigned NOT NULL default '0',
      `process_description` varchar(254) NOT NULL DEFAULT '',
      `load_time` varchar(5) NOT NULL default '',
      PRIMARY KEY  (`id`),
      KEY `log_id` (`log_id`),
   ) ENGINE=MyISAM

    */

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CLogLeadsQuoteTimes
   //
   // [DESCRIPTION]:   Class constructor
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CLogLeadsQuoteTimes($dbh)
   {
      if($dbh)
      {
         $this->objMySql  = $dbh;
         $this->closeDB = false;
      }
      else
      {
         // default configuration
         $this->objMySql = new CMySQL();

         if(! $this->objMySql->Open(DBNAME, DBHOST, DBUSER, DBPASS))
         {
           $this->strERR = $this->dbh->GetError();
           return;
         }

         $this->closeDB = true;
      }

      $this->strERR = "";
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AssertLeadQuoteTimes
   //
   // [DESCRIPTION]:   Checks the field values
   //
   // [PARAMETERS]:    $quoteTypeID, $logID, $loadTime, $serverIP, $date'
   //
   // [RETURN VALUE]:  true/false
   //
   // [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-05-10
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AssertLeadQuoteTimes($quoteTypeID=0,$logID=0,$loadTime=0,$serverIP='',$date='0000-00-00')
   {

      if(! preg_match("/\d+/",$quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
         return false;
      }

      if(! preg_match("/\d+/",$logID))
      {
         $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");
         return false;
      }

      if(! preg_match("/\d+/",$loadTime))
      {
         $this->strERR = GetErrorString("INVALID_LOAD_TIME_FIELD");
         return false;
      }

      if(empty($serverIP))
      {
         $this->strERR .= GetErrorString("INVALID_LOG_HOST_IP_FIELD");
         return false;
      }

      if(empty($date))
      {
         $this->strERR .= GetErrorString("INVALID_DATE_FIELD");
         return false;
      }

      return true;

   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AddLeadQuoteTimesToDb
   //
   // [DESCRIPTION]:   Adds the lead quote times to the databse
   //
   // [PARAMETERS]:    $quoteTypeID, $logID, $loadTime, $serverIP, $date'
   //
   // [RETURN VALUE]:  0 or ID
   //
   // [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-05-10
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AddLeadQuoteTimesToDb($quoteTypeID=0,$logID=0,$loadTime=0,$serverIP='',$date='0000-00-00')
   {
      if(! $this->AssertLeadQuoteTimes($quoteTypeID,$logID,$loadTime,$serverIP,$date))
      {
         $this->GetError();
         return 0;
      }

      $this->lastSQLCMD = "INSERT INTO leads_quote_times VALUES ('','$quoteTypeID','$logID','$loadTime','$serverIP','$date')";

      //print "$sqlCMD <br>";

      if(! $this->objMySql->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->objMySql->GetError();
			return 0;
		}

		$this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

		if(! $this->objMySql->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->objMySql->GetError();
			return 0;
		}

		if(! $this->objMySql->FetchRows())
		{
			$this->strERR = $this->objMySql->GetError();
			return 0;
		}

		return $this->objMySql->GetFieldValue("id");
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AddLeadQuoteTimeDetailsToDb
   //
   // [DESCRIPTION]:   Adds the lead quote time details to the databse
   //
   // [PARAMETERS]:    $logID=0,$loadTimeDetailsArray=array()
   //
   // [RETURN VALUE]:  true/false 
   //
   // [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2016-03-10
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AddLeadQuoteTimeDetailsToDb($logID=0,$loadTimeDetailsArray=array())
   {

   /*
      `id`
      `log_id`
      `process_description`
      `load_time`
   */
      //checks
      if(! preg_match("/\d+/",$logID))
      {
         $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");
         return false;
      }

      if(!is_array($loadTimeDetailsArray) AND count($loadTimeDetailsArray) == 0)
      {
         $this->strERR = GetErrorString("INVALID_DETAILS_ARRAY_SUPPLIED");
         return false;
      }

      foreach($loadTimeDetailsArray as $message => $loadTime)
      {
         $this->lastSQLCMD = "INSERT INTO leads_quote_times_details VALUES ('','$logID','$message','$loadTime')";

         if(! $this->objMySql->Exec($this->lastSQLCMD))
         {
            $this->strERR = $this->objMySql->GetError();
            return false;
         }
      }

     
      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetLeadsQuoteTimeByQuoteTypeIDAndDate()
   //
   // [DESCRIPTION]:   Returns the details by specified fields
   //
   // [PARAMETERS]:    $quoteTypeID, $date
   //
   // [RETURN VALUE]:  false / details
   //
   // [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-05-10
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetLeadsQuoteTimeByQuoteTypeIDAndDate($quoteTypeID=0,$date='0000-00-00')
   {
      
      if(! preg_match("/\d+/",$quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
         return false;
      }
      
      if(empty($date))
      {
         $this->strERR .= GetErrorString("INVALID_DATE_FIELD");
         return false;
      }

      $this->lastSQLCMD = "SELECT * FROM leads_quote_times WHERE quote_type_id = $quoteTypeID AND date = '$date'";

      if(! $this->objMySql->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->objMySql->GetError();
			return false;
		}

      if(! $this->objMySql->GetRows())
      {
         $this->strERR = GetErrorString("DETAILS_NOT_FOUND");
         return false;
      }

      $index = 0;
      while($this->objMySql->MoveNext())
      {
         $logID = $this->objMySql->GetFieldValue("log_id");

         $arrayResult[$logID]["id"]             = $this->objMySql->GetFieldValue("id");
         $arrayResult[$logID]["quote_type_id"]  = $this->objMySql->GetFieldValue("quote_type_id");
         $arrayResult[$logID]["log_id"]         = $this->objMySql->GetFieldValue("log_id");
         $arrayResult[$logID]["load_time"]      = $this->objMySql->GetFieldValue("load_time");
         $arrayResult[$logID]["host_ip"]        = $this->objMySql->GetFieldValue("host_ip");
         $arrayResult[$logID]["date"]           = $this->objMySql->GetFieldValue("date");
         $arrayResult[$logID]["server_ip"]      = $this->objMySql->GetFieldValue("server_ip");
      }
 
      return $arrayResult;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetLeadsQuoteTimeByQuoteTypeIDAndInterval()
   //
   // [DESCRIPTION]:   Returns the details by specified fields
   //
   // [PARAMETERS]:    $quoteTypeID, $startDate,$endDate
   //
   // [RETURN VALUE]:  false / details
   //
   // [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-05-10
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetLeadsQuoteTimeByQuoteTypeIDAndInterval($quoteTypeID=0,$startDate='0000-00-00',$endtDate='0000-00-00')
   {

      if(! preg_match("/\d+/",$quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
         return false;
      }

      if(empty($startDate))
      {
         $this->strERR .= GetErrorString("INVALID_START_DATE_FIELD");
         return false;
      }

      if(empty($endtDate))
      {
         $this->strERR .= GetErrorString("INVALID_END_DATE_FIELD");
         return false;
      }

      $this->lastSQLCMD = "SELECT * FROM leads_quote_times WHERE quote_type_id = $quoteTypeID AND date BETWEEN '$startDate' AND '$endtDate'";

      //print $this->lastSQLCMD ."<br>";

      if(! $this->objMySql->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->objMySql->GetError();
			return 0;
		}

      if(! $this->objMySql->GetRows())
      {
         $this->strERR = GetErrorString("DETAILS_NOT_FOUND");
         return false;
      }

      $index = 0;
      while($this->objMySql->MoveNext())
      {
         $logID = $this->objMySql->GetFieldValue("log_id");

         $arrayResult[$logID]["id"]             = $this->objMySql->GetFieldValue("id");
         $arrayResult[$logID]["quote_type_id"]  = $this->objMySql->GetFieldValue("quote_type_id");
         $arrayResult[$logID]["log_id"]         = $this->objMySql->GetFieldValue("log_id");
         $arrayResult[$logID]["load_time"]      = $this->objMySql->GetFieldValue("load_time");
         $arrayResult[$logID]["host_ip"]        = $this->objMySql->GetFieldValue("host_ip");
         $arrayResult[$logID]["date"]           = $this->objMySql->GetFieldValue("date");
         $arrayResult[$logID]["server_ip"]      = $this->objMySql->GetFieldValue("server_ip");
      }

      return $arrayResult;

   }
      //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetLeadsQuoteAVGTimeByQuoteTypeIDAndInterval()
   //
   // [DESCRIPTION]:   Returns the details by specified fields
   //
   // [PARAMETERS]:    $quoteTypeID, $startDate,$endDate
   //
   // [RETURN VALUE]:  false / details
   //
   // [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-05-10
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetLeadsQuoteAVGTimeByQuoteTypeIDAndInterval($quoteTypeID=0,$startDate='0000-00-00',$endtDate='0000-00-00')
   {

      if(! preg_match("/\d+/",$quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
         return false;
      }

      if(empty($startDate))
      {
         $this->strERR .= GetErrorString("INVALID_START_DATE_FIELD");
         return false;
      }

      if(empty($endtDate))
      {
         $this->strERR .= GetErrorString("INVALID_END_DATE_FIELD");
         return false;
      }

      $this->lastSQLCMD = "SELECT AVG(load_time) as average,date FROM leads_quote_times WHERE quote_type_id = $quoteTypeID AND date BETWEEN '$startDate' AND '$endtDate' GROUP BY quote_type_id,date";

      //print $this->lastSQLCMD ."<br>";

      if(! $this->objMySql->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->objMySql->GetError();
			return false;
		}

      if(! $this->objMySql->GetRows())
      {
         $this->strERR = GetErrorString("DETAILS_NOT_FOUND");
         return false;
      }

      while($this->objMySql->MoveNext())
      {
         $date = $this->objMySql->GetFieldValue("date");

         $arrayResult[$date]["average"]         = $this->objMySql->GetFieldValue("average");
      }
 
      return $arrayResult;

   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetAllLeadsQuoteTimeByInterval()
   //
   // [DESCRIPTION]:   Returns the details by specified fields
   //
   // [PARAMETERS]:    $startDate,$endDate
   //
   // [RETURN VALUE]:  false / details
   //
   // [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-05-10
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetAllLeadsQuoteTimeByInterval($startDate='0000-00-00',$endtDate='0000-00-00')
   {
      if(empty($startDate))
      {
         $this->strERR .= GetErrorString("INVALID_START_DATE_FIELD");
         return false;
      }

      if(empty($endtDate))
      {
         $this->strERR .= GetErrorString("INVALID_END_DATE_FIELD");
         return false;
      }

      $sqlCMD = "SELECT * FROM leads_quote_times WHERE date BETWEEN '$startDate' AND '$endtDate'";

      if(! $this->objMySql->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->objMySql->GetError();
			return 0;
		}

      if(! $this->objMySql->GetRows())
      {
         $this->strERR = GetErrorString("DETAILS_NOT_FOUND");
         return false;
      }

      $index = 0;
      while($this->objMySql->MoveNext())
      {
         $logID = $this->objMySql->GetFieldValue("log_id");

         $arrayResult[$logID]["id"]             = $this->objMySql->GetFieldValue("id");
         $arrayResult[$logID]["quote_type_id"]  = $this->objMySql->GetFieldValue("quote_type_id");
         $arrayResult[$logID]["log_id"]         = $this->objMySql->GetFieldValue("log_id");
         $arrayResult[$logID]["load_time"]      = $this->objMySql->GetFieldValue("load_time");
         $arrayResult[$logID]["host_ip"]        = $this->objMySql->GetFieldValue("host_ip");
         $arrayResult[$logID]["date"]           = $this->objMySql->GetFieldValue("date");
         $arrayResult[$logID]["server_ip"]      = $this->objMySql->GetFieldValue("server_ip");
      }

      return $arrayResult;

   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetLeadsQuoteTimeDetailsByLogID()
   //
   // [DESCRIPTION]:   Returns the details by specified fields
   //
   // [PARAMETERS]:    $quoteTypeID, $date
   //
   // [RETURN VALUE]:  false / details
   //
   // [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-05-10
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetLeadsQuoteTimeDetailsByLogID($logID=0)
   {
      
      if(! preg_match("/\d+/",$logID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
         return false;
      }      

      $this->lastSQLCMD = "SELECT * FROM leads_quote_times_details WHERE log_id = '$logID'";

      if(! $this->objMySql->Exec($this->lastSQLCMD))
      {
         $this->strERR = $this->objMySql->GetError();
         return false;
      }

      if(! $this->objMySql->GetRows())
      {
         $this->strERR = GetErrorString("DETAILS_NOT_FOUND");
         return false;
      }

      /*
      CREATE TABLE `leads_quote_times_details` (
      `id` bigint(20) unsigned NOT NULL auto_increment,
      `log_id` bigint(20) unsigned NOT NULL default '0',
      `process_description` varchar(254) NOT NULL DEFAULT '',
      `load_time` varchar(5) NOT NULL default '',
      PRIMARY KEY  (`id`),
      KEY `log_id` (`log_id`),
      */

      $index = 0;
      while($this->objMySql->MoveNext())
      {
         $logID = $this->objMySql->GetFieldValue("log_id");

         $arrayResult[$logID]["id"]                  = $this->objMySql->GetFieldValue("id");
         $arrayResult[$logID]["process_description"] = $this->objMySql->GetFieldValue("process_description");
         $arrayResult[$logID]["load_time"]           = $this->objMySql->GetFieldValue("load_time");
      }
 
      return $arrayResult;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetError
   //
   // [DESCRIPTION]:   Prints the error details
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  error
   //
   // [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-05-10
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetError()
   {
      //print $this->strERR;
      return $this->strERR;
   }

   
}

?>

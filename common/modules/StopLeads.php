<?php
/**
 * Class used to check if the selected insurer from the selected system has reach an predetermined number of quotes.
 *
 * The class is used by the insurer modules where we need to check this limit
 *
 * @author Ando Ciupav Cristian
 * @copyright SC ACRUX SOFTWARE SRL, 2010, Timisoara
 * @version 1.0
 * @package common
 */

include_once "MySQL.php";

class StopLeads
{
   /**
    * This variable will hold the quote type id of the system (as is defined in database)
    *
    * @var integer
    * @see StopLeads()
    */
   private $quoteTypeId;

   /**
    * Constructor for the class
    *
    * The constructor take as argument the quote type id of the system
    *
    * @param integer $quoteTypeId
    * @return void
    */
   public function StopLeads($quoteTypeId = 0)
   {
      $this->quoteTypeId = $quoteTypeId;
   }

   /**
    * function GetDailyLeads()
    *
    * This function will check the parameters received and will return the number of quotes.
    * The function need at least the site ID as parameter.
    * The start date: if missing, will be forced to current date
    * The format for date is: yyyy-mm-dd
    * The $hour variable is used to filter leads under certain hour value (check only leads made after $hour)
    * The function only check if the parameters are correct and the call the GetTotalLeads() function
    *
    * @param integer $siteId
    * @param date $date
    * @param date $hour
    * @return integer
    * @see GetTotalDailyLeads()
    */
   public function GetDailyLeads($siteId = 0, $date = '', $hour = 0)
   {
      $result = 0;
      if ($siteId == 0)
         return 0;
      if (trim($date) == '')
         $date = date("Y-m-d");
      $result = $this->GetTotalDailyLeads($siteId, $date, $hour, 0);
      return $result;
   }

   /**
    * function GetUniqueDailyLeads()
    *
    * This function will check the parameters received and will return the number of unique quotes.
    * The function need at least the site ID as parameter.
    * The start date: if missing, will be forced to current date
    * The format for date is: yyyy-mm-dd
    * The $hour variable is used to filter leads under certain hour value (check only leads made after $hour)
    * The function only check if the parameters are correct and the call the GetTotalLeads() function
    *
    * @param integer $siteId
    * @param date $date
    * @param date $hour
    * @return integer
    * @see GetTotalDailyLeads()
    */
   public function GetUniqueDailyLeads($siteId = 0, $date = '', $hour = 0)
   {
      $result = 0;
      if ($siteId == 0)
         return 0;
      if (trim($date) == '')
         $date = date("Y-m-d");
      $result = $this->GetTotalDailyLeads($siteId, $date, $hour, 1);
      return $result;
   }

   /**
    * function GetLeads()
    *
    * This function will check the parameters received and will return the number of quotes.
    * The function need at least the site ID as parameter.
    * The start date and end date, if missing, will be forced to current date
    * The format for date is: yyyy-mm-dd
    * The function only check if the parameters are correct and the call the GetTotalLeads() function
    *
    * @param integer $siteId
    * @param date $startDate
    * @param date $endDate
    * @return integer
    * @see GetTotalLeads()
    */
   public function GetLeads($siteId = 0, $startDate = '', $endDate = '')
   {
      $result = 0;
      if ($siteId == 0)
         return 0;
      if (trim($startDate) == '')
         $startDate = date("Y-m-d");
      if (trim($endDate) == '')
         $endDate = date("Y-m-d");
      $result = $this->GetTotalLeads($siteId, $startDate, $endDate);
      return $result;
   }

   /**
    * function GetTotalLeads()
    *
    * Function for retrieving total number of quotes from database
    * This function will check for the quotes in database and return the resulting value.
    * If there are any error, will return a zero value.
    *
    * @param integer $siteId
    * @param date $startDate
    * @param date $endDate
    * @return integer
    */
   private function GetTotalLeads($siteId = 0, $startDate, $endDate)
   {
      $result = 0;
      $dbObj = new CMySQL();
      $dbObj->Open();
      $sql  = "select count(qs.id) as total from quote_status qs, qzquotes qz, sites st, logs lg where qz.quote_type_id=" . $this->quoteTypeId . " and ";
      $sql .= "qz.log_id=qs.log_id and qs.site_id=".$siteId." and st.id=qs.site_id and lg.id=qz.log_id and qs.status != 'SKIPPED' and ";
      $sql .= "lg.date between '" . $startDate . "' and '" . $endDate . "' and lg.host_ip != '81.196.65.167'";
      if (! $dbObj -> Exec($sql))
      {
         unset($dbObj);
         return $result;
      }
      if (! $dbObj->GetRows())
      {
         unset($dbObj);
         return $result;
      }
      $row = $dbObj->FetchRowsA();
      $result = $row['total'];
      unset($dbObj);
      return $result;
   }

   /**
    * function GetTotalDailyLeads()
    *
    * Function for retrieving total number of unique quotes from database (unique ip)
    * This function will check for the quotes in database and return the resulting value.
    * If there are any error, will return a zero value.
    * $unique is used to force checking of total quotes (value=0 default) or total unique quotes (value=1)
    *
    * @param integer $siteId
    * @param date $startDate
    * @param date $endDate
    * @param integer $unique
    * @return integer
    */

   private function GetTotalDailyLeads($siteId, $date, $hour, $unique = 0)
   {
      $result = 0;
      $dbObj = new CMySQL();
      $dbObj->Open();
      if (! $unique)
         $sql = "select count(qs.id) as total";
      else
         $sql = "select count(distinct(lg.host_ip)) as total";
      $sql .= " from quote_status qs, qzquotes qz, sites st, logs lg where qz.quote_type_id=" . $this->quoteTypeId . " and ";
      $sql .= "qz.log_id=qs.log_id and qs.site_id=".$siteId." and st.id=qs.site_id and lg.id=qz.log_id and qs.status != 'SKIPPED' and ";
      $sql .= "lg.date='" . $date . "' and lg.host_ip != '81.196.65.167'";
      if ($hour)
         $sql .= " and HOUR(lg.time) >= ".$hour;
      if (! $dbObj -> Exec($sql))
      {
         unset($dbObj);
         return $result;
      }
      if (! $dbObj->GetRows())
      {
         unset($dbObj);
         return $result;
      }
      $row = $dbObj->FetchRowsA();
      $result = $row['total'];
      unset($dbObj);
      return $result;
   }

}

?>
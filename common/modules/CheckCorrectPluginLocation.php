<?php

include_once "/home/www/quotezone.co.uk/insurance-new/modules/globals.adm.inc";
include_once "/home/www/quotezone.co.uk/common/modules/globals.inc";
include_once "/home/www/quotezone.co.uk/insurance-new/modules/Site.php";

$objSite = new CSite();


$systemConfPathArray = array(
   "15" => "/home/www/quotezone.co.uk/trailer-tent-insurance/trailertent/sites/config/",
   "16" => "/home/www/quotezone.co.uk/boat-insurance/boat/sites/config/",
   "20" => "/home/www/quotezone.co.uk/limo-insurance/limo/sites/config/",
   "23" => "/home/www/quotezone.co.uk/icecream-van-insurance/icecreamvan/sites/config/",
   "24" => "/home/www/quotezone.co.uk/catering-van-insurance/cateringvan/sites/config/",
   "27" => "/home/www/quotezone.co.uk/park-home-insurance/parkhome/sites/config/",
   "30" => "/home/www/quotezone.co.uk/gap-insurance/gap/sites/config/",
   "32" => "/home/www/quotezone.co.uk/taxi-fleet-insurance/taxifleet/sites/config/",
   "36" => "/home/www/quotezone.co.uk/pub-insurance/pub/sites/config/",
   "37" => "/home/www/quotezone.co.uk/surgery-insurance/surgery/sites/config/",
   "38" => "/home/www/quotezone.co.uk/polish-insurance/polish/sites/config/",
   "41" => "/home/www/quotezone.co.uk/kit-car-insurance/kitcar/sites/config/",
   "48" => "/home/www/quotezone.co.uk/private-medical-insurance/pmi/sites/config/",
   "51" => "/home/www/quotezone.co.uk/care-home-insurance/carehome/sites/config/",
   "53" => "/home/www/quotezone.co.uk/disabled-car-insurance/disabledcar/sites/config/",
   "55" => "/home/www/quotezone.co.uk/prestige-car-insurance/prestigecar/sites/config/",
   "57" => "/home/www/quotezone.co.uk/modified-car-insurance/modifiedcar/sites/config/",
   "58" => "/home/www/quotezone.co.uk/hotel-insurance/hotel/sites/config/",
   "61" => "/home/www/quotezone.co.uk/mortgages/mortgages/sites/config/",
   "62" => "/home/www/quotezone.co.uk/investments/investments/sites/config/",
   "63" => "/home/www/quotezone.co.uk/debt-help/debt-help/sites/config/",
   "64" => "/home/www/quotezone.co.uk/annuities/annuities/sites/config/",
   "65" => "/home/www/quotezone.co.uk/pensions/pensions/sites/config/",
   "66" => "/home/www/quotezone.co.uk/loans/loans/sites/config/",

   //wl4 systems
   "10" => "/home/www/quotezone.co.uk/caravan-insurance/caravan/sites/config/",
   "11" => "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/motorhome/sites/config/",
   "12" => "/home/www/quotezone.co.uk/life-insurance/life/sites/config/",
   "13" => "/home/www/quotezone.co.uk/health-insurance/health/sites/config/",
   "14" => "/home/www/quotezone.co.uk/campervan-insurance/campervan/sites/config/",
   "18" => "/home/www/quotezone.co.uk/taxi-insurance/taxi/sites/config/",
   "19" => "/home/www/quotezone.co.uk/minibus-insurance/minibus/sites/config/",
   "21" => "/home/www/quotezone.co.uk/coach-insurance/coach/sites/config/",
   "22" => "/home/www/quotezone.co.uk/horsebox-insurance/horsebox/sites/config/",
   "25" => "/home/www/quotezone.co.uk/driving-school-insurance/drivingschool/sites/config/",
   "26" => "/home/www/quotezone.co.uk/motor-trade-insurance/motortrade/sites/config/",
   "28" => "/home/www/quotezone.co.uk/landlords-insurance/landlords/sites/config/",
   "29" => "/home/www/quotezone.co.uk/courier-insurance/courier/sites/config/",
   "31" => "/home/www/quotezone.co.uk/motor-fleet-insurance/motorfleet/sites/config/",
   "33" => "/home/www/quotezone.co.uk/truck-insurance/truck/sites/config/",
   "34" => "/home/www/quotezone.co.uk/office-insurance/office/sites/config/",
   "35" => "/home/www/quotezone.co.uk/shop-insurance/shop/sites/config/",
   "39" => "/home/www/quotezone.co.uk/import-car-insurance/importcar/sites/config/",
   "40" => "/home/www/quotezone.co.uk/professional-indemnity-insurance/professionalindemnity/sites/config/",
   "42" => "/home/www/quotezone.co.uk/tradesman-insurance/tradesman/sites/config/",
   "43" => "/home/www/quotezone.co.uk/4x4-insurance/4x4/sites/config/",
   "44" => "/home/www/quotezone.co.uk/classic-car-insurance/classiccar/sites/config/",
   "45" => "/home/www/quotezone.co.uk/income-protection-insurance/incomeprotection/sites/config/",
   "46" => "/home/www/quotezone.co.uk/public-liability-insurance/publicliability/sites/config/",
   "47" => "/home/www/quotezone.co.uk/convicted-driver-insurance/convicteddriver/sites/config/",
   "50" => "/home/www/quotezone.co.uk/mortgage-protection-insurance/mortgageprotection/sites/config/",
   "52" => "/home/www/quotezone.co.uk/holiday-home-insurance/holidayhome/sites/config/",
   "54" => "/home/www/quotezone.co.uk/horse-insurance/horse/sites/config/",
   "56" => "/home/www/quotezone.co.uk/performance-car-insurance/performancecar/sites/config/",
   "59" => "/home/www/quotezone.co.uk/salon-insurance/salon/sites/config/",
   "60" => "/home/www/quotezone.co.uk/young-driver-insurance/youngdriver/sites/config/",
   "67" => "/home/www/quotezone.co.uk/employers-liability-insurance/employersliability/sites/config/",
   "68" => "/home/www/quotezone.co.uk/non-standard-home-insurance/nonstandardhome/sites/config/",
   "69" => "/home/www/quotezone.co.uk/quad-insurance/quad/sites/config/",
   "70" => "/home/www/quotezone.co.uk/restaurant-insurance/restaurant/sites/config/",
   "71" => "/home/www/quotezone.co.uk/multi-car-insurance/multicar/sites/config/",
   "72" => "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/bicycle/sites/config/",
   "73" => "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/goodsintransit/sites/config/",
   "74" => "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/wedding/sites/config/",
   "75" => "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/unoccupiedproperty/sites/config/",
   "76" => "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/event/sites/config/",
   "77" => "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/fitnessinstructor/sites/config/",
   "78" => "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/directorsofficers/sites/config/",
   "79" => "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/markettrader/sites/config/",
);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//   This function returns the sites form the database for all the companies on one systen
//   We will pass the quote_type_id and the function will return all the companies in the DB
//
//   Created by PhpStorm
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function CheckConfFileCorrectLocation()
{

   global $systemConfPathArray;
   global $objSite;

   foreach($systemConfPathArray as $quoteTypeID => $fileDirLocation)
   {

      print "Start checking $fileDirLocation \n";

      //read the files from the conf directory
      $systemSiteIDArray  = array();
      $filesInFolderArray = array();
      $filesInFolderArray = scandir($fileDirLocation);

      foreach($filesInFolderArray as $k => $confFileName)
      {
         $fileDetailsArr = "";
         $fileDetailsArr = explode(".",$confFileName);

         if(! preg_match("/^\d+/isU",$fileDetailsArr[0]))
         {
            continue;
         }

         if(preg_match("/[a-z][A-Z]/isU",$fileDetailsArr[0]))
         {
            continue;
         }

         if($fileDetailsArr[1] != 'inc')
         {
            continue;
         }

         $siteIDAndQTIDArr = "";
         $siteIDAndQTIDArr = explode("_",$fileDetailsArr[0]);

         $systemSiteIDArray[$siteIDAndQTIDArr[0]] = $siteIDAndQTIDArr[1];
      }

      //returns array details in the format site_id->site_name
      $systemSiteDetailsArray = array();
      $systemSiteDetailsArray = $objSite->GetAllSites($quoteTypeID);

      print "For the directory : [$fileDirLocation] \n  files are :<pre> ";
      print_r($systemSiteIDArray);
//      print "\n DB details are : \n";
//      print_r($systemSiteDetailsArray);
      print "<pre>";

      foreach($systemSiteIDArray as $sID => $qtID)
      {
         if(! array_key_exists($sID,$systemSiteDetailsArray))
            print "File [$sID] exists but is not in the DB for this system [$quoteTypeID] \n\n";
      }

      print "Finished checking $fileDirLocation \n\n\n";

   }
}//function CheckConfFileCorrectLocation()



CheckConfFileCorrectLocation();



?>

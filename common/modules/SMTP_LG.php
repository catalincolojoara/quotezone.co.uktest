<?php

session_start();

/*****************************************************************************/
/*                                                                           */
/* SMTP class implementation                                                 */
/*                                                                           */
/*  (C) 2003 Eugen Savin (eugen@claspsolutions.com)                          */
/*                                                                           */
/*****************************************************************************/

include_once "TLSEmails.php";
include_once "TLSScore.php";
include_once "TlsUnder90Sender.php";

define("SMTP_INCLUDED", "1");

if(DEBUG_MODE)
   error_reporting(0);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CSMTP
//
// [DESCRIPTION]:  SMTP interface for the mail servers, PHP version
//                 SMTP intermediate code: 354
//                 SMTP success codes: 250,221,220
//                 SMTP failure codes: 552,554,451,452
//                 SMTP error codes  : 500,501,503,421
//
// [FUNCTIONS]:    bool   Open($host="", $port=25, $timeout=10);
//                 string|false ReadData();
//                 bool   WriteData($data="");
//                 bool   SendMail($mailFrom="", $mailTo="", $mailSubject="", $mailBody="", $headers="");
//
//                 void SetHost($host="localhost");
//                 void SetPort($port=25);
//                 void SetTimeout($timeout=10);
//                 void SetDebug($debug=1);
//
//                 string GetHost();
//                 int    GetPort();
//                 int    GetTimeout();
//                 string GetMessageID();
//                 int    GetDebug();
//
//                 string GenerateMimeBoundary();
//
//                 GetError();
//                 Close();
//
// [CREATED BY]:   Eugen SAVIN (eugen@claspsolutions.com) 2003-10-31
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CSMTP
{
   // default mailer configuration
   var $host;             // default SMTP host name
   var $port;             // default SMTP port
   var $crlf;             // line ending chars
   var $timeout;          // timeout in seconds when connecting to a server
   var $debug;            // debug mode
   var $smtpConn;         // SMTP connection socket handle
   var $messageID;        // mail's message ID
   var $reply;            // mail's reply
   var $tlsEmails;        // tls emails object
   var $tlsScore;         // tls score object
   var $session;          // session
   var $tlsUnder90Sender; // create emails for tls < 90

   var $strERR;      // last error string


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CSMTP
//
// [DESCRIPTION]:   Default class constructor. Initialisation goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CSMTP()
{
   //set AMAZON key
   if(PHP_VERSION != '5.2.11')
   {
      $this->host = "80.94.200.12";
   }
   else
   {
      $this->host = "10.2.12.12";
   }

   // all LG should use 10.2.24.134
   $this->host = "10.2.24.134";


   $this->port = 25;

   $this->crlf      = "\r\n";
   $this->timeout   = 10;

   $this->smtpConn  = 0;
   $this->debug     = 0;
   $this->messageID = '';

   $this->tlsEmails        = new CTLSEmails();
   $this->tlsScore         = new CTLSScore();
   $this->session          = $_SESSION;
   $this->tlsUnder90Sender = new CTlsUnder90Sender();

   $this->strERR    = "";
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Open
//
// [DESCRIPTION]:   Open a connection with the SMTP server
//
// [PARAMETERS]:    host="", port=0, timeout=0
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2003-10-31
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Open($host="", $port=0 , $timeout=0)
{
   $this->SetHost($host);
   $this->SetPort($port);
   $this->SetTimeout($timeout);

   $this->strERR = "";

   if($this->smtpConn)
   {
      $this->strERR = "already connected to server ".$this->host;
      return false;
   }

   $errno  = "";
   $errror = "";

   $this->smtpConn = fsockopen($this->host, $this->port, $errno, $error, $this->timeout);
   if(! $this->smtpConn)
   {
      $this->strERR = "Failed to connect to ".$this->host.": $error ($errno)";

      if($this->host != "localhost")
      {
         $this->host = "localhost";
         $this->smtpConn = fsockopen($this->host, $this->port, $errno, $error, $this->timeout);
         if(! $this->smtpConn)
         {
            $this->strERR = "Failed to connect to ".$this->host.": $error ($errno)";
            return false;
         }
      }
      else
         return false;
   }

   // Windows still does not have support for this timeout function
   //if(substr(PHP_OS, 0, 3) != "WIN")
   //   socket_set_timeout($this->smtpConn, $this->timeout, 0);

   $greet = $this->ReadData();

   if($this->debug)
      print "$greet\n";

   if(! $greet)
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ReadData
//
// [DESCRIPTION]:   Read the data returned by the an SMTP command
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Data string if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2003-10-31
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ReadData()
{
   $this->strERR = "";
   $data = "";

   while($str = fgets($this->smtpConn, 128))
   {
      if($str === FALSE)
      {
         $this->strERR = "Cannot read data from the remote server. Peer has closed connection?";
         break;
      }

      $data .= $str;

      if(substr($str, 3, 1) == " ")
         break;
   }

   if($this->debug)
      print "READING DATA: $data\n";

   if(! empty($this->strERR))
      return false;

   return $data;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: WriteData
//
// [DESCRIPTION]:   Write data to the SMTP server
//
// [PARAMETERS]:    data string
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2003-10-31
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function WriteData($data="")
{
   $this->strERR = "";

   if($this->debug)
      print "SENDING DATA: $data\n";

   if(! fwrite($this->smtpConn, $data . $this->crlf))
   {
      $this->strERR = "Cannot write data to remote server. Peer has closed connection?";
      return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SendMail
//
// [DESCRIPTION]:   Send mail via the SMTP server
//
// [PARAMETERS]:    from, to, subject, message, $headers
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2003-10-31
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SendMail($mailFrom="", $mailTo="", $mailSubject="", $mailBody="", $headers="",$session="",$outboundType="",$siteID="")
{

   //trim any before and after spaces from mailTo
   $mailTo = trim($mailTo);

   //check Mailto in database to see what TLS score it has
   $tlsEmailsArray = array();
   $tlsScore       = "";
   $tlsEmailsArray = $this->tlsEmails->GetTLSEmailByEmail($mailTo);
   $tlsScoreArray  = $this->tlsScore->GetTLSScoreByEmailID($tlsEmailsArray["id"]);

   if($tlsScoreArray == 0)
      $tlsScore = 0;
   else
      $tlsScore = $tlsScoreArray["score"];

   $ftls = fopen("/tmp/tlsEmailsSending.txt", "a+");
   fwrite($ftls, "Checked score for [$mailTo] score is [$tlsScore] on quote ref[".$session["_QZ_QUOTE_DETAILS_"]["quote_reference"]."]\n");
   fclose($ftls);

   //$tlsScore       = "100"; // force not to send just to OUR emails
   if(preg_match("/\@seopa\.com/isu",$mailTo))
      $tlsScore = "100";

   //some outbounds should not be sent as TLS<90
   $acceptedOutboundType = true;
   if($outboundType == "CSV")
      $acceptedOutboundType = false;

   //it gets called only if tls under 90 and we have the details sent
   if($tlsScore < 90 AND $acceptedOutboundType AND $siteID)
   {

      $ftls = fopen("/tmp/tlsEmailsSending.txt", "a+");
      fwrite($ftls, "Accessing SendMail -> for siteID [$siteID] ON system [".$session["_QZ_QUOTE_DETAILS_"]["system"]."]\n");
      fwrite($ftls, "Sending for score [$tlsScore] to [$mailTo] quote ref[".$session["_QZ_QUOTE_DETAILS_"]["quote_reference"]."]\n");
      fwrite($ftls, "\n <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< \n");

      $emailContent = $this->tlsUnder90Sender->CreateEmailDetailsToSend($session, $mailBody, $outboundType, $siteID);

      //replace the email body
      $mailBody = $emailContent;

      if(preg_match("/text\/plain/isU",$headers))
      {
         //Content-type: text/html; charset=iso-8859-1
         $headers = str_replace('Content-type: text/plain', "Content-type: text/html", $headers);
      }

      fclose($ftls);
   }
   //end details for emails with < 90

   if(trim($mailFrom) == "")
   {
      if(preg_match('/From\s*\:.*<(.+)>/i', $headers, $fromMatchArr))
         $mailFrom = trim($fromMatchArr[1]);
   }

   $headers = preg_replace('/\r/i', "", $headers);
   $headers = preg_replace('/\n/i', "\r\n", $headers);
   $headers = preg_replace('/From\s*\:.+\r\n/i', "", $headers);
   $headers = preg_replace('/X-Sender\s*\:.+\r\n/i', "", $headers);
   $headers = preg_replace('/X-Mailer\s*\:.+\r\n/i', "", $headers);
   $headers = preg_replace('/X-Priority\s*\:.+\r\n/i', "", $headers);

   if(!preg_match('/attachment/i', $headers))
      $headers = preg_replace('/^\h*\v+/m', "", $headers);

   //$fptm = fopen("/tmp/tlsEmailsSending.txt", "a+");
   //fwrite($fptm, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
   //fwrite($fptm, "from: ".$mailFrom."\n");
   //fwrite($fptm, "subject: ".$mailSubject."\n\n");
   //fwrite($fptm, "headers:".$headers."\n");
   //fclose($fptm);

   if(SEND_ONLY_FOR_ACRUX_MAIL == "1" AND ! preg_match("/@acrux\.biz$/",$mailTo) )
      return;

   //print "SMTP sent:".$mailTo."\n";

   $this->messageID = "";
   $this->strERR    = "";

   if(! $this->smtpConn)
      $this->Open();

   if(! $this->smtpConn)
      return false;

   if(preg_match("/((.+)\s)?([^\s]+)$/i", $mailFrom, $matches))
   {
      $from = $matches[3];

      $from = str_replace("<", "",  $from);
      $from = str_replace(">", "",  $from);
   }

   if(preg_match("/((.+)\s)?([^\s]+)$/i", $mailTo, $matches))
   {
      $to = $matches[3];

      $to = str_replace("<", "",  $to);
      $to = str_replace(">", "",  $to);
   }

   if($this->WriteData("MAIL FROM: $from") < 0)
      return false;

   if(! ($reply = $this->ReadData()))
      return false;

   $code = substr($reply, 0, 3);
   if($code != 250)
   {
      // need helo command
      if($code == 503)
      {
         if($this->WriteData("EHLO localhost") < 0)
            return false;

         if(! ($reply = $this->ReadData()))
            return false;

         $code = substr($reply, 0, 3);
         if($code != 250)
         {
            $this->strERR = "EHLO command not accepted by server. Error code: $code";
            return false;
         }

         // re-send the mail from command
         if($this->WriteData("MAIL FROM: $from") < 0)
            return false;

         if(! ($reply = $this->ReadData()))
            return false;

         $code = substr($reply, 0, 3);
         if($code != 250)
         {
            $this->strERR = "MAIL FROM command not accepted by server. Error code: $code";
            return false;
         }
      } // if($code == 503)
      else
      {
         $this->strERR = "MAIL FROM command not accepted by server. Error code: $code";
         return false;
      }
   }

   if($this->WriteData("RCPT TO: $to") < 0)
      return false;

   if(! ($reply = $this->ReadData()))
      return false;

   $code = substr($reply, 0, 3);
   if($code != 250)
   {
      $this->strERR = "RCPT TO command not accepted by server. Error code: $code";
      return false;
   }

   if($this->WriteData("DATA") < 0)
      return false;

   if(! ($reply = $this->ReadData()))
      return false;

   $code = substr($reply, 0, 3);
   if($code != 354)
   {
      $this->strERR = "DATA command not accepted by server. Error code: $code";
      return false;
   }

   if($this->WriteData("From: $mailFrom") < 0)
      return false;

   if($this->WriteData("To: $mailTo") < 0)
      return false;

   if($this->WriteData("Subject: $mailSubject") < 0)
      return false;

   if(! empty($headers))
      if($this->WriteData($headers) < 0)
         return false;

   // end of header
   if($this->WriteData($this->crlf) < 0)
      return false;

   if($this->WriteData($mailBody) < 0)
      return false;

   if($this->WriteData(".") < 0)
      return false;

   $reply = $this->ReadData();
   $this->reply = $reply;

   $code  = substr($reply, 0, 3);

   //$fptm = fopen("/tmp/testmail.txt", "a+");
   //fwrite($fptm, "reply msg:\n".$reply."\n\n");
   //fwrite($fptm, "code: ".$code."\n");
   //fwrite($fptm, "\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
   //fclose($fptm);

   if($code != 250)
   {
      $this->strERR = "Mail body was not accepted by server. Error code: $code";
      return false;
   }

   if(preg_match('/250\s.+\s(.+)\sMessage accepted for delivery/', $reply, $matches))
      $this->messageID = $matches[1];
   elseif(preg_match('/250 2.0.0 Ok: queued as (.+)/', $reply, $matches))
      $this->messageID = trim($matches[1]);


   if($this->WriteData("QUIT") < 0)
      return false;

   $reply = $this->ReadData();
   $code  = substr($reply, 0, 3);

   if($code != 221)
   {
      $this->strERR = "Disconnect was not accepted by server. Error code: $code";
      return false;
   }

   $this->Close();

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetHost
//
// [DESCRIPTION]:   Set the host we connect to
//
// [PARAMETERS]:    $host = "localhost"
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2003-10-31
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetHost($host = "10.2.12.12")
{
   if(empty($host))
      return;

   //set AMAZON key

   if(PHP_VERSION != '5.2.11')
   {
      if ($host == '10.2.12.12')
      {
         $host = '80.94.200.12';
      }
   }

   // all LG should use 10.2.24.134
   $this->host = "10.2.24.134";


}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetPort
//
// [DESCRIPTION]:   Set the hosts's port we connect to
//
// [PARAMETERS]:    $port = 25
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2003-10-31
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetPort($port = 25)
{
   if(empty($port))
      return;

   if($port > 0 && $port < 65536)
      $this->port = $port;

   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetTimeout
//
// [DESCRIPTION]:   Set the connection timeout in seconds
//
// [PARAMETERS]:    $timeout = 10
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2003-10-31
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetTimeout($timeout = 10)
{
   if(empty($timeout))
      return;

   if($timeout > 0)
      $this->timeout = $timeout;

   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetDebug
//
// [DESCRIPTION]:   Set the connection timeout in seconds
//
// [PARAMETERS]:    $debug = 1
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2003-10-31
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetDebug($debug = 1)
{
   if(empty($debug))
      return;

   if($debug == 0 || $debug == 1)
      $this->debug = $debug;

   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetHost
//
// [DESCRIPTION]:   Return the host we connect to
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Host string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2003-10-31
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetHost()
{
   return $this->host;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetPort
//
// [DESCRIPTION]:   Return the hosts's port we connect to
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Port number
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2003-10-31
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetPort()
{
   return $this->port;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTimeout
//
// [DESCRIPTION]:   Get the connection timeout in seconds
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Timeout seconds number
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2003-10-31
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTimeout()
{
   return $this->timeout;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMessageID
//
// [DESCRIPTION]:   Returns the message ID after a mail send operation attempt
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  The message ID string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMessageID()
{
   return $this->messageID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDebug
//
// [DESCRIPTION]:   Return the debug mode
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Debug mode number
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDebug()
{
   return $this->debug;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GenerateMimeBoundary
//
// [DESCRIPTION]:   Generates a MIME boundary string
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  The MIME string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GenerateMimeBoundary()
{
   $garbleText = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','R','S','T','U','V','X','Y','W','Z');

   $maxIndex = count($garbleText) - 1;
   $mimeBoundary = "----=_NextPart_";

   mt_srand($this->make_seed());
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= "_";

   mt_srand($this->make_seed());
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= "_";

   mt_srand($this->make_seed());
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= ".";

   mt_srand($this->make_seed());
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];

   return $mimeBoundary;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2003-10-31
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
   return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the connection with the SMTP server
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2003-10-31
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
   if($this->smtpConn)
      fclose($this->smtpConn);

   $this->smtpConn = 0;
}

// Make random number
function make_seed()
{
   list($usec, $sec) = explode(' ', microtime());
   return (float) $sec + ((float) $usec * 100000000);
}

} // end CSMTP class
?>

<?php

include_once "/home/www/quotezone.co.uk/insurance-new/modules/globals.adm.inc";
include_once "/home/www/quotezone.co.uk/common/modules/globals.inc";
include_once "/home/www/quotezone.co.uk/common/modules/companies_logos_info.inc";
include_once "/home/www/quotezone.co.uk/insurance-new/modules/Site.php";

$objSite        = new CSite();
$logoOrderArray = array();

//High Gear Insurance loo id 529
//Cover My Cab (J&M) logo id 31
$logoOrderArray[18] = array(
                              "3466",
                              "511",
                           );

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//   This function returns the sites form the database for all the companies on one systen
//   We will pass the quote_type_id and the function will return all the companies in the DB
//
//   Created by PhpStorm
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function GetCompanieLogosByQuoteTypeID($quoteTypeID=0,&$newOrderSitesArray)
{
   global $logosInformationArray; // this is the file with the logo details -> company name and image name
   global $companiesLogosInfo;
   global $objSite;
   global $logoOrderArray;

   $imageDetailsContent = "";
   $imagesNamesArr      = array();
   $newOrderSitesArray  = array();

   //returns array details in the format site_id->site_name
   $systemSiteDetailsArray = $objSite->GetAllOnSites($quoteTypeID);

   //if we have details to reorder the details we recreate the array
   if($logoOrderArray[$quoteTypeID])
   {
      foreach($logoOrderArray[$quoteTypeID] as $k => $orderSID)
      {
         if(array_key_exists($orderSID,$systemSiteDetailsArray))
         {
            $newOrderSitesArray[$orderSID] = $systemSiteDetailsArray[$orderSID];
            unset($systemSiteDetailsArray[$orderSID]);
         }
      }
   }

   foreach($systemSiteDetailsArray as $siteID => $companyName)
   {
      $newOrderSitesArray[$siteID] = $companyName;
   }

   foreach($newOrderSitesArray as $orderedSiteID => $orderedCompanySiteName)
   {
      $companyLogoConnectionID = $companiesLogosInfo[$orderedSiteID];

      $imageName               = "";
      $companyName             = "";

      $imageName               = $logosInformationArray["brokers"][$companyLogoConnectionID]["logo"];
      $companyName             = $logosInformationArray["brokers"][$companyLogoConnectionID]["name"];

      if(array_key_exists($imageName,$imagesNamesArr))
         continue;

      //filter Velos Insurance gif from scroller
      if(preg_match("/velo\_01/isU",$imageName))
         continue;

      $imagesNamesArr[$imageName] = $companyName;

      if($imageName)
         $imageDetailsContent .= "<li><img src=\"https://wl4.quotezone.co.uk/core/classiccar/images/logos/$imageName\" style=\"width:100px;\" alt=\"$companyName\"></li>";
   }

   return $imageDetailsContent;

}//function GetCompanieLogosByQuoteTypeID($quoteTypeID=0)


?>

<?php

include_once "errors.inc";
include_once "MySQL.php";

define("SQL_LEAD_QUOTE_FILTERS_TABLE","lead_quote_filters");

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CLeadQuoteFilters
//
// [DESCRIPTION]:  CLeadQuoteFilters was created in order to work with 'lead_quote_filters' table
//
// [FUNCTIONS]:
//
//                string           GetError();
//
//
// [CREATED BY]:  Ciprian STURZA  (cipi@acrux.biz) 2011-11-21
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CLeadQuoteFilters
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CLeadQuoteFilters
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Ciprian STURZA  (cipi@acrux.biz) 2011-11-21
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CLeadQuoteFilters($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddLeadQuoteFilters
//
// [DESCRIPTION]:   Add new entry to the lead_quote_filters table
//
// [PARAMETERS]:    $siteID=0, $skipRule='', $logID=0, $details=""
//
// [RETURN VALUE]:  leadQuoteFilterID or false in case of failure
//
// [CREATED BY]:    Ciprian STURZA  (cipi@acrux.biz) 2011-11-21
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddLeadQuoteFilters($siteID=0, $logID=0, $skipRule='', $details="")
{
   if(! preg_match("/\d+/",$siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITE_ID_FIELD");
      return false;
   }

   if(! preg_match("/\d+/",$logID))
   {
      $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");
      return false;
   }

   if(empty($skipRule))
   {
      $this->strERR = GetErrorString("INVALID_SKIP_RULE_FIELD");
      return false;
   }

   $skipRule = addslashes($skipRule);
   $details  = addslashes($details);

   $this->lastSQLCMD = "INSERT INTO ".SQL_LEAD_QUOTE_FILTERS_TABLE." (site_id,skip_rule,log_id,details) VALUES ('$siteID','$skipRule','$logID','$details')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 09-05-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
   return $this->strERR;
}


}// end COutbound class

?>

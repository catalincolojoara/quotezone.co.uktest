<?php
/*****************************************************************************/
/*                                                                           */
/*  CSetOfflineLeadsModule class interface                                   */
/*                                                                           */
/*  (C) 2011 Furtuna C. Alexandru (alex@acrux.biz)                           */
/*                                                                           */
/*****************************************************************************/

define("Status_INCLUDED", "1");
define("SQL_SET_OFFLINE_SITES", "set_offline_sites");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";
//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CSetOfflineLeadsModule
//
// [DESCRIPTION]:  CEmailExclusion class interface
//
// [FUNCTIONS]:      true   | false AssertAddOfflineLeads
// 
//                   Close()
//                   GetError()
//                   ShowError()
//
// [CREATED BY]:   Furtuna C. Alexandru (alex@acrux.biz) 2011-02-01
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CSetOfflineLeadsModule
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last Status error string

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CSetOfflineLeadsModule
   //
   // [DESCRIPTION]:   Default class constructor. Initialization goes here.
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Furtuna C. Alexandru (alex@acrux.biz) 2011-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CSetOfflineLeadsModule($dbh=0)
   {
      if($dbh)
      {
         $this->dbh = $dbh;
         $this->closeDB = false;
      }
      else
      {
         // default configuration
         $this->dbh = new CMySQL();

         if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
         {
         $this->strERR = $this->dbh->GetError();
         return;
         }

         $this->closeDB = true;
      }

      $this->strERR  = "";
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AssertAddOfflineLeads
   //
   // [DESCRIPTION]:   Checks fields for the AddOfflineLeads.
   //
   // [PARAMETERS]:    $qzQuoteType=0,$siteID=0,$startTime='',$startDate='',$endTime='',$endDate='',$message=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Furtuna C. Alexandru (alex@acrux.biz) 2011-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AssertAddOfflineLeads($qzQuoteType=0,$siteID=0,$startTime='',$startDate='',$endTime='',$endDate='',$message='')
   {

      if(! preg_match("/^\d+$/", $qzQuoteType))
      {      
         $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID");
         return false;
      }

      if(! preg_match("/^\d+$/", $siteID))
      {      
         $this->strERR = GetErrorString("INVALID_SITE_ID");
         return false;
      }

      if(! preg_match("/^\d{2}\:\d{2}\:\d{2}$/",$startTime))
      {      
         $this->strERR = GetErrorString("INVALID_START_TIME");
         return false;
      }

      if(! preg_match("/\d{4}\-\d{2}\-\d{2}/i",$startDate))
      {      
         $this->strERR = GetErrorString("INVALID_START_DATE");
         return false;
      }

      if(! preg_match("/^\d{2}\:\d{2}\:\d{2}$/",$endTime))
      {      
         $this->strERR = GetErrorString("INVALID_END_TIME");
         return false;
      }

      if(! preg_match("/\d{4}\-\d{2}\-\d{2}/i",$endDate))
      {      
         $this->strERR = GetErrorString("INVALID_END_DATE");
         return false;
      }



      return true;


   }//end function AssertAddOfflineLeads

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AddOfflineLeads
   //
   // [DESCRIPTION]:   Adds the details to the database
   //
   // [PARAMETERS]:    $qzQuoteType=0,$siteID=0,$startTime='',$startDate='',$endTime='',$endDate='',$message=''
   //
   // [RETURN VALUE]:  Id or 0 in case of failure
   //
   // [CREATED BY]:    Furtuna C. Alexandru (alex@acrux.biz) 2011-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AddOfflineLeads($qzQuoteType=0,$siteID=0,$startTime='',$startDate='',$endTime='',$endDate='',$message='',$offlineFlag=0)
   {
      if(! $this->AssertAddOfflineLeads($qzQuoteType,$siteID,$startTime,$startDate,$endTime,$endDate,$message))
         return false;

      $nowTime = date("H:i:s");
      $nowDate = date("Y-m-d");

      $sqlCMD = "INSERT INTO ".SQL_SET_OFFLINE_SITES." (`qz_quote_type_id`,`site_id`,`time_start`,`date_start`,`time_end`,`date_end`,`message`, `offline_flag`, `time_added`, `date_added`) VALUES ('$qzQuoteType','$siteID','$startTime','$startDate','$endTime','$endDate','$message', '$offlineFlag', '$nowTime', '$nowDate')";

      //print "SQL IS  : $sqlCMD<br>";
      
      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }

      $sqlCMD = "SELECT LAST_INSERT_ID() AS id";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }

      return $this->dbh->GetFieldValue("id");

   }//end function AddOfflineLeads

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: UpdateOfflineLeads
   //
   // [DESCRIPTION]:   Updateds the details to the database
   //
   // [PARAMETERS]:    $qzQuoteType=0,$siteID=0,$startTime='',$startDate='',$endTime='',$endDate='',$message='',$id
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Furtuna C. Alexandru (alex@acrux.biz) 2011-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function UpdateOfflineLeads($qzQuoteType=0,$siteID=0,$startTime='',$startDate='',$endTime='',$endDate='',$message='', $offlineFlag=0, $id=0)
   {
      if(! $this->AssertAddOfflineLeads($qzQuoteType,$siteID,$startTime,$startDate,$endTime,$endDate,$message))
         return false;

      if(! preg_match("/^\d+$/", $id))
      {
         $this->strERR = GetErrorString("INVALID_ID_FIELD");
         return false;
      }

      $nowTime = date("H:i:s");
      $nowDate = date("Y-m-d");

      $sqlCMD = "UPDATE ".SQL_SET_OFFLINE_SITES." SET  qz_quote_type_id = '$qzQuoteType' ,site_id = '$siteID',time_start = '$startTime',date_start = '$startDate',time_end = '$endTime',date_end = '$endDate' ,message = '$message', offline_flag = '$offlineFlag' WHERE id='$id'";

      //print "SQL IS  : $sqlCMD<br>";
      
      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }
      
      return true;

   }//end function AddOfflineLeads



   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetAllSetOfflineLeads
   //
   // [DESCRIPTION]:   Returns all the data in the table
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  array | false
   //
   // [CREATED BY]:    Furtuna C. Alexandru (alex@acrux.biz) 2011-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetAllSetOfflineLeads()
   {

      $sqlCmd = "SELECT sol.*,s.name as site_name,qzqt.name as qzq_name FROM ".SQL_SET_OFFLINE_SITES." sol, sites s, qzquote_types qzqt WHERE sol.site_id = s.id AND sol.qz_quote_type_id = qzqt.id ORDER BY sol.date_start";

      if(! $this->dbh->Exec($sqlCmd))
      {
      $this->strERR = $this->dbh->GetError();
      return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("SET_OFFLINE_NOT_FOUND");
         return false;
      }

      $cnt = 0;
      while($this->dbh->MoveNext())
      {
         $cnt++;

         $arrayResult[$cnt]["id"]               = $this->dbh->GetFieldValue("id");
         $arrayResult[$cnt]["qz_quote_type_id"] = $this->dbh->GetFieldValue("qz_quote_type_id");
         $arrayResult[$cnt]["site_id"]          = $this->dbh->GetFieldValue("site_id");
         $arrayResult[$cnt]["qz_quote_type"]    = $this->dbh->GetFieldValue("qzq_name");
         $arrayResult[$cnt]["site"]             = $this->dbh->GetFieldValue("site_name");
         $arrayResult[$cnt]["time_start"]       = $this->dbh->GetFieldValue("time_start");
         $arrayResult[$cnt]["date_start"]       = $this->dbh->GetFieldValue("date_start");
         $arrayResult[$cnt]["time_end"]         = $this->dbh->GetFieldValue("time_end");
         $arrayResult[$cnt]["date_end"]         = $this->dbh->GetFieldValue("date_end");
         $arrayResult[$cnt]["message"]          = $this->dbh->GetFieldValue("message");
         $arrayResult[$cnt]["offline_flag"]     = $this->dbh->GetFieldValue("offline_flag");
         $arrayResult[$cnt]["time_added"]       = $this->dbh->GetFieldValue("time_added");
         $arrayResult[$cnt]["date_added"]       = $this->dbh->GetFieldValue("date_added");

      }

      return $arrayResult;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetAllSetOfflineLeadsByQuoteTypeIdAndSiteName
   //
   // [DESCRIPTION]:   Returns all the data in the table for a company
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  array | false
   //
   // [CREATED BY]:    Furtuna C. Alexandru (alex@acrux.biz) 2011-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetAllSetOfflineLeadsByQuoteTypeIdAndSiteName($quoteTypeId=0,$siteId=0)
   {

      if(! preg_match("/^\d+$/", $quoteTypeId))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
         return false;
      }

      if(! preg_match("/^\d+$/", $siteId))
      {
         $this->strERR = GetErrorString("INVALID_SITE_ID_FIELD");
         return false;
      }

      $sqlCmd = "SELECT sol.*,s.name as site_name,qzqt.name as qzq_name FROM ".SQL_SET_OFFLINE_SITES." sol, sites s, qzquote_types qzqt WHERE sol.site_id = s.id AND sol.qz_quote_type_id = qzqt.id AND sol.site_id='$siteId' AND sol.qz_quote_type_id = '$quoteTypeId' ORDER BY sol.date_start";

      if(! $this->dbh->Exec($sqlCmd))
      {
      $this->strERR = $this->dbh->GetError();
      return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("SET_OFFLINE_NOT_FOUND");
         return false;
      }

      $cnt = 0;
      while($this->dbh->MoveNext())
      {
         $cnt++;

         $arrayResult[$cnt]["id"]               = $this->dbh->GetFieldValue("id");
         $arrayResult[$cnt]["qz_quote_type_id"] = $this->dbh->GetFieldValue("qz_quote_type_id");
         $arrayResult[$cnt]["site_id"]          = $this->dbh->GetFieldValue("site_id");
         $arrayResult[$cnt]["qz_quote_type"]    = $this->dbh->GetFieldValue("qzq_name");
         $arrayResult[$cnt]["site"]             = $this->dbh->GetFieldValue("site_name");
         $arrayResult[$cnt]["time_start"]       = $this->dbh->GetFieldValue("time_start");
         $arrayResult[$cnt]["date_start"]       = $this->dbh->GetFieldValue("date_start");
         $arrayResult[$cnt]["time_end"]         = $this->dbh->GetFieldValue("time_end");
         $arrayResult[$cnt]["date_end"]         = $this->dbh->GetFieldValue("date_end");
         $arrayResult[$cnt]["message"]          = $this->dbh->GetFieldValue("message");
         $arrayResult[$cnt]["offline_flag"]     = $this->dbh->GetFieldValue("offline_flag");
         $arrayResult[$cnt]["time_added"]       = $this->dbh->GetFieldValue("time_added");
         $arrayResult[$cnt]["date_added"]       = $this->dbh->GetFieldValue("date_added");
      }

      return $arrayResult;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetAllSetOfflineLeadsByQuoteTypeIdAndSiteName
   //
   // [DESCRIPTION]:   Returns all the data in the table for a company
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  array | false
   //
   // [CREATED BY]:    Furtuna C. Alexandru (alex@acrux.biz) 2011-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetAllSetOfflineLeadsByQuoteTypeIdAndSiteNameAndDates($quoteTypeId = 0,$siteId = 0,$startDate= "",$endDate = "")
   {
//       if(! preg_match("/^\d+$/", $quoteTypeId))
//       {
//          $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
//          return false;
//       }

//       if(! preg_match("/^\d+$/", $siteId))
//       {
//          $this->strERR = GetErrorString("INVALID_SITE_ID_FIELD");
//          return false;
//       }

//       if($startDate == "")
//       {
//          $this->strERR = GetErrorString("INVALID_START_DATE_FIELD");
//          return false;
//       }
// 
//       if($endDate == "")
//       {
//          $this->strERR = GetErrorString("INVALID_END_DATE_FIELD");
//          return false;
//       }



      $sqlCmd = "SELECT sol.*,s.name as site_name,qzqt.name as qzq_name FROM ".SQL_SET_OFFLINE_SITES." sol, sites s, qzquote_types qzqt WHERE sol.site_id = s.id AND sol.qz_quote_type_id = qzqt.id ";

      //quote_type_id sql cmd
      if(preg_match("/^\d+$/", $quoteTypeId))
         $sqlCmd .= "AND sol.qz_quote_type_id = '$quoteTypeId'";

      //site_id SQL CMD
      if(preg_match("/^\d+$/", $siteId))
         $sqlCmd .= "AND sol.site_id='$siteId'";  

      if($startDate != "")
         $sqlCmd .= " AND sol.date_start >= '$startDate'";
      
      if($endDate != "")
         $sqlCmd .= " AND sol.date_end <= '$endDate'";

      $sqlCmd .= " ORDER BY sol.date_start";

      if(! $this->dbh->Exec($sqlCmd))
      {
      $this->strERR = $this->dbh->GetError();
      return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("SET_OFFLINE_NOT_FOUND");
         return false;
      }

      $cnt = 0;
      while($this->dbh->MoveNext())
      {
         $cnt++;

         $arrayResult[$cnt]["id"]               = $this->dbh->GetFieldValue("id");
         $arrayResult[$cnt]["qz_quote_type_id"] = $this->dbh->GetFieldValue("qz_quote_type_id");
         $arrayResult[$cnt]["site_id"]          = $this->dbh->GetFieldValue("site_id");
         $arrayResult[$cnt]["qz_quote_type"]    = $this->dbh->GetFieldValue("qzq_name");
         $arrayResult[$cnt]["site"]             = $this->dbh->GetFieldValue("site_name");
         $arrayResult[$cnt]["time_start"]       = $this->dbh->GetFieldValue("time_start");
         $arrayResult[$cnt]["date_start"]       = $this->dbh->GetFieldValue("date_start");
         $arrayResult[$cnt]["time_end"]         = $this->dbh->GetFieldValue("time_end");
         $arrayResult[$cnt]["date_end"]         = $this->dbh->GetFieldValue("date_end");
         $arrayResult[$cnt]["message"]          = $this->dbh->GetFieldValue("message");
         $arrayResult[$cnt]["offline_flag"]     = $this->dbh->GetFieldValue("offline_flag");
         $arrayResult[$cnt]["time_added"]       = $this->dbh->GetFieldValue("time_added");
         $arrayResult[$cnt]["date_added"]       = $this->dbh->GetFieldValue("date_added");
      }

      return $arrayResult;
   }


   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetAllSetOfflineLeadsByQuoteTypeId
   //
   // [DESCRIPTION]:   Returns all the data in the table for a quote type
   //
   // [PARAMETERS]:    $quoteTypeId
   //
   // [RETURN VALUE]:  array | false
   //
   // [CREATED BY]:    Furtuna C. Alexandru (alex@acrux.biz) 2011-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetAllSetOfflineLeadsByQuoteTypeId($quoteTypeId=0)
   {

      if(! preg_match("/^\d+$/", $quoteTypeId))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
         return false;
      }

      $sqlCmd = "SELECT sol.*,s.name as site_name,qzqt.name as qzq_name FROM ".SQL_SET_OFFLINE_SITES." sol, sites s, qzquote_types qzqt WHERE sol.site_id = s.id AND sol.qz_quote_type_id = qzqt.id AND sol.qz_quote_type_id = '$quoteTypeId' ORDER BY sol.date_start, sol.time_start";

      if(! $this->dbh->Exec($sqlCmd))
      {
      $this->strERR = $this->dbh->GetError();
      return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("SET_OFFLINE_NOT_FOUND");
         return false;
      }

      $cnt = 0;
      while($this->dbh->MoveNext())
      {
         $cnt++;

         $arrayResult[$cnt]["id"]               = $this->dbh->GetFieldValue("id");
         $arrayResult[$cnt]["qz_quote_type_id"] = $this->dbh->GetFieldValue("qz_quote_type_id");
         $arrayResult[$cnt]["site_id"]          = $this->dbh->GetFieldValue("site_id");
         $arrayResult[$cnt]["qz_quote_type"]    = $this->dbh->GetFieldValue("qzq_name");
         $arrayResult[$cnt]["site"]             = $this->dbh->GetFieldValue("site_name");
         $arrayResult[$cnt]["time_start"]       = $this->dbh->GetFieldValue("time_start");
         $arrayResult[$cnt]["date_start"]       = $this->dbh->GetFieldValue("date_start");
         $arrayResult[$cnt]["time_end"]         = $this->dbh->GetFieldValue("time_end");
         $arrayResult[$cnt]["date_end"]         = $this->dbh->GetFieldValue("date_end");
         $arrayResult[$cnt]["message"]          = $this->dbh->GetFieldValue("message");
         $arrayResult[$cnt]["offline_flag"]     = $this->dbh->GetFieldValue("offline_flag");
         $arrayResult[$cnt]["time_added"]       = $this->dbh->GetFieldValue("time_added");
         $arrayResult[$cnt]["date_added"]       = $this->dbh->GetFieldValue("date_added");
      }

      return $arrayResult;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CheckSetOfflineLeadsStatusBySiteId
   //
   // [DESCRIPTION]:   Checks offline status by site id
   //
   // [PARAMETERS]:    $siteId
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Furtuna C. Alexandru (alex@acrux.biz) 2011-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CheckSetOfflineLeadsStatusBySiteId($siteId=0)
   {
      if(! preg_match("/^\d+$/", $siteId))
      {
         $this->strERR = GetErrorString("INVALID_SITE_ID_FIELD");
         return false;
      }

      $sqlCmd = "SELECT * FROM ".SQL_SET_OFFLINE_SITES." WHERE site_id = $siteId";

      if(! $this->dbh->Exec($sqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("SET_OFFLINE_NOT_FOUND");
         return false;
      }

      $cnt = 0;
      while($this->dbh->MoveNext())
      {
         $cnt++;

         $arrayResult[$cnt]["id"]               = $this->dbh->GetFieldValue("id");
         $arrayResult[$cnt]["qz_quote_type_id"] = $this->dbh->GetFieldValue("qz_quote_type_id");
         $arrayResult[$cnt]["site_id"]          = $this->dbh->GetFieldValue("site_id");
         $arrayResult[$cnt]["time_start"]       = $this->dbh->GetFieldValue("time_start");
         $arrayResult[$cnt]["date_start"]       = $this->dbh->GetFieldValue("date_start");
         $arrayResult[$cnt]["time_end"]         = $this->dbh->GetFieldValue("time_end");
         $arrayResult[$cnt]["date_end"]         = $this->dbh->GetFieldValue("date_end");
         $arrayResult[$cnt]["message"]          = $this->dbh->GetFieldValue("message");
         $arrayResult[$cnt]["offline_flag"]     = $this->dbh->GetFieldValue("offline_flag");
         $arrayResult[$cnt]["time_added"]       = $this->dbh->GetFieldValue("time_added");
         $arrayResult[$cnt]["date_added"]       = $this->dbh->GetFieldValue("date_added");
      }


      $mktime = mktime();

      foreach($arrayResult as $cntVal => $detailsArray)
      {
         $startTime = "";
         $startDate = "";
         $endTime   = "";
         $endDate   = "";

         $startTime = $detailsArray["time_start"];
         $startDate = $detailsArray["date_start"];
         $endTime   = $detailsArray["time_end"];
         $endDate   = $detailsArray["date_end"];

         $startTimeArray = explode(":",$startTime);
         $startDateArray = explode("-",$startDate);
         $endTimeArray   = explode(":",$endTime);
         $endDateArray   = explode("-",$endDate);

         $startMkTime = mktime($startTimeArray[0],$startTimeArray[1],$startTimeArray[2],$startDateArray[1],$startDateArray[2],$startDateArray[0]);
         $endMkTime   = mktime($endTimeArray[0],$endTimeArray[1],$endTimeArray[2],$endDateArray[1],$endDateArray[2],$endDateArray[0]);

         if($detailsArray["offline_flag"] == 1)
         {
            if(($mktime <= $startMkTime) OR ($mktime >= $endMkTime))
               return true;
         }
         elseif($detailsArray["offline_flag"] == 0)
         {
            if(($mktime >= $startMkTime) AND ($mktime <= $endMkTime))
               return true;
            else
               continue;
         }

      }//end foreach

      return false;

   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetSetOfflineLeadsByID
   //
   // [DESCRIPTION]:   Returns the data in the table by id
   //
   // [PARAMETERS]:    $setOfflineLeadID
   //
   // [RETURN VALUE]:  array | false
   //
   // [CREATED BY]:    Furtuna C. Alexandru (alex@acrux.biz) 2011-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetSetOfflineLeadsByID($setOfflineLeadID=0)
   {

      if(! preg_match("/^\d+$/", $setOfflineLeadID))
      {
         $this->strERR = GetErrorString("INVALID_ID_FIELD");
         return false;
      }

      $sqlCmd = "SELECT sol.*,s.name as site_name,qzqt.name as qzq_name FROM ".SQL_SET_OFFLINE_SITES." sol, sites s, qzquote_types qzqt WHERE sol.site_id = s.id AND sol.qz_quote_type_id = qzqt.id AND sol.id='$setOfflineLeadID'";

      if(! $this->dbh->Exec($sqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("SET_OFFLINE_NOT_FOUND");
         return false;
      }

      $cnt = 0;
      while($this->dbh->MoveNext())
      {
         $cnt++;

         $arrayResult[$cnt]["id"]               = $this->dbh->GetFieldValue("id");
         $arrayResult[$cnt]["qz_quote_type_id"] = $this->dbh->GetFieldValue("qz_quote_type_id");
         $arrayResult[$cnt]["site_id"]          = $this->dbh->GetFieldValue("site_id");
         $arrayResult[$cnt]["qz_quote_type"]    = $this->dbh->GetFieldValue("qzq_name");
         $arrayResult[$cnt]["site"]             = $this->dbh->GetFieldValue("site_name");
         $arrayResult[$cnt]["time_start"]       = $this->dbh->GetFieldValue("time_start");
         $arrayResult[$cnt]["date_start"]       = $this->dbh->GetFieldValue("date_start");
         $arrayResult[$cnt]["time_end"]         = $this->dbh->GetFieldValue("time_end");
         $arrayResult[$cnt]["date_end"]         = $this->dbh->GetFieldValue("date_end");
         $arrayResult[$cnt]["message"]          = $this->dbh->GetFieldValue("message");
         $arrayResult[$cnt]["offline_flag"]     = $this->dbh->GetFieldValue("offline_flag");
         $arrayResult[$cnt]["time_added"]       = $this->dbh->GetFieldValue("time_added");
         $arrayResult[$cnt]["date_added"]       = $this->dbh->GetFieldValue("date_added");

      }

      return $arrayResult;
   }


   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: DeleteSetOfflineLead
   //
   // [DESCRIPTION]:   Delets the data from the table by id
   //
   // [PARAMETERS]:    $setOfflineLeadID
   //
   // [RETURN VALUE]:  array | false
   //
   // [CREATED BY]:    Furtuna C. Alexandru (alex@acrux.biz) 2011-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function DeleteSetOfflineLead($setOfflineLeadID=0)
   {

      if(! preg_match("/^\d+$/", $setOfflineLeadID))
      {
         $this->strERR = GetErrorString("INVALID_ID_FIELD");
         return false;
      }

      $sqlCmd = "DELETE FROM ".SQL_SET_OFFLINE_SITES." WHERE id = '$setOfflineLeadID'";

      if(! $this->dbh->Exec($sqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      return true;
   }

}//end Class CSetOfflineLeadsModule



/*
SQL_SET_OFFLINE_SITES
CREATE TABLE `set_offline_sites` (
   `id` bigint(20) unsigned NOT NULL auto_increment,
   `qz_quote_type_id` bigint(20) unsigned NOT NULL,
   `site_id` bigint(20) unsigned NOT NULL,
   `time_start` time default '00:00:00',
   `date_start` date default '0000-00-00',
   `time_end` time default '00:00:00',
   `date_end` date default '0000-00-00',
   `message` varchar(255) NOT NULL default '',
   `time_added` time default '00:00:00',
   `date_added` date default '0000-00-00',
   PRIMARY KEY  (`id`)
) ENGINE=InnoDB
*/




?>
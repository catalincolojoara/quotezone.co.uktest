<?php

session_start();

/*****************************************************************************/
/*                                                                           */
/*  CCompanyConfig class interface                                           */
/*                                                                           */
/*  (C) 2012 Alexandru Furtuna (alex@acrux.biz)                              */
/*                                                                           */
/*****************************************************************************/
include_once "globals.inc";
include_once "MySQL.php";
include_once "Site.php";
include_once "SetOfflineLeadsModule.php";
include_once "CompanyLimits.php";
include_once "CompanyFilters.php";
include_once "QuoteStatus.php";
include_once "LeadQuoteFilters.php";


//////////////////////////////////////////////////////////////////////////////PE
//
// [CLASS NAME]:   CCompanyConfig
//
// [DESCRIPTION]:  CCompanyConfig class interface
//
// [FUNCTIONS]:    void Initialise();
//                 bolean CheckSiteOnlineStatus($siteID)
//
//
// [CREATED BY]:   Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CCompanyConfig
{
   var $objMySql;            // CMySQL class object
   var $objSite;             // CSite class object
   var $objSetOfflineLeads;  // CSetOfflineLeadsModule class object
   var $objLimits;           // CCompanyLimits class object
   var $objFilters;          // CCompanyLimits class object
   var $strERR;              // last Log error string
   var $closeDB;             // close db variable
   var $session;             // session container
   var $objLeadQuoteFilters; // CLeadQuoteFilters object
   var $objQuoteStatus;      // CQuoteStatus object


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CCompanyConfig
//
// [DESCRIPTION]:   Class constructor
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CCompanyConfig($dbh)
{
   if($dbh)
   {
      $this->objMySql  = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->objMySql = new CMySQL();

      if(! $this->objMySql->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->objSite                     = new CSite($dbh);
   $this->objSetOfflineLeads          = new CSetOfflineLeadsModule($dbh);
   $this->objLimits                   = new CCompanyLimits($dbh);
   $this->objFilters                  = new CCompanyFilters();
   $this->strERR                      = "";
   $this->session                     = array();

   $this->objLeadQuoteFilters         = new CLeadQuoteFilters($dbh);
   $this->objQuoteStatus              = new CQuoteStatus($dbh);


}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckCompanyQuoteDetails
//
// [DESCRIPTION]:   Checks all the company quote details - online limits , stauts ...
//
// [PARAMETERS]:    $siteID,$companyDetailsArray
//
// [RETURN VALUE]:  true if ok to send details , false if no data should be sent
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckCompanyQuoteDetails($siteID=0,$companyDetailsArray=array(),$session=array())
{
   global $_SESSION;
   $this->session = $session;

   //check online status of company
   if(! $this->CheckCompanyOnlineStatus($siteID))
   {
      //we have a key that we set to ignore the off status so we can test this
      if($companyDetailsArray['plugin_details']['ignore_off_status'])
      {
         if($this->session["USER_IP"] != "86.125.114.56" && $this->session["USER_IP"] != "37.153.72.90")
         {
            unset($_SESSION["REQUEST_SITES"][$siteID]);
            return false;
         }
      }
      else
      {
         unset($_SESSION["REQUEST_SITES"][$siteID]);
         return false;
      }
   }

   // if($this->session["USER_IP"] != "86.125.114.56" && $this->session["USER_IP"] != "37.153.72.90")
   // {
   //    if($companyDetailsArray['plugin_details']['offline_from_plugin'])
   //    {
   //       unset($_SESSION["REQUEST_SITES"][$siteID]);
   //       return false;
   //    }
   // }

   //check company limits - this is checked only in case the IP is not from Acrux
   if($this->session["USER_IP"] != "86.125.114.56" && $this->session["USER_IP"] != "37.153.72.90")
   {
      if(is_array($companyDetailsArray["plugin_details"]["limit"]))
      {
         foreach($companyDetailsArray["plugin_details"]["limit"] as $type => $limitNo)
         {
            if(! $this->CheckCompanyLimits($siteID,$limitNo,$type))
            {
               unset($_SESSION["REQUEST_SITES"][$siteID]);
               return false;
            }
         }
      }//end IF - end check company limits
   }

   $noOfFilters = count($companyDetailsArray['filter_details']);
	//check if we have bespoke filters and increment $noOfFilters and set a flag
   $bespokeFlag = false;
   if(count($companyDetailsArray['bespoke_filters']) > 0)
   {
      $noOfFilters++;
      $bespokeFlag = true;
   }
   $i = 0;
   
   if($noOfFilters > 0)
   {
      //if we have bespoke then run the script
      if($bespokeFlag)
      {
         $bespokeFilterDetails = $this->CheckBespokeCompanyFilters($companyDetailsArray['bespoke_filters']);
		 if(is_array($bespokeFilterDetails))
		 {
			foreach($bespokeFilterDetails as $number => $detailsOfBespoke)
				$companyDetailsArray['filter_details'][] = $detailsOfBespoke;
		 }
      }
      if($filteredKeyToAddToDB = $this->CheckCompanyFilters($siteID, $companyDetailsArray['filter_details'],$this->session))
      {
         //this is success
         if($filteredKeyToAddToDB == 1)
            return true;

         //set the filter details in the database
         $_SESSION["FILTER_DETAILS"][$siteID] = $filteredKeyToAddToDB;

         return false;
      }
      $i++;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckCompanyOnlineStatus
//
// [DESCRIPTION]:   Add filter details to the database
//
// [PARAMETERS]:    $siteID=0,$session=array(),$filterDetails=''
//
// [RETURN VALUE]:  true
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddCompanySkipDetails($siteID=0,$session=array(),$filterDetails='')
{
   if($session)
      $this->session = $session;

   $quoteType    = 'NORMAL';
   if(! empty($this->session['DEBUG_SITES']))
      $quoteType    = 'DEBUG';

   if(! $quoteStatusID = $this->objQuoteStatus->AddQuoteStatus($this->session['_QZ_QUOTE_DETAILS_']['qz_log_id'], $siteID,'SKIPPED',$quoteType))
   {
      $this->objQuoteStatus->ShowError();
      return false;
   }



   list($skipRule, $details) = explode("--",$filterDetails);

   if(! $leadQuoteFilterID = $this->objLeadQuoteFilters->AddLeadQuoteFilters($siteID, $this->session['_QZ_QUOTE_DETAILS_']['qz_log_id'], $skipRule, $details))
   {
      print $this->objLeadQuoteFilters->GetError();
      return false;
   }

   return true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckCompanyOnlineStatus
//
// [DESCRIPTION]:   Checks the site status from the database
//
// [PARAMETERS]:    $siteID
//
// [RETURN VALUE]:  Site status ON (true) or OFF (false)
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckCompanyOnlineStatus($siteID=0)
{

   $siteIsON = false;

   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = "INVALID_SITE_ID";
      return false;
   }

   //we check that the site is ON OD OFF in the database
   if(! $siteStatusDetails = $this->objSite->GetSite($siteID))
   {
      $this->strERR = $this->objSite->GetError();
      $siteIsON = false;
   }

   if($siteStatusDetails['status'] == 'OFF')
      $siteIsON = false;
   else
      $siteIsON = true;

   //if the site is ON , we chack if it is set OFF by a set interval
   if($siteIsON)
   {
      if($this->objSetOfflineLeads->CheckSetOfflineLeadsStatusBySiteId($siteID))
         $siteIsON = false;
      else
         $siteIsON = true;
   }
   else
      $siteIsON = false;


   if(! $siteIsON)
      return false;
   else
      return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckCompanyLimits
//
// [DESCRIPTION]:   Checks the limits for this sites (no of quotes per interval)
//
// [PARAMETERS]:    $siteID,$limit.$interval
//
// [RETURN VALUE]:  true/false (true if the limit is not reached / false if limit is reached)
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckCompanyLimits($siteID=0,$limit=0,$interval="")
{

   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = "INVALID_SITE_ID";
      return false;
   }

   if(! preg_match("/^\d+$/", $limit))
   {
      $this->strERR = "INVALID_LIMIT_SENT";
      return false;
   }

   if(! $interval)
   {
      $this->strERR = "INVALID_INTERVAL_SENT";
      return false;
   }

   $daysOfWeekArray = array(
                        "mon",
                        "tue",
                        "wed",
                        "thu",
                        "fri",
                        "sat",
                        "sun",
                     );

   if(in_array($interval,$daysOfWeekArray))
   {
      $todayDay = date('D');
      $todayDay = strtolower($todayDay);

      if($todayDay == $interval)
      {
         $noOfLeadsPerInterval = $this->objLimits->GetSiteLeadsPerPeriod($siteID,$interval);

         if(! preg_match("/^\d+$/", $noOfLeadsPerInterval))
         {
            $this->strERR = "ERROR_RETRIVING_LIMITS";
            return false;
         }
         else
         {
            if($noOfLeadsPerInterval >= $limit)
               return false;
            else
               return true;
         }
      }
   }
   else
   {
      $noOfLeadsPerInterval = $this->objLimits->GetSiteLeadsPerPeriod($siteID,$interval);

      if(! preg_match("/^\d+$/", $noOfLeadsPerInterval))
      {
         $this->strERR = "ERROR_RETRIVING_LIMITS";
         return false;
      }
      else
      {
         if($noOfLeadsPerInterval >= $limit)
            return false;
         else
            return true;
      }
   }

   return true;

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckCompanyFilters
//
// [DESCRIPTION]:   Checks the company filters
//
// [PARAMETERS]:    $siteID=0,$fieldName="",$checkType="",$valuesType=""
//
// [RETURN VALUE]:  true/false (true if filtered , false if NOT filtered)
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckCompanyFilters($siteID = 0,$companyFilterDetailsArray = array())
{
   $filterDetailsKeys = "";

   foreach($companyFilterDetailsArray as $cnt => $filterDetails)
   {
      foreach($filterDetails as $fieldName => $fieldNameDetailsArray)
      {
         foreach($fieldNameDetailsArray as $allFilterDetails => $filterValue)
         {
            $keyDetailsArray = explode(";",$allFilterDetails);

            $acceptance = $keyDetailsArray[0];
            $fieldType  = $keyDetailsArray[1];
            $valueType  = $keyDetailsArray[2];
            $filterKey  = $keyDetailsArray[3];

            if(! $this->objFilters->CheckFilterCondition($fieldName,$acceptance,$fieldType,$valueType,$filterKey,$filterValue,$this->session))
            {
               $filterDetailsKeysArray[] = $fieldName."-".$filterKey."[".$this->session["_YourDetails_"][$fieldName]."]";
            }
         }
      }
   }

   if($filterDetailsKeysArray)
      return $filterDetailsKeysArray;
   else
      return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckBespokeCompanyFilters
//
// [DESCRIPTION]:   Checks the company filters
//
// [PARAMETERS]:    $siteID=0,$fieldName="",$checkType="",$valuesType=""
//
// [RETURN VALUE]:  true/false (true if filtered , false if NOT filtered)
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckBespokeCompanyFilters($companyFilterDetailsArray = array())
{
   foreach($companyFilterDetailsArray as $bespokeNr => $bespokeFilter)
   {
         eval("\$bespokeFilterDetails = $bespokeFilter;");
		 $bespokeFilterDetaiksArr = $_companyDetails['filter_details'];
   }
   
   return $bespokeFilterDetaiksArr;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: skipSite
//
// [DESCRIPTION]:   Add the skipSite details to the database
//
// [PARAMETERS]:    $siteID=0,$fieldName="",$checkType="",$valuesType=""
//
// [RETURN VALUE]:  true/false (true if filtered , false if NOT filtered)
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function skipSite($siteID, $personalizedError)
{
   $quoteType = 'NORMAL';

   if (! empty($this->session['DEBUG_SITES']))
      $quoteType = 'DEBUG';

   if(! $this->session["LOCKED"])
   {
      if(! $quoteStatusID = $this->objQuoteStatus->AddQuoteStatus($this->session['_QZ_QUOTE_DETAILS_']['qz_log_id'], $siteID, 'SKIPPED', $quoteType))
      {
         $this->objQuoteStatus->ShowError();
         return false;
      }

      list($skipRule, $details) = explode("--", $filter);

      if (! $leadQuoteFilterID = $this->objLeadQuoteFilters->AddLeadQuoteFilters($siteID, $this->session['_QZ_QUOTE_DETAILS_']['qz_log_id'], $skipRule, $details))
      {
         print $this->objLeadQuoteFilters->GetError();
         return false;
      }
   }

   return true;

}



//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    print $this->strERR;
}


}//end Class


?>

<?php
/**
 * Class Logger
 *
 * This class is used to log messages from application.
 * The messages are stored by default in file 'logger.log' in /tmp folder.
 * Use: include the file from common/modules folder then log:
 *      Logger::LogMessageInFile('message');
 *      Logger::LogMessageToMail('message','mail_address_where_to_send','subject');
 * There is no need to instantiate the class.
 *
 * @author Ando Ciupav Cristian
 * @copyright SC Acrux Software SRL, 2010, Timisoara
 * @version 1.0
 * @package logger
 */


interface iLogger
{
   static public function LogMessageInFile($message = '');
   static public function LogMessageToMail($message = '', $mailAddress = '', $subject = '');
   static public function FormatXML($xml);
}

class Logger implements iLogger
{
   private $filename;
   private $fileHandler;
   static private $handler = null;

   public function __construct()
   {
      $this->filename = "/tmp/logger.log";
      $this->OpenLogFile();
   }

   public function __destruct()
   {
      if($this->fileHandler)
         fclose($this->fileHandler);
   }

   public function __toString()
   {
      return "<br><b>Class used to log errors from application</b><br>";
   }

   static public function LogMessageInFile($message = '')
   {
      if (self::$handler == NULL)
      {
         self::$handler = new Logger();
         self::$handler->WriteToFile($message);
         self::$handler = null;
      }
      else
      {
         self::$handler->WriteToFile($message);
      }
   }

   static public function LogMessageToMail($message = '', $mailAddress = '', $subject = '')
   {
      if (self::$handler == NULL)
      {
         self::$handler = new Logger();
         self::$handler->WriteToMail($message, $mailAddress, $subject);
         self::$handler = null;
      }
      else
      {
         self::$handler->WriteToMail($message, $mailAddress, $subject);
      }
   }

   static public function FormatXML($xml)
   {
      if (self::$handler == NULL)
      {
         self::$handler = new Logger();
         return self::$handler->ReturnFormatedXML($xml);
         self::$handler = null;
      }
      else
      {
         return self::$handler->ReturnFormatedXML($xml);
      }
   }

   /**
    * Function WriteToFile()
    *
    * This function will write to the log file the message throw by user
    *
    * @param unknown_type $message
    * @return unknown
    */
   private function WriteToFile($message = '')
   {
      if (trim($message) == '')
         return false;
      if (!$this->fileHandler)
         return false;
      $currentDate = date("Y.m.d - H:i:s");
      $temp = "\n**************************************************************************************\n";
      $temp .= "--------------- Message write by Logger class at $currentDate ---------------\n";
      $temp .= "**************************************************************************************\n";
      $temp .= $message;
      $temp .= "\n**************************************************************************************\n\n";
      fwrite($this->fileHandler, $temp);
      return true;
   }

   private function WriteToMail($message = '', $mailAddress = '', $subject = '')
   {
      if (trim($mailAddress) == "")
         return false;
      $headers  = 'MIME-Version: 1.0' . "\r\n";
      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      $headers .= "From: <debug@quotezone.co.uk>\r\n";
      $headers .= "X-Sender: <debug@quotezone.co.uk>\r\n";
      $headers .= "X-Mailer: PHP\r\n";
      $headers .= "X-Priority: 1\r\n";
      $headers .= "Return-Path: <debug@quotezone.co.uk>\r\n";

      $currentDate = date("Y.m.d - H:i:s");

      if(trim($subject) == "")
         $subject = "DEBUG MAIL created by Logger class - $currentDate";
      else
         $subject .= " - $currentDate";

      $temp = "<br>**************************************************************************************<br>";
      $temp .= "Message sent by Logger class at $currentDate<br>";
      $temp .= "**************************************************************************************<br>";
      $temp .= $message;

      mail($mailAddress,$subject,$temp,$headers);
      return true;
   }

   private function OpenLogFile()
   {
      if(! $this->fileHandler)
      {
         $this->fileHandler = @fopen($this->filename,"a+");
         if(! $this->fileHandler)
            return false;
         else
            return true;
      }
      else
      {
         return false;
      }
   }

   /**
    * Function ReturnFormatedXML()
    *
    * This function will receive as parameter an unformated xml string and return formated xml
    *
    * @param string $contents
    * @return string
    */
   private function ReturnFormatedXML($contents)
   {
       if (!function_exists('xml_parser_create'))
       {
           return "";
       }
       $parser = xml_parser_create('');

       xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
       xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
       xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
       xml_parse_into_struct($parser, trim($contents), $xml_values);
       xml_parser_free($parser);

       $stringXML = '';
       $tabulator = "    ";
       foreach ($xml_values as $value)
       {
       	if ($value['type'] == 'close')
       	{
       		for ($i=1; $i<$value['level']; $i++)
       			$stringXML .= $tabulator;
       		$stringXML .= "</" . $value['tag'] . ">\n";
       	}
       	if ($value['type'] == 'open')
       	{
       		for ($i=1; $i<$value['level']; $i++)
       			$stringXML .= $tabulator;
       		if (array_key_exists('attributes', $value))
       		{
       			$stringXML .= "<" . $value['tag'];
       			foreach ($value['attributes'] as $subkey=>$subvalue)
       			{
       				$stringXML .= " ".$subkey."=\"".$subvalue."\"";
       			}
       			$stringXML .= ">\n";
        		}
       		else
       		{
       			$stringXML .= "<" . $value['tag'] . ">\n";
       		}
       	}
       	elseif ($value['type'] == 'complete')
       	{
       		for ($i=1; $i<$value['level']; $i++)
       			$stringXML .= $tabulator;
       		if (array_key_exists('attributes', $value))
       		{
       			$stringXML .= "<" . $value['tag'];
       			foreach ($value['attributes'] as $subkey=>$subvalue)
       			{
       				$stringXML .= " ".$subkey."=\"".$subvalue."\"";
       			}
       			$stringXML .= ">";
   	    		if (! array_key_exists('value', $value))
   	    			$stringXML .= '';
   	    		else
   	    			$stringXML .= $value['value'];
       			$stringXML .= "</" . $value['tag'] . ">\n";
       		}
       		else
       		{
   	    		$stringXML .= "<" . $value['tag'].">";
   	    		if (! array_key_exists('value', $value))
   	    			$stringXML .= '';
   	    		else
   	    			$stringXML .= $value['value'];
   	    		$stringXML .= "</" . $value['tag'] . ">\n";
       		}
       	}
       }
       return $stringXML;
   }

}

?>
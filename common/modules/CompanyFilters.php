<?php
/*****************************************************************************/
/*                                                                           */
/*  CCompanyFilters class interface                                           */
/*                                                                           */
/*  (C) 2012 Alexandru Furtuna (alex@acrux.biz)                              */
/*                                                                           */
/*****************************************************************************/
include_once "globals.inc";
include_once "MySQL.php";
include_once "YourDetailsElements.php";

//////////////////////////////////////////////////////////////////////////////PE
//
// [CLASS NAME]:   CCompanyFilters
//
// [DESCRIPTION]:  CSkipValidator class interface
//
// [FUNCTIONS]:    void Initialise();
//
// [CREATED BY]:   Alexandru Furtuna (alex@acrux.biz) 2012-04-10
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CCompanyFilters
{
   var $strERR;  // last error string
   var $session; // session container

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CCompanyFilters
//
// [DESCRIPTION]:   Default class Constructor
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CCompanyFilters()
{
   $this->strERR  = "";
   $this->session = array();
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckFilterTypes
//
// [DESCRIPTION]:   Checks what function to call based on the filter type
//
// [PARAMETERS]:    fieldName
//
// [RETURN VALUE]:
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckFilterCondition($fieldName="",$acceptance="",$fieldType="",$valueType="",$filterKey="",$filterValue="",$session=array())
{
   if(! $fieldName) 
   {
      $this->strERR = "INVALID_FILED_NAME";
      return false;
   }

   if(! $acceptance)
   {
      $this->strERR = "INVALID_ACCEPTANCE_CONDITION";
      return false;
   }

   if(! $fieldType)
   {
      $this->strERR = "INVALID_FIELD_CONDITION";
      return false;
   }

   //$valueType; // this can be empty

   if(! $filterKey)
   {
      $this->strERR = "INVALID_FILTER_KEY";
      return false;
   }

   if(! $filterValue)
   {
      $this->strERR = "INVALID_VALUES_TO_FILTER_ACCEPT_KEY";
      return false;
   }

   if(! $session)
   {
      $this->strERR = "INVALID_SESSION_SENT";
      return false;
   }

   //How we decide if if is filtered or not :
   // $acceptance - this is SKP (filtered) OR ACC (accepted)
   // $fieldType  - this is LIST-(array) OR VAL (single value)
   // $valueType  - this is MIN OR MAX OR ALL  - this can be empty in case of array
   // $filterKey  - this is the filter key we add to the database

   //new value added for valueType = CTN - contains 

   //ignore empty fields from filtering - we need to check that the field is in the array as there are filters like postcode_sk  and
   // other bespoke ones that will be seen as empty as they are not in the session we match.
   //TODO - we need too see what filters are accepted or filtered with null values and not include those here

   //print "Values sent to the function are : [$fieldName],[$acceptance],[$fieldType],[$valueType],[$filterKey],[$filterValue]\n<br>";


   //update the details for empty bespoke filters
   if($session["_QZ_QUOTE_DETAILS_"]["system"] == "BOAT" || $session["USER_IP"] == "86.125.114.56")
   {
      //values to exclude from the list ; //oo, EMPTY,''
      $emptyFilterValueArray = array(
         "",
         "00",
         "EMPTY",
      );

      $fieldNameToCheck = $fieldName;

      if(preg_match("/\_sk/isU",$fieldName))
      {
         $fieldNameToCheck = str_replace("_sk","",$fieldName);
      }

      if(array_key_exists($fieldNameToCheck, $session["_YourDetails_"]))
      {

         //check that we dont have an empty filter
         if(trim($session["_YourDetails_"][$fieldNameToCheck]) == "")
         {

            if(is_array($filterValue))
            {
               $isArraySoWeFilter = true;

               foreach ($filterValue as $filKey => $filValue)
               {
                  if (in_array($filValue, $emptyFilterValueArray))
                     $isArraySoWeFilter = false; //in this case in the array the value si empty so we need to filter
               }

               if ($isArraySoWeFilter)
               {
                  return true;
               }
            }

            if(! in_array($filterValue, $emptyFilterValueArray))
            {
               return true;
            }
         }
      }
   }

   //updates
   if($filterValue == "EMPTY")
      $filterValue = "";

   //We fist need to take the value fom the session
  
   if(preg_match("/\;/i",$fieldName))
   {
      //we have multiple fields and need to check multiple reasons
      $fieldsListArr = explode(";",$fieldName);
      $noOfFiledsToCheck = count($fieldsListArr);

      if($fieldType == "VAL")
      {
         if($acceptance == "SKP")
         {
            if($valueType == "ALL")
            {
               $i = 0;
               foreach($fieldsListArr as $cnt => $filedFromListName)
               {
                  if($session["_YourDetails_"][$filedFromListName] == $filterValue)
                  {
                     $i++;
                  }
               }

               if($i == $noOfFiledsToCheck)
                  return false;
               else
                  return true;
            }
         }
      }
   }
   else
   {
      $valueToCompare = $session["_YourDetails_"][$fieldName];
      //here are the special filters : time, postocode, age ...
      if($fieldName == "other_business_trade" || $fieldName == "other_business" || $fieldName == "other_trade" || $fieldName == "other_fleet_type" || $fieldName == "other_property_type" || $fieldName == "other_property_type_commercial" || $fieldName == "unoccupancy_reason_other") // bespoke other business trade
      {

         if(trim($valueToCompare) == "")
            return true; 

         $valueToCompare = strtolower($valueToCompare);
         
         //check if the details in Skeleton are , "comma" separated so we need to explode them
         $filterValue = trim($filterValue);

         if($session["_QZ_QUOTE_DETAILS_"]["system"] == "PUB")
            $filterValue = strtolower($filterValue);

         if(preg_match("/,/isU",$filterValue))
         {
            $filterValuesOBTArray = explode(",",$filterValue);
            $fieldTypeToCheck     = "LIST";
         }
         else
         {
            $fieldTypeToCheck     = "VAL";
         }

         //Skip
         if($acceptance == "SKP")
         {
            if($fieldTypeToCheck == "VAL")
            {
               if($valueType == "EQL")
               {
                  if("$valueToCompare" == "$filterValue")
                     return false;
                  else
                     return true;
               }

               if($valueType == "CTN")
               {
                  if(preg_match("/$filterValue/isU",$valueToCompare))
                     return false;
                  else
                     return true;
               }
            }


            if($fieldTypeToCheck == "LIST")
            {
               if($valueType == "EQL")
               {
                  $isSkipped = false;
                  foreach($filterValuesOBTArray as $key=>$val)
                  {
                     $val = trim($val);

                     if("$val" == "$valueToCompare")
                     {
                        $isSkipped = true;
                     }
                  }

                  if($isSkipped)
                     return false;
                  else
                     return true;
               }

               if($valueType == "CTN")
               {
                  $isSkipped = false;
                  foreach($filterValuesOBTArray as $key=>$val)
                  {
                     $val = trim($val);
                     if(preg_match("/$val/isU",$valueToCompare))
                     {
                        $isSkipped = true;
                     }
                  }

                  if($isSkipped)
                     return false;
                  else
                     return true;
               }
            }
         }
         
         //Skip
         if($acceptance == "ACC")
         {
            if($fieldTypeToCheck == "VAL")
            {
               if($valueType == "EQL")
               {
                  if("$valueToCompare" == "$filterValue")
                     return true;
                  else
                     return false;
               }

               if($valueType == "CTN")
               {
                  if(preg_match("/$filterValue/isU",$valueToCompare))
                     return true;
                  else
                     return false;
               }
            }

            if($fieldTypeToCheck == "LIST")
            {
               if($valueType == "EQL")
               {
                  $isSkipped = false;
                  foreach($filterValuesOBTArray as $key=>$val)
                  {
                     $val = trim($val);
                     if("$val" == "$valueToCompare")
                     {
                        $isSkipped = true;
                     }
                  }

                  if($isSkipped)
                     return true;
                  else
                     return false;
               }

               if($valueType == "CTN")
               {
                  $isSkipped = false;
                  foreach($filterValuesOBTArray as $key=>$val)
                  {
                     $val = trim($val);
                     if(preg_match("/$val/isU",$valueToCompare))
                     {
                        $isSkipped = true;
                     }
                  }

                  if($isSkipped)
                     return true;
                  else
                     return false;
               }
            }
         }
      }//end bespoke other business trade
      elseif($fieldName == "age")
      {
         $birthDate = $session["_YourDetails_"]["date_of_birth_yyyy"]."-".$session["_YourDetails_"]["date_of_birth_mm"]."-" . $session["_YourDetails_"]["date_of_birth_dd"];
         $DISDate   = $session["_YourDetails_"]["date_of_insurance_start_yyyy"]."-" . $session["_YourDetails_"]["date_of_insurance_start_mm"]."-".$session["_YourDetails_"]["date_of_insurance_start_dd"];

         $ageOfProposer = $this->GetAge($birthDate,$DISDate);

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($ageOfProposer < $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MAX")
               {
                  if($ageOfProposer > $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }

         if($acceptance == "ACC")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MAX")
               {
                  if($ageOfProposer > $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MIN")
               {
                  if($ageOfProposer < $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
      }


   if($session['_QZ_QUOTE_DETAILS_']['system'] == 'KITCAR')
   {
      if($fieldName == "kitcar_year_of_manufacture")
      {
         $yearOfManufacture = $session['_YourDetails_']['kitcar_year_of_manufacture'];
                
         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if(in_array($yearOfManufacture,$filterValue))   
                 return false;
               else
                 return true;
            }

            if($acceptance == "ACC")
            {
               if(! in_array($yearOfManufacture,$filterValue))
                 return false;
               else
                 return true;
            }
         }
      }
   } 
      


  //We exclude filters from hidded questions
  $hiddenQuestionsArr = array(
                        "SURGERY"               => array("building_sum_insured","contents_sum_insured"),
                        "HORSE"                 => array("year_of_purchase"),
                        "CAMPERVAN"             => array("type_of_conversion", "time_until_completion"),
                        "DRIVINGSCHOOL"             => array("registration_number_question", "vehicle_registration_number", "motor_home_reg_number",),
                        "MINIBUS"               => array("plating_authority",),
                        "MOTORHOME"             => array("type_of_conversion", "time_until_completion"),
                        "LIMO"                  => array("year_of_manufacture"),
                        "MOTORFLEET"            => array("other_fleet_type","kept_postcodet_postcode","business_trade","other_business_trade","trading_years","fleet_rated",),
                        "NONSTANDARDHOME"       => array("other_property_type","other_property_type_commercial","property_type_commercial","property_type",),
                        "UNOCCUPIEDPROPERTY"    => array("unoccupancy_reason_other","other_property_type","other_property_type_commercial","property_type_commercial","property_type",),
                        "PROFESSIONALINDEMNITY" => array("kept_postcode", "business_trade", "other_business_trade", "limit_of_liability", "employers_liability_required", ),
                        "HOLIDAYHOME"           => array("property_value","contents_value"),
                         );

   foreach($hiddenQuestionsArr as $systemNameKey => $questionIds)
   {
      if($_SESSION["_QZ_QUOTE_DETAILS_"]["system"] == "UNOCCUPIEDPROPERTY")
      {
         if($fieldName == "property_value_sk" && trim($_SESSION["_YourDetails_"]["buildings_sum_insured"]) == "")
            return true;
         else if($fieldName == "content_value_sk" && trim($_SESSION["_YourDetails_"]["contents_sum_insured"]) == "")
            return true;
      }
      else if($_SESSION["_QZ_QUOTE_DETAILS_"]["system"] == "HOLIDAYHOME")
      {
         if($fieldName == "property_value_sk" && trim($_SESSION["_YourDetails_"]["property_value"]) == "")
            return true;
         else if($fieldName == "contents_value_sk" && trim($_SESSION["_YourDetails_"]["contents_value"]) == "")
            return true;  
      }
      else if($_SESSION["_QZ_QUOTE_DETAILS_"]["system"] == "PUB")
      {
         if($fieldName == "property_value_sk" && trim($_SESSION["_YourDetails_"]["building_sum_insured"]) == "")
            return true;
         else if($fieldName == "content_value_sk" && trim($_SESSION["_YourDetails_"]["contents_sum_insured"]) == "")
            return true;
         else if($fieldName == "value_of_stock_sk" && trim($_SESSION["_YourDetails_"]["stock_in_trade_sum_insured"]) == "")
            return true;
      }

      if($_SESSION["_QZ_QUOTE_DETAILS_"]["system"] == $systemNameKey)
         foreach($questionIds as $hiddenQuestionName)
         {
            if($hiddenQuestionName == $fieldName && trim($_SESSION["_YourDetails_"][$hiddenQuestionName]) === "")
               return true;
         }
   }

   //to set up the skeleton age filter
   if($fieldName == "age_range")
   {
      $birthDate = $session["_YourDetails_"]["date_of_birth_yyyy"]."-".$session["_YourDetails_"]["date_of_birth_mm"]."-".$session["_YourDetails_"]["date_of_birth_dd"];
      $DISDate   = $session["_YourDetails_"]["date_of_insurance_start_yyyy"]."-".$session["_YourDetails_"]["date_of_insurance_start_mm"]."-".$session["_YourDetails_"]["date_of_insurance_start_dd"];

      //for Healt we have other values
      if($session['_QZ_QUOTE_DETAILS_']['system'] == "HEALTH")
      {
         $DISDate = $session["_YourDetails_"]["start_date_yyyy"]."-".$session["_YourDetails_"]["start_date_mm"]."-".$session["_YourDetails_"]["start_date_dd"];
         if($DISDate == "--")
            $DISDate = date("Y-m-d");
      }

      $ageOfProposer = $this->GetAge($birthDate,$DISDate);

      $filterValueArray = explode("-",trim($filterValue));//first value is the smallest and the second is the biggest

      if($acceptance == "SKP") //we skip the range
      {
         if((($ageOfProposer > $filterValueArray[0]) && ($ageOfProposer < $filterValueArray[1])) || (($ageOfProposer == $filterValueArray[0]) && ($ageOfProposer == $filterValueArray[1])))
            return false;
         else
            return true;
      }

     if($acceptance == "ACC") //we accept the range
     {
        $minAgeToFilter = trim($filterValueArray[0]);
        $maxAgeToFilter = trim($filterValueArray[1]);

        if($minAgeToFilter > 0 && $maxAgeToFilter > 0 && ($minAgeToFilter == $maxAgeToFilter))
        $ageFilter = $minAgeToFilter;

        if($ageFilter) // this solves Accept if value is between 18 and 18 issue
        if(($ageOfProposer < $minAgeToFilter) || ($ageOfProposer > $maxAgeToFilter))
           return false;

        if(!$ageFilter)              
          if((($ageOfProposer < $minAgeToFilter) || ($ageOfProposer > $maxAgeToFilter)) || (($ageOfProposer == $minAgeToFilter) && ($ageOfProposer == $maxAgeToFilter)))
           return false;

        return true;
     }
   }
   
    // set off skeleton filters when question hides    
    if($session['_QZ_QUOTE_DETAILS_']['system'] == "HOLIDAYHOME" )
    {
      $propertyType       = $session['_YourDetails_']['property_type'];
      $yearPropertyBuilt  = $session['_YourDetails_']['year_property_built'];
      $propertyTypeArr    = array(6,7,15,16,22);

      // Hide question if "Property Type" = Beach Hut, Caravan, Mobile Home, Parkhome, Summer House
      if($fieldName == "year_property_built" && in_array($propertyType,$propertyTypeArr) && $yearPropertyBuilt == '')
        return true;      
    } 
    // END -- set off skeleton filters when question hides

      //sms_consent
      /*
      [consent_statements_values] => Array
      (
      [companyMarketing] => Array
      (
        [Email] => 
        [SMS] => 
      )
      */

      //sms consent
      if($fieldName == "sms_consent")
      {   
         if(isset($session['consent_statements_values']['companyMarketing']['SMS']))
         {
            $smsConsent = $session['consent_statements_values']['companyMarketing']['SMS'];   
            if($smsConsent == "")
               $smsConsent = 0;

            if($acceptance == "SKP")
            {
               if($fieldType == "LIST")
               {
                  if($smsConsent == $filterValue[0])
                     return false;
                  else
                     return true;
               }
            }

            if($acceptance == "ACC")
            {
               if($fieldType == "LIST")
               { 
                  if($smsConsent != $filterValue[0])
                     return false;
                  else
                     return true;
               }   
            }
         }
         else
            return true;
      }

      //email consent
      if($fieldName == "email_consent")
      {  

         if(isset($session['consent_statements_values']['companyMarketing']['Email']))
         {
            $emailConsent = $session['consent_statements_values']['companyMarketing']['Email'];   
            if($emailConsent == "")
               $emailConsent = 0;

            if($acceptance == "SKP")
            {
               if($fieldType == "LIST")
               {
                  if($emailConsent == $filterValue[0])
                     return false;
                  else
                     return true;
               }
            }

            if($acceptance == "ACC")
            {
               if($fieldType == "LIST")
               { 
                  if($emailConsent != $filterValue[0])
                     return false;
                  else
                     return true;
               }   
            }
         }
         else
            return true;
      }


     // content_value_sk
	  $systemName = $session['_QZ_QUOTE_DETAILS_']['system'];
     if($systemName == "UNOCCUPIEDPROPERTY" || $systemName == "NONSTANDARDHOME" || $systemName == "LANDLORDS" || $systemName == "HOLIDAYHOME" || $systemName == "COMMERCIALPROPERTY")
     {
        if ($fieldName == "content_value_sk" || $fieldName == "contents_value_sk")
        {
           if ($systemName == "UNOCCUPIEDPROPERTY")
              $contentsValue = $session['_YourDetails_']['contents_sum_insured'];
           elseif ($systemName == "NONSTANDARDHOME" || $systemName == "HOLIDAYHOME")
              $contentsValue = $session['_YourDetails_']['contents_value'];
           elseif ($systemName == "LANDLORDS" || $systemName == "COMMERCIALPROPERTY")
              $contentsValue = $session['_YourDetails_']['content_value'];

           $typeOfCover = $session['_YourDetails_']['type_of_cover'];

           if (($systemName == "UNOCCUPIEDPROPERTY" && $typeOfCover != 2) || ($systemName == "LANDLORDS" && $typeOfCover != 2) || ($systemName == "NONSTANDARDHOME" && $typeOfCover != 1) || ($systemName == "HOLIDAYHOME" && $typeOfCover != 1) || ($systemName == "COMMERCIALPROPERTY" && $typeOfCover != "Buildings Only"))
           {

              $filterValueArray = explode("-", str_replace(" ", "", $filterValue));

              if ($acceptance == "SKP") //we skip the range
              {
                 if ((($contentsValue > $filterValueArray[0]) && ($contentsValue < $filterValueArray[1])) || (($contentsValue == $filterValueArray[0]) || ($contentsValue == $filterValueArray[1])))
                    return false;
                 else
                    return true;
              }

              if ($acceptance == "ACC") //we accept the range
              {
                 if ((($contentsValue < $filterValueArray[0]) || ($contentsValue > $filterValueArray[1])))
                    return false;
                 else
                    return true;
              }
           }
        }// end content_value_sk

        if ($fieldName == "building_value_sk" && $systemName == "COMMERCIALPROPERTY")
        {
           $typeOfCover = $session['_YourDetails_']['type_of_cover'];
           $buildingValue = $session['_YourDetails_']['building_value'];

           if ($typeOfCover != "Contents Only")
           {
              $filterValueArray = explode("-", str_replace(" ", "", $filterValue));

              if ($acceptance == "SKP") //we skip the range
              {
                 if ((($buildingValue > $filterValueArray[0]) && ($buildingValue < $filterValueArray[1])) || (($buildingValue == $filterValueArray[0]) || ($buildingValue == $filterValueArray[1])))
                    return false;
                 else
                    return true;
              }

              if ($acceptance == "ACC") //we accept the range
              {
                 if ((($buildingValue < $filterValueArray[0]) || ($buildingValue > $filterValueArray[1])))
                    return false;
                 else
                    return true;
              }
           }
        }

        if ($fieldName == "building_value_sk" && $systemName == "LANDLORDS")
        {
           $typeOfCover = $session['_YourDetails_']['type_of_cover'];
           $buildingValue = $session['_YourDetails_']['building_value'];

           if ($typeOfCover != "3")
           {
              $filterValueArray = explode("-", str_replace(" ", "", $filterValue));

              if ($acceptance == "SKP") //we skip the range
              {
                 if ((($buildingValue > $filterValueArray[0]) && ($buildingValue < $filterValueArray[1])) || (($buildingValue == $filterValueArray[0]) || ($buildingValue == $filterValueArray[1])))
                    return false;
                 else
                    return true;
              }

              if ($acceptance == "ACC") //we accept the range
              {
                 if ((($buildingValue < $filterValueArray[0]) || ($buildingValue > $filterValueArray[1])))
                    return false;
                 else
                    return true;
              }
           }
        }
     }

     if($systemName == "SALON")
     {
        if($fieldName == "building_value_sk")
        {   
            $buildingValue =  $session['_YourDetails_']['buildings_cover_required'];           
            $buildingValueRadio = $session['_YourDetails_']['buildings_cover_required_radio'];

            if($buildingValueRadio == 'Y')
            {                                  
                $filterValueArray = explode("-",str_replace(" ","",$filterValue));

                if($acceptance == "SKP") //we skip the range
                {
                  if((($buildingValue > $filterValueArray[0]) && ($buildingValue < $filterValueArray[1])) || (($buildingValue == $filterValueArray[0]) || ($buildingValue == $filterValueArray[1])))
                     return false;
                  else
                     return true;
                }

                if($acceptance == "ACC") //we accept the range
                {
                  if((($buildingValue < $filterValueArray[0]) || ($buildingValue > $filterValueArray[1])))
                     return false;
                  else
                     return true;
                }
            }
         return true;
        }

        if($fieldName == "buildings_cover_required_radio")
        {   
            
          $buildingCover = $session['_YourDetails_']['buildings_cover_required_radio'];
          if($buildingCover != '')
          {  
            if($acceptance == "SKP")
            {
              if($fieldType == "LIST")
              {
                if($buildingCover == $filterValue[0])
                   return false;
                else
                   return true;
              }
            }

            if($acceptance == "ACC")
            {
              if($fieldType == "LIST")
              { 
                if($buildingCover != $filterValue[0])
                   return false;
                else
                   return true;
              }
            }
          }
          return true;
        }


        if($fieldName == "content_value_sk")
        {   
            $contentValue                 =  $session['_YourDetails_']['value_of_contents'];           
            $contentsToolsEquipamentCover =  $session['_YourDetails_']['contents_tools_equipment_cover'];

            if($contentsToolsEquipamentCover == 'Y')
            {                                  
                $filterValueArray = explode("-",str_replace(" ","",$filterValue));

                if($acceptance == "SKP") //we skip the range
                {
                  if((($contentValue > $filterValueArray[0]) && ($contentValue < $filterValueArray[1])) || (($contentValue == $filterValueArray[0]) || ($contentValue == $filterValueArray[1])))
                     return false;
                  else
                     return true;
                }

                if($acceptance == "ACC") //we accept the range
                {
                  if((($contentValue < $filterValueArray[0]) || ($contentValue > $filterValueArray[1])))
                     return false;
                  else
                     return true;
                }
            }
         return true;
        }


        if($fieldName == "value_of_stock_sk")
        {   
            $stockValue      =  $session['_YourDetails_']['value_of_stock'];           
            $stockCover      =  $session['_YourDetails_']['stock_cover'];

            if($stockCover == 'Y')
            {                                  
                $filterValueArray = explode("-",str_replace(" ","",$filterValue));

                if($acceptance == "SKP") //we skip the range
                {
                  if((($stockValue > $filterValueArray[0]) && ($stockValue < $filterValueArray[1])) || (($stockValue == $filterValueArray[0]) || ($stockValue == $filterValueArray[1])))
                     return false;
                  else
                     return true;
                }

                if($acceptance == "ACC") //we accept the range
                {
                  if((($stockValue < $filterValueArray[0]) || ($stockValue > $filterValueArray[1])))
                     return false;
                  else
                     return true;
                }
            }
         return true;
        }

      } 

      if($systemName == "HOLIDAYHOME" || $systemName == "NONSTANDARDHOME")
      {
         if($fieldName == "property_value_sk")
         {
            if($systemName == "HOLIDAYHOME")
               $propertyValue =  $session['_YourDetails_']['property_value'];

              if($systemName == "NONSTANDARDHOME")
                 $propertyValue =  $session['_YourDetails_']['property_value'];


             $typeOfCover = $session['_YourDetails_']['type_of_cover'];

            if($typeOfCover != 2)
            {
               $filterValueArray = explode("-",str_replace(" ","",$filterValue));

               if($acceptance == "SKP") //we skip the range
               {
               if((($propertyValue > $filterValueArray[0]) && ($propertyValue < $filterValueArray[1])) || (($propertyValue == $filterValueArray[0]) || ($propertyValue == $filterValueArray[1])))
                  return false;
               else
                  return true;
               }

               if($acceptance == "ACC") //we accept the range
               {
               if((($propertyValue < $filterValueArray[0]) || ($propertyValue > $filterValueArray[1])))
                  return false;
               else
                  return true;
               }
            }
            return true;
         }
      } // end content_value_sk


      //maximum_days_unoccupied_year filter - don't check it if it is hidden
      if($systemName == "NONSTANDARDHOME")
      {
         if($fieldName == "maximum_days_unoccupied_year")
         {
            $maxNoOfDaysUnoccupied = $session['_YourDetails_']['maximum_days_unoccupied_year'];
            $propertyOccupied      = $session['_YourDetails_']['property_ever_occupied'];

            if($propertyOccupied == "N")
            {
               if($acceptance == "SKP")
               {
                  if(in_array($maxNoOfDaysUnoccupied,$filterValue))
                     return false;
                  else
                     return true;
               }

               if($acceptance == "ACC")
               {
                  if(! in_array($maxNoOfDaysUnoccupied,$filterValue))
                     return false;
                  else
                     return true;
               }
            }
            return true;
         }
      } // end maximum_days_unoccupied_year

      //premises_cover_required
       if($systemName == "MOTORTRADE")
       {
          if($fieldName == "premises_cover_required")
          {

             $premCoverValue = $session['_YourDetails_']['premises_cover_required'];
             $operateFrom    = $session['_YourDetails_']['operate_from'];

             $businessCoverRequiredShownArray = array(
                "BP",
                "B",
             );

             if(in_array($operateFrom,$businessCoverRequiredShownArray))
             {
                if($acceptance == "SKP")
                {
                   if(in_array($premCoverValue,$filterValue))
                      return false;
                   else
                      return true;
                }

                if($acceptance == "ACC")
                {
                   if(! in_array($premCoverValue,$filterValue))
                      return false;
                   else
                      return true;
                }
             }
             return true;
          }
       }//
      
      
      // do not skip if hidden question
      if($systemName == "CAREHOME")
	  {
		  if($fieldName == "building_value")
		  {	
			if($systemName == "CAREHOME")
				$propertyValue =  $session['_YourDetails_']['building_value'];
				

			 $typeOfCover = $session['_YourDetails_']['type_of_cover'];

             if($systemName == "CAREHOME" && $typeOfCover == 3)             
               return true;
		  }
        
        if($fieldName == "content_value")
		  {	
			if($systemName == "CAREHOME")
				$propertyValue =  $session['_YourDetails_']['content_value'];
				

			 $typeOfCover = $session['_YourDetails_']['type_of_cover'];

             if($systemName == "CAREHOME" && $typeOfCover == 2)             
               return true;
		  }    
      } // end content_value_sk
      
      //to set up the skeleton value questions


      $skipSystemsArray = array(
         "COMMERCIALPROPERTY",
         "LANDLORDS",
         "NONSTANDARDHOME",
      );

      if(! in_array($systemName,$skipSystemsArray))
      {
         if ($fieldName == 'bicycle_value_sk' || $fieldName == 'vehicle_value_sk' || $fieldName == 'estimated_value_sk' || $fieldName == 'contents_value_sk' || $fieldName == 'total_vehicle_value_sk' || $fieldName == 'building_value_sk' || $fieldName == 'content_value_sk' || $fieldName == 'property_value_sk' || $fieldName == 'purchase_price_sk' ||
            $fieldName == 'annual_turnover_sk' || $fieldName == 'bicycle_value_total_sk' || $fieldName == 'value_of_stock_sk')
         {
            $preFieldName = '';
            switch ($fieldName) {
               case 'bicycle_value_sk':
                  $preFieldName = 'bicycle_value';
                  break;
               case 'vehicle_value_sk':
                  $preFieldName = 'vehicle_value';
                  break;
               case 'estimated_value_sk':
                  $preFieldName = 'estimated_value';
                  break;
               case 'contents_value_sk':
                  $preFieldName = 'contents_value';
                  break;
               case 'total_vehicle_value_sk':
                  $preFieldName = 'total_vehicle_value';
                  break;
               case 'building_value_sk':
                  $preFieldName = 'building_value';
                  break;
               case 'content_value_sk':
                  $preFieldName = 'content_value';
                  break;
               case 'property_value_sk':
                  $preFieldName = 'property_value';
                  break;
               case 'annual_turnover_sk':
                  $preFieldName = 'annual_turnover';
                  break;
               case 'annual_turnover_sk':
                  $preFieldName = 'annual_turnover';
                  break;
               case 'purchase_price_sk':
                  $preFieldName = 'purchase_price';
                  break;
                case 'bicycle_value_total_sk':
                  $preFieldName = 'bicycle_value_total';
                  break;
               default:
                  $preFieldName = '';
            }

            if ($fieldName == "estimated_value_sk" AND $session["_QZ_QUOTE_DETAILS_"]["system"] == "CAMPERVAN")
               $preFieldName = 'vehicle_value';

            if ($fieldName == "property_value_sk" AND $session["_QZ_QUOTE_DETAILS_"]["system"] == "PUB")
               $preFieldName = 'building_sum_insured';

            if ($fieldName == "content_value_sk" AND $session["_QZ_QUOTE_DETAILS_"]["system"] == "PUB")
               $preFieldName = 'contents_sum_insured';

            if ($fieldName == "value_of_stock_sk" AND $session["_QZ_QUOTE_DETAILS_"]["system"] == "PUB")
               $preFieldName = 'stock_in_trade_sum_insured';

            if ($fieldName == "property_value_sk" AND $session["_QZ_QUOTE_DETAILS_"]["system"] == "UNOCCUPIEDPROPERTY")
               $preFieldName = 'buildings_sum_insured';

            $sessionValue = $session["_YourDetails_"][$preFieldName];
            $filterValueArray = explode("-", str_replace(" ", "", $filterValue));//first value is the smallest and the second is the biggest

            if ($acceptance == "SKP") //we skip the range
            {
               if ((($sessionValue >= $filterValueArray[0]) && ($sessionValue <= $filterValueArray[1])))
                  return false;
               else
                  return true;
            }

            if ($acceptance == "ACC") //we accept the range
            {
               if ((($sessionValue < $filterValueArray[0]) || ($sessionValue > $filterValueArray[1])))
                  return false;
               else
                  return true;
            }
         }
      }
      
      
      //van vehicle seats
      if($fieldName == "van_seats")
      {
         include_once "/home/www/quotezone.co.uk/convicted-driver-insurance/convicteddriver/modules/VanVehicle.php";
         $objVanVehicle = new CVanVehicle();
         $vanCode = $objVanVehicle->GetVehicleCode($session["_YourDetails_"]["vehicle_confirm"]);
         $vanDetailsArray = $objVanVehicle->GetVehicle($vanCode);
         $seats = $vanDetailsArray['seats'];
         
         if($acceptance == "SKP") //we skip the range
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($seats >= $filterValue)
                     return false;
                  else
                     return true;
               }
               
               if($valueType == "MAX")
               {
                  if($seats <= $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
         
      }
      

	  //to set up the motorfleet total number of vehicles
      if($fieldName == "number_of_vehicles_mf")
      {
         $sessionNumberOfVehicles = $session["_YourDetails_"]["number_of_vehicle"];

         $filterValueArray = explode("-",trim($filterValue));//first value is the smallest and the second is the biggest

         if($acceptance == "SKP") //we skip the range
         {
            if((($sessionNumberOfVehicles > $filterValueArray[0]) && ($sessionNumberOfVehicles < $filterValueArray[1])) || (($sessionNumberOfVehicles == $filterValueArray[0]) || ($sessionNumberOfVehicles == $filterValueArray[1])))
               return false;
            else
               return true;
         }

         if($acceptance == "ACC") //we accept the range
         {
            if((($sessionNumberOfVehicles < $filterValueArray[0]) || ($sessionNumberOfVehicles > $filterValueArray[1])))
               return false;
            else
               return true;
         }
      }

	  //to set up the motor-trade combined no claims bonus
      if($fieldName == "combined_ncb_mt")
      {
         $motortrade_ncb = $session["_YourDetails_"]["motortrade_ncb"];
         $private_car_ncb = $session["_YourDetails_"]["private_car_ncb"];

         if($acceptance == "SKP") //we skip the range
         {
            if((in_array($motortrade_ncb, $filterValue) && in_array($private_car_ncb, $filterValue)) && $private_car_ncb == $motortrade_ncb)
               return false;
            else
               return true;
         }

         if($acceptance == "ACC") //we accept the range
         {
            if((in_array($motortrade_ncb, $filterValue) && in_array($private_car_ncb, $filterValue)) && $private_car_ncb == $motortrade_ncb)
               return true;
            else
               return false;
         }
      }
	  
	$vehicleTypeArr = array(
	"vehicle_type_1" => "type_cars",
	"vehicle_type_2" => "type_vans",
	"vehicle_type_3" => "type_motorbikes",
	"vehicle_type_4" => "type_pick_ups",
	"vehicle_type_5" => "type_lorries___trucks",
	"vehicle_type_10" => "type_minibuses",
	"vehicle_type_12" => "type_other_vehicles",
	);
	
	foreach($vehicleTypeArr as $fieldNameArr => $VehTypeVal)
	{	
		
		if($fieldName == $VehTypeVal)
	    {
			$motorFleetVehType = $session["_YourDetails_"][$fieldNameArr];
			
			if($acceptance == "SKP")
			{
			  if(in_array($motorFleetVehType, $filterValue))
				  return false;
			  else
				  return true;		
			}
			
			if($acceptance == "ACC")
			{
			  if(in_array($motorFleetVehType, $filterValue))
				  return true;
			  else
				  return false;		
			}
		}
	} 
		  
	  	  
	  
	  
      // taxi vehicle age
      if($fieldName == "taxi_vehicle_age")
      {
         $vehicleYear = $session['_YourDetails_']['year_of_manufacture'];
         $DISDateYear = $session["_YourDetails_"]["date_of_insurance_start_yyyy"];

         $vehicleAge = $DISDateYear - $vehicleYear;

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($vehicleAge >= $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MAX")
               {
                  if($vehicleAge <= $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
         if($acceptance == "ACC")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($vehicleAge < $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MAX")
               {
                  if($vehicleAge > $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
      } // END skip vehicle taxi vehicle age
		
		
		// import car vehicle age
      if($fieldName == "importcar_vehicle_age")
      {
         $vehicleYear = $session['_YourDetails_']['importcar_year_of_manufacture'];
         $DISDateYear = $session["_YourDetails_"]["date_of_insurance_start_yyyy"];

         $vehicleAge = $DISDateYear - $vehicleYear;

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($vehicleAge >= $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MAX")
               {
                  if($vehicleAge <= $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
         if($acceptance == "ACC")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($vehicleAge < $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MAX")
               {
                  if($vehicleAge > $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
      } // END skip vehicle taxi vehicle age
      
      		// kitcar car vehicle age
      if($fieldName == "kitcar_vehicle_age")
      {
         $vehicleYear = $session['_YourDetails_']['kitcar_year_of_manufacture'];
         $DISDateYear = $session["_YourDetails_"]["date_of_insurance_start_yyyy"];

         $vehicleAge = $DISDateYear - $vehicleYear;

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($vehicleAge >= $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MAX")
               {
                  if($vehicleAge <= $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
         if($acceptance == "ACC")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($vehicleAge < $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MAX")
               {
                  if($vehicleAge > $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
      } // END skip vehicle kitcar vehicle age

      // importcar make model
      if($fieldName == "importcar_make_model")
      {
         $importcarMake  = $_SESSION['_YourDetails_']['importcar_make'];
         $importcarModel = $_SESSION['_YourDetails_']['importcar_model'];

         $importcarModel = str_replace("Lancer Evolution", "Lancerevolution", $importcarModel);
         $importcarModel = str_replace("Hi Lux Surf", "Hiluxsurf", $importcarModel);
         $importcarModel = str_replace("Integra Type R", "Integratyper", $importcarModel);

         $importcarMake  = strtolower($importcarMake);
         $importcarMake  = ucfirst($importcarMake);

         $importcarModel  = strtolower($importcarModel);
         $importcarModel  = ucfirst($importcarModel);


         $wrongSpelledMitsubishiArray = array(
            "Misubishi",
            "Mitibushi",
            "Mitsabushi",
            "Mitsbushi",
            "Mitserbushi",
            "Mitshibishi",
            "Mitshubi",
            "Mitsibishi",
            "Mitsibushi",
            "Mitsubishi",
         );

         if(in_array($importcarMake,$wrongSpelledMitsubishiArray))
            $importcarMake = "Mitsubishi";

         if($fieldType == "LIST")
         {
            if($acceptance == "ACC")
            {
               if(array_key_exists($importcarMake, $filterValue))
               {
                  foreach($filterValue[$importcarMake] as $vehModel)
                  {
                     if(preg_match("/\b($vehModel)\b/",$importcarModel))
                     {
                        $filter = "accepted";
                     }
                  }
               }
               if($filter != "accepted" || ! array_key_exists($importcarMake, $filterValue))
                  return false;
               else
                  return true;
            }

            if($acceptance == "SKP")
            {
               if(array_key_exists($importcarMake, $filterValue))
               {
                  foreach($filterValue[$importcarMake] as $vehModel)
                  {
                     if(preg_match("/\b($vehModel)\b/",$importcarModel))
                     {
                        $filter = "skip";
                     }
                  }
               }
               if($filter == "skip")
                  return false;
            }
         }
      }
      
      // importcar make model -- INC ARRAY FROM THE FILTER IS UPPERCASE
      if($fieldName == "IMPORTCAR_MAKE_MODEL")
      {
         $importcarMake  = strtoupper(trim($_SESSION['_YourDetails_']['importcar_make']));
         $importcarModel = strtoupper(trim($_SESSION['_YourDetails_']['importcar_model']));
      
         if($fieldType == "LIST")
         {
            if($acceptance == "ACC")
            {
               if(array_key_exists($importcarMake, $filterValue))
               {
                  foreach($filterValue[$importcarMake] as $vehModel)
                  {
                     if(preg_match("/\b($vehModel)\b/",$importcarModel))
                     {
                        $filter = "accepted";
                     }
                  }
               }
               if($filter != "accepted" || ! array_key_exists($importcarMake, $filterValue))
                  return false;
               else
                  return true;
            }

            if($acceptance == "SKP")
            {
               if(array_key_exists($importcarMake, $filterValue))
               {
                  foreach($filterValue[$importcarMake] as $vehModel)
                  {
                     if(preg_match("/\b($vehModel)\b/",$importcarModel))
                     {
                        $filter = "skip";
                     }
                  }
               }
               if($filter == "skip")
                  return false;
            }
         }
      }


      //skeleton importcar make - model filter
      if($fieldName == "vehicle_model_import")
      {
         $importcarMake  = strtoupper(trim($_SESSION['_YourDetails_']['importcar_make']));
         $importcarModel = strtoupper(trim($_SESSION['_YourDetails_']['importcar_model']));
      
         if($fieldType == "LIST")
         {
            if($acceptance == "ACC")
            {
               foreach($filterValue as $vehMakeValue)
               {
                 $vehArr = explode(":", $vehMakeValue);
                 $make   = trim($vehArr[0]);
                 $model  = trim($vehArr[1]);

                 if($importcarMake == $make && $importcarModel == $model)
                 {
                    $filter = "accepted";                   
                    break;
                 } 
               }
                if($filter == "accepted")
                  return true;
                else
                  return false;             
            }

            if($acceptance == "SKP")
            {
              foreach($filterValue as $vehMakeValue)
               {
                 $vehArr = explode(":", $vehMakeValue);
                 $make   = trim($vehArr[0]);
                 $model  = trim($vehArr[1]);

                 if($importcarMake == $make && $importcarModel == $model)
                 {
                    $filter = "skiped";                   
                    break;
                 } 
               }
                if($filter == "skiped")
                  return false;
                else
                  return true;   
            }
         }
      }
      
      // current year age
      if($fieldName == "current_year_age")
      {
         $vehicleYear = $session['_YourDetails_']['caravan_year'];
         $DISDateYear = date('Y');

         $vehicleAge = $DISDateYear - $vehicleYear;

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($vehicleAge > $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MAX")
               {
                  if($vehicleAge < $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
      } // END current year age
	  
	  // date passed driving test years
      if($fieldName == "date_passed_driving_test_DIS")
      {
	  
	    $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
		$DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
		$DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];
		
         $DPDTdd   = $_SESSION['_YourDetails_']['date_passed_driving_test_dd'];
		 $DPDTmm   = $_SESSION['_YourDetails_']['date_passed_driving_test_mm'];
		 $DPDTyyyy = $_SESSION['_YourDetails_']['date_passed_driving_test_yyyy'];

		 $yearsToFilter = 60 * 60 * 24 * 365 * $filterValue;
		 
		 $DPDT= strtotime($DPDTdd."-".$DPDTmm."-".$DPDTyyyy);
		 $DIS =  strtotime($DISdd."-".$DISmm."-".$DISyyyy);
		 $difference = $DIS - $DPDT;
		//$yearsBefore   = date('Y-m-d',mktime(0,0,0, ($DISmm-$yearsToFilter), $DISdd, $DISyyyy));
         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MAX")
               {
                  if($difference < $yearsToFilter)
                     return false;
                  else
                     return true;
               }
            }
         }
      } // END date passed driving test years
	  
      // GENERAL  vehicle age filter 
      if($fieldName == "vehicle_age")
      {
         $vehicleYear = $session['_YourDetails_']['year_of_manufacture'];
         $DISDateYear = $session["_YourDetails_"]["date_of_insurance_start_yyyy"];

         $vehicleAge = $DISDateYear - $vehicleYear;

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($vehicleAge > $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MAX")
               {
                  if($vehicleAge < $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
      } // END skip vehicle age

       // boat age
      if($fieldName == "boat_age")
      {
         $vehicleYear = $session['_YourDetails_']['year_built'];
         $DISDateYear = $session['_YourDetails_']["date_of_insurance_start_yyyy"];

         $boatAge = $DISDateYear - $vehicleYear;

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($boatAge > $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MAX")
               {
                  if($boatAge < $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }

         if($acceptance == "ACC")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($boatAge > $filterValue)
                     return true;
                  else
                     return false;
               }

               if($valueType == "MAX")
               {
                  if($boatAge < $filterValue)
                     return true;
                  else
                     return false;
               }
            }
         }
      } // end boat age

      // BOAT --> SKIP / ACCEPT if mooring_location contains a word   
      
      if($fieldName == "mooring_location_contains")      
      {
         $boatType=$_SESSION["_YourDetails_"]["type_of_boat"];
         if ($boatType == "Yacht" || $boatType == "Motor Boat")
         {   
           $mooringLocation     = trim(strtoupper($_SESSION['_BoatDetails_']['mooring_location']));
        
           $mooringLocationFilter = 0;
           foreach($filterValue as $mooringLocationToCheck)
           {
              if(preg_match("/\b($mooringLocationToCheck)\b/", $mooringLocation))
              {
                 $mooringLocationFilter = 1;
                 break;
              }
           }
        
           if($fieldType == "LIST")
           {
              if($acceptance == "SKP")
              { 
                 if($mooringLocationFilter == 1)
                    return false;
                 else
                    return true;
              }

              if($acceptance == "ACC")
              {                              
                 if($mooringLocationFilter == 0)
                    return false;
                 else
                    return true;
              }
           }
        }    
      }

      if($fieldName == "vehicle_age_parkhome")
      {
         $vehicleYear = $session['_YourDetails_']['manufacture_year'];
         $DISDateYear = $session["_YourDetails_"]["date_of_insurance_start_yyyy"];

         $vehicleAge = $DISDateYear - $vehicleYear;

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($vehicleAge > $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MAX")
               {
                  if($vehicleAge < $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
      } // END skip vehicle age parkhome


      if($session["_QZ_QUOTE_DETAILS_"]["system"] == "BOAT")
      {

         //insured_sum - boat value aici alex
         if ($fieldName == "insured_sum")
         {
            $boatEstimatedValue    = $session["_YourDetails_"]["insured_sum"];
            $boatValIntervalsArray = explode("-",$filterValue);


            $minFilterBoatValue = $boatValIntervalsArray[0];
            $minFilterBoatValue = trim($minFilterBoatValue);

            $maxFilterBoatValue = $boatValIntervalsArray[1];
            $maxFilterBoatValue = trim($maxFilterBoatValue);

            if ($acceptance == "SKP")
            {
               if ($fieldType == "VAL")
               {
                  if ($valueType == "MIN")
                  {
                     if ($boatEstimatedValue > $minFilterBoatValue)
                        return false;
                     else
                        return true;
                  }

                  if ($valueType == "MAX")
                  {
                     if ($boatEstimatedValue < $minFilterBoatValue)
                        return false;
                     else
                        return true;
                  }


                  if ($valueType == "")
                  {
                     if (
                        ($boatEstimatedValue > $minFilterBoatValue) 
                        AND 
                        ($boatEstimatedValue < $maxFilterBoatValue)
                     )
                        return false;
                     else
                        return true;
                  }
               }
            }

            if ($acceptance == "ACC")
            {
               if ($fieldType == "VAL")
               {
                  if ($valueType == "MIN")
                  {
                     if ($boatEstimatedValue < $minFilterBoatValue)
                        return false;
                     else
                        return true;
                  }

                  if ($valueType == "MAX")
                  {
                     if ($boatEstimatedValue > $minFilterBoatValue)
                        return false;
                     else
                        return true;
                  }


                  if ($valueType == "")
                  {
                     if (
                        ($boatEstimatedValue < $minFilterBoatValue) 
                        || 
                        ($boatEstimatedValue > $maxFilterBoatValue)
                     )
                        return false;
                     else
                        return true;
                  }
               }
            }
         }

         //START boat filter details - questions that apply just to some of the boat types
         $boatType = $session["_YourDetails_"]["type_of_boat"];
         //Boat length (ft)	Standard integer, with min / max	Note: This filter is not applicable to PWC / Jetski boat types.
         if ($fieldName == "length") {
            $boatLength = $session["_YourDetails_"]["length"];

            if ($boatType == "7") {
               return true;
            }

            if ($acceptance == "SKP") {
               if ($fieldType == "VAL") {
                  if ($valueType == "MIN") {
                     if ($boatLength > $filterValue)
                        return false;
                     else
                        return true;
                  }

                  if ($valueType == "MAX") {
                     if ($boatLength < $filterValue)
                        return false;
                     else
                        return true;
                  }

                  if ($valueType == "EQL") {
                     if ($boatLength == $filterValue)
                        return false;
                     else
                        return true;
                  }
               }
            }

            if ($acceptance == "ACC") {
               if ($fieldType == "VAL") {
                  if ($valueType == "MIN") {
                     if ($boatLength < $filterValue)
                        return false;
                     else
                        return true;
                  }

                  if ($valueType == "MAX") {
                     if ($boatLength > $filterValue)
                        return false;
                     else
                        return true;
                  }

                  if ($valueType == "EQL") {
                     if ($boatLength == $filterValue)
                        return true;
                     else
                        return false;
                  }
               }
            }
         }

         //Year of manufacture	Standard select	Note: This filter is not applicable to Canoe / Kayak boat types.
         if ($fieldName == "year_built")
         {
            if ($boatType == "1")
               return true;
         }

         //Does your boat have an engine?	Standard select	Note: This filter is not applicable to Canoe / Kayak, PWC / Jeksti or Rowing boat types.
         //Cruising range / location	Standard select	Note: This filter is not applicable to Canoe / Kayak, PWC / Jeksti or Rowing boat types.
         //Would you like contents cover?	Standard select	Note: This filter is not applicable to Canoe / Kayak, PWC / Jeksti or Rowing boat types.
         if ($fieldName == "engine" || $fieldName == "cruising_range" || $fieldName == "boat_contents_cover")
         {
            $skippedBoatEngineArray = array("1", "7", "9");

            if (in_array($boatType, $skippedBoatEngineArray))
               return true;
         }

         //Engine horsepower (If known)	Standard integer, with min / max	Note: this filter is only applicable when the boat has an engine.
         //Maximum boat speed	Standard select	Note: this filter is only applicable when the boat has an engine.
         if ($fieldName == "boat_engine_horsepower" || $fieldName == "boat_speed")
         {
            $boatHasEngine = $session["_YourDetails_"]["engine"];

            if ($boatHasEngine == "" || $boatHasEngine == "3")
               return true;
         }

         //Type of PWC / Jetski	Standard select	Note: this filter only applies when PWC / Jetski is selected
         //Engine size (cc)	Standard integer, with min / max	Note: this filter only applies when PWC / Jetski is selected
         //Is your craft fitted with a Data Tag?	Standard select	Note: this filter only applies when PWC / Jetski is selected
         if ($fieldName == "type_of_pwc_jetsky" || $fieldName == "boat_engine_size" || $fieldName == "boat_data_tag")
         {
            if ($boatType != "7")
               return true;
         }

         //END boat filter details - questions that apply just to some of the boat types

      }
      

      //skp wl_id
      if($fieldName == "WLID")
      {
         $wlUserID = $_SESSION["_QZ_QUOTE_DETAILS_"]["wlUserID"];

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "EQL")
               {
                  if($wlUserID == $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
      }

      //skip affiliate_id
      if($fieldName == "AFFID")
      {
         $affiliateID = $_COOKIE["AFFID"];

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "EQL")
               {
                  if($affiliateID == $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
      }


      //start Consentment section of filters
      /*
      [consent_statements_values] => Array
      (
      [companyMarketing] => Array
          (
              [Email] => 
              [SMS] => 
          )

      [directMarketing] => Array
          (
              [Email] => 
          )
      )
      */
      //start MAIL CONSENT

      if($fieldName == "CONSENT_EMAIL")
      {
         $emailConsent = $_SESSION['consent_statements_values']['companyMarketing']['Email'];

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "EQL")
               {
                  if($emailConsent == $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }

         if($acceptance == "ACC")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "EQL")
               {
                  if($emailConsent == $filterValue)
                     return true;
                  else
                     return false;
               }
            }
         }
      }//end MAIL CONSENT


      //start SMS CONSENT
      if($fieldName == "CONSENT_SMS")
      {
         $smsConsent = $_SESSION['consent_statements_values']['companyMarketing']['SMS'];

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "EQL")
               {
                  if($smsConsent == $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }

         if($acceptance == "ACC")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "EQL")
               {
                  if($smsConsent == $filterValue)
                     return true;
                  else
                     return false;
               }
            }
         }
      }//end SMS CONSENT

      //end Consentment section of filters


      //date of insurance start intervals
      if($fieldName == "DIS")
      {
         $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
         $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
         $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

         $minDISDateFormat = date('Y-m-d',mktime(0,0,0,date('m'), date('d')+$filterValue, date('Y')));
         $minDISDateArr    = explode("-",$minDISDateFormat);

         $DIS        = mktime(0,0,0,$DISmm,$DISdd,$DISyyyy);
         $minDISDate = mktime(0,0,0,$minDISDateArr[1],$minDISDateArr[2],$minDISDateArr[0]);


         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($DIS <= $minDISDate)
                     return true;
                  else
                     return false;
               }

               if($valueType == "MAX")
               {
                  if($DIS > $minDISDate)
                     return true;
                  else
                     return false;
               }
            }
         }

         if($acceptance == "ACC")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MAX")
               {
                  if($DIS > $minDISDate)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MIN")
               {
                  if($DIS <= $minDISDate)
                     return true;
                  else
                     return false;
               }
            }
         }

      }//end DIS
      

      //ALEX AICI
      if($fieldName == "date_of_insurance_start_lg" AND $session['_QZ_QUOTE_DETAILS_']['system'] == "HEALTH")
      {

         $userEnteredValue = $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start'];

         if($acceptance == "SKP")
         {
            if($fieldType == "LIST")
            {
               foreach($filterValue as $hDISKey => $hDISVal)
               {
                  if($hDISVal == $userEnteredValue)
                     return false;
               }   
            }
         }

         if($acceptance == "ACC")
         {
            if($fieldType == "LIST")
            {               

               $filterFlag = true;

               foreach($filterValue as $hDISKey => $hDISVal)
               {
                  if($hDISVal == $userEnteredValue)
                     $filterFlag = false;
               }   

               if($filterFlag)
                  return false;
               else
                  return true;
            }
         }     
      }

      
      if($fieldName == "date_of_insurance_start_lg_new")
      {                   
         $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
         $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
         $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];
         $filterValueArray = explode("-",trim($filterValue));//first value is the smallest and the second is the biggest
         $minDISDate = mktime(0,0,0,date('m'), date('d')+$filterValueArray[0], date('Y'));
         $maxDISDate = mktime(0,0,0,date('m'), date('d')+$filterValueArray[1], date('Y'));
         $DIS        = mktime(0,0,0,$DISmm, $DISdd, $DISyyyy);
         
         
         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               
               if($DIS >= $minDISDate && $DIS <= $maxDISDate)
                  return false;
               else
                  return true;              
            }
         }

         if($acceptance == "ACC")
         {
            if($fieldType == "VAL")
            {               
               if($DIS >= $minDISDate && $DIS <= $maxDISDate)
                  return true;
               else
                  return false;               
            }
         }     
      }//end DIS_RANGE

      
      // motor_home_engine_cc_range
      if($fieldName == "motor_home_engine_cc_range")
      {
         $motorHomeEngineRange =  $_SESSION['_YourDetails_']['motor_home_engine_cc'];
         $filterValueArray = explode("-",trim($filterValue));//first value is the smallest and the second is the biggest

         if($acceptance == "SKP") //we skip the range
         {
            if((($motorHomeEngineRange > $filterValueArray[0]) && ($motorHomeEngineRange < $filterValueArray[1])) || (($motorHomeEngineRange == $filterValueArray[0]) && ($motorHomeEngineRange == $filterValueArray[1])))
               return false;
            else
               return true;
         }

         if($acceptance == "ACC") //we accept the range
         {
            if((($motorHomeEngineRange < $filterValueArray[0]) || ($motorHomeEngineRange > $filterValueArray[1])) || (($motorHomeEngineRange == $filterValueArray[0]) && ($motorHomeEngineRange == $filterValueArray[1])))
               return false;
            else
               return true;
         }

      } // end motor_home_engine_cc_range

      // vehicle_value_range
      if($fieldName == "vehicle_value_range")
      {
         $vehicleValue =  $_SESSION['_YourDetails_']['vehicle_value'];
         $filterValueArray = explode("-",trim($filterValue));//first value is the smallest and the second is the biggest

         if($acceptance == "SKP") //we skip the range
         {
            if((($vehicleValue > $filterValueArray[0]) && ($vehicleValue < $filterValueArray[1])) || (($vehicleValue == $filterValueArray[0]) && ($vehicleValue == $filterValueArray[1])))
               return false;
            else
               return true;
         }

         if($acceptance == "ACC") //we accept the range
         {
            if((($vehicleValue < $filterValueArray[0]) || ($vehicleValue > $filterValueArray[1])) || (($vehicleValue == $filterValueArray[0]) && ($vehicleValue == $filterValueArray[1])))
               return false;
            else
               return true;
         }

      } // end vehicle_value_range
		
		// purchase_price_range
      if($fieldName == "purchase_price_range")
      {
         $vehicleValue =  $_SESSION['_YourDetails_']['purchase_price'];
         $filterValueArray = explode("-",trim($filterValue));//first value is the smallest and the second is the biggest

         if($acceptance == "SKP") //we skip the range
         {
            if((($vehicleValue > $filterValueArray[0]) && ($vehicleValue < $filterValueArray[1])) || (($vehicleValue == $filterValueArray[0]) && ($vehicleValue == $filterValueArray[1])))
               return false;
            else
               return true;
         }

         if($acceptance == "ACC") //we accept the range
         {
            if((($vehicleValue < $filterValueArray[0]) || ($vehicleValue > $filterValueArray[1])) || (($vehicleValue == $filterValueArray[0]) && ($vehicleValue == $filterValueArray[1])))
               return false;
            else
               return true;
         }

      } // end vehicle_value_range
	  

	  

      // special postcode for Coversure Brentwood
      if($fieldName == "postcode_coversure")
      {
         $postCode = $session['_YourDetails_']['postcode'];
         $postCodePrefix = $session['_YourDetails_']['postcode_prefix'];


         preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
         $postcodeToCheck = $postCodeArr[1];

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if(in_array($postcodeToCheck,$filterValue) || in_array($postCodePrefix,$filterValue))
                  return false;
               else
                  return true;
            }


            if($acceptance == "ACC")
            {
               if(! in_array($postcodeToCheck,$filterValue) && (! in_array($postCodePrefix,$filterValue)))
                  return false;
               else
                  return true;
            }
         }
      }

	  if($fieldName == "secondary_postcodes")
      {
		 //$filterValue = $filterValue."[0-99]*";
         	//print_r($filterValue);
		  
			if($_SESSION['_QZ_QUOTE_DETAILS_']['system'] == "SALON")
			   $postCode = strtoupper($_SESSION['_YourDetails_']['business_postcode']);
		   
		    elseif($_SESSION['_QZ_QUOTE_DETAILS_']['system'] == "COURIER")
			   $postCode = strtoupper($_SESSION['_YourDetails_']['bussiness_postcode']);
			   
		    elseif($_SESSION['_QZ_QUOTE_DETAILS_']['system'] == "EVENT")
			   $postCode = strtoupper($_SESSION['_YourDetails_']['venue_postcode']);
			   
		    elseif($_SESSION['_QZ_QUOTE_DETAILS_']['system'] == "GOODSINTRANSIT")
			   $postCode = strtoupper($_SESSION['_YourDetails_']['bussiness_postcode']);
			   
		    elseif($_SESSION['_QZ_QUOTE_DETAILS_']['system'] == "HOTEL")
			   $postCode = strtoupper($_SESSION['_YourDetails_']['business_postcode']);
			   
		    elseif($_SESSION['_QZ_QUOTE_DETAILS_']['system'] == "MOTORFLEET")
			   $postCode = strtoupper($_SESSION['_YourDetails_']['bussiness_postcode']);
			   
		    elseif($_SESSION['_QZ_QUOTE_DETAILS_']['system'] == "MOTORTRADE")
			   $postCode = strtoupper($_SESSION['_YourDetails_']['bussiness_postcode']);
			   
		    elseif($_SESSION['_QZ_QUOTE_DETAILS_']['system'] == "OFFICE")
			   $postCode = strtoupper($_SESSION['_YourDetails_']['business_postcode']);
			   
		    elseif($_SESSION['_QZ_QUOTE_DETAILS_']['system'] == "PUB")
			   $postCode = strtoupper($_SESSION['_YourDetails_']['kept_postcode']);
			   
		    elseif($_SESSION['_QZ_QUOTE_DETAILS_']['system'] == "SHOP")
			   $postCode = strtoupper($_SESSION['_YourDetails_']['business_postcode']);
			   
		    elseif($_SESSION['_QZ_QUOTE_DETAILS_']['system'] == "SURGERY")
			   $postCode = strtoupper($_SESSION['_YourDetails_']['business_postcode']);
			   
		    elseif($_SESSION['_QZ_QUOTE_DETAILS_']['system'] == "TAXIFLEET")
			   $postCode = strtoupper($_SESSION['_YourDetails_']['taxi_rank_postcode']);
			   
		    elseif($_SESSION['_QZ_QUOTE_DETAILS_']['system'] == "TRUCK")
			   $postCode = strtoupper($_SESSION['_YourDetails_']['business_postcode']);
			   
		    elseif($_SESSION['_QZ_QUOTE_DETAILS_']['system'] == "CARAVAN")
			   $postCode = strtoupper($_SESSION['_YourDetails_']['caravan_postcode']);
			   
		    elseif($_SESSION['_QZ_QUOTE_DETAILS_']['system'] == "PARKHOME")
			   $postCode = strtoupper($_SESSION['_YourDetails_']['kept_postcode']);

         $postCode        = trim($postCode);

         $postCodeSuffix  = substr($postCode, -3);
         $postCodePreffix = trim(str_replace($postCodeSuffix,"",$postCode));



         $filter          = $acceptance;// skip or accept
         $postCodeArray   = $filterValue;
		 
		 
         $filterStatus = false;

         preg_match("/(.*)\d/isU",$postCodePreffix,$pregLettersToCheck);
         $lettersPostCodePreffix = $pregLettersToCheck[1];

         preg_match("/[0-9]+/",$postCodePreffix,$pregNumbersToCheck);
         $numbersPostCodePreffix = $pregNumbersToCheck[0];

         $prefLettAndNum = $lettersPostCodePreffix.$numbersPostCodePreffix;

         foreach($postCodeArray as $keypostCodeArray=>$skpPostcode)
         {
            if((preg_match("/[A-Z]*/",$skpPostcode,$pregMatchLetters)) && (preg_match("/\[(.*)\-/",$skpPostcode,$pregMinInterval)) && (preg_match("/\-(.*)\]/",$skpPostcode,$pregMaxInterval)))
            {
               $prefixLettersArr = $pregMatchLetters[0];
               $minIntervalArr   = $pregMinInterval[1];
               $maxIntervalArr   = $pregMaxInterval[1];
            }
            else
            {
               $prefixLettersArr = "";
               $minIntervalArr   = "";
               $maxIntervalArr   = "";
            }

			/*print "<pre>";
			print $lettersPostCodePreffix;
			print "<br>";
			print $prefixLettersArr; */
             
            //checking postcodes like BT[1-99]*
            if((preg_match("/\*/isU",$skpPostcode,$pregMatchAll)))
            {
               if(($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
               {
                  //"postcode has been accepted \n"; //only accept list
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes like BT[1-99]
            elseif(preg_match("/^[A-Z]{1,2}\[[0-9]{1,2}-[0-9]{1,2}\]$/isU",$skpPostcode,$pregMatchAll))
            {
               if(($postCodePreffix == $prefLettAndNum) && ($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
               {
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes like BT3 9
            elseif((preg_match("/(.*)\s+/isU",$skpPostcode, $pregMatchSpace)))
            {
            $sufixNumber = substr($postCodeSuffix,0,-2);
            $postCodeToCheck = $postCodePreffix." ".$sufixNumber;

               if($skpPostcode == $postCodeToCheck)
               {
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes like BT339AA OR BT39AA
            elseif(ctype_alpha(substr($skpPostcode,-2)))
            {
            $postCodeStrip = str_replace(" ","",$postCode);

               if($skpPostcode == $postCodeStrip)
               {
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes preffix like BT1, BT1A
            elseif((preg_match("/^$postCodePreffix$/",$skpPostcode)))
            {
               if($postCodePreffix == $skpPostcode);
               {
                  $filterStatus = true;
                  break;
               }
            }
         }
		 
         if($filter == "ACC")
         {  
	        if(empty($postCode) || $postCode == "")
			{
				return true;
			}
			else
			{
				if($filterStatus == false)
				   return false;
				else
				   return true;
			}
         }
         elseif($filter == "SKP")
         {
            if($filterStatus==true)
               return false;
            else
               return true;
         }

      }

	  
	  
	  if($fieldName == "postcode_sk")
      {
   		$postCode = $session['_YourDetails_']['postcode'];
   	 
   		//accept/skip the following postcodes
   		$postCode        = $_SESSION['_YourDetails_']['postcode'];
       	$postCodePreffix = $_SESSION['_YourDetails_']['postcode_prefix'];
   		$postCodeSuffix  = $_SESSION['_YourDetails_']['postcode_number'];

         $filter          = $acceptance;// skip or accept
         $postCodeArray   = $filterValue;

         $filterStatus = "";

         preg_match("/(.*)\d/isU",$postCodePreffix,$pregLettersToCheck);
         $lettersPostCodePreffix = $pregLettersToCheck[1];

         preg_match("/[0-9]+/",$postCodePreffix,$pregNumbersToCheck);
         $numbersPostCodePreffix = $pregNumbersToCheck[0];

         $prefLettAndNum = $lettersPostCodePreffix.$numbersPostCodePreffix;

         foreach($postCodeArray as $keypostCodeArray=>$skpPostcode)
         {
            if((preg_match("/[A-Z]*/",$skpPostcode,$pregMatchLetters)) && (preg_match("/\[(.*)\-/",$skpPostcode,$pregMinInterval)) && (preg_match("/\-(.*)\]/",$skpPostcode,$pregMaxInterval)))
            {
               $prefixLettersArr = $pregMatchLetters[0];
               $minIntervalArr   = $pregMinInterval[1];
               $maxIntervalArr   = $pregMaxInterval[1];
            }
            else
            {
               $prefixLettersArr = "";
               $minIntervalArr   = "";
               $maxIntervalArr   = "";
            }


            //checking postcodes like BT[1-99]*
            if((preg_match("/\*/isU",$skpPostcode,$pregMatchAll)))
            {
               if(($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
               {
                  //"postcode has been accepted \n"; //only accept list
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes like BT[1-99]
            elseif(preg_match("/^[A-Z]{1,2}\[[0-9]{1,2}-[0-9]{1,2}\]$/isU",$skpPostcode,$pregMatchAll))
            {

               if(($postCodePreffix == $prefLettAndNum) && ($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
               {
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes like BT3 9
            elseif((preg_match("/(.*)\s+/isU",$skpPostcode, $pregMatchSpace)))
            {
            $sufixNumber = substr($postCodeSuffix,0,-2);
            $postCodeToCheck = $postCodePreffix." ".$sufixNumber;

               if($skpPostcode == $postCodeToCheck)
               {
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes like BT339AA OR BT39AA
            elseif(ctype_alpha(substr($skpPostcode,-2)))
            {
            $postCodeStrip = str_replace(" ","",$postCode);

               if($skpPostcode == $postCodeStrip)
               {
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes preffix like BT1, BT1A
            elseif((preg_match("/^$postCodePreffix$/",$skpPostcode)))
            {
               if($postCodePreffix == $skpPostcode)
               {
                  $filterStatus = true;
                  break;
               }
            }
         }

         if($filter == "ACC")
         {
		   
            if($filterStatus == false)
               return false;
            else
               return true;
         }
         elseif($filter == "SKP")
         {
            if($filterStatus==true)
               return false;
            else
               return true;
         }
      }

	  if($fieldName == "entire_caravan_postcode")
      {
		$postCode = $session['_YourDetails_']['caravan_postcode'];
	 
		//accept/skip the following postcodes
		$postCode        = $_SESSION['_YourDetails_']['caravan_postcode'];
		$postCodePreffix = $_SESSION['_YourDetails_']['caravan_postcode_prefix'];
		$postCodeSuffix  = $_SESSION['_YourDetails_']['caravan_postcode_number'];
	 
		 
         $filter          = $acceptance;// skip or accept
         $postCodeArray   = $filterValue;

         $filterStatus = "";
		
		if(in_array($postCode, $postCodeArray))
			$filterStatus = true;
		else
			$filterStatus = false;

         if($filter == "ACC")
         {
            if($filterStatus == false)
               return false;
            else
               return true;
         }
         elseif($filter == "SKP")
         {
            if($filterStatus==true)
               return false;
            else
               return true;
         }
      }
	  
		if($fieldName == "kept_postcode")
      {
         $postCode = $session['_YourDetails_']['kept_postcode'];
         
         
         //accept/skip the following postcodes
         $postCode        = $_SESSION['_YourDetails_']['kept_postcode'];
         $postCodePreffix = $_SESSION['_YourDetails_']['kept_postcode_prefix'];
         $postCodeSuffix  = $_SESSION['_YourDetails_']['kept_postcode_number'];

         $filter          = $acceptance;// skip or accept
         $postCodeArray   = $filterValue;

         $filterStatus = "";

         preg_match("/(.*)\d/isU",$postCodePreffix,$pregLettersToCheck);
         $lettersPostCodePreffix = $pregLettersToCheck[1];

         preg_match("/[0-9]+/",$postCodePreffix,$pregNumbersToCheck);
         $numbersPostCodePreffix = $pregNumbersToCheck[0];

         $prefLettAndNum = $lettersPostCodePreffix.$numbersPostCodePreffix;

         foreach($postCodeArray as $keypostCodeArray=>$skpPostcode)
         {
            if((preg_match("/[A-Z]*/",$skpPostcode,$pregMatchLetters)) && (preg_match("/\[(.*)\-/",$skpPostcode,$pregMinInterval)) && (preg_match("/\-(.*)\]/",$skpPostcode,$pregMaxInterval)))
            {
               $prefixLettersArr = $pregMatchLetters[0];
               $minIntervalArr   = $pregMinInterval[1];
               $maxIntervalArr   = $pregMaxInterval[1];
            }
            else
            {
               $prefixLettersArr = "";
               $minIntervalArr   = "";
               $maxIntervalArr   = "";
            }


            //checking postcodes like BT[1-99]*
            if((preg_match("/\*/isU",$skpPostcode,$pregMatchAll)))
            {
               if(($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
               {
                  //"postcode has been accepted \n"; //only accept list
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes like BT[1-99]
            elseif(preg_match("/^[A-Z]{1,2}\[[0-9]{1,2}-[0-9]{1,2}\]$/isU",$skpPostcode,$pregMatchAll))
            {
               if(($postCodePreffix == $prefLettAndNum) && ($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
               {
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes like BT3 9
            elseif((preg_match("/(.*)\s+/isU",$skpPostcode, $pregMatchSpace)))
            {
            $sufixNumber = substr($postCodeSuffix,0,-2);
            $postCodeToCheck = $postCodePreffix." ".$sufixNumber;

               if($skpPostcode == $postCodeToCheck)
               {
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes like BT339AA OR BT39AA
            elseif(ctype_alpha(substr($skpPostcode,-2)))
            {
            $postCodeStrip = str_replace(" ","",$postCode);

               if($skpPostcode == $postCodeStrip)
               {
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes preffix like BT1, BT1A
            elseif((preg_match("/^$postCodePreffix$/",$skpPostcode)))
            {
               if($postCodePreffix == $skpPostcode);
               {
                  $filterStatus = true;
                  break;
               }
            }
         }

         if($filter == "ACC")
         {
            if($filterStatus == false)
               return false;
            else
               return true;
         }
         elseif($filter == "SKP")
         {
            if($filterStatus==true)
               return false;
            else
               return true;
         }
      }
		
      
      if($fieldName == "business_postcode_shop")
      {
         //accept/skip the following postcodes
         $postCode        = trim($_SESSION['_YourDetails_']['business_postcode']);
                 
         $postCodeSuffix  = substr($postCode, -3);
         $postCodePreffix = trim(str_replace($postCodeSuffix,"",$postCode)); 
         
         

         $filter          = $acceptance;// skip or accept
         $postCodeArray   = $filterValue;

         $filterStatus = "";

         preg_match("/(.*)\d/isU",$postCodePreffix,$pregLettersToCheck);
         $lettersPostCodePreffix = $pregLettersToCheck[1];

         preg_match("/[0-9]+/",$postCodePreffix,$pregNumbersToCheck);
         $numbersPostCodePreffix = $pregNumbersToCheck[0];

         $prefLettAndNum = $lettersPostCodePreffix.$numbersPostCodePreffix;

         foreach($postCodeArray as $keypostCodeArray=>$skpPostcode)
         {
            if((preg_match("/[A-Z]*/",$skpPostcode,$pregMatchLetters)) && (preg_match("/\[(.*)\-/",$skpPostcode,$pregMinInterval)) && (preg_match("/\-(.*)\]/",$skpPostcode,$pregMaxInterval)))
            {
               $prefixLettersArr = $pregMatchLetters[0];
               $minIntervalArr   = $pregMinInterval[1];
               $maxIntervalArr   = $pregMaxInterval[1];
            }
            else
            {
               $prefixLettersArr = "";
               $minIntervalArr   = "";
               $maxIntervalArr   = "";
            }


            //checking postcodes like BT[1-99]*
            if((preg_match("/\*/isU",$skpPostcode,$pregMatchAll)))
            {
               if(($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
               {
                  //"postcode has been accepted \n"; //only accept list
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes like BT[1-99]
            elseif(preg_match("/^[A-Z]{1,2}\[[0-9]{1,2}-[0-9]{1,2}\]$/isU",$skpPostcode,$pregMatchAll))
            {
               if(($postCodePreffix == $prefLettAndNum) && ($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
               {
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes like BT3 9
            elseif((preg_match("/(.*)\s+/isU",$skpPostcode, $pregMatchSpace)))
            {
            $sufixNumber = substr($postCodeSuffix,0,-2);
            $postCodeToCheck = $postCodePreffix." ".$sufixNumber;

               if($skpPostcode == $postCodeToCheck)
               {
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes like BT339AA OR BT39AA
            elseif(ctype_alpha(substr($skpPostcode,-2)))
            {
            $postCodeStrip = str_replace(" ","",$postCode);

               if($skpPostcode == $postCodeStrip)
               {
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes preffix like BT1, BT1A
            elseif((preg_match("/^$postCodePreffix$/",$skpPostcode)))
            {
               if($postCodePreffix == $skpPostcode)
               {
                  $filterStatus = true;
                  break;
               }
            }
         }

         if($filter == "ACC")
         {
            if($filterStatus == false)
               return false;
            else
               return true;
         }
         elseif($filter == "SKP")
         {
            if($filterStatus==true)
               return false;
            else
               return true;
         }
      }
		      
      
		if($fieldName == "bussiness_postcode")
      {
         $postCode = $session['_YourDetails_']['bussiness_postcode'];

         //accept/skip the following postcodes
         $postCode        = $_SESSION['_YourDetails_']['bussiness_postcode'];
         $postCodePreffix = $_SESSION['_YourDetails_']['bussiness_postcode_prefix'];
         $postCodeSuffix  = $_SESSION['_YourDetails_']['bussiness_postcode_number'];

         $filter          = $acceptance;// skip or accept
         $postCodeArray   = $filterValue;

         $filterStatus = "";

         preg_match("/(.*)\d/isU",$postCodePreffix,$pregLettersToCheck);
         $lettersPostCodePreffix = $pregLettersToCheck[1];

         preg_match("/[0-9]+/",$postCodePreffix,$pregNumbersToCheck);
         $numbersPostCodePreffix = $pregNumbersToCheck[0];

         $prefLettAndNum = $lettersPostCodePreffix.$numbersPostCodePreffix;

         foreach($postCodeArray as $keypostCodeArray=>$skpPostcode)
         {
            if((preg_match("/[A-Z]*/",$skpPostcode,$pregMatchLetters)) && (preg_match("/\[(.*)\-/",$skpPostcode,$pregMinInterval)) && (preg_match("/\-(.*)\]/",$skpPostcode,$pregMaxInterval)))
            {
               $prefixLettersArr = $pregMatchLetters[0];
               $minIntervalArr   = $pregMinInterval[1];
               $maxIntervalArr   = $pregMaxInterval[1];
            }
            else
            {
               $prefixLettersArr = "";
               $minIntervalArr   = "";
               $maxIntervalArr   = "";
            }


            //checking postcodes like BT[1-99]*
            if((preg_match("/\*/isU",$skpPostcode,$pregMatchAll)))
            {
               if(($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
               {
                  //"postcode has been accepted \n"; //only accept list
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes like BT[1-99]
            elseif(preg_match("/^[A-Z]{1,2}\[[0-9]{1,2}-[0-9]{1,2}\]$/isU",$skpPostcode,$pregMatchAll))
            {
               if(($postCodePreffix == $prefLettAndNum) && ($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
               {
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes like BT3 9
            elseif((preg_match("/(.*)\s+/isU",$skpPostcode, $pregMatchSpace)))
            {
            $sufixNumber = substr($postCodeSuffix,0,-2);
            $postCodeToCheck = $postCodePreffix." ".$sufixNumber;

               if($skpPostcode == $postCodeToCheck)
               {
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes like BT339AA OR BT39AA
            elseif(ctype_alpha(substr($skpPostcode,-2)))
            {
            $postCodeStrip = str_replace(" ","",$postCode);

               if($skpPostcode == $postCodeStrip)
               {
                  $filterStatus = true;
                  break;
               }
            }

            //checking postcodes preffix like BT1, BT1A
            elseif((preg_match("/^$postCodePreffix$/",$skpPostcode)))
            {
               if($postCodePreffix == $skpPostcode);
               {
                  $filterStatus = true;
                  break;
               }
            }
         }

         if($filter == "ACC")
         {
            if($filterStatus == false)
               return false;
            else
               return true;
         }
         elseif($filter == "SKP")
         {
            if($filterStatus==true)
               return false;
            else
               return true;
         }
      }
		
		
      if($fieldName == "postcode_all")
      {
         $postCode = $session['_YourDetails_']['postcode'];

         preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
         $postcodeToCheck = $postCodeArr[1];

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if(in_array($postcodeToCheck,$filterValue))
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {
               if(! in_array($postcodeToCheck,$filterValue))
                  return false;
               else
                  return true;
            }
         }
      }


      // caravan postcode
      if($fieldName == "caravan_postcode")
      {
         $postCode = $session['_YourDetails_']['caravan_postcode'];

         preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
         $postcodeToCheck = $postCodeArr[1];

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if(in_array($postcodeToCheck,$filterValue))
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {
               if(! in_array($postcodeToCheck,$filterValue))
                  return false;
               else
                  return true;
            }
         }
      }


      // bussiness postcode
      if($fieldName == "bussiness_postcode")
      {
         $postCode = $session['_YourDetails_']['bussiness_postcode'];

         preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
         $postcodeToCheck = $postCodeArr[1];

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if(in_array($postcodeToCheck,$filterValue))
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {
               if(! in_array($postcodeToCheck,$filterValue))
                  return false;
               else
                  return true;
            }
         }
      }


      if($fieldName == "postcode_prefix")
      {
         $postCodePrefix = $session['_YourDetails_']['postcode_prefix'];

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if(in_array($postCodePrefix,$filterValue))
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {
               if(! in_array($postCodePrefix,$filterValue))
                  return false;
               else
                  return true;
            }
         }
      }


      //this filter is used on postcodes like B[1-40] and can be used on filters like B[1-40] B[51-55].
      //the values will be set in an array like so "B"=>"1-40;51-55"
      if($fieldName == "postcode_prefix_interval")
      {
         $postCodePrefix = $session['_YourDetails_']['postcode_prefix'];

         preg_match("/^(.*)\d/isU",$postCodePrefix,$postCodePrefArr);
         $postcodePrefLetters = $postCodePrefArr[1];
         $postcodeNumber      = str_replace("$postcodePrefLetters","",$postCodePrefix);

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               foreach($filterValue as $pcLetters => $pcInterval)
               {
                  if($pcLetters == $postcodePrefLetters)
                  {
                     if(preg_match("/\;/isU",$pcInterval))
                     {
                        $intValPcArray = explode(";",$pcInterval);

                        foreach($intValPcArray as $cnt => $pcIntervalPart)
                        {
                           $intervalDetailsArray = explode("-",$pcIntervalPart);
                           $minValue             = $intervalDetailsArray[0];
                           $maxValue             = $intervalDetailsArray[1];

                           if(($postcodeNumber >= $minValue) && ($postcodeNumber <= $maxValue))
                              return false;
                        }

                        return true;
                     }
                     else
                     {
                        $intervalDetailsArray = explode("-",$pcInterval);
                        $minValue             = $intervalDetailsArray[0];
                        $maxValue             = $intervalDetailsArray[1];

                        if(($postcodeNumber >= $minValue) && ($postcodeNumber <= $maxValue))
                           return false;
                        else
                           return true;
                     }
                  }
               }
            }

            if($acceptance == "ACC")
            {
               foreach($filterValue as $pcLetters => $pcInterval)
               {
                  if($pcLetters == $postcodePrefLetters)
                  {
                     if(preg_match("/\;/isU",$pcInterval))
                     {
                        $intValPcArray = explode(";",$pcInterval);

                        foreach($intValPcArray as $cnt => $pcIntervalPart)
                        {
                           $intervalDetailsArray = explode("-",$pcIntervalPart);
                           $minValue             = $intervalDetailsArray[0];
                           $maxValue             = $intervalDetailsArray[1];

                           if(($postcodeNumber < $minValue) || ($postcodeNumber > $maxValue))
                              return false;
                        }

                        return true;
                     }
                     else
                     {
                        $intervalDetailsArray = explode("-",$pcInterval);
                        $minValue             = $intervalDetailsArray[0];
                        $maxValue             = $intervalDetailsArray[1];

                        if(($postcodeNumber < $minValue) || ($postcodeNumber > $maxValue))
                           return false;
                        else
                           return true;
                     }
                  }
               }
            }
         }
      }


      //this will filter postcodes based on prefix and sufix start FORMAT PREFIX-SUFIX_start. eg:OL13-8
      if($fieldName == "postcode_prefix_sufix")
      {
         $postCode       = $session['_YourDetails_']['postcode'];
         $postCodePrefix = $session['_YourDetails_']['postcode_prefix'];
         $postCodeSufix  = $session['_YourDetails_']['postcode_number'];

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               foreach($filterValue as $key => $filterPCValue)
               {
                  $postcodeToFilterArray = explode("-",$filterPCValue);

                  $filterValPrefix = $postcodeToFilterArray[0];
                  $filterValSufix  = $postcodeToFilterArray[1];

                  if($filterValPrefix == $postCodePrefix)
                  {
                     if(preg_match("/^$filterValSufix/i",$postCodeSufix))
                     {
                        return false;
                     }
                  }
               }

               return true;

            }

            if($acceptance == "ACC")
            {
               $pcIsFiltered = true;

               foreach($filterValue as $key => $filterPCValue)
               {
                  $postcodeToFilterArray = explode("-",$filterPCValue);

                  $filterValPrefix = $postcodeToFilterArray[0];
                  $filterValSufix  = $postcodeToFilterArray[1];

                  if($filterValPrefix == $postCodePrefix)
                  {
                     if(preg_match("/^$filterValSufix/i",$postCodeSufix))
                     {
                        $pcIsFiltered = false;
                     }
                  }
               }

               if($pcIsFiltered)
                  return false;
               else
                  return true;
            }
         }
      } //end this will filter postcodes based on prefix and sufix start FORMAT PREFIX-SUFIX_start. eg:OL13-8


 
      if($fieldName == "time")
      { 
         if($session['QA_test_request']["time"] != "")
         {
            $nowTime  = $session['QA_test_request']["time"];
            $todayDay = $session['QA_test_request']["time_day"];
         }
         else
         {
            $nowTime    = mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"));
            $todayDay   = date("N");
         }

         //we need to get the filter values -> this is an array
         if($fieldType == "LIST")
         {
            if($acceptance == "ACC")
            {
               foreach($filterValue as $day => $timeLimitsDetails)
               {
                  if($day == $todayDay)
                  {
                     if($timeLimitsDetails == "SKP_ALL_DAY")
                        return false;

                     //set here cases where we have more time intervals each day
                     if(preg_match("/\|/isU",$timeLimitsDetails))
                     {
                        $multiTimeIntervalPerDay = explode("|",$timeLimitsDetails);

                        foreach($multiTimeIntervalPerDay as $cntT => $multiTimeLimitDetails)
                        {
                           $timeLimitsDetailsArray = explode("-",$multiTimeLimitDetails);
                           $startTime = $timeLimitsDetailsArray[0];

                           $startTimeArr = explode(":",$startTime);
                           $startTimeHoure = $startTimeArr[0];
                           $startTimeMin   = $startTimeArr[1];
                           $startTimeSec   = "0";
                           if(($startTimeHoure == "23") AND ($startTimeMin == "59"))
                              $startTimeSec = "59";

                           $startMkTime = mktime($startTimeHoure,$startTimeMin,$startTimeSec,date("m"),date("d"),date("Y"));

                           $endTime   = $timeLimitsDetailsArray[1];

                           $endTimeArr = explode(":",$endTime);
                           $endTimeHoure = $endTimeArr[0];
                           $endTimeMin   = $endTimeArr[1];
                           $endTimeSec   = "0";
                           if(($endTimeHoure == "23") AND ($endTimeMin == "59"))
                              $endTimeSec = "59";

                           $endMkTime = mktime($endTimeHoure,$endTimeMin,$endTimeSec,date("m"),date("d"),date("Y"));

                           if(($nowTime >= $startMkTime) && ($nowTime <= $endMkTime))
                              return true;
                        }

                        return false;
                     }
                     else
                     {

                        $timeLimitsDetailsArray = explode("-",$timeLimitsDetails);
                        $startTime = $timeLimitsDetailsArray[0];

                        $startTimeArr = explode(":",$startTime);
                        $startTimeHoure = $startTimeArr[0];
                        $startTimeMin   = $startTimeArr[1];
                        $startTimeSec   = "0";
                        if(($startTimeHoure == "23") AND ($startTimeMin == "59"))
                           $startTimeSec = "59";


                        $startMkTime = mktime($startTimeHoure,$startTimeMin,$startTimeSec,date("m"),date("d"),date("Y"));

                        $endTime   = $timeLimitsDetailsArray[1];

                        $endTimeArr = explode(":",$endTime);
                        $endTimeHoure = $endTimeArr[0];
                        $endTimeMin   = $endTimeArr[1];
                        $endTimeSec   = "0";
                        if(($endTimeHoure == "23") AND ($endTimeMin == "59"))
                           $endTimeSec = "59";


                        $endMkTime = mktime($endTimeHoure,$endTimeMin,$endTimeSec,date("m"),date("d"),date("Y"));

                        if(($nowTime < $startMkTime) || ($nowTime > $endMkTime))
                           return false;
                        else
                           return true;
                     }
                  }
               }
            }
         }
      }//end if($fieldName == "time")

      if($fieldName == "multi_car_estimated_value_accept_at_least_one")
      {
         if($fieldType == "VAL")
         {
            if($acceptance == "ACC")
            {
               if($valueType == "MIN")
               {
                  $willFilter = 1;
                  $noOfVehicles = $session["_YourDetails_"]["number_of_vehicles"];

                  for($i=1;$i<=$noOfVehicles;$i++)
                  {
                     $vehValue = $session["_VehicleDetails_"]["estimated_value_$i"];
                     $vehValue = str_replace(",","",$vehValue);

                     if($vehValue > $filterValue)
                        $willFilter = 0;
                  }

                  if($willFilter == 1)
                     return false;
                  else
                     return true;
               }
            }
         }
      }
      
      
      if($fieldName == "multi_car_estimated_value")
      {
         if($fieldType == "VAL")
         {
            if($acceptance == "SKP")
            {
               if($valueType == "MAX")
               {
                  $noOfVehicles = $session["_YourDetails_"]["number_of_vehicles"];
                  $flag = "accept";
                  for($i=1;$i<=$noOfVehicles;$i++)
                  {
                     $vehValue = $session["_VehicleDetails_"]["estimated_value_$i"];
                     $vehValue = str_replace(",","",$vehValue);
               
                     if($vehValue < $filterValue)
                     {
                        $flag = "skip";
                        break;
                     }            
                  }
                  
               if($flag == "skip")
                  return false;
               else
                  return true;
               }              
            }
         }
      }
      
      
      if($fieldName == "multicar_year_of_manufacture_accept_at_least_one")
      {
         $noOfVehicles = $session["_YourDetails_"]["number_of_vehicles"];
         $DISDateYear  = $session["_YourDetails_"]["date_of_insurance_start_yyyy"];
         
         if($fieldType == "VAL")
         {
            if($acceptance == "ACC")
            {
               if($valueType == "MIN")
               {
                  $willFilter = 1;
                  

                  for($i=1;$i<=$noOfVehicles;$i++)
                  {
                     $yearOfManufacture   = $session["_VehicleDetails_"]["year_of_manufacture_$i"];
                     $vehicleAge          = $DISDateYear - $yearOfManufacture;
                     
                     if($vehicleAge >= $filterValue)
                     {
                        $willFilter = 0;
                        break;
                     }
                  }

                  if($willFilter == 1)
                     return false;
                  else
                     return true;
               }
            }
         }
      }
 
      // abi code general filter

      if($fieldName == "abicode_filter")
      {
      $path = getcwd();
         include_once "../../modules/Vehicle.php";
         include_once "./modules/Vehicle.php";

         $vehicleObj = new CVehicle();
         $vehicleCode  = $_SESSION['_YourDetails_']['vehicle_confirm'];
         $vehicleGroup = $vehicleObj->GetVehicleDetailsGroup50($vehicleCode);

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($vehicleGroup < $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MAX")
               {
                  if($vehicleGroup > $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
      }
      
      if($fieldName == "multicar_veh_group")
      {
         include_once "../../modules/Vehicle.php";
         include_once "./modules/Vehicle.php";
   
         $vehicleObj = new CVehicle();

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {              
               if($valueType == "MAX")
               {
                  $flag = "accept";
                  
                  for($i=1;$i<=10;$i++)
                  {
                     $vehicleCode  = $_SESSION['_VehicleDetails_']['vehicle_confirm_'.$i];
                     $vehicleGroup = $vehicleObj->GetVehicleDetailsGroup50($vehicleCode);
                     
                     if(!$vehicleGroup)
                        continue;                 
                   
                     if($vehicleGroup < $filterValue)
                     {                                           
                        $flag = "skip";   
                           break;
                     } 

                  }                 
                     if($flag == "skip")
                        return false;
                     else
                        return true;                       
               }
            }
         }      
      }
      
      
      if($fieldName == "multicar_veh_group_veh_One")
      {
         include_once "../../modules/Vehicle.php";
         include_once "./modules/Vehicle.php";
   
         $vehicleObj    = new CVehicle();
         $vehicleCode   = $_SESSION['_VehicleDetails_']['vehicle_confirm_1'];
         $vehicleGroup  = $vehicleObj->GetVehicleDetailsGroup50($vehicleCode);

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {              
               if($valueType == "MAX")
               {
                     if(($vehicleGroup < $filterValue) && ($vehicleGroup >= 1))
                        return false;
                     else
                        return true;                       
               }
            }
         }      
      }
      
      if($fieldName == "GROUP_50")
      {
         include_once "../../modules/Vehicle.php";
         include_once "./modules/Vehicle.php";
   
         $vehicleObj    = new CVehicle();
         $vehicleCode   = $_SESSION['_YourDetails_']['vehicle_confirm'];
         $vehicleGroup  = $vehicleObj->GetVehicleDetailsGroup50($vehicleCode);

         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {              
               if($valueType == "MAX")
               {
                     if($vehicleGroup < $filterValue)
                        return false;
                     else
                        return true;                       
               }
            }
         } 
                          
         if($acceptance == "ACC")
         {
            if($fieldType == "VAL")
            {              
               if($valueType == "MAX")
               {               
                     if($vehicleGroup > $filterValue)
                        return false;
                     else
                        return true;                       
               }
            }
         }         
      }      

      // SKIP if "Number of Convictions" < 'numeric val'
      if($fieldName == "number_of_convictions")
      {
         if($fieldType == "VAL")
         {
            if($acceptance == "SKP")
            {
               if($valueType == "MAX")
               {                 
                  $totalConvNumber     = 0;
                  // CONVICITONS
                     for($k=1;$k<=4;$k++)
                     {
                        if($session['_YourDetails_']["conviction_type_".$k] AND $session['_YourDetails_']["date_of_conviction_".$k])
                           $totalConvNumber++;
                     }   

                     if($totalConvNumber < $filterValue)
                        return false;
                  else
                     return true;
                        
                  }
               }
            }
         if($fieldType == "VAL")
         {
            if($acceptance == "SKP")
            {
               if($valueType == "MIN")
               {                 
                  $totalConvNumber     = 0;
                  // CONVICITONS
                     for($k=1;$k<=4;$k++)
                     {
                        if($session['_YourDetails_']["conviction_type_".$k] AND $session['_YourDetails_']["date_of_conviction_".$k])
                           $totalConvNumber++;
                     }   

                     if($totalConvNumber > $filterValue)
                        return false;
                  else
                     return true;
                        
                  }
               }
            }
         }
         
         
      if($fieldName == "TOTAL_NUMBER_POINTS")
      {
         for($i=1; $i<=4; $i++)
         {
            $pointsReceived       = $session["_YourDetails_"]["points_received_".$i];
            $totalPointsReceived += $pointsReceived;
         }         
         
         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($totalPointsReceived < $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MAX")
               {
                  if($totalPointsReceived > $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }

         if($acceptance == "ACC")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MAX")
               {
                  if($totalPointsReceived > $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MIN")
               {
                  if($totalPointsReceived < $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
      }

      // estimated value
      if($fieldName == "estimated_value")
      {
         $estimatedValue = $session['_YourDetails_']['estimated_value'];
         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($estimatedValue > $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MAX")
               {
                  if($estimatedValue < $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
      } // estimated value

      if($fieldName == "driver_age_1")
      {
         $driverAge1Value = $session['_YourDetails_']['driver_age_1'];
         if($acceptance == "SKP")
         {
            if($fieldType == "VAL")
            {
               if($valueType == "MIN")
               {
                  if($driverAge1Value > $filterValue)
                     return false;
                  else
                     return true;
               }

               if($valueType == "MAX")
               {
                  if($driverAge1Value < $filterValue)
                     return false;
                  else
                     return true;
               }
            }
         }
      }

      if($fieldName == "driver_age_2")
      {
         $driverAge2Value = $session['_YourDetails_']['driver_age_2'];
         if(!empty($driverAge2Value))
         {
            if($acceptance == "SKP")
            {
               if($fieldType == "VAL")
               {
                  if($valueType == "MIN")
                  {
                     if($driverAge2Value > $filterValue)
                        return false;
                     else
                        return true;
                  }

                  if($valueType == "MAX")
                  {
                     if($driverAge2Value < $filterValue)
                        return false;
                     else
                        return true;
                  }
               }
            }
         }
         else
            return true;
      }

      if($fieldName == "driver_age_3")
      {
         $driverAge3Value = $session['_YourDetails_']['driver_age_3'];
         if(!empty($driverAge3Value))
         {
            if($acceptance == "SKP")
            {
               if($fieldType == "VAL")
               {
                  if($valueType == "MIN")
                  {
                     if($driverAge3Value > $filterValue)
                        return false;
                     else
                        return true;
                  }

                  if($valueType == "MAX")
                  {
                     if($driverAge3Value < $filterValue)
                        return false;
                     else
                        return true;
                  }
               }
            }
         }
         else
            return true;
      }

      if($fieldName == "driver_age_4")
      {
         $driverAge4Value = $session['_YourDetails_']['driver_age_4'];
         if(!empty($driverAge4Value))
         {
            if($acceptance == "SKP")
            {
               if($fieldType == "VAL")
               {
                  if($valueType == "MIN")
                  {
                     if($driverAge4Value > $filterValue)
                        return false;
                     else
                        return true;
                  }

                  if($valueType == "MAX")
                  {
                     if($driverAge4Value < $filterValue)
                        return false;
                     else
                        return true;
                  }
               }
            }
         }
         else
            return true;
      }

      if($fieldName == "driver_age_5")
      {
         $driverAge5Value = $session['_YourDetails_']['driver_age_5'];
         if(!empty($driverAge5Value))
         {
            if($acceptance == "SKP")
            {
               if($fieldType == "VAL")
               {
                  if($valueType == "MIN")
                  {
                     if($driverAge5Value > $filterValue)
                        return false;
                     else
                        return true;
                  }

                  if($valueType == "MAX")
                  {
                     if($driverAge5Value < $filterValue)
                        return false;
                     else
                        return true;
                  }
               }
            }
         }
         else
            return true;
      }

      // conviction type 1 2 3 4 filter
      if($fieldName == "conviction_type_1_2_3_4")
      {
         if($fieldType == "LIST")
         {
            if($acceptance == "ACC")
            {

               $willFilter = 1;

               for ($i = 1; $i<=4; $i++)
               {
                  $convictionType = "";

                  $convictionType =  $_SESSION['_YourDetails_']["conviction_type_$i"];

                  if($_SESSION['_YourDetails_']["conviction_type_$i"])
                  {
                     if(! in_array($convictionType,$filterValue))
                        $willFilter = 0;
                  }
               }

               if($willFilter == 1)
                  return true;
               else
                  return false;
            }


            if($acceptance == "SKP")
            {
               $willFilter = 1;

               for ($i = 1; $i<=4; $i++)
               {
                  $convictionType = "";

                  $convictionType =  $_SESSION['_YourDetails_']["conviction_type_$i"];

                  if($_SESSION['_YourDetails_']["conviction_type_$i"])
                  {
                     if(in_array($convictionType,$filterValue))
                        $willFilter = 0;
                  }
               }

               if($willFilter == 1)
                  return true;
               else
                  return false;
            }
         }
      }
      
      // conviction type 1 2 3 4 filter ACCEPT IF ONE CONFICTION IS FOUND
      if($fieldName == "one_conviction_type_1_2_3_4")
      {
         if($fieldType == "LIST")
         {
            if($acceptance == "ACC")
            {

               $willFilter = 1;

               for ($i = 1; $i<=4; $i++)
               {
                  $convictionType = "";

                  $convictionType =  $_SESSION['_YourDetails_']["conviction_type_$i"];

                  if($_SESSION['_YourDetails_']["conviction_type_$i"])
                  {
                     if(in_array($convictionType,$filterValue))
                        return true;
                        
                  }
               }             
                  
                  return false;
            }
         }
      }
		
		// motorhome Make/Model filter accepts if Make = "make" and Model = contains a word
      if($fieldName == "taxi_make_model")
      {
      //will accept only if make is Toyota and model contains words like Granvia, Hiace or Regius
      //$filterValue = array("Toyota" => array("Granvia","Hiace","Regius",),); 
         $taxiMake  = $_SESSION['_YourDetails_']['taxi_make'];
         $taxiModel = $_SESSION['_YourDetails_']['taxi_model'];

         if($fieldType == "LIST")
         {
            if($acceptance == "ACC")
            {
               if(array_key_exists($taxiMake, $filterValue))
               {
                  foreach($filterValue[$taxiMake] as $vehModel)
                  {
                     if(preg_match("/$vehModel/i",$taxiModel))
                     {  
                        $filter = "accepted";
                     }      
                  }   
               }
               if($filter != "accepted" || ! array_key_exists($taxiMake, $filterValue))
                  return false;
               else
                  return true;
            }
         }
      }

      // motorhome Make/Model filter accepts if Make = "make" and Model = contains a word
      if($fieldName == "motor_home_make_model")
      {
      //will accept only if make is Toyota and model contains words like Granvia, Hiace or Regius
      //$filterValue = array("Toyota" => array("Granvia","Hiace","Regius",),); 
         $motorHomeMake  = $_SESSION['_YourDetails_']['motor_home_make'];
         $motorHomeModel = $_SESSION['_YourDetails_']['motor_home_model'];

         if($fieldType == "LIST")
         {
            if($acceptance == "ACC")
            {
               if(array_key_exists($motorHomeMake, $filterValue))
               {
                  foreach($filterValue[$motorHomeMake] as $vehModel)
                  {
                     if(preg_match("/$vehModel/i",$motorHomeModel))
                     {  
                        $filter = "accepted";
                     }      
                  }   
               }
               if($filter != "accepted" || ! array_key_exists($motorHomeMake, $filterValue))
                  return false;
               else
                  return true;
            }
         }
      }

      // motor_home_reg_number_empty
      if($fieldName == "motor_home_reg_number_empty")
      {
         $vehReg  = $_SESSION['_YourDetails_']['motor_home_reg_number'];

         $filterFlag = 1;

         if(empty($vehReg))
         {
            $vehReg = "empty";

            if($fieldType == "VAL")
            {
               if($acceptance == "SKP")
               {
                  if($valueType == "EQL")
                  {
                     if($vehReg  == $filterValue)
                        $filterFlag = 0;
                  }
               }
            }

            if($filterFlag)
               return true;
            else
               return false;
         }

      }

      //vehicle make motorhome match
      if($fieldName == "motorhome_make_match")
      {
         $motorhomeManualMake = $session['_YourDetails_']['motor_home_make_manual'];
         $motorhomeMake       = $session['_YourDetails_']['motor_home_make'];
         $motorhomeModel      = $session['_YourDetails_']['motor_home_model'];

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               foreach($filterValue as $k => $motorhomeMakeVal)
               {
                  if(preg_match("/$motorhomeMakeVal/i",$motorhomeManualMake))
                     return false;

                  if(preg_match("/$motorhomeMakeVal/i",$motorhomeMake))
                     return false;

                  if(preg_match("/$motorhomeMakeVal/i",$motorhomeModel))
                     return false;
               }

               return true;
            }

            if($acceptance == "ACC")
            {
               $isFiltered = 1;

               foreach($filterValue as $k => $motorhomeMakeVal)
               {
                  if(preg_match("/$motorhomeMakeVal/i",$motorhomeManualMake))
                     $isFiltered = 0;

                  if(preg_match("/$motorhomeMakeVal/i",$motorhomeMake))
                     $isFiltered = 0;

                  if(preg_match("/$motorhomeMakeVal/i",$motorhomeModel))
                     $isFiltered = 0;
               }

               if($isFiltered == 1)
                  return false;
               else
                  return true;

            }
         } 
      }
      //vehicle make motorhome match

       // kitcar vehicle make
      if($fieldName == "kitcar_make")
      {
         $kitcarMake  = strtoupper($session['_YourDetails_']['kitcar_make']);

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               foreach($filterValue as $k => $kitcarVal)
               {
                  if(preg_match("/\b($kitcarVal)\b/", $kitcarMake))
                     return false;
               }

               return true;
            }

            if($acceptance == "ACC")
            {
               $isFiltered = 1;

               foreach($filterValue as $k => $kitcarVal)
               {
                  if(preg_match("/\b($kitcarVal)\b/", $kitcarMake))
                     $isFiltered = 0;
               }

               if($isFiltered == 1)
                  return false;
               else
                  return true;
            }
         }
      }
      //end kitcar vehicle make
      
      // KITCAR VEH MAKE MODEL FILTER
      if($fieldName == "kitcar_make_model")
      {
         $vehicleMake   = trim(strtoupper($_SESSION['_YourDetails_']['kitcar_make']));   
         $vehicleModel  = trim(strtoupper($_SESSION['_YourDetails_']['kitcar_model'])); 

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {        
               foreach($filterValue as $makeKey => $modelVal)   
               {  
                  if($makeKey == $vehicleMake && in_array($vehicleModel, $filterValue[$vehicleMake]) )
                     return false;
               }              
                  return true;
            }

            if($acceptance == "ACC")
            {
               $isFiltered = 1;

               foreach($filterValue as $makeKey => $modelVal)   
               {  
                  if($makeKey == $vehicleMake && in_array($vehicleModel, $filterValue[$vehicleMake]) )
                     $isFiltered == 0;
               } 

               if($isFiltered == 1)
                  return false;
               else
                  return true;
            }
         }
      }
      
      
      //vehicle make
      if($fieldName == "vehicle_make")
      {
         $vehicleMake  = strtoupper($session['_YourDetails_']['vehicle_make']);

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               foreach($filterValue as $k => $vehicleMakeVal)
               {
                  if(preg_match("/\b($vehicleMakeVal)\b/", $vehicleMake))
                     return false;
               }

               return true;
            }

            if($acceptance == "ACC")
            {
               $isFiltered = 1;

               foreach($filterValue as $k => $vehicleMakeVal)
               {
                  if(preg_match("/\b($vehicleMakeVal)\b/", $vehicleMake))
                     $isFiltered = 0;
               }

               if($isFiltered == 1)
                  return false;
               else
                  return true;
            }
         }
      }
      //end vehicle make

      
      //LIMO vehicle make
      if($fieldName == "limousine_make")
      {
         $vehicleMake  = strtoupper($session['_YourDetails_']['limousine_make']);

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               foreach($filterValue as $k => $vehicleMakeVal)
               {
                  if(preg_match("/\b($vehicleMakeVal)\b/", $vehicleMake))
                     return false;
               }

               return true;
            }

            if($acceptance == "ACC")
            {
               $isFiltered = 1;

               foreach($filterValue as $k => $vehicleMakeVal)
               {
                  if(preg_match("/\b($vehicleMakeVal)\b/", $vehicleMake))
                     $isFiltered = 0;
               }

               if($isFiltered == 1)
                  return false;
               else
                  return true;
            }
         }
      }
      //end vehicle make
       
      //LIMO vehicle model
      if($fieldName == "limousine_model")
      {
         $vehicleMake  = strtoupper($session['_YourDetails_']['limousine_model']);

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               foreach($filterValue as $k => $vehicleMakeVal)
               {
                  if(preg_match("/\b($vehicleMakeVal)\b/", $vehicleMake))
                     return false;
               }

               return true;
            }

            if($acceptance == "ACC")
            {
               $isFiltered = 1;

               foreach($filterValue as $k => $vehicleMakeVal)
               {
                  if(preg_match("/\b($vehicleMakeVal)\b/", $vehicleMake))
                     $isFiltered = 0;
               }

               if($isFiltered == 1)
                  return false;
               else
                  return true;
            }
         }
      }
      //end vehicle model

      //TRUCK vehicle model
      if($fieldName == "truck_vehicle_model")
      {
         $vehicleMake  = strtoupper($session['_YourDetails_']['vehicle_model']);

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               foreach($filterValue as $k => $vehicleMakeVal)
               {
                  if(preg_match("/$vehicleMakeVal/isU", $vehicleMake))
                     return false;
               }

               return true;
            }

            if($acceptance == "ACC")
            {
               $isFiltered = 1;

               foreach($filterValue as $k => $vehicleMakeVal)
               {
                  if(preg_match("/$vehicleMakeVal/isU", $vehicleMake))
                     $isFiltered = 0;
               }

               if($isFiltered == 1)
                  return false;
               else
                  return true;
            }
         }
      }
      //end vehicle model



      //filter for motor-trade check box trade occupation
      // - in this case we skip or accept in case there is at least 1 of the occupation in the list
      if($fieldName == "motor_trade_occupation_all")
      {
         $tempArrayOfFileds = array();

         foreach($session['_YourDetails_'] as $fieldNameInSes => $valOfFiledInSes)
         {
            if(preg_match("/trade_occupation_(.*)/isu",$fieldNameInSes,$resOccupationNumbers))
            {
               $noOfField = $resOccupationNumbers[1];
               if($valOfFiledInSes == "Y")
                  $tempArrayOfFileds[$noOfField] = $valOfFiledInSes;
            }
         }

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               foreach($tempArrayOfFileds as $fName => $fValue)
               {
                  if(in_array($fName,$filterValue))
                  {
                     return false;
                  }
               }
               return true;
            }

            if($acceptance == "ACC")
            {
               $isSkipped = 1;
               foreach($tempArrayOfFileds as $fName => $fValue)
               {
                  if(in_array($fName,$filterValue))
                  {
                     $isSkipped = 0;
                  }
               }

               if($isSkipped)
                  return false;
               else
                  return true;
            }
         }
      }

	  
	  
	  
	  	$vehicleTypeArr = array(
	"vehicle_type_1" => "type_cars",
	"vehicle_type_2" => "type_vans",
	"vehicle_type_3" => "type_motorbikes",
	"vehicle_type_4" => "type_pick_ups",
	"vehicle_type_5" => "type_lorries_up_to_7_5_tonne",
	"vehicle_type_6" => "type_lorries_over_7_5_tonne",
	"vehicle_type_7" => "type_flatbeds",
	"vehicle_type_8" => "type_tippers",
	"vehicle_type_9" => "type_taxis",
	"vehicle_type_10" => "type_minibuses",
	"vehicle_type_11" => "type_buses_coaches",
	"vehicle_type_12" => "type_other_vehicles",
	);
	
	foreach($vehicleTypeArr as $fieldNameArr => $VehTypeVal)
	{	
		
		if($fieldName == $VehTypeVal)
	    {
			$motorFleetVehType = $session["_YourDetails_"][$fieldNameArr];
			
			if($acceptance == "SKP")
			{
			  if(in_array($motorFleetVehType, $filterValue))
				  return false;
			  else
				  return true;		
			}
			
			if($acceptance == "ACC")
			{
			  if(in_array($motorFleetVehType, $filterValue))
				  return true;
			  else
				  return false;		
			}
		}
	} 
	  
	  
   
     //////////////////////////////////////////////////////////// care home type start //////////////////////////////////////////////////////////   
     if($fieldName == "carehome_type")
     {   
          $tempArrayOfFileds = array();

          foreach($session['_YourDetails_'] as $fieldNameInSes => $valOfFiledInSes)
          {
            if(preg_match("/carehome_type_(.*)/isu",$fieldNameInSes,$resVehTypesNumbers))
            {
               $noOfField = $resVehTypesNumbers[1];
               if($valOfFiledInSes != "")
                 $tempArrayOfFileds[$noOfField] = $valOfFiledInSes;
            }
          }
         if($acceptance == "SKP")
         {
            foreach($tempArrayOfFileds as $fName => $fValue)
            {
              switch($fName)
              {
                  case '1':
                     $fName = 'Elderly';
                     break;
                  case '2':
                     $fName = 'Elderly with Nursing';
                     break;
                  case '3':
                     $fName = 'Dementia (EMI)';
                     break;
                  case '4':
                     $fName = 'Adults';
                     break;
                  case '5':
                     $fName = 'Physical disability';
                     break;
                  case '6':
                     $fName = 'Learning disability';
                     break;
                  case '7':
                     $fName = 'Supported living';
                     break;
                  case '8':
                     $fName = 'Supported living with Nursing';
                     break;
                  case '9':
                     $fName = 'Retirement Village';
                     break;
                  case '10':
                     $fName = 'Mental health';
                     break;
                  case '11':
                     $fName = 'Alcohol dependency';
                     break;
                  case '12':
                     $fName = 'Drug dependency';
                     break;
                  case '13':
                     $fName = 'No medical intervention (e.g. religious reasons)';
                     break;
                  default:
                     $fName = '';
              }
              if(in_array($fName,$filterValue))
              {
                return false;
              }
            }
            return true;
         }
         if($acceptance == "ACC")
         {
            $isSkipped = 1;
            foreach($tempArrayOfFileds as $fName => $fValue)
            {
              switch($fName)
              {
                  case '1':
                     $fName = 'Elderly';
                     break;
                  case '2':
                     $fName = 'Elderly with Nursing';
                     break;
                  case '3':
                     $fName = 'Dementia (EMI)';
                     break;
                  case '4':
                     $fName = 'Adults';
                     break;
                  case '5':
                     $fName = 'Physical disability';
                     break;
                  case '6':
                     $fName = 'Learning disability';
                     break;
                  case '7':
                     $fName = 'Supported living';
                     break;
                  case '8':
                     $fName = 'Supported living with Nursing';
                     break;
                  case '9':
                     $fName = 'Retirement Village';
                     break;
                  case '10':
                     $fName = 'Mental health';
                     break;
                  case '11':
                     $fName = 'Alcohol dependency';
                     break;
                  case '12':
                     $fName = 'Drug dependency';
                     break;
                  case '13':
                     $fName = 'No medical intervention (e.g. religious reasons)';
                     break;
                  default:
                     $fName = '';
              }
              if(in_array($fName,$filterValue))
              {
                $isSkipped = 0;
              }
            }

            if($isSkipped)
              return false;
            else
              return true;
         }
         if($acceptance == "ACC_ONLY")
         {
            $isSkipped = 1;
            foreach($tempArrayOfFileds as $fName => $fValue)
            {
              switch($fName)
              {
                  case '1':
                     $fName = 'Elderly';
                     break;
                  case '2':
                     $fName = 'Elderly with Nursing';
                     break;
                  case '3':
                     $fName = 'Dementia (EMI)';
                     break;
                  case '4':
                     $fName = 'Adults';
                     break;
                  case '5':
                     $fName = 'Physical disability';
                     break;
                  case '6':
                     $fName = 'Learning disability';
                     break;
                  case '7':
                     $fName = 'Supported living';
                     break;
                  case '8':
                     $fName = 'Supported living with Nursing';
                     break;
                  case '9':
                     $fName = 'Retirement Village';
                     break;
                  case '10':
                     $fName = 'Mental health';
                     break;
                  case '11':
                     $fName = 'Alcohol dependency';
                     break;
                  case '12':
                     $fName = 'Drug dependency';
                     break;
                  case '13':
                     $fName = 'No medical intervention (e.g. religious reasons)';
                     break;
                  default:
                     $fName = '';
              }
              if(in_array($fName,$filterValue))
                $isSkipped = 0;
              else
              {
                $isSkipped = 1;
                break;
              }
                
            }

            if($isSkipped)
              return false;
            else
              return true;       
         }
               
         if($acceptance == "SKP_ONLY")
         {
            $isSkipped = 1;
            foreach($tempArrayOfFileds as $fName => $fValue)
            {
              switch($fName)
              {
                  case '1':
                     $fName = 'Elderly';
                     break;
                  case '2':
                     $fName = 'Elderly with Nursing';
                     break;
                  case '3':
                     $fName = 'Dementia (EMI)';
                     break;
                  case '4':
                     $fName = 'Adults';
                     break;
                  case '5':
                     $fName = 'Physical disability';
                     break;
                  case '6':
                     $fName = 'Learning disability';
                     break;
                  case '7':
                     $fName = 'Supported living';
                     break;
                  case '8':
                     $fName = 'Supported living with Nursing';
                     break;
                  case '9':
                     $fName = 'Retirement Village';
                     break;
                  case '10':
                     $fName = 'Mental health';
                     break;
                  case '11':
                     $fName = 'Alcohol dependency';
                     break;
                  case '12':
                     $fName = 'Drug dependency';
                     break;
                  case '13':
                     $fName = 'No medical intervention (e.g. religious reasons)';
                     break;
                  default:
                     $fName = '';
              }
              if(!in_array($fName,$filterValue))
              {  
                $isSkipped = 0;
                break;
              }
              else
              {
                $isSkipped = 1;
              }
                
            }
            if($isSkipped)
              return false;
            else
              return true;       
         }
      }
      //////////////////////////////////////////////////////////// care home type end //////////////////////////////////////////////////////////  
   	  
	  
	  
      if($fieldName == "trade_occupations")
      {
         $tempArrayOfFileds = array();

         foreach($session['_YourDetails_'] as $fieldNameInSes => $valOfFiledInSes)
         {
            if(preg_match("/trade_occupation_(.*)/isu",$fieldNameInSes,$resOccupationNumbers))
            {
               $noOfField = $resOccupationNumbers[1];
               if($valOfFiledInSes == "Y")
                  $tempArrayOfFileds[$noOfField] = $valOfFiledInSes;
            }
         }


		if($acceptance == "SKP")
		{
		   foreach($tempArrayOfFileds as $fName => $fValue)
		   {
		      switch($fName)
			  {
					case '1':
					   $fName = 'Scrap Dealer';
					   break;
					case '2':
					   $fName = 'Vehicle Sales';
					   break;
					case '3':
					   $fName = 'Vehicle Servicing';
					   break;
					case '4':
					   $fName = 'Mechanical Repair';
					   break;
					case '5':
					   $fName = 'Vehicle Electrician';
					   break;
					case '6':
					   $fName = 'Breakdown Recovery';
					   break;
					case '7':
					   $fName = 'Body Repairs';
					   break;
					case '8':
					   $fName = 'Vehicle Valeter';
					   break;
					case '9':
					   $fName = 'Other';
					   break;
					case '10':
					   $fName = 'Classic Sales';
					   break;
					case '11':
					   $fName = 'Vehicle Importing / Exporting';
					   break;
					case '12':
					   $fName = 'Performance Sales';
					   break;
					case '13':
					   $fName = 'Car Detailing';
					   break;
					case '14':
					   $fName = 'Car Jockey';
					   break;
				   case '15':
					   $fName = 'Car Demos';
					   break;
				   case '16':
					   $fName = 'Tyre Fitting / Sales';
					   break;
				   case '17':
					   $fName = 'Window Tinting';
					   break;
				   case '18':
             $fName = 'MOT Prep / Testing';
             break;
           case '19':
             $fName = 'Accessory Fitting';
             break;
           case '20':
             $fName = 'Mobile Mechanic';
             break;
           case '21':
             $fName = 'Vehicle Collection / Delivery';
             break;
           case '22':
					   $fName = 'Windscreen Repair / Fitting';
					   break;

					default:
					   $fName = '';
			  }
			  if(in_array($fName,$filterValue))
			  {
				 return false;
			  }
		   }
		   return true;
		}
		if($acceptance == "ACC")
		{
		   $isSkipped = 1;
		   foreach($tempArrayOfFileds as $fName => $fValue)
		   {
		      switch($fName)
			  {
					case '1':
             $fName = 'Scrap Dealer';
             break;
          case '2':
             $fName = 'Vehicle Sales';
             break;
          case '3':
             $fName = 'Vehicle Servicing';
             break;
          case '4':
             $fName = 'Mechanical Repair';
             break;
          case '5':
             $fName = 'Vehicle Electrician';
             break;
          case '6':
             $fName = 'Breakdown Recovery';
             break;
          case '7':
             $fName = 'Body Repairs';
             break;
          case '8':
             $fName = 'Vehicle Valeter';
             break;
          case '9':
             $fName = 'Other';
             break;
          case '10':
             $fName = 'Classic Sales';
             break;
          case '11':
             $fName = 'Vehicle Importing / Exporting';
             break;
          case '12':
             $fName = 'Performance Sales';
             break;
          case '13':
             $fName = 'Car Detailing';
             break;
          case '14':
             $fName = 'Car Jockey';
             break;
           case '15':
             $fName = 'Car Demos';
             break;
           case '16':
             $fName = 'Tyre Fitting / Sales';
             break;
           case '17':
             $fName = 'Window Tinting';
             break;
           case '18':
             $fName = 'MOT Prep / Testing';
             break;
           case '19':
             $fName = 'Accessory Fitting';
             break;
           case '20':
             $fName = 'Mobile Mechanic';
             break;
           case '21':
             $fName = 'Vehicle Collection / Delivery';
             break;
           case '22':
             $fName = 'Windscreen Repair / Fitting';
             break;
					default:
					   $fName = '';
			  }
			  if(in_array($fName,$filterValue))
			  {
				 $isSkipped = 0;
			  }
		   }

		   if($isSkipped)
			  return false;
		   else
			  return true;
		}
		if($acceptance == "ACC_ONLY")
		{
		   $isSkipped = 1;
		   foreach($tempArrayOfFileds as $fName => $fValue)
		   {
		      switch($fName)
			  {
					case '1':
             $fName = 'Scrap Dealer';
             break;
          case '2':
             $fName = 'Vehicle Sales';
             break;
          case '3':
             $fName = 'Vehicle Servicing';
             break;
          case '4':
             $fName = 'Mechanical Repair';
             break;
          case '5':
             $fName = 'Vehicle Electrician';
             break;
          case '6':
             $fName = 'Breakdown Recovery';
             break;
          case '7':
             $fName = 'Body Repairs';
             break;
          case '8':
             $fName = 'Vehicle Valeter';
             break;
          case '9':
             $fName = 'Other';
             break;
          case '10':
             $fName = 'Classic Sales';
             break;
          case '11':
             $fName = 'Vehicle Importing / Exporting';
             break;
          case '12':
             $fName = 'Performance Sales';
             break;
          case '13':
             $fName = 'Car Detailing';
             break;
          case '14':
             $fName = 'Car Jockey';
             break;
           case '15':
             $fName = 'Car Demos';
             break;
           case '16':
             $fName = 'Tyre Fitting / Sales';
             break;
           case '17':
             $fName = 'Window Tinting';
             break;
           case '18':
             $fName = 'MOT Prep / Testing';
             break;
           case '19':
             $fName = 'Accessory Fitting';
             break;
           case '20':
             $fName = 'Mobile Mechanic';
             break;
           case '21':
             $fName = 'Vehicle Collection / Delivery';
             break;
           case '22':
             $fName = 'Windscreen Repair / Fitting';
             break;
					default:
					   $fName = '';
			  }
			  if(in_array($fName,$filterValue))
				 $isSkipped = 0;
			  else
			  {
				 $isSkipped = 1;
				 break;
			  }
				 
		   }

		   if($isSkipped)
			  return false;
		   else
			  return true;			
		}
    }


	 if($fieldName == "conviction_types_sk")
	 {
		if($acceptance == "ACC")
		{

		   $willFilter = 1;

		   for ($i = 1; $i<=4; $i++)
		   {
			  $convictionType = "";

			  $convictionType =  $_SESSION['_YourDetails_']["conviction_type_$i"];

			  if($_SESSION['_YourDetails_']["conviction_type_$i"])
			  {
				 if(in_array($convictionType,$filterValue))
					return true;
					
			  }
		   }             
			  
			return false;
		}

		if($acceptance == "SKP")
		{
		   $willFilter = 1;

		   for ($i = 1; $i<=4; $i++)
		   {
			  $convictionType = "";

			  $convictionType =  $_SESSION['_YourDetails_']["conviction_type_$i"];

			  if($_SESSION['_YourDetails_']["conviction_type_$i"])
			  {
				 if(in_array($convictionType,$filterValue))
					$willFilter = 0;
			  }
		   }

		   if($willFilter == 1)
			  return true;
		   else
			  return false;
		}
		
		if($acceptance == "ACC_ONLY")
		{
		   $isSkipped = 1;
		   for ($i = 1; $i<=4; $i++)
		   {
			  $convictionType = "";

			  $convictionType =  $_SESSION['_YourDetails_']["conviction_type_$i"];

			  if($_SESSION['_YourDetails_']["conviction_type_$i"])
			  {
				 if(in_array($convictionType,$filterValue))
				 $isSkipped = 0;
				 else
				 {
				    $isSkipped = 1;
					break;
				 }
			  }
		   }
		   if($isSkipped)
			  return false;
		   else
			  return true;			
		}
		
		if($acceptance == "SKP_ONLY")
		{
		   $isSkipped = 1;
		   for ($i = 1; $i<=4; $i++)
		   {
			  $convictionType = "";

			  $convictionType =  $_SESSION['_YourDetails_']["conviction_type_$i"];

			  if($_SESSION['_YourDetails_']["conviction_type_$i"])
			  {
				 if(!in_array($convictionType,$filterValue))
				 {
					$isSkipped = 0;
					break;
				 }
				 else
				    $isSkipped = 1;
			  }
		   }
		   if($isSkipped)
			  return false;
		   else
			  return true;			
		}	
	 }

	 
	  //dependency filter
     if($fieldName == "vehicle_types")
     {
        if($session['_QZ_QUOTE_DETAILS_']['system'] == 'TRUCK')
		{	
			if($session['_YourDetails_']['number_of_vehicle'] > '1')
			{		
				 $tempArrayOfFileds = array();

				 foreach($session['_YourDetails_'] as $fieldNameInSes => $valOfFiledInSes)
				 {
					if(preg_match("/vehicle_type_(.*)/isu",$fieldNameInSes,$resVehTypesNumbers))
					{
					   $noOfField = $resVehTypesNumbers[1];
					   if($valOfFiledInSes != "")
						  $tempArrayOfFileds[$noOfField] = $valOfFiledInSes;
					}
				 }
				if($acceptance == "SKP")
				{
				   foreach($tempArrayOfFileds as $fName => $fValue)
				   {
					  switch($fName)
					  {
							case '1':
							   $fName = 'Rigid';
							   break;
							case '2':
							   $fName = 'Tipper / Dump';
							   break;
							case '3':
							   $fName = 'Other';
							   break;
							case '4':
							   $fName = 'Articulated';
							   break;
							case '5':
							   $fName = 'Refridgerated';
							   break;
							case '6':
							   $fName = 'Pick Up';
							   break;
							case '7':
							   $fName = 'Recovery';
							   break;
							case '8':
							   $fName = 'Fork Lift';
							   break;
							case '9':
							   $fName = 'Skip Lorry';
							   break;
							default:
							   $fName = '';
					  }
					  if(in_array($fName,$filterValue))
					  {
						 return false;
					  }
				   }
				   return true;
				}
				if($acceptance == "ACC")
				{
				   $isSkipped = 1;
				   foreach($tempArrayOfFileds as $fName => $fValue)
				   {
					  switch($fName)
					  {
							case '1':
							   $fName = 'Rigid';
							   break;
							case '2':
							   $fName = 'Tipper / Dump';
							   break;
							case '3':
							   $fName = 'Other';
							   break;
							case '4':
							   $fName = 'Articulated';
							   break;
							case '5':
							   $fName = 'Refridgerated';
							   break;
							case '6':
							   $fName = 'Pick Up';
							   break;
							case '7':
							   $fName = 'Recovery';
							   break;
							case '8':
							   $fName = 'Fork Lift';
							   break;
							case '9':
							   $fName = 'Skip Lorry';
							   break;
							default:
							   $fName = '';
					  }
					  if(in_array($fName,$filterValue))
					  {
						 $isSkipped = 0;
					  }
				   }

				   if($isSkipped)
					  return false;
				   else
					  return true;
				}
				if($acceptance == "ACC_ONLY")
				{
				   $isSkipped = 1;
				   foreach($tempArrayOfFileds as $fName => $fValue)
				   {
					  switch($fName)
					  {
							case '1':
							   $fName = 'Rigid';
							   break;
							case '2':
							   $fName = 'Tipper / Dump';
							   break;
							case '3':
							   $fName = 'Other';
							   break;
							case '4':
							   $fName = 'Articulated';
							   break;
							case '5':
							   $fName = 'Refridgerated';
							   break;
							case '6':
							   $fName = 'Pick Up';
							   break;
							case '7':
							   $fName = 'Recovery';
							   break;
							case '8':
							   $fName = 'Fork Lift';
							   break;
							case '9':
							   $fName = 'Skip Lorry';
							   break;
							default:
							   $fName = '';
					  }
					  if(in_array($fName,$filterValue))
						 $isSkipped = 0;
					  else
					  {
						 $isSkipped = 1;
						 break;
					  }
						 
				   }

				   if($isSkipped)
					  return false;
				   else
					  return true;			
				}
						
				if($acceptance == "SKP_ONLY")
				{
				   $isSkipped = 1;
				   foreach($tempArrayOfFileds as $fName => $fValue)
				   {
					  switch($fName)
					  {
							case '1':
							   $fName = 'Rigid';
							   break;
							case '2':
							   $fName = 'Tipper / Dump';
							   break;
							case '3':
							   $fName = 'Other';
							   break;
							case '4':
							   $fName = 'Articulated';
							   break;
							case '5':
							   $fName = 'Refridgerated';
							   break;
							case '6':
							   $fName = 'Pick Up';
							   break;
							case '7':
							   $fName = 'Recovery';
							   break;
							case '8':
							   $fName = 'Fork Lift';
							   break;
							case '9':
							   $fName = 'Skip Lorry';
							   break;
							default:
							   $fName = '';
					  }
					  if(!in_array($fName,$filterValue))
					  {  
						 $isSkipped = 0;
						 break;
					  }
					  else
					  {
						 $isSkipped = 1;
					  }
						 
				   }
				   if($isSkipped)
					  return false;
				   else
					  return true;			
				}
			}
			return true;
		}
        elseif($session['_QZ_QUOTE_DETAILS_']['system'] == 'TAXIFLEET')
		{	
			 $tempArrayOfFileds = array();

			 foreach($session['_YourDetails_'] as $fieldNameInSes => $valOfFiledInSes)
			 {
				if(preg_match("/vehicle_type_(.*)/isu",$fieldNameInSes,$resVehTypesNumbers))
				{
				   $noOfField = $resVehTypesNumbers[1];
				   if($valOfFiledInSes != "")
					  $tempArrayOfFileds[$noOfField] = $valOfFiledInSes;
				}
			 }
			if($acceptance == "SKP")
			{
			   foreach($tempArrayOfFileds as $fName => $fValue)
			   {
				  switch($fName)
				  {
						case '1':
						   $fName = 'Saloon';
						   break;
						case '2':
						   $fName = 'Black Cab';
						   break;
						case '3':
						   $fName = 'MVP';
						   break;
						case '4':
						   $fName = 'Minibus';
						   break;
						case '5':
						   $fName = 'Coach';
						   break;
						case '6':
						   $fName = 'Limousine';
						   break;
						default:
						   $fName = '';
				  }
				  if(in_array($fName,$filterValue))
				  {
					 return false;
				  }
			   }
			   return true;
			}
			if($acceptance == "ACC")
			{
			   $isSkipped = 1;
			   foreach($tempArrayOfFileds as $fName => $fValue)
			   {
				  switch($fName)
				  {
						case '1':
						   $fName = 'Saloon';
						   break;
						case '2':
						   $fName = 'Black Cab';
						   break;
						case '3':
						   $fName = 'MVP';
						   break;
						case '4':
						   $fName = 'Minibus';
						   break;
						case '5':
						   $fName = 'Coach';
						   break;
						case '6':
						   $fName = 'Limousine';
						   break;
						default:
						   $fName = '';
				  }
				  if(in_array($fName,$filterValue))
				  {
					 $isSkipped = 0;
				  }
			   }

			   if($isSkipped)
				  return false;
			   else
				  return true;
			}
			if($acceptance == "ACC_ONLY")
			{
			   $isSkipped = 1;
			   foreach($tempArrayOfFileds as $fName => $fValue)
			   {
				  switch($fName)
				  {
						case '1':
						   $fName = 'Saloon';
						   break;
						case '2':
						   $fName = 'Black Cab';
						   break;
						case '3':
						   $fName = 'MVP';
						   break;
						case '4':
						   $fName = 'Minibus';
						   break;
						case '5':
						   $fName = 'Coach';
						   break;
						case '6':
						   $fName = 'Limousine';
						   break;
						default:
						   $fName = '';
				  }
				  if(in_array($fName,$filterValue))
					 $isSkipped = 0;
				  else
				  {
					 $isSkipped = 1;
					 break;
				  }
					 
			   }

			   if($isSkipped)
				  return false;
			   else
				  return true;			
			}
					
			if($acceptance == "SKP_ONLY")
			{
			   $isSkipped = 1;
			   foreach($tempArrayOfFileds as $fName => $fValue)
			   {
				  switch($fName)
				  {
						case '1':
						   $fName = 'Saloon';
						   break;
						case '2':
						   $fName = 'Black Cab';
						   break;
						case '3':
						   $fName = 'MVP';
						   break;
						case '4':
						   $fName = 'Minibus';
						   break;
						case '5':
						   $fName = 'Coach';
						   break;
						case '6':
						   $fName = 'Limousine';
						   break;
						default:
						   $fName = '';
				  }
				  if(!in_array($fName,$filterValue))
				  {  
					 $isSkipped = 0;
					 break;
				  }
				  else
				  {
					 $isSkipped = 1;
				  }
					 
			   }
			   if($isSkipped)
				  return false;
			   else
				  return true;			
			}
		}
		else
		{
			 $tempArrayOfFileds = array();

			 foreach($session['_YourDetails_'] as $fieldNameInSes => $valOfFiledInSes)
			 {
				if(preg_match("/vehicle_type_(.*)/isu",$fieldNameInSes,$resVehTypesNumbers))
				{
				   $noOfField = $resVehTypesNumbers[1];
				   if($valOfFiledInSes != "")
					  $tempArrayOfFileds[$noOfField] = $valOfFiledInSes;
				}
			 }
			if($acceptance == "SKP")
			{
			   foreach($tempArrayOfFileds as $fName => $fValue)
			   {
				  switch($fName)
				  {
						case '1':
						   $fName = 'Car';
						   break;
						case '2':
						   $fName = 'Van';
						   break;
						case '3':
						   $fName = 'Motorbike';
						   break;
						case '4':
						   $fName = 'Lorry';
						   break;
						case '5':
						   $fName = 'Bicycle';
						   break;
						case '6':
						   $fName = 'Other';
						   break;
						default:
						   $fName = '';
				  }
				  if(in_array($fName,$filterValue))
				  {
					 return false;
				  }
			   }
			   return true;
			}
			if($acceptance == "ACC")
			{
			   $isSkipped = 1;
			   foreach($tempArrayOfFileds as $fName => $fValue)
			   {
				  switch($fName)
				  {
						case '1':
						   $fName = 'Car';
						   break;
						case '2':
						   $fName = 'Van';
						   break;
						case '3':
						   $fName = 'Motorbike';
						   break;
						case '4':
						   $fName = 'Lorry';
						   break;
						case '5':
						   $fName = 'Bicycle';
						   break;
						case '6':
						   $fName = 'Other';
						   break;
						default:
						   $fName = '';
				  }
				  if(in_array($fName,$filterValue))
				  {
					 $isSkipped = 0;
				  }
			   }

			   if($isSkipped)
				  return false;
			   else
				  return true;
			}
			if($acceptance == "ACC_ONLY")
			{
			   $isSkipped = 1;
			   foreach($tempArrayOfFileds as $fName => $fValue)
			   {
				  switch($fName)
				  {
						case '1':
						   $fName = 'Car';
						   break;
						case '2':
						   $fName = 'Van';
						   break;
						case '3':
						   $fName = 'Motorbike';
						   break;
						case '4':
						   $fName = 'Lorry';
						   break;
						case '5':
						   $fName = 'Bicycle';
						   break;
						case '6':
						   $fName = 'Other';
						   break;
						default:
						   $fName = '';
				  }
				  if(in_array($fName,$filterValue))
					 $isSkipped = 0;
				  else
				  {
					 $isSkipped = 1;
					 break;
				  }
					 
			   }

			   if($isSkipped)
				  return false;
			   else
				  return true;			
			}
					
			if($acceptance == "SKP_ONLY")
			{
			   $isSkipped = 1;
			   foreach($tempArrayOfFileds as $fName => $fValue)
			   {
				  switch($fName)
				  {
						case '1':
						   $fName = 'Car';
						   break;
						case '2':
						   $fName = 'Van';
						   break;
						case '3':
						   $fName = 'Motorbike';
						   break;
						case '4':
						   $fName = 'Lorry';
						   break;
						case '5':
						   $fName = 'Bicycle';
						   break;
						case '6':
						   $fName = 'Other';
						   break;
						default:
						   $fName = '';
				  }
				  if(!in_array($fName,$filterValue))
				  {  
					 $isSkipped = 0;
					 break;
				  }
				  else
				  {
					 $isSkipped = 1;
				  }
					 
			   }
			   if($isSkipped)
				  return false;
			   else
				  return true;			
			}			
		}
    }	
	
	//dependency filter
   if($session['_QZ_QUOTE_DETAILS_']['system'] == 'COURIER')
   {
      if($fieldName == "year_of_manufacture")
      {
         if($session['_YourDetails_']['number_of_vehicle'] == 1)
         {
             $yearOfManufacture = $session['_YourDetails_']['year_of_manufacture'];
                   
             if($fieldType == "LIST")
             {
               if($acceptance == "SKP")
               {
                  if(in_array($yearOfManufacture,$filterValue))   
                    return false;
                  else
                    return true;
               }

               if($acceptance == "ACC")
               {
                  if(! in_array($yearOfManufacture,$filterValue))
                    return false;
                  else
                    return true;
               }
             }
         }
      return true;
      }
   }

   if($session["_QZ_QUOTE_DETAILS_"]["system"] == "BICYCLE")
   {
      if($fieldName == "bicycle_makes_sk")
      {
         $bikesArr[]          =  $session["_YourDetails_"]["bicycle_make"];
         $numberOfBicycles    =  $session["_YourDetails_"]["number_of_bicycles"];
         for($i=2;$i<=8;$i++)
         {  
            $bikesArr[] =  $session["_YourDetails_"]["bicycle_".$i."_make"];
         }

         if($numberOfBicycles <= "8")
         {   
             if($fieldType == "LIST")
             {
               if($acceptance == "SKP")
               {
                  $skpBikesList = array();

                  foreach($bikesArr as $bikeVal)
                  {
                     if(in_array($bikeVal,$filterValue))
                        $skpBikesList[] = $bikeVal;
                  }

                  if($skpBikesList[0] == "")   
                    return true;
                  else
                    return false;
               }

               if($acceptance == "SKP_ONLY")
               {
                  $skpBikesList = array();

                  foreach($bikesArr as $bikeVal)
                  {
                     if(!in_array($bikeVal,$filterValue))
                        $skpBikesList[] = $bikeVal;
                  }

                  if($skpBikesList[0] == "")   
                    return false;
                  else
                    return true;
               }

               if($acceptance == "ACC")
               {
                  $accBikesList = array();
                  foreach($bikesArr as $bikeVal)
                  {
                     if(in_array($bikeVal,$filterValue))
                        $accBikesList[] = $bikeVal;                    
                  }

                  if($accBikesList[0] == "") 
                    return false;
                  else
                    return true;
               }

               if($acceptance == "ACC_ONLY")
               {
                  $accBikesList = array();
                  foreach($bikesArr as $bikeVal)
                  {
                     if(!in_array($bikeVal,$filterValue))
                        $accBikesList[] = $bikeVal;                    
                  }

                  if($accBikesList[0] == "") 
                    return true;
                  else
                    return false;
               }
             }
          }

		    return true;
      }
   }


   //dependency filter
   if($session['_QZ_QUOTE_DETAILS_']['system'] == 'LANDLORDS')
   {
      $propertyCategory = $session['_YourDetails_']['property_category'];
      if($fieldName == "bussiness_nature")
      {
         if($session['_YourDetails_']['number_of_properties'] == 1 && ($propertyCategory == "Commercial" || $propertyCategory == "Mixed"))
         {
             $businessNature = $session['_YourDetails_']['bussiness_nature'];
                   
             if($fieldType == "LIST")
             {
               if($acceptance == "SKP")
               {
                  if(in_array($businessNature,$filterValue))   
                    return false;
                  else
                    return true;
               }

               if($acceptance == "ACC")
               {
                  if(! in_array($businessNature,$filterValue))
                    return false;
                  else
                    return true;
               }
             }
         }
      return true;
      }
   }

      //dependency filter
      if($session['_QZ_QUOTE_DETAILS_']['system'] == 'IMPORTCAR')
      {
         if($fieldName == "importcar_year_of_manufacture")
         {
            if($session['_YourDetails_']['do_you_know_year_of_manufacture'] == "Y")
            {
               $yearOfManufacture = $session['_YourDetails_']['importcar_year_of_manufacture'];

               if($fieldType == "LIST")
               {
                  if($acceptance == "SKP")
                  {
                     if(in_array($yearOfManufacture,$filterValue))
                        return false;
                     else
                        return true;
                  }

                  if($acceptance == "ACC")
                  {
                     if(! in_array($yearOfManufacture,$filterValue))
                        return false;
                     else
                        return true;
                  }
               }
            }
            return true;
         }
      }

      if($session['_QZ_QUOTE_DETAILS_']['system'] == 'TRUCK')
      {
         if($fieldName == "year_of_manufacture")
         {
            if($session['_YourDetails_']['number_of_vehicle'] == "1")
            {
               $yearOfManufacture = $session['_YourDetails_']['year_of_manufacture'];

               if($fieldType == "LIST")
               {
                  if($acceptance == "SKP")
                  {
                     if(in_array($yearOfManufacture,$filterValue))
                        return false;
                     else
                        return true;
                  }

                  if($acceptance == "ACC")
                  {
                     if(! in_array($yearOfManufacture,$filterValue))
                        return false;
                     else
                        return true;
                  }
               }
            }
            return true;
         }
      }

      if($session['_QZ_QUOTE_DETAILS_']['system'] == 'DRIVINGSCHOOL')
      {
         if($fieldName == "year_of_manufacture")
         {
            if($session['_YourDetails_']['number_of_cars'] == "1")
            {
               $yearOfManufacture = $session['_YourDetails_']['year_of_manufacture'];

               if($fieldType == "LIST")
               {
                  if($acceptance == "SKP")
                  {
                     if(in_array($yearOfManufacture,$filterValue))
                        return false;
                     else
                        return true;
                  }

                  if($acceptance == "ACC")
                  {
                     if(! in_array($yearOfManufacture,$filterValue))
                        return false;
                     else
                        return true;
                  }
               }
            }
            return true;
         }
      }

   if($session['_QZ_QUOTE_DETAILS_']['system'] == 'DRIVINGSCHOOL')
   {
      if($fieldName == "vehicle_is_fitted_dual_controls")
      {
         if($session['_YourDetails_']['number_of_cars'] == 1)
         {
             $vehicleFittedDualControls = $session['_YourDetails_']['vehicle_is_fitted_dual_controls'];
                   
             if($fieldType == "LIST")
             {
               if($acceptance == "SKP")
               {
                  if(in_array($vehicleFittedDualControls,$filterValue))   
                    return false;
                  else
                    return true;
               }

               if($acceptance == "ACC")
               {
                  if(! in_array($vehicleFittedDualControls,$filterValue))
                    return false;
                  else
                    return true;
               }
             }
         }
		return true;
      }
      
      if($fieldName == "vehicles_fitted_dual_controls")
      {
         if($session['_YourDetails_']['number_of_cars'] > 1)
         {
             $vehiclesFittedDualControls = $session['_YourDetails_']['vehicles_fitted_dual_controls'];
                   
             if($fieldType == "LIST")
             {
               if($acceptance == "SKP")
               {
                  if(in_array($vehiclesFittedDualControls,$filterValue))   
                    return false;
                  else
                    return true;
               }

               if($acceptance == "ACC")
               {
                  if(! in_array($vehiclesFittedDualControls,$filterValue))
                    return false;
                  else
                    return true;
               }
             }
         }
		return true;
      }     
   }
	
   if($session['_QZ_QUOTE_DETAILS_']['system'] == 'CATERINGVAN')
   {
      if($fieldName == "cateringvan_manufacture_year")
      {
         $yearOfManufacture = $session['_YourDetails_']['cateringvan_manufacture_year'];
             
         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if(in_array($yearOfManufacture,$filterValue))   
                 return false;
               else
                 return true;
            }

            if($acceptance == "ACC")
            {
               if(! in_array($yearOfManufacture,$filterValue))
                 return false;
               else
                 return true;
            }
         }
      }
   }
   
      // vehicle_type_courier
      if($fieldName == "vehicle_type_courier")
      {
         $tempArrayOfFileds = array();

         foreach($session['_YourDetails_'] as $fieldNameInSes => $valOfFiledInSes)
         {
            if(preg_match("/vehicle_type_(.*)/isu",$fieldNameInSes,$resOccupationNumbers))
            {
               $noOfField = $resOccupationNumbers[1];

               $vehicleTypeArray =  array ();
               array_push($vehicleTypeArray, $valOfFiledInSes);

               foreach($vehicleTypeArray as  $val)
               {
                  if($valOfFiledInSes == $val  && $val != "")
                     $tempArrayOfFileds[$noOfField] = $valOfFiledInSes;
               }
            }
         }

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               foreach($tempArrayOfFileds as $fName => $fValue)
               {
                  if(in_array($fValue,$filterValue))
                  {
                     return false;
                  }
               }
            }

            if($acceptance == "ACC")
            {
               $isSkipped = 1;
               foreach($tempArrayOfFileds as $fName => $fValue)
               {
                  if(! in_array($fValue,$filterValue))
                  {
                     $isSkipped = 0;
                     break;
                  }
               }

               if($isSkipped)
                  return true;
               else
                  return false;
            }
         }
      }


      //this will accept only the selected details
      if($fieldName == "motor_trade_occupation_only_selected")
      {
         $tempArrayOfFileds = array();

         foreach($session['_YourDetails_'] as $fieldNameInSes => $valOfFiledInSes)
         {
            if(preg_match("/trade_occupation_(.*)/isu",$fieldNameInSes,$resOccupationNumbers))
            {
               $noOfField = $resOccupationNumbers[1];
               if($valOfFiledInSes == "Y")
                  $tempArrayOfFileds[$noOfField] = $valOfFiledInSes;
            }
         }

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               foreach($tempArrayOfFileds as $fName => $fValue)
               {
                  if(in_array($fName,$filterValue))
                  {
                     return false;
                  }
               }
               return true;
            }

            if($acceptance == "ACC")
            {
               $isSkipped = 1;
               foreach($tempArrayOfFileds as $fName => $fValue)
               {
                  if(in_array($fName,$filterValue))
                     $isSkipped = 0;
                  else
                     $isSkipped = 1;
               }

               if($isSkipped)
                  return false;
               else
                  return true;
            }
         }
      }//if($fieldName == "motor_trade_occupation_all")
   
   
      // SKIP if "Lead Source" = EMV
      if($fieldName == "EMV")
      {
         $leadSource = $_COOKIE['EmailTracking'];
        
         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if($leadSource)
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {
               if(! $leadSource)
                  return false;
               else
                  return true;
            }
         }
      }
      
	  
	  /*$server = $session['_QZ_QUOTE_DETAILS_']['SERVER_IP'];
         $userIP = $session["USER_IP"];

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               $filterFlag = true;

               if(in_array($server,$filterValue))
                  $filterFlag = false;

               if(in_array($userIP,$filterValue))
                  $filterFlag = false;

               return $filterFlag;
            }

            if($acceptance == "ACC")
            {
               $filterFlag = true;

               if(! in_array($server,$filterValue))
                  $filterFlag = false;

               if(! in_array($userIP,$filterValue))
                  $filterFlag = false;

               return $filterFlag;
            }*/
	  // only accept if lead source is EMV or SMS
      if($fieldName == "EMV_SMS")
      {
         $leadSource = $_COOKIE['EmailTracking'];
         $server = $session['_QZ_QUOTE_DETAILS_']['SERVER_IP'];
         $userIP = $session["USER_IP"];
		 
         if($fieldType == "LIST")
         {
            if($acceptance == "ACC")
            {
               if((!$leadSource) && (! in_array($server,$filterValue)) && (! in_array($userIP,$filterValue)))
                  return false;
               else
                  return true;
            }
         }
      }
      
      // SKIP / ACCEPT only if "Taxi plating authority" = see attached
      if($fieldName == "taxi_plating")
      {
         $licesingAuthority = strtoupper($session['_YourDetails_']['plating_authority']);
                 
         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if(in_array($licesingAuthority,$filterValue))   
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {
               if(! in_array($licesingAuthority,$filterValue))
                  return false;
               else
                  return true;
            }
         }
      }
      
      
      // SKIP / ACCEPT only if "Taxi plating authority" = see attached
      if($fieldName == "plating_authority")
      {
         $licesingAuthority = strtoupper($session['_YourDetails_']['plating_authority']);
                 
         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if(in_array($licesingAuthority,$filterValue))   
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {
               if(! in_array($licesingAuthority,$filterValue))
                  return false;
               else
                  return true;
            }
         }
      }


      if($fieldName == "vehicle_type_select_vt")
      {     
			//if($session['_YourDetails_']['number_of_vehicle'] == '1')
			{ 			 
				$vehTypeSelectVT = $session['_YourDetails_']['vehicle_type_select'];
				if($fieldType == "LIST")
				{
					if($acceptance == "SKP")
					{
					   if(in_array($vehTypeSelectVT,$filterValue))   
						  return false;
					   else
						  return true;
					}

					if($acceptance == "ACC")
					{
					   if(! in_array($vehTypeSelectVT,$filterValue))
						  return false;
					   else
						  return true;
					}
				}
			}
			return true;
      }	
  
      if($fieldName == "type_of_driving_licence_tdl")
	  {
			$typeOfDrivingLicence = $session['_YourDetails_']['driving_licence'];
			if($fieldType == "LIST")
			{
				if($acceptance == "SKP")
				{
				   if(in_array($typeOfDrivingLicence,$filterValue))   
					  return false;
				   else
					  return true;
				}

				if($acceptance == "ACC")
				{
				   if(! in_array($typeOfDrivingLicence,$filterValue))
					  return false;
				   else
					  return true;
				}
			}
      }	

    if($fieldName == "property_listed_custom")
    {
        $propListed = $session['_YourDetails_']['property_listed'];

        switch($propListed)
        {
          case 'N':
          $propListed = "1";
          break;
          case '1':
          $propListed = "2";
          break;
          case '2':
          $propListed = "3";
          break;
          case '3':
          $propListed = "4";
          break;
          case '4':
          $propListed = "8";
          break;
          case '5':
          $propListed = "10";
          break;
          case '7':
          $propListed = "5";
          break;
          case '8':
          $propListed = "6";
          break;
          case '9':
          $propListed = "7";
          break;
          case '10':
          $propListed = "9";
          break;
        }

        if($fieldType == "LIST")
        {
          if($acceptance == "SKP")
          {
             if(in_array($propListed,$filterValue))   
              return false;
             else
              return true;
          }

          if($acceptance == "ACC")
          {
             if(! in_array($propListed,$filterValue))
              return false;
             else
              return true;
          }
        }
      }

		
		// SKIP / ACCEPT ABI CODES = see attached
      if($fieldName == "ABI_CODE")
      {
         $vehicleAbiCode = $session['_YourDetails_']['vehicle_confirm'];

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if(in_array($vehicleAbiCode,$filterValue))   
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {
               if(! in_array($vehicleAbiCode,$filterValue))
                  return false;
               else
                  return true;
            }
         }
      }
	

      // type of boat filter
      if($fieldName == "boat_type")
      {
         $boatType = $session['_YourDetails_']['type_of_boat'];

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if(in_array($boatType,$filterValue))
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {
               if(! in_array($boatType,$filterValue))
                  return false;
               else
                  return true;
            }
         }
      }



      // caravan make tourer
      if($fieldName == "caravan_make_tourer")
      {
         $caravanMake = $_SESSION["_YourDetails_"]["caravan_make_tourer"];

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if(in_array($caravanMake,$filterValue))
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {
               if(! in_array($caravanMake,$filterValue))
                  return false;
               else
                  return true;
            }
         }
      }
      
      // SKIP OR ACCEPT WL USER ID FROM ARRAY
      if($fieldName == "WLID_NO")
      {
         $wlUserID = $session["_QZ_QUOTE_DETAILS_"]["wlUserID"];
                 
         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if(in_array($wlUserID,$filterValue))   
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {
               if(! in_array($wlUserID,$filterValue))
                  return false;
               else
                  return true;
            }
         }
      }
      
      
      // SKIP OR ACCEPT  web service
      //if "webservice ID" = "5fdbf8c1a05cd4cdf82ed72104896c7e"
      if($fieldName == "WS_ID")
      {
         $wlUserID = $session['_QZ_QUOTE_DETAILS_']['ws_id'];
                 
         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if(in_array($wlUserID,$filterValue))   
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {
               if(! in_array($wlUserID,$filterValue))
                  return false;
               else
                  return true;
            }
         }
      }
      
            
      // SKIP SERVER 13 IP GENERIC
      if($fieldName == "server_13_filter")
      {
         $server = $session['_QZ_QUOTE_DETAILS_']['SERVER_IP'];
         $userIP = $session["USER_IP"];

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               $filterFlag = true;

               if(in_array($server,$filterValue))
                  $filterFlag = false;

               if(in_array($userIP,$filterValue))
                  $filterFlag = false;

               return $filterFlag;
            }

            if($acceptance == "ACC")
            {
               $filterFlag = true;

               if(! in_array($server,$filterValue))
                  $filterFlag = false;

               if(! in_array($userIP,$filterValue))
                  $filterFlag = false;

               return $filterFlag;
            }
         }
      }

      // MOTOR FLEET --> SKIP / ACCEPT if Business Type contains a word
      if($fieldName == "business_type_contains")
      {
         $bussinessType     = strtoupper($_SESSION['_YourDetails_']['business_type']);
      
         $businessFilter = 0;
         foreach($filterValue as $bussinessTypeToCheck)
         {
            if(preg_match("/\b($bussinessTypeToCheck)\b/", $bussinessType))
            {
               $businessFilter = 1;
               break;
            }
         }
      
         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            { 
               if($businessFilter == 1)
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {                              
               if($businessFilter == 0)
                  return false;
               else
                  return true;
            }
         }
      }


       //General filter for words contains.(field_contains_any_field)
       $arrFieldsNames = explode(' ',$fieldName);
       if($arrFieldsNames[0] == 'field_contains')
       {
           $valToCheck = $arrFieldsNames[1];
           $valToCheck     = trim(strtoupper($_SESSION['_YourDetails_'][$valToCheck]));

           //this will pass if parameter is empty for hidden questions
           $fleetType     = $_SESSION['_YourDetails_']['fleet_type'];
           if($fleetType != "BF")
               return true;

           $containsFilter = 0;
           foreach($filterValue as $valNameFromFilters)
           {
               if(preg_match("/\b($valNameFromFilters)\b/", $valToCheck))
               {
                   $containsFilter = 1;
                   break;
               }
           }

           if($fieldType == "LIST")
           {
               if($acceptance == "SKP")
               {
                   if($containsFilter == 1)
                       return false;
                   else
                       return true;
               }

               if($acceptance == "ACC")
               {
                   if($containsFilter == 0)
                       return false;
                   else
                       return true;
               }
           }
       }


      if($fieldName == "veh_model_limo_contains")
      {
         $limoModel     = strtoupper($_SESSION['_YourDetails_']['limousine_model']);



         $businessFilter = 0;
         foreach($filterValue as $limoModelToCheck)
         {
            if(preg_match("/\b($limoModelToCheck)\b/", $limoModel))
            {
               $businessFilter = 1;
               break;
            }
         }
      
         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            { 
               if($businessFilter == 1)
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {                              
               if($businessFilter == 0)
                  return false;
               else
                  return true;
            }
         }
      } 

      if($fieldName == "veh_make_limo_contains")
      {
         $limoMake     = strtoupper($_SESSION['_YourDetails_']['limousine_make']);
      
         $businessFilter = 0;
         foreach($filterValue as $limoMakeToCheck)
         {
            if(preg_match("/\b($limoMakeToCheck)\b/", $limoMake))
            {
               $businessFilter = 1;
               break;
            }
         }
      
         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            { 
               if($businessFilter == 1)
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {                              
               if($businessFilter == 0)
                  return false;
               else
                  return true;
            }
         }
      }      
      
      // Truck --> SKIP / ACCEPT if Vehicle Type contains a word
      if($fieldName == "other_vehicle_type_match")
      {
         $OtherVehType     = strtoupper($_SESSION['_YourDetails_']['other_vehicle_type']);
      
         $businessFilter = 0;
         
         foreach($filterValue as $OtherVehTypeToCheck)
         {
            if(preg_match("/$OtherVehTypeToCheck/isU", $OtherVehType))
            {
               $businessFilter = 1;            
               break;               
            }
         }
      
         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            { 
               if($businessFilter == 1)
                  return false;
               else
                  return true;
            }


            if($acceptance == "ACC")
            {                              
               if($businessFilter == 0)
                  return false;
               else
                  return true;
            }
         }
      } 		

      if($fieldName == "other_business_trade_contains")
      {

         $bussinessType    = strtoupper($_SESSION['_YourDetails_']['other_business_trade']);

         if ($fieldType == "LIST")
         {
            if ($acceptance == "SKP")
            {
               foreach($filterValue as $value)
               {
                  if ((strpos(strtoupper($bussinessType), strtoupper($value))) !== false)
                  {
                     return false;
                  }
               }
            }
         }
      }

      // SALON  --> SKIP / ACCEPT if OTHER Business Type contains a word
      if($fieldName == "other_business_trade_salon")
      {

         $bussinessType     = strtoupper($_SESSION['_YourDetails_']['other_business']);

         $businessFilter = 0;
         foreach($filterValue as $bussinessTypeToCheck)
         {
            if(preg_match("/$bussinessTypeToCheck/isU", $bussinessType))
            {
               $businessFilter = 1;
               break;
            }
         }

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if($businessFilter == 1)
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {
               if($businessFilter == 0)
                  return false;
               else
                  return true;
            }
         }
      }

      // MOTOR FLEET --> SKIP / ACCEPT if Business Type contains a word in a string
      if($fieldName == "business_type_contains_other_words")
      {
         $bussinessType     = strtoupper($_SESSION['_YourDetails_']['business_type']);

         $businessFilter = 0;
         foreach($filterValue as $bussinessTypeToCheck)
         {
            if(preg_match("/$bussinessTypeToCheck/isU", $bussinessType))
            {
               $businessFilter = 1;
               break;
            }
         }

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if($businessFilter == 1)
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {
               if($businessFilter == 0)
                  return false;
               else
                  return true;
            }
         }
      }
	  
	  // MOTOR TRADE --> SKIP / ACCEPT if other_trade contains words in a string
      if($fieldName == "mt_other_trade")
      {
         $bussinessType     = strtoupper($_SESSION['_YourDetails_']['other_trade']);

         $businessFilter = 0;
         foreach($filterValue as $bussinessTypeToCheck)
         {
			$bussinessTypeToCheck = str_replace(' ', '\s', $bussinessTypeToCheck);
            if(preg_match("/$bussinessTypeToCheck/isU", $bussinessType))
            {
               $businessFilter = 1;
               break;
            }
         }

         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            {
               if($businessFilter == 1)
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {
               if($businessFilter == 0)
                  return false;
               else
                  return true;
            }
         }
      }
      
      // MOTOR FLEET --> SKIP / ACCEPT if Business Type contains a word
      if($fieldName == "business_type_contains_acc_empty")
      {
         $bussinessType     = strtoupper($_SESSION['_YourDetails_']['business_type']);
         
         //this will pass if parameter is empty for hidden questions
         if(!isset($bussinessType) || $bussinessType == '')
            return true;
        
         $businessFilter = 0;
         foreach($filterValue as $bussinessTypeToCheck)
         {
            if(preg_match("/\b($bussinessTypeToCheck)\b/", $bussinessType))     
            {
               $businessFilter = 1;
               break;
            }
         }
      
         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            { 
               if($businessFilter == 1)
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {                              
               if($businessFilter == 0)
                  return false;
               else
                  return true;
            }
         }
      }
      
      // SKIP -- ACC  if "Main business type" = "MOTOR TRADE"...
      if($fieldName == "BUSINESS_TYPE")
      {
         $bussinessType     = strtoupper($_SESSION['_YourDetails_']['business_type']);
      
         $businessFilter = 0;
         foreach($filterValue as $bussinessTypeToCheck)
         {
            if($bussinessTypeToCheck == $bussinessType)     
            {
               $businessFilter = 1;
               break;
            }
         }
      
         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            { 
               if($businessFilter == 1)
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {                              
               if($businessFilter == 0)
                  return false;
               else
                  return true;
            }
         }
      }
      
      
      // SKIP -- ACC  if "BUSINESS_TRADE_empl" = ""...
      if($fieldName == "BUSINESS_TRADE_empl")
      {
         $bussinessType     = strtoupper($_SESSION['_YourDetails_']['business_trade']);
      
         $businessFilter = 0;
         foreach($filterValue as $bussinessTypeToCheck)
         {
            if($bussinessTypeToCheck == $bussinessType)     
            {
               $businessFilter = 1;
               break;
            }
         }
      
         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            { 
               if($businessFilter == 1)
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {                              
               if($businessFilter == 0)
                  return false;
               else
                  return true;
            }
         }
      }
      
      
      // Holiday Home: SKIP -- ACC  if "Main business type" = "MOTOR TRADE"...
      if($fieldName == "business_trade")
   {
      $bussinessType     = $_SESSION['_YourDetails_']['business_trade'];
      if($systemName == "PUBLICLIABILITY")
         if($bussinessType == "other")
            $bussinessType = "Other - Not Listed";

      $bussinessType     = strtolower($bussinessType);
      $bussinessType     = ucwords($bussinessType);

      $businessFilter = 0;
      foreach($filterValue as $bussinessTypeToCheck)
      {
         $bussinessTypeToCheck = strtolower($bussinessTypeToCheck);
         $bussinessTypeToCheck = ucwords($bussinessTypeToCheck);

         if($bussinessTypeToCheck == $bussinessType)
         {
            $businessFilter = 1;
            break;
         }
      }

      if($fieldType == "LIST")
      {
         if($acceptance == "SKP")
         {
            if($businessFilter == 1)
               return false;
            else
               return true;
         }

         if($acceptance == "ACC")
         {
            if($businessFilter == 0)
               return false;
            else
               return true;
         }
      }
   }

      //Shop: SKIP if Business Name Contains
       if($fieldName == "business_name"){
           {
               $businessName    = strtoupper($_SESSION['_YourDetails_']['business_name']);

               if ($fieldType == "LIST")
               {
                   if ($acceptance == "SKP")
                   {
                       foreach($filterValue as $value)
                       {
                           if ((strpos(strtoupper($businessName), strtoupper($value))) !== false)
                           {
                               return false;
                           }
                       }
                   }
               }
           }
       }

      //print "Field Name is : $fieldName <br>";
      if($fieldName == "number_of_convictions_sk")
      {

         $noOfConvictions= 0;

         for($k=1;$k<=4;$k++)
         {
            if($_SESSION["_YourDetails_"]["conviction_type_" . $k] AND $_SESSION["_YourDetails_"]["date_of_conviction_" . $k])
               $noOfConvictions++;
         }

         //print "No of convictions : [$noOfConvictions] -> filter is : [$acceptance -> $valueType -> $filterValue] <br>";


         //aici Alex
         if($acceptance == "SKP")
         {
            if($valueType == "MIN")
            {
               if($noOfConvictions >= $filterValue)
                  return false;
               else
                  return true;
            }

            if($valueType == "MAX")
            {
               if($noOfConvictions <= $filterValue)
                  return false;
               else
                  return true;
            }
         }

         if($acceptance == "ACC")
         {
            if($valueType == "MAX")
            {
               if($noOfConvictions > $filterValue)
                  return false;
               else
                  return true;
            }

            if($valueType == "MIN")
            {
               if($noOfConvictions < $filterValue)
                  return false;
               else
                  return true;
            }
         }
      }//end number_of_convictions_sk

      // bespoke filters on Business Trade / Annual Turnover
      $annualTurnover = $session["_YourDetails_"]["annual_turnover"];
      $businessTrade  = $session['_YourDetails_']['business_trade'];
      $businessTrade  = strtolower($businessTrade);
      $businessTrade  = ucwords($businessTrade);

      if($fieldName == "business_trade_ann_turnover")
      {         
         $businessFilter = 0;
         foreach($filterValue as $bussinessTypeToCheck => $annualTurn)
         {

            $bussinessTypeToCheck = strtolower($bussinessTypeToCheck);
            $bussinessTypeToCheck = ucwords($bussinessTypeToCheck);

            if($bussinessTypeToCheck == $businessTrade && $annualTurnover < $annualTurn)     
            {
               $businessFilter = 1;
               break;
            }
         }
      
         if($fieldType == "LIST")
         {
            if($acceptance == "SKP")
            { 
               if($businessFilter == 1)
                  return false;
               else
                  return true;
            }

            if($acceptance == "ACC")
            {                              
               if($businessFilter == 0)
                  return false;
               else
                  return true;
            }
         }
      }
      
      //we look at the values to filter
      if($fieldType == "LIST")
      {
         if(! is_array($filterValue))
         {
            return false;
         }

         if($acceptance == "ACC")
         {
            if(! in_array($valueToCompare, $filterValue))
            {
               return false;
            }
         }

         if($acceptance == "SKP")
         {
            if(in_array($valueToCompare, $filterValue))
            {
               return false;
            }
         }
      }//end if($fieldType == "LIST")

           

      //we look at the value types:
      if($fieldType == "VAL")
      {
         if($acceptance == "ACC")
         {
            if($fieldName == "estimated_wedding_cost")
               $valueToCompare = str_replace(",","",$valueToCompare);

            if($valueType == "MIN")
            {
               if(($fieldName == "property_value") || ($fieldName == "contents_value"))
               {
                  if($valueToCompare == "")
                     return true;
               }

               if($valueToCompare < $filterValue)
               {
                  return false;
               }
            }

            if($valueType == "MAX")
            {
               if($valueToCompare > $filterValue)
               {
                  return false;
               }
            }

            if($valueType == "EQL")
            {
               if($valueToCompare != $filterValue)
               {
                  return false;
               }
            }

         }
         else
         {
            if($acceptance == "SKP")
            {
               if($valueType == "MIN")
               {
                  if(($fieldName == "property_value") || ($fieldName == "contents_value"))
                  {
                     if($valueToCompare == "")
                        return true;
                  }

                  if($valueToCompare > $filterValue)
                  {
                     return false;
                  }
               }

               if($valueType == "MAX")
               {
                  if($valueToCompare < $filterValue)
                  {
                     return false;
                  }
               }

               if($valueType == "EQL")
               {
                  if($valueToCompare == $filterValue)
                  	return false;
               }
               if($valueType == "BLK"){
               		if(empty($valueToCompare)){
               			return false;
               		}
               }
            }
         }
      }//end if($fieldType == "VAL")
   }

   return true;

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAge
//
// [DESCRIPTION]:   Returns the drivers age
//
// [PARAMETERS]:
//
// [RETURN VALUE]:
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAge($birthDate, $curDate)
{
    list($birthYear,$birthMonth,$birthDay) = explode("-", $birthDate);
    list($curYear,$curMonth,$curDay)       = explode("-", $curDate);

    $yearDiff  = $curYear  - $birthYear;
    $monthDiff = $curMonth - $birthMonth;
    $dayDiff   = $curDay   - $birthDay;

    if(($monthDiff < 0) || ($monthDiff == 0 && $dayDiff < 0))
      $yearDiff--;

    //echo "<--! $birthDate $curDate [$birthYear-$birthMonth-$birthDay] [$curYear-$curMonth-$curDay] -->\n";
    return $yearDiff;
}

}//end class CSkipValidator


?>

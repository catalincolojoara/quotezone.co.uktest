<?php

/*****************************************************************************/
/*                                                                           */
/*  CLogRotatingCompaniesLg class interface                                  */
/*                                                                           */
/*  (C) 2017 Alexandru Furtuna (alexandru.furtuna@seopa.com)                 */
/*                                                                           */
/*****************************************************************************/
include_once "globals.inc";
include_once "MySQL.php";

//////////////////////////////////////////////////////////////////////////////PE
//
// [CLASS NAME]:   CLogRotatingCompaniesLg
//
// [DESCRIPTION]:  CLogRotatingCompaniesLg class interface
//
// [FUNCTIONS]:    void Initialise();
//                 bolean 
//
//
// [CREATED BY]:   Alexandru Furtuna (alexandru.furtuna@seopa.com) 2017-03-17
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CLogRotatingCompaniesLg
{

   var $objMySql;            // CMySQL class object
   var $strERR;              // last Log error string
   var $closeDB;             // close db variable

   /*
   CREATE TABLE `rotated_companies_lg` (
      `id` bigint(20) unsigned NOT NULL auto_increment,
      `log_id` bigint(20) unsigned NOT NULL default '0',
      `site_id` bigint(20) unsigned NOT NULL default '0',
      `lead_price` float(4,2) NOT NULL DEFAULT '0.00',
      `date` date default '0000-00-00',
      `time` time default '00:00:00',
      PRIMARY KEY  (`id`),
      KEY `log_id` (`log_id`),
      KEY `site_id` (`site_id`)
   ) ENGINE=InnoDB

    */

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CLogRotatingCompaniesLg
   //
   // [DESCRIPTION]:   Class constructor
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Alexandru Furtuna (alexandru.furtuna@seopa.com) 2012-04-11
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CLogRotatingCompaniesLg($dbh)
   {
      if($dbh)
      {
         $this->objMySql  = $dbh;
         $this->closeDB = false;
      }
      else
      {
         // default configuration
         $this->objMySql = new CMySQL();

         if(! $this->objMySql->Open(DBNAME, DBHOST, DBUSER, DBPASS))
         {
           $this->strERR = $this->dbh->GetError();
           return;
         }

         $this->closeDB = true;
      }

      $this->strERR = "";
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AssertRotatingCompaniesLg
   //
   // [DESCRIPTION]:   Checks the field values
   //
   // [PARAMETERS]:    $quoteTypeID, $logID, $loadTime, $serverIP, $date'
   //
   // [RETURN VALUE]:  true/false
   //
   // [CREATED BY]:    Alexandru Furtuna (alexandru.furtuna@seopa.com) 2017-03-17
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AssertRotatingCompaniesLg($logID=0,$siteID=0,$leadPrice='0.00')
   {

      if(! preg_match("/\d+/",$logID))
      {
         $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");
         return false;
      }

      if(! preg_match("/\d+/",$siteID))
      {
         $this->strERR = GetErrorString("INVALID_SITE_ID_FIELD");
         return false;
      }

      if(! preg_match("/\d+/",$leadPrice))
      {
         $this->strERR = GetErrorString("INVALID_LEAD_PRICE_FIELD");
         return false;
      }

      return true;

   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AddRotatingCompaniesLgToDb
   //
   // [DESCRIPTION]:   Adds the lead quote times to the databse
   //
   // [PARAMETERS]:    $quoteTypeID, $logID, $loadTime, $serverIP, $date'
   //
   // [RETURN VALUE]:  0 or ID
   //
   // [CREATED BY]:    Alexandru Furtuna (alexandru.furtuna@seopa.com) 2017-03-17
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AddRotatingCompaniesLgToDb($logID=0,$siteID=0,$leadPrice='0.00')
   {

      if(! $this->AssertRotatingCompaniesLg($logID,$siteID,$leadPrice))
      {
         $this->GetError();
         return 0;
      }

      $date = date("Y-m-d");
      $time = date("H:i:s");

      $this->lastSQLCMD = "INSERT INTO rotated_companies_lg VALUES ('','$logID','$siteID','$leadPrice','$date','$time')";

      //print $this->lastSQLCMD."<br>";

      if(! $this->objMySql->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->objMySql->GetError();
			return 0;
		}

		$this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

		if(! $this->objMySql->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->objMySql->GetError();
			return 0;
		}

		if(! $this->objMySql->FetchRows())
		{
			$this->strERR = $this->objMySql->GetError();
			return 0;
		}

		return $this->objMySql->GetFieldValue("id");
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetLeadsQuoteTimeByQuoteTypeIDAndDate()
   //
   // [DESCRIPTION]:   Returns the details by specified fields
   //
   // [PARAMETERS]:    $quoteTypeID, $date
   //
   // [RETURN VALUE]:  false / details
   //
   // [CREATED BY]:    Alexandru Furtuna (alexandru.furtuna@seopa.com) 2017-03-17
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetRotatingCompaniesLgByDate($date='0000-00-00')
   {

      if(! preg_match("/\d+/",$quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
         return false;
      }
      
      if(empty($date))
      {
         $this->strERR .= GetErrorString("INVALID_DATE_FIELD");
         return false;
      }

      $this->lastSQLCMD = "SELECT count(site_id) as total_rotated, site_id, lead_price,date  FROM rotated_companies_lg WHERE date = '$date' GROUP BY site_id,lead_price";

      if(! $this->objMySql->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->objMySql->GetError();
			return false;
		}

      if(! $this->objMySql->GetRows())
      {
         $this->strERR = GetErrorString("DETAILS_NOT_FOUND");
         return false;
      }

      $index = 0;
      while($this->objMySql->MoveNext())
      {
         $siteID = $this->objMySql->GetFieldValue("site_id");

         $arrayResult[$siteID]["total_rotated"] = $this->objMySql->GetFieldValue("total_rotated");
         $arrayResult[$siteID]["lead_price"]    = $this->objMySql->GetFieldValue("lead_price");
      }
 
      return $arrayResult;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetLeadsQuoteTimeByQuoteTypeIDAndDate()
   //
   // [DESCRIPTION]:   Returns the details by specified fields
   //
   // [PARAMETERS]:    $quoteTypeID, $date
   //
   // [RETURN VALUE]:  false / details
   //
   // [CREATED BY]:    Alexandru Furtuna (alexandru.furtuna@seopa.com) 2017-03-17
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetRotatingCompaniesLgByDateInterval($startDate='0000-00-00',$endDate='0000-00-00')
   {

      if(! preg_match("/\d+/",$quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
         return false;
      }
      
      if(empty($date))
      {
         $this->strERR .= GetErrorString("INVALID_DATE_FIELD");
         return false;
      }

      $this->lastSQLCMD = "SELECT count(site_id) as total_rotated, site_id, lead_price,date  FROM rotated_companies_lg WHERE date >= '$startDate' AND date <= '$endDate' GROUP BY site_id,lead_price";

      if(! $this->objMySql->Exec($this->lastSQLCMD))
      {
         $this->strERR = $this->objMySql->GetError();
         return false;
      }

      if(! $this->objMySql->GetRows())
      {
         $this->strERR = GetErrorString("DETAILS_NOT_FOUND");
         return false;
      }

      $index = 0;
      while($this->objMySql->MoveNext())
      {
         $siteID = $this->objMySql->GetFieldValue("site_id");

         $arrayResult[$siteID]["total_rotated"] = $this->objMySql->GetFieldValue("total_rotated");
         $arrayResult[$siteID]["lead_price"]    = $this->objMySql->GetFieldValue("lead_price");
      }
 
      return $arrayResult;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetError
   //
   // [DESCRIPTION]:   Prints the error details
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  error
   //
   // [CREATED BY]:    Alexandru Furtuna (alexandru.furtuna@seopa.com) 2017-03-17
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetError()
   {
      //print $this->strERR;
      return $this->strERR;
   }

   
}

?>

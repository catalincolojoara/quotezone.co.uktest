<?php
/*****************************************************************************/
/*                                                                           */
/*  CLeadsCompanyEmailsModule class interface                                */
/*                                                                           */
/*  (C) 2011 Moraru Valeriu (vali@acrux.biz)                                 */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";
//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CLeadsCompanyEmailsModule
//
// [DESCRIPTION]:  CLeadsCompanyEmailsModule class interface
//
// [FUNCTIONS]:      array | false    GetEmailsToSendForSiteID($siteID="")
// 
//                   Close()
//                   GetError()
//                   ShowError()
//
// [CREATED BY]:   Moraru Valeriu (vali@acrux.biz) 2011-04-20
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CLeadsCompanyEmailsModule
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last Status error string

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CLeadsCompanyEmailsModule
   //
   // [DESCRIPTION]:   Default class constructor. Initialization goes here.
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2011-04-20
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CLeadsCompanyEmailsModule($dbh=0)
   {
      if($dbh)
      {
         $this->dbh     = $dbh;
         $this->closeDB = false;
      }
      else
      {
         // default configuration
         $this->dbh = new CMySQL();

         if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
         {
            $this->strERR = $this->dbh->GetError();

            return;
         }

         $this->closeDB = true;
      }

      $this->strERR  = "";
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AssertAddEmailForCompany
   //
   // [DESCRIPTION]:   Checks fields for the AddEmailForCompany
   //
   // [PARAMETERS]:    $siteID='',$email='',$type='',$status='',$description=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2011-04-20
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AssertAddEmailForCompany($siteID='',$email='',$type='',$status='',$description='')
   {
      if(! preg_match("/^\d+$/", $siteID))
      {
         $this->strERR = GetErrorString("INVALID_SITE_ID");
         return false;
      }

      if(! preg_match("/^[A-Za-z0-9]{1}((\.)?[A-Za-z0-9_-]+)*@[A-Za-z0-9-\.]+(\.[A-Za-z]{2,4})$/",$email))
      {
         $this->strERR = GetErrorString("INVALID_EMAIL");
         return false;
      }

      if( trim($type) != "TECHNICAL" AND trim($type) != "MARKETING" AND trim($type) != "COMPANY")
      {
         $this->strERR = GetErrorString("INVALID_EMAIL_TYPE");
         return false;
      }

      if( trim($status) != "ON" AND trim($status) != "OFF" )
      {
         $this->strERR = GetErrorString("INVALID_EMAIL_STATUS");
         return false;
      }

      return true;

   }//end function AssertAddOfflineLeads

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AddEmailForCompany
   //
   // [DESCRIPTION]:   Adds the emails details to the database
   //
   // [PARAMETERS]:    $siteID='',$email='',$type='',$status='',$description=''
   //
   // [RETURN VALUE]:  Id or 0 in case of failure
   //
   // [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2011-04-20
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AddEmailForCompany($siteID='',$email='',$type='',$status='',$description='')
   {
      if(! $this->AssertAddEmailForCompany($siteID,$email,$type,$status,$description))
         return false;

      $sqlCMD = "INSERT INTO ".SQL_LEADS_COMPANY_EMAILS." (site_id,email,type,status,description) VALUES ('".$siteID."','".$email."','".$type."','".$status."','".$description."') ";

      //print "SQL IS  : $sqlCMD<br>";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }

      $sqlCMD = "SELECT LAST_INSERT_ID() AS id";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }

      return $this->dbh->GetFieldValue("id");

   }//end function AddEmailForCompany

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: UpdateEmailForCompany
   //
   // [DESCRIPTION]:   Update the details to the database
   //
   // [PARAMETERS]:    $siteID='',$email='',$type='',$status='',$description=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2011-04-20
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function UpdateEmailForCompany($siteID='',$email='',$type='',$status='',$description='')
   {
      /*
      if(! $this->AssertAddEmailForCompany($siteID,$email,$type,$status,$description))
         return false;

      $sqlCMD = "UPDATE ".SQL_LEADS_COMPANY_EMAILS." SET  qz_quote_type_id = '$qzQuoteType' ,site_id = '$siteID',time_start = '$startTime',date_start = '$startDate',time_end = '$endTime',date_end = '$endDate' ,message = '$message', offline_flag = '$offlineFlag' WHERE id='$id'";

      //print "SQL IS  : $sqlCMD<br>";
      
      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }
      
      return true;
      */

   }//end function UpdateEmailForCompany

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetAllEmailsToSendForCompanyBySiteID
   //
   // [DESCRIPTION]:   Returns all the data in the table
   //
   // [PARAMETERS]:    $siteID
   //
   // [RETURN VALUE]:  array | false
   //
   // [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2011-04-20
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetAllEmailsToSendForCompanyBySiteID($siteID='')
   {

      $sqlCmd = "SELECT * FROM ".SQL_LEADS_COMPANY_EMAILS." WHERE status = 'ON' AND site_id =  '".$siteID."' ";

      if(! $this->dbh->Exec($sqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("SET_OFFLINE_NOT_FOUND");
         return false;
      }

      $arrayResult = array();

      while($this->dbh->MoveNext())
      {
         $arrayResult[$this->dbh->GetFieldValue("type")][] = $this->dbh->GetFieldValue("email");
      }

      return $arrayResult;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: Close
   //
   // [DESCRIPTION]:   Close the object and also close the connection with the
   //                  database server if necessary
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function Close()
   {
      if($this->closeDB)
         $this->dbh->Close();

      return;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetError
   //
   // [DESCRIPTION]:   Retrieve the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetError()
   {
      return $this->strERR;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetError
   //
   // [DESCRIPTION]:   Retrieve the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function ShowError()
   {
      print $this->strERR;
   }

}

?>
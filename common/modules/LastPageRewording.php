<?php
/*****************************************************************************/
/*                                                                           */
/*  CLastPageRewording class interface                                       */
/*                                                                           */
/*  (C) 2012 Alexandru Furtuna (alex@acrux.biz)                              */
/*                                                                           */
/*****************************************************************************/
include_once "globals.inc";

//////////////////////////////////////////////////////////////////////////////PE
//
// [CLASS NAME]:   CLastPageRewording
//
// [DESCRIPTION]:  CLastPageRewording class interface
//
// [FUNCTIONS]:    void Initialise();
//
//
//
// [CREATED BY]:   Alexandru Furtuna (alex@acrux.biz) 2012-05-03
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CLastPageRewording
{

   var $productsArray;


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CLastPageRewording
//
// [DESCRIPTION]:   Class constructor
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-05-03
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CLastPageRewording()
{

   $this->productsArray = array(
      "10" => "Caravan",
      "11" => "Motorhome",
      "12" => "Life",
      "13" => "Health",
      "14" => "Campervan",
      "15" => "Trailer-tent",
      "16" => "Boat",
      //"17" => "energy",
      "18" => "Taxi",
      "19" => "Minibus",
      "20" => "Limo",
      "21" => "Coach",
      "22" => "Horsebox",
      "23" => "Icecream Van",
      "24" => "Catering Van",
      "25" => "Driving School",
      "26" => "Motortrade",
      "27" => "Park Home",
      "28" => "Landlords",
      "29" => "Courier",
      "30" => "Gap",
      "31" => "Motor-Fleet",
      "32" => "Taxi-Fleet",
      "33" => "Truck",
      "34" => "Office",
      "35" => "Shop",
      "36" => "Pub",
      "37" => "Surgery",
      "38" => "Polish",
      "39" => "Import Car",
      "40" => "Professional Indemnity",
      "41" => "Kit Car",
      "42" => "Tradesman",
      "43" => "4x4",
      "44" => "Classic Car",
      "45" => "Income Protection",
      "46" => "Public Liability",
      "47" => "Convicted Driver",
      "48" => "Private Medical Insurance",
      "50" => "Mortgage Protection",
      "51" => "Carehome",
      "52" => "Holiday Home",
      "53" => "Disabled Car",
      "54" => "Horse",
      "55" => "Prestige Car",
      "56" => "Performance Car",
      "57" => "Modified Car",
      "58" => "Hotel",
      "59" => "Salon",
      "60" => "Young Driver",
      "61" => "Mortgages",
      "62" => "Investments",
      "63" => "Debt help",
      "64" => "Annuities",
      "65" => "Pensions",
      "66" => "Loans",
      "67" => "Employers Liability",
      "68" => "Non-Standard Home",
      "69" => "Quad",
      "70" => "Restaurant",
      "71" => "Multi Car",
      "72" => "Bicycle",
      "73" => "Goods In Transit",
      "74" => "Wedding",
      "75" => "Unoccupied Property",
      "76" => "Event",
      "77" => "Sports",
      "78" => "Directors And Officers",
      "79" => "Market Trader",
      "80" => "Trailer",
      "81" => "Commercial Property",
      "82" => "Farm Vehicle",
   );

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: PrepareText
//
// [DESCRIPTION]:   Prepares the text to be shown on the last page based on preset details and system 
//
// [PARAMETERS]:    $session , $systemID
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-05-03
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function PrepareText($session= array(),$systemID=0)
{

   //prepare time interval
  "A  00.01 – 06.59 (Monday, Tuesday, Wednesday, Thursday, Friday)
   B  07.00 – 18.29 (Monday, Tuesday, Wednesday, Thursday, Friday)
   C  18.30 – 00.00 (Monday, Tuesday, Wednesday, Thursday)
   D  18.30 – 00.00 (Friday)
   W  Saturday / Sunday – all day";

   $timeInt  = "";
   $dayNo    = date("N");
   $houreMin = date("Hi");

   switch($dayNo)
   {
      case '1':
      case '2':
      case '3':
      case '4':
         if($houreMin > '0000' AND $houreMin <= '0659')
            $timeInt = "A";

         if($houreMin >= '0700' AND $houreMin <= '1829')
            $timeInt = "B";

         if($houreMin >= '1830' AND $houreMin <= '2359')
            $timeInt = "C";

      break;

      case '5':
         if($houreMin > '0000' AND $houreMin <= '0659')
            $timeInt = "A";

         if($houreMin >= '0700' AND $houreMin <= '1829')
            $timeInt = "B";

         if($houreMin >= '1830' AND $houreMin <= '2359')
            $timeInt = "D";
      break;

      case '6':
      case '7':
         $timeInt = "W";
      break;
   
   }//end switch


   //prepare text message
   $textMessage       = "";
   $noOfSuccessQuotes = 0;

   $firstName         = $session["_YourDetails_"]["first_name"];
   $prodNameToDisplay = $this->productsArray[$systemID];
   $noOfSuccessQuotes = count($session["NOT_SKIPPED_SITES"]);

   switch($timeInt)
   {
      case 'B':
         if($noOfSuccessQuotes > 1)
            $textMessage = "Hi ".$firstName.", <br> We have searched the market of <b>".$prodNameToDisplay."</b> insurance specialists and passed your details to the following panel of <b>".$prodNameToDisplay."</b> insurance providers, who should be in contact shortly.";

         if($noOfSuccessQuotes == 1)
            $textMessage = "Hi ".$firstName.", <br> We have searched the market of <b>".$prodNameToDisplay."</b> insurance specialists and passed your details to the following specialist <b>".$prodNameToDisplay."</b> insurance provider, who should be in contact shortly.";

         if($noOfSuccessQuotes == 0)
            $textMessage = "Hi ".$firstName.", <br> Due to the nature of your risk, online quotes are not available at this time.Alternatively, there are other companies who may be able to provide insurance quotes.";
      break;

      case 'A':
      case 'C':
         if($noOfSuccessQuotes > 1)
            $textMessage = "Hi ".$firstName.", <br> We have searched the market of <b>".$prodNameToDisplay."</b> insurance specialists and passed your details to the following panel of <b>".$prodNameToDisplay."</b> insurance providers.<br><br> Although it is currently out of office hours, our partner companies will endeavour to make contact with you as soon as office hours resume.";

         if($noOfSuccessQuotes == 1)
            $textMessage = "Hi ".$firstName.", <br> We have searched the market of <b>".$prodNameToDisplay."</b> insurance specialists and passed your details to the following specialist <b>".$prodNameToDisplay."</b> insurance provider.<br><br>Although it is currently out of office hours, our partner company will endeavour to make contact with you as soon as office hours resume.";

         if($noOfSuccessQuotes == 0)
            $textMessage = "Hi ".$firstName.", <br> Due to the nature of your risk, online quotes are not available at this time.Alternatively, there are other companies who may be able to provide insurance quotes.";
      break;

      case 'D':
      case 'W':
         if($noOfSuccessQuotes > 1)
            $textMessage = "Hi ".$firstName.", <br> We have searched the market of <b>".$prodNameToDisplay."</b> insurance specialists and passed your details to the following panel of <b>".$prodNameToDisplay."</b> insurance providers.<br><br> Although it is currently out of office hours, our partner companies will endeavour to make contact with you as soon as office hours resume.";

         if($noOfSuccessQuotes == 1)
            $textMessage = "Hi ".$firstName.", <br> We have searched the market of <b>".$prodNameToDisplay."</b> insurance specialists and passed your details to the following specialist <b>".$prodNameToDisplay."</b> insurance provider. <br><br>Although it is currently out of office hours, our partner company will endeavour to make contact with you as soon as office hours resume.";

         if($noOfSuccessQuotes == 0)
            $textMessage = "Hi ".$firstName.", <br> Due to the nature of your risk, online quotes are not available at this time.Alternatively, there are other companies who may be able to provide insurance quotes.";
      break;

   }

   return $textMessage;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: PrepareTextWL4
//
// [DESCRIPTION]:   Prepares the text to be shown on the last page based on preset details and system on WL4 
//
// [PARAMETERS]:    $session,$systemID,$labels
//
// [RETURN VALUE]:  string
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2012-11-26
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function PrepareTextWL4($session= array(),$systemID=0,$rerun_quotes='N',$labels='')
{
   //prepare time interval   
  "A  00.01 – 06.59 (Monday, Tuesday, Wednesday, Thursday, Friday)
   B  07.00 – 18.29 (Monday, Tuesday, Wednesday, Thursday, Friday)
   C  18.30 – 00.00 (Monday, Tuesday, Wednesday, Thursday)
   D  18.30 – 00.00 (Friday)
   W  Saturday / Sunday – all day";

   $timeInt  = "";
   $dayNo    = date("N");
   $houreMin = date("Hi");

   switch($dayNo)
   {
      case '1':
      case '2':
      case '3':
      case '4':
         if($houreMin > '0000' AND $houreMin <= '0659')
            $timeInt = "A";

         if($houreMin >= '0700' AND $houreMin <= '1829')
            $timeInt = "B";

         if($houreMin >= '1830' AND $houreMin <= '2359')
            $timeInt = "C";

      break;

      case '5':
         if($houreMin > '0000' AND $houreMin <= '0659')
            $timeInt = "A";

         if($houreMin >= '0700' AND $houreMin <= '1829')
            $timeInt = "B";

         if($houreMin >= '1830' AND $houreMin <= '2359')
            $timeInt = "D";
      break;

      case '6':
      case '7':
         $timeInt = "W";
      break;
   
   }//end switch


   //prepare text message
   $textMessage       = "";
   $noOfSuccessQuotes = 0;

   $firstName         = $session["_YourDetails_"]["first_name"];
   $prodNameToDisplay = $this->productsArray[$systemID];
   $noOfSuccessQuotes = count($session["NOT_SKIPPED_SITES"]);

   /*
   NEW - For Loans system only

   case 'A':
   case 'B':
   case 'C':
   case 'D':
   case 'W':

   if($noOfSuccessQuotes > 1)
      $textMessage = "Hi $firstName  We have searched the market of $prodNameToDisplay specialists and passed your details to the following panel of $prodNameToDisplay providers, who should be in contact shortly.";

   if($noOfSuccessQuotes == 1)
      $textMessage = "Hi $firstName  We have searched the market of $prodNameToDisplay specialists and passed your details to the following $prodNameToDisplay provider, who should be in contact shortly.";

   if($noOfSuccessQuotes == 0)
      $textMessage = "Hi $firstName  Due to the nature of your enquiry, we could not match you up with a provider from our panel.  Alternatively, there are other companies who may be able to assist.";
   */

   if($systemID == "66")
   {
      switch ($timeInt)
      {
         case 'A':
         case 'B':
         case 'C':
         case 'D':
         case 'W':
            if ($noOfSuccessQuotes > 1)
               $textMessage = "<div class='qrFirstRow'>Hi ". $firstName .", </div><div class='qrSecondRow'>We have searched the market of <b>" . $prodNameToDisplay . "</b> specialists and passed your details to the following panel of <b>". $prodNameToDisplay ."</b> providers, who should be in contact shortly.</div>";

            if ($noOfSuccessQuotes == 1)
               $textMessage = "<div class='qrFirstRow'>Hi ". $firstName .", </div><div class='qrSecondRow'>We have searched the market of <b>" . $prodNameToDisplay . "</b> specialists and passed your details to the following <b>". $prodNameToDisplay ."</b> provider, who should be in contact shortly.</div>";

            if ($noOfSuccessQuotes == 0)
               $textMessage = "<div class='qrFirstRow'>Hi ". $firstName .", </div><div class='qrSecondRow'>Thank you for submitting your <b>".$prodNameToDisplay."</b> request. Unfortunately, based on the details you’ve entered, we have been unable to match you with any of our <b>".$prodNameToDisplay."</b> providers.</div>";
            break;
      }
   }
   else
   {
      switch ($timeInt)
      {
         case 'B':
            if ($noOfSuccessQuotes > 1)
               $textMessage = "<div class='qrFirstRow'>Hi " . $firstName . ", </div><div class='qrSecondRow'>We have searched the market of <b>" . $prodNameToDisplay . "</b> insurance specialists and passed your details to the following panel of <b>" . $prodNameToDisplay . "</b> insurance providers, who should be in contact shortly.</div>";

            if ($noOfSuccessQuotes == 1)
               $textMessage = "<div class='qrFirstRow'>Hi " . $firstName . ", </div><div class='qrSecondRow'>We have searched the market of <b>" . $prodNameToDisplay . "</b> insurance specialists and passed your details to the following specialist <b>" . $prodNameToDisplay . "</b> insurance providers, who should be in contact shortly.</div>";

            if ($noOfSuccessQuotes == 0)
               $textMessage = "<div class='qrFirstRow'>Hi " . $firstName . ", </div><div class='qrSecondRow'>Thank you for submitting your quotation request. Unfortunately, based on the details you’ve entered, we have been unable to match you with any of our <b>".$prodNameToDisplay."</b> insurance providers.</div>";
         break;

         case 'A':
         case 'C':
            if ($noOfSuccessQuotes > 1)
               $textMessage = "<div class='qrFirstRow'>Hi " . $firstName . ", </div><div class='qrSecondRow'>We have searched the market of <b>" . $prodNameToDisplay . "</b> insurance specialists and passed your details to the following panel of <b>" . $prodNameToDisplay . "</b> insurance providers.<br><br> Although it is currently out of office hours, our partner companies will endeavour to make contact with you as soon as office hours resume.</div>";

            if ($noOfSuccessQuotes == 1)
               $textMessage = "<div class='qrFirstRow'>Hi " . $firstName . ", </div><div class='qrSecondRow'>We have searched the market of <b>" . $prodNameToDisplay . "</b> insurance specialists and passed your details to the following specialist <b>" . $prodNameToDisplay . "</b> insurance provider. <br><br>Although it is currently out of office hours, our partner company will endeavour to make contact with you as soon as office hours resume.</div>";

            if ($noOfSuccessQuotes == 0)
               $textMessage = "<div class='qrFirstRow'>Hi " . $firstName . ", </div><div class='qrSecondRow'>Thank you for submitting your quotation request. Unfortunately, based on the details you’ve entered, we have been unable to match you with any of our <b>".$prodNameToDisplay."</b> insurance providers.</div>";
         break;

         case 'D':
         case 'W':
            if ($noOfSuccessQuotes > 1)
               $textMessage = "<div class='qrFirstRow'>Hi " . $firstName . ", </div><div class='qrSecondRow'>We have searched the market of <b>" . $prodNameToDisplay . "</b> insurance specialists and passed your details to the following panel of <b>" . $prodNameToDisplay . "</b> insurance providers.<br><br> Although it is currently out of office hours, our partner companies will endeavour to make contact with you as soon as office hours resume.</div>";

            if ($noOfSuccessQuotes == 1)
               $textMessage = "<div class='qrFirstRow'>Hi " . $firstName . ", </div><div class='qrSecondRow'>We have searched the market of <b>" . $prodNameToDisplay . "</b> insurance specialists and passed your details to the following specialist <b>" . $prodNameToDisplay . "</b> insurance provider. <br><br>Although it is currently out of office hours, our partner company will endeavour to make contact with you as soon as office hours resume.</div>";

            if ($noOfSuccessQuotes == 0)
               $textMessage = "<div class='qrFirstRow'>Hi " . $firstName . ", </div><div class='qrSecondRow'>Thank you for submitting your quotation request. Unfortunately, based on the details you’ve entered, we have been unable to match you with any of our <b>".$prodNameToDisplay."</b> insurance providers.</div>";
         break;
      }
   }
   
   if($rerun_quotes == 'Y' && $noOfSuccessQuotes == 0)
   {
      $textMessage = "<div class='qrFirstRow qrFirstRowReRunQuotes'>Sorry " . $firstName . ", we were unable to match you up with any " . $prodNameToDisplay . " insurance providers.</div><div class='qrSecondRowReRunQuotes'>This may be due to the nature of, or time that your quotation request was submitted.</div><div class='qrThirdRowHeaderReRunQuotes marginTop15px'>	But wait! We may still be able to help you find insurance… </div><div class='qrThirdRowReRunQuotes'>As many of our insurance providers only offer quotations during their opening hours, you may have more success in being matched during these times. If you’d like us to search again for you, please indicate from the options below when would suit you best to be contacted:</div>";
   }
   
   return $textMessage;
}

   
}//end Class CLastPageRewording
?>

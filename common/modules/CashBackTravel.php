<?php

function CalculateCashBack($siteID,$cheapestQuote,$sumCpa,$countCpa,$sesCPAsiteID,$wlUserID,$quoteDetailsID)
{
	//mail("george@acrux.biz","testing cashback function",var_export($_SESSION,true)."------".VARIABLE_FOR_BASE_RATE."------------".PREMIUM_VARIABLE);

   include_once "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/modules/WlCashbackValues.php";
	$objCashback        = new CWlCashbackValue();

	if($cashbackValueArray = $objCashback->GetCashbackValuesByQuoteID($quoteDetailsID))
		$valueFromBd = $cashbackValueArray['1']['cashback_value'];

	if (isset($valueFromBd))
		return $valueFromBd;
	else
	{
		$fixedCashbackSitesArrTCB = array(265);//Hastings Direct for car TCB

		$initialCashback = 0.60 * $sesCPAsiteID; // (Q)

		if ($wlUserID == 584 ||
			 $wlUserID == 588 ||
			 $wlUserID == 585 ||
			 $wlUserID == 587 ||
			 $wlUserID == 586)
			$initialCashback = 0.50 * $initialCashback;


		// Removing randomisation - WR28-4CC
		//if (preg_match("/\d+(\.)(\d{1,2}).*/", $cheapestQuote, $res))
		//   $decimals = (strlen($res[2]) == 1)?$res[2]*10:$res[2];
		//else
		//   $decimals = 0;
		//
		//$decimals = (int)$decimals;
		//
		//switch ($decimals)
		//{
		//   case 0:
		//      $addValue = -0.5;
		//      break;
		//	case ($decimals >= 1 && $decimals <= 24):
		//		$addValue = -0.5;
		//		break;
		//	case ($decimals >= 25 && $decimals <= 49):
		//		$addValue = -0.25;
		//		break;
		//	case ($decimals >= 50 && $decimals <= 74):
		//		$addValue = 0.25;
		//		break;
		//	case ($decimals >= 75 && $decimals <= 99):
		//		$addValue = 0.5;
		//		break;
		//
		//	default:
		//		break;
		//}

		//$cashbackAmount = $initialCashback + $addValue;

		$cashbackAmount = $initialCashback;


		if ($wlUserID == 604)
		{
			if(in_array($siteID, $fixedCashbackSitesArrTCB))
				$cashbackAmount = 25;
		}

		$cashbackAmount = number_format($cashbackAmount,2);

		return $cashbackAmount;
	}
}

function CalculateCashBackQuidcoTravel($siteID,$premiumLevel,$Session,$policyType,$quoteDetailsID)
{

   include_once "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/modules/WlCashbackValues.php";
	$objCashback        = new CWlCashbackValue();

	if($cashbackValueArray = $objCashback->GetCashbackValuesByQuoteID($quoteDetailsID))
		$valueFromBd = $cashbackValueArray['1']['cashback_value'];

	if (isset($valueFromBd))
		return $valueFromBd;
	else
	{
		include_once("quidcoCashbackConfig.inc");

		if(empty($policyType))
			$policyType = "MT";

		if($policyType == 'MT')
		{
         //cashback calculation
         if($_SESSION['CPA']["annual_trip_comission_percent"][$siteID] > 0)
         {
            $premiumWithOutIPT = "";
            $premiumWithOutIPT = $premiumLevel * ( 1 - IPT );

            $cashbackAmount = "";
            $netAmount      = "";
            $cashbackAmount = 0.6 * ($_SESSION['CPA']["annual_trip_comission_percent"][$siteID] / 100 * $premiumWithOutIPT);

            $cashbackAmount = number_format($cashbackAmount,2,'.','');
            $netAmount      = $premiumLevel - $cashbackAmount;
            $netAmount      = number_format($netAmount,2,'.','');
         }
         elseif($_SESSION['CPA']["annual_trip_comission_fix"][$siteID] > 0)
         {
            $premiumWithOutIPT = "";
            $premiumWithOutIPT = $premiumLevel * ( 1 - IPT );

            $cashbackAmount = "";
            $netAmount      = "";

            $cashbackAmount = 0.6 * $_SESSION['CPA']["annual_trip_comission_fix"][$siteID];

            $cashbackAmount = number_format($cashbackAmount,2,'.','');
            $netAmount      = $premiumLevel - $cashbackAmount;
            $netAmount      = number_format($netAmount,2,'.','');
         }
         //end cashback calculation
		}
		elseif($policyType == 'ST')
		{

         //cashback calculation
         if($_SESSION['CPA']["single_trip_comission_percent"][$siteID] > 0)
         {
            $premiumWithOutIPT = "";
            $premiumWithOutIPT = $premiumLevel * ( 1 - IPT );

            $cashbackAmount = "";
            $netAmount      = "";
            $cashbackAmount = 0.6 * ($_SESSION['CPA']["single_trip_comission_percent"][$siteID] / 100 * $premiumWithOutIPT);

            $cashbackAmount = number_format($cashbackAmount,2,'.','');
            $netAmount      = $premiumLevel - $cashbackAmount;
            $netAmount      = number_format($netAmount,2,'.','');
         }
         elseif($_SESSION['CPA']["single_trip_comission_fix"][$siteID] > 0)
         {
            $premiumWithOutIPT = "";
            $premiumWithOutIPT = $premiumLevel * ( 1 - IPT );

            $cashbackAmount = "";
            $netAmount      = "";
            $cashbackAmount = 0.6 * $_SESSION['CPA']["single_trip_comission_fix"][$siteID];

            $cashbackAmount = number_format($cashbackAmount,2,'.','');
            $netAmount      = $premiumLevel - $cashbackAmount;
            $netAmount      = number_format($netAmount,2,'.','');
         }
         //end cashback calculation
		}

		return $cashbackAmount;
	}
}

function CalculateCashBackTCBTravel($siteID,$premiumLevel,$Session,$policyType,$quoteDetailsID)
{
   include_once "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/modules/WlCashbackValues.php";
	$objCashback        = new CWlCashbackValue();

	if($cashbackValueArray = $objCashback->GetCashbackValuesByQuoteID($quoteDetailsID))
		$valueFromBd = $cashbackValueArray['1']['cashback_value'];

	if (isset($valueFromBd))
		return $valueFromBd;
   else
	{

		//include_once("tcbCashbackConfig.inc");

      $premVal = 0;
      $premiumLevel = str_replace(",","", $premiumLevel);
      if($_SESSION['YourCover']['type_of_policy'] == 'ST')
      {
         if($_SESSION['CPA']["single_trip_comission_percent"][$siteID] > 0)
         {
            $premiumWithOutIPT = "";
            $premiumWithOutIPT = $premiumLevel * ( 1 - IPT );
            $premVal = $_SESSION['CPA']["single_trip_comission_percent"][$siteID] / 100 * $premiumWithOutIPT;
         }
         elseif($_SESSION['CPA']["single_trip_comission_fix"][$siteID] > 0)
         {
            $premVal = $_SESSION['CPA']["single_trip_comission_fix"][$siteID];
         }
      }//single-trip
      else
      {
         if($_SESSION['CPA']["annual_trip_comission_percent"][$siteID] > 0)
         {
            $premiumWithOutIPT = "";
            $premiumWithOutIPT = $premiumLevel * ( 1 - IPT );
            $premVal = $_SESSION['CPA']["annual_trip_comission_percent"][$siteID] / 100 * $premiumWithOutIPT;
         }
         elseif($_SESSION['CPA']["annual_trip_comission_fix"][$siteID] > 0)
         {
            $premVal = $_SESSION['CPA']["annual_trip_comission_fix"][$siteID];
         }
      }//annual-trip

      if($_SESSION['YourCover']['type_of_policy'] == 'ST')
      {
         // Single trip - monthly premium
         $initialCashback = 0.60 * $premVal;
      }
      else
      {
         // Multi trip - annual premium
         $initialCashback = 0.60 * $premVal;
      }
      $cashbackAmount = $initialCashback;
      $cashbackAmount = number_format($cashbackAmount,2);

		return $cashbackAmount;

	}
}

function CalculateCashBackTCB_TravelNew($siteID,$premiumLevel,$Session,$quoteDetailsID = "")
{
   include_once "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/modules/WlCashbackValues.php";
   $objCashback        = new CWlCashbackValue();

   if(!empty($quoteDetailsID))
   {
      if($cashbackValueArray = $objCashback->GetCashbackValuesByQuoteID($quoteDetailsID))
         $valueFromBd = $cashbackValueArray['1']['cashback_value'];
   }

   if (isset($valueFromBd))
      return $valueFromBd;
   else
   {
      $premVal = 0;
      $premiumLevel = str_replace(",","", $premiumLevel);
      if($Session['YourCover']['type_of_policy'] == 'ST')
      {
         if($Session['CPA']["single_trip_comission_percent"][$siteID] > 0)
         {
            $premiumWithOutIPT = "";
            $premiumWithOutIPT = $premiumLevel * ( 1 - IPT );
            $premVal = $Session['CPA']["single_trip_comission_percent"][$siteID] / 100 * $premiumWithOutIPT;
         }
         elseif($Session['CPA']["single_trip_comission_fix"][$siteID] > 0)
         {
            $premVal = $Session['CPA']["single_trip_comission_fix"][$siteID];
         }
      }//single-trip
      else
      {
         if($Session['CPA']["annual_trip_comission_percent"][$siteID] > 0)
         {
            $premiumWithOutIPT = "";
            $premiumWithOutIPT = $premiumLevel * ( 1 - IPT );
            $premVal = $Session['CPA']["annual_trip_comission_percent"][$siteID] / 100 * $premiumWithOutIPT;
         }
         elseif($Session['CPA']["annual_trip_comission_fix"][$siteID] > 0)
         {
            $premVal = $Session['CPA']["annual_trip_comission_fix"][$siteID];
         }
      }//annual-trip

      if($Session['YourCover']['type_of_policy'] == 'ST')
      {
         // Single trip - monthly premium
         $initialCashback = 0.60 * $premVal;
      }
      else
      {
         // Multi trip - annual premium
         $initialCashback = 0.60 * $premVal;
      }
      $cashbackAmount = $initialCashback;
      $cashbackAmount = number_format($cashbackAmount,2);

      return $cashbackAmount;

   }
}

//$_SESSION['CPA'][$siteID]
//$_SESSION['_QZ_QUOTE_DETAILS_']['wlUserID']
 ?>

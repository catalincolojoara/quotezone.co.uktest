<?php

function CalculateCashBack($siteID,$cheapestQuote,$sumCpa,$countCpa,$sesCPAsiteID,$wlUserID,$quoteDetailsID)
{
	//mail("george@acrux.biz","testing cashback function",var_export($_SESSION,true)."------".VARIABLE_FOR_BASE_RATE."------------".PREMIUM_VARIABLE);

    include_once "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/modules/WlCashbackValues.php";
    $objCashback        = new CWlCashbackValue();

    if($cashbackValueArray = $objCashback->GetCashbackValuesByQuoteID($quoteDetailsID))
        $valueFromBd = $cashbackValueArray['1']['cashback_value'];

    if (isset($valueFromBd))
        return $valueFromBd;
    else
    {
        $fixedCashbackSitesArrTCB = array(265);//Hastings Direct for car TCB

       $initialCashback = 0.60 * $sesCPAsiteID; // (Q)

       if ($wlUserID == 584 ||
           $wlUserID == 588 ||
           $wlUserID == 585 ||
           $wlUserID == 587 ||
           $wlUserID == 586)
          $initialCashback = 0.50 * $initialCashback;

       if (preg_match("/\d+(\.)(\d{1,2}).*/", $cheapestQuote, $res))
          $decimals = (strlen($res[2]) == 1)?$res[2]*10:$res[2];
       else
          $decimals = 0;

       $decimals = (int)$decimals;

       switch ($decimals)
       {
          case 0:
             $addValue = -1.5;
             break;
        case ($decimals >= 1 && $decimals <= 24):
            $addValue = -1.5;
            break;
        case ($decimals >= 25 && $decimals <= 49):
            $addValue = -0.5;
            break;
        case ($decimals >= 50 && $decimals <= 74):
            $addValue = 0.5;
            break;
        case ($decimals >= 75 && $decimals <= 99):
            $addValue = 1.5;
            break;

        default:
            break;
       }

       $cashbackAmount = $initialCashback + $addValue;

       if ($wlUserID == 604)
       {
          if(in_array($siteID, $fixedCashbackSitesArrTCB))
             $cashbackAmount = 25;
       }

       $cashbackAmount = number_format($cashbackAmount,2);
       return $cashbackAmount;
    }
}

function CalculateCashBackQuidco($siteID,$premiumLevel,$Session,$quoteDetailsID)
{  
	define("PREMIUM_VARIABLE", 0.004);   // (p)
	define("CPA_VARIABLE", 0.01);        // (c)
	define("VARIABLE_FOR_BASE_RATE", 4); // (B)

	include_once "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/modules/WlCashbackValues.php";
	$objCashback        = new CWlCashbackValue();

	if($cashbackValueArray = $objCashback->GetCashbackValuesByQuoteID($quoteDetailsID))
		$valueFromBd = $cashbackValueArray['1']['cashback_value'];

	if (isset($valueFromBd))
		return $valueFromBd;
	else
	{
		$sumCpa   = 0;
		$countCpa = 0;
		foreach ($Session['CPA'] as $sesCpaSiteID => $sesCpaVal)
		{
			if (!$sesCpaVal)
				continue;

			$sumCpa = $sumCpa + $sesCpaVal; // (s)
			$countCpa++; // (n)
		}

		$fixedCashbackSitesArr = array(265, 266);

		if(in_array($siteID, $fixedCashbackSitesArr))
			$cashbackAmount = 20;
		else
		{
			//$cashbackAmount = 0.60 * $Session['CPA'][$siteID];

			$avgCPA = $sumCpa/$countCpa; // (A)
			$quidcoCashback = 0.60 * $Session['CPA'][$siteID]; // (Q)
			$CAP = $quidcoCashback + ($Session['CPA'][$siteID]/$avgCPA);
			$BASE = $CAP - VARIABLE_FOR_BASE_RATE;
			$premiumBasedCashbackRate = $BASE + ((PREMIUM_VARIABLE * $premiumLevel) + (CPA_VARIABLE * $Session['CPA'][$siteID]));

			if ($premiumBasedCashbackRate <= $CAP)
				$cashbackAmount = $premiumBasedCashbackRate;
			else
				$cashbackAmount = $CAP;

		}

		$cashbackAmount = number_format($cashbackAmount,2);

		 return $cashbackAmount;
	}
}
//$_SESSION['CPA'][$siteID]
//$_SESSION['_QZ_QUOTE_DETAILS_']['wlUserID']
 ?>

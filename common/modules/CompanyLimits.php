<?php
/*****************************************************************************/
/*                                                                           */
/*  CCompanyLimits class interface                                           */
/*                                                                           */
/*  (C) 2012 Alexandru Furtuna (alex@acrux.biz)                              */
/*                                                                           */
/*****************************************************************************/
include_once "globals.inc";
include_once "MySQL.php";

//////////////////////////////////////////////////////////////////////////////PE
//
// [CLASS NAME]:   CCompanyLimits
//
// [DESCRIPTION]:  CCompanyLimits class interface
//
// [FUNCTIONS]:    void Initialise();
//
//
//
// [CREATED BY]:   Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CCompanyLimits
{
   var $dbh;     // database server handle
   var $strERR;  // last error string
   var $closeDB; // close db variable

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CCompanyLimits
//
// [DESCRIPTION]:   Class constructor
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CCompanyLimits($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSiteLeadsPerPeriod
//
// [DESCRIPTION]:   Get no of leads per set interval
//
// [PARAMETERS]:    $siteID='',$period=''
//
// [RETURN VALUE]:  int/false
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSiteLeadsPerPeriod($siteID='',$period='')
{

   if(! empty($siteID))
   {
      if(! preg_match("/^\d+$/", $siteID))
      {
         $this->strERR = "INVALID_SITE_ID_FIELD";
         return false;
      }
   }

   if(empty($period))
   {
      $this->strERR = "INVALID_PERIOD_FIELD";
      return false;
   }

   $sqlCMD = "SELECT COUNT(DISTINCT quote_user_id) AS leads FROM lead_success_quotes WHERE site_id='$siteID'";

   //what time and date we have now
   $nowMkTime = mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"));
   $nowDate   = date("Y-m-d",$nowMkTime);
   $nowTime   = date("H:i:s",$nowMkTime);

   $intInPastMkTime = "";
   $intInPastDate   = "";
   $intInPastTime   = "";

   //print "[$siteID <==> $period] <br>";

   switch($period)
   {
      case '30min':
         //we check the past 30 min
         $intInPastMkTime = $nowMkTime -  1800;
         $intInPastDate   = date("Y-m-d",$intInPastMkTime);
         $intInPastTime   = date("H:i:s",$intInPastMkTime);

         //print "30 min time is : [$intInPastMkTime] 30 min date is [$intInPastDate] 30 min time is [$intInPastTime] <br>\n";

         $sqlCMD .= " AND (date = '$intInPastDate' AND time >= '$intInPastTime')";

         if($nowDate != $intInPastDate)
            $sqlCMD .= " OR (date = '$nowDate' AND time <= '$nowTime')";

      break;

      case 'hour':
         //we check the past 30 min
         $intInPastMkTime = $nowMkTime -  3600;
         $intInPastDate   = date("Y-m-d",$intInPastMkTime);
         $intInPastTime   = date("H:i:s",$intInPastMkTime);

         //print "hour time is : [$intInPastMkTime] hour date is [$intInPastDate] hour time is [$intInPastTime] <br>\n";

         $sqlCMD .= " AND (date = '$intInPastDate' AND time >= '$intInPastTime')";

         if($nowDate != $intInPastDate)
            $sqlCMD .= " OR (date = '$nowDate' AND time <= '$nowTime')";

         break;

      case '2hours':
         //we check the past 30 min
         $intInPastMkTime = $nowMkTime -  7200;
         $intInPastDate   = date("Y-m-d",$intInPastMkTime);
         $intInPastTime   = date("H:i:s",$intInPastMkTime);

         //print "2hour time is : [$intInPastMkTime] 2hour date is [$intInPastDate] 2hour time is [$intInPastTime] <br>\n";

         $sqlCMD .= " AND (date = '$intInPastDate' AND time >= '$intInPastTime')";

         if($nowDate != $intInPastDate)
            $sqlCMD .= " OR (date = '$nowDate' AND time <= '$nowTime')";

         break;

      case '3hours':
         //we check the past 30 min
         $intInPastMkTime = $nowMkTime -  10800;
         $intInPastDate   = date("Y-m-d",$intInPastMkTime);
         $intInPastTime   = date("H:i:s",$intInPastMkTime);

         //print "3hour time is : [$intInPastMkTime] 3hour date is [$intInPastDate] 3hour time is [$intInPastTime] <br>\n";

         $sqlCMD .= " AND (date = '$intInPastDate' AND time >= '$intInPastTime')";

         if($nowDate != $intInPastDate)
            $sqlCMD .= " OR (date = '$nowDate' AND time <= '$nowTime')";

         break;

      case 'mon':
      case 'tue':
      case 'wed':
      case 'thu':
      case 'fri':
      case 'sat':
      case 'sun':
      case 'day':
         $sqlCMD .= " AND date = '$nowDate'";
      break;

      //for the week we look for the current week
      case 'week':         
         $mondayDate = date ('Y-m-d', strtotime($nowDate) - ((date ('N', strtotime($nowDate)) - 1) * 3600 * 24));
         // $mondayThisWeek = strtotime('monday this week');
         // $mondayDate     = date("Y-m-d",$mondayThisWeek);
         //print "Monday is : $mondayThisWeek and date is [$mondayDate] <br>";

         $sqlCMD .= " AND date >= '$mondayDate'";


      break;

      case 'month':
         $currentMonthDate = date("Y-m")."-01";
         $sqlCMD .= " AND date >= '$currentMonthDate'";
      break;

      case 'all':
      break;
   }

   if(! $this->dbh->Exec($sqlCMD))
   {
      $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR  = "QUOTE_STATUS_ID_NOT_FOUND";
      return false;
   }

   return $this->dbh->GetFieldValue("leads");
}

   //////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSiteLeadsPerPeriod
//
// [DESCRIPTION]:   Get no of leads per set interval
//
// [PARAMETERS]:    $siteID='',$period=''
//
// [RETURN VALUE]:  int/false
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2012-04-11
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSiteLeadsPerPeriodOLDBKP($siteID='',$period='')
{

   if(! empty($siteID))
   {
      if(! preg_match("/^\d+$/", $siteID))
      {
         $this->strERR = "INVALID_SITE_ID_FIELD";
         return false;
      }
   }

   if(empty($period))
   {
      $this->strERR = "INVALID_PERIOD_FIELD";
      return false;
   }


   $sqlCMD = "SELECT COUNT(DISTINCT quote_user_id) AS leads FROM lead_success_quotes WHERE site_id='$siteID'";

   switch($period)
   {
      case '30min':
         $sqlCMD .= " AND concat(date,' ',time) >= DATE_SUB(now(),INTERVAL 30 MINUTE)";
         break;

      case 'hour':
         $currentDayDate = date("Y-m-d");
         $currentDayHour = date("H");
         $sqlCMD .= " AND date = '$currentDayDate' AND time like '$currentDayHour%'";
         break;

      case '2hours':
         $sqlCMD .= " AND concat(date,' ',time) >= DATE_SUB(now(),INTERVAL 120 MINUTE)";
         break;

      case '3hours':
         $sqlCMD .= " AND concat(date,' ',time) >= DATE_SUB(now(),INTERVAL 180 MINUTE)";
         break;

      case 'mon':
      case 'tue':
      case 'wed':
      case 'thu':
      case 'fri':
      case 'sat':
      case 'sun':
      case 'day':
         $currentDayDate = date("Y-m-d");
         $sqlCMD .= " AND date = '$currentDayDate'";
         break;

      case 'week':
         $currentDate = date("Y-m-d");
         $currentYear = date("Y");
         $currentWeek = date("W");
         $sqlCMD .= " AND YEAR(date) = '$currentYear' AND WEEK(date,3) = '$currentWeek'";
         break;

      case 'month':
         $currentMonthDate = date("Y-m")."-01";
         $sqlCMD .= " AND date>='$currentMonthDate'";
         break;

      case 'all':
         break;
   }

   //print "<br><br> $sqlCMD <br><br>";


   if(! $this->dbh->Exec($sqlCMD))
   {
      $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR  = "QUOTE_STATUS_ID_NOT_FOUND";
      return false;
   }

   return $this->dbh->GetFieldValue("leads");
}


}//end ClassCompanyOffline


?>
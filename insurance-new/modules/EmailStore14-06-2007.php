<?php
/*****************************************************************************/
/*                                                                           */
/*  CEmailStore class interface                                       		  */
/*                                                                           */
/*  (C) 2006 Velnic Daniel (dan@acrux.biz)                                   */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
error_reporting(1);
else
error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CEmailStore
//
// [DESCRIPTION]:  CEmailStore class interface
//
// [FUNCTIONS]:    int  AddEmailStore($homeOwner=0, $realName='', $email='')
//                 bool UpdateHomeOwner($EmailStoreID='', $homeOwner='')
//                 bool UpdateEmail($EmailStoreID='', $emailStore='')
//                 bool DeleteEmailStore($EmailStoreID=0)
//                 bool GetAllEmailStore(&$arrayResult)
//                 bool GetEmailStoreByRN($realName='', &$arrayResult)
//                 bool GetEmailStoreByE($email='')
//                 bool GetEmailStoreIDByEmail($email='')
//                 bool GetEmailStoreByID($EmailStoreID = 0)
//                 bool GetAllHomeOwner($homeOwner='', &$arrayResult)
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Velnic Daniel (dan@acrux.biz) 14-02-2006
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CEmailStore
{
	// database handler
	var $dbh;         // database server handle
	var $closeDB;     // close database flag

	// class-internal variables
	var $lastSQLCMD;  // keep here the last SQL command
	var $strERR;      // last SITE error string

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: CEmailStore
	//
	// [DESCRIPTION]:   Default class constructor. Initialization goes here.
	//
	// [PARAMETERS]:    none
	//
	// [RETURN VALUE]:  none
	//
	// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 14-02-2006
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function CEmailStore($dbh=0)
	{
		if($dbh)
		{
			$this->dbh = $dbh;
			$this->closeDB = false;
		}
		else
		{
			// default configuration
			$this->dbh = new CMySQL();

			if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
			{
				$this->strERR = $this->dbh->GetError();
				return;
			}

			$this->closeDB = true;
		}

		$this->strERR  = "";
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: AddEmailStore
	//
	// [DESCRIPTION]:   Add new entry to the email_store table
	//
	// [PARAMETERS]:    $homeOwner=0, $realName='', $email=''
	//
	// [RETURN VALUE]:  ID or 0 in case of failure
	//
	// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 14-02-2006
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function AddEmailStore($homeOwner=0, $realName='', $email='')
	{
		if(! preg_match("/^\d+$/", $homeOwner))
		{
			$this->strERR = GetErrorString("INVALID_HOMEOWNER_FIELD");
			return 0;
		}

		if(empty($realName))
		{
			$this->strERR = GetErrorString("INVALID_REALNAME_FIELD");
			return 0;
		}

		if(!preg_match('/\.*\@.*\.[a-zA-Z]{2,3}/i',$email))
		{
			$this->strERR = GetErrorString("INVALID_EMAIL_FIELD");
			return 0;
		}

		$this->lastSQLCMD = "SELECT * FROM ".SQL_EMAIL_STORE." WHERE email = '$email'";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if( $this->dbh->GetRows() > 0)
		{
			$this->strERR = GetErrorString("EMAIL_ALLREADY_ENTERED");
			return false;
		}

		$this->lastSQLCMD = "INSERT INTO ".SQL_EMAIL_STORE." (homeowner, real_name, email) VALUES ('$homeOwner','$realName','$email')";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return 0;
		}

		$this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return 0;
		}

		if(! $this->dbh->FetchRows())
		{
			$this->strERR = $this->dbh->GetError();
			return 0;
		}

		return $this->dbh->GetFieldValue("id");

	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: UpdateHomeOwner
	//
	// [DESCRIPTION]:   Update email_store set homeowner='$homeOwner'
	//
	// [PARAMETERS]:    $EmailStoreID='', $homeOwner=''
	//
	// [RETURN VALUE]:  true | false
	//
	// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 14-02-2006
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function UpdateHomeOwner($EmailStoreID='', $homeOwner='')
	{
		if(! preg_match("/^\d+$/", $EmailStoreID))
		{
			$this->strERR = GetErrorString("INVALID_EMAIL_STORE_ID_FIELD");
			return 0;
		}

		if(! preg_match("/^\d+$/", $homeOwner))
		{
			$this->strERR = GetErrorString("INVALID_HOMEOWNER_FIELD");
			return 0;
		}

		$this->lastSQLCMD = "SELECT * FROM ".SQL_EMAIL_STORE." where id='$EmailStoreID'";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if(! $this->dbh->GetRows())
		{
			$this->strERR = GetErrorString("EMAIL_STORE_ID_NOT_FOUND");
			return false;
		}

		$this->lastSQLCMD = "UPDATE ".SQL_EMAIL_STORE." SET homeowner='$homeOwner' where id='$EmailStoreID'";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: UpdateEmail
	//
	// [DESCRIPTION]:   Update email_store
	//
	// [PARAMETERS]:    $EmailStoreID='', $emailStore=''
	//
	// [RETURN VALUE]:  true | false
	//
	// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 14-02-2006
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function UpdateEmail($EmailStoreID='', $userName='', $emailStore='')
	{
		if(! preg_match("/^\d+$/", $EmailStoreID))
		{
			$this->strERR = GetErrorString("INVALID_EMAIL_STORE_ID_FIELD");
			return 0;
		}

		if(!preg_match('/\.*\@.*\.[a-zA-Z]{2,3}/i', $emailStore))
		{
			$this->strERR = GetErrorString("INVALID_EMAIL_FIELD");
			return 0;
		}

		$this->lastSQLCMD = "SELECT * FROM ".SQL_EMAIL_STORE." where id='$EmailStoreID'";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if(! $this->dbh->GetRows())
		{
			$this->strERR = GetErrorString("EMAIL_STORE_ID_NOT_FOUND");
			return false;
		}

		$this->lastSQLCMD = "UPDATE ".SQL_EMAIL_STORE." SET email='$emailStore',real_name='$userName' WHERE id='$EmailStoreID'";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: DeleteEmailStore
	//
	// [DESCRIPTION]:   Delete an entry from email_store table by ID
	//
	// [PARAMETERS]:    $EmailStoreID=0
	//
	// [RETURN VALUE]:  true | false
	//
	// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 14-02-2006
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function DeleteEmailStore($EmailStoreID=0)
	{
		if(! preg_match("/^\d+$/", $EmailStoreID))
		{
			$this->strERR = GetErrorString("INVALID_EMAIL_STORE_ID_FIELD");
			return 0;
		}

		$this->lastSQLCMD = "SELECT id FROM ".SQL_EMAIL_STORE." WHERE id='$EmailStoreID'";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if(! $this->dbh->GetRows())
		{
			$this->strERR = GetErrorString("EMAIL_STORE_ID_NOT_FOUND");
			return false;
		}

		$this->lastSQLCMD = "DELETE FROM ".SQL_EMAIL_STORE." WHERE id='$EmailStoreID'";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: GetAllEmailStore
	//
	// [DESCRIPTION]:   Get all content of the table email_store
	//
	// [PARAMETERS]:    &$arrayResult
	//
	// [RETURN VALUE]:  true | false
	//
	// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 14-02-2006
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function GetAllEmailStore(&$arrayResult)
	{
		$this->lastSQLCMD = "SELECT * FROM ".SQL_EMAIL_STORE;

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if(! $this->dbh->GetRows())
		{
			$this->strERR = GetErrorString("EMAIL_STORE_NOT_FOUND");
			return false;
		}

		while($this->dbh->MoveNext())
		{
			$contentResult["id"]        = $this->dbh->GetFieldValue("id");
			$contentResult["real_name"] = $this->dbh->GetFieldValue("real_name");
			$contentResult["email"]     = $this->dbh->GetFieldValue("email");
			$contentResult["homeowner"] = $this->dbh->GetFieldValue("homeowner");

			$arrayResult[$this->dbh->GetFieldValue("id")] = $contentResult;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: GetEmailStoreByRN
	//
	// [DESCRIPTION]:   Get a record by real_name
	//
	// [PARAMETERS]:    $realName='', &$arrayResult
	//
	// [RETURN VALUE]:  true | false
	//
	// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 14-02-2006
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function GetEmailStoreByRN($realName='', &$arrayResult)
	{
		if(empty($realName))
		{
			$this->strERR = GetErrorString("INVALID_REALNAME_FIELD");
			return false;
		}

		$this->lastSQLCMD = "SELECT * FROM ".SQL_EMAIL_STORE." WHERE real_name='$realName'";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if(! $this->dbh->FetchRows())
		{
			$this->strERR = GetErrorString("REALNAME_NOT_FOUND");
			return false;
		}

		$arrayResult["id"]        = $this->dbh->GetFieldValue("id");
		$arrayResult["real_name"] = $this->dbh->GetFieldValue("real_name");
		$arrayResult["email"]     = $this->dbh->GetFieldValue("email");
		$arrayResult["homeowner"] = $this->dbh->GetFieldValue("homeowner");

		return true;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: GetEmailStoreByE
	//
	// [DESCRIPTION]:   Get a record by email
	//
	// [PARAMETERS]:    $email=''
	//
	// [RETURN VALUE]:  false | true
	//
	// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 14-02-2006
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function GetEmailStoreByE($email='')
	{
		if(!preg_match('/\.*\@.*\.[a-zA-Z]{2,3}/i',$email))
		{
			$this->strERR = GetErrorString("INVALID_EMAIL_FIELD");
			return false;
		}

		$this->lastSQLCMD = "SELECT * FROM ".SQL_EMAIL_STORE." WHERE email='$email'";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if(! $this->dbh->FetchRows())
		{
			$this->strERR = GetErrorString("EMAIL_NOT_FOUND");
			return false;
		}

		$arrayResult["id"]        = $this->dbh->GetFieldValue("id");
		$arrayResult["real_name"] = $this->dbh->GetFieldValue("real_name");
		$arrayResult["email"]     = $this->dbh->GetFieldValue("email");
		$arrayResult["homeowner"] = $this->dbh->GetFieldValue("homeowner");

		return true;
	}
	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: GetEmailStoreIDByE
	//
	// [DESCRIPTION]:   Get a id record by email
	//
	// [PARAMETERS]:    $email=''
	//
	// [RETURN VALUE]:  $arrayResult["id"]
	//
	// [CREATED BY]:    Cezar Listeveanu (cezar@acrux.biz) 15-02-2006
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function GetEmailStoreIDByEmail($email='')
	{
		if(!preg_match('/\.*\@.*\.[a-zA-Z]{2,3}/i',$email))
		{
			$this->strERR = GetErrorString("INVALID_EMAIL_FIELD");
			return false;
		}

		$this->lastSQLCMD = "SELECT * FROM ".SQL_EMAIL_STORE." WHERE email='$email'";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if(! $this->dbh->FetchRows())
		{
			$this->strERR = GetErrorString("EMAIL_NOT_FOUND");
			return false;
		}

		$arrayResult["id"] = $this->dbh->GetFieldValue("id");

		return $arrayResult["id"];
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: GetEmailStoreByID
	//
	// [DESCRIPTION]:   Get a record by id
	//
	// [PARAMETERS]:    $EmailStoreID = 0
	//
	// [RETURN VALUE]:  true | false
	//
	// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 14-02-2006
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function GetEmailStoreByID($EmailStoreID = 0)
	{
		if(! preg_match("/^\d+$/", $EmailStoreID))
		{
			$this->strERR = GetErrorString("INVALID_ID_FIELD");
			return false;
		}

		$this->lastSQLCMD = "SELECT * FROM ".SQL_EMAIL_STORE." where id='$EmailStoreID'";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if(! $this->dbh->FetchRows())
		{
			$this->strERR = GetErrorString("ID_NOT_FOUND");
			return false;
		}

		$arrayResult["id"]        = $this->dbh->GetFieldValue("id");
		$arrayResult["real_name"] = $this->dbh->GetFieldValue("real_name");
		$arrayResult["email"]     = $this->dbh->GetFieldValue("email");
		$arrayResult["homeowner"] = $this->dbh->GetFieldValue("homeowner");

		return $arrayResult;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: GetAllHomeOwner
	//
	// [DESCRIPTION]:   Get all homeowner
	//
	// [PARAMETERS]:    $homeOwner='', &$arrayResult
	//
	// [RETURN VALUE]:  true | false
	//
	// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 14-02-2006
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function GetAllHomeOwner($homeOwner='', &$arrayResult)
	{
		if(! preg_match("/^\d+$/", $homeOwner))
		{
			$this->strERR = GetErrorString("INVALID_HOMEOWNER_FIELD");
			return 0;
		}

		$this->lastSQLCMD = "SELECT * FROM ".SQL_EMAIL_STORE." where homeowner='$homeOwner'";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if(! $this->dbh->GetRows())
		{
			$this->strERR = GetErrorString("HOMEOWNER_NOT_FOUND");
			return false;
		}

		while($this->dbh->MoveNext())
		{
			$contentResult["id"]        = $this->dbh->GetFieldValue("id");
			$contentResult["real_name"] = $this->dbh->GetFieldValue("real_name");
			$contentResult["email"]     = $this->dbh->GetFieldValue("email");
			$contentResult["homeowner"] = $this->dbh->GetFieldValue("homeowner");

			$arrayResult[$this->dbh->GetFieldValue("id")] = $contentResult;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: GetError
	//
	// [DESCRIPTION]:   Retrieve the last error message
	//
	// [PARAMETERS]:    none
	//
	// [RETURN VALUE]:  Error string if there is any, empty string otherwise
	//
	// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 14-02-2006
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function GetError()
	{
		return $this->strERR;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: Close
	//
	// [DESCRIPTION]:   Close the object and also close the connection with the
	//                  database server if necessary
	//
	// [PARAMETERS]:    none
	//
	// [RETURN VALUE]:  none
	//
	// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 14-02-2006
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function Close()
	{
		if($this->closeDB)
		$this->dbh->Close();

		return;
	}
}

?>
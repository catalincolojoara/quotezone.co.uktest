<?php

/*****************************************************************************/
/*                                                                           */
/*  COffline class interface
/*                                                                           */
/*  (C) 2011 Istvancsek Gabi(gabi@acrux.biz)                                     */
/*                                                                           */
/*****************************************************************************/

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

include_once "WebSite.php";

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   COffline
//
// [DESCRIPTION]:  COffline class interface
//
// [FUNCTIONS]:   constructor COffline
//                string | false OpenOfflineFile
//                true | false PrepareOfflineData
//                array | false GetOfflineProducts
//                true | false IsOffline
//                true | false GetError
//
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz) 2011-05-23
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class COffline extends CWebSite
{
   var $offlineFile;     // offline message
   var $offlineProducts; // offline products
   var $offlineMessage;  // offline message

   var $majorProducts;   // major products
   var $minorProducts;   // minor products

   var $strERR;          // last error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: COffline
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2011-05-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function COffline()
{

   $this->offlineFile      = "/home/www/quotezone.co.uk/offline/offline-systems.inc";
   $this->offlineProducts  = "";
   $this->offlineMessage   = "";

   $this->majorProducts['1'] = 'car';
   $this->majorProducts['2'] = 'van';
   $this->majorProducts['4'] = 'home';
   $this->majorProducts['5'] = 'bike';

   $this->minorProducts['6'] = 'travel';
   $this->minorProducts['7'] = 'breakdown';
   $this->minorProducts['9'] = 'pet';

   $this->strERR  = "";

   // read template files
   $this->templatesDirectory = TEMPLATES_DIR;

   if(! empty($templatesDir))
      $this->templatesDirectory = $templatesDir;

   $this->Initialise();
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Initialise
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2011-05-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Initialise()
{
   session_start();

   $this->menuType  = COMMON;
   $this->userID    = 0;
   $this->userName  = "";
   $this->userEmail = "";
   $this->admin     = $_SERVER["REMOTE_USER"];

   if(! empty($this->admin))
      $this->menuType = ADMIN_USER;

   if(! is_dir($this->templatesDirectory))
   {
      $this->strERR = GetErrorString("DIR_NOT_FOUND").": [".$this->templatesDirectory."]";
      return;
   }

   $this->loadTemplates = explode(" ", PRELOAD_TEMPLATES);
   foreach($this->loadTemplates as $tmplFile)
   {
      if(! ($fh = fopen($this->templatesDirectory."/$tmplFile", "r")))
      {
         $this->strERR = GetErrorString("CANNOT_OPEN_FILE").": [".$this->templatesDirectory."/$tmplFile]";
         return;
      }

      $this->templates[$tmplFile] = fread($fh, filesize($this->templatesDirectory."/$tmplFile"));
      fclose($fh);
   }

   // in case that http://car-insurance.quotezone.co.uk/car/?logo=1 = > change the header
   if (preg_match("/car-insurance/i",$_SERVER['HTTP_HOST']))
   {
      //   if($_SERVER['REMOTE_ADDR'] != '81.196.65.167' AND $_SERVER['REMOTE_ADDR'] != '188.220.10.35')
      //      return;

      if($_SESSION["header_1"] == 1)
      {
         $headerFile = "Header_1.tmpl";
         $this->SetHeaderTmpl($headerFile);
      }
   }

   return;

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: OpenOfflineFile
//
// [DESCRIPTION]:   open offline file
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  false | data
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2011-05-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function OpenOfflineFile()
{
   if(! $this->offlineFile)
   {
      $this->strERR = "NO_OFFLINE_FILE_DEFINED";
      return false;
   }

   if(! $fh = fopen($this->offlineFile,"r"))
   {
      $this->strERR = "OFFLINE_FILE_NOT_FOUND";
      return false;
   }

   if(! $data = fread($fh, filesize($this->offlineFile)))
   {
      $this->strERR = "OFFLINE_FILE_NO_DATA";
      return false;
   }

   fclose($fh);

   return $data;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: PrepareOfflineData
//
// [DESCRIPTION]:   set the offline products and message from offline file config
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true or false in case o failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2011-05-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function PrepareOfflineData()
{
   if(! $data = $this->OpenOfflineFile())
      return false;

   $lines = explode("\n", $data);

   $this->offlineProducts = $lines[0];
   $this->offlineProducts = strtolower($this->offlineProducts);
   $this->offlineProducts = trim($this->offlineProducts);
   $this->offlineProducts = preg_replace("/\s+/"," ", $this->offlineProducts);


   unset($lines[0]);
   $this->offlineMessage  = implode("\n", $lines);

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetOfflineProducts()
//
// [DESCRIPTION]:   get offline products from offline file
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2011-05-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetOfflineProducts()
{
   if(empty($this->offlineProducts))
      return false;

   $productsArray = explode(" ", $this->offlineProducts);
   
   return $productsArray;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: IsOffline
//
// [DESCRIPTION]:   check if a product is offline
//
// [PARAMETERS]:    productName
//
// [RETURN VALUE]:  array  | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2011-05-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function IsOffline($productName)
{

   $productName = trim($productName);

   if(empty($productName))
      return false;

   $productName = strtolower($productName);

   if(! $this->OpenOfflineFile())
      return false;

   if(! $this->PrepareOfflineData())
      return false;

   if(! $offlineProducts = $this->GetOfflineProducts())
      return false;

   //check all group of products
   if(array_search("all", $offlineProducts) !== FALSE)
      return true;
   
   //check products
   if(array_search($productName, $offlineProducts) !== FALSE)
      return true;

   //check major group of products
   if(array_search("major", $offlineProducts) !== FALSE)
   {
      if(array_search($productName, $this->majorProducts) !== FALSE)
         return true;
   }

   //check minor group of products
   if(array_search("minor", $offlineProducts) !== FALSE)
   {
      if(array_search($productName, $this->minorProducts) !== FALSE)
         return true;
   }

   //check quotes group of products
   if(array_search("quotes", $offlineProducts) !== FALSE )
   {
      if(array_search($productName, $this->majorProducts) !== FALSE )
         return true;

      if(array_search($productName, $this->minorProducts) !== FALSE )
         return true;
   }

   //check leads group of products
   if(array_search("leads", $offlineProducts) !== FALSE )
   {
      if(array_search($productName, $this->majorProducts) === FALSE && array_search($productName, $this->minorProducts) === FALSE)
         return true;
   }

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetOfflineMessage
//
// [DESCRIPTION]:   Retrieve the offline message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  string | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2011-05-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetOfflineMessage()
{
    return $this->offlineMessage;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2011-05-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}


} // end of CQuote class
?>

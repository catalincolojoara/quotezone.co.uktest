<?php
/*****************************************************************************/
/*                                                                           */
/*  CCookieStats class interface                                            */
/*                                                                           */
/*  (C) 2004 Eugen Savin (eugen@claspsolutions.com)                          */
/*                                                                           */
/*****************************************************************************/
// include_once "globals.adm.inc";

define("AFFILIATES_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";


if(DEBUG_MODE)
   error_reporting(E_ALL);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CCookieStats
//
// [DESCRIPTION]:  CCookieStats class interface
//
// [FUNCTIONS]:    int  AddCookieStats($cookieID=0, $affID=0, $quoteTypeID=0, $crDate=0);
//                 bool UpdateCookieStats($cID=0, $cookieID=0, $quoteTypeID=0);
//                 bool DeleteCookieStats($cID=0);
//                 array|false GetCookieStatsByID($cID=0);
//                 array|false GetCookieStatsByName($cookieID=0);
//                 array|false GetAllCookiesStats($startDate="", $endDate="");
//                 int GetCookieStatsCount(($startDate="", $endDate="", $affiliateId=0, $quoteTypeID=0, $uniqueCookie=0));
//                 bool AssertCookieStats($cookieID=0, $affID=0, $quoteTypeID=0, $crDate="");
//
//                 void Close();
//                 string GetError();
//
// [CREATED BY]:   Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CCookieStats
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CCookieStats
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CCookieStats($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddCookieStats
//
// [DESCRIPTION]:   Add new entry to the cookie_stats table
//
// [PARAMETERS]:    $cookieID=0, $affId=0 $crDate = 0
//
// [RETURN VALUE]:  quoteID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddCookieStats($cookieID=0, $affID=0, $quoteTypeID=0, $crDate=0)
{
   if(! $this->AssertCookieStats($cookieID, $affID, $quoteTypeID, $crDate))
      return 0;

   // check if ID exists in DB
   $sqlCmd = "SELECT id FROM ".SQL_COOKIES." WHERE id='$cookieID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("COOKIE_ID_NOT_FOUND");
      return 0;
   }

   $sqlCmd = "INSERT INTO ".SQL_COOKIE_STATS." (cookie_id,affiliate_id,quote_type_id,cr_date) VALUES ('$cookieID','$affID','$quoteTypeID','$crDate')";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $cookieID = $this->dbh->GetFieldValue("id");

   return $cookieID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateCookieStats
//
// [DESCRIPTION]:   Update cookie_stats table
//
// [PARAMETERS]:    $cID=0, $cookieID=0, $affID=0, $quoteTypeID=0
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateCookieStats($cID=0, $cookieID=0, $affID=0, $quoteTypeID=0)
{
   if(! preg_match("/^\d+$/", $cID))
   {
      $this->strERR = GetErrorString("INVALID_COOKIE_STATSID_FIELD");
      return false;
   }

   if(! $this->AssertCookieStats($cID, $cookieID, $affID, $quoteTypeID))
      return false;

   // check if ID exists in DB
   $sqlCmd = "SELECT cookie_id FROM ".SQL_COOKIE_STATS." WHERE id='$cID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("COOKIEID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $dbCookie = $this->dbh->GetFieldValue("cookie_id");

   if($cookie != $dbCookie)
   {
      // check if cookie exists in DB
      $sqlCmd = "SELECT id FROM ".SQL_COOKIE_STATS." WHERE cookie_id='$cookieID'";

      if(! $this->dbh->Exec($sqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if($this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("COOKIE_ALREADY_EXISTS");
         return false;
      }
   }

   $sqlCmd = "UPDATE ".SQL_COOKIE_STATS." SET cookie_id='$cookieID', affiliate_id='$affID', quote_type_id='$quoteTypeID' WHERE id='$cID'";

   //echo $sqlCmd ;
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteCookieStats
//
// [DESCRIPTION]:   Delete an entry from cookie_stats table
//
// [PARAMETERS]:    $cID = 0
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteCookieStats($cID = 0)
{
   if(! preg_match("/^.{1,16}$/", $cID))
   {
      $this->strERR = GetErrorString("INVALID_COOKIEID_FIELD");
      return false;
   }

   // check if ID exists in DB
   $sqlCmd = "SELECT id FROM ".SQL_COOKIE_STATS." WHERE id='$cID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("COOKIEID_NOT_FOUND");
      return false;
   }

   $sqlCmd = "DELETE FROM ".SQL_COOKIE_STATS." WHERE id='$cID'";

   //print "$sqlCmd";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCookieStatsByID
//
// [DESCRIPTION]:   Read data from cookie_stats table and put it into an array variable
//
// [PARAMETERS]:    $cID = 0
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCookieStatsByID($cID = 0)
{
   if(! preg_match("/^.{1,16}$/", $cID))
   {
      print_r ($cookieID);
      $this->strERR = GetErrorString("INVALID_COOKIEID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT * FROM ".SQL_COOKIE_STATS." WHERE id='$cID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("COOKIEID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]            = $this->dbh->GetFieldValue("id");
   $arrayResult["cookie_id"]     = $this->dbh->GetFieldValue("cookie_id");
   $arrayResult["affiliate_id"]  = $this->dbh->GetFieldValue("affiliate_id");
   $arrayResult["quote_type_id"] = $this->dbh->GetFieldValue("quote_type_id");
   $arrayResult["cr_date"]       = $this->dbh->GetFieldValue("cr_date");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCookieStatsByName
//
// [DESCRIPTION]:   Read data from cookie_stats table and put it into an array variable
//
// [PARAMETERS]:    $cookieID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCookieStatsByName($cookieID = "")
{
   if(! preg_match("/^.{1,16}$/", $cookieID))
   {
      $this->strERR = GetErrorString("INVALID_COOKIEID_FIELD");
      return false;
   }

   // check if we have this  cookie
   $sqlCmd = "SELECT id FROM ".SQL_COOKIE_STATS." WHERE cookie_id='$cookieID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("COOKIEID_NOT_FOUND");
      return false;
   }

   $sqlCmd = "SELECT * FROM ".SQL_COOKIE_STATS." WHERE cookie_id='$cookieID' ORDER BY cookie_id ASC";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("COOKIE_STATSID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]            = $this->dbh->GetFieldValue("id");
   $arrayResult["cookie_id"]     = $this->dbh->GetFieldValue("cookie_id");
   $arrayResult["affiliate_id"]  = $this->dbh->GetFieldValue("affiliate_id");
   $arrayResult["quote_type_id"] = $this->dbh->GetFieldValue("quote_type_id");
   $arrayResult["cr_date"]       = $this->dbh->GetFieldValue("cr_date");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllCookiesStats
//
// [DESCRIPTION]:   Read data from cookie_stats table and put it into an array variable
//
// [PARAMETERS]:    $startDate="", $endDate=""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllCookiesStats($startDate="", $endDate="")
{
   $sqlCmd = "SELECT * FROM ".SQL_COOKIE_STATS." WHERE 1";

   if($startDate)
      $sqlCmd .= " AND cr_date>='$startDate'";

   $sqlCmd .= " ORDER BY id ";

   if($endDate)
      $sqlCmd .= " AND cr_date<='$endDate'";


   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("COOKIEIDS_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("cookie_id");

  //print_r ($arrayResult);

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCookieStatsCount
//
// [DESCRIPTION]:   Read data from cookie_stats table and put it into an array variable
//
// [PARAMETERS]:    $startDate='', $endDate='', $affiliateId=0, $quoteTypeID=0, $uniqueCookie=0
//
// [RETURN VALUE]:  int
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2005-11-30
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCookieStatsCount($startDate='', $endDate='', $affiliateId=0, $quoteTypeID=0, $uniqueCookie=0)
{
   $this->lastSQLCMD = "SELECT count(";

   if(! $uniqueCookie)
      $this->lastSQLCMD .= "*)";
   else
      $this->lastSQLCMD .= " distinct cookie_id)";

   $this->lastSQLCMD .= " as cnt FROM ".SQL_COOKIE_STATS." WHERE 1";

   if($startDate)
      $this->lastSQLCMD .= " AND cr_date>='$startDate 00:00:00' ";

   if($endDate)
      $this->lastSQLCMD .= " AND cr_date<='$endDate 23:59:59' ";

   if($affiliateId)
      $this->lastSQLCMD .= " AND affiliate_id='$affiliateId'";

   if($quoteTypeID)
      $this->lastSQLCMD .= " AND quote_type_id='$quoteTypeID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return -1;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return -2;
   }

   return $this->dbh->GetFieldValue("cnt");;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertCookieStats
//
// [DESCRIPTION]:   Check if all fields are OK and set the error string if needed
//
// [PARAMETERS]     $cookie, $affID, $quoteTypeID, $crDate
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertCookieStats($cookieID, $affID, $quoteTypeID, $crDate)
{
   $this->strERR = "";

   if(! preg_match("/^\d+$/", $cookieID))
      $this->strERR .= GetErrorString("INVALID_COOKIEID_FIELD")."\n";

   if(! preg_match("/^\d+$/", $affID))
      $this->strERR .= GetErrorString("INVALID_AFFILIATEID_FIELD")."\n";

   if(! preg_match("/^\d+$/", $quoteTypeID))
      $this->strERR .= GetErrorString("INVALID_QUOTETYPEID_FIELD")."\n";

   if(! preg_match("/^.{1,32}$/", $crDate))
      $this->strERR .= GetErrorString("INVALID_CRDATE_FIELD")."\n";

   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

} // end of CCookieStats class
?>
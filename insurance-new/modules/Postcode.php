<?


/*****************************************************************************/
/*                                                                           */
/*  CPostcode class interface
/*                                                                           */
/*  (C) 2005 Istvancsek Gabriel (jkl@acrux.biz)                                     */
/*                                                                           */
/*****************************************************************************/

include_once "MySQL.php";
include_once "RoyalPostcodeCodes.php";


if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);
//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CPostcode
//
// [DESCRIPTION]:  CPostcode class interface
//
// [FUNCTIONS]:
//
// [CREATED BY]:   Gabi Istvancsek (jkl@acrux.biz) 2005-05-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CPostcode
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   var $accountCode; // account Code
   var $licenseCode; // close database flag

   var $counter;     // number of retries

   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Postcode()
//
// [DESCRIPTION]:   constructor
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi Istvancsek (jkl@acrux.biz) 2005-12-15
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CPostcode($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
         $this->strERR = $this->dbh->GetError();

         $this->SendErrorMail("Hostname: ".$_SERVER['HOSTNAME']."\nError: ".$this->dbh->GetError()."\nError code: ".$this->dbh->GetErrorNum()."\n");

         return;
      }

      $this->closeDB = true;
   }

   $this->counter = 0;

   $this->accountCode = 'SEOPA11111';
   $this->licenseCode = 'PU86-HK95-RR75-GN22';

   $this->royalPostcodeObj = new CRoyalPostcodeCodes();

   $this->lookupUrl   = "http://services.postcodeanywhere.co.uk/xml.aspx?account_code=".$this->accountCode."&license_code=".$this->licenseCode."&action=lookup&type=by_postcode&postcode=";


//    $this->fetchUrl    = "http://services.postcodeanywhere.co.uk/xml.aspx?account_code=".$this->accountCode."&license_code=".$this->licenseCode."&action=fetch&style=rawgeographic&type=by_postcode&postcode=";

   $this->fetchUrl    = "http://services.postcodeanywhere.co.uk/xml.aspx?account_code=".$this->accountCode."&license_code=".$this->licenseCode."&action=fetch&id=";

   $this->strERR  = "";
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: FetchPostCode
//
// [DESCRIPTION]:
//
// [PARAMETERS]:
//
// [RETURN VALUE]:
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-05-28
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function RealFetchPostCode($postCode = "", $type="")
{
   if(empty($type))
      return false;

   $site = $this->lookupUrl.str_replace(" ", "+", $postCode);

   if(! $fd = fopen($site, "r"))
   {
      $this->SendErrorMail("Cannot open services.postcodeanywhere.co.uk"."\nError code: -7\n");
      return -7;
   }

   $postCodeText = "";

   while(! feof($fd))
      $postCodeText .= fread($fd, 1024);

   fclose($fd);

   $items = 0;

   if(preg_match("/\<Data Items=\"([^\"]+)\"/", $postCodeText, $matches))
      $items = $matches[1];

   if(! $items)
   {
      //$this->SendErrorMail("No data items found"."\nError code: -8\n");
      return -8;
   }

   preg_match_all("/description=\"([^\"]+)\"/", $postCodeText, $matches);

   for($i=0; $i<count($matches[1]); $i++)
   {
      $address = $matches[1][$i];
      $address = str_replace("'", "", $address);

      $selectBoxOptions[$address] = $address;
   }

   preg_match_all("/id=\"([^\"]+)\"/", $postCodeText, $matches2nd);


   // first check if we already have the post code in the database
   $sqlCMD = "SELECT id FROM ".SQL_POST_CODES." WHERE pcode='$postCode'";

   if(! $this->dbh->Exec($sqlCMD))
      return $selectBoxOptions;

   $pCodeID = 0;
   if(! $this->dbh->FetchRows())
   {
      // insert the new pcode into the database

      // update the database
      $sqlCMD = "INSERT INTO ".SQL_POST_CODES." (pcode) VALUES ('$postCode')";

      if(! $this->dbh->Exec($sqlCMD))
         return false;

      $sqlCMD = "SELECT LAST_INSERT_ID() AS pCodeID";

      if(! $this->dbh->Exec($sqlCMD))
         return false;

      if(! $this->dbh->FetchRows())
         return false;

      $pCodeID = $this->dbh->GetFieldValue("pCodeID");

   }

   if(empty($pCodeID))
      $pCodeID = $this->dbh->GetFieldValue("id");

   switch(strtolower($type))
   {
      case 'insert':
            for($i=0; $i<count($matches[1]); $i++)
            {
               $address = $matches[1][$i];

               $address = str_replace("'", "", $address);

               preg_match("/\d+/",$matches2nd[1][$i],$mathes3d);
               $realAddressID = $mathes3d[0];

               $sqlCMD = "INSERT INTO ".SQL_PCODE_STREETS." (pcode_id,address,rpcode_id) VALUES ('$pCodeID','$address','$realAddressID')";

               if(! $this->dbh->Exec($sqlCMD))
                  continue;
            }

         break;

      // done
      case 'update':

            $sqlCMD = "SELECT * FROM ".SQL_PCODE_STREETS." WHERE pcode_id='$pCodeID' ORDER BY id ASC";

            if(! $this->dbh->Exec($sqlCMD))
               return false;

            if(! $this->dbh->GetRows())
            {
               $this->strERR = GetErrorString("SCANNINGS_NOT_FOUND");
               return false;
            }

            $index = 0;
            while($this->dbh->MoveNext())
            {
               preg_match("/\d+/",$matches2nd[1][$index],$matches3d);
               $realAddressID = $matches3d[0];

               $arrayResult[$this->dbh->GetFieldValue("id")] = $realAddressID;
               $index++;
            }

            foreach($arrayResult as $pcodeStreetID => $realAddressID)
            {
               // update the database
               $sqlCMD = "UPDATE ".SQL_PCODE_STREETS." set rpcode_id='$realAddressID' WHERE id='$pcodeStreetID'";

               if(! $this->dbh->Exec($sqlCMD))
                  return false;

            }

         break;
   }


   return $selectBoxOptions;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: PostCodeLookup
//
// [DESCRIPTION]:   returns a selectbox that contains addresses for a valid postcode
//
// [PARAMETERS]:    $postCode = ""
//
// [RETURN VALUE]:  select box text | < 0 in case of errors
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-05-28
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function PostCodeLookup($postCode = "", $fullAddress = "")
{
    //ROYAL POSTCDES *************************************************

    $addressLines = $this->RoyalPostCodeLookup($postCode, $fullAddress);

    if(is_array($addressLines))
    {
       foreach($addressLines as $addressId => $addressLine)
       {
          if(preg_match("/^\d/",$addressLine))
             $numericAddressLines[$addressId] = $addressLine;
          else
             $stringAddressLines[$addressId] = $addressLine;
       }

       asort($numericAddressLines,SORT_NUMERIC);
       asort($stringAddressLines,SORT_STRING);

       if(empty($stringAddressLines) )
          $addressLines= $numericAddressLines;
       elseif(empty($numericAddressLines))
          $addressLines = $stringAddressLines;
       else
          $addressLines = array_merge($stringAddressLines,$numericAddressLines);

      return $addressLines;

     }

   //ROYAL POSTCDES *************************************************
   return -1;

   $fd = fopen("tmp/postcodesLogs.log","a+");
   fwrite($fd,date("Y-m-d H:i:s")." Postcode anywhere : ".$postCode."\n");
   fclose($fd);


   $postCode    = preg_replace('/\s+/',' ', trim($postCode));
   //$fullAddress = preg_replace('/\s+/',' ', trim($fullAddress));

   $selectBoxOptions = "";

   if(empty($postCode))
      return -1;

   // get the pcode from the database
   $sqlCMD = "SELECT * from ".SQL_POST_CODES." WHERE pcode='$postCode'";

   if(! $this->dbh->Exec($sqlCMD))
   {
      $this->SendErrorMail($this->dbh->GetError()."\nError code: -3\n");
      return -3;
   }

   if($this->dbh->GetRows())
   {
      if(! $this->dbh->FetchRows())
      {
         $this->SendErrorMail($this->dbh->GetError()."\nError code: -4\n");
         return -4;
      }

      $pCodeID = $this->dbh->GetFieldValue("id");

      $sqlCMD = "SELECT * from ".SQL_PCODE_STREETS." WHERE pcode_id='$pCodeID'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->SendErrorMail($this->dbh->GetError()."\nError code: -5\n");
         return -5;
      }

      if($this->dbh->GetRows())
      {
         //SendErrorMail($this->dbh->GetError()."\nPostcode [$postCode] exists in the database but it has no streets associated.\nError //code: -6\n");
         //return -6;

         $found  = false;
         $update = false;
         while($this->dbh->MoveNext())
         {
            $selectBoxOptions[$this->dbh->GetFieldValue("address")] = $this->dbh->GetFieldValue("address");

            if(preg_match("/^$fullAddress/i", $this->dbh->GetFieldValue("address")." "))
            {
               $found = true;

               $fullAddress = $this->dbh->GetFieldValue("address");

               if(! $this->dbh->GetFieldValue("rpcode_id"))
                  $update = true;
            }
         }

         if($update)
            $selectBoxOptions = $this->RealFetchPostCode($postCode, 'UPDATE');

         if($found)
            $this->GeoPostCodeLookup($postCode, $fullAddress);

         return $selectBoxOptions;
      }
      elseif($this->dbh->GetError())
      {
         $this->SendErrorMail($this->dbh->GetError()."\nError code: -6\n");
         return -6;
      }
   }

   $selectBoxOptions = $this->RealFetchPostCode($postCode, 'INSERT');

   if(! empty($fullAddress))
       $this->GeoPostCodeLookup($postCode, $fullAddress);

   return $selectBoxOptions;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GeoPostCodeLookup
//
// [DESCRIPTION]:   returns an array thatr contains: address 1, address 2, town,
//                  county
//
// [PARAMETERS]:    $postCode = ""
//
// [RETURN VALUE]:  array | false in case of errors
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-05-28
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GeoPostCodeLookup($postCode = "", $fullAddress = "")
{
    //ROYAL POSTCDES *************************************************
    $addressArray = $this->RoyalGeoPostCodeLookup($postCode, $fullAddress);
      if(is_array($addressArray))
	return $addressArray;

    //ROYAL POSTCDES **************************************************
   return -1;

   $fd = fopen("/tmp/postcodesLogs.log","a+");
   fwrite($fd,date("Y-m-d H:i:s")." Geo anywhere : ".$postCode."/".$fullAddress."\n");
   fclose($fd);

   $postCode = preg_replace('/\s+/',' ', trim($postCode));

   $fullAddress = str_replace("'", "", $fullAddress);

// we have to chech if we have this on database
// if we dont have real id we have to insert into table pcode_details

   // force only one entry if the address is not fully address
   $sqlCMD = "SELECT ".SQL_PCODE_STREETS.".* FROM ".SQL_POST_CODES.",".SQL_PCODE_STREETS." WHERE ".SQL_POST_CODES.".id=".SQL_PCODE_STREETS.".pcode_id AND  ".SQL_POST_CODES.".pcode='$postCode' AND ".SQL_PCODE_STREETS.".address LIKE '$fullAddress%' LIMIT 1";

//   print " <!-- $sqlCMD -->";

   if(! $this->dbh->Exec($sqlCMD))
   {
      $this->strERR = 'CANNOT_SELECT_POSTCODE';
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = 'INVALID_POSTCODE_AND_FULL_ADDRESS';
      return false;
   }

   $postcodeID      = $this->dbh->GetFieldValue("pcode_id");
   $addressID       = $this->dbh->GetFieldValue("id");
   $realAddressID   = $this->dbh->GetFieldValue("rpcode_id");
   $dbFullAddress   =  $this->dbh->GetFieldValue("address");

   $sqlCMD = "SELECT * FROM ".SQL_PCODE_DETAILS." WHERE pcs_id='$addressID'";

   $result = array();

   if(! $this->dbh->Exec($sqlCMD))
   {
      $this->strERR = 'CANNOT_CHECK_FETCHED_DETAILS';
      return false;
   }

   // we have an entry and return this
   if($this->dbh->FetchRows())
   {
      $result['organisation_name'] = $this->dbh->GetFieldValue("organisation_name");
      $result['department_name']   = $this->dbh->GetFieldValue("department_name");
      $result['post_town']         = $this->dbh->GetFieldValue("post_town");
      $result['barcode']           = $this->dbh->GetFieldValue("barcode");
      $result['county']            = $this->dbh->GetFieldValue("county");
      $result['line1']             = $this->dbh->GetFieldValue("line1");
      $result['line2']             = $this->dbh->GetFieldValue("line2");
      $result['line3']             = $this->dbh->GetFieldValue("line3");
      $result['line4']             = $this->dbh->GetFieldValue("line4");
      $result['line5']             = $this->dbh->GetFieldValue("line5");
      $result['full_address']      = $dbFullAddress;


      // we have to check if the address are correctly added to database
      if(! empty($fullAddress))
      {
         if(preg_match("/".$result['line1']."/i", $fullAddress))
            return $result;

          // the address are not the same, we have to delete all entries from addresses...
          $sqlCMD = "SELECT * FROM ".SQL_PCODE_STREETS." WHERE pcode_id='$postcodeID'";

          if(! $this->dbh->Exec($sqlCMD))
          {
             $this->strERR = 'CANNOT_CHECK_FETCHED_DETAILS';
             return false;
          }
          // get street address id
          while($this->dbh->MoveNext())
          {
            $resAddress[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("address");
          }

           // the address are not the same, we have to delete all entries from addresses...
           $sqlCMD = "DELETE  FROM ".SQL_PCODE_STREETS." WHERE pcode_id='$postcodeID'";

           if(! $this->dbh->Exec($sqlCMD))
           {
              $this->strERR = 'CANNOT_CHECK_FETCHED_DETAILS';
              return false;
           }

          foreach($resAddress as $adrID => $adrName)
          {
              $sqlCMD = "DELETE  FROM ".SQL_PCODE_DETAILS." WHERE pcs_id='$adrID'";

              if(! $this->dbh->Exec($sqlCMD))
              {
                 $this->strERR = 'CANNOT_CHECK_FETCHED_DETAILS';
                 return false;
              }
          }

          $this->counter++;

          if($this->counter>3)
          {
             $this->strERR = 'WE_CANT_MATCH_THE_ADDRESS';
             return false;
          }

          $this->PostCodeLookup($postCode, $fullAddress);
      }

      return $result;
   }

   if(empty($realAddressID))
   {
      $this->strERR = 'INVALID_REAL_POSTCODE_ADDRESS_ID';
      return false;
   }

   // else we have to add new full address to table pcode_details
   $site = $this->fetchUrl.$realAddressID;

   if(! $fd = fopen($site, "r"))
   {
      $this->strERR = 'CANT_OPEN_POSTCODE_FETCH_PAGE';
      return false;
   }

   $postCodeText = "";

   while(! feof($fd))
      $postCodeText .= fread($fd, 1024);

   fclose($fd);

   $postCodeInfo = array();

   preg_match("/organisation_name=\"([^\"]+)/i", $postCodeText, $matches);
   $result["organisation_name"] = $matches[1];

   preg_match("/department_name=\"([^\"]+)/i", $postCodeText, $matches);
   $result["department_name"] = $matches[1];

   preg_match("/post_town=\"([^\"]+)/i", $postCodeText, $matches);
   $result["post_town"] = $matches[1];

   preg_match("/barcode=\"([^\"]+)/i", $postCodeText, $matches);
   $result["barcode"] = $matches[1];

   preg_match("/county=\"([^\"]+)/i", $postCodeText, $matches);
   $result["county"] = $matches[1];

   preg_match("/line1=\"([^\"]+)/i", $postCodeText, $matches);
   $result["line1"] = $matches[1];

   preg_match("/line2=\"([^\"]+)/i", $postCodeText, $matches);
   $result["line2"] = $matches[1];

   preg_match("/line3=\"([^\"]+)/i", $postCodeText, $matches);
   $result["line3"] = $matches[1];

   preg_match("/line4=\"([^\"]+)/i", $postCodeText, $matches);
   $result["line4"] = $matches[1];

   preg_match("/line5=\"([^\"]+)/i", $postCodeText, $matches);
   $result["line5"] = $matches[1];

   $result['full_address']      = $dbFullAddress;

   foreach($result as $key => $value)
      $result[$key] = str_replace("'", "", $value);

   $sqlCMD = "INSERT INTO ".SQL_PCODE_DETAILS." (pcs_id,organisation_name,department_name,post_town,barcode,county,line1,line2,line3,line4,line5) VALUES('$addressID','$result[organisation_name]','$result[department_name]','$result[post_town]','$result[barcode]','$result[county]','$result[line1]','$result[line2]','$result[line3]','$result[line4]','$result[line5]')";

   if(! $this->dbh->Exec($sqlCMD))
   {
      $this->strERR = 'CANNOT_ADD_FETCHED_DETAILS';
      return false;
   }

   return $result;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: PostCodeLookup
//
// [DESCRIPTION]:   returns a selectbox that contains addresses for a valid postcode
//
// [PARAMETERS]:    $postCode = ""
//
// [RETURN VALUE]:  select box text | < 0 in case of errors
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-05-28
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function RoyalPostCodeLookup($postCode = "", $fullAddress = "")
{
   // PPP[P] SSS aici

   $fd = fopen("/tmp/postcodesLogs.log","a+");
   fwrite($fd,date("Y-m-d H:i:s")." Postcode royal : ".$postCode."\n");
   fclose($fd);


   $postCode    = split(' ', trim($postCode));

   $postcode = $postCode[0].$postCode[1];

   if(strlen($postCode[0]) == 3 )
      $postcode = $postCode[0]." ".$postCode[1];
   else if(strlen($postCode[0]) == 2 )
      $postcode = $postCode[0]."  ".$postCode[1];

   $selectBoxOptions = "";

   if(empty($postCode))
      return -1;

   // get the adresses from the database
   if(! $addresess = $this->royalPostcodeObj->GetAddresses($postcode))
   {
      //$this->SendErrorMail($this->dbh->GetError()."\nRoyal Mail Error code: -3\n");
      return -3;
   }

   if(is_array($addresess))
   {
      $found  = false;
      $update = false;

      foreach($addresess as $id => $details)
      {
        $postCode = str_replace("'", "",$details["postCode"]);
        $locality = str_replace("'", "",$details["locality"]);
        $thoroughfares = str_replace("'", "",$details["thoroughfares"]);
        $thoroughfares_description = str_replace("'", "",$details["thoroughfares_description"]);
        $dependend_thoroughfares = str_replace("'", "",$details["dependend_thoroughfares"]);
        $dependend_thoroughfares_description = str_replace("'", "",$details["dependend_thoroughfares_description"]);
        $BuildingNumber = str_replace("'", "",$details["BuildingNumber"]);
        $building_name =str_replace("'", "",$details["building_name"]);
        $subbuildings = str_replace("'", "",$details["subbuildings"]);
        $organisation = str_replace("'", "",$details["organisation"]);
        $POBoxnumber = str_replace("'", "",$details["POBoxnumber"]);
        $SingleAddresstext = str_replace("'", "",$details['address_line']);

         $SingleAddresstext = preg_replace("/(\s)+/",' ',$SingleAddresstext);

         $selectBoxOptions[trim($SingleAddresstext)] = trim($SingleAddresstext);

         if(preg_match("/^$fullAddress/i", $SingleAddresstext." "))
         {
             $found = true;

              $fullAddress = trim($SingleAddresstext);

         }

      }

             return $selectBoxOptions;
   }
   elseif($this->dbh->GetError())
   {
      $this->SendErrorMail($this->dbh->GetError()."\nError code: -6\n");
      return -6;
   }

   if(! empty($fullAddress))
        $this->RoyalGeoPostCodeLookup($postCode, $fullAddress);

   return $selectBoxOptions;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GeoPostCodeLookup
//
// [DESCRIPTION]:   returns an array thatr contains: address 1, address 2, town,
//                  county
//
// [PARAMETERS]:    $postCode = ""
//
// [RETURN VALUE]:  array | false in case of errors
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-05-28
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function RoyalGeoPostCodeLookup($postCode = "", $fullAddress = "")
{
   $fd = fopen("/tmp/postcodesLogs.log","a+");
   fwrite($fd,date("Y-m-d H:i:s")." Geo royal : ".$postCode."/".$fullAddress."\n");
   fclose($fd);


   $postCode = preg_replace('/\s+/',' ', trim($postCode));

   $fullAddress = str_replace("'", "", $fullAddress);

   $postCode    = split(' ', trim($postCode));

   $postcode = $postCode[0].$postCode[1];

   if(strlen($postCode[0]) == 3 )
      $postcode = $postCode[0]." ".$postCode[1];
   else if(strlen($postCode[0]) == 2 )
      $postcode = $postCode[0]."  ".$postCode[1];

   // force only one entry if the address is not fully address
   if(! $addresess = $this->royalPostcodeObj->GetAddresses($postcode))
   {
      $this->strERR = 'CANNOT_SELECT_POSTCODE';
      return false;
   }

    $prepAddress = explode(" ", $fullAddress);
    $prepFullAddress = implode(" ", array_slice($prepAddress,0,3)); // get first 3 strings from full address
    
    //added / for preg_quote - WR25-45F
    $prepFullAddress = preg_quote($prepFullAddress, "/");

    foreach($addresess as $AddrId => $AddrLine)
    {
        if(preg_match("/^$prepFullAddress/i", $AddrLine['address_line']))
           break;

    }

    $addressline = $AddrLine;

   // we have an entry and return this
   if(is_array($addressline))
   {
//print_r($addressline);
      $counties = $this->royalPostcodeObj->GetCountyByPostcode($postcode);

      $postCode = str_replace("'", "",$addressline["postCode"]);
      $locality = str_replace("'", "",$addressline["locality"]);
      $thoroughfares = str_replace("'", "",$addressline["thoroughfares"]);
      $thoroughfares_description = str_replace("'", "",$addressline["thoroughfares_description"]);
      $dependend_thoroughfares = str_replace("'", "",$addressline["dependend_thoroughfares"]);
      $dependend_thoroughfares_description = str_replace("'", "",$addressline["dependend_thoroughfares_description"]);
      $buildingNumber = intval($addressline["BuildingNumber"]);
      $buildingName =str_replace("'", "",$addressline["building_name"]);
      $subBuildings = str_replace("'", "",$addressline["subbuildings"]);
      $organisation = str_replace("'", "",$addressline["organisation"]);
      $POBoxnumber = trim(str_replace("'", "",$addressline["POBoxnumber"]));
      $singleAddresstext = str_replace("'", "",$addressline['address_line']);

     $line1 = "";

     if(! empty($POBoxnumber))
        $line1 = "Po Box ".$POBoxnumber;

     if(! empty($buildingNumber))
     {
        if(! empty($line1))
            $line2 = trim(ucwords(strtolower(trim($buildingNumber))).' '.ucwords(strtolower(trim($dependend_thoroughfares))).' '.ucwords(strtolower(trim($dependend_thoroughfares_description))).' '.ucwords(strtolower(trim($thoroughfares))).' '.ucwords(strtolower(trim($thoroughfares_description))));
        else
          $line1 = trim(ucwords(strtolower(trim($buildingNumber))).' '.ucwords(strtolower(trim($dependend_thoroughfares))).' '.ucwords(strtolower(trim($dependend_thoroughfares_description))).' '.ucwords(strtolower(trim($thoroughfares))).' '.ucwords(strtolower(trim($thoroughfares_description))));
     }

     if(! empty($subBuildings))
     {
        if(! empty($line2))
        {
           $line3 = $line2;
           $line2 = ucwords(strtolower(trim($subBuildings)));
        }
        else
        {
           $line2 = $line1;
           $line1 = ucwords(strtolower(trim($subBuildings)));
        }
     }

     if(! empty($buildingName))
     {
        if(! empty($line3))
        {
           $line4 = $line3;
           $line3 = ucwords(strtolower(trim($buildingName)));
        }
        else
        {
           //$line3 = $line2;
           //$line2 = ucwords(strtolower(trim($buildingName)));

           // new changes by adi
           if(! empty($line1))
           {
              $line3 = $line2;
              $line2 = ucwords(strtolower(trim($buildingName)));
           }
           else
           {
              $line1 = ucwords(strtolower(trim($buildingName)));
              $line2 = ucwords(strtolower(trim($dependend_thoroughfares))).' '.ucwords(strtolower(trim($dependend_thoroughfares_description))).' '.ucwords(strtolower(trim($thoroughfares))).' '.ucwords(strtolower(trim($thoroughfares_description)));
           }
        }
      }
      elseif(! empty($organisation))
      {
        if(! empty($line3))
        {
           $line4 = $line3;
           $line3 = ucwords(strtolower(trim($buildingName)));
        }
        else
        {
           if(! empty($line1))
           {
              $line3 = $line2;
              $line2 = ucwords(strtolower(trim($buildingName)));
           }
           else
           {
              $line1 = ucwords(strtolower(trim($organisation)));
              $line2 = ucwords(strtolower(trim($dependend_thoroughfares))).' '.ucwords(strtolower(trim($dependend_thoroughfares_description))).' '.ucwords(strtolower(trim($thoroughfares))).' '.ucwords(strtolower(trim($thoroughfares_description)));
           }
         }
       }

      $Locality ='';

      if(! empty($locality))
      {
         $prepLocality = preg_replace("/\s\s/",":", $locality);
         $prepLocality = preg_replace("/:+/",":", $prepLocality);

         $localityDetails = explode(":", $prepLocality);

         $Locality = ucwords(strtolower($localityDetails[0]));

         $subSubLocality = ucwords(strtolower($localityDetails[1]));
         $subLocality = ucwords(strtolower($localityDetails[2]));
      }

      if(! empty($subLocality))
      {
         if(empty($line2))
            $line2 = $subLocality;
         elseif(empty($line3))
            $line3 = $subLocality;
         elseif(empty($line4))
            $line4 = $subLocality;
         else
            $line5 = $subLocality;
      }

      if(! empty($subSubLocality))
      {
         if(empty($line2))
            $line2 = $subSubLocality;
         elseif(empty($line3))
            $line3 = $subSubLocality;
         elseif(empty($line4))
            $line4 = $subSubLocality;
         else
            $line5 = $subSubLocality;
      }

      $singleAddresstext = preg_replace("/(\s)+/",' ',$singleAddresstext);

      $result['organisation_name'] = ucwords(strtolower($addressline['organisation']));
      $result['department_name']   = '';
      $result['post_town']         = trim($Locality);
      $result['barcode']           = '';
      // The Royal mail County Type is T, P or A. T = Traditional County, P = Former Postal County and A =Administrative County.

      $result['county']            = ucwords(strtolower($counties['P']));
      if(empty($result['county']))
         $result['county']       = ucwords(strtolower($counties['T']));
      if(empty($result['county']))
         $result['county']        = ucwords(strtolower($counties['A']));

      $result['line1']                       = trim($line1);
      $result['line2']                       = trim($line2);
      $result['line3']                       = trim($line3);
      $result['line4']                       = trim($line4);
      $result['line5']                       = trim($line5);

      $result['full_address']                = trim($singleAddresstext);
      $result['road']                        = trim(ucwords(strtolower(trim($dependend_thoroughfares))).' '.ucwords(strtolower(trim($dependend_thoroughfares_description))).' '.ucwords(strtolower(trim($thoroughfares))).' '.ucwords(strtolower(trim($thoroughfares_description))));
      $result['building_name_and_number']    = trim(trim($subBuildings)." ".trim($buildingName)." ".($buildingNumber ? trim($buildingNumber) : ""));

      $result['buildingNumber'] = $buildingNumber;
      $result['subBuildings']   = $subBuildings;
      $result['buildingName']   = $buildingName;
   }

    return $result;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SendErrorMail
//
// [DESCRIPTION]:   Sends an error email in case we get an error from the postcode
//                  lookup function
//
// [PARAMETERS]:    $errorMsg = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2005-08-04
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SendErrorMail($errorMsg = "")
{
   $subject = "postcode lookup error";

   $headers  = "From: QuoteZone <errors@quotezone.co.uk>\n";
   $headers .= "X-Sender: <errors@quotezone.co.uk>\n";
   $headers .= "X-Mailer: PHP\n"; // mailer
   $headers .= "X-Priority: 1\n"; // Urgent message!
   $headers .= "Return-Path: <greg@seopa.com>\n"; // Return path for errors

   //removed due to : WR4N-KS8
   
   //removed
   // $to = "greg@seopa.com";
   // mail($to,$subject,$errorMsg,$headers);

   // SMS to seugen and greg
   $to = "valarm-seugen@acrux.biz";
   mail($to,$subject,$errorMsg,$headers);

   //removed
   // $to = "savin.eugen@gmail.com";
   // mail($to,$subject,$errorMsg,$headers);
   // $to = "gabi@acrux.biz";
   // mail($to,$subject,$errorMsg,$headers);
   // $to = "cipi@acrux.biz";
   // mail($to,$subject,$errorMsg,$headers);
   
   $to = "adi@acrux.biz";
   mail($to,$subject,$errorMsg,$headers);
   
   //added
   $to = "ebusiness1@seopa.com";
   mail($to,$subject,$errorMsg,$headers);

   $to = "eb1-technical@seopa.com";
   mail($to,$subject,$errorMsg,$headers);
}

function updatePcodeStreets()
{
   set_time_limit(0);

   $this->dbh->Exec("SELECT * FROM ".SQL_PCODE_STREETS." limit 10");

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;

      $result[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("street_nr")." ".$this->dbh->GetFieldValue("address");

      if(! $streetNr = $this->dbh->GetFieldValue("street_nr"))
         $result[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("address");

      if(! preg_match("/^[A-Z]/i", $this->dbh->GetFieldValue("address")))
         $result[$this->dbh->GetFieldValue("id")]['ADDRESS'] = $this->dbh->GetFieldValue("address");


      $result[$this->dbh->GetFieldValue("id")]['PCODE_ID']   = $this->dbh->GetFieldValue("pcode_id");

   }

// print_r($result);

   foreach($result as $id => $resShit)
   {
      $sqlCMD = "INSERT INTO ".SQL_PCODE_STREETS." (pcode_id,address) VAUES('$resShit[PCODE_ID]','$resShit[ADDRESS]')";

      if(! $this->dbh->Exec($sqlCMD))
      {
         print $this->dbh->strERR;
         print ",";
         continue;
      }

      print ".";
   }

}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SkipPostcode
//
// [DESCRIPTION]:   If we have the postcode into db we should skip the quote
//
//
// [PARAMETERS]:    $postcode, $siteID
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Gabi ISTVANCSEK  (gabi@acrux.biz) 2006-10-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SkipPostcode($postcode="", $siteID="")
{
   if(empty($postcode))
      return true;

   if(empty($siteID))
      return true;

   $this->dbh->Exec("SELECT * FROM ".SQL_SKIP_POSTCODES." WHERE postcode='$postcode' AND site_id='$siteID'");

   if($this->dbh->GetRows())
      return true;

   return false;
}


}// end Class CPostcode
?>

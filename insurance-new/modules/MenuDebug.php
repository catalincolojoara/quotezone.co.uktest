<?php
/*****************************************************************************/
/*                                                                           */
/*  CMenu class interface                                               */
/*                                                                           */
/*  (C) 2006 Istvancsek Gabriel (gabi@acrux.biz)                                     */
/*                                                                           */
/*****************************************************************************/

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CMenu
//
// [DESCRIPTION]:  CMenu - navigation menu of home insurance
//
// [FUNCTIONS]:
//
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz) 2006-01-05
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CMenu
{
   var $_post;
   var $_get;
   var $menuType;

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CMenu
   //
   // [DESCRIPTION]:   constructor
   //
   // [PARAMETERS]:    $_SESSION
   //
   // [RETURN VALUE]:  $numberOfAllInsurers | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CMenu($type='home', $SESSION="")
   {
      session_start();

      if(! empty($SESSION))
         $_SESSION = $SESSION;

      $this->_post           = $_POST;
      $this->_get            = $_GET;
      $this->menuType        = strtolower($type);

      $this->primaryKeys = array();

      switch($this->menuType)
      {
         case 'home':
            $this->primaryKeys[1] = 'INTRO';
            $this->primaryKeys[2] = 'HOME';
            $this->primaryKeys[3] = 'HOME_EXTRA';
            $this->primaryKeys[4] = 'INSURED';
            $this->primaryKeys[5] = 'COVER';
            $this->primaryKeys[6] = 'BUILDING_CLAIMS';
            $this->primaryKeys[7] = 'CONTENTS_CLAIMS';
            $this->primaryKeys[8] = 'QUOTE';
            $this->primaryKeys[9] = 'QUOTE RESULTS';

            // temp data
            $this->primaryKeys[10] = 'TMP';

            $this->primaryName    = 'category';
            $this->secondaryName  = 'idInsured';
            $this->thirdName      = 'idInsuredHistory';
             break;

         case 'bike':
            $this->primaryKeys[1] = 'VEHICLE';
            $this->primaryKeys[2] = 'DRIVERS';
            $this->primaryKeys[3] = 'COVER';
            $this->primaryKeys[4] = 'CLAIMS';
            $this->primaryKeys[5] = 'CONVICTIONS';
            $this->primaryKeys[6] = 'QUOTE';
            $this->primaryKeys[7] = 'QUOTE RESULTS';
            // temp data
            $this->primaryKeys[8] = 'TMP';

            $this->primaryName    = 'category';
            $this->secondaryName  = 'idDriver';
            $this->thirdName      = 'idDriverHistory';
             break;

         case 'car':
            $this->primaryKeys[1] = 'VEHICLE';
            $this->primaryKeys[2] = 'DRIVERS';
            $this->primaryKeys[3] = 'COVER';
            $this->primaryKeys[4] = 'CLAIMS';
            $this->primaryKeys[5] = 'CONVICTIONS';
            $this->primaryKeys[6] = 'QUOTE';
            $this->primaryKeys[7] = 'QUOTE RESULTS';

            $this->primaryKeys[8] = 'TMP';

            $this->primaryName    = 'category';
            $this->secondaryName  = 'idDriver';
            $this->thirdName      = 'idDriverHistory';
            break;

         case 'van':
            $this->primaryKeys[1] = 'VEHICLE';
            $this->primaryKeys[2] = 'DRIVERS';
            $this->primaryKeys[3] = 'COVER';
            $this->primaryKeys[4] = 'CLAIMS';
            $this->primaryKeys[5] = 'CONVICTIONS';
            $this->primaryKeys[6] = 'QUOTE';
            $this->primaryKeys[7] = 'QUOTE RESULTS';
            $this->primaryName    = 'category';
            $this->secondaryName  = 'idDriver';
            $this->thirdName      = 'idDriverHistory';
            break;
      }

      foreach($this->primaryKeys as $index => $key)
         $this->sessionKeys[$key] = "_".$key."_";
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CMenu
   //
   // [DESCRIPTION]:   ValidatePrimaryKey
   //
   // [PARAMETERS]:    $primaryKey
   //
   // [RETURN VALUE]:  integer | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function ValidatePrimaryKey($primaryKey)
   {
      if(empty($primaryKey))
      {
         $this->strErr = 'EMPTY_PRIMARY_KEY';
         return false;
      }
      
      if(! in_array(strtoupper($primaryKey), $this->primaryKeys))
      {

         $this->strErr = 'INVALID_PRIMARY_KEY';
         return false;
      }

      return true;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CMenu
   //
   // [DESCRIPTION]:   ValidateKey
   //
   // [PARAMETERS]:    $secondaryKey
   //
   // [RETURN VALUE]:  integer | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function ValidateKey($secondaryKey)
   {
      if(! preg_match("/^\d+$/",$secondaryKey))
      {
         return false;
      }

      return true;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CMenu
   //
   // [DESCRIPTION]:   ValidateKey
   //
   // [PARAMETERS]:    $secondaryKey
   //
   // [RETURN VALUE]:  integer | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function ValidateNavigationKey($navigationKey)
   {
      if(empty($navigationKey))
      {
         $this->strErr = 'EMPTY_NAVIGATION_KEY';
         return false;
      }
      
      $navigationKey = strtolower($navigationKey);
      
      if(! preg_match("/^(next)|(back)$/",$navigationKey))
      {
         $this->strErr = 'INVALID_NAVIGATION_KEY';
         return false;
      }

      return true;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetSessionCount
   //
   // [DESCRIPTION]:   Get number of all insurers
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey=''
   //
   // [RETURN VALUE]:  $numberOfAllInsurers | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetSessionCount($primaryKey, $secondaryKey='', $thirdKey='')
   {
      if(! $this->ValidatePrimaryKey($primaryKey))
         return false;

      if(empty($_SESSION[$this->sessionKeys[$primaryKey]]))
         return 0;

      if(! $this->ValidateKey($secondaryKey))
         return count($_SESSION[$this->sessionKeys[$primaryKey]]);

      if(! $this->ValidateKey($thirdKey))
         return count($_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey]);

      return count($_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey][$thirdKey]);
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetSessionContent
   //
   // [DESCRIPTION]:   Get session content
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey=''
   //
   // [RETURN VALUE]:  array| string | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetSessionContent($key='', $primaryKey='', $secondaryKey='', $thirdKey='')
   {
      if(! $this->ValidatePrimaryKey($primaryKey))
         return false;

      if(empty($_SESSION[$this->sessionKeys[$primaryKey]]))
         return false;
   
      if(! $this->ValidateKey($secondaryKey))
      {
         $value = $_SESSION[$this->sessionKeys[$primaryKey]];

         if(! empty($key))
            $value = $_SESSION[$this->sessionKeys[$primaryKey]][$key];
         
         return $value;
      }

      if(! $this->ValidateKey($thirdKey))
      {
         $value = $_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey];

         if(! empty($key))
            $value = $_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey][$key];

         return $value;
      }

      $value = $_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey][$thirdKey];

      if(! empty($key))
         $value = $_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey][$thirdKey][$key];
      
      return $value;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: SetSessionContent
   //
   // [DESCRIPTION]:   Set session content
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey=''
   //
   // [RETURN VALUE]:  array| string | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SetSessionContent($key='', $value='', $primaryKey='', $secondaryKey='', $thirdKey='')
   {
      if(! $this->ValidatePrimaryKey($primaryKey))
         return false;

      if(! $this->ValidateKey($secondaryKey))
      {
         if(! empty($key))
            $_SESSION[$this->sessionKeys[$primaryKey]][$key] = $value;
    
         return true;
      }

      if(! $this->ValidateKey($thirdKey))
      {
         if(! empty($key))
            $_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey][$key] = $value;

         return true;
      }

      $_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey][$thirdKey][$key] = $value;

      return true;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetSessionContent
   //
   // [DESCRIPTION]:   Get session content
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey=''
   //
   // [RETURN VALUE]:  array | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function DeleteSessionContent($key='', $primaryKey='', $secondaryKey='', $thirdKey='')
   {
      if(! $this->ValidatePrimaryKey($primaryKey))
         return false;

      if(empty($_SESSION[$this->sessionKeys[$primaryKey]]))
         return false;
   
      if(! $this->ValidateKey($secondaryKey))
      {
         if(! empty($key))
            unset($_SESSION[$this->sessionKeys[$primaryKey]][$key]);
         else
            unset($_SESSION[$this->sessionKeys[$primaryKey]]);

         return true;
      }

      if(! $this->ValidateKey($thirdKey))
      {
         if(! empty($key))
            unset($_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey][$key]);
         else
            unset($_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey]);

         return true;
      }

      if(! empty($key))
         unset($_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey][$thirdKey][$key]);
      else
         unset($_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey][$thirdKey]);

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ExistSessionContent
   //
   // [DESCRIPTION]:   Get session content
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey='', $thirdKey=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function ExistSessionContent($primaryKey='', $secondaryKey='', $thirdKey='')
   {
      $retCode = false;
      if($this->ValidatePrimaryKey($primaryKey))
         if(! empty($_SESSION[$this->sessionKeys[$primaryKey]]))
            $retCode = true;

      if($this->ValidateKey($secondaryKey))
      {
         $retCode = false;
         if(! empty($_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey]))
            $retCode = true;
      }

      if($this->ValidateKey($thirdKey))
      {
         $retCode = false;
         if(! empty($_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey][$thirdKey]))
            $retCode = true;
      }

      return $retCode;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AddMenuStep
   //
   // [DESCRIPTION]:   Add step to the navigation menu
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey='', $thirdKey=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AddMenuStep($primaryKey, $secondaryKey='', $thirdKey='')
   {
      if(! $this->ValidatePrimaryKey($primaryKey))
         return false;

      $counter = count($_SESSION["_NAVIGATION_STEP_"]);

      if(! $counter)
         $counter = 1;
   
      $_SESSION["_NAVIGATION_STEP_"][$counter][$this->primaryName]   = $primaryKey;
      $_SESSION["_NAVIGATION_STEP_"][$counter][$this->secondaryName] = $secondaryKey;
      $_SESSION["_NAVIGATION_STEP_"][$counter][$this->thirdName]     = $thirdKey;

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: InsertMenuStep
   //
   // [DESCRIPTION]:   Add step to the navigation menu
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey='', $thirdKey=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function InsertMenuStep($step, $primaryKey, $secondaryKey='', $thirdKey='')
   {
      if(! $this->ValidateKey($step))
         return false;

      if(! $this->ValidatePrimaryKey($primaryKey))
         return false;

      if($secondaryKey !== '')
      {
         if(! $this->ValidateKey($secondaryKey))
            return false;
      }

      if($thirdKey !== '')
      {
         if(! $this->ValidateKey($thirdKey))
            return false;
      }

      $totalSteps = count($_SESSION["_NAVIGATION_STEP_"]);

      $bufferSessionSteps[$step][$this->primaryName]   = $primaryKey;
      $bufferSessionSteps[$step][$this->secondaryName] = $secondaryKey;
      $bufferSessionSteps[$step][$this->thirdName]     = $thirdKey;

      for($i = $step; $i <= $totalSteps; $i++)
      {
         $bufferSessionSteps[$i+1] = $_SESSION["_NAVIGATION_STEP_"][$i];
      }
   
      for($i = 1; $i <= $totalSteps + 1; $i++)
      {
         if(! empty($bufferSessionSteps[$i]))
            $_SESSION["_NAVIGATION_STEP_"][$i] = $bufferSessionSteps[$i];
      }

      $this->_setServerPost();
      $this->_setServerGet();

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: DeleteMenuStep
   //
   // [DESCRIPTION]:   Delete step form the navigation menu
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey='', $thirdKey=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function DeleteMenuStep($primaryKey, $secondaryKey='', $thirdKey='')
   {
      if(! $this->ValidatePrimaryKey($primaryKey))
         return false;

      foreach($_SESSION["_NAVIGATION_STEP_"] as $counter => $resContent)
      {
         if($resContent[$this->primaryName] == $primaryKey && $resContent[$this->secondaryName] == $secondaryKey && $resContent[$this->thirdName] == $thirdKey)
         {
            unset($_SESSION["_NAVIGATION_STEP_"][$counter]);
         }
      }
   
      $this->OrdonateMenuSteps();

      return true;

   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: DeleteMenuStep
   //
   // [DESCRIPTION]:   Delete step form the navigation menu
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey='', $thirdKey=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CheckMenuStep($primaryKey, $secondaryKey='', $thirdKey='')
   {
      if(! $this->ValidatePrimaryKey($primaryKey))
         return false;

      foreach($_SESSION['_NAVIGATION_STEP_'] as $count => $resContent)
      {
         if($resContent[$this->primaryName] == $primaryKey && $resContent[$this->secondaryName] == $secondaryKey && $resContent[$this->thirdName] == $thirdKey)
            return $count;
      }
   
      return false;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: DeleteMenuStep
   //
   // [DESCRIPTION]:   Delete step form the navigation menu
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey='', $thirdKey=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CheckSecondaryMenuStepLimit($primaryKey, $secondaryKey='')
   {
      if(! $this->ValidatePrimaryKey($primaryKey))
         return false;

      if($secondaryKey !== '')
      {
         if(! $this->ValidateKey($secondaryKey))
            return false;
      }

      $count = false;
      foreach($_SESSION['_NAVIGATION_STEP_'] as $count => $resContent)
      {
         if($resContent[$this->primaryName] == $primaryKey  && $resContent[$this->secondaryName] <= $secondKey)
            $step = $count;
      }
   
      return $step;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: DeleteMenuStep
   //
   // [DESCRIPTION]:   Delete step form the navigation menu
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey='', $thirdKey=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CheckPrimaryMenuStepLimit($primaryKey)
   {
      if(! $this->ValidatePrimaryKey($primaryKey))
         return false;

      $count = false;
      foreach($_SESSION['_NAVIGATION_STEP_'] as $count => $resContent)
      {
         if($resContent[$this->primaryName] == $primaryKey)
            $step = $count;
      }
   
      return $step;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: DeleteMenuStep
   //
   // [DESCRIPTION]:   Delete step form the navigation menu
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey='', $thirdKey=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SetPrimaryMenuStepRedirect($primaryKey,$secondaryKey="", $thirdKey="")
   {
      if(! $this->ValidatePrimaryKey($primaryKey))
         return false;

      $_SESSION["_NAVIGATION_"][$this->primaryName]   = $primaryKey;
      $_SESSION["_NAVIGATION_"][$this->secondaryName] = $secondaryKey;
      $_SESSION["_NAVIGATION_"][$this->thirdName]     = $thirdKey;

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: OrdonateMenuSteps
   //
   // [DESCRIPTION]:   Delete step form the navigation menu
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey='', $thirdKey=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function OrdonateMenuSteps()
   {
      $navigationStep = $_SESSION['_NAVIGATION_STEP_'];
      unset($_SESSION['_NAVIGATION_STEP_']);
   
      ksort($navigationStep);
   
      $i = 0;
      foreach($navigationStep as $count => $resContent)
      {
         $i++;
         $_SESSION['_NAVIGATION_STEP_'][$i] = $resContent;
      }

   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CheckActiveMenuStep()
   //
   // [DESCRIPTION]:   Delete step form the navigation menu
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey='', $thirdKey=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CheckActiveMenuStep()
   {
      $count = 0;
      foreach($_SESSION['_NAVIGATION_STEP_'] as $count => $resContent)
      {
         if($_SESSION['_NAVIGATION_STEP_'][$count]['status'] == 'active')
            break;
      }
   
      return $count;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CheckActiveMenuStep()
   //
   // [DESCRIPTION]:   Delete step form the navigation menu
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey='', $thirdKey=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetActiveMenuStep(&$action, &$formAction, &$primaryKey, &$secondaryKey, &$thirdKey)
   {
      $step = $this->CheckActiveMenuStep();
   
      $action     = "UPDATE";
      $formAction = "SHOW";
   
      $primaryKey   = $_SESSION["_NAVIGATION_STEP_"][$step][$this->primaryName];
      $secondaryKey = $_SESSION["_NAVIGATION_STEP_"][$step][$this->secondaryName];
      $thirdKey     = $_SESSION["_NAVIGATION_STEP_"][$step][$this->thirdName];

   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CheckActiveMenuStep()
   //
   // [DESCRIPTION]:   Delete step form the navigation menu
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey='', $thirdKey=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SetActiveMenuStep($type)
   {
      $type = strtolower($type);

      if(! $this->ValidateNavigationKey($type))
         return false;

      // get current step
      foreach($_SESSION['_NAVIGATION_STEP_'] as $count => $resContent)
      {
         if($_SESSION['_NAVIGATION_STEP_'][$count]['status'] == 'active')
            break;
      }

      switch($type)
      {
         case "back":
      
            if(! empty($_SESSION['_NAVIGATION_STEP_'][$count]))
               $_SESSION['_NAVIGATION_STEP_'][$count]['status'] = '';
      
            $count--;
            if(! $count)
               $count = 1;
      
            $_SESSION['_NAVIGATION_STEP_'][$count]['status'] = 'active';
            break;
      
         case "next":
      
            if(! empty($_SESSION['_NAVIGATION_STEP_'][$count]))
               $_SESSION['_NAVIGATION_STEP_'][$count]['status'] = '';
      
            $count++;
            $_SESSION['_NAVIGATION_STEP_'][$count]['status'] = 'active';
      
            break;
      }

      $_SESSION['ACTIVE STEP'] = false;

   }
   
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CheckActiveMenuStep()
   //
   // [DESCRIPTION]:   Delete step form the navigation menu
   //
   // [PARAMETERS]:    $primaryKey, $secondaryKey='', $thirdKey=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-10-09
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function ProcessMenu(&$action, &$formAction, &$primaryKey, &$secondaryKey, &$thirdKey)
   {
      if(empty($_SESSION['_NAVIGATION_']))
      {
         $_SESSION['_NAVIGATION_']['action']           = 'ADD';
         $_SESSION['_NAVIGATION_'][$this->primaryName] = $this->primaryKeys[1];
         $_SESSION['_NAVIGATION_']['form_action']      = 'SHOW';
      }

      // read values from $_GET if we don't have $_POST
      $action = $_SESSION["_NAVIGATION_"]["action"];

      $secondaryKey = $this->_post[$this->secondaryName];

      $thirdKey = $this->_post[$this->thirdName];
   
      $primaryKey = $_SESSION["_NAVIGATION_"][$this->primaryName];

      if($_SESSION['ACTIVE STEP'] || ($this->CheckActiveMenuStep() < count($_SESSION['_NAVIGATION_STEP_'])))
      {
         $this->GetActiveMenuStep($action, $formAction, $primaryKey, $secondaryKey, $thirdKey);
         $formAction = '';

         if($this->_post['step'] == 'back')
         {
            $this->SetActiveMenuStep('back');
            $this->GetActiveMenuStep($action, $formAction, $primaryKey, $secondaryKey, $thirdKey);
   
         }

         $_SESSION['ACTIVE STEP'] = true;
      }
   
      if(! $_SESSION['ACTIVE STEP'])
         if($this->_post['step'] == 'back')
         {
            $this->SetActiveMenuStep('back');
            $this->GetActiveMenuStep($action, $formAction, $primaryKey, $secondaryKey, $thirdKey);
   
            $_SESSION['ACTIVE STEP'] = true;
         }
   
      if($this->_post['step'] == 'quit')
         $primaryKey = "QUIT";
   
      if($formAction == "")
         $formAction = $this->_post["form_action"];
   
      if($formAction == "")
         $formAction = $_SESSION['_NAVIGATION_']['form_action'];

      if(! $this->ValidateKey($secondaryKey))
      {
         $secondaryKey = $_SESSION["_NAVIGATION_"][$this->secondaryName];
      }

      if(! $this->ValidateKey($thirdKey))
      {
         $thirdKey = $_SESSION["_NAVIGATION_"][$this->thirdName];
      }

      $this->_setServerPost();
      $this->_setServerGet();

   }

   function _setServerPost()
   {
      $_POST    = $this->_post;
   }

   function _setServerGet()
   {
      $_GET     = $this->_get;
   }

# EXTRA FUNCTIONS

   function SetNavigationKey( $key, $value)
   {
      $_SESSION["_NAVIGATION_"][$key] = $value;
   }

   function GetNavigationKey($key)
   {
      return $_SESSION["_NAVIGATION_"][$key];
   }

   function GetNavigationStepContent()
   {
      return $_SESSION["_NAVIGATION_STEP_"];
   }

   function SetNavigationStep($value="")
   {
      $_SESSION["_NAVIGATION_STEP_"] = $value;
   }

   function SetActiveStep($flag)
   {
      $_SESSION["ACTIVE STEP"] = $flag;
   }

   function UnsetSessionContent($value='', $primaryKey='', $secondaryKey='', $thirdKey='')
   {
      if(! $this->ValidatePrimaryKey($primaryKey))
         return false;

      if(! $this->ValidateKey($secondaryKey))
      {
         $_SESSION[$this->sessionKeys[$primaryKey]] = $value;
    
         return true;
      }

      if(! $this->ValidateKey($thirdKey))
      {
         $_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey] = $value;

         return true;
      }

      $_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey][$thirdKey] = $value;

      return true;
   }

   function SetSessionContentArray($value='', $primaryKey='', $secondaryKey='', $thirdKey='')
   {
      if(! $this->ValidatePrimaryKey($primaryKey))
         return false;

      if(! $this->ValidateKey($secondaryKey))
      {
         $_SESSION[$this->sessionKeys[$primaryKey]] = $value;
    
         return true;
      }

      if(! $this->ValidateKey($thirdKey))
      {
         $_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey] = $value;

         return true;
      }

      $_SESSION[$this->sessionKeys[$primaryKey]][$secondaryKey][$thirdKey] = $value;

      return true;
   }

}

?>

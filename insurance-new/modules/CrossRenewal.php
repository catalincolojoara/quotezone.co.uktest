<?php
/*****************************************************************************/
/*                                                                           */
/*  CCrossRenewal class interface                                              */
/*                                                                           */
/*  (C) 2008 Moraru Valeriu (vali@acrux.biz)                              */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CCrossRenewal
//
// [DESCRIPTION]:  CCrossRenewal class interface
//
// [FUNCTIONS]:
//
//                 Close();
//                 GetError();
//                 ShowError();
//
// [CREATED BY]:   Moraru Valeriu (vali@acrux.biz) 2008-06-16
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CCrossRenewal
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CCrossRenewal
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-06-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CCrossRenewal($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertCrossRenewal
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    &$firstName, &$lastName, $birthDate='', $password=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-06-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertCrossRenewal($quoteUserID, $qzQuoteID, $emStoreID, $crDate, $upDate)
{
   $this->strERR = '';

   if(! preg_match('/\d+/i',$quoteUserID))
      $this->strERR .= GetErrorString("INVALID_USER_ID_FIELD");

   if(! preg_match('/\d+/i',$qzQuoteID))
      $this->strERR .= GetErrorString("INVALID_QZ_QUOTE_ID_FIELD");

   if(! preg_match('/\d+/i',$emStoreID))
      $this->strERR .= GetErrorString("INVALID_EM_STORE_ID_FIELD");

   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$crDate))
      $this->strERR .= GetErrorString("INVALID_CR_DATE_FIELD")."\n";

   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$upDate))
      $this->strERR .= GetErrorString("INVALID_UPDATE_FIELD")."\n";

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddCrossRenewal
//
// [DESCRIPTION]:   Add into cross_renewal table
//
// [PARAMETERS]:    $emStoreID, $crDate
//
// [RETURN VALUE]:  $renewalID | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-06-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddCrossRenewal($emStoreID, $crDate, $crTime,$type,$year,$campaignID,$compareNI=0)
{
   if(! preg_match('/\d+/i',$emStoreID))
   {
      $this->strERR .= GetErrorString("INVALID_EM_STORE_ID_FIELD");
      return false;
   }

   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$crDate))
   {
      $this->strERR .= GetErrorString("INVALID_CR_DATE_FIELD")."\n";
      return false;
   }

   if(empty($campaignID))
   {
      $this->strERR .= GetErrorString("INVALID_CAMPAIGN_ID_FIELD")."\n";
      return false;
   }

   $this->lastSQLCMD = "INSERT INTO cross_renewal (em_store_id,cr_date,cr_time,quote_type_id,year,campaign_id,compareNI) VALUES ('$emStoreID','$crDate','$crTime','$type','$year','$campaignID','$compareNI')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      print $this->strERR = $this->dbh->GetError();die;
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InsertCrossRenewalQuote
//
// [DESCRIPTION]:   Insert in table cross_renewal_quotes a quote
//
// [PARAMETERS]:    $renewalID, $quoteUserID, $qzQuoteID, $quoteDate, $quoteTime
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:   Florin (florin@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InsertCrossRenewalQuote($renewalID, $quoteUserID, $qzQuoteID, $quoteDate, $quoteTime)
{

   if(! preg_match("/^\d+$/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_USER_ID_FIELD");
      return false;
   }

   if(! preg_match('/\d+/i',$qzQuoteID))
   {
      $this->strERR .= GetErrorString("INVALID_QZ_QUOTE_ID_FIELD");
      return false;
   }

   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$quoteDate))
   {
      $this->strERR .= GetErrorString("INVALID_UPDATE_FIELD")."\n";
      return false;
   }

   // get log_id
   $this->lastSQLCMD = "SELECT log_id FROM qzquotes WHERE id='$qzQuoteID'";
   
   //if($_SERVER['REMOTE_ADDR'] == "81.196.65.167")
   //{
   //   print $this->lastSQLCMD;
   //}

   if(! $this->dbh->Exec($this->lastSQLCMD,true))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   $logId = $this->dbh->GetFieldValue("log_id");
   //print "<!--Log id =  [ $logId ] -->";

   $this->lastSQLCMD = "INSERT INTO cross_renewal_quotes (log_id,cross_renewal_id,date,time) VALUES ('$logId','$renewalID','$quoteDate','$quoteTime')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCrossRenewalsByDate
//
// [DESCRIPTION]:   Get number of cross_renewals by date
//
// [PARAMETERS]:    $dateFrom, $dateTo, $year='1'
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:   Moraru Valeriu (vali@acrux.biz) 2008-06-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCrossRenewalsByDate($dateFrom, $dateTo, $year='1')
{
   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$dateFrom))
   {
      $this->strERR .= GetErrorString("INVALID_DATEFROM_FIELD")."\n";
      return false;
   }

   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$dateTo))
   {
      $this->strERR .= GetErrorString("INVALID_DATETO_FIELD")."\n";
      return false;
   }

   $this->lastSQLCMD = "SELECT count(distinct(em_store_id)) as cnt FROM cross_renewal WHERE cr_date>='$dateFrom' AND cr_date<='$dateTo' AND year='$year'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");

   return $cnt;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCrossRenewalsByDateAndType
//
// [DESCRIPTION]:   Get number of cross_renewals by date
//
// [PARAMETERS]:    $dateFrom, $dateTo, $type, $year='1'
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-06-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCrossRenewalsByDateAndType($dateFrom, $dateTo, $type, $year='1')
{
   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$dateFrom))
   {
      $this->strERR .= GetErrorString("INVALID_DATEFROM_FIELD")."\n";
      return false;
   }

   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$dateTo))
   {
      $this->strERR .= GetErrorString("INVALID_DATETO_FIELD")."\n";
      return false;
   }

   $this->lastSQLCMD = "SELECT count(distinct(em_store_id)) as cnt FROM cross_renewal WHERE cr_date>='$dateFrom' AND cr_date<='$dateTo' and quote_type_id='$type' and year='$year'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");

   return $cnt;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCrossQuotesByDate($dateFrom, $dateTo, $type='1',$year='1')
//
// [DESCRIPTION]:   Get number of cross_renewals quotes by date
//
// [PARAMETERS]:    $dateFrom, $dateTo, $type='1',$year='1'
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-06-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCrossQuotesByDate($dateFrom, $dateTo, $type='1',$year='1')
{
   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$dateFrom))
   {
      $this->strERR .= GetErrorString("INVALID_DATEFROM_FIELD")."\n";
      return false;
   }

   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$dateTo))
   {
      $this->strERR .= GetErrorString("INVALID_DATETO_FIELD")."\n";
      return false;
   }

   $this->lastSQLCMD = "SELECT count(*) as cnt FROM cross_renewal WHERE update_date>='$dateFrom' AND update_date<='$dateTo' and quote_type_id='$type' and year='$year'";// AND quote_user_id!='0'";
//   $this->lastSQLCMD = "SELECT count(*) as cnt FROM renewal r,qzquotes qzq WHERE r.update_date>='$dateFrom' AND r.update_date<='$dateTo' and r.qz_quote_id=qzq.id and qzq.quote_type_id='$type'";
//print $this->lastSQLCMD;
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");   
   
   return $cnt;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCrossQuotesByDateAndType($dateFrom, $dateTo, $type,$year='1',$renType)
//
// [DESCRIPTION]:   Get number of  cross renewals quotes by date and type
//
// [PARAMETERS]:    $dateFrom, $dateTo, $type,$year='1',$renType
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-06-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCrossQuotesByDateAndType($dateFrom, $dateTo, $type,$year='1',$renType)
{
   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$dateFrom))
   {
      $this->strERR .= GetErrorString("INVALID_DATEFROM_FIELD")."\n";
      return false;
   }

   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$dateTo))
   {
      $this->strERR .= GetErrorString("INVALID_DATETO_FIELD")."\n";
      return false;
   }

//   $this->lastSQLCMD = "SELECT count(*) as cnt FROM renewal WHERE update_date>='$dateFrom' AND update_date<='$dateTo'";// AND quote_user_id!='0'";
   $this->lastSQLCMD = "SELECT count(*) as cnt FROM cross_renewal r,qzquotes qzq WHERE r.update_date>='$dateFrom' AND r.update_date<='$dateTo' and r.qz_quote_id=qzq.id and qzq.quote_type_id='$type' and r.quote_type_id='$renType' and r.year='$year'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");

   return $cnt;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCrossAverageQuotesPerDay($year='1')
//
// [DESCRIPTION]:   
//
// [PARAMETERS]:    $year='1'
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-06-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCrossAverageQuotesPerDay($year='1')
{

   $this->lastSQLCMD = "SELECT min(cr_date) as date1,max(cr_date) as date2 from cross_renewal where year='$year'";// AND quote_user_id!='0'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   $date1 = $this->dbh->GetFieldValue("date1");
   $date2 = $this->dbh->GetFieldValue("date2");

   $this->lastSQLCMD = "SELECT count(*) as cnt FROM cross_renewal WHERE quote_user_id!='0'  and year='$year'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");   

   //$daysInterval = intval((strtotime($date2) - strtotime($date1)) / 86400);
   if ($date2 == $date1)
        $daysInterval = 1;
   else
        $daysInterval = intval((strtotime($date2) - strtotime($date1)) / 86400);

   $avgQuotesPerDay = number_format(($cnt/$daysInterval), 2, ".","");

   return $avgQuotesPerDay;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCrossAverageVisitsPerDay($year='1')
//
// [DESCRIPTION]:   
//
// [PARAMETERS]:    $year='1'
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-06-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCrossAverageVisitsPerDay($year='1')
{

   $this->lastSQLCMD = "SELECT min(cr_date) as date1,max(cr_date) as date2 from cross_renewal where year='$year'";// AND quote_user_id!='0'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   $date1 = $this->dbh->GetFieldValue("date1");
   $date2 = $this->dbh->GetFieldValue("date2");
   
   $this->lastSQLCMD = "SELECT count(*) as cnt FROM cross_renewal where year='$year'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");   

   //$daysInterval = intval((strtotime($date2) - strtotime($date1)) / 86400);
   if ($date2 == $date1)
        $daysInterval = 1;
   else
        $daysInterval = intval((strtotime($date2) - strtotime($date1)) / 86400);

   $avgQuotesPerDay = number_format(($cnt/$daysInterval), 2, ".","");

   return $avgQuotesPerDay;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckIfUserMadeCrossQuote($emStoreID, $date, $year='1')
//
// [DESCRIPTION]:   
//
// [PARAMETERS]:    $emStoreID, $date, $year='1'
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-06-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckIfUserMadeCrossQuote($emStoreID, $date, $year='1')
{
   if(! preg_match("/^\d+$/", $emStoreID))
   {
      $this->strERR = GetErrorString("INVALID_USERID_FIELD");
      return 0;
   }

   $this->lastSQLCMD = "SELECT count(*) as cnt FROM cross_renewal where em_store_id='$emStoreID' AND update_date>='$date' and year='$year'";

//   print $this->lastSQLCMD;
   
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");

   if ($cnt > 0)
      return true;

   return false;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Show the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

}//end class CRenewal

?>

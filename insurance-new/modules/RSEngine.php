<?php

/*****************************************************************************/
/*                                                                           */
/*  CRSEngine class interface                                                */
/*                                                                           */
/*  (C) 2004 Eugen Savin (seugen@abc.ro)                                     */
/*                                                                           */
/*****************************************************************************/

define("RSENGINE_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "Site.php";
include_once "Url.php";

// debug mode
if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CRSEngine
//
// [DESCRIPTION]:  CRSEngine class interface, remote site data exchange support,
//                 PHP version
//
// [FUNCTIONS]: bool InitSite();
//
//              bool   FetchSite();
//              string PrepareUrl($url = "");
//              bool   ProcessResponse();
//              bool   ProcessLastPage();
//              bool   CheckResponseErrors();
//
//              string TranslateUrl($url = "");
//              string GetUrl($url="", $method="GET", $params="", $referer="");
//              bool   ParseHtmlPage(&$resultArray);
//              bool   GetFormElements($formText, $formElement, &$resultArray);
//
//              void SetSite($siteName = "");
//              void SetProtocol($proto = "http");
//              void SetTimeout($timeout = 0);
//              void SetUrls($urls = array());
//              void SetUrlParams($params = array());
//              void SetFailureResponse($failureResp = "");
//              void SetRetryNumber($retryNumber = 0);
//              void SetRetryDelay($retryDelay = 0);
//              void SetCurlFollowLocation($followLocation = 1);
//
//              void SkipNextUrl();
//              void SkipNextUrls($urlsCounter = 0);
//              void ShiftUrlsRight();
//              void ReIndexUrls();
//              bool StringExists($string="", [$searchString=""]);
//              array|false StringMatch($regExp="", [$matchAll=true], [$textSource=""]);
//
//              void SetError($errMessage = "");
//              void AddError($errMessage = "");
//
//              striny GetSite();
//              string GetProtocol();
//              int    GetTimeout();
//              array  GetUrls();
//              array  GetUrlParams();
//              string GetFailureResponse();
//              int    GettRetryNumber();
//              int    GetRetryDelay();
//              int    GetCurlFollowLocation();
//
//              array  GetSiteConfig();
//              string GetHtmlContent();
//              string GetLastUrl();
//              string GetLastUrlID();
//
//              bool Close();
//              bool GetError();
//
// [CREATED BY]:   Eugen SAVIN (seugen@abc.ro) 2004-08-02
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CRSEngine
{
    // class-internal variables
    var $siteName;                // remote site name
    var $siteConfig;              // site related configurations
    var $httpHeader;              // keep the http header responses here
    var $proto;                   // the protocol we use: HTTP, HTTPS
    var $timeout;                 // timeout when downloading a web page in seconds
    var $cookie;                  // additional cookie variable
    var $cookieFileName;          // cookie file name

    var $htmlContent;             // last downloaded web page
    var $lastUrl;                 // last accessed url
    var $lastUrlID;               // last accessed urlID
    var $maxUrlsCounter;          // site max urls counter
    var $urlsCounter;             // site urls counter
    var $urls;                    // site urls to be fetched. array(urlID => urlAddress)
    var $initUrl;                 // site init to be getted -> from every quote at InitSite section - urlAddress
    var $urlParams;               // site url params to be send. array(urlID => array(name => value))
    var $urlFailureResponse;      // site url responses. array(urlID => array(FAILURE => value))
    var $skipUrlsCounter;         // mark that these urls/steps will be skipped within the FetchUrl() function
    var $curlFollowLocation;      // curl follow location setting

    var $retryNumber;           // number of retries if GetUrl() fails
    var $retryDelay;            // delay in seconds between retries

    var $dbh;                   // database handle
    var $closeDB;               // database close flag


    var $strERR;                // last error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CRSEngine
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    $siteName = "", $dbh = 0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-02
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CRSEngine($siteName = "")
{
   $this->siteName             = $siteName;

   $this->httpHeader           = "";
   $this->httpReferer          = "";
   $this->proto                = "http://";
   $this->timeout              = "90";

   $this->cookie               = "";
   $this->cookieFileName       = "";

   $this->htmlContent          = "";
   $this->lastUrl              = "";
   $this->lastUrlID            = 0;

   $this->retryNumber          = "3";
   $this->retryDelay           = "30";

   $this->urlsCounter          = 0;
   $this->maxUrlsCounter       = 100;
   $this->urls                 = array();
   $this->urlParams            = array();
   $this->urlFailureResponse   = "";
   $this->initUrl              = '';

   $this->skipUrlsCounter      = 0;
   $this->curlFollowLocation   = 1;

   $this->siteConfig           = array();
   $this->closeDB              = false;

   $this->strERR               = "";

   if(! $dbh)
   {
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
         $this->strERR = $this->dbh->GetError();
         return;
      }

      $this->closeDB = true;
   }
   else
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }

}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InitSite
//
// [DESCRIPTION]:   Initialise a remote site session
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InitSite()
{
   return false;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: FetchSite
//
// [DESCRIPTION]:   Fetch a remote site
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function FetchSite()
{
   if(! $this->FetchProcessInit())
   {
      $this->DeleteCookieFile();
      return false;
   }

   $this->urlsCounter    = count($this->urls);
   $this->maxUrlsCounter = $this->urlsCounter * ($this->retryNumber+1);

   for($i=1; $i<=$this->urlsCounter; $i++)
   {
      $this->lastUrlID = $urlID = $i;
      $urlAddress = $this->urls[$urlID];

      // skip this url
      if($this->skipUrlsCounter > 0)
      {
         $this->skipUrlsCounter--;
         continue;
      }

      // prepare URL
      if(($this->lastUrl = $this->PrepareUrl()))
         $urlAddress = $this->lastUrl;

      // if no params available we use the GET method
      if(! count($this->urlParams[$urlID]))
         $result = $this->GetUrl($this->siteName.$urlAddress);
      else if($this->urlParams[$urlID]["XML_DATA"])
         $result = $this->GetUrl($this->siteName.$urlAddress, "POST_XML", $this->urlParams[$urlID]["XML_DATA"]);
      else
         $result = $this->GetUrl($this->siteName.$urlAddress, "POST", $this->urlParams[$urlID]);

      // process url response
      if(! $this->FetchProcessResponse())
         break;

   }// for($i=1; $i<=$this->urlsCounter; $i++)

   $this->DeleteCookieFile();

   if(! $this->FetchProcessResult())
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: PrepareUrl
//
// [DESCRIPTION]:   Prepare a remote site config
//
// [PARAMETERS]:    $url = ""
//
// [RETURN VALUE]:  string, prepared url
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function PrepareUrl($url = "")
{
   return $url;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessResponse()
{
   return false;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcesslastPage
//
// [DESCRIPTION]:   Process and parse html contents after the last url was downloaded
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  quote
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessLastPage()
{
   return false;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckResponseErrors
//
// [DESCRIPTION]:   Check an html response page for errors. Extract errors.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckResponseErrors()
{
   return false;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: TranslateUrl
//
// [DESCRIPTION]:   Replace special chars with a percent (%) sign followed by
//                  two hex digits. See RFC 1738.
//
// [PARAMETERS]:    $url = ""
//
// [RETURN VALUE]:  encoded url string if success, "" otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function TranslateUrl($url = "")
{
   if(empty($url))
      return;

   $urlParts = explode('/', $url);

   for($i = 0; $i < count($urlParts); $i++)
     $urlParts[$i] = rawurlencode($urlParts[$i]);

   return implode('/', $urlParts);
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUrl
//
// [DESCRIPTION]:   Download/Open a remote web page
//
// [PARAMETERS]:    $url="", [method="GET"], [$params=""] [$referer=""]
//
// [RETURN VALUE]:  downloaded page text if success, "" otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUrl($url="", $method="GET", $params="", $referer="")
{
   $this->maxUrlsCounter--;

   if($this->maxUrlsCounter <= 0)
      return false;

   $this->htmlContent = "";

   if(empty($url))
   {
      $this->strERR = GetErrorString("INVALID_URL");
      return false;
   }

   // add protocol
   if(! preg_match("/:\/\//", $url))
      $url = $this->proto.$url;

   if(is_array($params))
   {
      $paramString = "";

      foreach($params as $key => $value)
      {
         //replace & char from value parameter
         $value =  str_replace("&","%26amp;",$value);

         $paramString .= $key . '=' . $value . '&';
      }

      $paramString = preg_replace("/\&$/","", $paramString);
      $params = $paramString;
   }

   //print "\nGetUrl(): $url\n";
   //print "GetUrl(): $params\n\n";

   // override referer
   if(! empty($referer))
      $this->httpReferer =  $referer;

   $ch = curl_init();

   if($method == "GET")
   {
      if(! empty($params))
      {
         if(preg_match("/\?/", $url))
            $url .= "&$params";
         else
            $url .= "/?".$params;
      }
   }
   elseif($method == "POST")
   {
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
   }
   elseif($method == "POST_XML")
   {
      $header[] = "Host: ".$this->siteName;
      $header[] = "MIME-Version: 1.0";
      $header[] = "Content-type: multipart/mixed";
      $header[] = "Accept: text/xml";
      $header[] = "Content-length: ".strlen($params);
      $header[] = "Cache-Control: no-cache";
      $header[] = "Connection: close \r\n";
      $header[] = $params;

//      curl_setopt($ch, CURLOPT_URL,$url);
//      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//      curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
   }

   if(empty($this->cookieFileName))
   {
      if(! $this->cookieFileName = $this->GenerateCookieFile())
      {
         $this->AddError(GetErrorString("CANNOT_GENERATE_COOKIE_FILENAME"));
         return false;
      }
   }

   $url = preg_replace("/\&$/","", $url);

   // override referer
   if(! empty($referer))
      $this->httpReferer =  $referer;

   curl_setopt($ch, CURLOPT_FORBID_REUSE, 0);
   curl_setopt($ch, CURLOPT_URL, $url);

   if(! empty($this->httpReferer))
      curl_setopt($ch, CURLOPT_REFERER, $this->httpReferer);

   curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $this->curlFollowLocation);
   curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);

   curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFileName);
   curl_setopt($ch, CURLOPT_COOKIEJAR,  $this->cookieFileName);

   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  0);
   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
   curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; X11; Linux i686) Opera 7.54  [en]');
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//   curl_setopt($ch, CURLOPT_VERBOSE, 1);

   // try to get the url more times in case of failure
   $retryDelay = $this->retryDelay;

   $try = 1;

   while($try <= $this->retryNumber)
   {
      $result = curl_exec($ch);

      //print "\n result $try :".$result;
      if(! empty($result))
         break;

      $this->LogMsg($this->siteName." => ".$url." ( retry $try )");

      sleep($retryDelay);

      $retryDelay += $this->retryDelay;

      $this->SetTimeout($this->timeout + $this->retryDelay);

      $try++;

   } // end while($try < $this->retryNumber)

   curl_close ($ch);

   $this->lastUrl     = $url;
   $this->httpReferer = $url;
   $this->htmlContent = $result;

   if(preg_match("/^(.*)\r?\n\r?\n/isU", $result, $matches))
      $this->httpHeader = $matches[1];

   //print $this->httpHeader;
   //print "\n html result:\n".$this->htmlContent;

   return $result;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GenerateCookieFile
//
// [DESCRIPTION]:   generate cookie unique filename
//
// [PARAMETERS]:    $cookieDirname = '../cookies/'
//
// [RETURN VALUE]:  $this->cookie|false
//
// [CREATED BY]:    jkl (gabi@acrux.biz) 2005-06-03
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GenerateCookieFile($cookieDirName = '../cookies/')
{
   // create unique cookie filename
   $file = new CFile();

   if(empty($this->cookieFileName))
      $this->cookieFileName = $file->GenerateFileName($cookieDirName, true);

   if(! empty($this->cookie))
   {
      $file->Open($cookieDirName.$this->cookieFileName, "a");
      $file->Write($this->cookie);
      $file->Close();

      $this->cookie = "";
   }

   return $cookieDirName . $this->cookieFileName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteCookieFile
//
// [DESCRIPTION]:   unlink cookie file already generated at the final process
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    jkl (gabi@acrux.biz) 2005-06-03
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteCookieFile()
{
   if(! empty($this->cookieFileName))
   {
      if(! unlink($this->cookieFileName))
      {
         $this->AddError(GetErrorString('CANT_UNLINK_COOKIE_FILE'));
         return false;
      }
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetCookie
//
// [DESCRIPTION]:   Sets additional cookie variable
//
// [PARAMETERS]:    $cookieName="", $cookieValue="", $cookiePath=""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    jkl (gabi@acrux.biz) 2005-06-03
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetCookie($cookieName="", $cookieValue="", $cookiePath="")
{
   $this->cookie .= $this->siteName."\tFALSE\t$cookiePath\tFALSE\t0\t$cookieName\t$cookieValue\n";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ParseHtmlPage
//
// [DESCRIPTION]:   Parse the results of a web page and create an associative
//                  array
//
//                  $resultArray["formName"]["formElement"]["elementType"] =
//                                                 array("key" => "value" ... );
//                  e.g.:
//                  $resultArray["mainForm"]["input"]["text"] = array("username" => "",
//                                                                    "password" => "");
//
// [PARAMETERS]:    &$resultArray
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-02
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ParseHtmlPage(&$resultArray)
{
   $pageText = $this->GetHtmlContent();

   // declare form elements we parse
   $formElements = array("input","button","select","textarea");

   $pagePos     = 0;
   $formName    = "_body_form";
   $formText    = "";
   $exitFlag = false;

   while(true)
   {
      //$findFormPos = stripos($pageText, "<form ", $pagePos);
      $findFormPos = strpos($pageText, "<form ", $pagePos);

      if($findFormPos === false)
      {
         $findFormPos = strlen($pageText);
         $exitFlag = true;
      }

      $formText = '<'.substr($pageText, $pagePos, $findFormPos);

      if(preg_match("/<form([^\>]+)/is", $formText, $matches))
      {
         if(preg_match("/name=(\"|\')(.*)(\"|\')/iU", $matches[1], $matches2nd))
         {
            $formName = $matches2nd[2];
//             print "form name: $formName\n";
         }

         unset($matches2nd);
         if(preg_match("/action=(\"|\')(.*)(\"|\')/iU", $matches[1], $matches2nd))
         {
            $formAction = $matches2nd[2];
            //print "form action: $formAction\n";
            $resultArray[$formName]['formAction'] = $formAction;
         }
      }

      //print "[###$formText###]";

      foreach($formElements as $formElement)
      {
        $this->GetFormElements($formText, $formElement, $resArray);
        $resultArray[$formName][$formElement] = $resArray;

        $resArray = array();
      }

      $pagePos = $findFormPos + 1;

      if($exitFlag)
         break;

   } // end of while()
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFormElements
//
// [DESCRIPTION]:   Parse the HTML text and extract all the form elements
//                  and create an associative array
//
//                  e.g.:
//                  $resultArray["elementType"] = array("username" => "",
//                                                      "password" => "");
//
// [PARAMETERS]:    $formText, $formElement, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-02
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFormElements($formText, $formElement, &$resultArray)
{
   if( empty($formText))
      return false;

   if( empty($formElement))
      return false;

   if(($formElement == "input") || ($formElement == "button"))
   {
      preg_match_all("/<\s*$formElement([^\>]+)/is", $formText, $matches);

      for($i=0; $i<count($matches[0]); $i++)
      {

         $name  = "";
         $value = "";
         $type  = "";

         if(! preg_match("/\sname=(\"|\')(.*)(\"|\')/iU", $matches[1][$i], $matches2nd))
            continue;

            $name = $matches2nd[2];
//             print "name: $name\n";

         if(! preg_match("/\svalue=(\"|\')(.*)(\"|\')/iU", $matches[1][$i], $matches2nd))
            continue;

            $value = $matches2nd[2];
//             print "value: $value\n";

         if(! preg_match("/\stype=(\"|\')(.*)(\"|\')/iU", $matches[1][$i], $matches2nd))
            continue;

            $type = $matches2nd[2];
//             print "type: $type\n";

         $resultArray[$type][$name] = $value;
      } // end for($i=0; $i<count($matches[0]); $i++)
   } // end if(($formElement == "input") || ($formElement == "button"))

   if($formElement == "select")
   {
      preg_match_all("/\<\s*$formElement(.*)\<\s*\/$formElement/isU", $formText, $matches);

      //preg_match_all("/(<([\w]+)[^>]*>)(.*)(<\/\\2>)/i", $formText, $matches);

      for($i=0; $i<count($matches[0]); $i++)
      {
         //print "1: [[".$matches[1][$i]."]]\n";
         //print "2: [[".$matches[2][$i]."]]\n";
         //print "3: [[".$matches[3][$i]."]]\n";
         //print "4: [[".$matches[4][$i]."]]\n";
         //print "5: [[".$matches[5][$i]."]]\n";
         //continue;

         if(preg_match("/\sname=(\"|\')(.*)(\"|\')/isU", $matches[1][$i], $matches2nd))
            $name = $matches2nd[2];
         else
            continue;

         //print "xname: $name\n";

         // get each options element
         preg_match_all("/<\s*option(.*)\>(.*)\</isU", $matches[1][$i], $matchesOption);
         $selectedKey = "";

         for($j=0; $j<count($matchesOption[0]); $j++)
         {
            if($j == 0)
            {
               if(preg_match("/\svalue=(\"|\')(.*)(\"|\')/iU", $matchesOption[1][$j], $matches2nd))
                  $selected = $matches2nd[2];
            }

            if(preg_match("/selected/i", $matchesOption[1][$j]))
            {
               if(preg_match("/\svalue=(\"|\')(.*)(\"|\')/iU", $matchesOption[1][$j], $matches2nd))
                  $selected = $matches2nd[2];
               else
                  $selected = trim($matchesOption[2][$j]);

               break;
            }
         } // end for($j=0; $j<count($matchesOption[0]); $j++)

         $resultArray["select"][$name] = $selected;
      } // end for($i=0; $i<count($matches[0]); $i++)
   } // end if($formElement == "select")

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetSite
//
// [DESCRIPTION]:   Set the web site name
//
// [PARAMETERS]:    $siteName=""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetSite($siteName = "")
{
   $this->siteName = $siteName;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetProtocol
//
// [DESCRIPTION]:   Set the protocol we use with the remote sites
//
// [PARAMETERS]:    $proto = "http"
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetProtocol($proto = "http")
{
   $this->proto = "$proto://";
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetTimeout
//
// [DESCRIPTION]:   Set the timeout value when downloading a web page
//
// [PARAMETERS]:    $timeout = 0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetTimeout($timeout = 0)
{
   $this->timeout = $timeout;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetUrls
//
// [DESCRIPTION]:   Set the web site's URL array. All pages listed within this array
//                  will be downloaded in the order specified in the array.
//
// [PARAMETERS]:    $urls = array()
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetUrls($urls = array())
{
   if(is_array($urls))
      $this->urls = $urls;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetInitUrl
//
// [DESCRIPTION]:   Set the url from init secttion of every quote
//
// [PARAMETERS]:    $initUrl = '';
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-22
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetInitUrl($initUrl = '')
{
   if(! empty($initUrl))
      $this->initUrl = $initUrl;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetUrlParams
//
// [DESCRIPTION]:   Set the web site's params for each ot the URLs listed in
//                  the URL array
//
// [PARAMETERS]:    $params = array()
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetUrlParams($params = array())
{
   if(is_array($params))
      $this->urlParams = $params;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetFailureResponse
//
// [DESCRIPTION]:   Set the failure response string value. If this string
//                  is found, after a web page is downloaded,
//                  an error extractor should extract all the errors from
//                  the web page.
//
// [PARAMETERS]:    $failureResponse = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetFailureResponse($failureResponse = "")
{
   $this->urlFailureResponse = $failureResponse;
   $this->urlFailureResponse = preg_replace("/\//","\\/", $this->urlFailureResponse);
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetRetryNumber
//
// [DESCRIPTION]:   Set the retry number for GetUrl() in case of failure of this
//                  function
//
// [PARAMETERS]:    $retryNumber = 0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetRetryNumber($retryNumber = 0)
{
   if(! $retryNumber)
      return;

   $this->retryNumber = $retryNumber;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetRetryDelay
//
// [DESCRIPTION]:   Set the delay between the retries for GetUrl()
//                  in case of failure of this function
//
// [PARAMETERS]:    $retryDelay = 0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetRetryDelay($retryDelay = 0)
{
   if(! $retryDelay)
      return;

   $this->retryDelay = $retryDelay;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetCurlFollowLocation
//
// [DESCRIPTION]:   Set the follow location setting for GetUrl()
//
// [PARAMETERS]:    $followLocation = 1
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetCurlFollowLocation($followLocation = 1)
{
   $this->curlFollowLocation = $followLocation;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SkipNextUrl
//
// [DESCRIPTION]:   Skip the next url in FetchSite() function
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SkipNextUrl()
{
   $this->skipUrlsCounter = 1;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SkipNextUrls
//
// [DESCRIPTION]:   Skip the next urls in FetchSite() function
//
// [PARAMETERS]:    $urlsCounter = 0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SkipNextUrls($urlsCounter = 0)
{
   if(! $urlsCounter)
      return;

   $this->skipUrlsCounter = $urlsCounter;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShiftUrlsRight
//
// [DESCRIPTION]:   Shifts the URLs right urls step 2 becomes url step 1
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShiftUrlsRight()
{
   $urls      = $this->urls;
   $urlParams = $this->urlParams;

   $this->urls      = array();
   $this->urlParams = array();

   foreach($urls as $key => $value)
   {
      $this->urls[$key + 1] = $urls[$key];
      $this->urlParams[$key + 1] = $urlParams[$key];
   }

   $this->urlsCounter++;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ReIndexUrls()
//
// [DESCRIPTION]:   Re-indexes the URL steps. For example: if you have
//                  the steps 1,3 and 7, this function re-arranges the
//                  steps in sorted order: 1,2,3
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ReIndexUrls()
{
   $urls      = $this->urls;
   $urlParams = $this->urlParams;

   $this->urls      = array();
   $this->urlParams = array();

   $urlsCounter = 0;
   foreach($urls as $key => $value)
   {
      $urlsCounter++;

      $this->urls[$urlsCounter]      = $value;
      $this->urlParams[$urlsCounter] = $urlParams[$key];
   }

   $this->urlsCounter = $urlsCounter;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StringExists
//
// [DESCRIPTION]:   Search for a string within a text
//
// [PARAMETERS]:    $searchString="", [$textSource=""]
//
// [RETURN VALUE]:  true if the search string was found, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StringExists($searchString="", $textSource="")
{
   if(empty($searchString))
      return false;

   if(empty($textSource))
      $textSource = $this->htmlContent;

   if(empty($textSource))
      return false;

   // repalce " with \"
   //$searchString = str_replace("\"", "\\\"", $searchString);

   if(! preg_match("/$searchString/", $textSource))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StringExists
//
// [DESCRIPTION]:   Search for a string within a text
//
// [PARAMETERS]:    $regExp="", [$matchAll=true], [$textSource=""]
//
// [RETURN VALUE]:  array containing the matches if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StringMatch($regExp="", $matchAll=true, $textSource="")
{
   if(empty($regExp))
      return false;

   if($matchAll)
      $textSource = $this->htmlContent;

   if(empty($textSource))
      return false;

   if(empty($textSource))
      $textSource = $this->htmlContent;

   if(empty($textSource))
      return false;

   // repalce " with \"
   //$regExp = str_replace("\"", "\\\"", $regExp);

   if($matchAll)
   {
      if(preg_match_all("/$regExp/", $textSource, $matches))
         return $matches;
   }
   else
   {
      if(preg_match("/$regExp/", $textSource, $matches))
         return $matches;
   }

   return false;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetError
//
// [DESCRIPTION]:   Set the error string
//
// [PARAMETERS]:    $errMesage = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetError($errMessage = "")
{
   if(! empty($errMessage))
      $this->strERR = $errMessage;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddError
//
// [DESCRIPTION]:   Append an error
//
// [PARAMETERS]:    $errMesage = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddError($errMessage = "")
{
   if(! empty($errMessage))
      $this->strERR .= $errMessage."\n";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSite
//
// [DESCRIPTION]:   Get the site name
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the site name as string
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSite()
{
   return $this->siteName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetProtocol
//
// [DESCRIPTION]:   Get the protocol
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the protocol as string
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetProtocol()
{
   $proto = substr($this->proto, 0, strlen($this->proto)-3);
   return $proto;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTimeout
//
// [DESCRIPTION]:   Get the timeout for page download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the timeout as integer
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTimeout()
{
   return $this->timeout;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUrls
//
// [DESCRIPTION]:   Get the URLs we download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the URLs as array
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUrls()
{
   return $this->urls;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUrlParams
//
// [DESCRIPTION]:   Get the params associated with the site's URLS
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the params as array
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUrlParams()
{
   return $this->urlParams;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFailureResponse
//
// [DESCRIPTION]:   Get the failure response
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the failure response as string
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFailureResponse()
{
   return $this->urlFailureResponse;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRetryNumber
//
// [DESCRIPTION]:   Get the retry number
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the retry number as integer
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetRetryNumber()
{
   return $this->retryNumber;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRetryDelay
//
// [DESCRIPTION]:   Get the retry delay between the retries
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the retry delay as integer
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetRetryDelay()
{
   return $this->retryDelay;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCurlFollowLocation
//
// [DESCRIPTION]:   Get the curl follow location setting
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the follow location setting as integer
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCurlFollowLocation()
{
   return $this->curlFollowLocation;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSiteConfig
//
// [DESCRIPTION]:   Get the site config array. Some internal settings are
//                  used/modified while crowling the website
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the site config as array
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSiteConfig()
{
   return $this->siteConfig;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetHtmlContent
//
// [DESCRIPTION]:   Get the HTML content after a page is downloaded
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the HTML contents as string
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetHtmlContent()
{
   return $this->htmlContent;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLastUrl
//
// [DESCRIPTION]:   Get the last accessed URL
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the URL as string
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLastUrl()
{
   return $this->lastUrl;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLastUrlID
//
// [DESCRIPTION]:   Get the last URL ID associated with the last download URL
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the URL ID as integer
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLastUrlID()
{
   return $this->lastUrlID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-02
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-05-28
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    $counter = 0;
    $errors  = explode("\n", $this->strERR);

    $this->strERR = "";

    foreach($errors as $error)
    {
      if(empty($error))
         continue;

      $counter++;
      $this->strERR .= "$counter. $error<br>\n";
    }

    return $this->strERR;
}

} // end of CRSEngine class

?>

<?php

/*****************************************************************************/
/*                                                                           */
/*  CQuoteFilter class interface                                                */
/*                                                                           */
/*  (C) 2008 Istvancsek Gabriel (gabi@acrux.biz)                             */
/*                                                                           */
/*****************************************************************************/

include_once "errors.inc";
include_once "MySQL.php";

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuoteFilter
//
// [DESCRIPTION]:  CQuoteFilter class interface
//
// [FUNCTIONS]:      CQuoteFilter($dbh=0)
//                   AssertQuoteFilter($logID, $siteID, &$quoteReference, &$quotePassword, &$insurer, &$annualPremium, &$monthlyPremium, &$voluntaryExcess, &$voluntaryExcess2nd, $compulsoryExcess, &$compulsoryExcess2nd)
//                   AddQuoteFilter($logID, $siteID, $quoteReference, $quotePassword, $insurer, $annualPremium, $monthlyPremium=0, $voluntaryExcess=0, $voluntaryExcess2nd=0, $compulsoryExcess=0, $compulsoryExcess2nd=0)
//                   UpdateQuoteFilterPosition($logID, $siteID, $insurer, $annualPremium, $position)
//                   DeleteQuoteFilter($quoteFilterID)
//                   GetQuoteFilter($quoteFilterID)
//                   Close()
//                   GetError()
//                   ShowError()
//
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CQuoteFilter
{

    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string
   

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuoteFilter
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function CQuoteFilter($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuoteFilter
//
// [DESCRIPTION]:   assert data fields
//
// [PARAMETERS]:    $logID, $siteID, &$quoteReference, &$quotePassword, &$insurer, &$annualPremium, &$monthlyPremium, &$voluntaryExcess, &$voluntaryExcess2nd, $compulsoryExcess, &$compulsoryExcess2nd
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function AssertQuoteFilter($logID, $siteID, &$quoteReference, &$quotePassword, &$insurer, &$annualPremium, &$monthlyPremium, &$voluntaryExcess, &$voluntaryExcess2nd, $compulsoryExcess, &$compulsoryExcess2nd)
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = "\n".GetErrorString("INVALID_LOG_ID_FIELD");
   }

   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR .= "\n".GetErrorString("INVALID_SITE_ID_FIELD");
   }

   if(empty($quoteReference))
      $quoteReference = '-';

   if(empty($quotePassword))
      $quotePassword = '-';
      
   $insurer = str_replace("'", "", trim($insurer));

   if(empty($insurer))
      $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_DETAILS_INSURER_FIELD");

   $annualPremium = str_replace(",", "", trim($annualPremium));

   if(! preg_match("/([0-9\.]+)/", $annualPremium, $matchesAnnualPremium))
      $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_DETAILS_ANNUAL_PREMIUM_FIELD");

   $annualPremium = $matchesAnnualPremium[1];

   $monthlyPremium = str_replace(",", "", trim($monthlyPremium));

   if(! empty($monthlyPremium))
   {
      if(! preg_match("/([0-9\.]+)/", $monthlyPremium, $matchesMonthlyPremium))
         $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_DETAILS_MONTHLY_PREMIUM_FIELD");

      $monthlyPremium = $matchesMonthlyPremium[1];
   }

   $voluntaryExcess     = str_replace(",", "", trim($voluntaryExcess));
   $voluntaryExcess2nd  = str_replace(",", "", trim($voluntaryExcess2nd));
   $compulsoryExcess    = str_replace(",", "", trim($compulsoryExcess));
   $compulsoryExcess2nd = str_replace(",", "", trim($compulsoryExcess2nd));

   if(! preg_match("/([0-9\.\-]+)/", $voluntaryExcess, $matchesVoluntaryExcess))
      $voluntaryExcess = 0;

   $voluntaryExcess = $matchesVoluntaryExcess[1];

   if(! preg_match("/([0-9\.\-]+)/", $voluntaryExcess2nd, $matchesVoluntaryExcess2nd))
      $voluntaryExcess2nd = 0;

   $voluntaryExcess2nd = $matchesVoluntaryExcess2nd[1];

   if(! preg_match("/([0-9\.\-]+)/", $compulsoryExcess, $matchesCompulsoryExcess))
      $compulsoryExcess = 0;

   $compulsoryExcess = $matchesCompulsoryExcess[1];


   if(! preg_match("/([0-9\.\-]+)/", $compulsoryExcess2nd, $matchesCompulsoryExcess2nd))
      $compulsoryExcess2nd = 0;

   $compulsoryExcess2nd = $matchesCompulsoryExcess2nd[1];

   // prepare float values to compare with values from db
   $annualPremium      = number_format($annualPremium, 2, '.', '');
   $monthlyPremium     = number_format($monthlyPremium, 2, '.', '');
   $voluntaryExcess    = number_format($voluntaryExcess, 2, '.', '');
   $voluntaryExcess2nd = number_format($voluntaryExcess2nd, 2, '.', '');
   $compulsoryExcess   = number_format($compulsoryExcess, 2, '.', '');

   if(! preg_match("/[0-9\.\-]+/", $voluntaryExcess))
      $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_DETAILS_VOLUNTARY_EXCESS_FIELD");

   if(! preg_match("/[0-9\.\-]+/", $voluntaryExcess2nd))
      $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_DETAILS_VOLUNTARY_EXCESS_SEC_FIELD");

   if(! preg_match("/[0-9\.\-]+/", $compulsoryExcess))
      $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_DETAILS_COMPULSORY_EXCESS_FIELD");

   if($annualPremium <= 0)
      $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_DETAILS_ANNUAL_PREMIUM_FIELD_IS_NULL");

   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteFilter
//
// [DESCRIPTION]:   add data into user rates
//
// [PARAMETERS]:    $siteID, $filteredQuotes, $totalQuotes, $top
//
// [RETURN VALUE]:  int | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2010-06-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteFilter($logID, $siteID, $quoteReference, $quotePassword, $insurer, $annualPremium, $monthlyPremium=0, $voluntaryExcess=0, $voluntaryExcess2nd=0, $compulsoryExcess=0, $compulsoryExcess2nd=0)
{
   if(! $this->AssertQuoteFilter($logID, $siteID, $quoteReference, $quotePassword, $insurer, $annualPremium, $monthlyPremium, $voluntaryExcess, $voluntaryExcess2nd, $compulsoryExcess, $compulsoryExcess2nd))
      return false;
   
   $sqlCmd = "SELECT id FROM ".SQL_QUOTE_FILTER." WHERE log_id='$logID' AND site_id='$siteID' AND insurer='$insurer' 
   AND annual_premium LIKE '$annualPremium' AND monthly_premium LIKE '$monthlyPremium' AND voluntary_excess LIKE
   '$voluntaryExcess' AND voluntary_excess_sec='$voluntaryExcess2nd' and compulsory_excess='$compulsoryExcess' 
   and compulsory_excess_sec='$compulsoryExcessS2nd' ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if($this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITE_FILTER_FOUND");
      return false;
   }

   $sqlCmd = " INSERT INTO ".SQL_QUOTE_FILTER." (log_id, site_id, quote_reference, quote_password, insurer, annual_premium, monthly_premium, voluntary_excess, voluntary_excess_sec, compulsory_excess, compulsory_excess_sec) VALUES('$logID', '$siteID', '$quoteReference', '$quotePassword', '$insurer', '$annualPremium', '$monthlyPremium', '$voluntaryExcess', '$voluntaryExcess2nd', '$compulsoryExcess', '$compulsoryExcess2nd') ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteFilter
//
// [DESCRIPTION]:   add data into user rates
//
// [PARAMETERS]:    $siteID, $filteredQuotes, $totalQuotes, $top
//
// [RETURN VALUE]:  int | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2010-06-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQuoteFilterPosition($quoteFilterID, $position)
{
   if(! preg_match("/^\d+$/", $quoteFilterID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_FILTER_ID_FIELD");
      return false;
   }

   $sqlCmd = " UPDATE ".SQL_QUOTE_FILTER." SET position='$position' WHERE id='$quoteFilterID' ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQuoteFilter
//
// [DESCRIPTION]:   delete entry from USER_SYSTEMs by user rate id
//
// [PARAMETERS]:    $siteFilterID = ""
//
// [RETURN VALUE]:  false|true
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function DeleteQuoteFilter($quoteFilterID)
{
   if(! preg_match("/^\d+$/", $quoteFilterID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_FILTER_ID_FIELD");
      return false;
   }

   $sqlCmd = " DELETE FROM ".SQL_QUOTE_FILTER." WHERE id='$quoteFilterID' ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteFilter
//
// [DESCRIPTION]:   Read data from USER_SYSTEMs table and put it into an array variable
//
// [PARAMETERS]:    $siteFilterID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteFilter($quoteFilterID)
{
   if(! preg_match("/^\d+$/", $quoteFilterID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_FILTER_ID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT * FROM ".SQL_QUOTE_FILTER." WHERE id='$quoteFilterID' ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITE_FILTER_ID_NOT_FOUND");
      return false;
   }
  
   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("USER_SYSTEM_ID_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]                    = $this->dbh->GetFieldValue("id");
   $arrayResult["log_id"]                = $this->dbh->GetFieldValue("log_id");
   $arrayResult["site_id"]               = $this->dbh->GetFieldValue("site_id");
   $arrayResult["quote_reference"]       = $this->dbh->GetFieldValue("quote_reference");
   $arrayResult["quote_password"]        = $this->dbh->GetFieldValue("quote_password");
   $arrayResult["insurer"]               = $this->dbh->GetFieldValue("insurer");
   $arrayResult["annual_premium"]        = $this->dbh->GetFieldValue("annual_premium");
   $arrayResult["monthly_premium"]       = $this->dbh->GetFieldValue("monthly_premium");
   $arrayResult["voluntary_excess"]      = $this->dbh->GetFieldValue("voluntary_excess");
   $arrayResult["voluntary_excess_sec"]  = $this->dbh->GetFieldValue("voluntary_excess_sec");
   $arrayResult["compulsory_excess"]     = $this->dbh->GetFieldValue("compulsory_excess");
   $arrayResult["compulsory_excess_sec"] = $this->dbh->GetFieldValue("compulsory_excess_sec");
   $arrayResult["position"]              = $this->dbh->GetFieldValue("position");
   
   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteFilter
//
// [DESCRIPTION]:   Read data from USER_SYSTEMs table and put it into an array variable
//
// [PARAMETERS]:    $siteFilterID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuoteFilterByUser($quoteUserID)
{
   if(! preg_match("/^\d+$/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_USER_ID_FIELD");
      return false;
   }
   
   $date = date('Y-m-d', mktime() - 30 * 86400);

   $sqlCmd = "SELECT sf.* FROM ".SQL_QUOTE_FILTER." qf LEFT JOIN ".SQL_SITE_FILTERS." sf ON (sf.site_id=qf.site_id)  LEFT JOIN ".SQL_LOGS." l ON (l.id=qf.log_id) WHERE l.quote_user_id='$quoteUserID' AND sf.site_id is NOT NULL AND l.date >= '$date' AND sf.status='ON' GROUP BY qf.site_id ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITE_FILTER_ID_NOT_FOUND");
      return false;
   }
  
   $arrayResult = array();
   
   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("site_id")]['filtered_quotes'] = $this->dbh->GetFieldValue("filtered_quotes");
      $arrayResult[$this->dbh->GetFieldValue("site_id")]['total_quotes']    = $this->dbh->GetFieldValue("total_quotes");
      $arrayResult[$this->dbh->GetFieldValue("site_id")]['top']             = $this->dbh->GetFieldValue("top");
   }
   
   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteFilter
//
// [DESCRIPTION]:   Read data from USER_SYSTEMs table and put it into an array variable
//
// [PARAMETERS]:    $siteFilterID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuotesByLogID($logID)
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");
      return false;
   }

   $sqlCmd = " select qs.id as quote_status_id,qf.id as quote_filter_id, qs.site_id, if(qd.annual_premium is not NULL, qd.annual_premium,qf.annual_premium) as annual_premium, if(qd.annual_premium is not NULL, 0,1) as filtered from ".SQL_QUOTE_STATUS." qs LEFT JOIN ".SQL_QUOTES." q ON (qs.id=q.quote_status_id) LEFT JOIN ".SQL_QUOTE_DETAILS." qd ON (q.id=qd.quote_id) LEFT JOIN  ".SQL_QUOTE_FILTER." qf ON (qf.log_id=qs.log_id AND qf.site_id=qs.site_id) WHERE qs.log_id=$logID having annual_premium is not null ORDER BY annual_premium asc ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTES_NOT_FOUND");
      return false;
   }
  
   $arrayResult = array();

   $counter = 0;
   while($this->dbh->MoveNext())
   {
      $counter++;
      $arrayResult[$counter]['quote_status_id'] = $this->dbh->GetFieldValue("quote_status_id");
      
      if($this->dbh->GetFieldValue("filtered"))
         $arrayResult[$counter]['quote_filter_id'] = $this->dbh->GetFieldValue("quote_filter_id");
         
      $arrayResult[$counter]['site_id']         = $this->dbh->GetFieldValue("site_id");
      $arrayResult[$counter]['annual_premium']  = $this->dbh->GetFieldValue("annual_premium");
      $arrayResult[$counter]['filtered']        = $this->dbh->GetFieldValue("filtered");
      $arrayResult[$counter]['initial_top']     = $counter;
   }
   
   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

}

?>

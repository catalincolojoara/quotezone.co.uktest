<?php
/*****************************************************************************/
/*                                                                           */
/*  CEsvTracker class interface                                       		  */
/*                                                                           */
/*  (C) 2010 Goia Calin (calin@acrux.biz)                                   */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";


//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CEsvTracker
//
// [DESCRIPTION]:  CEsvTracker class interface
//
// [FUNCTIONS]:    int   AddAdKey($adKey='', $crDate='')
//                 int   AddAdKeyDetails($adKeyID=0, $searchEngine='', $market='', $campaign='', $adgroup='', $term='', $match='',// 										 $crDate='')
//                 int   AddAdKeyQzQuote($qzQuoteID=0, $adKeyID=0, $crDate='')
//                 int   DeleteAdKey($adKey='')
//                 bool  DeleteAdKeyDetails($adKeyID=0)
//                 bool  DeleteAdKeyQzQuote($adKeyID=0)
//                 array GetAdKeyByKey($adKey='')
//                 array GetAdKeyDetailsByAdKeyID($adKeyID='')
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Goia Calin (calin@acrux.biz) 19-05-2010
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CEsvTracker
{
// database handler
var $dbh;         // database server handle
var $closeDB;     // close database flag

// class-internal variables
var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CEsvTracker
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 19-05-2010
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CEsvTracker($dbh=0)
{
	if($dbh)
	{
		$this->dbh = $dbh;
		$this->closeDB = false;
	}
	else
	{
		// default configuration
		$this->dbh = new CMySQL();

		if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
		{
			$this->strERR = $this->dbh->GetError();
			return;
		}

		$this->closeDB = true;
	}

	$this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddAdKey
//
// [DESCRIPTION]:   Add new entry to the esv_adkeys table
//
// [PARAMETERS]:    $adKey='', $crDate=''
//
// [RETURN VALUE]:  ID or 0 in case of failure
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 20-05-2010
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddAdKey($adKey='', $crDate='')
{
	if(empty($crDate))
	   $crDate = date('Y-m-d H:i:s');
	
	$sqlCMD = "INSERT INTO ".SQL_ESV_ADKEYS." (adkey, cr_date) VALUES ('$adKey', '$crDate')";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	$sqlCMD = "SELECT LAST_INSERT_ID() AS id";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	if(! $this->dbh->FetchRows())
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	return $this->dbh->GetFieldValue("id");

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddAdKeyDetails
//
// [DESCRIPTION]:   Add new entry to the esv_adkeys_details table
//
// [PARAMETERS]:    $adKeyID=0, $searchEngine='', $market='', $campaign='', $adgroup='', $term='', $match='', $crDate=''
//
// [RETURN VALUE]:  ID or 0 in case of failure
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 20-05-2010
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddAdKeyDetails($adKeyID=0, $searchEngine='', $market='', $campaign='', $adgroup='', $term='', $match='', $crDate='')
{
	if(! preg_match("/^\d+$/", $adKeyID))
    {
       $this->strERR = GetErrorString("INVALID_ADKEYID_FIELD");
       return false;
    }
	
	if(empty($crDate))
	   $crDate = date('Y-m-d H:i:s');
	
	$sqlCMD = "INSERT INTO ".SQL_ESV_ADKEYS_DETAILS." (`esv_adkey_id`, `search_engine`, `market`, `campaign`, `adgroup`, `term`, `match`, `cr_date`) VALUES ('$adKeyID', '$searchEngine', '$market', '$campaign', '$adgroup', '$term', '$match', '$crDate')";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	$sqlCMD = "SELECT LAST_INSERT_ID() AS id";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	if(! $this->dbh->FetchRows())
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	return $this->dbh->GetFieldValue("id");

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddAdKeyQzQuote
//
// [DESCRIPTION]:   Add new entry to the qzquote_esv table
//
// [PARAMETERS]:    $qzQuoteID=0, $adKeyID=0, $crDate=''
//
// [RETURN VALUE]:  ID or 0 in case of failure
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 20-05-2010
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddAdKeyQzQuote($qzQuoteID=0, $adKeyID=0, $crDate='')
{
	if(! preg_match("/^\d+$/", $qzQuoteID))
    {
       $this->strERR = GetErrorString("INVALID_QZ_QUOTE_ID_FIELD");
       return false;
    }
	
	if(! preg_match("/^\d+$/", $adKeyID))
    {
       $this->strERR = GetErrorString("INVALID_ADKEYID_FIELD");
       return false;
    }
	
	if(empty($crDate))
	   $crDate = date('Y-m-d H:i:s');
	
	$sqlCMD = "INSERT INTO ".SQL_QZQUOTE_ESV." (qz_quote_id, esv_adkey_id, cr_date) VALUES ('$qzQuoteID', '$adKeyID', '$crDate')";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	$sqlCMD = "SELECT LAST_INSERT_ID() AS id";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	if(! $this->dbh->FetchRows())
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	return $this->dbh->GetFieldValue("id");

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteAdKey
//
// [DESCRIPTION]:   Delete an entry from esv_adkeys table
//
// [PARAMETERS]:    $adKey=''
//
// [RETURN VALUE]:   ID or 0 in case of failure
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 20-05-2010
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteAdKey($adKey='')
{
   // check if key exists
   $sqlCMD = "SELECT id FROM ".SQL_ESV_ADKEYS." WHERE adkey='$adKey'";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("ADKEY_ID_NOT_FOUND");
      return false;
   }
   
   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
   
   $adKeyIDToBeDeleted = $this->dbh->GetFieldValue("id");

   $sqlCMD = "DELETE FROM ".SQL_ESV_ADKEYS." WHERE id='$adKeyIDToBeDeleted'";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return $adKeyIDToBeDeleted;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteAdKeyDetails
//
// [DESCRIPTION]:   Delete an entry from esv_adkeys_details table
//
// [PARAMETERS]:    $adKeyID=0
//
// [RETURN VALUE]:   true | false
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 20-05-2010
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteAdKeyDetails($adKeyID=0)
{
   if(! preg_match("/^\d+$/", $adKeyID))
   {
      $this->strERR = GetErrorString("INVALID_ADKEYID_FIELD");
      return false;
   }

   // check if adKeyID exists
   $sqlCMD = "SELECT id FROM ".SQL_ESV_ADKEYS_DETAILS." WHERE esv_adkey_id='$adKeyID'";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("ADKEY_ID_NOT_FOUND");
      return false;
   }

   $sqlCMD = "DELETE FROM ".SQL_ESV_ADKEYS_DETAILS." WHERE esv_adkey_id='$adKeyID'";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteAdKeyQzQuote
//
// [DESCRIPTION]:   Delete an entry from qzquote_esv table
//
// [PARAMETERS]:    $adKeyID=0
//
// [RETURN VALUE]:   true | false
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 20-05-2010
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteAdKeyQzQuote($adKeyID=0)
{
   if(! preg_match("/^\d+$/", $adKeyID))
   {
      $this->strERR = GetErrorString("INVALID_ADKEYID_FIELD");
      return false;
   }

   // check if adKeyID exists
   $sqlCMD = "SELECT id FROM ".SQL_QZQUOTE_ESV." WHERE esv_adkey_id='$adKeyID'";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("ADKEY_ID_NOT_FOUND");
      return false;
   }

   $sqlCMD = "DELETE FROM ".SQL_QZQUOTE_ESV." WHERE esv_adkey_id='$adKeyID'";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAdKeyByKey
//
// [DESCRIPTION]:   Read data from esv_adkeys tables and put it into an array variable
//
// [PARAMETERS]:    $adKey=''
//
// [RETURN VALUE]:  Array(with the parameters that belong to the adKey) or false in case of failure
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 20-05-2010
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAdKeyByKey($adKey='')
{
   $sqlCMD = "SELECT * FROM ".SQL_ESV_ADKEYS." WHERE adkey='$adKey'";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("ADKEY_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]               = $this->dbh->GetFieldValue("id");
   $arrayResult["adkey"]            = $this->dbh->GetFieldValue("adkey");
   $arrayResult["cr_date"]          = $this->dbh->GetFieldValue("cr_date");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAdKeyDetailsByAdKeyID
//
// [DESCRIPTION]:   Read data from esv_adkeys_details table and put it into an array variable
//
// [PARAMETERS]:    $adKeyID=0
//
// [RETURN VALUE]:  Array(with the parameters that belong to the adKeyID) or false in case of failure
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 20-05-2010
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAdKeyDetailsByAdKeyID($adKeyID=0)
{
   if(! preg_match("/^\d+$/", $adKeyID))
   {
      $this->strERR = GetErrorString("INVALID_ADKEYID_FIELD");
      return false;
   }
	
   $sqlCMD = "SELECT * FROM ".SQL_ESV_ADKEYS_DETAILS." WHERE esv_adkey_id='$adKeyID'";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("ADKEY_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["adKeyDetailsID"]   = $this->dbh->GetFieldValue("id");
   $arrayResult["adKeyID"]          = $this->dbh->GetFieldValue("esv_adkey_id");
   $arrayResult["searchEngine"]     = $this->dbh->GetFieldValue("search_engine");
   $arrayResult["market"]           = $this->dbh->GetFieldValue("market");
   $arrayResult["campaign"]         = $this->dbh->GetFieldValue("campaign");
   $arrayResult["adgroup"]          = $this->dbh->GetFieldValue("adgroup");
   $arrayResult["term"]             = $this->dbh->GetFieldValue("term");
   $arrayResult["match"]            = $this->dbh->GetFieldValue("match");
   $arrayResult["cr_date"]          = $this->dbh->GetFieldValue("cr_date");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 20-05-2010
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 20-05-2010
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

}

?>

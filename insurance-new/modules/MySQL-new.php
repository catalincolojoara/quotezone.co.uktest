<?php
/*****************************************************************************/
/*                                                                           */
/*  MySQL Function Wrapper Class                                             */
/*                                                                           */
/*  (C) 2003 Eugen Savin (seugen@abc.ro)                                     */
/*                                                                           */
/*****************************************************************************/

define("MYSQL_INCLUDED", "1");
define("MYSQL_DEBUG", "0");

if(DEBUG_MODE)
  error_reporting(1);
else
  error_reporting(0);


//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CMySQL
//
// [DESCRIPTION]:  Database interface for the MySQL server, PHP version
//
// [FUNCTIONS]:    Open($dbname="",$host="",$user="",$pass="",$pcon=0);
//                 Close();
//                 GetLastCmd();
//                 GetError();
//                 GetErrorNum();
//                 Exec($sqlCMD="");
//                 GetRows();
//                 GetAffectedRows();
//                 FetchRows();
//                 FetchRowsA();
//                 GetFieldValue($fieldNAME="");
//                 GetFieldNr($fieldINDEX=0);
//                 GetFieldNames($tableNAME="")
//                 MoveTo($rowNUM=0);
//                 MoveFirst();
//                 MoveLast();
//                 MoveNext();
//                 BeginTrans();
//                 Commit();
//                 RollBack();
//                 Free();
//
// [CREATED BY]:   Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CMySQL
{
    // default database configuration
    var $host;         // host name
    var $user;         // user name
    var $pass;         // password
    var $dbname;       // database name

    var $masterHost;   // master host name
    var $masterUser;   // master user name
    var $masterPass;   // master password
    var $masterDbname; // master database name

    // class-internal variables
    var $linkID;       // mysql link id - slave/local db usually
    var $masterLinkID; // mysql link id - master db

    var $strERR;       // last mysql error string
    var $lastSQLCMD;   // last mysql command/query
    var $lastRESULT;   // last mysql query result
    var $lastROW;      // last row fetched


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CMySQL
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CMySQL()
{
   // default database configuration
   $this->host         = DBHOST;
   $this->dbname       = DBNAME;
   $this->user         = DBUSER;
   $this->pass         = DBPASS;

   // master database configuration
   $this->masterHost   = MASTER_DBHOST;
   $this->masterDbname = MASTER_DBNAME;   
   $this->masterUser   = MASTER_DBUSER;       
   $this->masterPass   = MASTER_DBPASS;

   // in testing mode, both master and local/slave db are set according to the test configuration definition
   if(defined('TESTING_MODE'))
   {
      if(TESTING_MODE)
      {     
         // default database configuration
         $this->host            = TEST_DBHOST;
         $this->dbname          = TEST_DBNAME;
         $this->user            = TEST_DBUSER;
         $this->pass            = TEST_DBPASS;
 
         // master database configuration
         $this->masterHost      = TEST_DBHOST;
         $this->masterDbname    = TEST_DBNAME;  
         $this->masterUser      = TEST_DBUSER;        
         $this->masterPass      = TEST_DBPASS; 
      }
   }

   // class-internal variables
   $this->linkID       = 0;
   $this->masterLinkID = 0;

   $this->strERR       = "";
   $this->lastSQLCMD   = "";
   $this->lastRESULT   = "";
   $this->lastROW      = "";

   $this->strERR       = "No connection";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Open
//
// [DESCRIPTION]:   Open a connection with the MySQL database server
//
// [PARAMETERS]:    dbname, host, user, pass, [pcon]
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Open($dbname="",$host="",$user="",$pass="",$pcon=0)
{
   // force persistent connections
   $pcon = 1;

   // connect to the master DB server if we have it defined
   if(! empty($this->masterDbname))
   {
      // open persistent or normal connection
      if($pcon)
         $this->masterLinkID = mysql_pconnect($this->masterHost, $this->masterUser, $this->masterPass);
      else
         $this->masterLinkID = mysql_pconnect($this->masterHost, $this->masterUser, $this->masterPass, true);

      // connection to mysql server failed
      if(! $this->masterLinkID)
      {
         $this->strERR  = $argv[0]." ";
         $this->strERR .= $pcon ? "persistent " : "";
         $this->strERR .= "connect failed ('{$this->masterUser}@{$this->masterHost}')";

         $date = date("Y-m-d H:i:s");
         $fd = fopen("/tmp/mysql.log", "a+");
         fwrite($fd, "--------------------------------------------\n");
         fwrite($fd, $date." ".$this->strERR."\n");
         fclose($fd);

         return 0; // error
      }

      // select the specified database
      $result = mysql_select_db($this->masterDbname, $this->masterLinkID);
      if(! $result)
      {
         // db selection failed
         mysql_close($this->masterLinkID);
         $this->strERR = "database not found ('{$this->masterDbname}')";

         $date = date("Y-m-d H:i:s");
         $fd = fopen("/tmp/mysql.log", "a+");
         fwrite($fd, "--------------------------------------------\n");
         fwrite($fd, $date." ".$this->strERR."\n");
         fclose($fd);

         return 0; // error
      }
   }

   // overwrite default database definitions
   if($dbname != "") 
      $this->dbname = $dbname;
   else 
      $dbname = $this->dbname;

   if($host != "") 
      $this->host = $host;
   else 
      $host = $this->host;

   if($user != "")
      $this->user = $user;
   else 
      $user = $this->user;

   if($pass != "") 
      $this->pass = $pass;
   else 
      $pass = $this->pass;

    // open persistent or normal connection
    if($pcon)
        $this->linkID = mysql_pconnect($host, $user, $pass);
    else
        $this->linkID = mysql_connect($host, $user, $pass, true);

    // connection to mysql server failed
    if(! $this->linkID)
    {
        $this->strERR  = $argv[0]." ";
        $this->strERR .= $pcon ? "persistent " : "";
        $this->strERR .= "connect failed ('$user:$pass@$host')";

        $date = date("Y-m-d H:i:s");
        $fd = fopen("/tmp/mysql.log", "a+");
        fwrite($fd, "--------------------------------------------\n");
        fwrite($fd, $date." ".$this->strERR."\n");
        fclose($fd);

        return 0; // error
    }

    // select database
    $result = mysql_select_db($dbname, $this->linkID);
    if(! $result)
    {
        // db selection failed
        mysql_close($this->linkID);
        $this->strERR = "database not found ('$dbname')";

        $date = date("Y-m-d H:i:s");
        $fd = fopen("/tmp/mysql.log", "a+");
        fwrite($fd, "--------------------------------------------\n");
        fwrite($fd, $date." ".$this->strERR."\n");
        fclose($fd);

        return 0; // error
    }

    // everything OK
    $this->strERR = "";

    // return with success
    return 1;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the connection with the database server
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
   $this->Free();

   if($this->linkID)
      mysql_close($this->linkID);
 
   if($this->masterLinkID)
      mysql_close($this->masterLinkID);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLastCmd
//
// [DESCRIPTION]:   Retrieve the last SQL command
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error number if there is any, 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLastCmd()
{
    return $this->lastSQLCMD;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    //if(! empty($this->strERR))
    //   return $this->strERR;

    $errorMSG = mysql_error($this->linkID);
    $errorNUM = mysql_errno($this->linkID);

    if(!empty($errorMSG) && $errorNUM)
       $this->strERR = "$errorMSG ($errorNUM)";

    if(MYSQL_DEBUG)
       $this->strERR .= "\n".$this->lastSQLCMD;

    return $this->strERR;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetErrorNum
//
// [DESCRIPTION]:   Retrieve the last error number
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error number if there is any, 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetErrorNum()
{
    return mysql_errno($this->linkID);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Exec
//
// [DESCRIPTION]:   Executes an SQL command , depends by type of sqlCMD : ^SELECT => from localhost(slave) or
//                  ^INSERT,UPDATE,DELETE => from master. In case we set linkMaster => all queries from master.
//
// [PARAMETERS]:    sqlCMD
//
// [RETURN VALUE]:  Data struct (non 0) if success, 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Exec($sqlCMD="", $linkMaster=false)
{
   $this->Free();

   $this->lastSQLCMD = $sqlCMD;

   // default we have the local/slave linkID
   $linkID = $this->linkID;

   if(! preg_match("/^SELECT/i",$sqlCMD))
      $linkID = $this->masterLinkID;

   // last insert ID from master
   if(preg_match("/LAST_INSERT_ID/i",$sqlCMD))
      $linkID = $this->masterLinkID;

   // force query from master db
   if($linkMaster)
      $linkID = $this->masterLinkID;

   //echo "using link id: $linkID\n";
   $this->lastRESULT = mysql_query($sqlCMD, $linkID);

   //$this->LogQuery($sqlCMD);

   if(! $this->lastRESULT)	
   {
      $this->strERR = "Command failed ('$sqlCMD')";

      $date = date("Y-m-d H:i:s");
      $fd = fopen("/tmp/mysqle.log", "a+");
      fwrite($fd, "---------------------Exec-----------------------\n");
      fwrite($fd, $date." ".$this->strERR."\n");
      fclose($fd);

      return 0; // error
   }

   return $this->lastRESULT;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRows
//
// [DESCRIPTION]:   The number of fetched rows, mostly use with the SELECT command
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Number of rows
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetRows()
{
    if(! $this->lastRESULT)
       return 0;

    return mysql_num_rows($this->lastRESULT);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffectedRows
//
// [DESCRIPTION]:   The number of affected rows, mostly use with the UPDATE,
//                  DELETE, INSERT, DROP commands
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Number of affected rows
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAffectedRows()
{
    if(! $this->linkID)
       return 0;

    return mysql_affected_rows($this->linkID);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: FetchRows
//
// [DESCRIPTION]:   Fetch the rows retrieved as a result of an Exec() operation
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  An associative array (hash) of the type key = value if success,
//                  0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function FetchRows()
{
    if($this->lastRESULT)
        $row = mysql_fetch_object($this->lastRESULT);
    else
        $row = 0;

    $this->lastROW = $row;
    return $row;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: FetchRowsA
//
// [DESCRIPTION]:   Fetch the rows retrieved as a result of an Exec() operation
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  An array (hash) containing the field values if success,
//                  0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function FetchRowsA()
{
    if ($this->lastRESULT)
        $row = mysql_fetch_array($this->lastRESULT);
    else
        $row = 0;

    $this->lastROW = $row;
    return $row;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFieldValue
//
// [DESCRIPTION]:   Retrieve the value of a field name after a FetchRows()
//                  operation has been launched against a result set
//
// [PARAMETERS]:    fieldNAME
//
// [RETURN VALUE]:  String containing the field value if success,
//                  empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFieldValue($fieldNAME="")
{
    if(! $this->lastROW)
       return "";

    return $this->lastROW->$fieldNAME;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFieldNr
//
// [DESCRIPTION]:   Retrieve the value of a field name after a FetchRowsA()
//                  operation has been launched against a result set
//
// [PARAMETERS]:    fieldINDEX
//
// [RETURN VALUE]:  String containing the field value if success,
//                  empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFieldNr($fieldINDEX=0)
{
    if(! $this->lastROW)
       return "";

    return $this->lastROW[$fieldINDEX];
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFieldNames
//
// [DESCRIPTION]:   Retrieve the column name of all fields from a sepcified table
//
// [PARAMETERS]:    $fieldNAME
//
// [RETURN VALUE]:  an array with fields names
//
// [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2006-22-02
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllFieldsName($tableNAME="")
{
   if($tableNAME == '')
      return;
   
   $fieldNames = array();

   $this->Exec("SHOW COLUMNS FROM $tableNAME");

   while($this->MoveNext())
   {
      array_push($fieldNames, $this->GetFieldValue("Field"));
   }

   return $fieldNames;

}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MoveTo
//
// [DESCRIPTION]:   Move the cursor position within the rows at a specified
//                  row number
//
// [PARAMETERS]:    rowNUM
//
// [RETURN VALUE]:  True if success, False otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MoveTo($rowNUM=0)
{
    if(! $this->lastRESULT)
       return FALSE;

    if($rowNUM < 0 || $rowNUM > $this->GetRows())
       return FALSE;

    return mysql_data_seek($this->lastRESULT, $rowNUM);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MoveFirst
//
// [DESCRIPTION]:   Move the cursor position to the first row entry
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, False otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MoveFirst()
{
    if(! $this->lastRESULT)
       return FALSE;

    if(! $this->MoveTo(0))
       return FALSE;

    return TRUE;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MoveLast
//
// [DESCRIPTION]:   Move the cursor position to the last row entry
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, False otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MoveLast()
{
    if(! $this->lastRESULT)
       return FALSE;

    if(! $this->MoveTo($this->GetRows()-1))
       return FALSE;

    return TRUE;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MoveNext
//
// [DESCRIPTION]:   Move the cursor position to the last next row entry
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, False otherwise or if there are no more rows
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MoveNext()
{
    if(! $this->lastRESULT)
       return FALSE;

    if(! $this->FetchRows())
       return FALSE;

    return TRUE;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: BeginTrans
//
// [DESCRIPTION]:   Start an SQL transaction
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Non 0 if success, 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function BeginTrans()
{
    return $this->Exec("BEGIN");
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Commit
//
// [DESCRIPTION]:   Commit an SQL transaction
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Non 0 if success, 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Commit()
{
    return $this->Exec("COMMIT");
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: RollBack
//
// [DESCRIPTION]:   Roll back an SQL transaction
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Non 0 if success, 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function RollBack()
{
    return $this->Exec("ROLLBACK");
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Free
//
// [DESCRIPTION]:   Free the memory used after some results have been retrieved
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, False otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Free()
{
    $this->strERR = "";
    return mysql_free_result($this->lastRESULT);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: LogQuery
//
// [DESCRIPTION]:   Loging the sql query to sql.log file
//
// [PARAMETERS]:    sqlCmd
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function LogQuery($sqlCmd='')
{
   // debuging
   if($_SERVER['REMOTE_ADDR'] != '81.196.65.167')
      return ;

   $errorLeft  = '';
   $errorRight = '';
   if(preg_match("/SQL_/", $sqlCmd))
   {
      $errorLeft  = "--- ERROR : ";
      $errorRight = " ---";
   }

   $fh = fopen(ROOT_PATH."sql.log", "a+");
   fwrite($fh, $errorLeft.$sqlCmd.";$errorRight\n");
   fclose($fh);
}

} // end of CMySQL class

?>

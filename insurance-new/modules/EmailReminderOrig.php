<?php
/*****************************************************************************/
/*                                                                           */
/*  CNewsEmailStore class interface                                          */
/*                                                                           */
/*  (C) 2006 Florin MENGHEA (florin@acrux.biz)                               */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";
include_once "functions.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CEmailReminder
//
// [DESCRIPTION]:  CEmailReminder class interface
//
// [FUNCTIONS]:    int  AddEmailRem($id, $userID, $emStoreID,$reminder,$crDate,$update,$status)
//                 bool DelEmailrem($id=0)
//                 bool UpdateEmailRem($id,$reminder=0,$update="",$status=0)
//                 bool GetEmRemByID($id=0, &$arrayResult)
//                 bool GetEmRemByUserID($userID, &$arrayResult)
//                 bool GetEmRemByEmStoreID($emStoreID=0, &$arrayResult)
//                 bool GetEmRemByCrDate($crdate="", &$arrayResult)
//                 bool GetEmRemByUpdate($update="", &$arrayResult)
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Florin MENGHEA(florin@acrux.biz) 23-05-2006
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CEmailReminder
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CEmailReminder
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Florin MENGHEA (florin@acrux.biz) 23-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CEmailReminder($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddEmailRem($id, $userID, $emStoreID,$reminder,$crDate,$update,$status)
//
// [DESCRIPTION]:   Add new entry to the email_reminder table
//
// [PARAMETERS]:    $id, $userID, $emStoreID,$reminder,$crDate,$update,$status
//
// [RETURN VALUE]:  ID or 0 in case of failure
//
// [CREATED BY]:    Florin MENGHEA (florin@acrux.biz) 23-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddEmailRem( $userID, $emStoreID, $reminder,$crDate,$update,$status,$authInfo,$type)
{
//   if(! preg_match("/^\d+$/", $id))
//   {
//      $this->strERR = GetErrorString("INVALID_ID_FIELD");
//      return 0;
//   }

   if(! preg_match("/^\d+$/", $userID))
   {
      $this->strERR = GetErrorString("INVALID_USERID_FIELD");
      return 0;
   }

   if(! preg_match("/^\d+$/", $emStoreID))
   {
      $this->strERR = GetErrorString("INVALID_STOREID_FIELD");
      return 0;
   }

   if(! preg_match("/^\d+$/", $reminder))
   {
      $this->strERR = GetErrorString("INVALID_REMINDER_FIELD");
      return 0;
   }

   $this->lastSQLCMD = "INSERT INTO email_reminder (id,user_id,em_store_id,reminder,cr_date,`update`,status,auth_info,quote_type_id) VALUES ('','$userID','$emStoreID','$reminder','$crDate','$update','$status','$authInfo','$type')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DelEmailRem($id=0)
//
// [DESCRIPTION]:   Delete an entry from email_reminder table by ID
//
// [PARAMETERS]:    $id=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Florin MENGHEA (florin@acrux.biz) 23-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DelEmailrem($id=0)
{
   if(! preg_match("/^\d+$/", $id))
   {
      $this->strERR = GetErrorString("INVALID_EMAIL_REMINDER_ID_FIELD");
      return 0;
   }

   $this->lastSQLCMD = "SELECT id FROM email_reminder WHERE id='$id'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("EMAIL_STORE_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM email_reminder WHERE id='$id'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateEmailRem($id,$reminder=0,$update="",$status=0)
//
// [DESCRIPTION]:   Update email_reminder
//
// [PARAMETERS]:    $reminder=0,$update="",$status=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Florin MENGHEA (florin@acrux.biz) 23-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateEmailRem($id,$reminder=0,$update="",$status=0)
{
//   if(! preg_match("/^\d+$/", $id))
//   {
//      $this->strERR = GetErrorString("INVALID_ID_FIELD");
//      return 0;
//   }
//
//   if(! preg_match("/^\d+$/", $reminder))
//   {
//      $this->strERR = GetErrorString("INVALID_REMINDER_FIELD");
//      return 0;
//   }
//
//   if(! preg_match("/^\d+$/", $status))
//   {
//      $this->strERR = GetErrorString("INVALID_STATUS_FIELD");
//      return 0;
//   }

   $this->lastSQLCMD = "SELECT * FROM email_reminder where id='$id'";

//   print $this->lastSQLCMD."<br>";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("EMAIL_REMINDER_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE email_reminder SET reminder='$reminder', `update`='$update', status='$status' where id='$id'";

//   print $this->lastSQLCMD."<br>";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmRemByID($emRemID=0, &$arrayResult)
//
// [DESCRIPTION]:   Get email_reminder by id
//
// [PARAMETERS]:    $emRemID=0,&$arrayResult
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Florin MENGHEA(florin@acrux.biz) 23-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmRemByID($id=0, &$arrayResult)
{
   $this->lastSQLCMD = "SELECT * FROM email_reminder WHERE id='$id'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("EMAIL_REMINDER_NOT_FOUND");
      return false;
   }

      $arrayResult["id"]          = $this->dbh->GetFieldValue("id");
      $arrayResult["user_id"]     = $this->dbh->GetFieldValue("user_id");
      $arrayResult["em_store_id"] = $this->dbh->GetFieldValue("em_store_id");
      $arrayResult["reminder"]    = $this->dbh->GetFieldValue("reminder");
      $arrayResult["cr_date"]     = $this->dbh->GetFieldValue("cr_date");
      $arrayResult["update"]      = $this->dbh->GetFieldValue("update");
      $arrayResult["status"]      = $this->dbh->GetFieldValue("status");
      $arrayResult["auth_info"]   = $this->dbh->GetFieldValue("auth_info");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmRemByUserID($userID, &$arrayResult)
//
// [DESCRIPTION]:   Get a record by user id
//
// [PARAMETERS]:    $userID, &$arrayResult
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Florin MENGHEA (florin@acrux.biz) 23-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmRemByUserID($userID, &$arrayResult)
{
   if(! preg_match("/^\d+$/", $userID))
   {
      $this->strERR = GetErrorString("INVALID_USERID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM email_reminder WHERE user_id='$userID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("USERID_NOT_FOUND");
      return false;
   }

      $arrayResult["id"]          = $this->dbh->GetFieldValue("id");
      $arrayResult["user_id"]     = $this->dbh->GetFieldValue("user_id");
      $arrayResult["em_store_id"] = $this->dbh->GetFieldValue("em_store_id");
      $arrayResult["reminder"]    = $this->dbh->GetFieldValue("reminder");
      $arrayResult["cr_date"]     = $this->dbh->GetFieldValue("cr_date");
      $arrayResult["update"]      = $this->dbh->GetFieldValue("update");
      $arrayResult["status"]      = $this->dbh->GetFieldValue("status");
      $arrayResult["auth_info"]   = $this->dbh->GetFieldValue("auth_info");

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmRemByAuthInfo($authInfo, &$arrayResult)
//
// [DESCRIPTION]:   Get a record by authinfo
//
// [PARAMETERS]:    $authInfo, &$arrayResult
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Florin MENGHEA (florin@acrux.biz) 23-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmRemByAuthInfo($authInfo, &$arrayResult)
{
//   if(! preg_match("/^\d+$/", $userID))
//   {
//      $this->strERR = GetErrorString("INVALID_USERID_FIELD");
//      return false;
//   }

   $this->lastSQLCMD = "SELECT * FROM email_reminder WHERE auth_info='$authInfo'";

   //print $this->lastSQLCMD;

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("USERID_NOT_FOUND");
      return false;
   }

      $arrayResult["id"]          = $this->dbh->GetFieldValue("id");
      $arrayResult["user_id"]     = $this->dbh->GetFieldValue("user_id");
      $arrayResult["em_store_id"] = $this->dbh->GetFieldValue("em_store_id");
      $arrayResult["reminder"]    = $this->dbh->GetFieldValue("reminder");
      $arrayResult["cr_date"]     = $this->dbh->GetFieldValue("cr_date");
      $arrayResult["update"]      = $this->dbh->GetFieldValue("update");
      $arrayResult["status"]      = $this->dbh->GetFieldValue("status");
      $arrayResult["auth_info"]   = $this->dbh->GetFieldValue("auth_info");
      $arrayResult["quote_type_id"]   = $this->dbh->GetFieldValue("quote_type_id");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmRemByStatus($status = "1", &$arrayResult)
//
// [DESCRIPTION]:   Get a record by his status
//
// [PARAMETERS]:    $status, &$arrayResult
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Florin MENGHEA (florin@acrux.biz) 30-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmRemByStatusAndNoOfDaysAgo($status, $noOfDays, &$arrayResult)
{
   if(! preg_match("/^\d+$/", $status))
   {
      $this->strERR = GetErrorString("INVALID_STATUS_FIELD");
      return false;
   }

//   if(! preg_match("/^\d+$/", $noOfDays));
//   {
//      $this->strERR = GetErrorString("INVALID_NOOFDAYS_FIELD");
//      return false;
//   }
//
   $oneDay = 86400;

   $daysAgo = $oneDay * $noOfDays;

   $date = date("Y-m-d", _mktime(0,0,0,date("m"), date("d"), date("Y")) - $daysAgo);

   $this->lastSQLCMD = "SELECT * FROM email_reminder WHERE status='$status' AND cr_date='$date'";

   print $this->lastSQLCMD."<br>";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERID_NOT_FOUND");
      return false;
   }

//   if(! $this->dbh->FetchRows())
//   {
//      $this->strERR = GetErrorString("USERID_NOT_FOUND");
//      return false;
//   }

   $index = 0;

   while ($this->dbh->MoveNext())
   {
      $arrayResult[$index]["id"]          = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["user_id"]     = $this->dbh->GetFieldValue("user_id");
      $arrayResult[$index]["em_store_id"] = $this->dbh->GetFieldValue("em_store_id");
      $arrayResult[$index]["reminder"]    = $this->dbh->GetFieldValue("reminder");
      $arrayResult[$index]["cr_date"]     = $this->dbh->GetFieldValue("cr_date");
      $arrayResult[$index]["update"]      = $this->dbh->GetFieldValue("update");
      $arrayResult[$index]["status"]      = $this->dbh->GetFieldValue("status");
      $arrayResult[$index]["auth_info"]   = $this->dbh->GetFieldValue("auth_info");
      $index++;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmRemByEmStoreID($emStoreID=0, &$arrayResult)
//
// [DESCRIPTION]:   Get a record by email store id
//
// [PARAMETERS]:    $emStoreID=0, $arrayResult
//
// [RETURN VALUE]:  false | true
//
// [CREATED BY]:    Florin MENGHEA (florin@acrux.biz) 23-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetEmRemByEmStoreID($emStoreID=0, &$arrayResult)
{
   if(! preg_match("/^\d+$/", $emStoreID))
   {
      $this->strERR = GetErrorString("INVALID_EMSTOREID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM email_reminder WHERE em_store_id='$emStoreID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("EMSTOREID_NOT_FOUND");
      return false;
   }

      $arrayResult["id"]          = $this->dbh->GetFieldValue("id");
      $arrayResult["user_id"]     = $this->dbh->GetFieldValue("user_id");
      $arrayResult["em_store_id"] = $this->dbh->GetFieldValue("em_store_id");
      $arrayResult["reminder"]    = $this->dbh->GetFieldValue("reminder");
      $arrayResult["cr_date"]     = $this->dbh->GetFieldValue("cr_date");
      $arrayResult["update"]      = $this->dbh->GetFieldValue("update");
      $arrayResult["status"]      = $this->dbh->GetFieldValue("status");
      $arrayResult["auth_info"]   = $this->dbh->GetFieldValue("auth_info");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmRemByCrDate($crDate="", ,&$arrayResult)
//
// [DESCRIPTION]:   Get a record by create date
//
// [PARAMETERS]:    $crDate="", , $arrayResult
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Florin MENGHEA (florin@acrux.biz) 23-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetEmRemByCrDate($crDate="", &$arrayResult)
{
//   if(! preg_match("/^\d+$/", $newsEmailStoreID))
//   {
//      $this->strERR = GetErrorString("INVALID_ID_FIELD");
//      return false;
//   }

   $this->lastSQLCMD = "SELECT * FROM email_reminder where cr_date='$crDate'";

if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("CRDATE_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $contentResult["id"]          = $this->dbh->GetFieldValue("id");
      $contentResult["user_id"]     = $this->dbh->GetFieldValue("user_id");
      $contentResult["em_store_id"] = $this->dbh->GetFieldValue("em_store_id");
      $contentResult["reminder"]    = $this->dbh->GetFieldValue("reminder");
      $contentResult["cr_date"]     = $this->dbh->GetFieldValue("cr_date");
      $contentResult["update"]      = $this->dbh->GetFieldValue("update");
      $contentResult["status"]      = $this->dbh->GetFieldValue("status");
      $contentResult["auth_info"]   = $this->dbh->GetFieldValue("auth_info");

      $arrayResult[$this->dbh->GetFieldValue("id")] = $contentResult;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmRemByUpdate($update="", &$arrayResult)
//
// [DESCRIPTION]:   Get all homeowner
//
// [PARAMETERS]:    $update="", $arrayResult
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Florin MENGHEA (florin@acrux.biz) 23-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmRemByUpdate($update="", &$arrayResult)
{
   if(! preg_match("/^\d+$/", $update))
   {
      $this->strERR = GetErrorString("INVALID_UPDATE_FIELD");
      return 0;
   }

   $this->lastSQLCMD = "SELECT * FROM email_remainder where update='$update'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("UPDATE_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $contentResult["id"]          = $this->dbh->GetFieldValue("id");
      $contentResult["user_id"]     = $this->dbh->GetFieldValue("user_id");
      $contentResult["em_store_id"] = $this->dbh->GetFieldValue("em_store_id");
      $contentResult["reminder"]    = $this->dbh->GetFieldValue("reminder");
      $contentResult["cr_date"]     = $this->dbh->GetFieldValue("cr_date");
      $contentResult["update"]      = $this->dbh->GetFieldValue("update");
      $contentResult["status"]      = $this->dbh->GetFieldValue("status");
      $contentResult["auth_info"]   = $this->dbh->GetFieldValue("auth_info");

      $arrayResult[$this->dbh->GetFieldValue("id")] = $contentResult;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmRemByInterval($update="", &$arrayResult)
//
// [DESCRIPTION]:   Get all homeowner
//
// [PARAMETERS]:    $update="", $arrayResult
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Florin MENGHEA (florin@acrux.biz) 23-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmRemByInterval($dateFrom, $dateTo)
{
	
	$result = array();
	
   $this->lastSQLCMD = "SELECT count(*) as total FROM email_reminder where cr_date>='$dateFrom' AND cr_date<='$dateTo'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("UPDATE_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("UPDATE_NOT_FOUND");
      return false;
   }

	$result["total"] = $this->dbh->GetFieldValue("total");

   $this->lastSQLCMD = "SELECT count(*) as resent FROM email_reminder where `update`>='$dateFrom' AND `update`<='$dateTo' AND reminder='2'";
//print $this->lastSQLCMD;
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("UPDATE_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("UPDATE_NOT_FOUND");
      return false;
   }
   
	$result["resent"] = $this->dbh->GetFieldValue("resent");

	return $result;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmRemByInterval($update="", &$arrayResult)
//
// [DESCRIPTION]:   Get all homeowner
//
// [PARAMETERS]:    $update="", $arrayResult
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Florin MENGHEA (florin@acrux.biz) 23-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmRemByIntervalAndType($dateFrom, $dateTo, $type)
{
	
	$result = array();
	
   $this->lastSQLCMD = "SELECT count(*) as total FROM email_reminder where quote_type_id='$type' and cr_date>='$dateFrom' AND cr_date<='$dateTo'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("UPDATE_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("UPDATE_NOT_FOUND");
      return false;
   }

	$result["total"] = $this->dbh->GetFieldValue("total");

   $this->lastSQLCMD = "SELECT count(*) as resent FROM email_reminder where quote_type_id='$type' and `update`>='$dateFrom' AND `update`<='$dateTo' AND reminder='2'";
//print $this->lastSQLCMD;
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("UPDATE_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("UPDATE_NOT_FOUND");
      return false;
   }
   
	$result["resent"] = $this->dbh->GetFieldValue("resent");

	return $result;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRenewalsByDate
//
// [DESCRIPTION]:   Get number of renewals by date
//
// [PARAMETERS]:    $renewalID, $quoteUserID, $qzQuoteID, $upDate
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:   Florin (florin@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteTypeIdByEmStoreId($emStId)
{

   $this->lastSQLCMD = "SELECT * FROM email_reminder WHERE em_store_id='$emStId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("quote_type_id");   
   
   return $cnt;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckIfUserMadeQuote($userId, $noOfDays)
//
// [DESCRIPTION]:   Get all homeowner
//
// [PARAMETERS]:    $update="", $arrayResult
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Florin MENGHEA (florin@acrux.biz) 23-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckIfUserMadeQuote($userId, $date)
{
   if(! preg_match("/^\d+$/", $userId))
   {
      $this->strERR = GetErrorString("INVALID_USERID_FIELD");
      return 0;
   }

   $this->lastSQLCMD = "SELECT count(*) as cnt FROM logs where quote_user_id='$userId' AND date>='$date'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("UPDATE_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("UPDATE_NOT_FOUND");
      return false;
   }

   $userDate = $this->dbh->GetFieldValue("cnt");

   if ($cnt > 0)
      return true;

   return false;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Florin MENGHEA (florin@acrux.biz) 23-05-206
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Florin MENGHEA (florin@acrux.biz) 23-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

}
?>

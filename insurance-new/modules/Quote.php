<?php

/*****************************************************************************/
/*                                                                           */
/*  CQuote class interface
/*                                                                           */
/*  (C) 2005 Istvancsek Gabi(gabi@acrux.biz)                                     */
/*                                                                           */
/*****************************************************************************/

define("QUOTES_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuote
//
// [DESCRIPTION]:  CQuote class interface
//
// [FUNCTIONS]:    true   | false AssertQuote($quoteStatusID='', &$quoteRef, &$password)
//                 string | false StdToMysqlDate($date='')
//                 int    | false AddQuote($quoteStatusID='', $quoteRef='', $password='')
//                 true   | false UpdateQuote($quoteID='', $quoteStatusID='', $quoteRef='', $password='')
//                 true   | false DeleteQuote($quoteID='')
//                 array  | false GetQuoteByID($quoteID='')
//                 array  | false GetQuoteByQuoteStatusID($quoteStatusID='')
//                 array  | false GetAllQuotes($siteID='', $startDate='', $endDate='')
//
//                                Close()
//                                GetError()
//                                ShowError()
//
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CQuote
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last Quote error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuote
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQuote($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuote
//
// [DESCRIPTION]:   Verify to see if the requested fields are completed
//
// [PARAMETERS]:    $quoteStatusID, &$quoteRef, &$password
//
// [RETURN VALUE]:  true or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertQuote($quoteStatusID='', &$quoteRef, &$password)
{
    $this->strERR = "";

    if(! preg_match("/^\d+$/", $quoteStatusID))
       $this->strERR .= GetErrorString("INVALID_QUOTE_USER_ID_FIELD");


   if(empty($quoteRef))
      $quoteRef = '-';

   if(empty($password))
      $password = '-';

//    if(empty($quoteRef))
//        $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_REFERENCE_FIELD");
//
//    if(empty($password))
//        $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_PASSWORD_FIELD");

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   if(! preg_match("/\d{1,2}\/\d{1,2}\/\d{4}/i",$date))
      $this->strERR = GetErrorString("INVALID_QUOTE_STD_TO_MYSQL_DATE")."\n";

   if(! empty($this->strERR))
      return false;

   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuote
//
// [DESCRIPTION]:   Add new entry to the quotes table
//
// [PARAMETERS]:    $quoteStatusID, $quoteRef, $password
//
// [RETURN VALUE]:  id or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuote($quoteStatusID='', $quoteRef='', $password='')
{
   if(! $this->AssertQuote($quoteStatusID,$quoteRef,$password))
      return false;

   // we have to check if already exist an quote_status_id by log_id and site id
   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTES." WHERE quote_status_id='$quoteStatusID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
      return false;
   }

   // if exist return quote_id
   if($this->dbh->GetRows())
   {
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      return $this->dbh->GetFieldValue("id");
   }

   $this->lastSQLCMD = "INSERT INTO ".SQL_QUOTES." (quote_status_id,quote_ref,password) VALUES('$quoteStatusID','$quoteRef','$password')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQuote
//
// [DESCRIPTION]:   Update quotes table
//
// [PARAMETERS]:    $quoteID, $quoteStatusID, $quoteRef, $password
//
// [RETURN VALUE]:  true or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQuote($quoteID='', $quoteStatusID='', $quoteRef='', $password='')
{
   if(! preg_match("/^\d+$/", $quoteID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ID_FIELD");
      return false;
   }

   if(! $this->AssertQuote($quoteStatusID,$quoteRef,$password))
      return false;

   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTES." WHERE id='$quoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_QUOTES." SET quote_status_id='$quoteStatusID',quote_ref='$quoteReference',password='$quotePassword' WHERE id='$quoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQuote
//
// [DESCRIPTION]:   Delete an entry from quotes table
//
// [PARAMETERS]:    $quoteID
//
// [RETURN VALUE]:   true or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteQuote($quoteID='')
{
   if(! preg_match("/^\d+$/", $quoteID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTES." WHERE id='$quoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_QUOTES." WHERE id='$quoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteByID
//
// [DESCRIPTION]:   Read data from quotes table and put it into an array variable
//
// [PARAMETERS]:    $quoteID
//
// [RETURN VALUE]:  Array(with the parameters that belong to the quoteID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteByID($quoteID='')
{
   if(! preg_match("/^\d+$/", $quoteID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTES." WHERE id='$quoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]              = $this->dbh->GetFieldValue("id");
   $arrayResult["quote_status_id"] = $this->dbh->GetFieldValue("quote_status_id");
   $arrayResult["log_id"]          = $this->dbh->GetFieldValue("log_id");
   $arrayResult["quote_ref"]       = $this->dbh->GetFieldValue("quote_ref");
   $arrayResult["password"]        = $this->dbh->GetFieldValue("password");

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteByQuoteStatusID
//
// [DESCRIPTION]:   Read data from quotes table and put it into an array variable
//
// [PARAMETERS]:    $quoteStatusID=''
//
// [RETURN VALUE]:  Array(with the parameters that belong to the $quoteStatusID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteByQuoteStatusID($quoteStatusID='')
{
   if(! preg_match("/^\d+$/", $quoteStatusID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTES." WHERE quote_status_id='$quoteStatusID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_ID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]              = $this->dbh->GetFieldValue("id");
   $arrayResult["quote_status_id"] = $this->dbh->GetFieldValue("quote_status_id");
   $arrayResult["quote_ref"]       = $this->dbh->GetFieldValue("quote_ref");
   $arrayResult["password"]        = $this->dbh->GetFieldValue("password");

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuotes
//
// [DESCRIPTION]:   Read data from quotes table and put it into an array variable
//
// [PARAMETERS]:    $siteID, $startDate, $endDate
//
// [RETURN VALUE]:  Array(with the parameters) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuotes($siteID='', $startDate='', $endDate='')
{
   if(! empty($siteID))
   {
      if(! preg_match("/^\d+$/", $siteID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_SITE_ID_FIELD");
         return false;
      }
   }

   if(! empty($startDate))
      if(! $startDate = $this->StdToMysqlDate($startDate))
         return false;

   if(! empty($endDate))
      if(! $endDate = $this->StdToMysqlDate($endDate))
         return false;

   $this->lastSQLCMD = "SELECT l.id as log_id,l.quote_user_id,l.filename,l.date,l.time,qs.site_id,q.* FROM ".SQL_LOGS." l, ".SQL_QUOTE_STATUS." qs, ".SQL_QUOTES." q WHERE 1";

   if($siteID)
      $this->lastSQLCMD .= " AND qs.site_id='$siteID' ";

   if($startDate)
      $this->lastSQLCMD .= " AND l.date>='$startDate' ";

   if($endDate)
      $this->lastSQLCMD .= " AND l.date<='$endDate' ";

   $this->lastSQLCMD .= " AND l.id=qs.log_id AND qs.id=q.quote_status_id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_QUOTE_ID_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index]["id"]              = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["log_id"]          = $this->dbh->GetFieldValue("log_id");
      $arrayResult[$index]["quote_status_id"] = $this->dbh->GetFieldValue("quote_status_id");
      $arrayResult[$index]["quote_user_id"]   = $this->dbh->GetFieldValue("quote_user_id");
      $arrayResult[$index]["quote_ref"]       = $this->dbh->GetFieldValue("quote_ref");
      $arrayResult[$index]["password"]        = $this->dbh->GetFieldValue("password");
      $arrayResult[$index]["site_id"]         = $this->dbh->GetFieldValue("site_id");
      $arrayResult[$index]["filename"]        = $this->dbh->GetFieldValue("filename");
      $arrayResult[$index]["date"]            = $this->dbh->GetFieldValue("date");
      $arrayResult[$index]["time"]            = $this->dbh->GetFieldValue("time");
  }

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

} // end of CQuote class
?>

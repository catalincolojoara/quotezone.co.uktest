<?php

/*****************************************************************************/
/*                                                                           */
/*  CQzQuote class interface                                                 */
/*                                                                           */
/*  (C) 2005 Istvancsek Gabriel (gabi@acrux.ro)                                     */
/*                                                                           */
/*****************************************************************************/

define("QZQUOTES_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQzQuote
//
// [DESCRIPTION]:  CQzQuote class interface
//
// [FUNCTIONS]:      true | false  AssertQzQuotes($quoteUserDetailsID='', $quoteTypeID='',$quoteRef='')
//                   int | false   AddQzQuote($quoteUserDetailsID='', $quoteTypeID='' , $quoteRef='', //                                 $affiliateID=0)
//                   true | fasle  UpdateQzQuote($quoteID='', $quoteUserDetailsID='', $quoteTypeID='', $quoteRef='', $affiliateID=0)
//                   true | fasle  DeleteQzQuote($quoteID='')
//                   array | false GetQzQuoteByID($quoteID='')
//                   array | false GetQzQuoteByQRef($quoteRef="")
//                   array | false GetUnsentEmailQzQuote($qzQuoteID='')
//                   array | false GetQzQuoteByQUserID($quoteUserDetailsID='')
//                   true | false  ValidateQzQuoteRefPasswd($quoteRef="", $password="")
//                   true | false  CheckEmailUnicity($quoteUserDetailsID='', &$quoteUserEmail)
//                   array | false GetAllQzQuotes($siteID='', $startDate="", $endDate="")
//                   array | false GetAllQzQuotesByUser($quoteUserDetailsID='')
//                   true | false  ValidateQzQuoteReference($quoteRef='')
//                   string        GenerateQuoteRef()
//                   int | false  GetNumberOfQuotes($siteID='', $startDate="", $endDate="")
//                   int | false  GetNumberOfUniqueQuotes($siteID='', $startDate="", $endDate="")
//                   string        StdToMysqlDate($date='')
//                   string        MakeSeed()
//
//                   Close();
//                   GetError();
//                   ShowError();
//
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CQzQuote
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last Quote error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQzQuote
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQzQuote($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQzQuotes
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    $quoteUserDetailsID=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertQzQuotes($logID='', $quoteUserDetailsID='', $quoteTypeID='', $quoteRef='')
{
   $this->strERR = '';

   if(! preg_match("/^\d+$/", $logID))
      $this->strERR  = GetErrorString("INVALID_QZ_QUOTE_LOG_ID_FIELD");

   if(! preg_match("/^\d+$/", $quoteUserDetailsID))
      $this->strERR  = GetErrorString("INVALID_QZ_QUOTE_USER_DETAILS_ID_FIELD");

   if(! preg_match("/^\d+$/", $quoteTypeID))
      $this->strERR .= GetErrorString("INVALID_QZ_QUOTE_QUOTE_TYPE_ID_FIELD");

   if(empty($quoteRef))
      $this->strERR .= GetErrorString("INVALID_QZ_QUOTE_REFERENCE_FIELD");

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQzQuote
//
// [DESCRIPTION]:   Add new entry to the qzquotes table
//
// [PARAMETERS]:    $quoteUserDetailsID='', $quoteTypeID='', $quoteRef='', $affiliateID=0
//
// [RETURN VALUE]:  $qzQuoteID | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQzQuote($logID='', $quoteUserDetailsID='', $quoteTypeID='' , $quoteRef='', $affiliateID=0)
{

   if(! $this->AssertQzQuotes($logID,$quoteUserDetailsID,$quoteTypeID,$quoteRef))
      return false;

   $this->lastSQLCMD = "INSERT INTO ".SQL_QZQUOTES." (log_id,quote_user_details_id,quote_type_id,quote_ref,affiliate_id) VALUES('$logID','$quoteUserDetailsID','$quoteTypeID','$quoteRef','$affiliate_id')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQzQuote
//
// [DESCRIPTION]:   Update qzquotes table
//
// [PARAMETERS]:    $quoteID=0, $quoteUserDetailsID='', $quoteReference='', $quoteRef='', $affiliateID=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQzQuote($quoteID='', $logID='', $quoteUserDetailsID='', $quoteTypeID='', $quoteRef='', $affiliateID=0)
{
   if(! preg_match("/^\d+$/", $quoteID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTE_ID_FIELD");
      return false;
   }

   if(! $this->AssertQzQuotes($logID,$quoteUserDetailsID,$quoteTypeID,$quoteRef))
      return false;

   $this->lastSQLCMD = "SELECT id FROM ".SQL_QZQUOTES." WHERE id='$quoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QZ_QUOTE_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_QZQUOTES." SET log_id='$logID', quote_user_details_id='$quoteUserDetailsID',quote_type_id='$quoteTypeID',quote_ref='$quoteReference',affiliate_id='$affiliateID' WHERE id='$quoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQzQuote
//
// [DESCRIPTION]:   Delete an entry from qzquotes table
//
// [PARAMETERS]:    $quoteID
//
// [RETURN VALUE]:   true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteQzQuote($quoteID='')
{
   if(! preg_match("/^\d+$/", $quoteID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTE_ID_FIELD");
      return false;
   }

   // check if codeID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_QZQUOTES." WHERE id='$quoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QZ_QUOTE_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_QZQUOTES." WHERE id='$quoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQzQuoteByID
//
// [DESCRIPTION]:   Read data from qzquotes table and put it into an array variable
//
// [PARAMETERS]:    $quoteID
//
// [RETURN VALUE]:  Array(with the parameters that belong to the quoteID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQzQuoteByID($quoteID='')
{
   if(! preg_match("/^\d+$/", $quoteID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTE_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QZQUOTES." WHERE id='$quoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QZ_QUOTE_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]                    = $this->dbh->GetFieldValue("id");
   $arrayResult["log_id"]                = $this->dbh->GetFieldValue("log_id");
   $arrayResult["quote_user_details_id"] = $this->dbh->GetFieldValue("quote_user_details_id");
   $arrayResult["quote_type_id"]         = $this->dbh->GetFieldValue("quote_type_id");
   $arrayResult["quote_ref"]             = $this->dbh->GetFieldValue("quote_ref");
   $arrayResult["affiliate_id"]          = $this->dbh->GetFieldValue("affiliate_id");

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQzQuoteByQRef
//
// [DESCRIPTION]:   Read data from qzquotes table and put it into an array variable
//
// [PARAMETERS]:    $quoteRef=""
//
// [RETURN VALUE]:  Array(with the parameters that belong to the quoteID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQzQuoteByQRef($quoteRef="")
{
   if(empty($quoteRef))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QREF_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QZQUOTES." WHERE quote_ref='$quoteRef'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]                    = $this->dbh->GetFieldValue("id");
   $arrayResult["log_id"]                = $this->dbh->GetFieldValue("log_id");
   $arrayResult["quote_user_details_id"] = $this->dbh->GetFieldValue("quote_user_details_id");
   $arrayResult["quote_type_id"]         = $this->dbh->GetFieldValue("quote_type_id");
   $arrayResult["quote_ref"]             = $this->dbh->GetFieldValue("quote_ref");
   $arrayResult["affiliate_id"]          = $this->dbh->GetFieldValue("affiliate_id");

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQzQuoteBiggerThanQuoteId();
//
// [DESCRIPTION]:   Read data bigger then $quoteID from qzquotes table and put it into an array variable
//
// [PARAMETERS]:    $quoteID=''
//
// [RETURN VALUE]:  Array(with the parameters that belong to the quoteID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUnsentEmailQzQuote($qzQuoteID, $quoteTypeID)
{
   if(! preg_match("/^\d+$/", $qzQuoteID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTE_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   $time = time();
   $time -= 600;

   $date = date("Y-m-d", $time);
   $time = date("H:i:s", $time);

   $this->lastSQLCMD = "SELECT ".SQL_LOGS.".id as log_id,".SQL_LOGS.".filename,".SQL_QZQUOTES.".affiliate_id,".SQL_QZQUOTES.".id as quote_id,".SQL_QUOTE_USER_DETAILS.".id as quote_user_details_id,".SQL_QUOTE_USER_DETAILS.".email,".SQL_QUOTE_USER_DETAILS.".title,".SQL_QZQUOTES.".quote_ref,".SQL_QUOTE_USERS.".id as quote_user_id FROM ".SQL_QZQUOTES.",".SQL_LOGS.",".SQL_QUOTE_USER_DETAILS.",".SQL_QUOTE_USERS." WHERE ".SQL_LOGS.".date<='$date' AND ".SQL_LOGS.".time<='$time' AND ".SQL_QZQUOTES.".id>'$qzQuoteID' AND ".SQL_QZQUOTES.".quote_type_id='$quoteTypeID' AND ".SQL_LOGS.".id=".SQL_QZQUOTES.".log_id AND ".SQL_QZQUOTES.".quote_user_details_id=".SQL_QUOTE_USER_DETAILS.".id AND ".SQL_QUOTE_USERS.".id=".SQL_LOGS.".quote_user_id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QZ_QUOTE_ID_NOT_FOUND");
      return false;
   }

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index]["log_id"]                = $this->dbh->GetFieldValue("log_id");
      $arrayResult[$index]["quote_user_details_id"] = $this->dbh->GetFieldValue("quote_user_details_id");
      $arrayResult[$index]["quote_user_id"]         = $this->dbh->GetFieldValue("quote_user_id");
      $arrayResult[$index]["quote_id"]              = $this->dbh->GetFieldValue("quote_id");
      $arrayResult[$index]["quote_ref"]             = $this->dbh->GetFieldValue("quote_ref");
      $arrayResult[$index]["filename"]              = $this->dbh->GetFieldValue("filename");
      $arrayResult[$index]["affiliate_id"]          = $this->dbh->GetFieldValue("affiliate_id");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQzQuoteBiggerThanQuoteId();
//
// [DESCRIPTION]:   Read data bigger then $quoteID from qzquotes table and put it into an array variable
//
// [PARAMETERS]:    $quoteID=''
//
// [RETURN VALUE]:  Array(with the parameters that belong to the quoteID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUnsentScanningEmailQzQuote($topLevel, $quoteTypeID)
{
   if(! preg_match("/^\d+$/", $topLevel))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTE_TOP_LEVEL_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   switch($topLevel)
   {
      case '1':
         $topLevelOn = " AND ".SQL_MAIL_INFO.".top_one='ON' ";
         break;

      case '3':
         $topLevelOn = " AND ".SQL_MAIL_INFO.".top_three='ON' ";
         break;

      default:
        $this->strERR = GetErrorString("INVALID_QZ_QUOTE_TOP_LEVEL_FIELD");
        return false;
      break;
   }

   $this->lastSQLCMD = "SELECT ".SQL_LOGS.".id as log_id,".SQL_LOGS.".filename,".SQL_QZQUOTES.".affiliate_id,".SQL_QZQUOTES.".id as quote_id, ".SQL_QUOTE_USERS.".id as quote_user_id,".SQL_QUOTE_USER_DETAILS.".id as quote_user_details_id,".SQL_QUOTE_USER_DETAILS.".email,".SQL_QUOTE_USER_DETAILS.".title,".SQL_QZQUOTES.".quote_ref  FROM ".SQL_QZQUOTES.",".SQL_QUOTE_USER_DETAILS.",".SQL_QUOTE_USERS.",".SQL_LOGS.",".SQL_MAIL_INFO." WHERE ".SQL_LOGS.".id=".SQL_QZQUOTES.".log_id AND ".SQL_LOGS.".quote_user_id=".SQL_QUOTE_USERS.".id AND ".SQL_QUOTE_USERS.".id=".SQL_QUOTE_USER_DETAILS.".quote_user_id AND ".SQL_MAIL_INFO.".qzquote_id=".SQL_QZQUOTES.".id AND ".SQL_QZQUOTES.".quote_user_details_id=".SQL_QUOTE_USER_DETAILS.".id AND ".SQL_QZQUOTES.".quote_type_id='$quoteTypeID' $topLevelOn";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QZ_QUOTE_ID_NOT_FOUND");
      return false;
   }

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index]["log_id"]                = $this->dbh->GetFieldValue("log_id");
      $arrayResult[$index]["quote_user_details_id"] = $this->dbh->GetFieldValue("quote_user_details_id");
      $arrayResult[$index]["quote_user_id"]         = $this->dbh->GetFieldValue("quote_user_id");
      $arrayResult[$index]["quote_id"]              = $this->dbh->GetFieldValue("quote_id");
      $arrayResult[$index]["quote_ref"]             = $this->dbh->GetFieldValue("quote_ref");
      $arrayResult[$index]["filename"]              = $this->dbh->GetFieldValue("filename");
      $arrayResult[$index]["affiliate_id"]          = $this->dbh->GetFieldValue("affiliate_id");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQzQuoteByQUserID
//
// [DESCRIPTION]:   Read data from qzquotes table and put it into an array variable
//
// [PARAMETERS]:    $quoteRef=""
//
// [RETURN VALUE]:  Array(with the parameters that belong to the quoteID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQzQuoteByQUserID($quoteUserDetailsID='')
{
   if(! preg_match("/\d+/", $quoteUserDetailsID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTE_USER_DETAILS_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QZQUOTES." WHERE quote_user_details_id='$quoteUserDetailsID' ORDER BY id DESC LIMIT 1";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QZ_QUOTE_USER_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]                     = $this->dbh->GetFieldValue("id");
   $arrayResult["log_id"]                 = $this->dbh->GetFieldValue("log_id");
   $arrayResult["quote_user_details_id"]  = $this->dbh->GetFieldValue("quote_user_details_id");
   $arrayResult["quote_type_id"]          = $this->dbh->GetFieldValue("quote_type_id");
   $arrayResult["quote_ref"]              = $this->dbh->GetFieldValue("quote_ref");
   $arrayResult["affiliate_id"]           = $this->dbh->GetFieldValue("affiliate_id");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ValidateQzQuoteRefPasswd
//
// [DESCRIPTION]:   Read data from qzquotes table and put it into an array variable
//
// [PARAMETERS]:    $quoteRef=""
//
// [RETURN VALUE]:  Array(with the parameters that belong to the quoteID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ValidateQzQuoteRefPasswd($quoteRef='', $password='')
{
   if(empty($quoteRef))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QREF_FIELD");
      return false;
   }

   if(empty($password))
   {
      $this->strERR = GetErrorString("INVALID_QZ_PASSWORD_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QZQUOTES.",".SQL_QUOTE_USERS.",".SQL_QUOTE_USER_DETAILS." WHERE ".SQL_QZQUOTES.".quote_user_details_id= ".SQL_QUOTE_USER_DETAILS.".id AND ".SQL_QUOTE_USER_DETAILS.".quote_user_id=".SQL_QUOTE_USERS.".id AND ".SQL_QZQUOTES.".quote_ref='$quoteRef' and ".SQL_QUOTE_USERS.".password='$password'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QREF_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["quote_user_details_id"] = $this->dbh->GetFieldValue("quote_user_details_id");
   $arrayResult["quote_user_id"] = $this->dbh->GetFieldValue("quote_user_id");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuotesByUser
//
// [DESCRIPTION]:   Read data from qzquotes table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserDetailsID=0
//
// [RETURN VALUE]:  Array(with the parameters that belong to the quoteID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQzQuotesByUser($quoteUserID='', $quoteTypeID='')
{
   if(! preg_match("/^\d+$/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTE_USER_ID");
      return false;
   }

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTE_TYPE_ID");
      return false;
   }

   $this->lastSQLCMD = "SELECT ".SQL_QZQUOTES.".*,".SQL_LOGS.".date,".SQL_LOGS.".time,".SQL_LOGS.".host_ip,".SQL_LOGS.".filename FROM ".SQL_QUOTE_USERS.",".SQL_QUOTE_USER_DETAILS.",".SQL_QZQUOTES.",".SQL_LOGS." where ".SQL_QUOTE_USERS.".id='$quoteUserID' AND ".SQL_QUOTE_USERS.".id=".SQL_LOGS.".quote_user_id AND ".SQL_QUOTE_USERS.".id=".SQL_QUOTE_USER_DETAILS.".quote_user_id AND ".SQL_QUOTE_USER_DETAILS.".id = ".SQL_QZQUOTES.".quote_user_details_id AND ".SQL_LOGS.".id=".SQL_QZQUOTES.".log_id AND ".SQL_QZQUOTES.".quote_type_id='$quoteTypeID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_USER_ID_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index]["id"]                    = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["quote_user_details_id"] = $this->dbh->GetFieldValue("quote_user_details_id");
      $arrayResult[$index]["quote_type_id"]         = $this->dbh->GetFieldValue("quote_type_id");
      $arrayResult[$index]["quote_ref"]             = $this->dbh->GetFieldValue("quote_ref");
      $arrayResult[$index]["affiliate_id"]          = $this->dbh->GetFieldValue("affiliate_id");
      $arrayResult[$index]["log_id"]                = $this->dbh->GetFieldValue("log_id");
      $arrayResult[$index]["date"]                  = $this->dbh->GetFieldValue("date");
      $arrayResult[$index]["time"]                  = $this->dbh->GetFieldValue("time");
      $arrayResult[$index]["host_ip"]               = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$index]["filename"]              = $this->dbh->GetFieldValue("filename");
   }

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ValidateQuoteReference()
//
// [DESCRIPTION]:   Check if we have already this quote reference in qz database
//
// [PARAMETERS]:    $quoteRef
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ValidateQzQuoteReference($quoteRef='', $quoteTypeID='')
{
   if(empty($quoteRef))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_REFERENCE");
      return false;
   }

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTE_TYPE_ID");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QZQUOTES." where quote_ref='$quoteRef' AND quote_type_id='$quoteTypeID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
      return true;

   return false;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GenerateRandomText()
//
// [DESCRIPTION]:   Generates a password text
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the random text
//
// [CREATED BY]:    Eugen Savin (seugen@acrux.biz) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GenerateQuoteRef($quoteTypeID='1')
{
   $garbleTextLetters = array('A','B','C','D','E','F','G','H','J','K','L','M','N','P','R','S','T','U','V','X','Y','W','Z');
   $garbleTextNumbers = array('0','1','2','3','4','5','6','7','8','9');

   $maxLettersIndex = count($garbleTextLetters) - 1;
   $maxNumbersIndex = count($garbleTextNumbers) - 1;

   switch($quoteTypeID)
   {
      case '1':
         $randomText = "C";
         break;
      
      case '2':
         $randomText = "V";
         break;

      // ignnore generic type
      case '3':
         break;

      case '4':
         $randomText = "H";
         break;

      case '5':
         $randomText = "B";
         break;

      default:
         // default car
         $randomText = "C";
         break;
   }

   $time = time();

   for($i=0; $i<5; $i++)
   {
      // guess if it is a letter or a number
      // 0 == text
      // 1 = number

      mt_srand($this->MakeSeed());
      $guess = mt_rand(0, 1);

      mt_srand($this->MakeSeed());

      if($guess == 0)
         $randomText .= $garbleTextLetters[mt_rand(0, $maxLettersIndex)];
      else
         $randomText .= $garbleTextNumbers[mt_rand(0, $maxNumbersIndex)];

      if($i == 1)
         $randomText .= '-';
   }

   return $randomText;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   //if(! preg_match("\/", $date))
   //  return $date;

   if(! preg_match("/\d{1,2}\/\d{1,2}\/\d{4}/i",$date))
      $this->strERR = GetErrorString("INVALID_QZ_QUOTES_STD_TO_MYSQL_DATE")."\n";

   if(! empty($this->strERR))
      return false;

   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetNumberOfQuotes
//
// [DESCRIPTION]:   Read data from qzquotes table and put it into an array variable
//
// [PARAMETERS]:    $siteID='', $startDate="", $endDate=""
//
// [RETURN VALUE]:  int or false in case of failure
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetNumberOfQuotes($siteID='', $quoteTypeID='', $startDate="", $endDate="")
{

   if(! empty($siteID))
   {
      if(! preg_match("/^\d+$/", $siteID))
      {
         $this->strERR = GetErrorString("INVALID_SITEID_FIELD");
         return false;
      }
   }

   if(! empty($quoteTypeID))
   {
      if(! preg_match("/^\d+$/", $quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
         return false;
      }
   }

   if(! empty($startDate))
   {
      if(! $startDate = $this->StdToMysqlDate($startDate))
         return false;
   }

   if(! empty($endDate))
   {
      if(! $endDate = $this->StdToMysqlDate($endDate))
         return false;
   }


   $this->lastSQLCMD = "select count(qz.id) as numberOfQuotes from ".SQL_QZQUOTES." qz, ".SQL_LOGS." l";

   if($siteID)
      $this->lastSQLCMD .= ",".SQL_QUOTE_STATUS." qs ";

   if($siteID || $startDate || $endDate)
      $this->lastSQLCMD .=" WHERE 1";

   $this->lastSQLCMD .= "  AND qz.log_id=l.id";

   if($siteID)
      $this->lastSQLCMD .= " AND and qs.log_id=l.id AND qs.site_id='$siteID' ";

   if($quoteTypeID)
      $this->lastSQLCMD .= " AND qz.quote_type_id='$quoteTypeID' ";

   if($startDate)
      $this->lastSQLCMD .= " AND l.date>='$startDate' ";

   if($endDate)
      $this->lastSQLCMD .= " AND l.date<='$endDate' ";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("numberOfQuotes");;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetNumberOfUniqueQuotes
//
// [DESCRIPTION]:   Read data from qzquotes table and put it into an array variable
//
// [PARAMETERS]:    $siteID='', $startDate="", $endDate=""
//
// [RETURN VALUE]:  int or false in case of failure
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetNumberOfUniqueQuotes($siteID='', $quoteTypeID='', $startDate="", $endDate="")
{
   if(! empty($siteID))
   {
      if(! preg_match("/^\d+$/", $siteID))
      {
         $this->strERR = GetErrorString("INVALID_SITEID_FIELD");
         return false;
      }
   }

   if(! empty($quoteTypeID))
   {
      if(! preg_match("/^\d+$/", $quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
         return false;
      }
   }

   if(! empty($startDate))
   {
      if(! $startDate = $this->StdToMysqlDate($startDate))
         return false;
   }

   if(! empty($endDate))
   {
      if(! $endDate = $this->StdToMysqlDate($endDate))
         return false;
   }

   $this->lastSQLCMD = "select count(distinct l.quote_user_id) as numberOfQuotes from ".SQL_QZQUOTES." qz, ".SQL_LOGS." l";

   if($siteID)
      $this->lastSQLCMD .= ",".SQL_QUOTE_STATUS." qs ";

   if($siteID || $startDate || $endDate)
      $this->lastSQLCMD .=" WHERE 1";

   $this->lastSQLCMD .= "  AND qz.log_id=l.id";

   if($siteID)
      $this->lastSQLCMD .= "  AND and qs.log_id=l.id AND qs.site_id='$siteID' ";

   if($quoteTypeID)
      $this->lastSQLCMD .= " AND qz.quote_type_id='$quoteTypeID' ";

   if($startDate)
      $this->lastSQLCMD .= " AND l.date>='$startDate' ";

   if($endDate)
      $this->lastSQLCMD .= " AND l.date<='$endDate' ";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("numberOfQuotes");;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Print the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MakeSeed()
//
// [DESCRIPTION]:   Make random number
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  random number
//
// [CREATED BY]:    Eugen SAVIN (seugen@acrux.biz) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MakeSeed()
{
   list($usec, $sec) = explode(' ', microtime());

   return (float) $sec + ((float) $usec * 100000000);
}

} // end of CQzQuote class
?>

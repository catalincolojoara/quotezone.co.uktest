<?php
/*****************************************************************************/
/*                                                                           */
/*  CQuoteStatus class interface
/*                                                                           */
/*  (C) 2005 Istvancsek Gabriel (gabi@acrux.biz)                                     */
/*                                                                           */
/*****************************************************************************/

define("Status_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuoteStatus
//
// [DESCRIPTION]:  CQuoteStatus class interface
//
// [FUNCTIONS]:      true   | false AssertQuoteStatus($logID='', $siteID='', $status='',$type='')
//                   string | false StdToMysqlDate($date='')
//                   int    | false AddQuoteStatus($logID='', $siteID='', $status='SUCCESS',$type='NORMAL')
//                   true   | false UpdateQuoteStatus($quoteStatusID='', $logID='', $siteID='', $status='SUCCESS',$type='NORMAL')
//                   true   | false DeleteQuoteStatus($quoteStatusID='')
//                   array  | false GetQuoteStatus($quoteStatusID='')
//                   array  | false GetQuoteStatusByLogIdAndSiteId($logID='', $siteID='')
//                   array  | false GetAllQuoteStatus($siteID='', $startDate='', $endDate='')
//                   array  | false GetAllQuoteStatusBySiteID($siteID='')
//                   array  | false GetAllQuoteStatusByStatus($status="")
//                   array  | false GetAllQuoteStatusByLogID($logID='')
//                   array  | false GetAllQuoteStatusBySiteIDAndStatus($siteID='',$status='')
//                   array  | false GetAllQuotesStatusByInterval($quoteUserID='', $hostIp='', $startDate='',$endDate='',$startTime='',$endTime='')
//    TO DO          array  | false GetAllQuoteStatusBySiteIDAndStatusAndType($siteID='',$status='')???
//                   array  | false GetAllQuoteStatus($siteID='', $startDate='', $endDate='')
//                   array  | false GetLastQuoteStatusByQuoteUserID($quoteUserID='', $brokerID='')
//                   true   | false ExistQuoteStatusSite($logID='', $siteID='')
//
//                   Close()
//                   GetError()
//                   ShowError()
//
// [CREATED BY]:   Istvcansek Gabriel (gabi@acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CQuoteStatus
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last Status error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuoteStatus
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQuoteStatus($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuoteStatus
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    $logID, $siteID, $status, $type
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertQuoteStatus($logID='', $siteID='', $status='', $type='')
{
   $this->strERR = '';

   if(! preg_match("/\d+/",$logID))
      $this->strERR  = GetErrorString("INVALID_QUOTE_STATUS_LOG_ID_FIELD");

   if(! preg_match("/\d+/",$siteID))
      $this->strERR .= GetErrorString("INVALID_QUOTE_STATUS_SITE_ID_FIELD");

   if(empty($status))
      $this->strERR .= GetErrorString("INVALID_QUOTE_STATUS_STATUS_FIELD");

   if(empty($type))
      $this->strERR .= GetErrorString("INVALID_QUOTE_STATUS_TYPE_FIELD");

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   if(! preg_match("/\d{1,2}\/\d{1,2}\/\d{4}/i",$date))
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_STD_TO_MYSQL_DATE")."\n";

   if(! empty($this->strERR))
      return false;

   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteStatus
//
// [DESCRIPTION]:   Add new entry to the Statuss table
//
// [PARAMETERS]:    $logID, $siteID, $status, $type
//
// [RETURN VALUE]:  int or false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteStatus($logID='', $siteID='', $status='SUCCESS', $type='NORMAL')
{
   if(! $this->AssertQuoteStatus($logID,$siteID,$status,$type))
      return false;

   // we have to check if already exist an quote_status_id by log_id and site id
   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTE_STATUS." WHERE log_id='$logID' AND site_id='$siteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD, true))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   // if exist return quote_status_id
   if($this->dbh->GetRows())
   {
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      return $this->dbh->GetFieldValue("id");
   }

   $this->lastSQLCMD = "INSERT INTO ".SQL_QUOTE_STATUS." (log_id,site_id,status,type) VALUES ('$logID','$siteID','$status','$type')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQuoteStatus
//
// [DESCRIPTION]:   Update Status table
//
// [PARAMETERS]:    $quoteStatusID, $logID, $siteID, $status,$type
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQuoteStatus($quoteStatusID='', $logID='', $siteID='', $status='SUCCESS',$type='NORMAL')
{

   if(! preg_match("/^\d+$/", $quoteStatusID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_ID_FIELD");
      return false;
   }

   if(! $this->AssertQuoteStatus($logID,$siteID,$status,$type))
      return false;
/*
   // master/slave sync problems
   // check if logID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTE_STATUS." WHERE id='$quoteStatusID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }


   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_ID_NOT_FOUND");
      return false;
   }

*/

   $this->lastSQLCMD = "UPDATE ".SQL_QUOTE_STATUS." SET log_id='$logID',site_id='$siteID',status='$status',type='$type' WHERE id='$quoteStatusID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQuoteStatus
//
// [DESCRIPTION]:   Delete an entry from Status table
//
// [PARAMETERS]:    $quoteStatusID
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteQuoteStatus($quoteStatusID='')
{
   if(! preg_match("/^\d+$/", $quoteStatusID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_ID_FIELD");
      return false;
   }

   // check if logID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTE_STATUS." WHERE id='$quoteStatusID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_QUOTE_STATUS." WHERE id='$quoteStatusID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteStatus
//
// [DESCRIPTION]:   Read data from Statuss table and put it into an array variable
//
// [PARAMETERS]:    $quoteStatusID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteStatus($quoteStatusID='')
{

   if(! preg_match("/^\d+$/", $quoteStatusID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_STATUS." WHERE id='$quoteStatusID'";

   if(! $this->dbh->Exec($this->lastSQLCMD, true))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_ID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]             = $this->dbh->GetFieldValue("id");
   $arrayResult["log_id"]         = $this->dbh->GetFieldValue("log_id");
   $arrayResult["site_id"]        = $this->dbh->GetFieldValue("site_id");
   $arrayResult["status"]         = $this->dbh->GetFieldValue("status");
   $arrayResult["type"]           = $this->dbh->GetFieldValue("type");

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteStatusByLogIdAndSiteId
//
// [DESCRIPTION]:   Read data from Statuss table and put it into an array variable
//
// [PARAMETERS]:    $logID, $siteID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteStatusByLogIdAndSiteId($logID='', $siteID='')
{

   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_LOG_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_SITE_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT ".SQL_QUOTE_STATUS.".* FROM ".SQL_LOGS.",".SQL_QUOTE_STATUS." WHERE ".SQL_LOGS.".id='$logID' AND ".SQL_QUOTE_STATUS.".site_id='$siteID' AND ".SQL_LOGS.".id=".SQL_QUOTE_STATUS.".log_id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_ID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]             = $this->dbh->GetFieldValue("id");
   $arrayResult["log_id"]         = $this->dbh->GetFieldValue("log_id");
   $arrayResult["site_id"]        = $this->dbh->GetFieldValue("site_id");
   $arrayResult["status"]         = $this->dbh->GetFieldValue("status");
   $arrayResult["type"]           = $this->dbh->GetFieldValue("type");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSitesFilterConditions
//
// [DESCRIPTION]:   Read data from quote_errors table 
//
// [PARAMETERS]:    $logID, $siteID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2009-10-16
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSitesFilterConditions($logID='', $siteID='')
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_LOG_ID_FIELD");
      return false;
   }

   //if(! preg_match("/^\d+$/", $siteID))
   //{
   //   $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_SITE_ID_FIELD");
   //   return false;
   //}

   $this->lastSQLCMD = "SELECT qs.site_id,qe.message FROM ".SQL_QUOTE_STATUS." qs, ".SQL_QUOTE_ERRORS." qe WHERE qs.log_id='$logID' AND qs.status='SKIPPED' AND qs.id=qe.quote_status_id";

   if($siteID)
      $this->lastSQLCMD .= " AND qs.site_id='$siteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD, 1))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_LOG_ID_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $siteID                          = $this->dbh->GetFieldValue("site_id");
      $arrayResult[$siteID]["message"] = $this->dbh->GetFieldValue("message");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteStatusByLogId
//
// [DESCRIPTION]:   Read data from Statuss table and put it into an array variable
//
// [PARAMETERS]:    $logID
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2009-09-29
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteStatusByLogId($logID='')
{

   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_LOG_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT ".SQL_QUOTE_STATUS.".* FROM ".SQL_LOGS.",".SQL_QUOTE_STATUS." WHERE ".SQL_LOGS.".id='$logID' AND ".SQL_LOGS.".id=".SQL_QUOTE_STATUS.".log_id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_LOG_ID_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $siteID = $this->dbh->GetFieldValue("site_id");
      
      $arrayResult[$siteID]["id"]             = $this->dbh->GetFieldValue("id");
      $arrayResult[$siteID]["log_id"]         = $this->dbh->GetFieldValue("log_id");
      $arrayResult[$siteID]["site_id"]        = $this->dbh->GetFieldValue("site_id");
      $arrayResult[$siteID]["status"]         = $this->dbh->GetFieldValue("status");
      $arrayResult[$siteID]["type"]           = $this->dbh->GetFieldValue("type");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteStatusSiteIdSendMailInfo
//
// [DESCRIPTION]:   Get the site id which we have to send the mail status
//
// [PARAMETERS]:    $logID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-10-27
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteStatusSiteIdSendMailInfo($logID='')
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_QZQUOTE_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT ".SQL_QUOTE_STATUS.".site_id FROM ".SQL_LOGS.",".SQL_QUOTE_STATUS.",".SQL_QUOTE_SCANNINGS." WHERE ".SQL_LOGS.".id='$logID'  AND ".SQL_LOGS.".id=".SQL_QUOTE_STATUS.".log_id AND ".SQL_QUOTE_STATUS.".id=".SQL_QUOTE_SCANNINGS.".quote_status_id AND ".SQL_QUOTE_STATUS.".type='SCAN' AND  ".SQL_QUOTE_STATUS.".status='SUCCESS' ORDER BY ".SQL_QUOTE_SCANNINGS.".date DESC,".SQL_QUOTE_SCANNINGS.".time DESC LIMIT 1";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_ID_NOT_FOUND");
      return false;
   }

   return $this->dbh->GetFieldValue("site_id");;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuoteStatusBySiteID
//
// [DESCRIPTION]:   Read data from Statuss table and put it into an array variable
//
// [PARAMETERS]:    $siteID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetAllQuoteStatusBySiteID($siteID='')
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_SITE_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_STATUS." WHERE site_id='$siteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_SITE_ID_NOT_FOUND");
      return false;
   }

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index]["id"]             = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["log_id"]         = $this->dbh->GetFieldValue("log_id");
      $arrayResult[$index]["site_id"]        = $this->dbh->GetFieldValue("site_id");
      $arrayResult[$index]["status"]         = $this->dbh->GetFieldValue("status");
      $arrayResult[$index]["type"]           = $this->dbh->GetFieldValue("type");
   }

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuoteStatusByStatus
//
// [DESCRIPTION]:   Read data from Statuss table and put it into an array variable
//
// [PARAMETERS]:    $status
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuoteStatusByStatus($status="")
{
   if(empty($status))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_STATUS_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_STATUS." WHERE status='$status'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_SITE_ID_NOT_FOUND");
      return false;
   }

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index]["id"]             = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["log_id"]         = $this->dbh->GetFieldValue("log_id");
      $arrayResult[$index]["site_id"]        = $this->dbh->GetFieldValue("site_id");
      $arrayResult[$index]["status"]         = $this->dbh->GetFieldValue("status");
      $arrayResult[$index]["type"]           = $this->dbh->GetFieldValue("type");
   }

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuoteStatusByLogID
//
// [DESCRIPTION]:   Read data from Statuss table and put it into an array variable
//
// [PARAMETERS]:    $logID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuoteStatusByLogID($logID='')
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_LOG_ID_FIELD");
      return false;
   }

   // get all quote status entries only by normal and scan type !!!
   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_STATUS." WHERE log_id='$logID' AND type<>'DEBUG'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_LOG_ID_NOT_FOUND");
      return false;
   }

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;

      $arrayResult[$index]["id"]      = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["log_id"]  = $this->dbh->GetFieldValue("log_id");
      $arrayResult[$index]["site_id"] = $this->dbh->GetFieldValue("site_id");
      $arrayResult[$index]["status"]  = $this->dbh->GetFieldValue("status");
      $arrayResult[$index]["type"]    = $this->dbh->GetFieldValue("type");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuoteStatusByLogID
//
// [DESCRIPTION]:   Read data from Statuss table and put it into an array variable
//
// [PARAMETERS]:    $logID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetWaitingQuotesNumberByLogID($logID='')
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_LOG_ID_FIELD");
      return false;
   }

   // get all quote status entries only by normal and scan type !!!
   $this->lastSQLCMD = "SELECT if((SELECT count(*) from ".SQL_QUOTE_STATUS." where log_id=$logID AND status != 'SKIPPED')=0, -1,count(*)) as total FROM ".SQL_QUOTE_STATUS." WHERE log_id=$logID AND status='WAITING' ";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_LOG_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_LOG_ID_NOT_FOUND");
      return false;
   }

   return $this->dbh->GetFieldValue("total");
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCompleteQuoteStatusByLogID
//
// [DESCRIPTION]:   Read data from Statuss table and put it into an array variable
//
// [PARAMETERS]:    $logID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCompleteQuoteStatusByLogID($logID='')
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_LOG_ID_FIELD");
      return false;
   }

   // get all quote status entries only by normal and scan type !!!
   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_STATUS." WHERE log_id='$logID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_LOG_ID_NOT_FOUND");
      return false;
   }

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;

      $arrayResult[$index]["id"]      = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["log_id"]  = $this->dbh->GetFieldValue("log_id");
      $arrayResult[$index]["site_id"] = $this->dbh->GetFieldValue("site_id");
      $arrayResult[$index]["status"]  = $this->dbh->GetFieldValue("status");
      $arrayResult[$index]["type"]    = $this->dbh->GetFieldValue("type");
   }

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuotesStatusByInterval
//
// [DESCRIPTION]:   Read data from Status table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserID, $hostIp, $startDate,$endDate,$startTime,$endTime
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuotesStatusByInterval($quoteUserID='', $hostIp='',$quoteTypeID='', $startDate='',$endDate='',$startTime='',$endTime='')
{
   if(! empty($quoteUserID))
   {
      if(! preg_match("/^\d+$/", $quoteUserID))
      {
         $this->strERR = GetErrorString("INVALID_LOG_QUOTE_USER_ID_FIELD");
         return false;
      }
   }

   if(! empty($hostIp))
   {
      if(! preg_match("/\d{3}\.\d{3}\.\d{3}\.\d{3}/",$hostIp))
      {
         $this->strERR = GetErrorString("INVALID_LOG_HOST_IP_FIELD");
         return false;
      }
   }

   if(! empty($quoteTypeID))
   {
      if(! preg_match("/^\d+$/",$quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_ID_FIELD");
         return false;
      }
   }

   if(! empty($startDate))
      if(! $startDate = $this->StdToMysqlDate($startDate))
         return false;

   if(! empty($endDate))
      if(! $endDate = $this->StdToMysqlDate($endDate))
         return false;

   if(! empty($startTime))
   {
      if(! preg_match("/^\d{2}\:\d{2}\:\d{2}$/", $startTime))
      {
         $this->strERR = GetErrorString("INVALID_START_TIME_FIELD");
         return false;
      }
   }

   if(! empty($endTime))
   {
      if(! preg_match("/^\d{2}\:\d{2}\:\d{2}$/", $endTime))
      {
         $this->strERR = GetErrorString("INVALID_END_TIME_FIELD");
         return false;
      }
   }

   // we have to check if the startDate and endDate are not the same date and remorting the select mysql
   //check dates

   list($startYear, $startMonth, $startDay) = split("-", $startDate);
   list($endYear, $endMonth, $endDay)       = split("-", $endDate);

   $diffTimeStamp  = mktime(0,0,0,$endMonth, $endDay, $endYear) - mktime(0,0,0,$startMonth, $startDay, $startYear);

   $case = 0;
   // the same date
   if($diffTimeStamp  == 0 )
      $case = 1;
   else if($diffTimeStamp == 24*60*60 ) // the diff is less or equal than 1 day
      $case = 2;
   else // the diff is more than 1 day
      $case = 3;


   $this->lastSQLCMD = "SELECT l.quote_user_id,l.host_ip,l.filename,l.date,l.time,qs.* FROM ".SQL_LOGS." l,".SQL_QUOTE_STATUS." qs,".SQL_QZQUOTES." qq";

   switch($case)
   {
      case '1':

         if($startDate || $endDate || $startTime || $endTime)
            $this->lastSQLCMD .=" WHERE 1";

         if($startDate)
            $this->lastSQLCMD .= " AND l.date='$startDate' ";
         else if($endDate)
            $this->lastSQLCMD .= " AND l.date='$endDate' ";

         if($startTime)
            $this->lastSQLCMD .= " AND l.time>='$startTime' ";

         if($endTime)
            $this->lastSQLCMD .= " AND l.time<'$endTime' ";
         break;

      case '2':
         if($startDate || $endDate || $startTime || $endTime)
            $this->lastSQLCMD .=" WHERE 1";

         if($startDate)
            $this->lastSQLCMD .= " AND ((l.date='$startDate' ";

         if($startTime)
            $this->lastSQLCMD .= " AND l.time>='$startTime') ";
         else
            $this->lastSQLCMD .= " ) ";

         if($endDate)
            $this->lastSQLCMD .= " OR (l.date='$endDate' ";

         if($endTime)
            $this->lastSQLCMD .= " AND l.time<'$endTime')) ";
         else
            $this->lastSQLCMD .= " )) ";

         break;

      case '3':
         if($startDate || $endDate || $startTime || $endTime)
            $this->lastSQLCMD .=" WHERE 1";

         if($startDate)
            $this->lastSQLCMD .= " AND ((l.date='$startDate' ";

         if($startTime)
            $this->lastSQLCMD .= " AND l.time>='$startTime') ";
         else
            $this->lastSQLCMD .= " ) ";

         if($endDate)
            $this->lastSQLCMD .= " OR (l.date='$endDate' ";

         if($endTime)
            $this->lastSQLCMD .= " AND l.time<'$endTime') ";
         else
            $this->lastSQLCMD .= " ) ";

         $endDateInter   = date("Y-m-d", mktime(0,0,0,$endMonth, $endDay - 1, $endYear));
         $startDateInter = date("Y-m-d", mktime(0,0,0,$startMonth, $startDay + 1, $startYear));

         if($startDate)
            $this->lastSQLCMD .= " OR (l.date>='$startDateInter' ";
         else
            $this->lastSQLCMD .= " ) ";

         if($endDate)
            $this->lastSQLCMD .= " AND l.date<='$endDateInter')) ";
         else
            $this->lastSQLCMD .= " )) ";

         break;
   }// end switch($case)

   if(! empty($quoteUserID))
      $this->lastSQLCMD .=" AND l.quote_user_id='".$quoteUserID."' ";

   if(! empty($hostIp))
      $this->lastSQLCMD .=" AND l.host_ip='".$hostIp."' ";

   if(! empty($quoteTypeID))
      $this->lastSQLCMD .=" AND qq.quote_type_id='".$quoteTypeID."' ";

   $this->lastSQLCMD .= " AND l.id=qs.log_id AND qq.log_id = l.id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("LOG_ID_NOT_FOUND");
      return false;
   }

//    $index = 0;
   while($this->dbh->MoveNext())
   {

      $logID          = $this->dbh->GetFieldValue("log_id");
      $quoteStatusID  = $this->dbh->GetFieldValue("id");

      $arrayResult[$logID][$quoteStatusID]['time']             = $this->dbh->GetFieldValue("time");
      $arrayResult[$logID][$quoteStatusID]['date']             = $this->dbh->GetFieldValue("date");
      $arrayResult[$logID][$quoteStatusID]['status']           = $this->dbh->GetFieldValue("status");
      $arrayResult[$logID][$quoteStatusID]['type']             = $this->dbh->GetFieldValue("type");
      $arrayResult[$logID][$quoteStatusID]['filename']         = $this->dbh->GetFieldValue("filename");
      $arrayResult[$logID][$quoteStatusID]['site_id']          = $this->dbh->GetFieldValue("site_id");
      $arrayResult[$logID][$quoteStatusID]['host_ip']          = $this->dbh->GetFieldValue("host_ip");
   }

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuoteStatusBySiteIDAndStatus
//
// [DESCRIPTION]:   Read data from Statuss table and put it into an array variable
//
// [PARAMETERS]:    $siteID,$status
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetAllQuoteStatusBySiteIDAndStatus($siteID='',$status='', $checkQuotes=false)
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITEID_FIELD");
      return false;
   }

   if(empty($status))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_STATUS_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT qs.* FROM ".SQL_LOGS." l, ".SQL_QUOTE_STATUS." qs WHERE l.id=qs.log_id AND qs.site_id='$siteID' AND qs.status='$status' ";

   if($checkQuotes)
   {
      $timeStamp = mktime() - 10*60;
      $date = date("Y-m-d", $timeStamp);
      $time = date("H:i:s", $timeStamp);

      $this->lastSQLCMD .= " AND (l.date<'$date' OR (l.date='$date' AND l.time<='$time')) ";
   }

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITEID_OR_STATUS_NOT_FOUND");
      return false;
   }

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index]["id"]             = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["log_id"]         = $this->dbh->GetFieldValue("log_id");
      $arrayResult[$index]["site_id"]        = $this->dbh->GetFieldValue("site_id");
      $arrayResult[$index]["status"]         = $this->dbh->GetFieldValue("status");
      $arrayResult[$index]["type"]           = $this->dbh->GetFieldValue("type");

   }

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuoteStatus
//
// [DESCRIPTION]:   Read data from Statuss table and put it into an array variable
//
// [PARAMETERS]:    $siteID, $startDate, $endDate
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-14
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuoteStatus($siteID='', $quoteTypeID='', $startDate='', $endDate='')
{

 if(! empty($siteID))
   {
      if(! preg_match("/^\d+$/", $siteID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_SITE_ID_FIELD");
         return false;
      }
   }

 if(! empty($quoteTypeID))
   {
      if(! preg_match("/^\d+$/", $quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_QUOTE_TYPE_ID_FIELD");
         return false;
      }
   }

   if(! empty($startDate))
      if(! $startDate = $this->StdToMysqlDate($startDate))
         return false;

   if(! empty($endDate))
      if(! $endDate = $this->StdToMysqlDate($endDate))
         return false;

   $this->lastSQLCMD = "SELECT l.date,l.time,qs.* FROM  ".SQL_LOGS." l,".SQL_QUOTE_STATUS." qs, ".SQL_QZQUOTES." qq";

   if($quoteTypeID)
      $this->lastSQLCMD .=" ";

   if($siteID || $quoteTypeID|| $startDate || $endDate)
      $this->lastSQLCMD .=" WHERE 1";

   if($siteID)
      $this->lastSQLCMD .= " AND qs.site_id='$siteID'";

   if($quoteTypeID)
      $this->lastSQLCMD .= " AND qq.quote_type_id='$quoteTypeID'";

   if($startDate)
      $this->lastSQLCMD .= " AND l.date>='$startDate' ";

   if($endDate)
      $this->lastSQLCMD .= " AND l.date<='$endDate' ";

      $this->lastSQLCMD .= " AND l.id=qs.log_id AND l.id = qq.log_id";


   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $logID          = $this->dbh->GetFieldValue("log_id");
      $quoteStatusID  = $this->dbh->GetFieldValue("id");

      $arrayResult[$logID][$quoteStatusID]["site_id"]    = $this->dbh->GetFieldValue("site_id");
      $arrayResult[$logID][$quoteStatusID]["status"]     = $this->dbh->GetFieldValue("status");
      $arrayResult[$logID][$quoteStatusID]["type"]       = $this->dbh->GetFieldValue("type");
      $arrayResult[$logID][$quoteStatusID]["date"]       = $this->dbh->GetFieldValue("date");
      $arrayResult[$logID][$quoteStatusID]["time"]       = $this->dbh->GetFieldValue("time");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLastSiteSuccess
//
// [DESCRIPTION]:   Read data from Statuss table and put it into a variable
//
// [PARAMETERS]:    $siteID
//
// [RETURN VALUE]:  int|false
//
// [CREATED BY]:    Sturza Ciprian (cipi@acrux.biz) 2010-04-20
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLastSiteSuccess($siteID='')
{
   if(! empty($siteID))
   {
      if(! preg_match("/^\d+$/", $siteID))
      {
         $this->strERR = GetErrorString("INVALID_SITE_ID_FIELD");
         return false;
      }
  }

    $this->lastSQLCMD = "SELECT MAX(qs.id) AS last_id FROM ".SQL_QUOTE_STATUS." qs LEFT JOIN ".SQL_LOGS." l ON l.id=qs.log_id WHERE qs.status <> 'SKIPPED' AND qs.site_id='$siteID' AND l.host_ip <> '81.196.65.167' AND l.host_ip <> '188.220.10.35'";

   //$this->lastSQLCMD = "SELECT MAX(qs.id) AS last_id FROM ".SQL_QUOTE_STATUS." qs WHERE qs.status <> 'SKIPPED' AND qs.site_id='$siteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_ID_NOT_FOUND");
      return false;
   }

   return $this->dbh->GetFieldValue("last_id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSiteSuccessLeads
//
// [DESCRIPTION]:   Count the number of success leads sent per company in the current month
//                  The count exclude acrux and seopa IP's
//                  We take only unique users
//
// [PARAMETERS]:    $siteID
//
// [RETURN VALUE]:  int|false
//
// [CREATED BY]:    Sturza Ciprian (cipi@acrux.biz) 2010-04-21
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSiteSuccessLeads($siteID='')
{
   if(! empty($siteID))
   {
      if(! preg_match("/^\d+$/", $siteID))
      {
         $this->strERR = GetErrorString("INVALID_SITE_ID_FIELD");
         return false;
      }
   }

   $currentDate = date("Y-m")."-01";

   $this->lastSQLCMD = "SELECT COUNT(DISTINCT l.quote_user_id) AS leads FROM ".SQL_QUOTE_STATUS." qs LEFT JOIN ".SQL_LOGS." l ON l.id=qs.log_id WHERE site_id='$siteID' AND qs.status='SUCCESS' AND l.host_ip <> '81.196.65.167' AND l.host_ip <> '188.220.10.35' AND l.date>='$currentDate'";
   
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATUS_ID_NOT_FOUND");
      return false;
   }

   return $this->dbh->GetFieldValue("leads");
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLastQuoteStatusByQuoteUserID
//
// [DESCRIPTION]:   Read data from Statuss table and put it into a variable
//
// [PARAMETERS]:    $quoteUserID, $brokerID
//
// [RETURN VALUE]:  int|false
//
// [CREATED BY]:    Calin Goia (calin@acrux.biz) 2010-04-27
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLastQuoteStatusByQuoteUserID($quoteUserID='', $brokerID='')
{
   if(! empty($quoteUserID))
   {
      if(! preg_match("/^\d+$/", $quoteUserID))
      {
         $this->strERR = GetErrorString("INVALID_SITE_ID_FIELD");
         return false;
      }
  }
  
  $startDate = date("Y-m")."-01";
  $endDate   = date("Y-m")."-31";

   $this->lastSQLCMD = "SELECT l.id AS logID FROM ".SQL_SITES." s, ".SQL_QUOTE_STATUS." qs LEFT JOIN ".SQL_LOGS." l ON qs.log_id = l.id WHERE l.quote_user_id = '$quoteUserID' AND s.broker_id = '$brokerID' AND s.id = qs.site_id AND l.date BETWEEN '$startDate' AND '$endDate' order by l.id desc limit 1";
   
   //$this->lastSQLCMD = "SELECT l.id AS log_id FROM ".SQL_LOGS." l WHERE l.quote_user_id = '$quoteUserID' AND l.date BETWEEN '$startDate' AND '$endDate' order by l.id desc limit 1";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTE_SITE_ID_NOT_FOUND");
      return false;
   }
   
   $this->lastSQLCMD = "SELECT site_id FROM ".SQL_QUOTE_STATUS." qs WHERE qs.log_id = '".$this->dbh->GetFieldValue("logID")."'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_SITE_ID_NOT_FOUND");
      return false;
   }
   
   $arrayResult = array();
   while($this->dbh->MoveNext())
   {
      $arrayResult[] = $this->dbh->GetFieldValue("site_id");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ExistQuoteStatusSite
//
// [DESCRIPTION]:   check if exist the quote quote status by site into quote status table
//
// [PARAMETERS]:    $logID='', $siteID=''
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-14
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ExistQuoteStatusSite($logID='', $siteID='')
{

   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_LOG_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_SITE_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_STATUS." WHERE site_id='$siteID' AND log_id='$logID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetNumberOutboundsSent
//
// [DESCRIPTION]:   Count the number of outbounds leads sent per company in the current day/month
//                  The count exclude acrux and seopa IP's
//
// [PARAMETERS]:    $outboundConfigID,$period
//
// [RETURN VALUE]:  int|false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2010-04-27
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetNumberOutboundsSent($outboundConfigID='',$period='')
{
   if(! empty($outboundConfigID))
   {
      if(! preg_match("/^\d+$/", $outboundConfigID))
      {
         $this->strERR = GetErrorString("INVALID_OUTBOUND_CONFIG_ID_FIELD");
         return false;
      }
   }

   if(empty($period))
   {
      $this->strERR = GetErrorString("INVALID_PERIOD_FIELD");
      return false;
   }

   switch($period)
   {
/*
      case 'day'  :
                  $currentDayDate = date("Y-m-d");

                  $this->lastSQLCMD = "SELECT COUNT(DISTINCT l.quote_user_id) AS leads FROM ".SQL_QUOTE_STATUS." qs LEFT JOIN ".SQL_LOGS." l ON l.id=qs.log_id WHERE site_id='$siteID' and qs.status = 'SUCCESS' AND l.host_ip <> '81.196.65.167' AND l.host_ip <> '188.220.10.35' AND l.date = '$currentDayDate'";

                  break;

      case 'week'  :
                  $currentDate = date("Y-m-d");
                  $currentYear = date("Y");
                  $currentWeek = date("W");

                  $this->lastSQLCMD = "SELECT COUNT(DISTINCT l.quote_user_id) AS leads FROM ".SQL_QUOTE_STATUS." qs LEFT JOIN ".SQL_LOGS." l ON l.id=qs.log_id WHERE site_id='$siteID' and qs.status = 'SUCCESS' AND l.host_ip <> '81.196.65.167' AND l.host_ip <> '188.220.10.35' AND    YEAR(l.date) = '$currentYear' AND WEEK(l.date) = '$currentWeek' ";

                  break;

      case 'month':
                  $currentMonthDate = date("Y-m")."-01";

                  $this->lastSQLCMD = "SELECT COUNT(DISTINCT l.quote_user_id) AS leads FROM ".SQL_QUOTE_STATUS." qs LEFT JOIN ".SQL_LOGS." l ON l.id=qs.log_id WHERE site_id='$siteID' and qs.status = 'SUCCESS' AND l.host_ip <> '81.196.65.167' AND l.host_ip <> '188.220.10.35' AND l.date>='$currentMonthDate'";

                  break;
*/
      case 'all':
                  // all
                  $this->lastSQLCMD = "SELECT COUNT(DISTINCT l.quote_user_id) AS leads FROM outbounds o LEFT JOIN ".SQL_LOGS." l ON l.id=o.log_id WHERE o.outbound_config_id='$outboundConfigID' and o.sent_status = '1' AND l.host_ip <> '81.196.65.167' AND l.host_ip <> '188.220.10.35'";

                  break;
   }

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("OUTBOUND_CONFIG_ID_NOT_FOUND");
      return false;
   }

   return $this->dbh->GetFieldValue("leads");
}

} // end of CQuoteStatus class
?>

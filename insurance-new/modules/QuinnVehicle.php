<?php
/*****************************************************************************/
/*                                                                           */
/*  CQuinnVehicle class interface                                            */
/*                                                                           */
/*  (C) 2005 Velnic Daniel (dan@acrux.biz)                                   */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuinnVehicle
//
// [DESCRIPTION]:  CQuinnVehicle class interface
//
// [FUNCTIONS]:   true   | false AssertQuinnVehicle($abiCode=0, $make_description='', $model_description='', $veh_series='',
// 												    $veh_capacity='', $from_year=0, $to_year='',$doors=0, $veh_type='', 
//												    $veh_fuel='', $veh_transmission='', $veh_abi_grp='', 
//												    $veh_grp_suffix='', $veh_security='', $veh_user_class='', 
//												    $veh_user_term1='', $veh_user_term2='', $veh_user_term3='', 
//												    $veh_user_defined='', $veh_eff_date='',$veh_model_type='')
//                int    | false AddQuinnVehicle($abiCode=0, $make_description='', $model_description='', $veh_series='',
// 												    $veh_capacity='', $from_year=0, $to_year='',$doors=0, $veh_type='', 
//												    $veh_fuel='', $veh_transmission='', $veh_abi_grp='', 
//												    $veh_grp_suffix='', $veh_security='', $veh_user_class='', 
//												    $veh_user_term1='', $veh_user_term2='', $veh_user_term3='', 
//												    $veh_user_defined='', $veh_eff_date='',$veh_model_type='')
//                true   | false UpdateQuinnVehicle($quinnVehicleID='',$abiCode=0, $make_description='', $model_description='',
// 												    $veh_series='',$veh_capacity='', $from_year=0, $to_year='',$doors=0, 
// 												    $veh_type='', $veh_fuel='', $veh_transmission='', $veh_abi_grp='', 
// 												    $veh_grp_suffix='', $veh_security='', $veh_user_class='', 
//												    $veh_user_term1='', $veh_user_term2='', $veh_user_term3='', 
//												    $veh_user_defined='', $veh_eff_date='',$veh_model_type='')
//                true   | false DeleteQuinnVehicle($quinnVehicleID='')
//                array  | false GetQuinnVehicleByID($quinnVehicleID='')
//                int    | false GetQuinnVehicleByAbiCode($quinnAbiCode='')
//                array  | false GetAllQuinnVehicle()
//
//                Close();
//                GetError();
//                ShowError();
//
// [CREATED BY]:  Velnic Daniel (dan@acrux.biz)
//
// [MODIFIED]:    - [programmer (email) date]
//                  [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CQuinnVehicle
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuinnVehicle
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQuinnVehicle($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuinnVehicle
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    $abiCode=0, $make_description='', $model_description='', $veh_series='',
//				    $veh_capacity='', $from_year=0, $to_year='',$doors=0, $veh_type='', 
//					$veh_fuel='', $veh_transmission='', $veh_abi_grp='', 
//					$veh_grp_suffix='', $veh_security='', $veh_user_class='', 
//					$veh_user_term1='', $veh_user_term2='', $veh_user_term3='', 
//					$veh_user_defined='', $veh_eff_date='',$veh_model_type=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertQuoteUser($abiCode=0, $make_description='', $model_description='', $veh_series='',
						 $veh_capacity='', $from_year=0, $to_year='',$doors=0, $veh_type='', 
						 $veh_fuel='', $veh_transmission='', $veh_abi_grp='', 
						 $veh_grp_suffix='', $veh_security='', $veh_user_class='', 
						 $veh_user_term1='', $veh_user_term2='', $veh_user_term3='', 
						 $veh_user_defined='', $veh_eff_date='',$veh_model_type='')
{
   $this->strERR = '';

   if(! preg_match('/[\d]{8}/i',$abiCode))
      $this->strERR .= GetErrorString("INVALID_ABI_CODE")."\n";     

   if(empty($make_description))
      $this->strERR .= GetErrorString("INVALID_MAKE_DESCRIPTION")."\n";
  
   if(empty($model_description))
      $this->strERR .= GetErrorString("INVALID_MODEL_DESCRIPTION")."\n";      

   if(! preg_match('/\d+/i',$veh_capacity))
      $this->strERR .= GetErrorString("INVALID_MODEL_DESCRIPTION")."\n";      
      
   if(! preg_match('/\d+/i',$from_year))
      $this->strERR .= GetErrorString("INVALID_FROM_YEAR")."\n";   
      
   if(! preg_match('/[\d-]+/i',$to_year))
      $this->strERR .= GetErrorString("INVALID_TO_YEAR")."\n";
      
   if(empty($doors))
      $this->strERR .= GetErrorString("INVALID_DOORS")."\n";

   if(empty($veh_type))
      $this->strERR .= GetErrorString("INVALID_VEHICLE_TYPE")."\n";

   if(empty($veh_fuel))
      $this->strERR .= GetErrorString("INVALID_VEHICLE_FUEL")."\n";

   if(empty($veh_transmission))
      $this->strERR .= GetErrorString("INVALID_VEHICLE_TRANSMISSION")."\n";      

   if(! preg_match('/[\d]+/i',$veh_abi_grp))
      $this->strERR .= GetErrorString("INVALID_ABI_GROUP")."\n";

   if(! preg_match('/[\d]+/i',$veh_eff_date))
      $this->strERR .= GetErrorString("INVALID_VEHICLE_EFFECTIVE_DATE")."\n";

   if(empty($veh_model_type))
      $this->strERR .= GetErrorString("INVALID_VECHICLE_MODEL_TYPE")."\n";


   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuinnVehicle
//
// [DESCRIPTION]:   Check if exist a car and add if not exist into QUINN_VEHICLE table
//
// [PARAMETERS]:    $abiCode=0, $make_description='', $model_description='', $veh_series='',
//				    $veh_capacity='', $from_year=0, $to_year='',$doors=0, $veh_type='', 
//					$veh_fuel='', $veh_transmission='', $veh_abi_grp='', 
//					$veh_grp_suffix='', $veh_security='', $veh_user_class='', 
//					$veh_user_term1='', $veh_user_term2='', $veh_user_term3='', 
//					$veh_user_defined='', $veh_eff_date='',$veh_model_type=''
//
// [RETURN VALUE]:  $quinnVehicleID | false
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuinnVehicle($abiCode=0, $make_description='', $model_description='', $veh_series='',$veh_capacity='', $from_year=0,						 $to_year='',$doors=0, $veh_type='', $veh_fuel='', $veh_transmission='', $veh_abi_grp='', 
						 $veh_grp_suffix='', $veh_security='', $veh_user_class='', $veh_user_term1='', $veh_user_term2='', 
						 $veh_user_term3='', $veh_user_defined='', $veh_eff_date='',$veh_model_type='')
{

   if(! $this->AssertQuoteUser($abiCode, $make_description, $model_description, $veh_series, $veh_capacity, $from_year, $to_year,$doors, $veh_type, $veh_fuel, $veh_transmission, $veh_abi_grp, $veh_grp_suffix, $veh_security, $veh_user_class, $veh_user_term1, $veh_user_term2, $veh_user_term3, $veh_user_defined, $veh_eff_date,$veh_model_type))
      return false;

   
   $lastSqlCmd = "SELECT id FROM ".SQL_QUINN_VEHICLE." WHERE abiCode='$abiCode'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   if($this->dbh->GetRows())
   {
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      $quinnVehicleID = $this->dbh->GetFieldValue("id");
      
      return $quinnVehicleID;
   }

   $lastSqlCmd = "INSERT INTO ".SQL_QUINN_VEHICLE." (abiCode, make_description, model_description, veh_series, veh_capacity, from_year, to_year, doors, veh_type, veh_fuel, veh_transmission, veh_abi_grp, veh_grp_suffix, veh_security, veh_user_class, veh_user_term1, veh_user_term2, veh_user_term3, veh_user_defined, veh_eff_date, veh_model_type) VALUES ('$abiCode', '".addslashes($make_description)."', '".addslashes($model_description)."', '$veh_series', '$veh_capacity', '$from_year', '$to_year','$doors', '$veh_type', '$veh_fuel', '$veh_transmission', '$veh_abi_grp', '$veh_grp_suffix', '$veh_security', '$veh_user_class', '$veh_user_term1', '$veh_user_term2', '$veh_user_term3', '$veh_user_defined', '$veh_eff_date','$veh_model_type');";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $lastSqlCmd = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQuinnVehicle
//
// [DESCRIPTION]:   Update a user data from the quote_users table
//
// [PARAMETERS]:    $quinnVehicleID='', $abiCode=0, $make_description='', $model_description='', $veh_series='',
//				    $veh_capacity='', $from_year=0, $to_year='',$doors=0, $veh_type='', 
//					$veh_fuel='', $veh_transmission='', $veh_abi_grp='', 
//					$veh_grp_suffix='', $veh_security='', $veh_user_class='', 
//					$veh_user_term1='', $veh_user_term2='', $veh_user_term3='', 
//					$veh_user_defined='', $veh_eff_date='',$veh_model_type=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQuinnVehicle($quinnVehicleID='',$abiCode=0, $make_description='', $model_description='', $veh_series='',$veh_capacity='', $from_year=0, $to_year='',$doors=0, $veh_type='', $veh_fuel='', $veh_transmission='', $veh_abi_grp='',  $veh_grp_suffix='', $veh_security='', $veh_user_class='', $veh_user_term1='', $veh_user_term2='', $veh_user_term3='',  $veh_user_defined='', $veh_eff_date='',$veh_model_type='')
{

   if(! preg_match("/^\d+$/", $quinnVehicleID))
   {
         $this->strERR = GetErrorString("INVALID_QUINN_VEHICLE_ID")."\n";
      return false;
   }

   if(! $this->AssertQuoteUser($abiCode, $make_description, $model_description, $veh_series, $veh_capacity, $from_year, $to_year,$doors, $veh_type, $veh_fuel, $veh_transmission, $veh_abi_grp, $veh_grp_suffix, $veh_security, $veh_user_class, $veh_user_term1, $veh_user_term2, $veh_user_term3, $veh_user_defined, $veh_eff_date,$veh_model_type))
      return false;

   $lastSqlCmd = "SELECT id FROM ".SQL_QUINN_VEHICLE." WHERE id='$quinnVehicleID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUINN_VEHICLE_ID_NOT_FOUND");
      return false;
   }

   $lastSqlCmd = "UPDATE ".SQL_QUINN_VEHICLE." SET abiCode='$abiCode', make_description='".addslashes($make_description)."', model_description='".addslashes($model_description)."', veh_series='$veh_series', veh_capacity='$veh_capacity', from_year='$from_year', to_year='$to_year', doors='$doors', veh_type='$veh_type', veh_fuel='$veh_fuel', veh_transmission='$veh_transmission', veh_abi_grp='$veh_abi_grp', veh_grp_suffix='$veh_grp_suffix', veh_security='$veh_security', veh_user_class='$veh_user_class', veh_user_term1='$veh_user_term1', veh_user_term2='$veh_user_term2', veh_user_term3='$veh_user_term3',  veh_user_defined='$veh_user_defined', veh_eff_date='$veh_eff_date', veh_model_type='$veh_model_type' WHERE id='$quinnVehicleID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
/////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQuinnVehicle
//
// [DESCRIPTION]:   Delete an entry from the QUINN_VEHICLE table
//
// [PARAMETERS]:    $quinnVehicleID
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteQuinnVehicle($quinnVehicleID='')
{
   if(! preg_match("/^\d+$/", $quinnVehicleID))
   {
      $this->strERR = GetErrorString("INVALID_QUINN_VEHICLE_ID");
      return false;
   }

   $lastSqlCmd = "SELECT id FROM ".SQL_QUINN_VEHICLE." WHERE id='$quinnVehicleID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUINN_VEHICEL_ID_NOT_FOUND");
      return false;
   }

   $lastSqlCmd = "DELETE FROM ".SQL_QUINN_VEHICLE." WHERE id='$quinnVehicleID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuinnVehicleByID
//
// [DESCRIPTION]:   Read data of a car from QUINN_VEHICLE table and put it into an array variable
//
// [PARAMETERS]:    $quinnVehicleID
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Velnic Daniel  (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuinnVehicleByID($quinnVehicleID='')
{
   if(! preg_match("/^\d+$/", $quinnVehicleID))
   {
      $this->strERR = GetErrorString("INVALID_QUINN_VEHICEL_ID");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM ".SQL_QUINN_VEHICLE." WHERE id='$quoteUserID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUINN_VEHICLE_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   preg_match("/([\d]+)/",$this->dbh->GetFieldValue("doors"),$matche);
   $doors       = $matche[1];
   $arrayResult = array();

   $arrayResult["id"]                = trim($this->dbh->GetFieldValue("id"));
   $arrayResult["abiCode"]           = trim($this->dbh->GetFieldValue("abiCode"));
   $arrayResult["make_description"]  = trim(stripslashes($this->dbh->GetFieldValue("make_description")));
   $arrayResult["model_description"] = trim(stripslashes($this->dbh->GetFieldValue("model_description")));
   $arrayResult["veh_series"]        = trim($this->dbh->GetFieldValue("veh_series"));
   $arrayResult["veh_capacity"]      = trim($this->dbh->GetFieldValue("veh_capacity"));
   $arrayResult["from_year"]         = trim($this->dbh->GetFieldValue("from_year"));
   $arrayResult["to_year"]           = trim($this->dbh->GetFieldValue("to_year"));
   $arrayResult["doors"]             = trim($doors);
   $arrayResult["veh_type"]          = trim($this->dbh->GetFieldValue("veh_type"));
   $arrayResult["veh_fuel"]          = trim($this->dbh->GetFieldValue("veh_fuel"));
   $arrayResult["veh_transmission"]  = trim($this->dbh->GetFieldValue("veh_transmission"));
   $arrayResult["veh_abi_grp"]       = trim($this->dbh->GetFieldValue("veh_abi_grp"));
   $arrayResult["veh_grp_suffix"]    = trim($this->dbh->GetFieldValue("veh_grp_suffix"));
   $arrayResult["veh_security"]      = trim($this->dbh->GetFieldValue("veh_security"));
   $arrayResult["veh_user_class"]    = trim($this->dbh->GetFieldValue("veh_user_class"));
   $arrayResult["veh_user_term1"]    = trim($this->dbh->GetFieldValue("veh_user_term1"));
   $arrayResult["veh_user_term2"]    = trim($this->dbh->GetFieldValue("veh_user_term2"));
   $arrayResult["veh_user_term3"]    = trim($this->dbh->GetFieldValue("veh_user_term3"));
   $arrayResult["veh_user_defined"]  = trim($this->dbh->GetFieldValue("veh_user_defined"));
   $arrayResult["veh_eff_date"]      = trim($this->dbh->GetFieldValue("veh_eff_date"));
   $arrayResult["veh_model_type"]    = trim($this->dbh->GetFieldValue("veh_model_type"));   

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuinnVehicleByAbiCode
//
// [DESCRIPTION]:   Read data of a car from QUINN_VEHICLE table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Velnic Daniel  (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuinnVehicleByAbiCode($quinnAbiCode='')
{
   if(! preg_match("/^\d+$/", $quinnAbiCode))
   {
      $this->strERR = GetErrorString("INVALID_QUINN_VEHICEL_ID");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM ".SQL_QUINN_VEHICLE." WHERE abiCode='$quinnAbiCode'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUINN_VEHICLE_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   preg_match("/([\d]+)/",$this->dbh->GetFieldValue("doors"),$matche);
   $doors       = $matche[1];
   $arrayResult = array();
   
   $arrayResult["id"]                = trim($this->dbh->GetFieldValue("id"));
   $arrayResult["abiCode"]           = trim($this->dbh->GetFieldValue("abiCode"));
   $arrayResult["make_description"]  = trim(stripslashes($this->dbh->GetFieldValue("make_description")));
   $arrayResult["model_description"] = trim(stripslashes($this->dbh->GetFieldValue("model_description")));
   $arrayResult["veh_series"]        = trim($this->dbh->GetFieldValue("veh_series"));
   $arrayResult["veh_capacity"]      = trim($this->dbh->GetFieldValue("veh_capacity"));
   $arrayResult["from_year"]         = trim($this->dbh->GetFieldValue("from_year"));
   $arrayResult["to_year"]           = trim($this->dbh->GetFieldValue("to_year"));
   $arrayResult["doors"]             = trim($doors);
   $arrayResult["veh_type"]          = trim($this->dbh->GetFieldValue("veh_type"));
   $arrayResult["veh_fuel"]          = trim($this->dbh->GetFieldValue("veh_fuel"));
   $arrayResult["veh_transmission"]  = trim($this->dbh->GetFieldValue("veh_transmission"));
   $arrayResult["veh_abi_grp"]       = trim($this->dbh->GetFieldValue("veh_abi_grp"));
   $arrayResult["veh_grp_suffix"]    = trim($this->dbh->GetFieldValue("veh_grp_suffix"));
   $arrayResult["veh_security"]      = trim($this->dbh->GetFieldValue("veh_security"));
   $arrayResult["veh_user_class"]    = trim($this->dbh->GetFieldValue("veh_user_class"));
   $arrayResult["veh_user_term1"]    = trim($this->dbh->GetFieldValue("veh_user_term1"));
   $arrayResult["veh_user_term2"]    = trim($this->dbh->GetFieldValue("veh_user_term2"));
   $arrayResult["veh_user_term3"]    = trim($this->dbh->GetFieldValue("veh_user_term3"));
   $arrayResult["veh_user_defined"]  = trim($this->dbh->GetFieldValue("veh_user_defined"));
   $arrayResult["veh_eff_date"]      = trim($this->dbh->GetFieldValue("veh_eff_date"));
   $arrayResult["veh_model_type"]    = trim($this->dbh->GetFieldValue("veh_model_type"));
   

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuinnVehicleByAbiCodeNew
//
// [DESCRIPTION]:   Read data of a car from QUINN_VEHICLE table and put it into an array variable
//
// [PARAMETERS]:    $carAbiCode
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Furtuna Alexandru  (alex@acrux.biz) 2010-09-02
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuinnVehicleByAbiCodeNew($quinnAbiCode='')
{
   if(! preg_match("/^\d+$/", $quinnAbiCode))
   {
      $this->strERR = GetErrorString("INVALID_QUINN_VEHICEL_ID");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM QUINN_VEHICLE WHERE abiCode='$quinnAbiCode'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUINN_VEHICLE_ID_NOT_FOUND");
      return false;
   }
   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]                = trim($this->dbh->GetFieldValue("id"));                
   $arrayResult["abiCode"]           = trim($this->dbh->GetFieldValue("abiCode"));
   $arrayResult["make_description"]  = trim($this->dbh->GetFieldValue("make_description"));
   $arrayResult["model_description"] = trim($this->dbh->GetFieldValue("model_description"));
   $arrayResult["veh_series"]        = trim($this->dbh->GetFieldValue("veh_series"));
   $arrayResult["veh_capacity"]      = trim($this->dbh->GetFieldValue("veh_capacity"));
   $arrayResult["from_year"]         = trim($this->dbh->GetFieldValue("from_year"));
   $arrayResult["to_year"]           = trim($this->dbh->GetFieldValue("to_year"));
   $arrayResult["doors"]             = trim($this->dbh->GetFieldValue("doors"));
   $arrayResult["veh_type"]          = trim($this->dbh->GetFieldValue("veh_type"));
   $arrayResult["veh_fuel"]          = trim($this->dbh->GetFieldValue("veh_fuel"));
   $arrayResult["veh_user_term1"]    = trim($this->dbh->GetFieldValue("veh_user_term1"));

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuinnVehicle
//
// [DESCRIPTION]:   Read data from QUINN_VEHICLE table and put it into an array variable
//                  key = quinnVehicleID, value = Array(that contain all the record for that user)
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Velnic Daniel  (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuinnVehicle()
{
   $lastSqlCmd = "SELECT * FROM ".SQL_QUINN_VEHICLE;

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERS_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   $index       = 0;
   while($this->dbh->MoveNext())
   {
   		$matche = array();
		preg_match("/([\d]+)/",$this->dbh->GetFieldValue("doors"),$matche);
		$doors = "";
		$doors = $matche[1];

		$index++;
		$arrayResult[$index]["id"]                = trim($this->dbh->GetFieldValue("id"));
		$arrayResult[$index]["abiCode"]           = trim($this->dbh->GetFieldValue("abiCode"));
		$arrayResult[$index]["make_description"]  = trim(stripslashes($this->dbh->GetFieldValue("make_description")));
		$arrayResult[$index]["model_description"] = trim(stripslashes($this->dbh->GetFieldValue("model_description")));
		$arrayResult[$index]["veh_series"]        = trim($this->dbh->GetFieldValue("veh_series"));
		$arrayResult[$index]["veh_capacity"]      = trim($this->dbh->GetFieldValue("veh_capacity"));
		$arrayResult[$index]["from_year"]         = trim($this->dbh->GetFieldValue("from_year"));
		$arrayResult[$index]["to_year"]           = trim($this->dbh->GetFieldValue("to_year"));
		$arrayResult[$index]["doors"]             = trim($doors);
		$arrayResult[$index]["veh_type"]          = trim($this->dbh->GetFieldValue("veh_type"));
		$arrayResult[$index]["veh_fuel"]          = trim($this->dbh->GetFieldValue("veh_fuel"));
		$arrayResult[$index]["veh_transmission"]  = trim($this->dbh->GetFieldValue("veh_transmission"));
		$arrayResult[$index]["veh_abi_grp"]       = trim($this->dbh->GetFieldValue("veh_abi_grp"));
		$arrayResult[$index]["veh_grp_suffix"]    = trim($this->dbh->GetFieldValue("veh_grp_suffix"));
		$arrayResult[$index]["veh_security"]      = trim($this->dbh->GetFieldValue("veh_security"));
		$arrayResult[$index]["veh_user_class"]    = trim($this->dbh->GetFieldValue("veh_user_class"));
		$arrayResult[$index]["veh_user_term1"]    = trim($this->dbh->GetFieldValue("veh_user_term1"));
		$arrayResult[$index]["veh_user_term2"]    = trim($this->dbh->GetFieldValue("veh_user_term2"));
		$arrayResult[$index]["veh_user_term3"]    = trim($this->dbh->GetFieldValue("veh_user_term3"));
		$arrayResult[$index]["veh_user_defined"]  = trim($this->dbh->GetFieldValue("veh_user_defined"));
		$arrayResult[$index]["veh_eff_date"]      = trim($this->dbh->GetFieldValue("veh_eff_date"));
		$arrayResult[$index]["veh_model_type"]    = trim($this->dbh->GetFieldValue("veh_model_type"));
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Show the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

}//end class CQuinnVehicle

?>

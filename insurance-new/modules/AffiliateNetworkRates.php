<?php

/*****************************************************************************/
/*                                                                           */
/*  CAffiliateNetworkRate class interface                                                */
/*                                                                           */
/*  (C) 2008 Istvancsek Gabriel (gabi@acrux.biz)                             */
/*                                                                           */
/*****************************************************************************/

include_once "errors.inc";
include_once "MySQL.php";

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CAffiliateNetworkRates
//
// [DESCRIPTION]:  CAffiliateNetworkRates class interface
//
// [FUNCTIONS]:
//
//
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CAffiliateNetworkRate
{

    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CAffiliateNetworkRates
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//
//////////////////////////////////////////////////////////////////////////////FE
function CAffiliateNetworkRate($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertAffiliateNetworkRate
//
// [DESCRIPTION]:   assert data fields
//
// [PARAMETERS]:    $userID, $efid,$quoteTypeID,$rate,$quoteLevel
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//
//////////////////////////////////////////////////////////////////////////////FE
function AssertAffiliateNetworkRate($userID, $efid, $quoteTypeID, $userRate, $quoteLevel, $userRateDate)
{
   if(! preg_match("/^\d+$/", $userID))
   {
      $this->strERR = GetErrorString("INVALID_USER_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   if(! preg_match("/^[0-9\.]+$/", $userRate))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATE_NETWORK_RATE_FIELD");
      return false;
   }

   if(! preg_match("/^[0-9\.]+$/", $quoteLevel))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_LEVEL_FIELD");
      return false;
   }

   if(! preg_match("/^[0-9\.]{4}\-[0-9\.]{2}\-[0-9\.]{2}$/", $userRateDate))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATE_NETWORK_RATE_DATE_FIELD");
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddAffiliateNetworkRates
//
// [DESCRIPTION]:   add data into user rates
//
// [PARAMETERS]:    $userID, $efid, $quoteTypeID, $userRate, $quoteLevel
//
// [RETURN VALUE]:  int | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//
//////////////////////////////////////////////////////////////////////////////FE
function AddAffiliateNetworkRate($userID, $efid, $quoteTypeID, $userRate, $quoteLevel, $userRateDate)
{
   if(! $this->AssertAffiliateNetworkRate($userID, $efid, $quoteTypeID, $userRate, $quoteLevel, $userRateDate))
      return false;

   $sqlCmd = " SELECT * FROM affiliate_network_rates WHERE user_id='$userID' AND efid='$efid' AND quote_type_id='$quoteTypeID' AND quote_level='$quoteLevel' AND date='$userRateDate'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if($this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATE_NETWORK_RATE_DETAILS_FOUND");
      return false;
   }

   $sqlCmd = " INSERT INTO affiliate_network_rates (user_id,efid,quote_type_id,user_rate,quote_level,date) VALUES('$userID','$efid','$quoteTypeID','$userRate', '$quoteLevel','$userRateDate') ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateAffiliateNetworkRates
//
// [DESCRIPTION]:   update fields by user rate id
//
// [PARAMETERS]:    $userRateID, $userID, $efid, $quoteTypeID, $userRate, $quoteLevel
//
// [RETURN VALUE]:  false | true
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//
//////////////////////////////////////////////////////////////////////////////FE
function UpdateAffiliateNetworkRate($userRateID, $userID, $efid, $quoteTypeID, $userRate, $quoteLevel, $userRateDate)
{
   if(! preg_match("/^\d+$/", $userRateID))
   {
      $this->strERR = GetErrorString("INVALID_USER_ID_FIELD");
      return false;
   }

   if(! $this->AssertAffiliateNetworkRate($userID, $efid, $quoteTypeID, $userRate, $quoteLevel, $userRateDate))
      return false;

   $sqlCmd = " UPDATE affiliate_network_rates set user_id='$userID', efid='$efid', quote_type_id='$quoteTypeID', quote_level='$quoteLevel', user_rate='$userRate', date='$userRateDate'  WHERE id='$userRateID' ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteAffiliateNetworkRates
//
// [DESCRIPTION]:   delete entry from user_rates by user rate id
//
// [PARAMETERS]:    $userRateID = ""
//
// [RETURN VALUE]:  false|true
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//
//////////////////////////////////////////////////////////////////////////////FE
function DeleteAffiliateNetworkRate($userRateID)
{
   if(! preg_match("/^\d+$/", $userRateID))
   {
      $this->strERR = GetErrorString("INVALID_USER_ID_FIELD");
      return false;
   }

   $sqlCmd = " DELETE FROM affiliate_network_rates WHERE id='$userRateID' ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteAllAffiliateNetworkRates
//
// [DESCRIPTION]:   delete all entries from user_rates by userID
//
// [PARAMETERS]:    $userID = ""
//
// [RETURN VALUE]:  false|true
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//
//////////////////////////////////////////////////////////////////////////////FE
function DeleteAllAffiliateNetworkRates($userID)
{
   if(! preg_match("/^\d+$/", $userID))
   {
      $this->strERR = GetErrorString("INVALID_USER_ID_FIELD");
      return false;
   }

   $sqlCmd = " DELETE FROM affiliate_network_rates WHERE user_id='$userID' ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffiliateNetworkRates
//
// [DESCRIPTION]:   Read data from user_rates table and put it into an array variable
//
// [PARAMETERS]:    $userRateID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//
//////////////////////////////////////////////////////////////////////////////FE
function GetAffiliateNetworkRates($userRateID)
{
   if(! preg_match("/^\d+$/", $userRateID))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATE_NETWORK_RATE_ID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT * FROM affiliate_network_rates WHERE id='$userRateID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATE_NETWORK_RATE_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("AFFILIATE_NETWORK_RATE_ID_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   $arrayResult['user_id']   = $this->dbh->GetFieldValue("user_id");
   $arrayResult['user_rate_date'] = $this->dbh->GetFieldValue("date");
   $arrayResult['efid']           = $this->dbh->GetFieldValue("efid");
   $arrayResult['quote_type_id']  = $this->dbh->GetFieldValue("quote_type_id");
   $arrayResult['user_rate']      = $this->dbh->GetFieldValue("user_rate");
   $arrayResult['quote_level']    = $this->dbh->GetFieldValue("quote_level");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRemittanceAffiliateNetworkRate
//
// [DESCRIPTION]:   Read data from user_rates table and put it into an array variable
//
// [PARAMETERS]:    $userRateID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//
//////////////////////////////////////////////////////////////////////////////FE
function GetRemittanceAffiliateNetworkRate($userID, $efid, $quoteTypeID, $nrOfQuotes=0)
{
   // TO DO user rate date
   if(! preg_match("/^\d+$/", $userID))
   {
      $this->strERR = GetErrorString("INVALID_USER_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $nrOfQuotes))
   {
      $this->strERR = GetErrorString("INVALID_NUMBER_OF_QUOTES_FIELD");
      return false;
   }

   $sqlCmd = "SELECT user_rate FROM affiliate_network_rates WHERE user_id='$userID' AND efid='$efid' AND quote_type_id='$quoteTypeID' AND '$nrOfQuotes' >= quote_level ORDER BY quote_level desc limit 1";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATE_NETWORK_RATE_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("AFFILIATE_NETWORK_RATE_ID_NOT_FOUND");
      return false;
   }

   return $this->dbh->GetFieldValue("user_rate");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRemittanceAffiliateNetworkRate
//
// [DESCRIPTION]:   Read data from user_rates table and put it into an array variable
//
// [PARAMETERS]:    $userRateID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//
//////////////////////////////////////////////////////////////////////////////FE
function GetRemittanceAffiliateNetworkRateByInterval($userID, $efid, $quoteTypeID, $startDate, $endDate)
{
   // TO DO user rate date
   if(! preg_match("/^\d+$/", $userID))
   {
      $this->strERR = GetErrorString("INVALID_USER_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   //check if we have some rates defined on the first day of the intertval
   if(! $this->GetAffiliateNetworkRatesByDate($userID, $efid, $quoteTypeID, $startDate))
   {
      $resAffiliateNetworkRates = $this->GetLastAffiliateNetworkRatesByDate($userID, $efid, $quoteTypeID, $startDate);

      //preparing user rates by date
      $results = array();
      $counter = 0;
      foreach($resAffiliateNetworkRates  as $index => $userRate)
      {
         $counter++;
         $arrayResult[$startDate][$counter] = $userRate;
      }
   }

   $sqlCmd = "SELECT * FROM affiliate_network_rates WHERE user_id='$userID' AND efid='$efid' AND quote_type_id='$quoteTypeID' and date between '$startDate' AND '$endDate' ORDER BY date asc, quote_level asc ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $counter = 0;
   $currentDate = '';
   while($this->dbh->MoveNext())
   {
      if($currentDate != $this->dbh->GetFieldValue("date"))
      {
         $counter = 0;
         $currentDate = $this->dbh->GetFieldValue("date");
      }

      $counter++;
      $arrayResult[$this->dbh->GetFieldValue("date")][$counter]['id']                 = $this->dbh->GetFieldValue("id");
      $arrayResult[$this->dbh->GetFieldValue("date")][$counter]['user_id']       = $this->dbh->GetFieldValue("user_id");
      $arrayResult[$this->dbh->GetFieldValue("date")][$counter]['efid']               = $this->dbh->GetFieldValue("efid");
      $arrayResult[$this->dbh->GetFieldValue("date")][$counter]['quote_type_id']      = $this->dbh->GetFieldValue("quote_type_id");
      $arrayResult[$this->dbh->GetFieldValue("date")][$counter]['user_rate']          = $this->dbh->GetFieldValue("user_rate");
      $arrayResult[$this->dbh->GetFieldValue("date")][$counter]['quote_level']        = $this->dbh->GetFieldValue("quote_level");
      $arrayResult[$this->dbh->GetFieldValue("date")][$counter]['user_rate_date']     = $this->MysqlToStdDate($this->dbh->GetFieldValue("date"));
      $arrayResult[$this->dbh->GetFieldValue("date")][$counter]['sql_user_rate_date'] = $this->dbh->GetFieldValue("date");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffiliateNetworkRates
//
// [DESCRIPTION]:   Read data from user_rates table and put it into an array variable
//
// [PARAMETERS]:    $userRateID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//
//////////////////////////////////////////////////////////////////////////////FE
function GetAllAffiliateNetworkRates($userID, $efid, $quoteTypeID, $userRateDate)
{
   if(! preg_match("/^\d+$/", $userID))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATE_NETWORK_RATE_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT * FROM affiliate_network_rates WHERE user_id='$userID' AND efid='$efid' AND quote_type_id='$quoteTypeID' AND date like '$userRateDate%' ORDER BY date asc, quote_level asc";
//print $sqlCmd;
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $counter = 0;
   while($this->dbh->MoveNext())
   {
      $counter++;
      $arrayResult[$counter]['id']                 = $this->dbh->GetFieldValue("id");
      $arrayResult[$counter]['user_id']       = $this->dbh->GetFieldValue("user_id");
      $arrayResult[$counter]['efid_type']          = $this->dbh->GetFieldValue("efid");
      $arrayResult[$counter]['quote_type_id']      = $this->dbh->GetFieldValue("quote_type_id");
      $arrayResult[$counter]['user_rate']          = $this->dbh->GetFieldValue("user_rate");
      $arrayResult[$counter]['quote_level']        = $this->dbh->GetFieldValue("quote_level");
      $arrayResult[$counter]['user_rate_date']     = $this->MysqlToStdDate($this->dbh->GetFieldValue("date"));
      $arrayResult[$counter]['sql_user_rate_date'] = $this->dbh->GetFieldValue("date");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffiliateNetworkRates
//
// [DESCRIPTION]:   Read data from user_rates table and put it into an array variable
//
// [PARAMETERS]:    $userRateID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//
//////////////////////////////////////////////////////////////////////////////FE
function GetLastAffiliateNetworkRatesByDate($userID, $efid, $quoteTypeID, $userRateDate)
{
   if(! preg_match("/^\d+$/", $userID))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATE_NETWORK_RATE_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT * FROM affiliate_network_rates WHERE user_id='$userID' AND efid='$efid' AND quote_type_id='$quoteTypeID' AND date < '$userRateDate' ORDER BY date desc limit 1";
//print $sqlCmd;
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   if(! $this->dbh->FetchRows())
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $lastDate = $this->dbh->GetFieldValue("date");

   $sqlCmd = "SELECT * FROM affiliate_network_rates WHERE user_id='$userID' AND efid='$efid' AND quote_type_id='$quoteTypeID' AND date='$lastDate' ORDER BY date asc, quote_level desc";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $sqlCmd = "SELECT * FROM affiliate_network_rates WHERE user_id='$userID' AND efid='$efid' AND quote_type_id='$quoteTypeID' AND date='$lastDate' ORDER BY date asc, quote_level desc";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $counter = 0;
   while($this->dbh->MoveNext())
   {
      $counter++;
      $arrayResult[$counter]['id']                 = $this->dbh->GetFieldValue("id");
      $arrayResult[$counter]['user_id']       = $this->dbh->GetFieldValue("user_id");
      $arrayResult[$counter]['efid']               = $this->dbh->GetFieldValue("efid");
      $arrayResult[$counter]['quote_type_id']      = $this->dbh->GetFieldValue("quote_type_id");
      $arrayResult[$counter]['user_rate']          = $this->dbh->GetFieldValue("user_rate");
      $arrayResult[$counter]['quote_level']        = $this->dbh->GetFieldValue("quote_level");
      $arrayResult[$counter]['user_rate_date']     = $this->MysqlToStdDate($this->dbh->GetFieldValue("date"));
      $arrayResult[$counter]['sql_user_rate_date'] = $this->dbh->GetFieldValue("date");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffiliateNetworkRates
//
// [DESCRIPTION]:   Read data from user_rates table and put it into an array variable
//
// [PARAMETERS]:    $userRateID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//
//////////////////////////////////////////////////////////////////////////////FE
function GetAffiliateNetworkRatesByDate($userID, $efid, $quoteTypeID, $userRateDate)
{
   if(! preg_match("/^\d+$/", $userID))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATE_NETWORK_RATE_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT * FROM affiliate_network_rates WHERE user_id='$userID' AND efid='$efid' AND quote_type_id='$quoteTypeID' AND date='$userRateDate' ORDER BY date asc, quote_level desc";
//print $sqlCmd;
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $counter = 0;
   while($this->dbh->MoveNext())
   {
      $counter++;
      $arrayResult[$counter]['id']                 = $this->dbh->GetFieldValue("id");
      $arrayResult[$counter]['user_id']       = $this->dbh->GetFieldValue("user_id");
      $arrayResult[$counter]['efid']               = $this->dbh->GetFieldValue("efid");
      $arrayResult[$counter]['quote_type_id']      = $this->dbh->GetFieldValue("quote_type_id");
      $arrayResult[$counter]['user_rate']          = $this->dbh->GetFieldValue("user_rate");
      $arrayResult[$counter]['quote_level']        = $this->dbh->GetFieldValue("quote_level");
      $arrayResult[$counter]['user_rate_date']     = $this->MysqlToStdDate($this->dbh->GetFieldValue("date"));
      $arrayResult[$counter]['sql_user_rate_date'] = $this->dbh->GetFieldValue("date");
   }

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   if(! preg_match("/\d{1,2}\/\d{1,2}\/\d{4}/i",$date))
      $this->strERR = GetErrorString("INVALID_STD_TO_MYSQL_DATE")."\n";

   if(! empty($this->strERR))
      return false;

   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MysqlToStdDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : yyyy-mm-dd
//
// [RETURN VALUE]:  $date : dd/mm/yyyy
//
// [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MysqlToStdDate($date='')
{
   $this->strERR = '';

   if(! preg_match("/\d{4}\-\d{1,2}\-\d{1,2}/i",$date))
      $this->strERR = GetErrorString("INVALID_MYSQL_TO_STD_DATE")."\n";

   if(! empty($this->strERR))
      return false;

   list($year,$month,$day) = split("-",$date);
   $date = "$day/$month/$year";

   return $date;
}

}

?>

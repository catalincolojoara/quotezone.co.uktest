<?php
/*****************************************************************************/
/*                                                                           */
/*  CQuoteUserDetails class interface                                              */
/*                                                                           */
/*  (C) 2005 Istvancsek Gabi(gabi@acrux.biz)                                   */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuoteUserDetails
//
// [DESCRIPTION]:  CQuoteUser class interface
//
// [FUNCTIONS]:    true | false AssertQuoteUserDetails($quoteUserID, $bornUk, $ukResDate,$maritalStatus, $typeDrvLic, $drvLicDate,     //                                $passPlus,$ftEmploymentStatus, $dayPhone, $eveningPhone, $mobilePhone,$postCode, $homeOwner,$email,  //                                $startInsuranceDate, $driversInsured, $claims, $convictions, $carAbiCode,                            //                                $policyExpire,&$address,$typeOfCover, $typeOfUse, $vehicleBought,$title,$estimatedValue)
//                 string | false StdToMysqlDate($date='')
//                 int    | false AddQuoteUserDetails($quoteUserID='', $bornUk='', $ukResDate='', $maritalStatus='', $typeDrvLic='',   //                                 $drvLicDate='', $passPlus='', $ftEmploymentStatus='', $ftBusiness='', $ftOcupation='',              //                                 $havePartTime='', $ptEmploymentStatus='', $ptBusiness='', $ptOcupation='', $dayPhone='',            //                                 $eveningPhone='', $mobilePhone='', $postCode='', $homeOwner='', $email='',                          //                                 $startInsuranceDate='',$driversInsured='', $claims='', $convictions='', $carAbiCode='',             //                                 $policyExpire='', $address='', $typeOfCover='', $typeOfUse='', $vehicleBought='',                   //                                 $title='',$estimatedValue='')
//                  true | false DeleteQuoteUserDetails($userDetailsID='')
//                 array | false GetQuoteUserDetailsByID($userDetailsID='')
//                 array | false GetQuoteUserDetailsByEmail($userEmail='')
//                 array | false GetQuoteUserDetailsByEmailDateOfBirth($userEmail='', $dateOfBirth='')
//                 array | false GetAllQuoteUserDetails($quoteUserID='')
//                 array | false GetAllUsersDetailsByLogID($logID = '')
//
//                 Close();
//                 GetError();
//                 ShowError();
//
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CQuoteUserDetails
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuoteUserDetails
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQuoteUserDetails($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuoteUser
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:                $quoteUserID, $bornUk, $ukResDate,$maritalStatus, $typeDrvLic, $drvLicDate, $passPlus,
//                              $ftEmploymentStatus, $dayPhone, $eveningPhone, $mobilePhone,$postCode, $homeOwner,
//                              $email, $startInsuranceDate, $driversInsured, $claims, $convictions, $carAbiCode, $policyExpire,
//                              &$address,$typeOfCover, $typeOfUse, $vehicleBought,$title,$estimatedValue,$title,$estimatedValue
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertQuoteUserDetails($quoteUserID, $bornUk, $ukResDate,$maritalStatus, $typeDrvLic, $drvLicDate, $passPlus,
                                $ftEmploymentStatus, $dayPhone, $eveningPhone, $mobilePhone,$postCode, $homeOwner,
                                $email, $startInsuranceDate, $driversInsured, $claims, $convictions, $carAbiCode, $policyExpire,
                                &$address,$typeOfCover, $typeOfUse, $vehicleBought,$title,$estimatedValue)
{
      $this->strERR = '';

      if(! preg_match('/[(Mr)|(Mrs)|(Ms)|(Miss)]/i',$title))
         $this->strERR = GetErrorString("INVALID_TITLE_FIELD");

      if(! preg_match('/\d+/i',$estimatedValue))
         $this->strERR .= GetErrorString("INVALID_ESTIMATED_VALUE_FIELD");

      if(! preg_match('/\d+/i',$quoteUserID))
         $this->strERR .= GetErrorString("INVALID_USER_ID_FIELD");

      $address = str_replace("'","\'",$address);

      if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$address))
         $this->strERR .= GetErrorString("INVALID_ADDRESS_NAME_FIELD");

     if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$policyExpire))
         $this->strERR .= GetErrorString("INVALID_POLICY_EXPIRE_FIELD");

     if(! preg_match('/[A-Z]{1}/',$bornUk))
         $this->strERR .= GetErrorString("INVALID_BORN_UK_FIELD");

     if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$ukResDate))
         $this->strERR .= GetErrorString("INVALID_UK_RESIDENCE_DATE_FIELD");

     if(! preg_match('/[A-Z]{1}/',$maritalStatus))
         $this->strERR .= GetErrorString("INVALID_MARITAL_STATUS_FIELD");

     if(! preg_match('/[A-Z]{1}/',$typeDrvLic))
         $this->strERR .= GetErrorString("INVALID_TYPE_DRIVEN_LICENCE_FIELD");

     if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$drvLicDate))
         $this->strERR .= GetErrorString("INVALID_DRIVE_LICENCE_DATE_FIELD");

     if(! preg_match('/[A-Z]{1}/',$passPlus))
         $this->strERR .= GetErrorString("INVALID_PASS_PLUS_FIELD");

     if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$ftEmploymentStatus))
         $this->strERR .= GetErrorString("INVALID_FULL_TIME_EMPLOYMENT_STATUS_FIELD");

     if(! preg_match('/[0-9\-\s\_]{1,32}/i',$dayPhone))
         $this->strERR .= GetErrorString("INVALID_DAY_PHONE_FIELD");

     if(! preg_match('/[0-9\-\s\_]{1,32}/i',$eveningPhone))
         $this->strERR .= GetErrorString("INVALID_EVENING_PHONE_FIELD");

     if(! preg_match('/[0-9\-\s\_]{1,32}/i',$mobilePhone))
         $this->strERR .= GetErrorString("INVALID_MOBILE_PHONE_FIELD");

     if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,16}/i',$postCode))
         $this->strERR .= GetErrorString("INVALID_POST_CODE_FIELD");

     if(! preg_match('/[A-Z]{1}/',$homeOwner))
         $this->strERR .= GetErrorString("INVALID_HOME_OWNER_FIELD");

     if(! preg_match('/\.*\@.*\.[a-zA-Z]{2,4}/i',$email))
         $this->strERR .= GetErrorString("INVALID_EMAIL_FIELD");

     if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$startInsuranceDate))
         $this->strERR .= GetErrorString("INVALID_START_INSURANCE_FIELD");

     if(! preg_match('/^[1-4]$/',$driversInsured))
         $this->strERR .= GetErrorString("INVALID_DRIVERS_INSURED_FIELD");

     if(! preg_match('/[A-Z]{1}/',$claims))
         $this->strERR .= GetErrorString("INVALID_CLAIMS_FIELD");

     if(! preg_match('/[A-Z]{1}/',$convictions))
         $this->strERR .= GetErrorString("INVALID_CONVICTIONS_FIELD");

     if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,16}/i',$carAbiCode))
         $this->strERR .= GetErrorString("INVALID_CAR_ABI_CODE_FIELD");

      if(! preg_match('/\d+/i',$typeOfCover))
         $this->strERR = GetErrorString("INVALID_TYPE_OF_COVER_FIELD");

      if(! preg_match('/\d+/i',$typeOfUse))
         $this->strERR = GetErrorString("INVALID_TYPE_OF_USE_FIELD");

     if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$vehicleBought))
         $this->strERR .= GetErrorString("INVALID_VEHICLE_BOUGHT_DATE_FIELD");

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   //if(! preg_match("\/", $date))
   //  return $date;
      
   if(! preg_match("/\d{1,2}\/\d{1,2}\/\d{4}/i",$date))
      $this->strERR = GetErrorString("INVALID_QUOTE_USER_DETAILS_STD_TO_MYSQL_DATE");

   if(! empty($this->strERR))
      return false;
   
   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteUserDetails
//
// [DESCRIPTION]:   Add new entry to the quote_user_details table
//
// [PARAMETERS]:             $quoteUserID='', $bornUk='', $ukResDate='', $maritalStatus='', $typeDrvLic='', $drvLicDate='',
//                           $passPlus='', $ftEmploymentStatus='', $ftBusiness='', $ftOcupation='', $havePartTime='',
//                           $ptEmploymentStatus='', $ptBusiness='', $ptOcupation='', $dayPhone='', $eveningPhone='',
//                           $mobilePhone='', $postCode='', $homeOwner='', $email='', $startInsuranceDate='',$driversInsured='',
//                           $claims='', $convictions='', $carAbiCode='', $policyExpire='', $address='', $typeOfCover='', //           //                           $typeOfUse='', $vehicleBought='', $title='',$estimatedValue=''
//
// [RETURN VALUE]:  userID | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteUserDetails($quoteUserID='', $bornUk='', $ukResDate='', $maritalStatus='', $typeDrvLic='', $drvLicDate='',
                             $passPlus='', $ftEmploymentStatus='', $ftBusiness='', $ftOcupation='', $havePartTime='',
                             $ptEmploymentStatus='', $ptBusiness='', $ptOcupation='', $dayPhone='', $eveningPhone='',
                             $mobilePhone='', $postCode='', $homeOwner='', $email='', $startInsuranceDate='',$driversInsured='',
                             $claims='', $convictions='', $carAbiCode='', $policyExpire='', $address='', $typeOfCover='', $typeOfUse='', $vehicleBought='', $title='', $estimatedValue='')
{
   if(! $vehicleBought = $this->StdToMysqlDate($vehicleBought))
      return false;

   if(! $policyExpire = $this->StdToMysqlDate($policyExpire))
      return false;

   if(! $ukResDate = $this->StdToMysqlDate($ukResDate))
      return false;

   if(! $drvLicDate = $this->StdToMysqlDate($drvLicDate))
      return false;

   if(! $startInsuranceDate = $this->StdToMysqlDate($startInsuranceDate))
      return false;

  if(! $this->AssertQuoteUserDetails($quoteUserID, $bornUk, $ukResDate, $maritalStatus, $typeDrvLic, $drvLicDate, $passPlus,
                                     $ftEmploymentStatus, $dayPhone, $eveningPhone, $mobilePhone, $postCode, $homeOwner, $email,
                                     $startInsuranceDate, $driversInsured, $claims, $convictions, $carAbiCode, $policyExpire,
                                     $address,$typeOfCover,$typeOfUse,$vehicleBought,$title,$estimatedValue))
   return false;

   $lastSqlCmd = "INSERT INTO quote_user_details (quote_user_id, born_uk, uk_res_date, marital_status, type_drv_lic, drv_lic_date, pass_plus, ft_employment_status, ft_business, ft_ocupation, have_part_time, pt_employment_status, pt_business, pt_ocupation, day_phone, evening_phone, mobile_phone, post_code, home_owner, email, start_insurance_date, drivers_insured, claims, convictions, car_abi_code, policy_expire, house_number_or_name,type_of_cover,type_of_use,vehicle_bought,title,estimated_value) VALUES ('$quoteUserID', '$bornUk', '$ukResDate', '$maritalStatus', '$typeDrvLic', '$drvLicDate', '$passPlus', '$ftEmploymentStatus', '$ftBusiness', '$ftOcupation', '$havePartTime', '$ptEmploymentStatus', '$ptBusiness', '$ptOcupation', '$dayPhone', '$eveningPhone', '$mobilePhone', '$postCode', '$homeOwner', '$email', '$startInsuranceDate', '$driversInsured', '$claims', '$convictions', '$carAbiCode', '$policyExpire', '$address','$typeOfCover','$typeOfUse','$vehicleBought','$title','$estimatedValue')";


   if(! $this->dbh->Exec($lastSqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $lastSqlCmd = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
/////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQuoteUserDetails
//
// [DESCRIPTION]:   Delete an entry from quote_user_details table
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteQuoteUserDetails($userDetailsID='')
{
   if(! preg_match("/^\d+$/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_USER_DETAILS_ID_FIELD");
      return false;
   }

   // check if userID exists
   $lastSqlCmd = "SELECT id FROM quote_user_details WHERE id='$userDetailsID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_DETAILS_ID_NOT_FOUND");
      return false;
   }

   $lastSqlCmd = "DELETE FROM quote_user_details WHERE id='$userDetailsID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteUserDetailsByID
//
// [DESCRIPTION]:   Read data of a user from quote_user_details table and put it into an array variable
//
// [PARAMETERS]:    $userDetailsID
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteUserDetailsByID($userDetailsID='')
{
   if(! preg_match("/^\d+$/", $userDetailsID))
   {
      $this->strERR = GetErrorString("INVALID_USER_DETAILS_ID_FIELD");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM quote_user_details WHERE id='$userDetailsID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_DETAILS_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]                    = $this->dbh->GetFieldValue("id");
   $arrayResult["quote_user_id"]         = $this->dbh->GetFieldValue("quote_user_id");
   $arrayResult["born_uk"]               = $this->dbh->GetFieldValue("born_uk");
   $arrayResult["uk_res_date"]           = $this->dbh->GetFieldValue("uk_res_date");
   $arrayResult["marital_status"]        = $this->dbh->GetFieldValue("marital_status");
   $arrayResult["type_drv_lic"]          = $this->dbh->GetFieldValue("type_drv_lic");
   $arrayResult["drv_lic_date"]          = $this->dbh->GetFieldValue("drv_lic_date");
   $arrayResult["pass_plus"]             = $this->dbh->GetFieldValue("pass_plus");
   $arrayResult["ft_employment_status"]  = $this->dbh->GetFieldValue("ft_employment_status");
   $arrayResult["ft_business"]           = $this->dbh->GetFieldValue("ft_business");
   $arrayResult["ft_ocupation"]          = $this->dbh->GetFieldValue("ft_ocupation");
   $arrayResult["have_part_time"]        = $this->dbh->GetFieldValue("have_part_time");
   $arrayResult["pt_employment_status"]  = $this->dbh->GetFieldValue("pt_employment_status");
   $arrayResult["pt_business"]           = $this->dbh->GetFieldValue("pt_business");
   $arrayResult["pt_ocupation"]          = $this->dbh->GetFieldValue("pt_ocupation");
   $arrayResult["day_phone"]             = $this->dbh->GetFieldValue("day_phone");
   $arrayResult["evening_phone"]         = $this->dbh->GetFieldValue("evening_phone");
   $arrayResult["mobile_phone"]          = $this->dbh->GetFieldValue("mobile_phone");
   $arrayResult["post_code"]             = $this->dbh->GetFieldValue("post_code");
   $arrayResult["home_owner"]            = $this->dbh->GetFieldValue("home_owner");
   $arrayResult["email"]                 = $this->dbh->GetFieldValue("email");
   $arrayResult["start_insurance_date"]  = $this->dbh->GetFieldValue("start_insurance_date");
   $arrayResult["drivers_insured"]       = $this->dbh->GetFieldValue("drivers_insured");
   $arrayResult["claims"]                = $this->dbh->GetFieldValue("claims");
   $arrayResult["convictions"]           = $this->dbh->GetFieldValue("convictions");
   $arrayResult["car_abi_code"]          = $this->dbh->GetFieldValue("car_abi_code");
   $arrayResult["policy_expire"]         = $this->dbh->GetFieldValue("policy_expire");
   $arrayResult["house_number_or_name"]  = $this->dbh->GetFieldValue("house_number_or_name");
   $arrayResult["type_of_cover"]         = $this->dbh->GetFieldValue("type_of_cover");
   $arrayResult["type_of_use"]           = $this->dbh->GetFieldValue("type_of_use");
   $arrayResult["vehicle_bought"]        = $this->dbh->GetFieldValue("vehicle_bought");
   $arrayResult["title"]                 = $this->dbh->GetFieldValue("title");
   $arrayResult["estimated_value"]       = $this->dbh->GetFieldValue("estimated_value");

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteUserDetailsByEmail
//
// [DESCRIPTION]:   Read data of a user from quote_user_details table and put it into an array variable
//
// [PARAMETERS]:    $userEmail
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteUserDetailsByEmail($userEmail='')
{
   if(! preg_match("/\.*\@.*\.[a-zA-Z]{2,3}/i", $userEmail))
   {
      $this->strERR = GetErrorString("INVALID_USER_EMAIL_FIELD");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM quote_user_details WHERE email='$userEmail'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_EMAIL_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index]["id"]                    = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["quote_user_id"]         = $this->dbh->GetFieldValue("quote_user_id");
      $arrayResult[$index]["born_uk"]               = $this->dbh->GetFieldValue("born_uk");
      $arrayResult[$index]["uk_res_date"]           = $this->dbh->GetFieldValue("uk_res_date");
      $arrayResult[$index]["marital_status"]        = $this->dbh->GetFieldValue("marital_status");
      $arrayResult[$index]["type_drv_lic"]          = $this->dbh->GetFieldValue("type_drv_lic");
      $arrayResult[$index]["drv_lic_date"]          = $this->dbh->GetFieldValue("drv_lic_date");
      $arrayResult[$index]["pass_plus"]             = $this->dbh->GetFieldValue("pass_plus");
      $arrayResult[$index]["ft_employment_status"]  = $this->dbh->GetFieldValue("ft_employment_status");
      $arrayResult[$index]["ft_business"]           = $this->dbh->GetFieldValue("ft_business");
      $arrayResult[$index]["ft_ocupation"]          = $this->dbh->GetFieldValue("ft_ocupation");
      $arrayResult[$index]["have_part_time"]        = $this->dbh->GetFieldValue("have_part_time");
      $arrayResult[$index]["pt_employment_status"]  = $this->dbh->GetFieldValue("pt_employment_status");
      $arrayResult[$index]["pt_business"]           = $this->dbh->GetFieldValue("pt_business");
      $arrayResult[$index]["pt_ocupation"]          = $this->dbh->GetFieldValue("pt_ocupation");
      $arrayResult[$index]["day_phone"]             = $this->dbh->GetFieldValue("day_phone");
      $arrayResult[$index]["evening_phone"]         = $this->dbh->GetFieldValue("evening_phone");
      $arrayResult[$index]["mobile_phone"]          = $this->dbh->GetFieldValue("mobile_phone");
      $arrayResult[$index]["post_code"]             = $this->dbh->GetFieldValue("post_code");
      $arrayResult[$index]["home_owner"]            = $this->dbh->GetFieldValue("home_owner");
      $arrayResult[$index]["email"]                 = $this->dbh->GetFieldValue("email");
      $arrayResult[$index]["start_insurance_date"]  = $this->dbh->GetFieldValue("start_insurance_date");
      $arrayResult[$index]["drivers_insured"]       = $this->dbh->GetFieldValue("drivers_insured");
      $arrayResult[$index]["claims"]                = $this->dbh->GetFieldValue("claims");
      $arrayResult[$index]["convictions"]           = $this->dbh->GetFieldValue("convictions");
      $arrayResult[$index]["car_abi_code"]          = $this->dbh->GetFieldValue("car_abi_code");
      $arrayResult[$index]["policy_expire"]         = $this->dbh->GetFieldValue("policy_expire");
      $arrayResult[$index]["house_number_or_name"]  = $this->dbh->GetFieldValue("house_number_or_name");
      $arrayResult[$index]["type_of_cover"]         = $this->dbh->GetFieldValue("type_of_cover");
      $arrayResult[$index]["type_of_use"]           = $this->dbh->GetFieldValue("type_of_use");
      $arrayResult[$index]["vehicle_bought"]        = $this->dbh->GetFieldValue("vehicle_bought");
      $arrayResult[$index]["title"]                 = $this->dbh->GetFieldValue("title");
      $arrayResult[$index]["estimated_value"]       = $this->dbh->GetFieldValue("estimated_value");
   }

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteUserDetailsByEmailDateOfBirth
//
// [DESCRIPTION]:   Read data of a user from quote_user_details table and put it into an array variable
//
// [PARAMETERS]:    $userEmail, $dateOfBirth
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteUserDetailsByEmailDateOfBirth($userEmail='', $dateOfBirth='')
{
   if(! preg_match("/\.*\@.*\.[a-zA-Z]{2,3}/i", $userEmail))
   {
      $this->strERR = GetErrorString("INVALID_USER_EMAIL_FIELD");
      return false;
   }

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$dateOfBirth))
   {
      $this->strERR = GetErrorString("INVALID_USER_DATE_OF_BIRTH_FIELD");
      return false;
   }  

   $lastSqlCmd = "SELECT * FROM quote_user_details, quote_users WHERE quote_user_details.quote_user_id=quote_users.id quote_user_details.email='$userEmail' AND quote_users.birth_date='$dateOfBirth' ";
   
   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("INVALID_USER_DATE_OF_BIRTH_FIELD");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]                    = $this->dbh->GetFieldValue("id");
   $arrayResult["first_name"]            = $this->dbh->GetFieldValue("first_name");
   $arrayResult["last_name"]             = $this->dbh->GetFieldValue("last_name");
   $arrayResult["birth_date"]            = $this->dbh->GetFieldValue("birth_date");
   $arrayResult["born_uk"]               = $this->dbh->GetFieldValue("born_uk");
   $arrayResult["uk_res_date"]           = $this->dbh->GetFieldValue("uk_res_date");
   $arrayResult["marital_status"]        = $this->dbh->GetFieldValue("marital_status");
   $arrayResult["type_drv_lic"]          = $this->dbh->GetFieldValue("type_drv_lic");
   $arrayResult["drv_lic_date"]          = $this->dbh->GetFieldValue("drv_lic_date");
   $arrayResult["pass_plus"]             = $this->dbh->GetFieldValue("pass_plus");
   $arrayResult["ft_employment_status"]  = $this->dbh->GetFieldValue("ft_employment_status");
   $arrayResult["ft_business"]           = $this->dbh->GetFieldValue("ft_business");
   $arrayResult["ft_ocupation"]          = $this->dbh->GetFieldValue("ft_ocupation");
   $arrayResult["have_part_time"]        = $this->dbh->GetFieldValue("have_part_time");
   $arrayResult["pt_employment_status"]  = $this->dbh->GetFieldValue("pt_employment_status");
   $arrayResult["pt_business"]           = $this->dbh->GetFieldValue("pt_business");
   $arrayResult["pt_ocupation"]          = $this->dbh->GetFieldValue("pt_ocupation");
   $arrayResult["day_phone"]             = $this->dbh->GetFieldValue("day_phone");
   $arrayResult["evening_phone"]         = $this->dbh->GetFieldValue("evening_phone");
   $arrayResult["mobile_phone"]          = $this->dbh->GetFieldValue("mobile_phone");
   $arrayResult["post_code"]             = $this->dbh->GetFieldValue("post_code");
   $arrayResult["home_owner"]            = $this->dbh->GetFieldValue("home_owner");
   $arrayResult["email"]                 = $this->dbh->GetFieldValue("email");
   $arrayResult["start_insurance_date"]  = $this->dbh->GetFieldValue("start_insurance_date");
   $arrayResult["drivers_insured"]       = $this->dbh->GetFieldValue("drivers_insured");
   $arrayResult["claims"]                = $this->dbh->GetFieldValue("claims");
   $arrayResult["convictions"]           = $this->dbh->GetFieldValue("convictions");
   $arrayResult["car_abi_code"]          = $this->dbh->GetFieldValue("car_abi_code");
   $arrayResult["policy_expire"]         = $this->dbh->GetFieldValue("policy_expire");
   $arrayResult["house_number_or_name"]  = $this->dbh->GetFieldValue("house_number_or_name");
   $arrayResult["type_of_cover"]         = $this->dbh->GetFieldValue("type_of_cover");
   $arrayResult["type_of_use"]           = $this->dbh->GetFieldValue("type_of_use");
   $arrayResult["vehicle_bought"]        = $this->dbh->GetFieldValue("vehicle_bought");
   $arrayResult["title"]                 = $this->dbh->GetFieldValue("title");
   $arrayResult["estimated_value"]       = $this->dbh->GetFieldValue("estimated_value");

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuoteUserDetails
//
// [DESCRIPTION]:   Read data from quote_user_details table and put it into an array variable
//                  key = userID, value = Array(that contain all the record for that user)
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuoteUserDetails($quoteUserID='')
{
   if(! preg_match("/\d+/",$quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_USER_ID_FIELD");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM quote_user_details WHERE quote_user_id='$quoteUserID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_DETAILS_NOT_FOUND");
      return false;
   }

   $arrayResult=array();

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index]["id"]                   = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["born_uk"]              = $this->dbh->GetFieldValue("born_uk");
      $arrayResult[$index]["uk_res_date"]          = $this->dbh->GetFieldValue("uk_res_date");
      $arrayResult[$index]["marital_status"]       = $this->dbh->GetFieldValue("marital_status");
      $arrayResult[$index]["type_drv_lic"]         = $this->dbh->GetFieldValue("type_drv_lic");
      $arrayResult[$index]["drv_lic_date"]         = $this->dbh->GetFieldValue("drv_lic_date");
      $arrayResult[$index]["pass_plus"]            = $this->dbh->GetFieldValue("pass_plus");
      $arrayResult[$index]["ft_employment_status"] = $this->dbh->GetFieldValue("ft_employment_status");
      $arrayResult[$index]["ft_business"]          = $this->dbh->GetFieldValue("ft_business");
      $arrayResult[$index]["ft_ocupation"]         = $this->dbh->GetFieldValue("ft_ocupation");
      $arrayResult[$index]["have_part_time"]       = $this->dbh->GetFieldValue("have_part_time");
      $arrayResult[$index]["pt_employment_status"] = $this->dbh->GetFieldValue("pt_employment_status");
      $arrayResult[$index]["pt_business"]          = $this->dbh->GetFieldValue("pt_business");
      $arrayResult[$index]["pt_ocupation"]         = $this->dbh->GetFieldValue("pt_ocupation");
      $arrayResult[$index]["day_phone"]            = $this->dbh->GetFieldValue("day_phone");
      $arrayResult[$index]["evening_phone"]        = $this->dbh->GetFieldValue("evening_phone");
      $arrayResult[$index]["mobile_phone"]         = $this->dbh->GetFieldValue("mobile_phone");
      $arrayResult[$index]["post_code"]            = $this->dbh->GetFieldValue("post_code");
      $arrayResult[$index]["home_owner"]           = $this->dbh->GetFieldValue("home_owner");
      $arrayResult[$index]["email"]                = $this->dbh->GetFieldValue("email");
      $arrayResult[$index]["start_insurance_date"] = $this->dbh->GetFieldValue("start_insurance_date");
      $arrayResult[$index]["drivers_insured"]      = $this->dbh->GetFieldValue("drivers_insured");
      $arrayResult[$index]["claims"]               = $this->dbh->GetFieldValue("claims");
      $arrayResult[$index]["convictions"]          = $this->dbh->GetFieldValue("convictions");
      $arrayResult[$index]["car_abi_code"]         = $this->dbh->GetFieldValue("car_abi_code");
      $arrayResult[$index]["policy_expire"]        = $this->dbh->GetFieldValue("policy_expire");
      $arrayResult[$index]["house_number_or_name"] = $this->dbh->GetFieldValue("house_number_or_name");
      $arrayResult[$index]["type_of_cover"]        = $this->dbh->GetFieldValue("type_of_cover");
      $arrayResult[$index]["type_of_use"]          = $this->dbh->GetFieldValue("type_of_use");
      $arrayResult[$index]["vehicle_bought"]       = $this->dbh->GetFieldValue("vehicle_bought");
      $arrayResult[$index]["title"]                = $this->dbh->GetFieldValue("title");
      $arrayResult[$index]["estimated_value"]      = $this->dbh->GetFieldValue("estimated_value");
    }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllUsersDetailsByLogID
//
// [DESCRIPTION]:   Read data from quote_user_details table and put it into an array variable
//                  key = userID, value = Array(that contain all the record for that user)
//
// [PARAMETERS]:     $logID
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllUsersDetailsByLogID($logID = '')
{
   if(! preg_match("/\d+/",$logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_USER_DETAIK_LOG_ID_FIELD");
      return false;
   }

   $lastSqlCmd = "SELECT quote_users.*,quote_user_details.*  FROM quote_users,quote_user_details,qzquotes,logs WHERE logs.id='$logID' AND qzquotes.log_id=logs.id AND qzquotes.quote_user_details_id=quote_user_details.id AND quote_user_details.quote_user_id=quote_users.id";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("USER_DETAILS_LOG_ID_NOT_FOUND");
      return false;
   }

   $arrayResult=array();

   $arrayResult["first_name"]          = $this->dbh->GetFieldValue("first_name");
   $arrayResult["last_name"]           = $this->dbh->GetFieldValue("last_name");
   $arrayResult["birth_date"]          = $this->dbh->GetFieldValue("birth_date");
   $arrayResult["born_uk"]             = $this->dbh->GetFieldValue("born_uk");
   $arrayResult["uk_res_date"]         = $this->dbh->GetFieldValue("uk_res_date");
   $arrayResult["marital_status"]      = $this->dbh->GetFieldValue("marital_status");
   $arrayResult["type_drv_lic"]        = $this->dbh->GetFieldValue("type_drv_lic");
   $arrayResult["drv_lic_date"]        = $this->dbh->GetFieldValue("drv_lic_date");
   $arrayResult["pass_plus"]           = $this->dbh->GetFieldValue("pass_plus");
   $arrayResult["ft_employment_status"]= $this->dbh->GetFieldValue("ft_employment_status");
   $arrayResult["ft_business"]         = $this->dbh->GetFieldValue("ft_business");
   $arrayResult["ft_ocupation"]        = $this->dbh->GetFieldValue("ft_ocupation");
   $arrayResult["have_part_time"]      = $this->dbh->GetFieldValue("have_part_time");
   $arrayResult["pt_employment_status"]= $this->dbh->GetFieldValue("pt_employment_status");
   $arrayResult["pt_business"]         = $this->dbh->GetFieldValue("pt_business");
   $arrayResult["pt_ocupation"]        = $this->dbh->GetFieldValue("pt_ocupation");
   $arrayResult["day_phone"]           = $this->dbh->GetFieldValue("day_phone");
   $arrayResult["evening_phone"]       = $this->dbh->GetFieldValue("evening_phone");
   $arrayResult["mobile_phone"]        = $this->dbh->GetFieldValue("mobile_phone");
   $arrayResult["post_code"]           = $this->dbh->GetFieldValue("post_code");
   $arrayResult["home_owner"]          = $this->dbh->GetFieldValue("home_owner");
   $arrayResult["email"]               = $this->dbh->GetFieldValue("email");
   $arrayResult["start_insurance_date"]= $this->dbh->GetFieldValue("start_insurance_date");
   $arrayResult["drivers_insured"]     = $this->dbh->GetFieldValue("drivers_insured");
   $arrayResult["claims"]              = $this->dbh->GetFieldValue("claims");
   $arrayResult["convictions"]         = $this->dbh->GetFieldValue("convictions");
   $arrayResult["car_abi_code"]        = $this->dbh->GetFieldValue("car_abi_code");
   $arrayResult["policy_expire"]       = $this->dbh->GetFieldValue("policy_expire");
   $arrayResult["house_number_or_name"]= $this->dbh->GetFieldValue("house_number_or_name");
   $arrayResult["type_of_cover"]       = $this->dbh->GetFieldValue("type_of_cover");
   $arrayResult["type_of_use"]         = $this->dbh->GetFieldValue("type_of_use");
   $arrayResult["vehicle_bought"]      = $this->dbh->GetFieldValue("vehicle_bought");
   $arrayResult["title"]               = $this->dbh->GetFieldValue("title");
   $arrayResult["estimated_value"]     = $this->dbh->GetFieldValue("estimated_value");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}
}//end class CQuoteUserDetails

?>

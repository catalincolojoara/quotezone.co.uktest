<?php
/*****************************************************************************/
/*                                                                           */
/*  CTrackDetailsNoCookies class interface                                */
/*                                                                           */
/*  (C) 2008 Moraru Valeriu (vali@acrux.biz)                                 */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CTrackDetailsNoCookies
//
// [DESCRIPTION]:  CTrackDetailsNoCookies class interface
//
// [FUNCTIONS]:    true   |false     AssertTrackDetailsNoCookies($quoteTypeID='',$userAgent='', $hostIP='')
//                 integer|false     AddTrackDetailsNoCookies($quoteTypeID='',$userAgent='',$hostIP='')
//                 true   |false     UpdateTrackDetailsStatusNoCookies($trackNoCookiesID='',$status='',$logID='')
//
//                                   Close();
//                                   GetError();
//
// [CREATED BY]:   Moraru Valeriu (vali@acrux.biz) 17-11-2008
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CTrackDetailsNoCookies
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CTrackDetailsNoCookies
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 17-11-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CTrackDetailsNoCookies($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertTrackerDetailsNoCookies
//
// [DESCRIPTION]:   Verify to see if the params are completed
//
// [PARAMETERS]:    $quoteTypeID,$userAgent,$hostIP
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 17-11-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertTrackDetailsNoCookies($quoteTypeID='',$userAgent='',$hostIP='')
{
   $this->strERR = "";

   if(! preg_match("/^\d+$/", $quoteTypeID))
      $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");

   if(empty($userAgent))
      $this->strERR = GetErrorString("INVALID_USER_AGENT_FIELD");

   if(empty($hostIP))
      $this->strERR = GetErrorString("INVALID_HOST_IP_FIELD");

    if($this->strERR != "")
      return true;

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddTrackDetailsNoCookies
//
// [DESCRIPTION]:   Add new entry to the track_no_cookies table
//
// [PARAMETERS]:    $quoteTypeID,$userAgent,$hostIP
//
// [RETURN VALUE]:  trUrlID or 0 in case of failure
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 17-11-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddTrackDetailsNoCookies($quoteTypeID='',$userAgent='',$hostIP='')
{
   if($this->AssertTrackDetailsNoCookies($quoteTypeID,$userAgent,$hostIP))
      return false;

   $this->lastSQLCMD = "INSERT INTO track_no_cookies (quote_type_id, user_agent, host_ip, date, time) VALUES ('$quoteTypeID','$userAgent','$hostIP',NOW(),NOW())";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateTrackDetailsStatusNoCookies
//
// [DESCRIPTION]:   Update a entry to the track_no_cookies table
//
// [PARAMETERS]:    $trackNoCookiesID,$status,$logID
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 18--2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateTrackDetailsStatusNoCookies($trackNoCookiesID='',$status='',$logID='')
{
   $this->strERR = "";

   if(! preg_match("/^\d+$/", $trackNoCookiesID))
      $this->strERR = GetErrorString("INVALID_TRACK_NO_COOKIES_ID_FIELD");

   if(empty($status))
      $this->strERR = GetErrorString("INVALID_TRACK_NO_COOKIES_STATUS_FIELD");

   if($this->strERR != "")
      return false;

   $sqlLogID = "";
   if(preg_match("/^\d+$/", $logID))
      $sqlLogID = " ,log_id = ".$logID." ";

   $this->lastSQLCMD = "UPDATE track_no_cookies SET status= '".$status."' ".$sqlLogID." WHERE id = ".$trackNoCookiesID." ";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   if(! preg_match("/\d{1,2}\/\d{1,2}\/\d{4}/i",$date))
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_STD_TO_MYSQL_DATE")."\n";

   if(! empty($this->strERR))
      return false;

   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print  $this->strERR;
}

}

?>

<?php

/*****************************************************************************/
/*                                                                           */
/*  CUser class interface                                                    */
/*                                                                           */
/*  (C) 2004 Eugen Savin (seugen@abc.ro)                                     */
/*                                                                           */
/*****************************************************************************/

define("USER_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CUser
//
// [DESCRIPTION]:  CUser class interface
//
// [FUNCTIONS]:    int  AddUser($email='', $password='', $name='', $address='',
//                              $city='', $pcode='', $state='', $country='GB',
//                              $phone='', $fax='', $org='', $locked='NO');
//                 bool UpdateUser($userID=0, $email='', $password='', $name='',
//                                 $address='', $city='', $pcode='', $state='',
//                                 $country='', $phone='', $fax='', $org='', $locked='');
//                 bool DeleteUser($userID=0);
//                 bool GetUser($userID=0, &$arrayResult);
//                 bool GetAllUsers(&$arrayResult);
//                 bool GetUserProfiles($userID=0, &$arrayResult);
//
//                 bool Login($email="", $password="");
//                 int  GetUserID($sessionKey="");
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CUser
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CUser
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CUser($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddUser
//
// [DESCRIPTION]:   Add new entry to the users table
//
// [PARAMETERS]:    $email='', $password='', $name='', $address='', $city='',
//                  $pcode='', $state='', $country='GB', $phone='', $fax='',
//                  $org='', $locked='NO'
//
// [RETURN VALUE]:  userID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddUser($email='', $password='', $name='', $address='',
                 $city='', $pcode='', $state='', $country='GB',
                 $phone='', $fax='', $org='', $locked='NO')
{
   if(empty($email) || !ereg(".+\@.+", $email))
      $this->strERR .= GetErrorString("INVALID_EMAIL_FIELD")."\n";

   if(empty($password))
      $this->strERR .= GetErrorString("INVALID_PASSWORD_FIELD")."\n";

   if(empty($name))
      $this->strERR .= GetErrorString("INVALID_USER_NAME_FIELD")."\n";

   if(empty($address))
      $this->strERR .= GetErrorString("INVALID_ADDRESS_FIELD")."\n";

   if(empty($pcode))
      $this->strERR .= GetErrorString("INVALID_POSTAL_CODE_FIELD")."\n";

   if(empty($state))
      $this->strERR .= GetErrorString("INVALID_STATE_FIELD")."\n";

   if(empty($country))
      $this->strERR .= GetErrorString("INVALID_COUNTRY_FIELD")."\n";

   if(empty($phone))
      $this->strERR .= GetErrorString("INVALID_PHONE_FIELD")."\n";

   //if(empty($fax))
   //   $this->strERR .= GetErrorString("INVALID_FAX_FIELD")."\n";

   if(($locked != 'NO') && ($locked != 'YES'))
      $this->strERR .= GetErrorString("INVALID_LOCKED_FIELD")."\n";

   if(! empty($this->strERR))
      return 0;

   $this->lastSQLCMD = "INSERT INTO ".SQL_USERS." (email,password,name,address,city,pcode,state,country,phone,fax,organisation,locked)
                        VALUES ('$email','$password','$name','$address','$city','$pcode','$state','$country','$phone','$fax','$org','$locked')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $userID = $this->dbh->GetFieldValue("id");
   $adminName = $_SERVER['REMOTE_USER'];

   $this->lastSQLCMD = "INSERT INTO user_hist (user_id,email,password,name,address,city,pcode,state,country,phone,fax,organisation,locked,admin)
                        VALUES ('$userID','$email','$password','$name','$address','$city','$pcode','$state','$country','$phone','$fax','$org','$locked','$adminName')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $userID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateUser
//
// [DESCRIPTION]:   Update users table
//
// [PARAMETERS]:    $userID=0, $email='', word='', $name='', $address='',
//                  $city='', $pcode='', $state='', $country='', $phone='',
//                  $fax='', $org='', $locked=''
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateUser($userID=0, $email='', $password='', $name='',
                    $address='', $city='', $pcode='', $state='',
                    $country='', $phone='', $fax='', $org='', $locked='')
{
   if(! preg_match("/^\d+$/", $userID))
      $this->strERR .= GetErrorString("INVALID_USERID_FIELD")."\n";

   if(empty($email) || !ereg(".+\@.+", $email))
      $this->strERR .= GetErrorString("INVALID_EMAIL_FIELD")."\n";

   if(empty($password))
      $this->strERR .= GetErrorString("INVALID_PASSWORD_FIELD")."\n";

   if(empty($name))
      $this->strERR .= GetErrorString("INVALID_USER_NAME_FIELD")."\n";

   if(empty($address))
      $this->strERR .= GetErrorString("INVALID_ADDRESS_FIELD")."\n";

   if(empty($pcode))
      $this->strERR .= GetErrorString("INVALID_POSTAL_CODE_FIELD")."\n";

   if(empty($state))
      $this->strERR .= GetErrorString("INVALID_STATE_FIELD")."\n";

   if(empty($country))
      $this->strERR .= GetErrorString("INVALID_COUNTRY_FIELD")."\n";

   if(empty($phone))
      $this->strERR .= GetErrorString("INVALID_PHONE_FIELD")."\n";

   //if(empty($fax))
   //   $this->strERR = GetErrorString("INVALID_FAX_FIELD")."\n";

   if(($locked != 'NO') && ($locked != 'YES'))
      $this->strERR .= GetErrorString("INVALID_LOCKED_FIELD")."\n";

   if(! empty($this->strERR))
      return false;

   // check if userID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_USERS." WHERE id='$userID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERID_NOT_FOUND");
      return false;
   }


   $this->lastSQLCMD = "UPDATE ".SQL_USERS." SET email='$email',password='$password',name='$name',address='$address',city='$city',pcode='$pcode',
                        state='$state',country='$country',phone='$phone',fax='$fax',organisation='$org',locked='$locked' WHERE id='$userID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   // TODO ?
   //if(! $this->dbh->GetAffectedRows())
   //{
   //   $this->strERR = GetErrorString("USER_FAILED_UPDATE");
   //   return false;
   //}


   $adminName = $_SERVER['REMOTE_USER'];

   $this->lastSQLCMD = "INSERT INTO user_hist (user_id,email,password,name,address,city,pcode,state,country,phone,fax,organisation,locked,admin)
                        VALUES ('$userID','$email','$password','$name','$address','$city','$pcode','$state','$country','$phone','$fax','$org','$locked','$adminName')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteUser
//
// [DESCRIPTION]:   Delete an entry from users table
//
// [PARAMETERS]:    $userID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteUser($userID=0)
{
   if(! preg_match("/^\d+$/", $userID))
   {
      $this->strERR = GetErrorString("INVALID_USERID_FIELD");
      return false;
   }

   // check if userID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_USERS." WHERE id='$userID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_USERS." WHERE id='$userID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   // TODO ?
   //if(! $this->dbh->GetAffectedRows())
   //{
   //   $this->strERR = GetErrorString("USER_FAILED_DELETE");
   //   return false;
   //}

   return true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUser
//
// [DESCRIPTION]:   Read data from users table and put it into an array variable
//
// [PARAMETERS]:    $userID, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUser($userID=0, &$arrayResult)
{
   if(! preg_match("/^\d+$/", $userID))
   {
      $this->strERR = GetErrorString("INVALID_USERID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_USERS." WHERE id='$userID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("USERID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]           = $this->dbh->GetFieldValue("id");
   $arrayResult["email"]        = $this->dbh->GetFieldValue("email");
   $arrayResult["password"]     = $this->dbh->GetFieldValue("password");
   $arrayResult["name"]         = $this->dbh->GetFieldValue("name");
   $arrayResult["address"]      = $this->dbh->GetFieldValue("address");
   $arrayResult["city"]         = $this->dbh->GetFieldValue("city");
   $arrayResult["pcode"]        = $this->dbh->GetFieldValue("pcode");
   $arrayResult["state"]        = $this->dbh->GetFieldValue("state");
   $arrayResult["country"]      = $this->dbh->GetFieldValue("country");
   $arrayResult["phone"]        = $this->dbh->GetFieldValue("phone");
   $arrayResult["fax"]          = $this->dbh->GetFieldValue("fax");
   $arrayResult["organisation"] = $this->dbh->GetFieldValue("organisation");
   $arrayResult["locked"]       = $this->dbh->GetFieldValue("locked");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserV1
//
// [DESCRIPTION]:   Read data from users table and put it into an array variable
//
// [PARAMETERS]:    $userID, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserV1($userID=0, &$arrayResult)
{
   if(! preg_match("/^\d+$/", $userID))
   {
      $this->strERR = GetErrorString("INVALID_USERID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_USERS." WHERE id='$userID'";

   if(! $this->dbh->Exec($this->lastSQLCMD, true))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("USERID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]           = $this->dbh->GetFieldValue("id");
   $arrayResult["group_login"]  = $this->dbh->GetFieldValue("group_login");
   $arrayResult["email"]        = $this->dbh->GetFieldValue("email");
   $arrayResult["password"]     = $this->dbh->GetFieldValue("password");
   $arrayResult["name"]         = $this->dbh->GetFieldValue("name");
   $arrayResult["address"]      = $this->dbh->GetFieldValue("address");
   $arrayResult["city"]         = $this->dbh->GetFieldValue("city");
   $arrayResult["pcode"]        = $this->dbh->GetFieldValue("pcode");
   $arrayResult["state"]        = $this->dbh->GetFieldValue("state");
   $arrayResult["country"]      = $this->dbh->GetFieldValue("country");
   $arrayResult["phone"]        = $this->dbh->GetFieldValue("phone");
   $arrayResult["fax"]          = $this->dbh->GetFieldValue("fax");
   $arrayResult["organisation"] = $this->dbh->GetFieldValue("organisation");
   $arrayResult["website"]      = $this->dbh->GetFieldValue("website");
   $arrayResult["username"]     = $this->dbh->GetFieldValue("username");
   $arrayResult["locked"]       = $this->dbh->GetFieldValue("locked");
   $arrayResult["signup_version"] = $this->dbh->GetFieldValue("signup_version");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllUsers
//
// [DESCRIPTION]:   Read data from users table and put it into an array variable
//                  key = userID, value = emailAddress
//
// [PARAMETERS]:    &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllUsers(&$arrayResult)
{
   $this->lastSQLCMD = "SELECT id,email FROM ".SQL_USERS."";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERS_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("email");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserProfiles
//
// [DESCRIPTION]:   Read data from profiles table and put it into an array variable
//                  key = profileID, value = profileName
//
// [PARAMETERS]:    $userID, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserProfiles($userID=0, &$arrayResult)
{
   if(! preg_match("/^\d+$/", $userID))
   {
      $this->strERR = GetErrorString("INVALID_USERID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM profiles WHERE user_id='$userID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_PROFILES_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("name");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Login
//
// [DESCRIPTION]:   Client log in procedure
//
// [PARAMETERS]:    $email=", $password=""
//
// [RETURN VALUE]:  Session key string or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Login($email="", $password="")
{
   if(empty($email))
      $this->strERR .= GetErrorString("INVALID_EMAIL_FIELD")."\n";

   if(empty($password))
      $this->strERR .= GetErrorString("INVALID_PASSWORD_FIELD")."\n";

   if(! empty($this->strERR))
      return 0;

   $this->lastSQLCMD = "SELECT id FROM ".SQL_USERS." WHERE email='$email' AND password='$password'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AUTHENTICATION_FAILED");
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $userID = $this->dbh->GetFieldValue('id');

   mt_srand($this->make_seed());
   $sessionKey = time().mt_rand().mt_rand().mt_rand();
   $hostIP = $_SERVER['REMOTE_ADDR'];

   $this->lastSQLCMD = "INSERT INTO logins (user_id,status,host_ip,session_key,cr_date) VALUES('$userID','SUCCESS','$hostIP','$sessionKey',now())";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $sessionKey;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUSerID
//
// [DESCRIPTION]:   Client logger verification
//
// [PARAMETERS]:    $sessionKey
//
// [RETURN VALUE]:  userID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserID($sessionKey="")
{
   if(empty($sessionKey))
      $this->strERR .= GetErrorString("INVALID_SESSION_KEY_FIELD")."\n";

   if(! empty($this->strERR))
      return 0;

   $this->lastSQLCMD = "SELECT user_id FROM logins WHERE session_key='$sessionKey'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_NOT_LOGGED_IN");
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $userID = $this->dbh->GetFieldValue('user_id');

   return $userID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

// Make random number
function make_seed()
{
   list($usec, $sec) = explode(' ', microtime());
   return (float) $sec + ((float) $usec * 100000000);
}

} // end of CUser class
?>

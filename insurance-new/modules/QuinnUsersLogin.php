<?php
/*****************************************************************************/
/*                                                                           */
/*  CQuinnUsersLogin class interface                                            */
/*                                                                           */
/*  (C) 2005 Velnic Daniel (dan@acrux.biz)                                   */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuinnUsersLogin
//
// [DESCRIPTION]:  CQuinnUsersLogin class interface
//
// [FUNCTIONS]:   true   | false AssertQuinnUsersLogin($qzQuoteID=0,$quoteUserDetailsID=0, $quotePassword='')
//                int    | false AddQuinnUsersLogin($qzQuoteID=0,$quoteUserDetailsID=0, $quotePassword='')
//                true   | false UpdateQuinnUsersLogin($quinnUsersLoginID=0,$qzQuoteID=0,$quoteUserDetailsID=0, $quotePassword='')
//                true   | false DeleteQuinnUsersLoginByID($quinnUsersLoginID=0)
//                array  | false GetQuinnUsersLoginByID($quinnUsersLoginID=0)
//                int    | false GetQuinnUsersLoginByUserDetID($quoteUserDetailsID=0)
//                int    | false GetHomeQuinnUsersLoginByUserDetID($quoteUserDetailsID=0)
//                int    | false GetQuinnUsersLoginByPass($quotePassword='')
//                int    | false GetQuinnUsersLoginByQzQuoteID($qzQuoteID=0)
//                array  | false GetAllQuinnUsersLogin()
//
//                Close();
//                GetError();
//                ShowError();
//
// [CREATED BY]:  Velnic Daniel (dan@acrux.biz)
//
// [MODIFIED]:    - [programmer (email) date]
//                  [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CQuinnUsersLogin
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuinnUsersLogin
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQuinnUsersLogin($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuinnUsersLogin
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    $qzQuoteID=0,$quoteUserDetailsID=0, $quotePassword=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertQuinnUsersLogin($qzQuoteID=0,$quoteUserDetailsID=0, $quotePassword='')
{
   $this->strERR = '';

   if(! preg_match('/([\d]+)/i',$qzQuoteID))
      $this->strERR .= GetErrorString("INVALID_QZ_QUOTE_ID")."\n";     
      
   if(! preg_match('/([\d]+)/i',$quoteUserDetailsID))
      $this->strERR .= GetErrorString("INVALID_QUOTE_USER_DETAIL_ID")."\n";           
  
   if(empty($quotePassword))
      $this->strERR .= GetErrorString("INVALID_PASSWORD")."\n";      

   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuinnUsersLogin
//
// [DESCRIPTION]:   Check if exist a car and add if not exist into QUINN_VEHICLE table
//
// [PARAMETERS]:    $qzQuoteID=0,$quoteUserDetailsID=0, $quotePassword=''
//
// [RETURN VALUE]:  $QuinnUsersLoginID | false
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuinnUsersLogin($qzQuoteID=0,$quoteUserDetailsID=0, $quotePassword='')
{

   if(! $this->AssertQuinnUsersLogin($qzQuoteID,$quoteUserDetailsID, $quotePassword))
      return false;

   
   $lastSqlCmd = "SELECT id FROM quinn_users_login WHERE qz_quote_id='$qzQuoteID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   if($this->dbh->GetRows())
   {
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      $QuinnUsersLoginID = $this->dbh->GetFieldValue("id");
      
      return $QuinnUsersLoginID;
   }

   $lastSqlCmd = "INSERT INTO quinn_users_login (qz_quote_id, quote_user_details_id, password) VALUES ('$qzQuoteID','$quoteUserDetailsID', '".addslashes($quotePassword)."');";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $lastSqlCmd = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQuinnUsersLogin
//
// [DESCRIPTION]:   Update a user data from the quinn_users_login table
//
// [PARAMETERS]:    $quinnUsersLoginID='',$qzQuoteID=0,$quoteUserDetailsID=0, $quotePassword=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQuinnUsersLogin($quinnUsersLoginID=0,$qzQuoteID=0,$quoteUserDetailsID=0, $quotePassword='')
{

   if(! preg_match("/^\d+$/", $quinnUsersLoginID))
   {
         $this->strERR = GetErrorString("INVALID_QUINN_USER_LOGIN_ID")."\n";
      return false;
   }

   if(! $this->AssertQuinnUsersLogin($qzQuoteID,$quoteUserDetailsID, $quotePassword))
      return false;

   $lastSqlCmd = "SELECT id FROM quinn_users_login WHERE id='$quinnUsersLoginID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUINN_USER_LOGIN_ID_NOT_FOUND");
      return false;
   }
 
   $lastSqlCmd = "UPDATE quinn_users_login SET qz_quote_id='$qzQuoteID',quote_user_details_id='$quoteUserDetailsID', password='".addslashes($quotePassword)."' WHERE id='$quinnUsersLoginID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
/////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQuinnUsersLogByID
//
// [DESCRIPTION]:   Delete an entry from the quinn_users_login table
//
// [PARAMETERS]:    $quinnUsersLoginID=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteQuinnUsersLoginByID($quinnUsersLoginID=0)
{
   if(! preg_match("/^\d+$/", $quinnUsersLoginID))
   {
      $this->strERR = GetErrorString("INVALID_QUINN_USER_LOGIN_ID");
      return false;
   }

   $lastSqlCmd = "SELECT id FROM quinn_users_login WHERE id='$quinnUsersLoginID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUINN_USER_LOGIN_ID_NOT_FOUND");
      return false;
   }

   $lastSqlCmd = "DELETE FROM quinn_users_login WHERE id='$quinnUsersLoginID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuinnUsersLoginByID
//
// [DESCRIPTION]:   Read data of a car from quinn_users_login table and put it into an array variable
//
// [PARAMETERS]:    $quinnUsersLoginID=0
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Velnic Daniel  (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuinnUsersLoginByID($quinnUsersLoginID)
{
   if(! preg_match("/^\d+$/", $quinnUsersLoginID))
   {
      $this->strERR = GetErrorString("INVALID_QUINN_USER_LOGIN_ID");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM quinn_users_login WHERE id='$quinnUsersLoginID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUINN_USER_LOGIN_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   
   $arrayResult = array();

   $arrayResult["id"]                    = trim($this->dbh->GetFieldValue("id"));
   $arrayResult["qz_quote_id"]           = trim($this->dbh->GetFieldValue("qz_quote_id"));
   $arrayResult["quote_user_details_id"] = trim($this->dbh->GetFieldValue("quote_user_details_id"));
   $arrayResult["password"]              = trim(stripslashes($this->dbh->GetFieldValue("password")));   

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuinnUsersLoginByUserDetID
//
// [DESCRIPTION]:   Read data of a car from quinn_users_login table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserDetailsID=0
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Velnic Daniel  (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuinnUsersLoginByUserDetID($quoteUserDetailsID=0)
{
   if(! preg_match("/^\d+$/", $quoteUserDetailsID))
   {
      $this->strERR = GetErrorString("INVALID_QUINN_USER_DETAILS_ID");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM quinn_users_login WHERE quote_user_details_id='$quoteUserDetailsID' order by id desc limit 1";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUINN_USER_DETAILS_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();
   
   $arrayResult["id"]                    = trim($this->dbh->GetFieldValue("id"));
   $arrayResult["qz_quote_id"]           = trim($this->dbh->GetFieldValue("qz_quote_id"));
   $arrayResult["quote_user_details_id"] = trim($this->dbh->GetFieldValue("quote_user_details_id"));
   $arrayResult["password"]              = trim(stripslashes($this->dbh->GetFieldValue("password")));   
   

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuinnUsersLoginByUserDetID
//
// [DESCRIPTION]:   Read data of a home from quinn_users_login table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserDetailsID=0
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Velnic Daniel  (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetHomeQuinnUsersLoginByUserEmail($userEmail=0)
{
   if(! preg_match("/^\d+$/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_QUINN_USER_DETAILS_ID");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM quinn_users_login qul,home_quote_user_details hqud WHERE hqud.id=qul.quote_user_details_id and hqud.quote_user_id='$quoteUserID' order by qul.id desc limit 1";

   print($lastSqlCmd);

   //print_r($this->dbh);
   //$fd = fopen("/tmp/lll.txt","a+");
   //fwrite($fd,"SQL : ".$lastSqlCmd);
   //fclose($fd);

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUINN_USER_DETAILS_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();
   
   //$arrayResult["id"]                    = trim($this->dbh->GetFieldValue("id"));
   $arrayResult["qz_quote_id"]           = trim($this->dbh->GetFieldValue("qz_quote_id"));
   $arrayResult["quote_user_details_id"] = trim($this->dbh->GetFieldValue("quote_user_details_id"));
   $arrayResult["password"]              = trim(stripslashes($this->dbh->GetFieldValue("password")));   
   $arrayResult["quote_user_id"]         = trim($this->dbh->GetFieldValue("quote_user_id"));   
   

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuinnUsersLoginByPass
//
// [DESCRIPTION]:   Read data of a car from quinn_users_login table and put it into an array variable
//
// [PARAMETERS]:    $quotePassword=''
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Velnic Daniel  (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuinnUsersLoginByPass($quotePassword='')
{
   if(empty( $quotePassword))
   {
      $this->strERR = GetErrorString("INVALID_QUINN_USER_PASSWORD");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM quinn_users_login WHERE password='".stripslashes($quotePassword)."'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUINN_USER_PASSWORD_NOT_FOUND");
      return false;
   }

   $arrayResult = array();   
   $index       = 0;
   while($this->dbh->MoveNext())
   {   
   	   $index++;
	   $arrayResult[$index]["id"]                    = trim($this->dbh->GetFieldValue("id"));
	   $arrayResult[$index]["qz_quote_id"]           = trim($this->dbh->GetFieldValue("qz_quote_id"));
	   $arrayResult[$index]["quote_user_details_id"] = trim($this->dbh->GetFieldValue("quote_user_details_id"));
	   $arrayResult[$index]["password"]              = trim(stripslashes($this->dbh->GetFieldValue("password")));   
   }   

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuinnUsersLoginByQzQuoteID
//
// [DESCRIPTION]:   Read data of a car from quinn_users_login table and put it into an array variable
//
// [PARAMETERS]:    $qzQuoteID=0
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Velnic Daniel  (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuinnUsersLoginByQzQuoteID($qzQuoteID=0)
{
   if(! preg_match("/^\d+$/", $qzQuoteID))
   {
      $this->strERR = GetErrorString("INVALID_QUINN_QZ_QUOTE_ID");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM quinn_users_login WHERE qz_quote_id='$qzQuoteID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUINN_QZ_QUOTE_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();
   
   $arrayResult["id"]                    = trim($this->dbh->GetFieldValue("id"));
   $arrayResult["qz_quote_id"]           = trim($this->dbh->GetFieldValue("qz_quote_id"));
   $arrayResult["quote_user_details_id"] = trim($this->dbh->GetFieldValue("quote_user_details_id"));
   $arrayResult["password"]              = trim(stripslashes($this->dbh->GetFieldValue("password")));   
   

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuinnUsersLogin
//
// [DESCRIPTION]:   Read data from quinn_users_login table and put it into an array variable
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Velnic Daniel  (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuinnUsersLogin()
{
   $lastSqlCmd = "SELECT * FROM quinn_users_login";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERS_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   $index       = 0;
   while($this->dbh->MoveNext())
   {
		$index++;
		$arrayResult[$index]["id"]                    = trim($this->dbh->GetFieldValue("id"));
		$arrayResult[$index]["qz_quote_id"]           = trim($this->dbh->GetFieldValue("qz_quote_id"));
		$arrayResult[$index]["quote_user_details_id"] = trim($this->dbh->GetFieldValue("quote_user_details_id"));
		$arrayResult[$index]["password"]              = trim(stripslashes($this->dbh->GetFieldValue("password")));   
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Show the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuinnUserIfExists
//
// [DESCRIPTION]:   Check if the user is into the database
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  false or int
//
// [CREATED BY]:    Velnic Daniel  (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuinnUserIfExists($emailAddress)
{
   if(empty($emailAddress))
   {
      $this->strERR = GetErrorString("INVALID_QUINN_FIRST_NAME");
      return false;
   }

   $lastSqlCmd = "select * from quote_user_details where email='$emailAddress'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   $nrOfRecords = $this->dbh->GetRows();

   if($nrOfRecords > 1)
      return true;

   return false;
}

}//end class CQuinnUsersLogin

?>

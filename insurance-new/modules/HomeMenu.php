<?php

/*****************************************************************************/
/*                                                                           */
/*  CMenu class interface                                                    */
/*                                                                           */
/*  (C) 2006 Eugen Savin (seugen@acrux.biz) 2006-11-14                       */
/*                                                                           */
/*****************************************************************************/
define("HOMEMENU_INCLUDED", "1");
error_reporting(E_ALL);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CHomeMenu
//
// [DESCRIPTION]:  CHomeMenu class interface
//
// [FUNCTIONS]:    void    SetMenu($menuArray=array());
//
//                 void    SetForwardStep($activeStep=-1);
//                 void    SetBackwardStep();
//                 void    SetFirstStep();
//                 void    SetLastStep();
//
//                 void    SetActiveStep($category=0, $insured=0, $catIndex=0);
//                 void    GetActiveStep();
//                 array   GetStepsHistory();
//
//                 void    SaveSessionMenu();
//                 void    ReadSession();
//                 void    RebuildStepsHistory();
//
// [CREATED BY]:   Eugen Savin (seugen@acrux.biz) 2006-11-14
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CHomeMenu
{
   var $session;
   var $activeStep;
   var $menuNav;
   var $strErr;

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CHomeMenu
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CHomeMenu($session=array())
{
   $this->session    = $session;
   $this->activeStep = "0:0:0";
   $this->strErr     = "";

   $this->menuNav = array ('intro',
                           'home',
                           'home occupancy',
                           'home security',
                           'insured',
                           'buildings cover',
                           'buildings claims',
                           'contents cover',
                           'contents claims',
                           'cover',
                           'quote summary',
                           'quote results');

   if(! is_array($this->session['_ACTIVE_STEP_HISTORY_']))
   {
      $this->session['_ACTIVE_STEP_HISTORY_'] = array();
      array_push($this->session['_ACTIVE_STEP_HISTORY_'], $this->activeStep);
   }

   if($this->session['_ACTIVE_STEP_'])
      $this->activeStep = $this->session['_ACTIVE_STEP_'];


   $this->SaveSessionMenu();
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: HaveBuildingsCover
//
// [DESCRIPTION]:   check to see if the insured wants have buildings cover
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2009-03-17
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function HaveBuildingsCover()
{
   if($this->session['_COVER_']['build_cover'] == "Y")
      return true;

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: HaveContentsCover
//
// [DESCRIPTION]:   check to see if the insured wants have contents cover
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2009-03-17
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function HaveContentsCover()
{
   if($this->session['_COVER_']['contents_cover'] == "Y")
      return true;

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: HaveBuildingsClaims
//
// [DESCRIPTION]:   check to see if we have buildings claims
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2009-03-17
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function HaveBuildingsClaims()
{
   if($this->session['_COVER_']['build_claims_nr'] > 0)
      return true;

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: HaveContentsClaims
//
// [DESCRIPTION]:   check to see if we have contents claims
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2009-03-17
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function HaveContentsClaims()
{
   if($this->session['_COVER_']['contents_claims_nr'] > 0)
      return true;

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: HaveClaims
//
// [DESCRIPTION]:   return the first driver that has claim(s)
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  integer (driver id)
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function HaveClaims()
{
   $insured = -1;

   for($i=0; $i<$this->GetInsuredNr(); $i++)
   {
      if($this->InsuredDeclaredClaims($i))
      {
         $insured = $i;
         break;
      }
   }

   return $insured;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InsuredDeclaredClaims
//
// [DESCRIPTION]:   check to see if the specified driver has claims
//
// [PARAMETERS]:    $insured=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InsuredDeclaredClaims($insured=0)
{
   $additionalInsured = '';

   if($insured > 0)
      $additionalInsured = "additional_";

   if($this->session['_INSURED_'][$insured][$additionalInsured.'had_any_accidents'] == "Y")
      return true;

   return false;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InsuredHasMoreClaims
//
// [DESCRIPTION]:   check to see if the specified driver has another claim
//
// [PARAMETERS]:    $insured=0, $claim=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InsuredHasMoreClaims($insured=0, $claim=0)
{
   if($this->session['_CLAIMS_'][$insured][$claim]['any_other_acc'] == "Y")
      return true;

   return false;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InsuredHasMoreConvictions
//
// [DESCRIPTION]:   check to see if the specified driver has another conviction
//
// [PARAMETERS]:    $insured=0, $convictionNr=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InsuredHasMoreConvictions($insured=0, $convictionNr=0)
{
   if($this->session['_CONVICTIONS_'][$insured][ $convictionNr]['any_other_conv'] == "Y")
      return true;

   return false;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetClaimsNr
//
// [DESCRIPTION]:   return the number of claims that the specified driver has
//
// [PARAMETERS]:    $insured=0
//
// [RETURN VALUE]:  integer
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetClaimsNr($insured=0)
{
   return count($this->session['_CLAIMS_'][$insured]);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: HaveConvictions
//
// [DESCRIPTION]:   return the first driver number that has conviction(s)
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  integer
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function HaveConvictions()
{
   $insured = -1;

   for($i=0; $i<$this->GetInsuredNr(); $i++)
   {
      if($this->InsuredDeclaredConvictions($i))
      {
         $insured = $i;
         break;
      }
   }

   return $insured;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InsuredDeclaredConvictions
//
// [DESCRIPTION]:   check to see if the specified driver has convictions
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InsuredDeclaredConvictions($insured=0)
{
  $additionalInsured = '';

   if($insured > 0)
      $additionalInsured = "additional_";

   if($this->session['_DRIVERS_'][$insured][$additionalInsured.'had_any_convictions'] == "Y")
      return true;

   return false;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetConvictionsNr
//
// [DESCRIPTION]:   return the number of convictions that the specified driver has
//
// [PARAMETERS]:    $insured=0
//
// [RETURN VALUE]:  integer
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetConvictionsNr($insured=0)
{
   return count($this->session['_CONVICTIONS_'][$insured]);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetInsuredNr
//
// [DESCRIPTION]:   return the number of drivers
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  integer
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetInsuredNr()
{
   return count($this->session['_INSURED_']);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDeclaredInsuredNr
//
// [DESCRIPTION]:   return the number of drivers declared by the proposer
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  integer
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDeclaredInsuredNr()
{
   $nrOfInsured = 0;

   if($this->session['_INSURED_'][0]['joint_insured'] == "N")
      $nrOfInsured = 1;
   else if($this->session['_INSURED_'][0]['joint_insured'] == "Y")
      $nrOfInsured = 2;

   return $nrOfInsured;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDeclaredVehicleModifications
//
// [DESCRIPTION]:   check if the user has any modifications to his car
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetDeclaredVehicleModifications()
{
   if($this->session['_VEHICLE_']['vehicle_modified_from_manufacturer'] == "Y")
	   return true;

   return false;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetForwardStep
//
// [DESCRIPTION]:   sets the next step as active
//
// [PARAMETERS]:    $activeStep=-1
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetForwardStep($activeStep=-1)
{
//die("acilea");
   // refresh session content
   $this->ReadSession();

   $foundNextStep = true;

   if($activeStep < 0)
      $activeStep = $this->activeStep;

   list($category, $insured, $catIndex) = split(":", $activeStep);

   switch($this->menuNav[$category])
   {
      // intro
      case 'intro':
            $this->SetActiveStep($category+1);
         break;

      // home
      case 'home':
            $this->SetActiveStep($category+1);
         break;

      // home occupancy
      case 'home occupancy':
            $this->SetActiveStep($category+1);
         break;

      // home security
      case 'home security':
            $this->SetActiveStep($category+1);
         break;

      // about you
      case 'insured':
            // if we have joint proposer
            if($insured < $this->GetDeclaredInsuredNr()-1)
            {
               $this->SetActiveStep($category, $insured+1);
               break;
            }

            // do we have buildings cover? if not skip 2 steps
            $buildingsCover = $this->HaveBuildingsCover();
            if(! $buildingsCover)
            {
               $this->SetActiveStep($category+3);
               break;
            } 

            $this->SetActiveStep($category + 1);
         break;

      // buildings cover
      case 'buildings cover':
            // do we have buildings claims? if not we skip one step
            // do we have contents cover? if not skip more 2 steps

            $buildingsClaims = $this->HaveBuildingsClaims();
            $contentsCover   = $this->HaveContentsCover();

            // skip 3 steps
            if(! $buildingsClaims AND ! $contentsCover)
            {
               $this->SetActiveStep($category+4);
               break;
            }
            //skip 1 step
            elseif(! $buildingsClaims)
            {
               $this->SetActiveStep($category+2);
               break;
            }

            $this->SetActiveStep($category + 1);
         break;

      // buildings claims
      case 'buildings claims':
            // do we have contents cover? if not skip more 2 steps
            $contentsCover   = $this->HaveContentsCover();

            // skip 2 steps
            if( ! $contentsCover)
            {
               $this->SetActiveStep($category+3);
               break;
            }

            $this->SetActiveStep($category + 1);
         break;

      // contents cover
      case 'contents cover':
            // do we have contents claims? if not we skip one step
            $contentsClaims = $this->HaveContentsClaims();
            if(! $contentsClaims)
            {
               $this->SetActiveStep($category+2);
               break;
            }

            $this->SetActiveStep($category + 1);
         break;

      // contents claims
      case 'contents claims':
            $this->SetActiveStep($category + 1);
         break;

      // cover details
      case 'cover':
         // quote summary
         $this->SetActiveStep($category + 1);
      break;

      // quote summary
      case 'quote summary':
         // quote results
         $this->SetActiveStep($category + 1);
      break;

      case 'quote results':
         $this->SetActiveStep($category + 1);
      break;

      default:
         $foundNextStep = false;
      break;

   } // switch($menu[$category])

   if(! $foundNextStep)
      return false;

   array_push($this->session['_ACTIVE_STEP_HISTORY_'], $this->activeStep);
   $this->SaveSessionMenu();

   return true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetBackwardStep
//
// [DESCRIPTION]:   sets the previous step as active
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetBackwardStep()
{
   // remove the last element
   array_pop($this->session['_ACTIVE_STEP_HISTORY_']);

   // get the last element in the list
   $lastHistIndex = count($this->session['_ACTIVE_STEP_HISTORY_']) - 1;

   // set new active step
   $this->activeStep = $this->session['_ACTIVE_STEP_HISTORY_'][$lastHistIndex];

   // save session
   $this->SaveSessionMenu();
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetFirstStep
//
// [DESCRIPTION]:   sets the first step as active
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetFirstStep()
{
   $this->activeStep = "0:0:0";
   $this->session['_ACTIVE_STEP_HISTORY_'] = array();
   array_push($this->session['_ACTIVE_STEP_HISTORY_'], "0:0:0");

   // save session
   $this->SaveSessionMenu();
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetLastStep
//
// [DESCRIPTION]:   sets the last step as active
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetLastStep()
{
   // get the last element in the list
   $lastHistIndex = count($this->session['_ACTIVE_STEP_HISTORY_']) - 1;

   // set new active step
   $this->activeStep = $this->session['_ACTIVE_STEP_HISTORY_'][$lastHistIndex];

   // save session
   $this->SaveSessionMenu();
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetActiveStep
//
// [DESCRIPTION]:   set the active step
//
// [PARAMETERS]:    $category=0, $insured=0, $catIndex=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetActiveStep($category=0, $insured=0, $catIndex=0)
{
   $this->activeStep = "$category:$insured:$catIndex";
}



//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetActiveStep
//
// [DESCRIPTION]:   return the active step
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetActiveStep()
{
   return $this->activeStep;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetStepsHistory
//
// [DESCRIPTION]:   return the steps history
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array of history steps
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetStepsHistory()
{
   return $this->session['_ACTIVE_STEP_HISTORY_'];
}



//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SaveSessionMenu
//
// [DESCRIPTION]:   saves the menu into the session
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SaveSessionMenu()
{
   $_SESSION["_ACTIVE_STEP_"]         = $this->GetActiveStep();
   $_SESSION["_ACTIVE_STEP_HISTORY_"] = $this->session['_ACTIVE_STEP_HISTORY_'];
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ReadSession
//
// [DESCRIPTION]:   read the session
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ReadSession()
{
   $this->session = $_SESSION;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: RebuildStepsHistory
//
// [DESCRIPTION]:   rebuilds the history of the forward steps
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function RebuildStepsHistory()
{
   $this->ReadSession();
   $this->SetFirstStep();

   while(true)
      if(! $this->SetForwardStep())
         break;

   // move to the previous step as this was the last OK step
   $this->SetBackwardStep();
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMenuInsured
//
// [DESCRIPTION]:   get the active driver
//
// [PARAMETERS]:    $activeStep
//
// [RETURN VALUE]:  none | array
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMenuInsured()
{
   list($category, $insured, $catIndex) = split(":", $this->activeStep);
   return $insured;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMenuCatIndex
//
// [DESCRIPTION]:   get the active catindex
//
// [PARAMETERS]:    $activeStep
//
// [RETURN VALUE]:  none | array
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMenuCatIndex()
{
   list($category, $insured, $catIndex) = split(":", $this->activeStep);
   return $catIndex;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMenuCategory()
//
// [DESCRIPTION]:   get the active category
//
// [PARAMETERS]:    $activeStep
//
// [RETURN VALUE]:  none | string
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMenuCategory()
{
   list($category, $insured, $catIndex) = split(":", $this->activeStep);
   return $this->menuNav[$category];
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFormAction
//
// [DESCRIPTION]:   get the form action
//
// [PARAMETERS]:    $formAction
//
// [RETURN VALUE]:  none | string
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFormAction()
{
   $formAction = strtolower($_POST["form_action"]);

   if(! $formAction)
      $formAction = "show";

   return $formAction;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetPageAction
//
// [DESCRIPTION]:   get the content of a driver. Can be either next or back
//
// [PARAMETERS]:    $insured
//
// [RETURN VALUE]:  none | array
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetPageAction()
{
   $action = $_POST["step"];

   if(! $action)
      $action = "next";

   return $action;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMenuNav
//
// [DESCRIPTION]:   get the navigation bar
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMenuNav()
{
   return $this->menuNav;
}

} // end CCarMenu

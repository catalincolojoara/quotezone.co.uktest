<?php
/*****************************************************************************/
/*                                                                           */
/*  CAffiliateNetworks class interface                                            */
/*                                                                           */
/*  (C) 2004 Eugen Savin (eugen@claspsolutions.com)                          */
/*                                                                           */
/*****************************************************************************/
// include_once "globals.adm.inc";

define("AFFILIATES_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";


//if(DEBUG_MODE)
//   error_reporting(E_ALL);
//else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CAffiliateNetworks
//
// [DESCRIPTION]:  CAffiliateNetworks class interface
//
// [FUNCTIONS]:    int  AddWlKeywords($cookieID=0, $wlID=0, $quoteTypeID=0, $crDate=0);
//                 bool UpdateWlKeywords($wlKwID=0, $cookieID=0, $quoteTypeID=0);
//                 bool DeleteWlKeywords($wlKwID=0);
//                 array|false GetWlKeywordsByID($wlKwID=0);
//                 array|false GetWlKeywordsByName($cookieID=0);
//                 array|false GetAllWlKeywords($startDate="", $endDate="");
//                 int GetWlKeywordsCount(($startDate="", $endDate="", $affiliateId=0, $quoteTypeID=0, $uniqueCookie=0));
//                 bool AssertWlKeywords($cookieID=0, $wlID=0, $quoteTypeID=0, $crDate="");
//
//                 void Close();
//                 string GetError();
//
// [CREATED BY]:   Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CAffiliateNetworks
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CAffiliateNetworks
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CAffiliateNetworks($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddWlKeywords
//
// [DESCRIPTION]:   Add new entry to the wl_keywords table
//
// [PARAMETERS]:    $cookieID=0, $wlID=0 $crDate = 0
//
// [RETURN VALUE]:  quoteID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
/*
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `affiliate_id` mediumint(8) unsigned NOT NULL auto_increment,
  `cookie_stats_id` bigint(20) NOT NULL default '',
  `cookie_id` bigint(20) NOT NULL default '0',
  `network` varchar(250) NOT NULL default '',
  `efid` varchar(250) NOT NULL default '',
  `subid` varchar(250) NOT NULL default '',
  `date` date NOT NULL default '0000-00-00';
  `time` time NOT NULL default '00:00:00';
*/
//////////////////////////////////////////////////////////////////////////////FE
function AddAffiliateNetwork($affiliateId=0, $cookieStatsId=0, $cookieId,$network,$efId,$subId)
{

	// check if new efid to set rates.
	$sqlCmd = "SELECT * FROM affiliate_networks where network='$network' and efid='$efId' limit 1";

	$this->dbh->Exec($sqlCmd);

	if (!$this->dbh->GetRows())
	{
		$this->AddDefaultRates4NewEfid($affiliateId,$network,$efId);
	}

	//

   $sqlCmd = "INSERT INTO affiliate_networks (affiliate_id,cookie_stats_id,cookie_id,network,efid,subid,date,time) VALUES ('$affiliateId','$cookieStatsId','$cookieId','$network','$efId','$subId',now(),now())";
//print $sqlCmd;
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $affNetId = $this->dbh->GetFieldValue("id");

   return $affNetId;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddDefaultRates4NewEfid($affiliateId,$network,$efid)
{

	include_once "AffiliateNetworkRates.php";
	include_once "Affiliates.php";
	include_once "/home/www/customers.quotezone.co.uk/modules/UserSystems.php";
	include_once "/home/quotezone/www/nw/networkDefaultRates.php";
	$objAffNetRates = new CAffiliateNetworkRate();
	$objAffiliates  = new CAffiliates();
	$objUserSystems = new CUserSystem();

	$affiliateDetails = $objAffiliates->GetAffiliatesByID($affiliateId);
	$userID = $affiliateDetails['user_id'];

	$systems = $objUserSystems->GetAllUserSystems($userID,'affiliate');

	$now = date("Y-m-d");

	foreach ($systems as $qTypeId=>$qTypeName)
	{
		if (!$objAffNetRates->AddAffiliateNetworkRate($userID,$efid,$qTypeId,$affiliateNetworkRatesArray[$network][$qTypeId],0,$now))
			print $objAffNetRates->GetError();
	}

	return;

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddWlKeywords
//
// [DESCRIPTION]:   Add new entry to the wl_keywords table
//
// [PARAMETERS]:    $cookieID=0, $wlID=0 $crDate = 0
//
// [RETURN VALUE]:  quoteID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
/*
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `affiliate_id` mediumint(8) unsigned NOT NULL auto_increment,
  `log_id` bigint(20) NOT NULL default '0',
  `site_id` bigint(20) NOT NULL default '0',
  `os` varchar(250) NOT NULL default '',
  `browser` varchar(250) NOT NULL default '',
  `user_agent` varchar(250) NOT NULL default '',
  `date` date NOT NULL default '0000-00-00';
  `time` time NOT NULL default '00:00:00';
*/
//////////////////////////////////////////////////////////////////////////////FE
function AddAffiliateNetworkTrackerFields($affiliateId=0, $efid='',$logId=0, $siteId=0,$hostIP,$os='',$browser='',$userAgent='')
{

   $sqlCmd = "INSERT INTO affiliate_networks_tracker (affiliate_id,efid,log_id,site_id,host_ip,os,browser,user_agent,date,time) VALUES ('$affiliateId','$efid','$logId','$siteId','$hostIP','$os','$browser','$userAgent',now(),now())";
//print $sqlCmd;
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $affNetTrId = $this->dbh->GetFieldValue("id");

   return $affNetTrId;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

} // end of CAffiliateNetworks class
?>

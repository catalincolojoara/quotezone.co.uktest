<?php

/*****************************************************************************/
/*                                                                           */
/*  CHelp class interface                                                     */
/*                                                                           */
/*  (C) 2004 Eugen Savin (seugen@abc.ro)                                     */
/*                                                                           */
/*****************************************************************************/

define("URL_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CHelp
//
// [DESCRIPTION]:  CHelp class interface
//
// [FUNCTIONS]:    int  AddHelp($siteID=0, $step=0, $url='');
//                 bool UpdateHelp($urlID=0, $siteID=0, $step=0, $url='');
//                 bool DeleteHelp($urlID=0);
//                 bool GetHelp($urlID=0, &$arrayResult);
//                 bool GetAllHelps($siteID=0, &$arrayResult);
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CHelp
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last url error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CHelp
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CHelp($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddHelp
//
// [DESCRIPTION]:   Add new entry to the urls table
//
// [PARAMETERS]:    $siteID, $step, $url
//
// [RETURN VALUE]:  HelpID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddHelp($paramID=0, $title='', $description='', $example='')
{
   if(! preg_match("/^\d+$/", $paramID))
   {
      $this->strERR = GetErrorString("INVALID_PARAMID_FIELD");
      return 0;
   }

   if(empty($title))
   {
      $this->strERR = GetErrorString("INVALID_TITLE_FIELD");
      return 0;
   }
   
      if(empty($description))
   {
      $this->strERR = GetErrorString("INVALID_DESCRIPTION_FIELD");
      return 0;
   }

   if(empty($example))
   {
      $this->strERR = GetErrorString("INVALID_EXAMPLE_FIELD");
      return 0;
   }
   
   $this->lastSQLCMD = "INSERT INTO ".SQL_HELP." (param_help_id,title,description,example) VALUES ('$paramID','$title','$description','$example')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateHelp
//
// [DESCRIPTION]:   Update urls table
//
// [PARAMETERS]:    $urlID, $siteID, $step, $url
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateHelp($helpID=0, $paramID=0, $title='', $description='', $example='')
{
   if(! preg_match("/^\d+$/", $helpID))
   {
      $this->strERR = GetErrorString("INVALID_HELPID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $paramID))
   {
      $this->strERR = GetErrorString("INVALID_PARAMID_FIELD");
      return false;
   }

   if(empty($title))
   {
      $this->strERR = GetErrorString("INVALID_TITLE_FIELD");
      return 0;
   }
   
   if(empty($description))
   {
      $this->strERR = GetErrorString("INVALID_DESCRIPTION_FIELD");
      return 0;
   }

   if(empty($example))
   {
      $this->strERR = GetErrorString("INVALID_EXAMPLE_FIELD");
      return 0;
   }
   
   // check if HelpID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_HELP." WHERE id='$helpID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("HELPID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_HELP." SET param_help_id='$paramID',title='$title',description='$description',example='$example' WHERE id='$helpID'";
   
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteHelp
//
// [DESCRIPTION]:   Delete an entry from urls table
//
// [PARAMETERS]:    $urlID,
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteHelp($helpID=0)
{
   if(! preg_match("/^\d+$/", $helpID))
   {
      $this->strERR = GetErrorString("INVALID_HELPID_FIELD");
      return false;
   }

   // check if HelpID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_HELP." WHERE id='$helpID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("HELPID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_HELP." WHERE id='$helpID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetHelp
//
// [DESCRIPTION]:   Read data from urls table and put it into an array variable
//
// [PARAMETERS]:    $urlID=0, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetHelp($helpID=0, &$arrayResult)
{
   if(! preg_match("/^\d+$/", $helpID))
   {
      $this->strERR = GetErrorString("INVALID_HELPID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_HELP." WHERE id='$helpID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("HELPID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]            = $this->dbh->GetFieldValue("id");
   $arrayResult["param_help_id"] = $this->dbh->GetFieldValue("param_help_id");
   $arrayResult["title"]         = $this->dbh->GetFieldValue("title");
   $arrayResult["description"]   = $this->dbh->GetFieldValue("description");
   $arrayResult["example"]       = $this->dbh->GetFieldValue("example");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllHelps
//
// [DESCRIPTION]:   Read data from urls table and put it into an array variable
//                  key = siteID, value = urlAddress
//
// [PARAMETERS]:    $siteID=0, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllHelps( &$arrayResult)
{
   $this->lastSQLCMD = "SELECT id,param_help_id FROM ".SQL_HELP." ORDER BY param_help_id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("HELPS_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("param_help_id");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

} // end of CHelp class
?>

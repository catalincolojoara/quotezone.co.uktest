<?php
/*****************************************************************************/
/*                                                                           */
/* File class implementation                                                 */
/*                                                                           */
/*  (C) 2004 Eugen Savin (eugen@claspsolutions.com)                          */
/*                                                                           */
/*****************************************************************************/

define("FILE_INCLUDED", "1");

//if(DEBUG_MODE)
//   error_reporting(1);
//else
//   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CFile
//
// [DESCRIPTION]:  Support for operationd with files, PHP version
//
// [FUNCTIONS]:    bool   Open($fileName = "", $fileMode = "r");
//                 bool   OpenRemote($fileName = "");
//                 bool   Create($fileName = "");
//                 bool   Touch($fileName = "");
//                 string|false Read($bytesToRead = 0);
//                 int    Write($data = "", [$bytesToWrite = 0]);
//
//                 bool   Flush();
//                 bool   Lock($lockFlag = LOCK_SH);
//                 bool   Unlock();
//
//                 bool   Seek($offset = 0, $whence = SEEK_SET);
//                 bool   SeekToBegin();
//                 bool   SeekToEnd();
//
//                 bool   Delete([$fileName = "")];
//                 bool   Remove($fileName = "");
//                 bool   Move($fileName = "", $newFileName = "", $forceRename = false);
//                 bool   Exists($fileName = "");
//                 array|false Stat([$fileName = ""]);
//
//                 bool IsEOF();
//                 bool IsFile($fileName = "");
//                 bool IsLink($fileName = "");
//                 bool IsDir($fileName = "");

//                 array|false ListFiles($dirName = "", $limit = 0);
//                 array|false ListDirs($path="", $includeSubDirs=false);
//                 array|false ListHttpFiles($urlAddr="");
//                 array|false ListHttpDirs($urlAddr="");
//                 string GetUrl($urlAddr="");
//
//                 string GenerateFileName($dirName = "", $createFile=false);
//
//                 void   SetFileName($fileName = "");
//                 bool   SetFileMode($fileMode = "r+");
//                 void   SetDebug($debug = 1);
//
//                 string GetFileName();
//                 string GetFileMode();
//                 int    GetSize([$fileName = ""]);
//                 int    GetPosition();
//                 int    GetDebug();
//
//                 void   Close();
//                 string GetError()
//
//                 void SetDebug($debug = 1);
//                 int  GetDebug();
//
// [CREATED BY]:   Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CFile
{
   var $fileName;     // file name
   var $fileMode;     // file mode
   var $fileHandle;   // file hanlde

   var $debug;        // debug mode
   var $strERR;       // last error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CFile
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CFile()
{
   $this->fileName   = "";
   $this->fileMode   = "";
   $this->fileHandle = 0;

   $this->debug  = 0;
   $this->strERR = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Open
//
// [DESCRIPTION]:   Open a local file
//
// [PARAMETERS]:    $fileName = "", $fileMode = "r"
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Open($fileName = "", $fileMode = "r")
{
   $this->strERR = "";

   if($this->fileHandle)
   {
      $this->strERR = "[$fileName] is already opened. Please close this file first!";
      return false;
   }

   if(! $this->SetFileMode($fileMode))
      return false;

   if(ereg("r|r\+", $fileMode))
   {
      if(! $this->Exists($fileName))
      {
         $this->strERR = "file name [$fileName] not found";
         return false;
      }
   }

   $this->SetFileName($fileName);

   $this->fileHandle = fopen($fileName, $fileMode);

   if(! $this->fileHandle)
   {
      $this->strERR = "cannot open file name [$fileName]";
      $this->Close();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: OpenRemote
//
// [DESCRIPTION]:   Open a remote file
//
// [PARAMETERS]:    $fileName = ""
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function OpenRemote($fileName = "")
{
   $this->strERR = "";

   if($this->fileHandle)
   {
      $this->strERR = "[$fileName] is already opened. Please close this file first!";
      return false;
   }

   if(! $this->SetFileMode("r"))
      return false;

   $this->SetFileName($fileName);

   $this->fileHandle = fopen($fileName, $fileMode);

   if(! $this->fileHandle)
   {
      $this->strERR = "cannot open remote file [$fileName]";
      $this->Close();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Create
//
// [DESCRIPTION]:   Create a local file
//
// [PARAMETERS]:    $fileName = "", $forceCreate = false
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Create($fileName = "", $forceCreate = false)
{
   $this->strERR = "";

   if($this->Exists($fileName))
   {
      if(! $forceCreate)
      {
         $this->strERR = "file name [$fileName] already exists. Please set the force create flag if you want to overwrite it!";
         return false;
      }
   }

   $fileHandle = fopen($fileName, "w+");

   if(! $fileHandle)
   {
      $this->strERR = "cannot create file name [$fileName]";
      return false;
   }

   fclose($fileHandle);

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Touch
//
// [DESCRIPTION]:   Set the access and modification time of the file
//
// [PARAMETERS]:    $fileName = ""
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Touch($fileName = "")
{
   if(empty($fileName))
      $fileName = $this->fileName;

   if(empty($fileName))
   {
      $this->strERR = "no file specified. Cannot perform touch() operation.";
      return false;
   }

   if(! touch($fileName))
   {
      $this->strERR = "cannot change modification time of [$fileName]";
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Read
//
// [DESCRIPTION]:   Read data from file in the amount of the specified bytes
//
// [PARAMETERS]:    $bytesToRead = 0
//
// [RETURN VALUE]:  Data read if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Read($bytesToRead = 0)
{
   if(! ereg("[0-9]+", $bytesToRead))
   {
      $this->strERR = "invalid read bytes value. Use only numbers please!";
      return false;
   }

   if(! $this->fileHandle)
   {
      $this->strERR = "no file is opened. Cannot perform read() operation.";
      return false;
   }

   $data = fread($this->fileHandle, $bytesToRead);

   return $data;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Write
//
// [DESCRIPTION]:   Write data up to the specified bytes value
//
// [PARAMETERS]:    $data = "" [$bytesToWrite = 0]
//
// [RETURN VALUE]:  Number of written bytes if success, < 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Write($data = "", $bytesToWrite = 0)
{
   if(! ereg("[0-9]+", $bytesToWrite))
   {
      $this->strERR = "invalid write bytes value. Use only numbers please!";
      return -1;
   }

   if(! $this->fileHandle)
   {
      $this->strERR = "no file is opened. Cannot perform write() operation.";
      return -2;
   }

   if($bytesToWrite)
      $bytes = fwrite($this->fileHandle, $data, $bytesToWrite);
   else
      $bytes = fwrite($this->fileHandle, $data);

   if($bytes === FALSE)
   {
      $this->strERR = "cannot write data to file [$this->fileName]";
      return -3;
   }

   return $bytes;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Flush
//
// [DESCRIPTION]:   Flush the file
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Flush()
{
   if(! $this->fileHandle)
   {
      $this->strERR = "no file is opened. Cannot perform flush() operation.";
      return false;
   }

   if(! fflush($this->fileHandle))
   {
      $this->strERR = "cannot flush file name [$this->fileName]";
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Lock
//
// [DESCRIPTION]:   Lock the file
//
// [PARAMETERS]:    $lockFlag = LOCK_SH
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Lock($lockFlag = LOCK_SH)
{
   if(! $this->fileHandle)
   {
      $this->strERR = "no file is opened. Cannot perform flock() operation.";
      return false;
   }

   if(! flock($this->fileHandle, $lockFlag))
   {
      $this->strERR = "cannot lock file name [$this->fileName]";
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Unlock
//
// [DESCRIPTION]:   UnLock the file
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Unlock()
{
   if(! $this->fileHandle)
   {
      $this->strERR = "no file is opened. Cannot perform flock() operation.";
      return false;
   }

   if(! flock($this->fileHandle, SH_UNLOCK))
   {
      $this->strERR = "cannot lock file name [$this->fileName]";
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Seek
//
// [DESCRIPTION]:   Move the file cursor to the specified position
//
// [PARAMETERS]:    $offset = 0, $whence = SEEK_SET
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Seek($offset = 0, $whence = SEEK_SET)
{
   if(! ereg("[0-9]+", $offset))
   {
      $this->strERR = "invalid offset value. Use only numbers please!";
      return false;
   }

   if(! $this->fileHandle)
   {
      $this->strERR = "no file is opened. Cannot perform fseek() operation.";
      return false;
   }

   if(fseek($this->fileHandle, $offset, $whence) == -1)
   {
      $this->strERR = "cannot seek into file name [$this->fileName]";
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SeekToBegin
//
// [DESCRIPTION]:   Move the file cursor to beginning of the file
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SeekToBegin()
{
   return $this->Seek(0);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SeekToEnd
//
// [DESCRIPTION]:   Move the file cursor to end of the file
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SeekToEnd()
{
   return $this->Seek(0, SEEK_END);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Delete
//
// [DESCRIPTION]:   Delete the specified file name
//
// [PARAMETERS]:    [$fileName = ""]
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Delete($fileName = "")
{
   if(empty($fileName))
      $fileName = $this->fileName;

   if(empty($fileName))
   {
      $this->strERR = "no file specified. Cannot perform unlink() operation.";
      return -1;
   }

   if(! $this->Exists($fileName))
   {
      $this->strERR = "file name [$fileName] not found";
      return false;
   }

   // close the file before the delete operation
   if($fileName == $this->fileName)
      $this->Close();

   if(! unlink($fileName))
   {
      $this->strERR = "cannot delete file name [$fileName]";
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Remove
//
// [DESCRIPTION]:   Delete the specified file name
//
// [PARAMETERS]:    [$fileName = ""]
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Remove($fileName = "")
{
   return $this->Delete($fileName);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Rename
//
// [DESCRIPTION]:   Rename a file name to a new file name
//
// [PARAMETERS]:    $fileName = "", $newFileName = "", $forceRename = false
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Rename($fileName = "", $newFileName = "", $forceRename = false)
{
   if(! $this->Exists($fileName))
   {
      $this->strERR = "file name [$fileName] not found";
      return false;
   }

   if(! $forceRename)
   {
      if($this->Exists($newFileName))
      {
         $this->strERR = "new file name [$newFileName] already exists. Please set the force rename flag if you want to overwrite it!";
         return false;
      }
   }

   if(! rename($fileName, $newFileName))
   {
      $this->strERR = "cannot rename file name [$fileName] into [$newFileName]";
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Move
//
// [DESCRIPTION]:   Rename a file name to a new file name
//
// [PARAMETERS]:    $fileName = "", $newFileName = "", $forceRename = false
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Move($fileName = "", $newFileName = "", $forceRename = false)
{
   return $this->Rename($fileName, $newFileName, $forceRename);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Exists
//
// [DESCRIPTION]:   Check if a file name exists
//
// [PARAMETERS]:    $fileName = ""
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Exists($fileName = "")
{
   return file_exists($fileName);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Stat
//
// [DESCRIPTION]:   Read file status
//
// [PARAMETERS]:    [$fileName = ""]
//
// [RETURN VALUE]:  Status array if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Stat($fileName = "")
{
   if(empty($fileName))
      $fileName = $this->fileName;

   if(empty($fileName))
   {
      $this->strERR = "no file is specified. Cannot perform stat() operation.";
      return -1;
   }

   if(! $this->Exists($fileName))
   {
      $this->strERR = "file name [$fileName] not found";
      return false;
   }

   clearstatcache($fileName);
   $statArray = stat($fileName);

   if(! $statArray)
   {
      $this->strERR = "cannot stat file name [$fileName]";
      return false;
   }

   return $statArray;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: IsEOF
//
// [DESCRIPTION]:   Detects the EOF (end of file)
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function IsEOF()
{
   if(! $this->fileHandle)
   {
      $this->strERR = "no file is opened. Cannot perform feof() operation.";
      return true;
   }

   if(feof($this->fileHandle))
      return true;

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: IsFile
//
// [DESCRIPTION]:   Test if the argument is a file
//
// [PARAMETERS]:    $fileName = ""
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function IsFile($fileName = "")
{
   if(! $this->Exists($fileName))
   {
      $this->strERR = "file name [$fileName] not found";
      return false;
   }

   if(! is_file($fileName))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: IsLink
//
// [DESCRIPTION]:   Test if the argument is a link
//
// [PARAMETERS]:    $fileName = ""
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function IsLink($fileName = "")
{
   if(! $this->Exists($fileName))
   {
      $this->strERR = "file name [$fileName] not found";
      return false;
   }

   if(! is_link($fileName))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: IsDir
//
// [DESCRIPTION]:   Test if the argument is a directory
//
// [PARAMETERS]:    $fileName = ""
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function IsDir($fileName = "")
{
   if(! $this->Exists($fileName))
   {
      $this->strERR = "dir name [$fileName] not found";
      return false;
   }

   if(! is_dir($fileName))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ListFiles
//
// [DESCRIPTION]:   List the files contained by a directory
//
// [PARAMETERS]:    $dirName = "", $limit = 0
//
// [RETURN VALUE]:  Array containing the files name if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ListFiles($dirName = "", $limit = 0)
{
   if(! $this->IsDir($dirName))
      return false;

   $hDir = opendir($dirName);

   if(! $hDir)
   {
      $this->strERR = "cannot open dir name [$dirName]";
      return false;
   }

   $files = array();
   $fileCounter = 0;

   while(false !== ($fileName = readdir($hDir)))
   {
      if($fileName == '.' || $fileName == '..')
         continue;

      if(is_dir($dirName."/".$fileName))
         continue;

      array_push($files, $fileName);
      $fileCounter++;

      if($limit)
         if($fileCounter == $limit)
            break;
   }

   return $files;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ListDirs
//
// [DESCRIPTION]:   List the directories contained by a path
//
// [PARAMETERS]:    $path="", $includeSubDirs=false
//
// [RETURN VALUE]:  Array containing the files name if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ListDirs($path="", $includeSubDirs=false)
{
   if(! $this->IsDir($path))
      return false;

   $hDir = opendir($path);

   if(! $hDir)
   {
      $this->strERR = "cannot open dir name [$path]";
      return false;
   }

   $dirs = array();
   while(false !== ($dirName = readdir($hDir)))
   {
      if($dirName == '.' || $dirName == '..')
         continue;

      if(! is_dir($path."/".$dirName))
         continue;

      $dirs[$dirName] = '';

      if($includeSubDirs)
      {
         $subDirs = $this->ListDirs($path."/".$dirName, $includeSubDirs);

         if($subDirs !== false)
            if(count($subDirs))
               $dirs[$dirName] = $subDirs;
      }
   }

   return $dirs;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ListHttpFiles
//
// [DESCRIPTION]:   List the files listed by an apache web site
//                  listing dir
//
// [PARAMETERS]:    $urlAddr=""
//
// [RETURN VALUE]:  Array containing the files name if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ListHttpFiles($urlAddr="")
{
   $myFile = new CFile();

   if(! $myFile->OpenRemote($urlAddr))
   {
      $this->strERR = $myFile->GetError();
      return false;
   }

   $remoteText = "";
   while(! $myFile->IsEOF())
      $remoteText .=  $myFile->Read(1024);

   $myFile->Close();

   $files = array();

   if(preg_match_all("/alt=\"\[\s\s\s\]\">\s\<a href=\"([^\"]+).*\s(\d+\-\w{3}\-\d{4}\s\d{2}:\d{2})/", $remoteText, $matches))
   {
      for($i=0; $i<count($matches[0]); $i++)
         $files[$matches[1][$i]] = $matches[2][$i];
   }

   return $files;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ListHttpDirs
//
// [DESCRIPTION]:   List the directories listed by an apache web site
//                  listing dir
//
// [PARAMETERS]:    $urlAddr=""
//
// [RETURN VALUE]:  Array containing the files name if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ListHttpDirs($urlAddr="")
{
   $myFile = new CFile();

   if(! $myFile->OpenRemote($urlAddr))
   {
      $this->strERR = $myFile->GetError();
      return false;
   }

   $remoteText = "";
   while(! $myFile->IsEOF())
      $remoteText .=  $myFile->Read(1024);

   $myFile->Close();

   $dirs = array();

   if(preg_match_all("/alt=\"\[DIR\]\">\s\<a href=\"([^\/]+).*\s(\d+\-\w{3}\-\d{4}\s\d{2}:\d{2})/", $remoteText, $matches))
   {
      for($i=0; $i<count($matches[0]); $i++)
         $dirs[$matches[1][$i]] = $matches[2][$i];
   }

   return $dirs;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUrl
//
// [DESCRIPTION]:   Downloads a URL address from a remote location
//
// [PARAMETERS]:    $urlAddr=""
//
// [RETURN VALUE]:  String with the URL's contents
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUrl($urlAddr="")
{
   $myFile = new CFile();

   if(! $myFile->OpenRemote($urlAddr))
   {
      $this->strERR = $myFile->GetError();
      return;
   }

   $urlContents = "";
   while(! $myFile->IsEOF())
      $urlContents .=  $myFile->Read(1024);

   $myFile->Close();

   return $urlContents;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GenerateFileName
//
// [DESCRIPTION]:   Generates and creates a unique file name into the specified
//                  directory or in the current directory if dir is empty
//
// [PARAMETERS]:    $dirName = "", $createFile=false
//
// [RETURN VALUE]:  The file name as string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GenerateFileName($dirName = "", $createFile=false)
{
   if(empty($dirName))
      $dirName = ".";

   $fileName = time();

   for($i=0; $i<1000; $i++)
   {
      mt_srand($this->make_seed());
      $randomNr = mt_rand(100000,999999);

      if(! $this->Exists($dirName."/".$fileName."_".$randomNr))
      {
         $fileName .= "_".$randomNr;

         if($createFile)
            if(! $this->Create($dirName."/".$fileName))
               return;

         break;
      }
   }

   return $fileName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetFileName
//
// [DESCRIPTION]:   Set the file name
//
// [PARAMETERS]:    $fileName = ""
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetFileName($fileName = "")
{
   $this->fileName = $fileName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetFileMode
//
// [DESCRIPTION]:   Set the file mode
//
// [PARAMETERS]:    $fileMode = ""
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetFileMode($fileMode = "r+")
{
   if(! ereg("(r|r\+|w|w\+|a|a\+)", $fileMode))
   {
      $this->strERR = "invalid open file mode [$fileMode]. Valid modes are: r, r+, w, w+, a, a+";
      return false;
   }

   // windows compatibility
   $fileMode .= "b";

   $this->fileMode = $fileMode;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetDebug
//
// [DESCRIPTION]:   Set the debug mode
//
// [PARAMETERS]:    $debug = 1
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetDebug($debug = 1)
{
   if(empty($debug))
      return;

    if($debug == 0 || $debug == 1)
      $this->debug = $debug;

   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFileName
//
// [DESCRIPTION]:   Return the file name
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  File name string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFileName()
{
   return $this->fileName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFileMode
//
// [DESCRIPTION]:   Return the file mode
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  File mode string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFileMode()
{
   return $this->fileMode;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSize
//
// [DESCRIPTION]:   Return the file name size
//
// [PARAMETERS]:    [$fileName = ""]
//
// [RETURN VALUE]:  The size of the file name if success, < 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSize($fileName = "")
{
   $fileSize = 0;

   if(empty($fileName))
      $fileName = $this->fileName;

   if(empty($fileName))
   {
      $this->strERR = "no file is specified. Cannot perform filesize() operation.";
      return -1;
   }

   if(! $this->Exists($fileName))
   {
      $this->strERR = "file name [$fileName] not found";
      return -2;
   }

   clearstatcache($fileName);
   $fileSize = filesize($fileName);

   if($fileSize === FALSE)
   {
      $this->strERR = "cannot do filesize() on file name [$fileName]";
      return -3;
   }

   return $fileSize;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetPosition
//
// [DESCRIPTION]:   Return file's cursor position
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  The positoin of the cursor if success, < 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetPosition()
{
   if(! $this->fileHandle)
   {
      $this->strERR = "no file is opened. Cannot perform ftell() operation.";
      return -1;
   }

   $pos = ftell($this->fileHandle);

   if($pos === FALSE)
   {
      $this->strERR = "cannot do ftell(), file name [$this->fileName]";
      return -2;
   }

   return $pos;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDebug
//
// [DESCRIPTION]:   Return the debug mode
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Debug mode number
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDebug()
{
   return $this->debug;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close file handle
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
   fclose($this->fileHandle);

   $this->fileHandle = 0;
   $this->fileName   = "";
   $this->fileMode   = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
   return $this->strERR;
}

// Make random number
function make_seed()
{
   list($usec, $sec) = explode(' ', microtime());
   return (float) $sec + ((float) $usec * 100000000);
}

} // end of class CFile

?>
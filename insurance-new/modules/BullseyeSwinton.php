<?php
/*****************************************************************************/
/*                                                                           */
/*  CBSTop class interface                                               */
/*                                                                           */
/*  (C) 2007 Moraru Valeriu (vali@acrux.biz)                                   */
/*                                                                           */
/*****************************************************************************/
include_once "errors.inc";
//include_once "globals.inc";
include_once "MySQL.php";
//error_reporting(E_ALL);
//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CBSTop
//
// [DESCRIPTION]:  CBSTop class interface
//
// [FUNCTIONS]:    bool        Assert($logID='',$quoteReference='',$topPosition='');
//                 int         Add($logID='',$quoteReference='',$topPosition='');
//
//                 Close();
//                 string      GetError();
//
// [CREATED BY]:   Moraru Valeriu (vali@acrux.biz) 05-06-2007
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CBSTop
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CBSTop
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 05-06-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CBSTop($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Assert
//
// [DESCRIPTION]:   Verify to see if the requested fields are completed and in corect format.
//
// [PARAMETERS]:    $logID,$quoteReference,$topPosition
//
// [RETURN VALUE]:  bool
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 05-06-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Assert($logID='',$quoteReference='',$topPosition='')
{
   if(!preg_match("/^\d+$/",$logID))
   {
      $this->strERR = GetErrorString("LOG_ID_ERROR");
      return false;
   }
   
   if(!preg_match("/^\d+$/",$quoteReference))
   {
      $this->strERR = GetErrorString("LOG_ID_ERROR");
      return false;
   }

   if(!preg_match("/^\d$/",$topPosition))
   {
      $this->strERR = GetErrorString("LOG_ID_ERROR");
      return false;
   }
   
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Add
//
// [DESCRIPTION]:   Add new entry into the bullseye_swinton_top table, if exists specified quote reference return that id 
//
// [PARAMETERS]:    $logID,$quoteReference,$topPosition
//
// [RETURN VALUE]:  int
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 05-06-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Add($logID='',$quoteReference='',$topPosition='')
{
   if(! $this->Assert($logID,$quoteReference,$topPosition))
      return false;

   
   // check if quoteReference already exists
   $sqlCmd = "SELECT id FROM bullseye_swinton_top WHERE quote_reference = '$quoteReference'";


   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

//print $sqlCmd;   
   if($this->dbh->GetRows())
      return false;
   
   // quoteReference is new quote reference
   $sqlCmd = "INSERT INTO bullseye_swinton_top(log_id,quote_reference,top_position) VALUES ('$logID','$quoteReference','$topPosition')";
      
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $sqlCmd = "SELECT LAST_INSERT_ID() AS id";


   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

  print "SwintonBulls table id: ".$this->dbh->GetFieldValue("id")." \n ";

   return $this->dbh->GetFieldValue("id");

}
   

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 05-06-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
   return $this->strERR;
}

}//end CTracker class

?>

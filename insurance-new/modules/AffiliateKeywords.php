<?php
/*****************************************************************************/
/*                                                                           */
/*  CAffiliateKeywords class interface                                            */
/*                                                                           */
/*  (C) 2004 Eugen Savin (eugen@claspsolutions.com)                          */
/*                                                                           */
/*****************************************************************************/
// include_once "globals.adm.inc";

define("AFFILIATES_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";


if(DEBUG_MODE)
   error_reporting(E_ALL);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CAffiliateKeywords
//
// [DESCRIPTION]:  CAffiliateKeywords class interface
//
// [FUNCTIONS]:    int  AddAffiliateKeywords($cookieID=0, $affID=0, $quoteTypeID=0, $crDate=0);
//                 bool UpdateAffiliateKeywords($affKwID=0, $cookieID=0, $quoteTypeID=0);
//                 bool DeleteAffiliateKeywords($affKwID=0);
//                 array|false GetAffiliateKeywordsByID($affKwID=0);
//                 array|false GetAffiliateKeywordsByName($cookieID=0);
//                 array|false GetAllAffiliateKeywords($startDate="", $endDate="");
//                 int GetAffiliateKeywordsCount(($startDate="", $endDate="", $affiliateId=0, $quoteTypeID=0, $uniqueCookie=0));
//                 bool AssertAffiliateKeywords($cookieID=0, $affID=0, $quoteTypeID=0, $crDate="");
//
//                 void Close();
//                 string GetError();
//
// [CREATED BY]:   Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CAffiliateKeywords
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CAffiliateKeywords
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CAffiliateKeywords($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddAffiliateKeywords
//
// [DESCRIPTION]:   Add new entry to the affiliate_keywords table
//
// [PARAMETERS]:    $cookieID=0, $affId=0 $crDate = 0
//
// [RETURN VALUE]:  quoteID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddAffiliateKeywords($affID=0, $keyword=0, $cookieStatsID=0, $cookieID=0,$search='')
{
   if(! $this->AssertAffiliateKeywords($affID, $keyword))
      return 0;

   $sqlCmd = "INSERT INTO affiliate_keywords (affiliate_id,keyword,cookie_stats_id,cookie_id,date,time,search) VALUES ('$affID','$keyword','$cookieStatsID','$cookieID',now(),now(),'$search')";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $affKwID = $this->dbh->GetFieldValue("id");

   return $affKwID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddAffiliateKeywords
//
// [DESCRIPTION]:   Add new entry to the affiliate_keywords table
//
// [PARAMETERS]:    $cookieID=0, $affId=0 $crDate = 0
//
// [RETURN VALUE]:  quoteID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQzQuoteAffiliateKeywords($affKwID=0, $qzqAffiliateID=0)
{
   $sqlCmd = "INSERT INTO qzquote_affiliate_keywords (affiliate_keyword_id,qzquote_affiliate_id) VALUES ('$affKwID','$qzqAffiliateID')";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $affKwID = $this->dbh->GetFieldValue("id");

   return $affKwID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateAffiliateKeywords
//
// [DESCRIPTION]:   Update affiliate_keywords table
//
// [PARAMETERS]:    $affKwID=0, $cookieID=0, $affID=0, $quoteTypeID=0
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateAffiliateKeywords($affKwID=0, $affID=0, $keyword='', $cookieStatsID=0, $cookieID=0)
{
   if(! preg_match("/^\d+$/", $affKwID))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATE_KEYWORDS_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $affID))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATE_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $keyword))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATE_KEYWORDS_FIELD");
      return false;
   }

   // check if ID exists in DB
   $sqlCmd = "SELECT id FROM affiliate_keywords WHERE id='$affKwID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATE_KEYWORDS_ID_NOT_FOUND");
      return false;
   }

   $sqlCmd = "UPDATE affiliate_keywords SET affiliate_id='$affID', keyword='$keyword', cookie_stats_id='$cookieStatsID', cookie_id='$cookieID' WHERE id='$affKwID'";

   //echo $sqlCmd ;
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError('CANNOT_UPDATE_AFFILIATE_KEYWORDS');
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteAffiliateKeywords
//
// [DESCRIPTION]:   Delete an entry from affiliate_keywords table
//
// [PARAMETERS]:    $affKwID = 0
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteAffiliateKeywords($affKwID = 0)
{
   if(! preg_match("/^\d+$/", $affKwID))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATES_KEYWORDS_ID_FIELD");
      return false;
   }

   // check if ID exists in DB
   $sqlCmd = "SELECT id FROM affiliate_keywords WHERE id='$affKwID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATES_KEYWORDS_ID_NOT_FOUND");
      return false;
   }

   $sqlCmd = "DELETE FROM affiliate_keywords WHERE id='$affKwID'";

   //print "$sqlCmd";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError('CANNOT_DELETE_AFFILIATES_KEYWORDS');
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffiliateKeywordsByID
//
// [DESCRIPTION]:   Read data from affiliate_keywords table and put it into an array variable
//
// [PARAMETERS]:    $affKwID = 0
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAffiliateKeywordsByID($affKwID = 0)
{
   if(! preg_match("/^\d+$/", $affKwID))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATES_KEYWORDS_ID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT * FROM affiliate_keywords WHERE id='$affKwID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATES_KEYWORDS_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]            = $this->dbh->GetFieldValue("id");
   $arrayResult["affiliate_id"]  = $this->dbh->GetFieldValue("affiliate_id");
   $arrayResult["keyword"]       = $this->dbh->GetFieldValue("keyword");
   $arrayResult["cookie_stats_id"] = $this->dbh->GetFieldValue("cookie_stats_id");
   $arrayResult["cookie_id"]       = $this->dbh->GetFieldValue("cookie_id");
   $arrayResult["date"]          = $this->dbh->GetFieldValue("date");
   $arrayResult["time"]          = $this->dbh->GetFieldValue("time");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffiliateKeywordsByKeyword
//
// [DESCRIPTION]:   Read data from affiliate_keywords table and put it into an array variable
//
// [PARAMETERS]:    $cookieID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAffiliateKeywordsByKeyword($keyword = "")
{
   if( empty($keyword))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATES_KEYWORDS_FIELD");
      return false;
   }

   $sqlCmd = "SELECT * FROM affiliate_keywords WHERE keyword='$keyword' ORDER BY affiliate_id,cookie_stats_id,keyword,id";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATE_KEYWORDS_ID_NOT_FOUND");
      return false;
   }
/*
   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
*/
   $arrayResult = array();

   while ($this->dbh->MoveNext())
   {
   	$affKwID = $this->dbh->GetFieldValue('id');
	   //$arrayResult["id"]            = $this->dbh->GetFieldValue("id");
	   $arrayResult[$affKwID]["affiliate_id"]  = $this->dbh->GetFieldValue("affiliate_id");
	   $arrayResult[$affKwID]["keyword"]       = $this->dbh->GetFieldValue("keyword");
	   $arrayResult[$affKwID]["cookie_stats_id"] = $this->dbh->GetFieldValue("cookie_stats_id");
	   $arrayResult[$affKwID]["cookie_id"]       = $this->dbh->GetFieldValue("cookie_id");
	   $arrayResult[$affKwID]["date"]          = $this->dbh->GetFieldValue("date");
	   $arrayResult[$affKwID]["time"]          = $this->dbh->GetFieldValue("time");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffiliateKeywordsByKeyword
//
// [DESCRIPTION]:   Read data from affiliate_keywords table and put it into an array variable
//
// [PARAMETERS]:    $cookieID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAffiliateKeywordsByAffIDAndKeyword($affID=0, $keyword = "")
{
   if( empty($keyword))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATES_KEYWORDS_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $affID))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATES_ID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT * FROM affiliate_keywords WHERE affiliate_id='$affID' AND keyword='$keyword' ORDER BY affiliate_id,cookie_stats_id,keyword,id";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATE_KEYWORDS_ID_NOT_FOUND");
      return false;
   }
/*
   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
*/
   $arrayResult = array();

   while ($this->dbh->MoveNext())
   {
   	$affKwID = $this->dbh->GetFieldValue('id');
	   //$arrayResult["id"]            = $this->dbh->GetFieldValue("id");
	   $arrayResult[$affKwID]["affiliate_id"]  = $this->dbh->GetFieldValue("affiliate_id");
	   $arrayResult[$affKwID]["keyword"]       = $this->dbh->GetFieldValue("keyword");
	   $arrayResult[$affKwID]["cookie_stats_id"] = $this->dbh->GetFieldValue("cookie_stats_id");
	   $arrayResult[$affKwID]["cookie_id"]       = $this->dbh->GetFieldValue("cookie_id");
	   $arrayResult[$affKwID]["date"]          = $this->dbh->GetFieldValue("date");
	   $arrayResult[$affKwID]["time"]          = $this->dbh->GetFieldValue("time");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllAffiliateKeywords
//
// [DESCRIPTION]:   Read data from affiliate_keywords table and put it into an array variable
//
// [PARAMETERS]:    $startDate="", $endDate=""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllAffiliateKeywords($startDate="", $endDate="")
{
   $sqlCmd = "SELECT * FROM affiliate_keywords WHERE 1";

   if($startDate)
      $sqlCmd .= " AND date>='$startDate'";

   $sqlCmd .= " ORDER BY id ";

   if($endDate)
      $sqlCmd .= " AND date<='$endDate'";


   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATE_KEYWORDS_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("id")]['affiliate_id'] = $this->dbh->GetFieldValue("affiliate_id");
      $arrayResult[$this->dbh->GetFieldValue("id")]['keyword']      = $this->dbh->GetFieldValue("keyword");
      $arrayResult[$this->dbh->GetFieldValue("id")]['cookie_stats_id'] = $this->dbh->GetFieldValue("cookie_stats_id");
      $arrayResult[$this->dbh->GetFieldValue("id")]['cookie_id'] = $this->dbh->GetFieldValue("cookie_id");
      $arrayResult[$this->dbh->GetFieldValue("id")]['date']         = $this->dbh->GetFieldValue("date");
      $arrayResult[$this->dbh->GetFieldValue("id")]['time']         = $this->dbh->GetFieldValue("time");
   }

  //print_r ($arrayResult);

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffiliateKeywordsCount
//
// [DESCRIPTION]:   Read data from affiliate_keywords table and put it into an array variable
//
// [PARAMETERS]:    $startDate='', $endDate='', $affiliateId=0, $quoteTypeID=0, $uniqueCookie=0
//
// [RETURN VALUE]:  int
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2005-11-30
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAffiliateKeywordsCount($startDate='', $endDate='', $affID=0, $keyword='', $uniqueKeyword)
{
   $this->lastSQLCMD = "SELECT count(";

   $this->lastSQLCMD .= "*)";

   $this->lastSQLCMD .= " as cnt FROM affiliate_keywords WHERE 1";

   if($startDate)
      $this->lastSQLCMD .= " AND date>='$startDate' ";

   if($endDate)
      $this->lastSQLCMD .= " AND date<='$endDate' ";

   if($affiliateId)
      $this->lastSQLCMD .= " AND affiliate_id='$affID'";

   if($keyword)
      $this->lastSQLCMD .= " AND keyword='$keyword'";

   if ($uniqueKeyword)
   	$this->lastSQLCMD = "SELECT count(distinct cookie_id) as cnt FROM affiliate_keywords ak WHERE 1  and ak.affiliate_id='$affID' AND ak.date>='$startDate' AND ak.date<='$endDate' AND ak.keyword='$keyword'";
 //print $this->lastSQLCMD."<br>";
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return -1;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return -2;
   }

   return $this->dbh->GetFieldValue("cnt");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffiliateKeywordsCount
//
// [DESCRIPTION]:   Read data from affiliate_keywords table and put it into an array variable
//
// [PARAMETERS]:    $startDate='', $endDate='', $affiliateId=0, $quoteTypeID=0, $uniqueCookie=0
//
// [RETURN VALUE]:  int
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2005-11-30
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAffiliateKeywordsList($affID=0)
{
   $this->lastSQLCMD = "SELECT distinct keyword from affiliate_keywords where affiliate_id='$affID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return -1;
   }
/*
   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return -2;
   }
*/

	$arrayResult = array();
	$counter = 0;
	while ($this->dbh->MoveNext())
	{
		$arrayResult[$counter] = $this->dbh->GetFieldValue('keyword');
		$counter++;
	}

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffiliateKeywordsCount
//
// [DESCRIPTION]:   Read data from affiliate_keywords table and put it into an array variable
//
// [PARAMETERS]:    $startDate='', $endDate='', $affiliateId=0, $quoteTypeID=0, $uniqueCookie=0
//
// [RETURN VALUE]:  int
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2005-11-30
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAffiliateKeywordsQuotes($affID=0,$startDate='',$endDate='',$quoteType=1,$keyword='',$unique=false)
{
	$selecting = 'count(distinct ak.cookie_stats_id)';
	if ($unique)
		$selecting = 'count(distinct ak.cookie_id)';

	$quoteTypeSel = "and qzq.quote_type_id='$quoteType'";
	if ($quoteType == 0)
		$quoteTypeSel = "";

   $this->lastSQLCMD = "SELECT $selecting as cnt from qzquote_affiliates qzqa, affiliate_keywords ak, qzquotes qzq,logs l where ak.affiliate_id='$affID' and ak.keyword='$keyword' and qzqa.qz_quote_id=qzq.id and qzq.log_id=l.id and l.date between '$startDate' and '$endDate' $quoteTypeSel and ak.cookie_id=qzqa.cookie_id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return -1;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return -2;
   }


	$cnt = $this->dbh->GetFieldValue('cnt');

   return $cnt;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffiliateKeywordsCount
//
// [DESCRIPTION]:   Read data from affiliate_keywords table and put it into an array variable
//
// [PARAMETERS]:    $startDate='', $endDate='', $affiliateId=0, $quoteTypeID=0, $uniqueCookie=0
//
// [RETURN VALUE]:  int
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2005-11-30
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUsersWithKeywordsList()
{
   $this->lastSQLCMD = "select u.id,u.name,u.email from users u, affiliates a where a.id in (select distinct affiliate_id from affiliate_keywords) and a.user_id=u.id group by name";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return -1;
   }
/*
   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return -2;
   }
*/

	$arrayResult = array();

	while ($this->dbh->MoveNext())
	{
		$arrayResult[$this->dbh->GetFieldValue('id')]['name'] = $this->dbh->GetFieldValue('name');
		$arrayResult[$this->dbh->GetFieldValue('id')]['email'] = $this->dbh->GetFieldValue('email');
	}

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffiliateKeywordsCount
//
// [DESCRIPTION]:   Read data from affiliate_keywords table and put it into an array variable
//
// [PARAMETERS]:    $startDate='', $endDate='', $affiliateId=0, $quoteTypeID=0, $uniqueCookie=0
//
// [RETURN VALUE]:  int
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2005-11-30
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAffiliatesWithKeywordsByUser($userID)
{
   $this->lastSQLCMD = "select ak.affiliate_id,concat_ws('-',a.prefix,a.suffix) as aid from users u,affiliates a,affiliate_keywords ak where u.id='$userID' and u.id=a.user_id and a.id=ak.affiliate_id group by affiliate_id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return -1;
   }
/*
   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return -2;
   }
*/

	$arrayResult = array();

	while ($this->dbh->MoveNext())
	{
		$arrayResult[$this->dbh->GetFieldValue('affiliate_id')] = $this->dbh->GetFieldValue('aid');
		$counter++;
	}

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertAffiliateKeywords
//
// [DESCRIPTION]:   Check if all fields are OK and set the error string if needed
//
// [PARAMETERS]     $cookie, $affID, $quoteTypeID, $crDate
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertAffiliateKeywords($affID, $keyword)
{
   $this->strERR = "";

   if(! preg_match("/^\d+$/", $affID))
      $this->strERR .= GetErrorString("INVALID_AFFILIATEID_FIELD")."\n";

   if(empty($keyword))
      $this->strERR .= GetErrorString("INVALID_KEYWORD_FIELD")."\n";

   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

} // end of CAffiliateKeywords class
?>

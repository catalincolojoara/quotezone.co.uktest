<?php

/**
   * Step Tracker, phpDocumentor 
   * 
   * @author Moraru Valeriu <vali@acrux.biz>
   * @version 1.0
   * @package StepTracker
   */

error_reporting(0);

//include_once("globals.inc");
include_once("errors.inc");
//include_once("sql.inc");
include_once("MySQL.php");

class CStepTracker
{
   /**
      * This class variable contains MySQL object - manage connection to mysql server
      * @var object 
      */

   var $dbh;

   /**
      * This class variable contains error message
      * @var string 
      */

   var $strERR;

   /**
      * This class variable contains true|false, if we connection to db is opened or closed
      * @var boolean 
      */

   var $closeDB;

   /**
      * Constructor of class, connect to db if it is not set already
      */

   function CStepTracker($dbh=0)
   {
      if($dbh)
      {
         $this->dbh     = $dbh;
         $this->closeDB = false;
      }
      else
      {
         // default configuration
         $this->dbh = new CMySQL();

         if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
         {
            $this->strERR = $this->dbh->GetError();
            return;
         }

         $this->closeDB = true;
      }

      $this->strERR  = "";
   }

   /**
      * Insert/Update into table STEP_TRACKER product, system, step, session id, ip, date, time
      *
      * @param string $product contains the product name tracked(qz,cni,wl) 
      * @param string $system contains the system name tracked 
      * @param string $step contains the step name tracked
      * @param string $sessionID contains the session ID tracked
      * @param string $ip contains the ip address tracked
      * @param string $web_server_ip contains the web server ip address tracked
      * @return boolean true or false;
      */

   function InsertUpdateStepTracker($product="",$system="",$step="",$sessionID="",$ip="",$web_server_ip="")
   {
      if(empty($product))
      {
         $this-> strERR = GetErrorString("INVALID_STEP_TRACKER_PRODUCT");
         return false;
      }

      if(empty($system))
      {
         $this-> strERR = GetErrorString("INVALID_STEP_TRACKER_SYSTEM");
         return false;
      }

      if(empty($step))
      {
         $this-> strERR = GetErrorString("INVALID_STEP_TRACKER_STEP");
         return false;
      }

      if(empty($sessionID))
      {
         $this-> strERR = GetErrorString("INVALID_STEP_TRACKER_SESSION_ID");
         return false;
      }

      if(empty($ip))
      {
         $this-> strERR = GetErrorString("INVALID_STEP_TRACKER_IP");
         return false;
      }

      if(empty($web_server_ip))
      {
         $this-> strERR = GetErrorString("INVALID_STEP_TRACKER_WEB_SERVER_IP");
         return false;
      }

      // see if session id is already inserted, if yes then update values for it
      $sqlCmd = "SELECT * FROM ".STEP_TRACKER." WHERE session_id = '".$sessionID."' ";

      // on master
      if(! $this->dbh->Exec($sqlCmd, true))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      // if it is a new sessionID
      if($this->dbh->GetRows() == 0)
      {
         // insert this new sessionID into table with specified values
         $sqlCmd = "INSERT INTO ".STEP_TRACKER." (product,system,step,session_id,ip,web_server_ip,date,time) VALUES ('".$product."','".$system."','".$step."','".$sessionID."','".$ip."','".$web_server_ip."',NOW(),NOW()) ";

         if(! $this->dbh->Exec($sqlCmd))
         {
            $this->strERR = $this->dbh->GetError();
            return false;
         }

         return true;
      }
      else
      {
	      // update fields value for specified sessionID
         $sqlCmd = "UPDATE ".STEP_TRACKER." SET product ='".$product."',system ='".$system."',step ='".$step."',ip ='".$ip."',web_server_ip ='".$web_server_ip."',date = NOW(),time = NOW() WHERE session_id = '".$sessionID."' ";

         if(! $this->dbh->Exec($sqlCmd))
         {
            $this->strERR = $this->dbh->GetError();
            return false;
         }

         return true;
      }

   }

   /**
      * DELETE from table STEP_TRACKER for specified session id 
      *
      * @param string $sessionID contains the session ID 
      * @return bolean true or false
      */

   function DeleteStepTracker($sessionID="")
   {
      if(empty($sessionID))
      {
         $this-> strERR = GetErrorString("INVALID_STEP_TRACKER_SESSION_ID");
         return false;
      }

      $sqlCmd = "DELETE FROM ".STEP_TRACKER." WHERE session_id = '".$sessionID."' ";

      if(! $this->dbh->Exec($sqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      return true;
   }


   /**
      * Get the error string
      *
      * @return string error string
      */

   function GetError()
   {
      return $this->strERR;
   }

}

?>

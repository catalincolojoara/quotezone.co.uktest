<?php
   // download from http://www.abi.org.uk/carinsurance/download.asp
   // import into MySQL database -> only VEHICLE_DETAILS table
   // execute this script to make update of our database
   

   include_once "../modules/MySQL.php";
   include_once "../car/modules/Vehicle.php";
   
   $dbh     = New CMySQL();
   $vehicle = New CVehicle();
   
   if(! $dbh->Open(""))
      print $dbh->GetError();
      
   // get all entry
   if(! $dbh->Exec('select * from VEHICLE_DETAILS order by MAKE_DESCRIPTION asc'))
      print $dbh->GetError();
      
      
   $oldMakeDescription = "";
   while($dbh->MoveNext())
   {
      // ignore DUMP CODE, INVALID FIELDS

      if($dbh->GetFieldValue("MAKE_DESCRIPTION") == 'DUMP CODE')
         continue;
      
      if($dbh->GetFieldValue("MAKE_DESCRIPTION") == 'INVALID')
         continue;
         
      // init mode
      // table VEHICLE_CODES
      $MAKE_CODE    = $dbh->GetFieldValue("MAKE_CODE");
      $MODEL_CODE   = $dbh->GetFieldValue("MODEL_CODE");
      $VARIANT_CODE = $dbh->GetFieldValue("VARIANT_CODE");
      // end table VEHICLE_CODES

      if(! $vehicleCodeId = $vehicle->AddVehicleCode($MAKE_CODE,$MODEL_CODE,$VARIANT_CODE))
         print $vehicle->GetError();
         
      
      // table VEHICLE_MAKE_DESCRIPTION
      $MAKE_DESCRIPTION = $dbh->GetFieldValue("MAKE_DESCRIPTION");
      // end table VEHICLE_MAKE_DESCRIPTION
      
      if(! $vehicleMakeDescriptionId = $vehicle->AddVehicleMakeDescription($MAKE_DESCRIPTION))
         print $vehicle->GetError();
      
      
      if($oldMakeDescription != $MAKE_DESCRIPTION)
      {
         print "\n".$MAKE_DESCRIPTION."\n";
         $oldMakeDescription = $MAKE_DESCRIPTION;
      }
      
      print ".";
      
      
      // table VEHICLE_MODEL_DESCRIPTION
      $MODEL_DESCRIPTION = $dbh->GetFieldValue("MODEL_DESCRIPTION");
      // end table VEHICLE_MODEL_DESCRIPTION
      
      if(! $vehicleModelDescriptionId = $vehicle->AddVehicleModelDescription($MODEL_DESCRIPTION))
         print $vehicle->GetError();
      
      // table VEHICLE
      $MANUFACTURED_FROM = $dbh->GetFieldValue("MANUFACTURED_FROM");
      $MANUFACTURED_TO   = $dbh->GetFieldValue("MANUFACTURED_TO");
      $ENGINE_CC         = $dbh->GetFieldValue("ENGINE_CC");
      $DOORS             = $dbh->GetFieldValue("DOORS");
      $BODY_TYPE         = $dbh->GetFieldValue("BODY_TYPE");
      $ENGINE_TYPE       = $dbh->GetFieldValue("ENGINE_TYPE");
      $TRANSMISSION_TYPE = $dbh->GetFieldValue("TRANSMISSION_TYPE");
      // end table VEHICLE
      
      if(! $vehicle->AddVehicle($vehicleCodeId,$vehicleMakeDescriptionId,$vehicleModelDescriptionId,$ENGINE_CC,$DOORS,$BODY_TYPE,$ENGINE_TYPE,$TRANSMISSION_TYPE,$MANUFACTURED_FROM,$MANUFACTURED_TO))
         print $vehicle->GetError();
      
      
   }

   
?>
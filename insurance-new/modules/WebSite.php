<?php
/*****************************************************************************/
/*                                                                           */
/*  CWebSite class interface                                                 */
/*                                                                           */
/*  (C) 2004 Eugen Savin (seugen@abc.ro)                                     */
/*                                                                           */
/*****************************************************************************/

define("WEB_SITE_INCLUDED", "1");

include_once "Template.php";
include_once "MySQL.php";
include_once "User.php";
include_once "IpBan.php";


if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CWebSite
//
// [DESCRIPTION]:  CWebSite class interface
//
// [FUNCTIONS]:    void Initialise();
//                 void SetTitle($title = "");
//
//                 virtual bool Prepare();
//                 void SetWorkArea($workAreaContent = "");
//                 void Show();
//                 bool Run();
//
//                 dbh  GetDbHandle();
//                 int  GetUserID();
//                 user GetUserObj();
//                 string GetTmplsDir();
//
//                 void Error($errString = "");
//                 string GetError();
//                 void SetOfflineMessage($message);
//                 void Close();
//
// [CREATED BY]:   Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CWebSite
{
    // class-internal variables
    // database handler
    var $dbh;         // database server handle
    var $userObj;     // user object

    var $userID;      // user ID if logged on
    var $userName;    // user Name if logged on
    var $userEmail;   // user Email if logged on
    var $admin;       // admin name

    var $menuType;           // menu type related to user's actions
    var $siteContent;        // web site content
    var $siteTitle;          // the title of the shown web page
    var $templates;          // templates array("file name" => "file contents")
    var $templatesDirectory; // templates directory
    var $loadTemplates;      // keep the templates that will be pre-loaded in this array
    var $metaTags;           // key the meta tags entries array

    /****************************************************************************/
    /*  In the section listed below, sometimes ads refers to js tracking codes. */
    /****************************************************************************/

    var $googleAds;        // google ads flag
    var $uniqueGoogleAds;        // google ads flag
    var $finalPageLoader;  // final page loader flag
    var $msnAds;           // msn ads flag
    var $overtureAds;      // overture ads flag
    var $searchVisionAds;  // searchvision ads flag
    var $facebookAds;  // searchvision ads flag
    var $DGMTracker;       // DGMTracker flag
    var $CPHILIPSTracker;  // DGMTracker flag
    var $amadesaTracker;   // amadesa js tracker
    var $amadesaTag;   // amadesa js tracker
    var $newGoogleTracker;   // amadesa js tracker
    var $piwikTracker;       //piwik js tracker 
    var $piwikUserTracking;  //tracks piwik user details
    var $quoteUserID;      // quote user id used by the google/indextools javascript
    var $svQuoteUserID;    // quote user id used by the searchVision javascript tracker
    var $DGMQuoteRef;      // quote ref used by DGM tracker
    var $CPHILIPSQuoteRef; // quote ref used by DGM tracker
    var $trackerPage;      // used for tracking purposes
    var $serverAddr;       //shows the server ip
    var $vehiclePopunder;  // used to display some overture popunder in the vehicle page
    var $googleTracker;    // google js tracker
    var $blowsearchTracker; // blowsearch js tracker
    var $findologyTracker; // findology js Tracker
    var $yahooDRPixeQuotePageTracker; // yahooDRPixeQuotePage tracker
    var $advivaTracker; // adviva js tracker
    var $startAdvivaTracker; // adviva js tracker
    var $TBNTracker; // TBN js tracker
    var $GunggoTracker; // Gunggo js tracker
    var $googleOptimizerHead; //Google Optimizer Head
    var $googleAgeBracketVar;
    var $googleGenderVar;
    var $googleQuoteRefVar;
    var $googleOptimizerBody; //Google Optimizer Body
    var $MySpaceTracker; // TBN js tracker
    var $showPopUpDiv;     // show the popup div
    var $setOnBeforeUnloadToLastPage; // set the function that will be called when the driver close the windon to the last page

    var $svAmount;// search vision amount
    var $gAmount; // google amount

    var $googleECommerce;
    var $country;
    var $cityortown;
    var $unique_orderid_product;
    var $quantity;

    var $strERR;           // last SITE error string


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CWebSite
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CWebSite($templatesDir="")
{
   $this->strERR = "";

   $this->userID    = 0;
   $this->userName  = "";
   $this->userEmail = "";
   $this->admin     = "";

   $this->menuType = COMMON_MENU;

   $this->siteContent   = "";
   $this->siteTitle     = "";
   $this->templates     = array();
   $this->loadTemplates = array();
   $this->metaTags['robots']      = 'index,follow'; //default

   $this->activeStep                  = ""; // used for home system
   $this->activeStepCar               = ""; // used for car system
   $this->activeStepBike              = ""; // used for bike system
   $this->activeStepHome              = ""; // used for home system
   $this->imageLevel                  = "0";
   $this->imageDir                    = "";
   $this->googleAds                   = false;
   $this->uniqueGoogleAds             = false;
   $this->blowsearchTracker           = false;
   $this->findologyTracker            = false;
   $this->yahooDRPixeQuotePageTracker = false;
   $this->advivaTracker               = false;
   $this->startAdvivaTracker          = false;
   $this->TBNTracker 		      = false;
   $this->GunggoTracker 	      = false;
   $this->googleOptimizerHead         = false;
   $this->googleAgeBracketVar         = "";
   $this->googleGenderVar             = "";
   $this->googleQuoteRefVar           = "";
   $this->googleOptimizerBody         = false;
   $this->MySpaceTracker 		        = false;
   $this->overtureAds                 = false;
   $this->searchVisionAds             = false;
   $this->facebookAds             = false;
   $this->DGMTracker                  = false;
   $this->CPHILIPSTracker             = false;
   $this->amadesaTag                  = false;
   $this->msnAds                      = false;
   $this->amadesaTracker              = '';
   $this->newGoogleTracker            = '';
   $this->piwikTracker                = '';
   $this->piwikUserTracking           = '';
   $this->googleTracker               = '';
   $this->quoteUserID                 = 0;
   $this->svQuoteUserID               = 0;
   $this->DGMQuoteRef                 = '';
   $this->CPHILIPSQuoteRef            = '';
   $this->trackerPage                 = "";
   $this->serverAddr                  = $_SERVER['SERVER_ADDR'];
   $this->vehiclePopunder             = "";

   $this->imagesPreloaded             = "";
   $this->showPopUpDiv                = "";
   $this->setOnBeforeUnloadToLastPage = "";

   $this->svAmount                    = 0;
   $this->gAmount                     = 0;

   // read template files
   $this->templatesDirectory = TEMPLATES_DIR;

   if(! empty($templatesDir))
      $this->templatesDirectory = $templatesDir;

   $this->Initialise();

   $objIpBan = new CIpBan();
   if($objIpBan->IsIpBanned($_SERVER['REMOTE_ADDR']))
      $this->SetBannedIpWebsite();

// uncomment this if you want to put offline all websites car, van , home , bike ...
//  $this->SetOfflineWebsite();

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Initialise
//
// [DESCRIPTION]:   Initialises the web site class.
//                  - initialise php session
//                  - get userID in case someone's logged in
//                  - get menu type
//                  - read all files located  in the templates dir and put them
//                    into an array: key = file name, value = file content
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Initialise()
{
   session_start();

   $this->menuType  = COMMON;
   $this->userID    = 0;
   $this->userName  = "";
   $this->userEmail = "";
   $this->admin     = $_SERVER["REMOTE_USER"];

   $this->dbh = new CMySQL();

   if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
   {
      $this->strERR = $this->dbh->GetError();
      return;
   }

   $this->userObj = new CUser($this->dbh);

   if(! empty($_SESSION["session_key"]))
   {
      $this->userID    = $this->userObj->GetUserID($_SESSION["session_key"]);
      $this->userEmail = $_SESSION["userEmail"];

      if($this->userID)
         $this->menuType = SIMPLE_USER;
   }

   if(! empty($this->admin))
      $this->menuType = ADMIN_USER;

   if(! is_dir($this->templatesDirectory))
   {
      $this->strERR = GetErrorString("DIR_NOT_FOUND").": [".$this->templatesDirectory."]";
      return;
   }

   $this->loadTemplates = explode(" ", PRELOAD_TEMPLATES);
   foreach($this->loadTemplates as $tmplFile)
   {
      if(! ($fh = fopen($this->templatesDirectory."/$tmplFile", "r")))
      {
         $this->strERR = GetErrorString("CANNOT_OPEN_FILE").": [".$this->templatesDirectory."/$tmplFile]";
         return;
      }

      $this->templates[$tmplFile] = fread($fh, filesize($this->templatesDirectory."/$tmplFile"));
      fclose($fh);
   }

// rewriting header in case of custom affiliate header
//if($_SERVER['REMOTE_ADDR'] == '81.196.65.167')
{
	if ($_COOKIE['AFFID'] || $_SESSION['_AFFILIATES_']['id'])
	{
		if ($_SESSION['_AFFILIATES_']['modified'] == 1 || $_SESSION['_AFFILIATES_']['modified'] == 2)
		{
			$affiliateHeaderFile = 'affiliates/modified/Header.tmpl';
		} else {
			include_once "Affiliates.php";
			$objAff = new CAffiliates();

			$affiliateAid = $_COOKIE['AFFID'];
			if (!isset($affiliateAid) AND $affiliateAid == "")
			   $affiliateAid = $_SESSION['_AFFILIATES_']['id'];
			list($affPrefix,$affSuffix) = explode("-",$affiliateAid);

			$affiliateID = $objAff->GetAffiliatesIDBySufixAndPrefix($affPrefix,$affSuffix);

			$affiliateHeaderFile = 'affiliates/'.$affiliateID.'/Header.tmpl';

			$affiliateFooterFile = 'affiliates/'.$affiliateID.'/Footer.tmpl';
		}
		//clearstatcache();
		//if (file_exists(ROOT_PATH."car/templates/".$affiliateHeaderFile))
		if (file_exists(TEMPLATES_DIR."/".$affiliateHeaderFile))
		{
			$this->SetHeaderTmpl($affiliateHeaderFile);

			$_SESSION['HEADER_OVERWRITEN'] = "1";
		}

		if (file_exists(TEMPLATES_DIR."/".$affiliateFooterFile))
		{
			$this->SetFooterTmpl($affiliateFooterFile);

			$_SESSION['FOOTER_OVERWRITEN'] = "1";
		}

	}
}

   // in case that http://car-insurance.quotezone.co.uk/car/?logo=1 = > change the header
   if (preg_match("/car-insurance/i",$_SERVER['HTTP_HOST']))
   {
      //   if($_SERVER['REMOTE_ADDR'] != '81.196.65.167' AND $_SERVER['REMOTE_ADDR'] != '188.220.10.35')
      //      return;

      if($_SESSION["header_1"] == 1)
      {
         $headerFile = "Header_1.tmpl";
         $this->SetHeaderTmpl($headerFile);
      }
   }

   return;

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetTitle
//
// [DESCRIPTION]:   Set the web site title
//
// [PARAMETERS]:    $title = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetTitle($title = "")
{
   $this->siteTitle = $title;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetMetaTag
//
// [DESCRIPTION]:   Set the meta tags of pages
//
// [PARAMETERS]:    $name="", $content=""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel Istvancsek (gabi@acrux.biz) 2010-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetMetaTag($name="", $content)
{
   if(empty($name))
      return;
      
   if(empty($content))
      return;

   $this->metaTags[$name] = $content;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetHeaderTmpl
//
// [DESCRIPTION]:   Set the header template
//
// [PARAMETERS]:    $tmplName = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetHeaderTmpl($tmplName = "Header.tmpl")
{
   // disabling for bike
   //if (preg_match("/bike-insurance/i",$_SERVER['HTTP_HOST']))
   //{
   //   if($_SERVER['REMOTE_ADDR'] != '81.196.65.167' AND $_SERVER['REMOTE_ADDR'] != '188.220.10.35' AND $_SERVER['REMOTE_ADDR'] != '212.117.158.222' AND $_SERVER['REMOTE_ADDR'] != '24.14.36.8')
   //      return;
   //}

   if(! ($fh = fopen($this->templatesDirectory."/$tmplName", "r")))
   {
      $this->strERR = GetErrorString("CANNOT_OPEN_FILE").": [".$this->templatesDirectory."/$tmplName]";
      return;
   }

   $this->templates['Header.tmpl'] = fread($fh, filesize($this->templatesDirectory."/$tmplName"));
   fclose($fh);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetHeaderTmpl
//
// [DESCRIPTION]:   Set the header template
//
// [PARAMETERS]:    $tmplName = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetFooterTmpl($tmplName = "Footer.tmpl")
{

      if(! ($fh = fopen($this->templatesDirectory."/$tmplName", "r")))
      {
         $this->strERR = GetErrorString("CANNOT_OPEN_FILE").": [".$this->templatesDirectory."/$tmplName]";
         return;
      }

      $this->templates['Footer.tmpl'] = fread($fh, filesize($this->templatesDirectory."/$tmplName"));
      fclose($fh);



}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Prepare
//
// [DESCRIPTION]:   Prepare the web site. Replace template vars
//                  with the values based on a specific algorithm we define below
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Prepare()
{

   if(! empty($this->strERR))
      return false;

   $tmpl = new CTemplate();

   $tmpl->SetTemplateDirectory($this->templatesDirectory);

   if(! $tmpl->Load(WEB_SITE_FIRST_TMPL))
   {
      $this->strERR = $tmpl->GetError();
      return false;
   }

   $tmplVars = array();

   $tmplVars["site.title"]  = $this->siteTitle;
   $tmplVars["site.header"] = $this->templates['Header.tmpl'];
   $tmplVars["site.footer"] = $this->templates['Footer.tmpl'];
   
   foreach($this->metaTags as $name => $content)
   {
      $tmplVars["site.meta"]  .= "<meta name=\"".$name."\" content=\"".$content."\"> \n";
   }

   if($this->menuType == COMMON)
      $tmplVars["site.menu"] = $this->templates['CommonMenu.tmpl'];
   else if($this->menuType == SIMPLE_USER)
      $tmplVars["site.menu"] = $this->templates['UserMenu.tmpl'];
   else if($this->menuType == ADVANCED_USER)
      $tmplVars["site.menu"] = $this->templates['AdvancedMenu.tmpl'];
   else if($this->menuType == ADMIN_USER)
      $tmplVars["site.menu"] = $this->templates['AdminMenu.tmpl'];

   $tmplVars["site.work_area"] = $this->workAreaContent;

   $tmplVars["header.menu"] = $this->templates['HeaderMenu.tmpl'];
   
   // For CAR system SET HEADER STEPS
   if( SYSTEM_TYPE == "CAR")
   {
      // 1
      //newStepsFirstStepOn
      //newStepsFirstStepOff

      // 2
      //newStepsSecondStepOn
      //newStepsSecondStepOff

      // 3
      //newStepsThirdStepOn
      //newStepsThirdStepOff

      switch($this->activeStepCar)
      {
         case "YOUR_VEHICLE" :
               $tmplVars["YOUR_VEHICLE"]        = "newStepsFirstStepOn";
               $tmplVars["ABOUT_YOU"]           = "newStepsSecondStepOff";
               $tmplVars["YOUR_COVER"]          = "newStepsThirdStepOff";
               $tmplVars["STEPS_DIV_DISPLAY"]   = "block";

               break;

         case "ABOUT_YOU" :
               $tmplVars["YOUR_VEHICLE"]        = "newStepsFirstStepOff";
               $tmplVars["ABOUT_YOU"]           = "newStepsSecondStepOn";
               $tmplVars["YOUR_COVER"]          = "newStepsThirdStepOff";
               $tmplVars["STEPS_DIV_DISPLAY"]   = "block";
               break;

         case "YOUR_COVER" :
               $tmplVars["YOUR_VEHICLE"]        = "newStepsFirstStepOff";
               $tmplVars["ABOUT_YOU"]           = "newStepsSecondStepOff";
               $tmplVars["YOUR_COVER"]          = "newStepsThirdStepOn";
               $tmplVars["STEPS_DIV_DISPLAY"]   = "block";
               break;

         default:
               $tmplVars["YOUR_VEHICLE"]        = "newStepsFirstStepOff";
               $tmplVars["ABOUT_YOU"]           = "newStepsSecondStepOff";
               $tmplVars["YOUR_COVER"]          = "newStepsThirdStepOff";
               $tmplVars["STEPS_DIV_DISPLAY"]   = "none";
               break;
      }

      // include last page css
      if ($_SESSION["_ACTIVE_STEP_"] == "7:0:0")
      {
  	 if($_SESSION["version"]=="new")
            $tmplVars["LAST_PAGE_CSS"] = '<link href="css-new/qzLastPage.css?2" rel="stylesheet" type="text/css">';
         else
            $tmplVars["LAST_PAGE_CSS"] = '<link href="css/qzLastPage.css?2" rel="stylesheet" type="text/css">';

      } 
   }

   // For HOME system SET HEADER STEPS
   if( SYSTEM_TYPE == "HOME")
   {
      // 1
      //newStepsFirstStepOn
      //newStepsFirstStepOff

      // 2
      //newStepsSecondStepOn
      //newStepsSecondStepOff

      // 3
      //newStepsThirdStepOn
      //newStepsThirdStepOff

      switch($this->activeStepHome)
      {
         case "YOUR_HOME" :
               $tmplVars["YOUR_HOME"]           = "newStepsFirstStepOn";
               $tmplVars["ABOUT_YOU"]           = "newStepsSecondStepOff";
               $tmplVars["YOUR_COVER"]          = "newStepsThirdStepOff";
               $tmplVars["STEPS_DIV_DISPLAY"]   = "block";

               break;

         case "ABOUT_YOU" :
               $tmplVars["YOUR_HOME"]           = "newStepsFirstStepOff";
               $tmplVars["ABOUT_YOU"]           = "newStepsSecondStepOn";
               $tmplVars["YOUR_COVER"]          = "newStepsThirdStepOff";
               $tmplVars["STEPS_DIV_DISPLAY"]   = "block";
               break;

         case "YOUR_COVER" :
               $tmplVars["YOUR_HOME"]           = "newStepsFirstStepOff";
               $tmplVars["ABOUT_YOU"]           = "newStepsSecondStepOff";
               $tmplVars["YOUR_COVER"]          = "newStepsThirdStepOn";
               $tmplVars["STEPS_DIV_DISPLAY"]   = "block";
               break;

         default:
               $tmplVars["YOUR_HOME"]           = "newStepsFirstStepOff";
               $tmplVars["ABOUT_YOU"]           = "newStepsSecondStepOff";
               $tmplVars["YOUR_COVER"]          = "newStepsThirdStepOff";
               $tmplVars["STEPS_DIV_DISPLAY"]   = "none";
               break;
      }
   
       // include last page css
       if($_SESSION["_ACTIVE_STEP_"] == "11:0:0")
          $tmplVars["LAST_PAGE_CSS"] = '<link href="[% DIR %]css/qzLastPage.css?1" rel="stylesheet" type="text/css">';
   }

   // For BIKE system SET HEADER STEPS
   if( SYSTEM_TYPE == "BIKE")
   {
      // 1
      //newStepsFirstStepOn
      //newStepsFirstStepOff

      // 2
      //newStepsSecondStepOn
      //newStepsSecondStepOff

      // 3
      //newStepsThirdStepOn
      //newStepsThirdStepOff

      switch($this->activeStepBike)
      {
         case "YOUR_VEHICLE" :
               $tmplVars["YOUR_VEHICLE"]        = "newStepsFirstStepOn";
               $tmplVars["ABOUT_YOU"]           = "newStepsSecondStepOff";
               $tmplVars["YOUR_COVER"]          = "newStepsThirdStepOff";
               $tmplVars["STEPS_DIV_DISPLAY"]   = "block";

               break;

         case "ABOUT_YOU" :
               $tmplVars["YOUR_VEHICLE"]        = "newStepsFirstStepOff";
               $tmplVars["ABOUT_YOU"]           = "newStepsSecondStepOn";
               $tmplVars["YOUR_COVER"]          = "newStepsThirdStepOff";
               $tmplVars["STEPS_DIV_DISPLAY"]   = "block";
               break;

         case "YOUR_COVER" :
               $tmplVars["YOUR_VEHICLE"]        = "newStepsFirstStepOff";
               $tmplVars["ABOUT_YOU"]           = "newStepsSecondStepOff";
               $tmplVars["YOUR_COVER"]          = "newStepsThirdStepOn";
               $tmplVars["STEPS_DIV_DISPLAY"]   = "block";
               break;

         default:
               $tmplVars["YOUR_VEHICLE"]        = "newStepsFirstStepOff";
               $tmplVars["ABOUT_YOU"]           = "newStepsSecondStepOff";
               $tmplVars["YOUR_COVER"]          = "newStepsThirdStepOff";
               $tmplVars["STEPS_DIV_DISPLAY"]   = "none";
               break;
      }

      // include last page css
      if(trim($_SESSION["_MENU_"][$_SESSION["ACTIVE STEP"]]) == "_QUOTE_RESULTS_")
         $tmplVars["LAST_PAGE_CSS"] = '<link href="[% DIR %]css/qzLastPage.css?1" rel="stylesheet" type="text/css">';

   }

   $tmplVars["DIR"] = $this->imageDir;

   $tmplVars["LEVEL"] = $this->imageLevel;

   $tmplVars["IMAGES"] = $this->imagesPreloaded;

   $tmplVars["SETONBEFOREUNLOADTOLASTPAGE"] = $this->setOnBeforeUnloadToLastPage;
   $tmplVars["showpopupdiv"]                = $this->showPopUpDiv;

// setting css dir change if header is overwritten
   if ($_SESSION['HEADER_OVERWRITEN'] && $_COOKIE['AFFID'])
   {
	$tmplVars["AFFILIATES"] = "affiliates/";
   }


   if($this->googleAds)
   {
      $tmplGoogleAds = new CTemplate();

      if(! $tmplGoogleAds->Load("GoogleAds.tmpl"))
      {
         $this->strERR = $tmplGoogleAds->GetError();
         return false;
      }

      $googlePrepArray["amount"]  = $this->gAmount;
      $googlePrepArray["orderid"] = $this->quoteUserID;
      $tmplGoogleAds->Prepare($googlePrepArray);

      $tmplVars["GOOGLE_ADS"] = $tmplGoogleAds->GetContent();
   }

   if($this->googleECommerce)
   {
      $tmplGoogleECommerce = new CTemplate();

      if(! $tmplGoogleECommerce->Load("GoogleECommerce.tmpl"))
      {
         $this->strERR = $tmplGoogleECommerce->GetError();
         return false;
      }

      $googlePrepArray["orderid"]                = $this->quoteUserID;
      $googlePrepArray["cityortown"]             = $this->cityortown;
      $googlePrepArray["country"]                = $this->country;
      $googlePrepArray["unique_orderid_product"] = $this->unique_orderid_product;
      $googlePrepArray["quantity"]               = $this->quantity;

      if($this->quantity == 1)
         $googlePrepArray["amount"]               = $this->gAmount;
      else
         $googlePrepArray["amount"]               = 0;

      $tmplGoogleECommerce->Prepare($googlePrepArray);
      $tmplVars["GOOGLE_ECOMMERCE"] = $tmplGoogleECommerce->GetContent();
   }


   if($this->uniqueGoogleAds)
   {
      $tmplGoogleAdsUnique = new CTemplate();

      if(! $tmplGoogleAdsUnique->Load("GoogleAdsUnique.tmpl"))
      {
         $this->strERR = $tmplGoogleAdsUnique->GetError();
         return false;
      }

      $googleUniquePrepArray["amount"]  = $this->gAmount;
      $googleUniquePrepArray["orderid"] = $this->quoteUserID;
      $tmplGoogleAdsUnique->Prepare($googleUniquePrepArray);

      $tmplVars["UNIQUE_GOOGLE_ADS"] = $tmplGoogleAdsUnique->GetContent();

   }


   if($this->finalPageLoader)
   {
       $tmplFinalPageLoader = new CTemplate();

      if(! $tmplFinalPageLoader->Load("FinalPageLoader.tmpl"))
      {
         $this->strERR = $tmplFinalPageLoader->GetError();
         return false;
      }

      $tmplVars["FINAL_PAGE_LOADER"] =  $tmplFinalPageLoader->GetContent();;
   }



   if($this->overtureAds)
   {
      $tmplOvertureAds = new CTemplate();

      if(! $tmplOvertureAds->Load("OvertureAds.tmpl"))
      {
         $this->strERR = $tmplOvertureAds->GetError();
         return false;
      }

      $tmplVars["OVERTURE_ADS"] = $tmplOvertureAds->GetContent();
   }

   if($this->yahooDRPixeQuotePageTracker)
   {
      $tmplYahooDR = new CTemplate();
      if(! $tmplYahooDR->Load("YahooDRPixelQuote.tmpl"))
      {
         $this->strERR = $tmplYahooDR->GetError();
         return false;
      }

      $tmplVars["YahooDR_QUOTE_TRACKER"] = $tmplYahooDR->GetContent();
   }


   if($this->blowsearchTracker)
   {
      $tmplBlowsearch = new CTemplate();

      if(! $tmplBlowsearch->Load("BlowsearchTracker.tmpl"))
      {
         $this->strERR = $tmplBlowsearch->GetError();
         return false;
      }

      $tmplVars["BLOWSEARCH_TRACKER"] = $tmplBlowsearch->GetContent();
   }

   if($this->findologyTracker)
   {
      $tmplFindology = new CTemplate();

      if(! $tmplFindology->Load("FindologyTracker.tmpl"))
      {
         $this->strERR = $tmplFindology->GetError();
         return false;
      }

      $tmplVars["FINDOLOGY_TRACKER"] = $tmplFindology->GetContent();
   }

   if($this->advivaTracker)
   {
      $tmplAdviva = new CTemplate();

      if(! $tmplAdviva->Load("AdvivaTracker.tmpl"))
      {
         $this->strERR = $tmplAdviva->GetError();
         return false;
      }

      $tmplVars["ADVIVA_TRACKER"] = $tmplAdviva->GetContent();
   }

   if($this->startAdvivaTracker)
   {
      $tmplAdviva = new CTemplate();

      if(! $tmplAdviva->Load("StartAdvivaTracker.tmpl"))
      {
         $this->strERR = $tmplAdviva->GetError();
         return false;
      }

      $tmplVars["ADVIVA_TRACKER"] = $tmplAdviva->GetContent();
   }

   if($this->TBNTracker)
   {
      $tmplTBN = new CTemplate();

      if(! $tmplTBN->Load("TBNTracker.tmpl"))
      {
         $this->strERR = $tmplTBN->GetError();
         return false;
      }

      $tmplVars["TBN_TRACKER"] = $tmplTBN->GetContent();
   }

   if($this->GunggoTracker)
   {
      $tmplGunggo = new CTemplate();

      if(! $tmplGunggo->Load("GunggoTracker.tmpl"))
      {
         $this->strERR = $tmplGunggo->GetError();
         return false;
      }

      $tmplVars["GUNGGO_TRACKER"] = $tmplGunggo->GetContent();
   }
   
   if($this->googleOptimizerHead)
   {
      $tmplGoogleOptHead = new CTemplate();

      if(! $tmplGoogleOptHead->Load("GoogleOptimizerHead.tmpl"))
      {
         $this->strERR = $tmplGoogleOptHead->GetError();
         return false;
      }
      
      $googleOptHeadPrepArray["AGE_VAR"]       = $this->googleAgeBracketVar;
      $googleOptHeadPrepArray["GENDER_VAR"]    = $this->googleGenderVar;
      $googleOptHeadPrepArray["QUOTE_REF_VAR"] = $this->googleQuoteRefVar;
      $tmplGoogleOptHead->Prepare($googleOptHeadPrepArray);

      $tmplVars["GOOGLE_OPTIMIZER_HEAD"] = $tmplGoogleOptHead->GetContent();
   }
   
   if($this->googleOptimizerBody)
   {
      $tmplGoogleOptBody = new CTemplate();

      if(! $tmplGoogleOptBody->Load("GoogleOptimizerBody.tmpl"))
      {
         $this->strERR = $tmplGoogleOptBody->GetError();
         return false;
      }

      $tmplVars["GOOGLE_OPTIMIZER_BODY"] = $tmplGoogleOptBody->GetContent();
   }


   if($this->MySpaceTracker)
   {
      $tmplMySpace = new CTemplate();

      if(! $tmplMySpace->Load("MySpaceTracker.tmpl"))
      {
         $this->strERR = $tmplMySpace->GetError();
         return false;
      }

      $tmplVars["MYSPACE_TRACKER"] = $tmplMySpace->GetContent();
   }

   if($this->amadesaTag)
   {
      $tmplAmadesaTag = new CTemplate();

      if(! $tmplAmadesaTag->Load("AmadesaTag.tmpl"))
      {
         $this->strERR = $tmplAmadesaTag->GetError();
         return false;
      }

      $tmplVars["AMADESA_TAG"] = $tmplAmadesaTag->GetContent();
   }


   if($this->searchVisionAds)
   {
      $tmplSearchVisionAds = new CTemplate();

      if(! $tmplSearchVisionAds->Load("SearchVisionAds.tmpl"))
      {
         $this->strERR = $tmplSearchVisionAds->GetError();
         return false;
      }

      $searchVisionPrepArray["amount"]  = $this->svAmount;
      $searchVisionPrepArray["orderid"] = $this->svQuoteUserID;
      $tmplSearchVisionAds->Prepare($searchVisionPrepArray);

      $tmplVars["SEARCHVISION_ADS"] = $tmplSearchVisionAds->GetContent();
   }

   if($this->facebookAds)
   {
      $tmplFacebookAds = new CTemplate();

      if(! $tmplFacebookAds->Load("FacebookTracker.tmpl"))
      {
         $this->strERR = $tmplFacebookAds->GetError();
         return false;
      }

      $facebookPrepArray["orderid"] = $this->svQuoteUserID;
      $tmplFacebookAds->Prepare($facebookPrepArray);

      $tmplVars["FACEBOOK_ADS"] = $tmplFacebookAds->GetContent();
   }

   if($this->DGMTracker)
   {
      $tmplDGMTracker = new CTemplate();

      if(! $tmplDGMTracker->Load("DGMTracker.tmpl"))
      {
         $this->strERR = $tmplDGMTracker->GetError();
         return false;
      }

      $DGMTrackerPrepArray["ORDERID"] = $this->DGMQuoteRef;
      $tmplDGMTracker->Prepare($DGMTrackerPrepArray);

      $tmplVars["DGM_TRACKER"] = $tmplDGMTracker->GetContent();
   }

   if($this->CPHILIPSTracker)
   {
      $tmplCPHILIPSTracker = new CTemplate();

      if(! $tmplCPHILIPSTracker->Load("CPHILIPSTracker.tmpl"))
      {
         $this->strERR = $tmplCPHILIPSTracker->GetError();
         return false;
      }

      $CPHILIPSTrackerPrepArray["ORDERID"] = $this->CPHILIPSQuoteRef;
      $tmplCPHILIPSTracker->Prepare($CPHILIPSTrackerPrepArray);

      $tmplVars["CPHILIPS_TRACKER"] = $tmplCPHILIPSTracker->GetContent();
   }

   if($this->amadesaTracker)
      $tmplVars["amadesa_tracker"] = $this->amadesaTracker;

   if($this->newGoogleTracker)
      $tmplVars["new_google_analytics_tracker"] = $this->newGoogleTracker;

   if($this->googleTracker)
      $tmplVars["google_tracker"] = $this->googleTracker;

   if($this->trackerPage)
      $tmplVars["tracker_page"] = $this->trackerPage;

   if($this->serverAddr)
      $tmplVars["server_addr"] = $this->serverAddr;

   if($this->msnAds)
   {
      $tmplMsnAds = new CTemplate();
      if(! $tmplMsnAds->Load("MsnAds.tmpl"))
      {
         $this->strERR = $tmplMsnAds->GetError();
         return false;
      }

      $tmplVars["MSN_ADS"] = $tmplMsnAds->GetContent();
   }

   if($this->vehiclePopunder)
   {
      $tmplVehiclePopunder = new CTemplate();

      if(! $tmplVehiclePopunder->Load("VehiclePopunder.tmpl"))
      {
         $this->strERR = $tmplVehiclePopunder->GetError();
         return false;
      }

      $tmplVars["VEHICLE_POPUNDER"] = $tmplVehiclePopunder->GetContent();
   }

   if($this->piwikTracker)
   {
      $tmplPiwikTracker = new CTemplate();

      if(! $tmplPiwikTracker->Load("PiwikTracker.tmpl"))
      {
         $this->strERR = $tmplPiwikTracker->GetError();
         return false;
      }

      //check if it is Affiliate   
      if($_COOKIE['AFFID'])
         $this->piwikTracker = $this->piwikTracker."_affiliate";

      $piwikPrepArray["virtual_document_title"] = $this->piwikTracker;
      $piwikPrepArray["user_tracking"]          = $this->piwikUserTracking;
      $tmplPiwikTracker->Prepare($piwikPrepArray);

      $tmplVars["PIWIK_TRACKER"] = $tmplPiwikTracker->GetContent();
   }
   
   // set current year
   $tmplVars["current_year.value"] = date("Y");

   // testing bike navigation menu
   $tmplVars["MENU"] = $this->menu;
 
   // set current year
   $tmplVars["current_year.value"] = date("Y");

   $tmpl->Prepare($tmplVars);

   $this->siteContent = $tmpl->GetContent();

   // clean up some session vars
   unset($_SESSION["loginErrorMessage"]);
   

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetMenu
//
// [DESCRIPTION]:   Set the menu
//
// [PARAMETERS]:    $menuContent = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-03-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetMenu($menuContent = "")
{
   $this->menu = $menuContent;

   return;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetWorkArea
//
// [DESCRIPTION]:   Set the work area content
//
// [PARAMETERS]:    $workAreaContent = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetWorkArea($workAreaContent = "")
{
   $this->workAreaContent = $workAreaContent;

   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Show
//
// [DESCRIPTION]:   Show template content
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Show()
{
   print $this->siteContent;
   flush();
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetImageHeaderLevel
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetImageHeaderLevel($imageLevel)
{
   $this->imageLevel = $imageLevel;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetHeaderActiveStep
//
// [DESCRIPTION]:   set the active step from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Calin Goia (calin@acrux.biz) 2009-02-25
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetHeaderActiveStep($activeStep)
{
   $this->activeStep = $activeStep;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetHeaderActiveStepCar
//
// [DESCRIPTION]:   set the active step from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2009-10-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetHeaderActiveStepCar($activeStepCar)
{
   $this->activeStepCar = $activeStepCar;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetHeaderActiveStepBike
//
// [DESCRIPTION]:   set the active step from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2009-10-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetHeaderActiveStepBike($activeStepBike)
{
   $this->activeStepBike = $activeStepBike;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetHeaderActiveStepHome
//
// [DESCRIPTION]:   set the active step from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2009-10-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetHeaderActiveStepHome($activeStepHome)
{
   $this->activeStepHome = $activeStepHome;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetOnBeforeUnloadToLastPage($textToSet="")
//
// [DESCRIPTION]:   set the text that will be shown when a driver leaves quotezone
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function  SetOnBeforeUnloadToLastPage($textToSet="")
{
  $this->setOnBeforeUnloadToLastPage = $textToSet;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowPopupDivContent($textToSet="")
//
// [DESCRIPTION]:   show the div on the page
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function  ShowPopupDivContent($textToSet="")
{
  $this->showPopUpDiv = $textToSet;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetImageHeaderLevel
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetImagesPreloaded($images)
{
   foreach($images as $name)
   {
      $this->imagesPreloaded .= "'".$name."',";
   }

   $this->imagesPreloaded = substr($this->imagesPreloaded, 0, strlen($this->imagesPreloaded) - 1);

}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetImageHeaderLevel
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetImageDir($imageDir="")
{
   $this->imageDir = $imageDir;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetGoogleAds
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    $quoteUserID=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetGoogleAds($quoteUserID=0,$gAmount=0)
{ 
   if(! preg_match("/[0-9\.]+/i", $gAmount))
      $gAmount = 0;

   $this->googleAds = true;
   $this->quoteUserID = $quoteUserID;
   $this->gAmount     = $gAmount;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetGoogleAds
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    $quoteUserID=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetGoogleECommerce($quoteUserID=0, $gAmount=0, $country="", $cityortown="", $product_orderid="", $quantity="")
{ 
   if(! preg_match("/[0-9\.]+/i", $gAmount))
      $gAmount = 0;

   $this->googleECommerce        = true;
   $this->quoteUserID            = $quoteUserID;
   $this->gAmount     	         = $gAmount;
   $this->country     	         = $country;
   $this->cityortown             = $cityortown;
   $this->unique_orderid_product = $product_orderid;
   $this->quantity               = $quantity;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetUniqueGoogleAds
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    $quoteUserID=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetUniqueGoogleAds($quoteUserID=0,$gAmount=0)
{ 
   if(! preg_match("/[0-9\.]+/i", $gAmount))
      $gAmount = 0;

   $this->uniqueGoogleAds = true;
   $this->quoteUserID = $quoteUserID;
   $this->gAmount     = $gAmount;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetGoogleAds
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    $quoteUserID=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckAffiliateUserId($aid='')
{
	if (empty($aid))
		return;

	if (!$_COOKIE['AFFID'])
		return;

	include_once "Affiliates.php";
	$affObject = new CAffiliates();

	$affUserId = $affObject->GetAffiliatesUIDByAID($aid);

	return $affUserId;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetBlowsearchTracker
//
// [DESCRIPTION]:   set the blowsearch tracker
//
// [PARAMETERS]:    $quoteUserID=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 2008-06-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetBlowsearchTracker()
{
//	if (!$_COOKIE['AFFID'])
//		return;
//
//	$blowSearchUserIdArray = array(
//		"89",
//		"90",
//		"91",
//		"92",
//		"93",
//		"95",
//		"96",
//		"97",
//		"98",
//		"99",
//		"89",
//		"126",
//		"127",
//		"134",
//	);
//
//	if (!in_array($this->CheckAffiliateUserId($_COOKIE['AFFID']),$blowSearchUserIdArray))
//		return;

   $this->blowsearchTracker = true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetFindologyTracker
//
// [DESCRIPTION]:   set the Findology tracker
//
// [PARAMETERS]:    $quoteUserID=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 2008-06-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetFindologyTracker()
{
   $this->findologyTracker = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetYahooDRPixeQuotePageTracker
//
// [DESCRIPTION]:   set the SetYahooDRPixeQuotePageTracker unique quote tracker
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 2008-11-27
//
// [MODIFIED]:      - [programmer (email) date] //zzzz
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetYahooDRPixeQuotePageTracker()
{
	if (!$_COOKIE['AFFID'])
		return;

	if ($this->CheckAffiliateUserId($_COOKIE['AFFID']) != '155')
		return;

   $this->yahooDRPixeQuotePageTracker = true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetAdvivaTracker
//
// [DESCRIPTION]:   set the adviva tracker
//
// [PARAMETERS]:    $quoteUserID=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 2008-06-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetAdvivaTracker()
{
	if (!$_COOKIE['AFFID'])
		return;

	if ($this->CheckAffiliateUserId($_COOKIE['AFFID']) != '148')
		return;

   $this->advivaTracker = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetStartAdvivaTracker
//
// [DESCRIPTION]:   set the adviva tracker
//
// [PARAMETERS]:    $quoteUserID=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 2008-06-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetStartAdvivaTracker()
{
   $this->startAdvivaTracker = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetTBNTracker
//
// [DESCRIPTION]:   set the TBN tracker
//
// [PARAMETERS]:    $quoteUserID=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Calin Goia (calin@acrux.biz) 2008-10-27
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetTBNTracker()
{
	if (!$_COOKIE['AFFID'])
		return;

	if ($this->CheckAffiliateUserId($_COOKIE['AFFID']) != '156')
		return;

   $this->TBNTracker = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetGunggoTracker
//
// [DESCRIPTION]:   set the Gunggo tracker
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Ciprian Sturza (cipi@acrux.biz) 2012-10-26
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetGunggoTracker()
{
   if (!$_COOKIE['AFFID'])
      return;   

   if ($this->CheckAffiliateUserId($_COOKIE['AFFID']) != '3031')
      return;

   $this->GunggoTracker = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetGoogleOptimizerHead
//
// [DESCRIPTION]:   set the Google Optimizer
//
// [PARAMETERS]:    
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Calin Goia (calin@acrux.biz) 2010-03-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetGoogleOptimizerHead($googleAgeBracketVar, $googleGenderVar, $googleQuoteRefVar)
{
   $this->googleOptimizerHead = true;
   $this->googleAgeBracketVar = $googleAgeBracketVar;
   $this->googleGenderVar     = $googleGenderVar;
   $this->googleQuoteRefVar   = $googleQuoteRefVar;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetGoogleOptimizerBody
//
// [DESCRIPTION]:   set the Google Optimizer
//
// [PARAMETERS]:    
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Calin Goia (calin@acrux.biz) 2010-03-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetGoogleOptimizerBody()
{
   $this->googleOptimizerBody = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetMySpaceTracker
//
// [DESCRIPTION]:   set the MySpace tracker
//
// [PARAMETERS]:    $quoteUserID=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Calin Goia (calin@acrux.biz) 2008-10-27
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetMySpaceTracker()
{
	if (!$_COOKIE['AFFID'])
		return;

	if ($this->CheckAffiliateUserId($_COOKIE['AFFID']) != '226')
		return;

   $this->MySpaceTracker = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetMySpaceTracker
//
// [DESCRIPTION]:   set the MySpace tracker
//
// [PARAMETERS]:    $quoteUserID=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Calin Goia (calin@acrux.biz) 2008-10-27
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetAmadesaTag()
{
   //if (! $_COOKIE['AFFID'])
   //   return;

   //if ($this->CheckAffiliateUserId($_COOKIE['AFFID']) != '226')
   //   return;

   $this->amadesaTag = true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetFinalPageLoader
//
// [DESCRIPTION]:   set the final page special loading animation
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Racu Daniel (dade@4x.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetFinalPageLoader()
{
   $this->finalPageLoader = true;
}



//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetOvertureAds
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetOvertureAds()
{
   $this->overtureAds = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetSearchVisionAds
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetSearchVisionAds($quoteUserID, $svAmount=0)
{
   if(! preg_match("/[0-9\.]+/i", $svAmount))
      $svAmount = 0;

   $this->searchVisionAds = true;
   $this->svAmount        = $svAmount;
   $this->svQuoteUserID   = $quoteUserID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetFacebookAds
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetFacebookAdds($quoteUserID)
{
   $this->facebookAds = true;
   $this->svQuoteUserID   = $quoteUserID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetDGMTracker
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    $quoteRef
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetDGMTracker($quoteRef)
{
	if (!$_COOKIE['AFFID'])
		return;

	if ($this->CheckAffiliateUserId($_COOKIE['AFFID']) != '166')
		return;

   $this->DGMTracker  = true;
   $this->DGMQuoteRef = $quoteRef;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetCPHILIPSTracker
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    $quoteRef
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetCPHILIPSTracker($quoteRef)
{
	if (!$_COOKIE['AFFID'])
		return;

	if ($this->CheckAffiliateUserId($_COOKIE['AFFID']) != '193')
		return;

   $this->CPHILIPSTracker  = true;
   $this->CPHILIPSQuoteRef = $quoteRef;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetAmadesaTracker
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetAmadesaTracker($amadesaCode='')
{
   // disable amadesa for affiliates for car
   //if ( $_COOKIE['AFFID'] AND SYSTEM_TYPE == "CAR" AND $_SERVER['REMOTE_ADDR'] != '81.196.65.167' )
   //   return false;

   $this->amadesaTracker = $amadesaCode;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetAmadesaTracker
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetNewGoogleTracker($newGoogleCode='')
{
   $this->newGoogleTracker = $newGoogleCode;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetPiwikTracker
//
// [DESCRIPTION]:   sets the piwik title in tracking code
//
// [PARAMETERS]:    $documentTitle
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetPiwikTracker($documentTitle='',$userTracking='')
{
   $this->piwikTracker      = $documentTitle;
   $this->piwikUserTracking = $userTracking;
   
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetGoogleTracker
//
// [DESCRIPTION]:   set the js content of google tracker
//
// [PARAMETERS]:    js content
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2007-11-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetGoogleTracker($googleCode='')
{
   if(defined('TESTING_MODE'))
      if(TESTING_MODE)
         return;

   $this->googleTracker = $googleCode;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetTrackerPage
//
// [DESCRIPTION]:   set the page name for the tracker system
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetTrackerPage($pageName='')
{
   $this->trackerPage = $pageName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetMsnAds
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetMsnAds()
{
   $this->msnAds = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetVehiclePopunder
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetVehiclePopunder()
{
   $this->vehiclePopunder = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Run
//
// [DESCRIPTION]:   Run the website
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Run()
{
   if(! $this->Prepare())
      return false;

   $this->PrepareCookie();

   $this->Show();

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDbHandle
//
// [DESCRIPTION]:   Return the database handle
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  database handle
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDbHandle()
{
   return $this->dbh;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserID
//
// [DESCRIPTION]:   Return the user ID
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  user ID
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserID()
{
   return $this->userID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserObj
//
// [DESCRIPTION]:   Return the user object
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  user object
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserObj()
{
   return $this->userObj;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTmplDir
//
// [DESCRIPTION]:   Return the templates directory
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  string
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTmplDir()
{
   return TEMPLATES_DIR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Error
//
// [DESCRIPTION]:   Show error and exit program!
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Error($errString = "")
{
   $this->SetWorkArea($errString);
   $this->Run();
   exit(0);
}

//////////////////////////////////////////////////////////////////////////////FB
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    $this->dbh->Close();
    $this->templates   = array();
    $this->siteContent = "";

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetOfflineWebsite
//
// [DESCRIPTION]:   Show offline website message
//
// [PARAMETERS]:    $message
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel Istvancsek (gabi@acrux.biz) 2007-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetOfflineWebsite($message = '<br><br><br><br><b>This site is currently undergoing essential maintenance.  Please come back in 10 minutes.</b><br><br><br><br>')
{
    //$message='This site is currently under maintenance. Please come back in 10 minutes.';

    $this->Error("$message");

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetBannedIpWebsite
//
// [DESCRIPTION]:   Show ip banned website message
//
// [PARAMETERS]:    $message
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel Istvancsek (gabi@acrux.biz) 2007-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetBannedIpWebsite()
{
    $objTemplate = new CTemplate();

    $objTemplate->SetTemplateDirectory($this->templatesDirectory);
    $objTemplate->Load('IpBanned.tmpl');
    $objTemplate->Show();
    exit(0);

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckCookieSessionID
//
// [DESCRIPTION]:   Check cookie session id
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none | string
//
// [CREATED BY]:    Gabriel Istvancsek (gabi@acrux.biz) 2007-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function PrepareCookie()
{
   if(! $cookieSession = $this->GetCookieSessionID())
      return;

   if(! preg_match_all("/\<form.*action=\"(.+)\".*\>/isU",$this->siteContent, $matches))
      return;

   //unique urls
   foreach($matches[1] as $index => $url)
      $resAllUrls[$url] = '1';

   foreach($resAllUrls as $url => $value)
   {
      $prepUrl = $url."?cs=$cookieSession";
      if(preg_match("/\?/", $url))
         $prepUrl = $url."&cs=$cookieSession";

      //$prepUrl = preg_quote($prepUrl);
      $url     = preg_quote($url);
      $this->siteContent = preg_replace("/$url/", "$prepUrl", $this->siteContent);
   }

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCookieSessionID
//
// [DESCRIPTION]:   Get cookie session id
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none | string
//
// [CREATED BY]:    Gabriel Istvancsek (gabi@acrux.biz) 2007-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCookieSessionID()
{
   if(defined('COOKIE_SESSION_ID'))
      return COOKIE_SESSION_ID;

   return false;
}

}// end of CWebSite class

?>

<?php
/*****************************************************************************/
/*                                                                           */
/*  CQuinnVehicle class interface                                            */
/*                                                                           */
/*  (C) 2005 Racu Daniel (dade@acrux.biz)                                   */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CSwintonVehicle
//
// [DESCRIPTION]:  CSwintonVehicle class interface
//
// [FUNCTIONS]:  
//
//                Close();
//                GetError();
//                ShowError();
//
// [CREATED BY]:  Racu Daniel (dade@acrux.biz)
//
// [MODIFIED]:    - [programmer (email) date]
//                  [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CSwintonVehicle
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuinnVehicle
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CSwintonVehicle($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}



//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuinnVehicleByAbiCode
//
// [DESCRIPTION]:   Read data of a car from QUINN_VEHICLE table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Racu Daniel  (dade@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSwintonVehicleByAbiCode($vehicleConfirm='')
{
   if(! preg_match("/^\d+$/", $vehicleConfirm))
   {
      $this->strERR = GetErrorString("INVALID_SWINTON_VEHICEL_ID");
      return false;
   }

   $lastSqlCmd = "select * from SWINTON where AbiCode  = '".$vehicleConfirm."'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $error = $this->dbh->GetError();
     print "\n".$error."\n";
   }

   if(! $this->dbh->GetRows())
   {
      $error = "ROW_NOT_FOUND";
      print "\n".$error."\n";
   }

   $vehicleDetails = array();
   
   while($this->dbh->MoveNext())
   {
		$vehicleDetails["AbiVehicleModelId"] = $this->dbh->GetFieldValue("AbiVehicleModelId");
		$vehicleDetails["AbiManufacturerId"] = $this->dbh->GetFieldValue("AbiManufacturerId");
		$vehicleDetails["Name"]              = $this->dbh->GetFieldValue("Name");
		$vehicleDetails["CscName"]           = $this->dbh->GetFieldValue("CscName");
		$vehicleDetails["Cc"]                = $this->dbh->GetFieldValue("Cc");
		$vehicleDetails["YearFrom"]          = $this->dbh->GetFieldValue("YearFrom");
		$vehicleDetails["YearTo"]            = $this->dbh->GetFieldValue("YearTo");
		$vehicleDetails["BodyType"]          = $this->dbh->GetFieldValue("BodyType");
		$vehicleDetails["FuelType"]          = $this->dbh->GetFieldValue("FuelType");
		$vehicleDetails["TransType"]         = $this->dbh->GetFieldValue("TransType");
		$vehicleDetails["Doors"]             = $this->dbh->GetFieldValue("Doors");
		$vehicleDetails["AbiCode"]           = $this->dbh->GetFieldValue("AbiCode");
		$vehicleDetails["StartDate"]         = $this->dbh->GetFieldValue("StartDate");
		$vehicleDetails["ExpiryDate"]        = $this->dbh->GetFieldValue("ExpiryDate");
   }

   return $vehicleDetails;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Show the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

}//end class CQuinnVehicle

?>

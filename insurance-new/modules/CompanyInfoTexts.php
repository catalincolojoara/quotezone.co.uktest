<?php
/*****************************************************************************/
/*                                                                           */
/*  CCompanyInfoTexts class interface                                       		  */
/*                                                                           */
/*  (C) 2011 Goia Calin (calin@acrux.biz)                                   */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

define("SQL_COMPANY_INFO_TEXTS","ci_new.company_info_texts");

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CCompanyInfoTexts
//
// [DESCRIPTION]:  CCompanyInfoTexts class interface
//
// [FUNCTIONS]:    int   AddText($siteID=0, $txt='')
//                 bool  DeleteText($id=0)
//                 array GetCompanyText($siteID=0)
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Goia Calin (calin@acrux.biz) 18-07-2011
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CCompanyInfoTexts
{
// database handler
var $dbh;         // database server handle
var $closeDB;     // close database flag

// class-internal variables
var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CCompanyInfoTexts
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 18-07-2011
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CCompanyInfoTexts($dbh=0)
{
	if($dbh)
	{
		$this->dbh = $dbh;
		$this->closeDB = false;
	}
	else
	{
		// default configuration
		$this->dbh = new CMySQL();

		if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
		{
			$this->strERR = $this->dbh->GetError();
			return;
		}

		$this->closeDB = true;
	}

	$this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddText
//
// [DESCRIPTION]:   Add new entry to the company_info_texts table
//
// [PARAMETERS]:    $siteID=0, $txt=''
//
// [RETURN VALUE]:  ID or 0 in case of failure
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 18-07-2011
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddText($siteID=0, $txt='')
{
	if(! preg_match("/^\d+$/", $siteID))
    {
      $this->strERR = GetErrorString("INVALID_SITE_ID_FIELD");
      return false;
    }
	
	$sqlCMD = "INSERT INTO ".SQL_COMPANY_INFO_TEXTS." (site_id, company_text) VALUES ('$siteID', '".addslashes($txt)."')";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	$sqlCMD = "SELECT LAST_INSERT_ID() AS id";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	if(! $this->dbh->FetchRows())
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	return $this->dbh->GetFieldValue("id");

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteText
//
// [DESCRIPTION]:   Delete an entry from company_info_texts table
//
// [PARAMETERS]:    $id=0
//
// [RETURN VALUE]:   true | false
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 18-07-2011
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteText($id=0)
{
   if(! preg_match("/^\d+$/", $id))
   {
      $this->strERR = GetErrorString("INVALID_ID_FIELD");
      return false;
   }

   // check if id exists
   $sqlCMD = "SELECT id FROM ".SQL_COMPANY_INFO_TEXTS." WHERE site_id='$id'";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("ID_NOT_FOUND");
      return false;
   }

   $sqlCMD = "DELETE FROM ".SQL_COMPANY_INFO_TEXTS." WHERE site_id='$id'";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCompanyText
//
// [DESCRIPTION]:   Read data from company_info_texts tables and put it into an array variable
//
// [PARAMETERS]:    $siteID=0
//
// [RETURN VALUE]:  Array(with the parameters that belong to the siteID) or false in case of failure
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 18-07-2011
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCompanyText($siteID=0)
{
   $sqlCMD = "SELECT * FROM ".SQL_COMPANY_INFO_TEXTS." WHERE site_id='$siteID'";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITE_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]               = $this->dbh->GetFieldValue("id");
   $arrayResult["site_id"]          = $this->dbh->GetFieldValue("site_id");
   $arrayResult["company_text"]     = $this->dbh->GetFieldValue("company_text");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSearchSitesCount
//
// [DESCRIPTION]:   Get number of searched sites
//
// [PARAMETERS]:    $cnt
//
// [RETURN VALUE]:  int
//
// [CREATED BY]:    Ciprian Sturza (cipi@acrux.biz) 2007-02-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSearchSitesCount($siteName="", $quoteTypeID="0")
{
   if($quoteTypeID != "0")
      $type = "AND s.quote_type_id=$quoteTypeID";

   $sqlCmd= "SELECT COUNT(cit.id) AS cnt FROM ".SQL_COMPANY_INFO_TEXTS." cit, ".SQL_SITES." s WHERE s.id = cit.site_id AND s.name LIKE '%$siteName%' $type";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
      return 0;

   $cnt = $this->dbh->GetFieldValue("cnt");

   if(! $cnt)
      $cnt = 0;


   return $cnt;

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSitesCount
//
// [DESCRIPTION]:   Get number of sites
//
// [PARAMETERS]:    $cnt
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSitesCount($quoteTypeID="0")
{
   if($quoteTypeID != "0")
      $type = "AND s.quote_type_id=$quoteTypeID";

   $sqlCmd = "SELECT COUNT(cit.id) AS cnt FROM ".SQL_COMPANY_INFO_TEXTS." cit, ".SQL_SITES." s WHERE s.id = cit.site_id $type";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
      return 0;

   $cnt = $this->dbh->GetFieldValue("cnt");

   if(! $cnt)
      $cnt = 0;

   return $cnt;

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SearchSiteByName
//
// [DESCRIPTION]:   Read data from sites table and put it into an array variable
//
// [PARAMETERS]:    $siteName="", $limit=0, $offset=0, $quoteTypeID=0
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Ciprian Sturza (cipi@acrux.biz) 2007-02-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SearchSiteByName($siteName="", $limit=0, $offset=0, $quoteTypeID=0)
{
   if($quoteTypeID != "0")
      $type = " AND s.quote_type_id=$quoteTypeID";

   $sqlCmd= "SELECT s.*, qz.name AS type, cit.company_text FROM ". SQL_SITES." s, ".SQL_QZQUOTE_TYPES." qz, ".SQL_COMPANY_INFO_TEXTS." cit WHERE cit.site_id = s.id AND s.quote_type_id=qz.id $type AND s.name LIKE '%$siteName%' ORDER BY s.name";

   if($limit)
   {
      if($offset)
         $sqlCmd .= " LIMIT $offset,$limit";
      else
         $sqlCmd .= " LIMIT $limit";
   }

   if(! $this->dbh->Exec($sqlCmd, true))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITENAME_NOT_FOUND");
      return false;
   }

    while($this->dbh->MoveNext())
   {
      $id                                  = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["id"]              = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["name"]            = $this->dbh->GetFieldValue("name");
      $arrayResult[$id]["proto"]           = $this->dbh->GetFieldValue("proto");
      $arrayResult[$id]["description"]     = $this->dbh->GetFieldValue("description");
      $arrayResult[$id]["results"]         = $this->dbh->GetFieldValue("results");
      $arrayResult[$id]["master_site_id"]  = $this->dbh->GetFieldValue("master_site_id");
      $arrayResult[$id]["status"]          = $this->dbh->GetFieldValue("status");
      $arrayResult[$id]["type"]            = $this->dbh->GetFieldValue("type");
      $arrayResult[$id]["returns"]         = $this->dbh->GetFieldValue("returns");
      $arrayResult[$id]["company_text"]    = $this->dbh->GetFieldValue("company_text");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllSites
//
// [DESCRIPTION]:   Read data from sites table and put it into an array variable
//                  key = siteID, value = siteName
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllSites($offset=0, $limit=0, $quoteTypeID="0")
{
   if(! preg_match("/^\d+$/", $offset))
   {
      $this->strERR = GetErrorString("INVALID_OFFSET_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $limit))
   {
      $this->strERR = GetErrorString("INVALID_LIMIT_FIELD");
      return false;
   }

   if($quoteTypeID != "0")
      $type = " AND s.quote_type_id=$quoteTypeID";

   $sqlCmd = "SELECT s.*, qz.name as type, cit.company_text FROM ".SQL_SITES." s, ".SQL_QZQUOTE_TYPES." qz, ".SQL_COMPANY_INFO_TEXTS." cit WHERE cit.site_id = s.id AND s.quote_type_id=qz.id $type ORDER BY s.name";

   if($offset || $limit)
   {
      $sqlCmd.= " LIMIT $offset";

      if($limit)
         $sqlCmd.= ",$limit";
      else
         $sqlCmd.= ",-1";
   }

   if(! $this->dbh->Exec($sqlCmd, true))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITES_NOT_FOUND");
      return false;
   }

   $cnt = 0;
   while($this->dbh->MoveNext())
   {
      $cnt++;
      $arrayResult[$cnt]["id"]              = $this->dbh->GetFieldValue("id");
      $arrayResult[$cnt]["name"]            = $this->dbh->GetFieldValue("name");
      $arrayResult[$cnt]["proto"]           = $this->dbh->GetFieldValue("proto");
      $arrayResult[$cnt]["type"]            = $this->dbh->GetFieldValue("type");
      $arrayResult[$cnt]["description"]     = $this->dbh->GetFieldValue("description");
      $arrayResult[$cnt]["results"]         = $this->dbh->GetFieldValue("results");
      $arrayResult[$cnt]["master_site_id"]  = $this->dbh->GetFieldValue("master_site_id");
      $arrayResult[$cnt]["status"]          = $this->dbh->GetFieldValue("status");
      $arrayResult[$cnt]["returns"]         = $this->dbh->GetFieldValue("returns");
      $arrayResult[$cnt]["company_text"]    = $this->dbh->GetFieldValue("company_text");
      
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSite
//
// [DESCRIPTION]:   Read data from sites table and put it into an array variable
//
// [PARAMETERS]:    $siteID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSite($siteID=0)
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITEID_FIELD");
      return false;
   }

   $sqlCMD = "SELECT s.name,s.id,cit.company_text,qz.name as type FROM ".SQL_SITES." s, ".SQL_COMPANY_INFO_TEXTS." cit, ".SQL_QZQUOTE_TYPES." qz WHERE s.id = cit.site_id AND s.quote_type_id=qz.id AND s.id='$siteID'";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("SITEID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]             = $this->dbh->GetFieldValue("id");
   $arrayResult["name"]           = $this->dbh->GetFieldValue("name");
   $arrayResult["company_text"]   = $this->dbh->GetFieldValue("company_text");
   $arrayResult["type"]           = $this->dbh->GetFieldValue("type");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateSite
//
// [DESCRIPTION]:   Update sites table
//
// [PARAMETERS]:    $siteID=0, $name='', $proto=''
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateSite($siteID=0, $companyText='')
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITEID_FIELD");
      return false;
   }

   if(empty($companyText))
   {
      $this->strERR = "Invalid company text";
      return false;
   }

   // check if siteID exists
   $sqlCMD = "SELECT id FROM ".SQL_COMPANY_INFO_TEXTS." WHERE site_id='$siteID'";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITEID_NOT_FOUND");
      return false;
   }

   $sqlCMD = "UPDATE ".SQL_COMPANY_INFO_TEXTS." SET company_text='$companyText' WHERE site_id='$siteID'";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllSitesByType
//
// [DESCRIPTION]:   Read data from sites table and put it into an array variable
//                  key = siteID, value = siteName
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllSitesybyType($quoteTypeID='', $siteMode='allSites')
{
   if(! empty($quoteTypeID))
      if(! preg_match("/^\d+$/",$quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_SITEID_FIELD");
         return false;
      }

   $this->lastSQLCMD = "SELECT s.id,s.name,cit.id as citID FROM ".SQL_SITES." s LEFT JOIN ".SQL_COMPANY_INFO_TEXTS." cit ON s.id = cit.site_id WHERE cit.id is NULL ";

   if(! empty($quoteTypeID))
      $this->lastSQLCMD.= " AND quote_type_id='$quoteTypeID'";

   if($siteMode == 'onlyOnline')
      $this->lastSQLCMD.= "  and status = 'ON' ";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITES_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("name");

//   print_r($arrayResult);

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 18-07-2011
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Goia Calin (calin@acrux.biz) 18-07-2011
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

}

?>

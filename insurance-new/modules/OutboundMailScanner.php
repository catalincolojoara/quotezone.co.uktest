<?php
include_once "Outbound.php";
include_once "File.php";

$outboundObj = new COutbound();
$file        = new CFile();   

//values got from globals.inc due to sync problem
$ipServer    = $OUTBOUND_IP_SERVER;
$results = $outboundObj->GetWaitingMailOutbounds($ipServer);

CheckLog();


function CheckLog($logFileName="/var/log/maillog")
{
   global $outboundObj;
   global $file;
   global $ipServer;
   global $results;
   
   if(! $file->Open($logFileName, "r"))
      print $file->GetError();

   if(! $size = $file->GetSize($logFileName))
      print $file->GetError();

   if(! $data = $file->Read($size))
      print $file->GetError();

   $file->Close();


 
   foreach($results as $key => $resArray)
   {
      $msgId = $resArray["messageId"];
      $outId = $resArray["id"];

      if(empty($msgId))
        continue;

      preg_match_all("/(.*$msgId.*)/im",$data,$match);


      foreach($match[0] as $key => $matchString)
      { 
         if(preg_match("/stat=sent/i",$matchString,$stats))
            $outboundObj->UpdateOutboundField($outId, "received_status","1");
         else
	    $outboundObj->UpdateOutboundField($outId, "received_status","2");
     }
   }
}

?>

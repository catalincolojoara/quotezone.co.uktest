<?php
/*****************************************************************************/
/*                                                                           */
/*  COvertureTrackerArticles class interface                                 */
/*                                                                           */
/*  (C) 2009 Moraru Valeriu (vali@acrux.biz)                                 */
/*                                                                           */
/*****************************************************************************/

include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);



//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   COvertureTrackerArticles
//
// [DESCRIPTION]:  COvertureTracker class interface
//
// [FUNCTIONS]:    bool AssertOvertureTraker($articleID=0,$imression='',$click='',$nrLinksRequested=0,$top='0',$url='',$hostIP='')
//                 int  AddOvertureTracker($articleID=0,$imression='',$click='',$nrLinksRequested=0,$top='0',$url='',$hostIP='')
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Moraru Valeriu (vali@acrux.biz) 29-10-2009
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class COvertureTrackerArticles
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: COvertureTracker
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 29-10-2009
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function COvertureTrackerArticles($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertOverureTraker
//
// [DESCRIPTION]:   Verify to see if the params are completed
//
// [PARAMETERS]:    $articleID=0,$imression='',$click='',$nrLinksRequested=0,$top='0',$url='',$hostIP=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 29-10-2009
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertOvertureTraker($articleID, $imression, $click, $nrLinksRequested, $top, $url, $hostIP)
{
   $this->strERR = "";

   if(! preg_match("/^\d+$/", $articleID))
      $this->strERR = GetErrorString("INVALID_ARTICLE_ID_FIELD");

   if(! preg_match("/0|1/", $imression))
      $this->strERR = GetErrorString("INVALID_IMPRESSION_FIELD");

   if(! preg_match("/0|1/", $click))
      $this->strERR = GetErrorString("INVALID_CLICK_FIELD");

   if(! preg_match("/^\d+$/", $nrLinksRequested))
      $this->strERR = GetErrorString("INVALID_NR_LINKS_REQUESTED_FIELD");

   if(! preg_match("/^\d+$/", $top))
      $this->strERR = GetErrorString("INVALID_TOP_FIELD");

   if(! preg_match("/((1|2)?\d?\d)\.((1|2)?\d?\d)\.((1|2)?\d?\d)\.((1|2)?\d?\d)/", $hostIP))
      $this->strERR = GetErrorString("INVALID_HOST_IP_FIELD");

    if($this->strERR != "")
      return true;

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddOvertureTracker
//
// [DESCRIPTION]:   Add new entry to the overture_tracker table
//
// [PARAMETERS]:    $articleID=0,$imression='',$click='',$nrLinksRequested=0,$top='0',$url='',$hostIP=''
//
// [RETURN VALUE]:  trakerID or 0 in case of failure
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 29-10-2009
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddOvertureTracker($articleID=0,$imression='',$click='',$nrLinksRequested=0,$top='0',$url='',$hostIP='')
{
   if($this->AssertOvertureTraker($articleID, $imression, $click, $nrLinksRequested, $top, $url, $hostIP))
      return 0;

   $date = date("Y-m-d"); // 0000-00-00
   $time = date("H:i:s"); // 00:00:00

   $this->lastSQLCMD = "INSERT INTO ". SQL_OVERTURE_ARTICLES_TRACKINGS."(overture_articles_id, impression, click, nr_links_requested, top, url, host_ip, date, time) VALUES ('$articleID','$imression', '$click', '$nrLinksRequested', '$top','$url','$hostIP', '$date','$time')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetArticleID
//
// [DESCRIPTION]:   Get article id if not exist insert it into DB and return its id
//
// [PARAMETERS]:    $articleTitle, $articleUrl
//
// [RETURN VALUE]:  trakerID or 0 in case of failure
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 29-10-2009
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetArticleID($articleTitle='', $articleUrl='')
{
   if(empty($articleTitle))
   {
      $this->strERR = GetErrorString("INVALID_ARTICLE_TITLE_FIELD");
      return 0;
   }

   if(empty($articleUrl))
   {
      $this->strERR = GetErrorString("INVALID_ARTICLE_URL_FIELD");
      return 0;
   }

   // check if the article already exists
   $this->lastSQLCMD = "SELECT id FROM ". SQL_OVERTURE_ARTICLES." WHERE title = '".$articleTitle."' AND url = '".$articleUrl."' ";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   if($this->dbh->FetchRows())
   {
      $articleID = $this->dbh->GetFieldValue("id");

      return $articleID;
   }
   else
   {
      $this->lastSQLCMD = "INSERT INTO ". SQL_OVERTURE_ARTICLES."(title, url) VALUES ('$articleTitle','$articleUrl') ";

      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }

      $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }

      return $this->dbh->GetFieldValue("id");
   }
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllArticles
//
// [DESCRIPTION]:   Read data from overture_articles table and put it into an array variable
//
// [PARAMETERS]:    $arrayResult
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2009-10-30
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllArticles()
{
   $sqlCmd = "SELECT * FROM ".SQL_OVERTURE_ARTICLES." ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("ARTICLES_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("title");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllArticlesDetails
//
// [DESCRIPTION]:   Read data from overture_articles table and put it into an array variable
//
// [PARAMETERS]:    $arrayResult
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2009-10-30
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllArticlesDetails()
{
   $sqlCmd = "SELECT * FROM ".SQL_OVERTURE_ARTICLES." ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("ARTICLES_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("id")]["title"] = $this->dbh->GetFieldValue("title");
      $arrayResult[$this->dbh->GetFieldValue("id")]["url"]   = $this->dbh->GetFieldValue("url");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllArticlesImpressions
//
// [DESCRIPTION]:   Read data from overture_articles_tracker table and put it into an array variable
//
// [PARAMETERS]:    $startDate,$startTime,$endDate,$endTime,$articleID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2009-10-30
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllArticlesImpressions($startDate,$startTime,$endDate,$endTime,$articleID)
{
   $sqlCMD = "SELECT overture_articles_id,COUNT(id) as number FROM ".SQL_OVERTURE_ARTICLES_TRACKINGS." WHERE impression = 1 AND date >= '$startDate' AND time >= '$startTime' AND date <= '$endDate' AND time <= '$endTime' AND host_ip <> '81.196.65.167' AND host_ip <> '188.220.10.35' ";

   if($articleID != "")
      $sqlCMD .= " AND overture_articles_id = $articleID ";

   $sqlCMD .= " GROUP BY overture_articles_id";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $articleID        = $this->dbh->GetFieldValue("overture_articles_id");;
      $number           = $this->dbh->GetFieldValue("number");

      $arrayResult[$articleID] = $number;
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllArticlesImpressions
//
// [DESCRIPTION]:   Read data from overture_articles_tracker table and put it into an array variable
//
// [PARAMETERS]:    $startDate,$startTime,$endDate,$endTime,$articleID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2009-10-30
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllArticlesClicks($startDate,$startTime,$endDate,$endTime,$articleID)
{
   $sqlCMD = "SELECT overture_articles_id,COUNT(id) as number FROM ".SQL_OVERTURE_ARTICLES_TRACKINGS." WHERE click = 1 AND date >= '$startDate' AND time >= '$startTime' AND date <= '$endDate' AND time <= '$endTime' AND host_ip <> '81.196.65.167' AND host_ip <> '188.220.10.35' ";

   if($articleID != "")
      $sqlCMD .= " AND overture_articles_id = $articleID ";

   $sqlCMD .= " GROUP BY overture_articles_id";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $articleID        = $this->dbh->GetFieldValue("overture_articles_id");;
      $number           = $this->dbh->GetFieldValue("number");

      $arrayResult[$articleID] = $number;
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 29-10-2009
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 29-10-2009
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}


}
?>

<?php
/*****************************************************************************/
/*                                                                           */
/*  CQuoteUserDetails class interface                                              */
/*                                                                           */
/*  (C) 2005 Istvancsek Gabi(gabi@acrux.biz)                                   */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";
include_once "QzQuoteType.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuoteUserDetails
//
// [DESCRIPTION]:  CQuoteUser class interface
//
// [FUNCTIONS]:
//                 true | false DeleteQuoteUserDetails($userDetailsID='')
//                 array | false GetQuoteUserDetailsByID($userDetailsID='')
//                 array | false GetQuoteUserDetailsByEmail($userEmail='')
//                 array | false GetQuoteUserDetailsByEmailDateOfBirth($userEmail='', $dateOfBirth='')
//                 array | false GetAllQuoteUserDetails($quoteUserID='')
//                 array | false GetAllUsersDetailsByLogID($logID = '')
//
//                 Close();
//                 GetError();
//                 ShowError();
//
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CQuoteUserDetails
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string
    var $quoteType;

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuoteUserDetails
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQuoteUserDetails($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->qzQuoteType = new CQzQuoteType($this->dbh);

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuoteUser
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:  
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function _assertQuoteUserDetails($quoteUserID='', $qzQuoteTypeID='', $SESSION='', $type='add', &$sqlCMD)
{
   $this->strERR = '';

   if(empty($SESSION))
   {
      $this->strERR .= GetErrorString("INVALID_QUOTE_USER_DETAILS");
      return false;
   }

   if(! preg_match('/\d+/i',$quoteUserID))
   {
      $this->strERR .= GetErrorString("INVALID_USER_DETAILS_QUOTE_USER_ID");
      return false;
   }

   if(! preg_match("/^\d+$/", $qzQuoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   $resSession = array();

   $resSession['quote_user_id'] = $quoteUserID;
   $sqlParamCMD                 = '';

   $sqlCMD = "INSERT INTO ".SQL_QUOTE_USER_DETAILS."";

   switch($qzQuoteTypeID)
   {
      // car section
      case '1':

         ####################################################
         // get param values from the session
         ####################################################

         $SessionProposerDriver  = $SESSION['_DRIVERS_'][0];
         $SessionVehicle         = $SESSION['_VEHICLE_'];
         $SessionCover           = $SESSION['_COVER_'];
      
         $driversInsured = $SessionProposerDriver["who_drive_the_vehicle"];
      
         if($driversInsured == "IO")
            $driversInsured = 1;

         if($driversInsured == "IS")
            $driversInsured = 2;
      
         $driversInsured = intval($driversInsured);

         // vehicle
         $resSession['car_abi_code']         = $SessionVehicle['vehicle_confirm']; // Car abi code
         $resSession['vehicle_bought']       = $SessionVehicle['vehicle_bought'];  // Date of vehicle bought
         $resSession['estimated_value']      = $SessionVehicle['estimated_value']; // Car estimated value;

         // proposer data
         $resSession['title']                = $SessionProposerDriver["title"]; // Proposer title
         $resSession['house_number_or_name'] = $SessionProposerDriver["house_number_or_name"]; // Proposer house number and name
         $resSession['born_uk']              = $SessionProposerDriver["born_in_uk"]; // Proposer born in  uk
         $resSession['uk_res_date']          = $SessionProposerDriver["date_of_uk_resident"]; // Uk residence Proposer
         $resSession['marital_status']       = $SessionProposerDriver["marital_status"]; // Proposer marital status
         $resSession['type_drv_lic']         = $SessionProposerDriver["type_of_driving"]; // Proposer driver licence
         $resSession['drv_lic_date']         = $SessionProposerDriver["period_of_licence_held"]; // Driver licence date;
         $resSession['pass_plus']            = $SessionProposerDriver["driver_pass_plus"]; // Driver licence plus;

         $resSession['ft_employment_status'] = $SessionProposerDriver["employment_status"]; // Full time employment status
         $resSession['ft_business']          = $SessionProposerDriver["business_list"]; // Full time business
         $resSession['ft_ocupation']         = $SessionProposerDriver["occupation_list"]; // Full time occupation

         $resSession['have_part_time']       = $SessionProposerDriver["part_time_occupation"]; // Proposer part time
         $resSession['pt_employment_status'] = $SessionProposerDriver["employment_status_pt"]; // Proposer part time employment status
         $resSession['pt_business']          = $SessionProposerDriver["business_list_pt"]; // Proposer part time business
         $resSession['pt_ocupation']         = $SessionProposerDriver["occupation_list_pt"]; // Proposer part time occupation

         $resSession['day_phone']            = $SessionProposerDriver["daytime_telephone_prefix"]."-".$SessionProposerDriver["daytime_telephone_number"]; // Proposer day(work) phone
         $resSession['evening_phone']        = $SessionProposerDriver["home_telephone_prefix"]."-".$SessionProposerDriver["home_telephone_number"]; // Proposer evening(home) phone
         $resSession['mobile_phone']         = $SessionProposerDriver["mobile_telephone_prefix"]."-".$SessionProposerDriver["mobile_telephone_number"]; // Proposer mobile phone

         $resSession['post_code']            = $SessionProposerDriver["postcode_prefix"]." ".$SessionProposerDriver["postcode_number"]; // Proposer postcode

         $resSession['home_owner']           = $SessionProposerDriver["home_owner"]; // Proposer home owner
         $resSession['email']                = $SessionProposerDriver["email_address"]; // Proposer email address
         $resSession['start_insurance_date'] = $SessionProposerDriver["date_of_insurance_start"]; // Proposer start insurance date

         $resSession['claims']               = $SessionProposerDriver["had_any_accidents"]; // Have proposer claims
         $resSession['convictions']          = $SessionProposerDriver["had_any_convictions"]; // Have proposer claims
         
         // cover
         $resSession['policy_expire']        = $SessionCover["policy_expire"]; // Policy expire
         $resSession['type_of_cover']        = $SessionCover['type_of_cover']; // Type of cover
         $resSession['type_of_use']          = $SessionCover['type_of_use']; //Type of use

         // other - like additional drivers, claims, convictions 
         $resSession['drivers_insured']      = $driversInsured; // how many drivers insured
         
         // convert date into sql date format

         if(! $resSession['vehicle_bought'] = $this->StdToMysqlDate($resSession['vehicle_bought']))
            return false;

         if(! $resSession['policy_expire'] = $this->StdToMysqlDate($resSession['policy_expire']))
            return false;

         if(! $resSession['uk_res_date'] = $this->StdToMysqlDate($resSession['uk_res_date']))
            return false;

         if(! $resSession['drv_lic_date'] = $this->StdToMysqlDate($resSession['drv_lic_date']))
            return false;

         if(! $resSession['start_insurance_date'] = $this->StdToMysqlDate($resSession['start_insurance_date']))
            return false;


         ####################################################         
         // check the params values
         ####################################################


         if(! preg_match('/[(Mr)|(Mrs)|(Ms)|(Miss)]/i',$resSession['title']))
            $this->strERR = GetErrorString("INVALID_TITLE_FIELD");
      
         if(! preg_match('/\d+/i',$resSession['estimated_value']))
            $this->strERR .= GetErrorString("INVALID_ESTIMATED_VALUE_FIELD");
      
         $resSession['house_number_or_name'] = str_replace("'","\'",$resSession['house_number_or_name']);
      
         if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$resSession['house_number_or_name']))
            $this->strERR .= GetErrorString("INVALID_ADDRESS_NAME_FIELD");
      
         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['policy_expire']))
            $this->strERR .= GetErrorString("INVALID_POLICY_EXPIRE_FIELD");
      
         if(! preg_match('/[A-Z]{1}/',$resSession['born_uk']))
            $this->strERR .= GetErrorString("INVALID_BORN_UK_FIELD");
      
         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['uk_res_date']))
            $this->strERR .= GetErrorString("INVALID_UK_RESIDENCE_DATE_FIELD");
      
         if(! preg_match('/[A-Z]{1}/',$resSession['marital_status']))
            $this->strERR .= GetErrorString("INVALID_MARITAL_STATUS_FIELD");
      
         if(! preg_match('/[A-Z]{1}/',$resSession['type_drv_lic']))
            $this->strERR .= GetErrorString("INVALID_TYPE_DRIVEN_LICENCE_FIELD");
      
         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['drv_lic_date']))
            $this->strERR .= GetErrorString("INVALID_DRIVE_LICENCE_DATE_FIELD");
      
         if(! preg_match('/[A-Z]{1}/',$resSession['pass_plus']))
            $this->strERR .= GetErrorString("INVALID_PASS_PLUS_FIELD");
      
         if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$resSession['ft_employment_status']))
            $this->strERR .= GetErrorString("INVALID_FULL_TIME_EMPLOYMENT_STATUS_FIELD");
      
         if(! preg_match('/[0-9\-\s\_]{1,32}/i',$resSession['day_phone']))
            $this->strERR .= GetErrorString("INVALID_DAY_PHONE_FIELD");
      
         if(! preg_match('/[0-9\-\s\_]{1,32}/i',$resSession['evening_phone']))
            $this->strERR .= GetErrorString("INVALID_EVENING_PHONE_FIELD");
      
         if(! preg_match('/[0-9\-\s\_]{1,32}/i',$resSession['mobile_phone']))
            $this->strERR .= GetErrorString("INVALID_MOBILE_PHONE_FIELD");
      
         if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,16}/i',$resSession['post_code']))
            $this->strERR .= GetErrorString("INVALID_POST_CODE_FIELD");
      
         if(! preg_match('/[A-Z]{1}/',$resSession['home_owner']))
            $this->strERR .= GetErrorString("INVALID_HOME_OWNER_FIELD");
      
         if(! preg_match('/\.*\@.*\.[a-zA-Z]{2,3}/i',$resSession['email']))
            $this->strERR .= GetErrorString("INVALID_EMAIL_FIELD");
      
         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['start_insurance_date']))
            $this->strERR .= GetErrorString("INVALID_START_INSURANCE_FIELD");
      
         if(! preg_match('/^[1-4]$/',$resSession['drivers_insured']))
            $this->strERR .= GetErrorString("INVALID_DRIVERS_INSURED_FIELD");
      
         if(! preg_match('/[A-Z]{1}/',$resSession['claims']))
            $this->strERR .= GetErrorString("INVALID_CLAIMS_FIELD");
      
         if(! preg_match('/[A-Z]{1}/',$resSession['convictions']))
            $this->strERR .= GetErrorString("INVALID_CONVICTIONS_FIELD");
      
         if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,16}/i',$resSession['car_abi_code']))
            $this->strERR .= GetErrorString("INVALID_CAR_ABI_CODE_FIELD");
      
         if(! preg_match('/\d+/i',$resSession['type_of_cover']))
            $this->strERR = GetErrorString("INVALID_TYPE_OF_COVER_FIELD");

         if(! preg_match('/\d+/i',$resSession['type_of_use']))
            $this->strERR = GetErrorString("INVALID_TYPE_OF_USE_FIELD");
      
         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['vehicle_bought']))
            $this->strERR .= GetErrorString("INVALID_VEHICLE_BOUGHT_DATE_FIELD");
         
         break;

      // van section
      case '2':

         ####################################################         
         // get param values from the session
         ####################################################

         $SessionProposerDriver  = $SESSION['_DRIVERS_'][0];
         $SessionVehicle         = $SESSION['_VEHICLE_'];
         $SessionCover           = $SESSION['_COVER_'];
      
         $driversInsured = $SessionProposerDriver["who_drive_the_vehicle"];
      
          if($driversInsured == "IO")
            $driversInsured = 1;

         if($driversInsured == "IS")
            $driversInsured = 2;
      
         $driversInsured = intval($driversInsured);
      
         // vehicle
         $resSession['car_abi_code']         = $SessionVehicle['vehicle_confirm']; // Car abi code
         $resSession['vehicle_bought']       = $SessionVehicle['vehicle_bought'];  // Date of vehicle bought
         $resSession['estimated_value']      = $SessionVehicle['estimated_value']; // Car estimated value;

         // proposer data
         $resSession['title']                = $SessionProposerDriver["title"]; // Proposer title
         $resSession['house_number_or_name'] = $SessionProposerDriver["house_number_or_name"]; // Proposer house number and name
         $resSession['born_uk']              = $SessionProposerDriver["born_in_uk"]; // Proposer born in  uk
         $resSession['uk_res_date']          = $SessionProposerDriver["date_of_uk_resident"]; // Uk residence Proposer
         $resSession['marital_status']       = $SessionProposerDriver["marital_status"]; // Proposer marital status
         $resSession['type_drv_lic']         = $SessionProposerDriver["type_of_driving"]; // Proposer driver licence
         $resSession['drv_lic_date']         = $SessionProposerDriver["period_of_licence_held"]; // Driver licence date;
         
         $resSession['ft_employment_status'] = $SessionProposerDriver["employment_status"]; // Full time employment status
         $resSession['ft_business']          = $SessionProposerDriver["business_list"]; // Full time business
         $resSession['ft_ocupation']         = $SessionProposerDriver["occupation_list"]; // Full time occupation

         $resSession['have_part_time']       = $SessionProposerDriver["part_time_occupation"]; // Proposer part time
         $resSession['pt_employment_status'] = $SessionProposerDriver["employment_status_pt"]; // Proposer part time employment status
         $resSession['pt_business']          = $SessionProposerDriver["business_list_pt"]; // Proposer part time business
         $resSession['pt_ocupation']         = $SessionProposerDriver["occupation_list_pt"]; // Proposer part time occupation

         $resSession['day_phone']            = $SessionProposerDriver["daytime_telephone_prefix"]."-".$SessionProposerDriver["daytime_telephone_number"]; // Proposer day(work) phone
         $resSession['evening_phone']        = $SessionProposerDriver["home_telephone_prefix"]."-".$SessionProposerDriver["home_telephone_number"]; // Proposer evening(home) phone
         $resSession['mobile_phone']         = $SessionProposerDriver["mobile_telephone_prefix"]."-".$SessionProposerDriver["mobile_telephone_number"]; // Proposer mobile phone

         $resSession['post_code']            = $SessionProposerDriver["postcode_prefix"]." ".$SessionProposerDriver["postcode_number"]; // Proposer postcode

         $resSession['home_owner']           = $SessionProposerDriver["home_owner"]; // Proposer home owner
         $resSession['email']                = $SessionProposerDriver["email_address"]; // Proposer email address
         $resSession['start_insurance_date'] = $SessionProposerDriver["date_of_insurance_start"]; // Proposer start insurance date

         $resSession['claims']               = $SessionProposerDriver["had_any_accidents"]; // Have proposer claims
         $resSession['convictions']          = $SessionProposerDriver["had_any_convictions"]; // Have proposer claims
         
         // cover
         $resSession['policy_expire']        = $SessionCover["policy_expire"]; // Policy expire
         $resSession['type_of_cover']        = $SessionCover['type_of_cover']; // Type of cover
         $resSession['type_of_use']          = $SessionCover['type_of_use']; //Type of use

         // other - like additional drivers, claims, convictions 
         $resSession['drivers_insured']      = $driversInsured; // how many drivers insured
         
         // convert date into sql date format

         if(! $resSession['vehicle_bought'] = $this->StdToMysqlDate($resSession['vehicle_bought']))
            return false;
      
         if(! $resSession['policy_expire'] = $this->StdToMysqlDate($resSession['policy_expire']))
            return false;
      
         if(! $resSession['uk_res_date'] = $this->StdToMysqlDate($resSession['uk_res_date']))
            return false;
      
         if(! $resSession['drv_lic_date'] = $this->StdToMysqlDate($resSession['drv_lic_date']))
            return false;
      
         if(! $resSession['start_insurance_date'] = $this->StdToMysqlDate($resSession['start_insurance_date']))
            return false;


         ####################################################         
         // check the params values
         ####################################################


         if(! preg_match('/[(Mr)|(Mrs)|(Ms)|(Miss)]/i',$resSession['title']))
            $this->strERR = GetErrorString("INVALID_TITLE_FIELD");
      
         if(! preg_match('/\d+/i',$resSession['estimated_value']))
            $this->strERR .= GetErrorString("INVALID_ESTIMATED_VALUE_FIELD");
      
         $resSession['house_number_or_name'] = str_replace("'","\'",$resSession['house_number_or_name']);
      
         if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$resSession['house_number_or_name']))
            $this->strERR .= GetErrorString("INVALID_ADDRESS_NAME_FIELD");
      
         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['policy_expire']))
            $this->strERR .= GetErrorString("INVALID_POLICY_EXPIRE_FIELD");
      
         if(! preg_match('/[A-Z]{1}/',$resSession['born_uk']))
            $this->strERR .= GetErrorString("INVALID_BORN_UK_FIELD");
      
         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['uk_res_date']))
            $this->strERR .= GetErrorString("INVALID_UK_RESIDENCE_DATE_FIELD");
      
         if(! preg_match('/[A-Z]{1}/',$resSession['marital_status']))
            $this->strERR .= GetErrorString("INVALID_MARITAL_STATUS_FIELD");
      
         if(! preg_match('/[A-Z]{1}/',$resSession['type_drv_lic']))
            $this->strERR .= GetErrorString("INVALID_TYPE_DRIVEN_LICENCE_FIELD");
      
         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['drv_lic_date']))
            $this->strERR .= GetErrorString("INVALID_DRIVE_LICENCE_DATE_FIELD");
      
         if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$resSession['ft_employment_status']))
            $this->strERR .= GetErrorString("INVALID_FULL_TIME_EMPLOYMENT_STATUS_FIELD");
      
         if(! preg_match('/[0-9\-\s\_]{1,32}/i',$resSession['day_phone']))
            $this->strERR .= GetErrorString("INVALID_DAY_PHONE_FIELD");
      
         if(! preg_match('/[0-9\-\s\_]{1,32}/i',$resSession['evening_phone']))
            $this->strERR .= GetErrorString("INVALID_EVENING_PHONE_FIELD");
      
         if(! preg_match('/[0-9\-\s\_]{1,32}/i',$resSession['mobile_phone']))
            $this->strERR .= GetErrorString("INVALID_MOBILE_PHONE_FIELD");
      
         if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,16}/i',$resSession['post_code']))
            $this->strERR .= GetErrorString("INVALID_POST_CODE_FIELD");
      
         if(! preg_match('/[A-Z]{1}/',$resSession['home_owner']))
            $this->strERR .= GetErrorString("INVALID_HOME_OWNER_FIELD");
      
         if(! preg_match('/\.*\@.*\.[a-zA-Z]{2,3}/i',$resSession['email']))
            $this->strERR .= GetErrorString("INVALID_EMAIL_FIELD");
      
         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['start_insurance_date']))
            $this->strERR .= GetErrorString("INVALID_START_INSURANCE_FIELD");
      
         if(! preg_match('/^[1-4]$/',$resSession['drivers_insured']))
            $this->strERR .= GetErrorString("INVALID_DRIVERS_INSURED_FIELD");
      
         if(! preg_match('/[A-Z]{1}/',$resSession['claims']))
            $this->strERR .= GetErrorString("INVALID_CLAIMS_FIELD");
      
         if(! preg_match('/[A-Z]{1}/',$resSession['convictions']))
            $this->strERR .= GetErrorString("INVALID_CONVICTIONS_FIELD");
      
         if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,16}/i',$resSession['car_abi_code']))
            $this->strERR .= GetErrorString("INVALID_CAR_ABI_CODE_FIELD");
      
         if(! preg_match('/\d+/i',$resSession['type_of_cover']))
            $this->strERR = GetErrorString("INVALID_TYPE_OF_COVER_FIELD");

         if(! preg_match('/\d+/i',$resSession['type_of_use']))
            $this->strERR = GetErrorString("INVALID_TYPE_OF_USE_FIELD");
      
         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['vehicle_bought']))
            $this->strERR .= GetErrorString("INVALID_VEHICLE_BOUGHT_DATE_FIELD");

         break;

      // generic section
      case '3':
         $this->strERR  .= 'INVALID_QUOTE_USER_DETAILS_QUOTE_TYPE_ID_GENERIC_SECTION_UNUSED';
         break;

      // home section
      case '4':

//          print "<pre>";
//          print_r($_SESSION);

         ####################################################
         // get param values from the session
         ####################################################

         $SessionHome            = $SESSION['_HOME_'];
         $SessionHomeExtra       = $SESSION['_HOME_EXTRA_'];
         $SessionProposer        = $SESSION['_INSURED_'][0];
         $SessionCover           = $SESSION['_COVER_'];
      
         $driversInsured = $SessionDriver["who_drive_the_vehicle"];
      
         if($driversInsured == "IO")
            $driversInsured = 1;
      
         $driversInsured = intval($driversInsured);

         // home
         $resSession['property_type']         = $SessionHome['property_type'];
         $resSession['property_owned']        = $SessionHome['property_owned'];
         $resSession['property_rooms']        = $SessionHome['property_rooms'];
         $resSession['property_bedrooms']     = $SessionHome['property_bedrooms'];
         $resSession['property_year']         = $SessionHome['property_year'];
         $resSession['property_business']     = $SessionHome['property_business'];
         $resSession['property_work']         = $SessionHome['property_work'];
         $resSession['property_unoccupied']   = $SessionHome['property_unoccupied'];
         $resSession['property_watch']        = $SessionHome['property_watch'];
         $resSession['property_prof_alarm']   = $SessionHome['property_prof_alarm'];
         $resSession['property_alert_alarm']  = $SessionHome['property_alert_alarm'];
         $resSession['property_door_lock']    = $SessionHome['property_door_lock'];
         $resSession['property_window_lock']  = $SessionHome['property_window_lock'];
         $resSession['property_smoke_alarm']  = $SessionHome['property_smoke_alarm'];

         // add extra parameters
         $resSession['property_permanent_residence'] = $SessionHome['property_permanent_residence'];
         $resSession['property_unoccupied_days']     = $SessionHome['property_unoccupied_days'];
         $resSession['own_lockable_entrance']        = $SessionHome['own_lockable_entrance'];
         $resSession['property_french_lock']         = $SessionHome['property_french_lock'];

         // home extra page
         $resSession['property_state_of_repair']  = $SessionHomeExtra['property_state_of_repair'];
         $resSession['property_listed']           = $SessionHomeExtra['property_listed'];
         $resSession['property_roof_made']        = $SessionHomeExtra['property_roof_made'];
         $resSession['property_flat_roof']        = $SessionHomeExtra['property_flat_roof'];
         $resSession['property_walls_made']       = $SessionHomeExtra['property_walls_made'];
         $resSession['property_subsidence']       = $SessionHomeExtra['property_subsidence'];
         $resSession['property_landslip_heave']   = $SessionHomeExtra['property_landslip_heave'];
         $resSession['property_flooding']         = $SessionHomeExtra['property_flooding'];
         $resSession['property_nearest_river']    = $SessionHomeExtra['property_nearest_river'];
         $resSession['property_nearest_tree']     = $SessionHomeExtra['property_nearest_tree'];


         // proposer data

         $resSession['title']                = $SessionProposer["title"]; // Proposer title
         $resSession['house_number_or_name'] = $SessionProposer["house_number_or_name"]; // Proposer house number and name
         $resSession['marital_status']       = $SessionProposer["marital_status"]; // Proposer marital status
         $resSession['ft_employment_status'] = $SessionProposer["employment_status"]; // Full time employment status
         $resSession['ft_business']          = $SessionProposer["business_list"]; // Full time business
         $resSession['ft_occupation']        = $SessionProposer["occupation_list"]; // Full time occupation

         $resSession['have_part_time']       = $SessionProposer["part_time_occupation"]; // Proposer part time

         $resSession['day_phone']            = $SessionProposer["daytime_telephone_prefix"]."-".$SessionProposer["daytime_telephone_number"]; // Proposer day(work) phone
         $resSession['evening_phone']            = $SessionProposer["home_telephone_prefix"]."-".$SessionProposer["home_telephone_number"]; // Proposer evening(home) phone
         $resSession['mobile_phone']             = $SessionProposer["mobile_telephone_prefix"]."-".$SessionProposer["mobile_telephone_number"]; // Proposer mobile phone

         $resSession['post_code']                = $SessionProposer["postcode_prefix"]." ".$SessionProposer["postcode_number"]; // Proposer postcode

         $resSession['email']                    = $SessionProposer["email_address"]; // Proposer email address
         $resSession['start_insurance_date']     = $SessionProposer["date_of_insurance_start"]; // Proposer start insurance date

         $resSession['joint_insured']            = $SessionProposer["joint_insured"]; // Joint insured policy

         // add extra questions for proposer insured
         $resSession['property_bankrupt']        = $SessionProposer["property_bankrupt"];
         $resSession['special_terms_policy']     = $SessionProposer["special_terms_policy"];
         $resSession['policy_declined']          = $SessionProposer["policy_declined"];
         $resSession['occupants_convictions']    = $SessionProposer["occupants_convictions"];


         // build cover
         $resSession['policy_expire']            = $SessionCover["policy_expire"]; // Policy expire
         $resSession['build_cover']              = $SessionCover["build_cover"]; // Require Build Cover
         $resSession['build_no_claims_bonus']    = $SessionCover["build_no_claims_bonus"]; // Build no claims bonus
         $resSession['prev_build_cover']         = $SessionCover["prev_build_cover"]; // Previous Build Cover
         $resSession['build_sums_ins']           = $SessionCover["build_sums_ins"]; // Build sums insurance
         $resSession['build_sums_market']        = $SessionCover["build_sums_market"]; // Build sums market value
         $resSession['build_acc_dam']            = $SessionCover["build_acc_dam"]; // Build Accidental Damage
         $resSession['had_any_build_claims']     = $SessionCover["had_any_build_claims"]; // Any build claims

         // contents cover
         $resSession['contents_cover']           = $SessionCover["contents_cover"]; // Require Contents Cover
         $resSession['prev_contents_cover']      = $SessionCover["prev_contents_cover"]; // Previous Contents Cover
         $resSession['contents_sums_ins']        = $SessionCover["contents_sums_ins"]; // Contents sum insurance
         $resSession['all_risks_unspecified']    = $SessionCover["all_risks_unspecified"]; // All risks unsepcified
         $resSession['contents_no_claims_bonus'] = $SessionCover["contents_no_claims_bonus"]; // Contents no clamims bonus
         $resSession['had_any_contents_claims']  = $SessionCover["had_any_contents_claims"]; // Any contents claims

         // convert date into sql date format
         if(! $resSession['policy_expire']       = $this->StdToMysqlDate($resSession['policy_expire']))
            return false;

         if(! $resSession['start_insurance_date'] = $this->StdToMysqlDate($resSession['start_insurance_date']))
            return false;

         ####################################################
         // check the params values
         ####################################################

         // HOME

         if(! preg_match('/[a-z]{1,10}/i',$resSession['property_type']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_TYPE");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_owned']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_OWNED");

         if(! preg_match('/[0-9]{1,2}/i',$resSession['property_rooms']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_ROOMS");

         if(! preg_match('/[0-9]{1,2}/i',$resSession['property_bedrooms']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_BEDROOMS");

         if(! preg_match('/[0-9]{4,4}/i',$resSession['property_year']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_YEAR");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_business']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_BUSINESS");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_work']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_WORK");

         if(! preg_match('/[a-z]{2,2}/i',$resSession['property_unoccupied']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_UNOCCUPIED");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_watch']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_WATCH");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_prof_alarm']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_PROF_ALARM");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_alert_alarm']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_ALERT_ALARM");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_door_lock']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_DOOR_LOCK");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_window_lock']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_WINDOW_LOCK");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_smoke_alarm']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_SMOKE_ALARM");

         // add extra parameters
         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_permanent_residence']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_PERMANENT_RESIDENCE");

         if(! preg_match('/[0-9]{1,2}/i',$resSession['property_unoccupied_days']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_UNOCCUPIED_DAYS");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['own_lockable_entrance']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_OWN_LOCKABLE_ENTRANCE");

         if(! preg_match('/[a-z]{1,2}/i',$resSession['property_french_lock']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_FRENCH_LOCK");

         // end HOME
         // EXTRA HOME

         // home extra page
         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_state_of_repair']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_STATE_OF_REPAIR");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_listed']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_LISTED");

         if(! preg_match('/[a-z]{2,2}/i',$resSession['property_roof_made']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_ROOF_MADE");

         if(! preg_match('/[0-9]{1,2}/i',$resSession['property_flat_roof']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_FLAT_ROOF");

         if(! preg_match('/[a-z]{2,2}/i',$resSession['property_walls_made']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_WALLS_MADE");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_subsidence']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_SUBSIDENCE");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_landslip_heave']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_LANDSLIP_HEAVE");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_flooding']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_FLOODING");

         if(! preg_match('/[0-9]{1,3}/i',$resSession['property_nearest_river']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_NEAREST_RIVER");

         if(! preg_match('/[0-9]{1,2}/i',$resSession['property_nearest_tree']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_NEAREST_TREE");
         // END EXTRA HOME

         // PROPOSER
         if(! preg_match('/[a-z]{1,16}/i',$resSession['title']))
            $this->strERR .= GetErrorString("INVALID_INSURED_TITLE");

         if(! preg_match('/[a-z0-9\-\s]{1,128}/i',$resSession['house_number_or_name']))
            $this->strERR .= GetErrorString("INVALID_INSURED_HOUSE_NUMBER_OR_NAME");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['marital_status']))
            $this->strERR .= GetErrorString("INVALID_INSURED_MARITAL_STATUS");
         
         if(! preg_match('/[a-z]{1,1}/i',$resSession['ft_employment_status']))
            $this->strERR .= GetErrorString("INVALID_INSURED_FT_EMPLOYMENT_STATUS");

         if(! preg_match('/[a-z0-9]{1,3}/i',$resSession['ft_business']))
            $this->strERR .= GetErrorString("INVALID_INSURED_FT_BUSINESS");

         if(! preg_match('/[a-z0-9]{1,3}/i',$resSession['ft_occupation']))
            $this->strERR .= GetErrorString("INVALID_INSURED_FT_OCCUPATION");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['have_part_time']))
            $this->strERR .= GetErrorString("INVALID_INSURED_PART_TIME");

         if(! preg_match('/[0-9\-]{1,16}/i',$resSession['day_phone']))
            $this->strERR .= GetErrorString("INVALID_INSURED_DAY_PHONE");

         if(! preg_match('/[0-9\-]{1,16}/i',$resSession['evening_phone']))
            $this->strERR .= GetErrorString("INVALID_INSURED_EVENING_PHONE");

         if(! preg_match('/[0-9\-]{1,16}/i',$resSession['mobile_phone']))
            $this->strERR .= GetErrorString("INVALID_INSURED_MOBILE_PHONE");

         if(! preg_match('/[a-z0-9\s]{6,8}/i',$resSession['post_code']))
            $this->strERR .= GetErrorString("INVALID_INSURED_POSTCODE");

         if(! preg_match('/\.*\@.*\.[a-zA-Z]{2,3}/i',$resSession['email']))
            $this->strERR .= GetErrorString("INVALID_INSURED_EMAIL");

         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['start_insurance_date']))
            $this->strERR .= GetErrorString("INVALID_DATE_OF_INSURANCE_START");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['joint_insured']))
            $this->strERR .= GetErrorString("INVALID_JOINT_INSURED_POLICY");

         // add extra proposer

         if(! preg_match('/[a-z]{1,1}/i',$resSession['property_bankrupt']))
            $this->strERR .= GetErrorString("INVALID_PROPERTY_BANKRUPT");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['special_terms_policy']))
            $this->strERR .= GetErrorString("INVALID_SPECIAL_TERMS_POLICY");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['policy_declined']))
            $this->strERR .= GetErrorString("INVALID_POLICY_DECLINED");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['occupants_convictions']))
            $this->strERR .= GetErrorString("INVALID_OCCUPANTS_CONVICTIONS");

         // end PROPOSER

         // BUILD COVER

         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['policy_expire']))
            $this->strERR .= GetErrorString("INVALID_POLICY_EXPIRE");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['build_cover']))
            $this->strERR .= GetErrorString("INVALID_BUILDING_COVER");

         if($resSession['build_cover'] == 'Y')
         {
            if(! preg_match('/[0-9]{1,1}/i',$resSession['build_no_claims_bonus']))
               $this->strERR .= GetErrorString("INVALID_PREVIOUS_BUILDING_NCB");

            if(! preg_match('/[a-z]{1,1}/i',$resSession['prev_build_cover']))
               $this->strERR .= GetErrorString("INVALID_PREVIOUS_BUILDING_COVER");

            if(! preg_match('/[0-9]{1,8}/i',$resSession['build_sums_ins']))
               $this->strERR .= GetErrorString("INVALID_BUILDING_SUMS_INSURANCE");

            if(! preg_match('/[0-9]{1,8}/i',$resSession['build_sums_market']))
               $this->strERR .= GetErrorString("INVALID_BUILDING_SUMS_MARKET");

            if(! preg_match('/[a-z]{1,1}/i',$resSession['build_acc_dam']))
               $this->strERR .= GetErrorString("INVALID_BUILDING_ACCIDENTAL_DAMAGE");

            if(! preg_match('/[a-z]{1,1}/i',$resSession['had_any_build_claims']))
               $this->strERR .= GetErrorString("INVALID_BUILDING_CLAIMS");
         }
         // end BUILD COVER

         // CONTENTS COVER
         if(! preg_match('/[a-z]{1,1}/i',$resSession['contents_cover']))
            $this->strERR .= GetErrorString("INVALID_CONTENTS_COVER");

         if($resSession['contents_cover'] == 'Y')
         {
            if(! preg_match('/[0-9]{1,1}/i',$resSession['contents_no_claims_bonus']))
               $this->strERR .= GetErrorString("INVALID_CONTENTS_NCB");

            if(! preg_match('/[a-z]{1,1}/i',$resSession['prev_contents_cover']))
               $this->strERR .= GetErrorString("INVALID_PREVIOUS_CONTENTS_COVER");

            if(! preg_match('/[0-9]{1,8}/i',$resSession['contents_sums_ins']))
               $this->strERR .= GetErrorString("INVALID_CONTENTS_SUMS_INSURANCE");

            if(! preg_match('/[0-9]{1,5}/i',$resSession['all_risks_unspecified']))
               $this->strERR .= GetErrorString("INVALID_ALL_RISKS_UNSPECIFIED");

            if(! preg_match('/[a-z]{1,1}/i',$resSession['had_any_contents_claims']))
               $this->strERR .= GetErrorString("INVALID_CONTENTS_CLAIMS");
         }

         // end CONTENTS COVER

         break;

      // bike section
      case '5':

         ####################################################
         // get param values from the session
         ####################################################

         $SessionProposerDriver  = $SESSION['_DRIVERS_'][0];
         $SessionVehicle         = $SESSION['_VEHICLE_'];
         $SessionCover           = $SESSION['_COVER_'];
      
         $driversInsured = $SessionProposerDriver["who_drive_the_vehicle"];
      
         if($driversInsured == "IO")
            $driversInsured = 1;

         if($driversInsured == "IS")
            $driversInsured = 2;
      
         $driversInsured = intval($driversInsured);

         // BIKE
         $resSession['bike_abi_code']              = $SessionVehicle['vehicle_confirm']; // Bike abi code
         $resSession['estimated_value']            = $SessionVehicle['estimated_value']; // Bike estimated value;
         $resSession['estimated_accesories_value'] = $SessionVehicle['estimated_accesories_value']; // Bike estimated 
         $resSession['vehicle_sidecar']            = $SessionVehicle['vehicle_sidecar']; // Vehicle sidecar
         $resSession['vehicle_immobiliser']        = $SessionVehicle['vehicle_immobiliser']; // Vehicle immobiliser
         $resSession['vehicle_tracking_device']    = $SessionVehicle['vehicle_tracking_device']; // Vehicle tracking device
         $resSession['vehicle_grey_or_import']     = $SessionVehicle['vehicle_grey_or_import']; // Vehicle gray or import
         $resSession['vehicle_kept']               = $SessionVehicle['vehicle_kept']; // Vehicle kept overnight
         $resSession['vehicle_bought']             = $SessionVehicle['vehicle_bought'];  // Date of vehicle bough
         // END BIKE

         // proposer data
         $resSession['title']                  = $SessionProposerDriver["title"]; // Proposer title
         $resSession['house_number_or_name']   = $SessionProposerDriver["house_number_or_name"]; // house number and name
         $resSession['born_uk']                = $SessionProposerDriver["born_in_uk"]; // Proposer born in  uk
         $resSession['uk_res_date']            = $SessionProposerDriver["date_of_uk_resident"]; // Uk residence Proposer
         $resSession['sex']                    = $SessionProposerDriver["sex"]; // Proposer sex
         $resSession['marital_status']         = $SessionProposerDriver["marital_status"]; // Proposer marital status
         $resSession['type_drv_lic']           = $SessionProposerDriver["type_of_driving"]; // Proposer driver licence
         $resSession['drv_lic_date']           = $SessionProposerDriver["period_of_licence_held"]; // Driver licence date;

         $resSession['ft_employment_status']   = $SessionProposerDriver["employment_status"]; // Full time employment status
         $resSession['ft_business']            = $SessionProposerDriver["business_list"]; // Full time business
         $resSession['ft_occupation']          = $SessionProposerDriver["occupation_list"]; // Full time occupation

         $resSession['have_part_time']         = $SessionProposerDriver["part_time_occupation"]; // Proposer part time

         $resSession['day_phone']              = $SessionProposerDriver["daytime_telephone_prefix"]."-".$SessionProposerDriver["daytime_telephone_number"]; // Proposer day(work) phone
         $resSession['evening_phone']          = $SessionProposerDriver["home_telephone_prefix"]."-".$SessionProposerDriver["home_telephone_number"]; // Proposer evening(home) phone
         $resSession['mobile_phone']           = $SessionProposerDriver["mobile_telephone_prefix"]."-".$SessionProposerDriver["mobile_telephone_number"]; // Proposer mobile phone

         $resSession['post_code']              = $SessionProposerDriver["postcode_prefix"]." ".$SessionProposerDriver["postcode_number"]; // Proposer postcode

         $resSession['home_owner']           = $SessionProposerDriver["home_owner"]; // Proposer home owner
         $resSession['email']                = $SessionProposerDriver["email_address"]; // Proposer email address
         $resSession['start_insurance_date'] = $SessionProposerDriver["date_of_insurance_start"]; // Proposer start insurance date

         $resSession['claims']               = $SessionProposerDriver["had_any_accidents"]; // Have proposer claims
         $resSession['convictions']          = $SessionProposerDriver["had_any_convictions"]; // Have proposer claims
         
         // cover
         $resSession['policy_expire']        = $SessionCover["policy_expire"]; // Policy expire
         $resSession['type_of_cover']        = $SessionCover['type_of_cover']; // Type of cover
         $resSession['type_of_use']          = $SessionCover['type_of_use']; //Type of use

         // other - like additional drivers, claims, convictions 
         $resSession['drivers_insured']      = $driversInsured; // how many drivers insured
         
         // convert date into sql date format

         if(! $resSession['vehicle_bought'] = $this->StdToMysqlDate($resSession['vehicle_bought']))
            return false;

         if(! $resSession['policy_expire'] = $this->StdToMysqlDate($resSession['policy_expire']))
            return false;

         if(! $resSession['uk_res_date'] = $this->StdToMysqlDate($resSession['uk_res_date']))
            return false;

         if(! $resSession['drv_lic_date'] = $this->StdToMysqlDate($resSession['drv_lic_date']))
            return false;

         if(! $resSession['start_insurance_date'] = $this->StdToMysqlDate($resSession['start_insurance_date']))
            return false;


         ####################################################         
         // check the params values
         ####################################################

         if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,16}/i',$resSession['bike_abi_code']))
            $this->strERR .= GetErrorString("INVALID_BIKE_ABI_CODE_FIELD");
      
         if(! preg_match('/\d+/i',$resSession['estimated_value']))
            $this->strERR .= GetErrorString("INVALID_ESTIMATED_VALUE_FIELD");
      
         if(! preg_match('/\d+/i',$resSession['estimated_accesories_value']))
            $this->strERR .= GetErrorString("INVALID_ESTIMATED_ACCESORIES_VALUE_FIELD");

         if(! preg_match('/(Y)|(N)/i',$resSession['vehicle_sidecar']))
            $this->strERR .= GetErrorString("INVALID_VEHICLE_SIDECAR_VALUE_FIELD");
         
         if( empty($resSession['vehicle_immobiliser']))
            $this->strERR .= GetErrorString("INVALID_VEHICLE_IMMOBILISER_VALUE_FIELD");

         if( empty($resSession['vehicle_tracking_device']))
            $this->strERR .= GetErrorString("INVALID_VEHICLE_TRACKING_DEVICE_VALUE_FIELD");

         if( empty($resSession['vehicle_grey_or_import']))
            $this->strERR .= GetErrorString("INVALID_VEHICLE_GREY_OR_IMPORT_VALUE_FIELD");

         if( empty($resSession['vehicle_grey_or_import']))
            $this->strERR .= GetErrorString("INVALID_VEHICLE_GREY_OR_IMPORT_VALUE_FIELD");

         if( empty($resSession['vehicle_kept']))
            $this->strERR .= GetErrorString("INVALID_VEHICLE_KEPT_VALUE_FIELD");

         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['vehicle_bought']))
            $this->strERR .= GetErrorString("INVALID_VEHICLE_BOUGHT_DATE_FIELD");

         if( empty($resSession['title']))
            $this->strERR .= GetErrorString("INVALID_TITLE_FIELD");

         $resSession['house_number_or_name'] = str_replace("'","\'",$resSession['house_number_or_name']);
      
         if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$resSession['house_number_or_name']))
            $this->strERR .= GetErrorString("INVALID_ADDRESS_NAME_FIELD");

         if(! preg_match('/[A-Z]{1}/',$resSession['born_uk']))
            $this->strERR .= GetErrorString("INVALID_BORN_UK_FIELD");

         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['uk_res_date']))
            $this->strERR .= GetErrorString("INVALID_UK_RESIDENCE_DATE_FIELD");
      
         if( empty($resSession['sex']))
            $this->strERR .= GetErrorString("INVALID_SEX_FIELD");

         if(! preg_match('/[A-Z]{1}/',$resSession['marital_status']))
            $this->strERR .= GetErrorString("INVALID_MARITAL_STATUS_FIELD");

         if( empty($resSession['type_drv_lic']))
            $this->strERR .= GetErrorString("INVALID_TYPE_OF_DRIVING_FIELD");

         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['drv_lic_date']))
            $this->strERR .= GetErrorString("INVALID_DRIVE_LICENCE_DATE_FIELD");

         if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$resSession['ft_employment_status']))
            $this->strERR .= GetErrorString("INVALID_FULL_TIME_EMPLOYMENT_STATUS_FIELD");

         if(! preg_match('/[a-z0-9]{1,3}/i',$resSession['ft_business']))
            $this->strERR .= GetErrorString("INVALID_INSURED_FT_BUSINESS");

         if(! preg_match('/[a-z0-9]{1,3}/i',$resSession['ft_occupation']))
            $this->strERR .= GetErrorString("INVALID_INSURED_FT_OCCUPATION");

         if(! preg_match('/[a-z]{1,1}/i',$resSession['have_part_time']))
            $this->strERR .= GetErrorString("INVALID_INSURED_PART_TIME");

         if(! preg_match('/[0-9\-\s\_]{1,32}/i',$resSession['day_phone']))
            $this->strERR .= GetErrorString("INVALID_DAY_PHONE_FIELD");
      
         if(! preg_match('/[0-9\-\s\_]{1,32}/i',$resSession['evening_phone']))
            $this->strERR .= GetErrorString("INVALID_EVENING_PHONE_FIELD");
      
         if(! preg_match('/[0-9\-\s\_]{1,32}/i',$resSession['mobile_phone']))
            $this->strERR .= GetErrorString("INVALID_MOBILE_PHONE_FIELD");

         if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,16}/i',$resSession['post_code']))
            $this->strERR .= GetErrorString("INVALID_POST_CODE_FIELD");

         if(! preg_match('/[A-Z]{1}/',$resSession['home_owner']))
            $this->strERR .= GetErrorString("INVALID_HOME_OWNER_FIELD");

         if(! preg_match('/\.*\@.*\.[a-zA-Z]{2,3}/i',$resSession['email']))
            $this->strERR .= GetErrorString("INVALID_EMAIL_FIELD");

         if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$resSession['start_insurance_date']))
            $this->strERR .= GetErrorString("INVALID_START_INSURANCE_FIELD");

         if(! preg_match('/[A-Z]{1}/',$resSession['claims']))
            $this->strERR .= GetErrorString("INVALID_CLAIMS_FIELD");
      
         if(! preg_match('/[A-Z]{1}/',$resSession['convictions']))
            $this->strERR .= GetErrorString("INVALID_CONVICTIONS_FIELD");

         if(! preg_match('/\d+/i',$resSession['type_of_cover']))
            $this->strERR = GetErrorString("INVALID_TYPE_OF_COVER_FIELD");

         if(! preg_match('/\d+/i',$resSession['type_of_use']))
            $this->strERR = GetErrorString("INVALID_TYPE_OF_USE_FIELD");

         break;

      default:
         $this->strERR  .= 'INVALID_QUOTE_USER_DETAILS_QUOTE_TYPE_ID';
         break;

   }


   if(! empty($this->strERR))
      return false;

   switch($type)
   {
      case 'add':

         $sqlParamCMDKeys   = ' (';
         $sqlParamCMDValues = ' VALUES (';

         foreach($resSession as $sqlParamName => $sqlParamValue)
         {
            $sqlParamCMDKeys   .= $sqlParamName.",";
            $sqlParamCMDValues .= "'".$sqlParamValue."',";
         }

         $sqlParamCMDKeys   = preg_replace("/,$/s","",$sqlParamCMDKeys);
         $sqlParamCMDValues = preg_replace("/,$/s","",$sqlParamCMDValues);

         $sqlParamCMDKeys   .= ')';
         $sqlParamCMDValues .= ')';

         break;

      default :
         $this->strERR = 'INVALID_QUOTE_USER_DETAILS_ACTION_TYPE';
         return false;

   }

   $sqlCMD .= $sqlParamCMDKeys.$sqlParamCMDValues;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   if(! preg_match("/\d{1,2}\/\d{1,2}\/\d{4}/i",$date))
      $this->strERR = GetErrorString("INVALID_QUOTE_USER_DETAILS_STD_TO_MYSQL_DATE");

   if(! empty($this->strERR))
      return false;
   
   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteUserDetails
//
// [DESCRIPTION]:   Add new entry to the quote_user_details table
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  userID | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteUserDetails($quoteUserID='', $qzQuoteTypeID='', $SESSION='')
{
   if(! $this->_assertQuoteUserDetails($quoteUserID, $qzQuoteTypeID, $SESSION, 'add',$lastSqlCmd))
      return false;

   if(! $this->dbh->Exec($lastSqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $lastSqlCmd = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteUserDetailsByID
//
// [DESCRIPTION]:   Read data of a user from quote_user_details table and put it into an array variable
//
// [PARAMETERS]:    $userDetailsID
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
// Send TopQuotesEmail
function GetQuoteUserDetailsByID($userDetailsID='')
{
   if(! preg_match("/^\d+$/", $userDetailsID))
   {
      $this->strERR = GetErrorString("INVALID_USER_DETAILS_ID_FIELD");
      return false;
   }

   $quoteUserDetailsFields = $this->dbh->GetAllFieldsName(SQL_QUOTE_USER_DETAILS);

   $lastSqlCmd = "SELECT * FROM ".SQL_QUOTE_USER_DETAILS." WHERE id='$userDetailsID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_DETAILS_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   foreach($quoteUserDetailsFields as $index => $fieldName)
      $arrayResult[$fieldName] = $this->dbh->GetFieldValue($fieldName);
   
   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllUsersDetailsByLogID
//
// [DESCRIPTION]:   Read data from quote_user_details table and put it into an array variable
//                  key = userID, value = Array(that contain all the record for that user)
//
// [PARAMETERS]:     $logID
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllUsersDetailsByLogID($logID = '')
{
   if(! preg_match("/\d+/",$logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_USER_DETAIK_LOG_ID_FIELD");
      return false;
   }

   $quoteUserDetailsFields = $this->dbh->GetAllFieldsName(SQL_QUOTE_USER_DETAILS);
   $quoteUsersFields       = $this->dbh->GetAllFieldsName(SQL_QUOTE_USERS);


   $lastSqlCmd = "SELECT ".SQL_QUOTE_USERS.".*,".SQL_QUOTE_USER_DETAILS.".*  FROM ".SQL_QUOTE_USERS.",".SQL_QUOTE_USER_DETAILS.",".SQL_QZQUOTES.",".SQL_LOGS." WHERE ".SQL_LOGS.".id='$logID' AND ".SQL_QZQUOTES.".log_id=".SQL_LOGS.".id AND ".SQL_QZQUOTES.".quote_user_details_id=".SQL_QUOTE_USER_DETAILS.".id AND ".SQL_QUOTE_USER_DETAILS.".quote_user_id=".SQL_QUOTE_USERS.".id";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("USER_DETAILS_LOG_ID_NOT_FOUND");
      return false;
   }

   $arrayResult=array();

   foreach($quoteUserDetailsFields as $index => $fieldName)
      $arrayResult[$fieldName] = $this->dbh->GetFieldValue($fieldName);

   foreach($quoteUsersFields as $index => $fieldName)
      $arrayResult[$fieldName] = $this->dbh->GetFieldValue($fieldName);

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}
}//end class CQuoteUserDetails

?>

<?php

/*****************************************************************************/
/*                                                                           */
/*  CQuoteDetails class interface
/*                                                                           */
/*  (C) 2005 Istvancsek Gabi(gabi@acrux.biz)                                     */
/*                                                                           */
/*****************************************************************************/

define("QUOTES_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);


//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuoteDetails
//
// [DESCRIPTION]:  CQuoteDetails class interface
//
// [FUNCTIONS]:    true  | false AssertQuotesDetails($quoteID='', &$insurer, &$annualPremium, &$monthlyPremium, $voluntaryExcess='')
//                 int   | false AddQuoteDetails($quoteID='', $insurer='', $annualPremium=0, $monthlyPremium=0, $voluntaryExcess=0)
//                 true  | false UpdateQuoteDetails($quoteDetailsID='', $quoteID='', $insurer='', $annualPremium=0, $monthlyPremium=0, $voluntaryExcess=0)
//                 true  | false DeleteQuoteDetails($quoteDetailsID='')
//                 array | false GetQuoteDetailsByID($quoteDetailsID='')
//                 array | false GetQuoteDetailsByQuoteID($quoteID=0)
//                 array | false GetCheapestTopFiveSitesQuoteDetails($logID='', $limit=5)
//                 array | false GetExpensiveTopFiveSitesQuoteDetails($logID='')
//                 array | false GetCheapestDetailsTopFiveSitesQuoteDetails($logID='')
//                 array | false GetExpensiveDetailsTopFiveSitesQuoteDetails($logID='')
//                 array | false GetCheapestSiteQuoteDetails($logID='',$siteID='')
//                 array | false GetExpensiveSiteQuoteDetails($logID='', $siteID='')
//                 array | false GetQuoteDetailsByQuoteStatusID($quoteStatusID='')
//
//                 Close()
//                 GetError()
//                 ShowError()
//
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CQuoteDetails
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last Quote error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuoteDetails
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQuoteDetails($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuotesDetails
//
// [DESCRIPTION]:   Verify to see if the requested fields are completed
//
// [PARAMETERS]:    $quoteID, &$insurer, &$annualPremium, &$monthlyPremium, $voluntaryExcess
//
// [RETURN VALUE]:  true or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertQuotesDetails($quoteID='', &$insurer, &$annualPremium, &$monthlyPremium, &$voluntaryExcess, &$voluntaryExcess2nd)
{
   $this->strERR = "";

   $insurer = str_replace("'", "", trim($insurer));

   if(! preg_match("/^\d+$/", $quoteID))
     $this->strERR = GetErrorString("INVALID_QUOTE_DETAILS_QUOTE_ID_FIELD");

   if(empty($insurer))
      $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_DETAILS_INSURER_FIELD");

   $annualPremium = str_replace(",", "", trim($annualPremium));

   if(! preg_match("/([0-9\.]+)/", $annualPremium, $matchesAnnualPremium))
      $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_DETAILS_ANNUAL_PREMIUM_FIELD");

   $annualPremium = $matchesAnnualPremium[1];

   $monthlyPremium = str_replace(",", "", trim($monthlyPremium));

   if(! empty($monthlyPremium))
   {
      if(! preg_match("/([0-9\.]+)/", $monthlyPremium, $matchesMonthlyPremium))
         $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_DETAILS_MONTHLY_PREMIUM_FIELD");

      $monthlyPremium = $matchesMonthlyPremium[1];
   }

   $voluntaryExcess    = str_replace(",", "", trim($voluntaryExcess));
   $voluntaryExcess2nd = str_replace(",", "", trim($voluntaryExcess2nd));

   if(! preg_match("/([0-9\.\-]+)/", $voluntaryExcess, $matchesVoluntaryExcess))
      $voluntaryExcess = 0;

   $voluntaryExcess = $matchesVoluntaryExcess[1];

   if(! preg_match("/([0-9\.\-]+)/", $voluntaryExcess2nd, $matchesVoluntaryExcess2nd))
      $voluntaryExcess2nd = 0;

   $voluntaryExcess2nd = $matchesVoluntaryExcess2nd[1];

   // prepare float values to compare with values from db
   $annualPremium      = number_format($annualPremium, 2, '.', '');
   $monthlyPremium     = number_format($monthlyPremium, 2, '.', '');
   $voluntaryExcess    = number_format($voluntaryExcess, 2, '.', '');
   $voluntaryExcess2nd = number_format($voluntaryExcess2nd, 2, '.', '');

   if(! preg_match("/[0-9\.\-]+/", $voluntaryExcess))
      $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_DETAILS_VOLUNTARY_EXCESS_FIELD");

   if(! preg_match("/[0-9\.\-]+/", $voluntaryExcess2nd))
      $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_DETAILS_VOLUNTARY_EXCESS_SEC_FIELD");

   if($annualPremium <= 0)
      $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_DETAILS_ANNUAL_PREMIUM_FIELD_IS_NULL");

   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteDetails
//
// [DESCRIPTION]:   Add new entry to the quote_details table
//
// [PARAMETERS]:    $quoteID, $insurer, $annualPremium, $monthlyPremium, $voluntaryExcess
//
// [RETURN VALUE]:  quoteDetailsID or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteDetails($quoteID='', $insurer='', $annualPremium=0, $monthlyPremium=0, $voluntaryExcess=0, $voluntaryExcessSec=0)
{

   if(! $this->AssertQuotesDetails($quoteID, $insurer, $annualPremium, $monthlyPremium, $voluntaryExcess, $voluntaryExcessSec))
      return false;

   // we have to check if we have unique set of values by quoteID
   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTE_DETAILS." WHERE quote_id='$quoteID' AND insurer='$insurer' AND annual_premium LIKE '$annualPremium' AND monthly_premium LIKE '$monthlyPremium' AND voluntary_excess LIKE '$voluntaryExcess' AND voluntary_excess_sec='$voluntaryExcessSec'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
      return false;
   }

   // if exist return quote_details_id
   if($this->dbh->GetRows())
   {
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      return $this->dbh->GetFieldValue("id");
   }



   $this->lastSQLCMD = "INSERT INTO ".SQL_QUOTE_DETAILS." (quote_id,insurer,annual_premium,monthly_premium,voluntary_excess, voluntary_excess_sec) VALUES('$quoteID', '$insurer', '$annualPremium', '$monthlyPremium', '$voluntaryExcess', '$voluntaryExcessSec')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQuoteDetails
//
// [DESCRIPTION]:   Update quote_details table
//
// [PARAMETERS]:    $quoteDetailsID, $quoteID, $insurer, $annualPremium, $monthlyPremium, $voluntaryExcess
//
// [RETURN VALUE]:  true or false in case o failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQuoteDetails($quoteDetailsID='', $quoteID='', $insurer='', $annualPremium=0, $monthlyPremium=0, $voluntaryExcess=0)
{
   if(! preg_match("/^\d+$/", $quoteDetailsID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_DETAILS_ID_FIELD");
      return false;
   }

   if(! $this->AssertQuotesDetails($quoteID, $insurer, $annualPremium, $monthlyPremium, $voluntaryExcess))
      return false;

   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTE_DETAILS." WHERE id='$quoteDetailsID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_DETAILS_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_QUOTE_DETAILS." SET quote_id='$quoteID', insurer='$insurer', annual_premium='$annualPremium', monthly_premium='$monthlyPremium', voluntary_excess ='$voluntaryExcess' WHERE id='$quoteDetailsID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQuoteDetails
//
// [DESCRIPTION]:   Delete an entry from quote_details table
//
// [PARAMETERS]:    $quoteDetailsID
//
// [RETURN VALUE]:  true or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteQuoteDetails($quoteDetailsID='')
{
   if(! preg_match("/^\d+$/", $quoteDetailsID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_DETAILS_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTE_DETAILS." WHERE id='$quoteDetailsID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_DETAILS_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_QUOTE_DETAILS." WHERE id='$quoteDetailsID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteDetailsByID
//
// [DESCRIPTION]:   Read data from quote_details table and put it into an array variable
//
// [PARAMETERS]:    $quoteDetailsID
//
// [RETURN VALUE]:  Array(withe the parameters of the $quoteDetailsID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteDetailsByID($quoteDetailsID='')
{
   if(! preg_match("/^\d+$/", $quoteDetailsID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_DETAILS_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_DETAILS." WHERE id='$quoteDetailsID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_DETAILS_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]                   = $this->dbh->GetFieldValue("id");
   $arrayResult["quote_id"]             = $this->dbh->GetFieldValue("quote_id");
   $arrayResult["insurer"]              = $this->dbh->GetFieldValue("insurer");
   $arrayResult["annual_premium"]       = $this->dbh->GetFieldValue("annual_premium");
   $arrayResult["monthly_premium"]      = $this->dbh->GetFieldValue("monthly_premium");
   $arrayResult["voluntary_excess"]     = $this->dbh->GetFieldValue("voluntary_excess");
   $arrayResult["voluntary_excess_sec"] = $this->dbh->GetFieldValue("voluntary_excess_sec");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteDetailsByQuoteID
//
// [DESCRIPTION]:   Read data from quote_details table and put it into an array variable
//
// [PARAMETERS]:    $quoteID
//
// [RETURN VALUE]:  Array(with the parameters of the $quoteID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteDetailsByQuoteID($quoteID=0)
{
   if(! preg_match("/^\d+$/", $quoteID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_DETAILS_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_DETAILS." WHERE quote_id='$quoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_DETAILS_ID_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index]["id"]                   = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["quote_id"]             = $this->dbh->GetFieldValue("quote_id");
      $arrayResult[$index]["insurer"]              = $this->dbh->GetFieldValue("insurer");
      $arrayResult[$index]["annual_premium"]       = $this->dbh->GetFieldValue("annual_premium");
      $arrayResult[$index]["monthly_premium"]      = $this->dbh->GetFieldValue("monthly_premium");
      $arrayResult[$index]["voluntary_excess"]     = $this->dbh->GetFieldValue("voluntary_excess");
      $arrayResult[$index]["voluntary_excess_sec"] = $this->dbh->GetFieldValue("voluntary_excess_sec");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCheapestTopFiveSitesQuoteDetails
//
// [DESCRIPTION]:   Read data from quote_details table and put it into an array variable
//
// [PARAMETERS]:    $logID='', $limit=5
//
// [RETURN VALUE]:  Array(withe the parameters of the $logID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCheapestTopFiveSitesQuoteDetails($logID='', $limit=5)
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_DETAILS_LOG_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $limit))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_DETAILS_LIMIT_FIELD");
      return false;
   }

   $this->lastSQLCMD = "select MIN(annual_premium) as cheapest_quote,qs.site_id from ".SQL_QUOTE_STATUS." qs,".SQL_QUOTES." q,".SQL_QUOTE_DETAILS." qd WHERE qs.log_id='$logID' AND qs.id = q.quote_status_id AND q.id = qd.quote_id GROUP BY (qs.site_id) ORDER BY cheapest_quote, qs.site_id ASC limit $limit";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_DETAILS_TOP_FIVE_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   $index = 0;

   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index] = $this->dbh->GetFieldValue("site_id");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetExpensiveTopFiveSitesQuoteDetails
//
// [DESCRIPTION]:   Read data from quote_details table and put it into an array variable
//
// [PARAMETERS]:    $logID
//
// [RETURN VALUE]:  Array(with the parameters of the $logID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetExpensiveTopFiveSitesQuoteDetails($logID='', $limit=5)
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_DETAILS_LOG_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "select MAX(annual_premium) as expensive_quote,qs.site_id from ".SQL_QUOTE_STATUS." qs,".SQL_QUOTES." q,".SQL_QUOTE_DETAILS." qd WHERE qs.log_id='$logID' AND qs.id = q.quote_status_id AND q.id = qd.quote_id GROUP BY (qs.site_id) ORDER BY expensive_quote DESC limit $limit";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_DETAILS_TOP_FIVE_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   $index = 0;

   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index] = $this->dbh->GetFieldValue("site_id");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCheapestDetailsTopFiveSitesQuoteDetails
//
// [DESCRIPTION]:   Read data from quote_details table and put it into an array variable
//
// [PARAMETERS]:    $logID
//
// [RETURN VALUE]:  Array(with the parameters of the $logID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCheapestDetailsTopFiveSitesQuoteDetails($logID='', $limit=5)
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_DETAILS_LOG_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "select MIN(annual_premium) as expensive_quote,qs.site_id from ".SQL_QUOTE_STATUS." qs,".SQL_QUOTES." q,".SQL_QUOTE_DETAILS." qd WHERE qs.log_id='$logID' AND qs.id = q.quote_status_id AND q.id = qd.quote_id GROUP BY (qd.id)  ORDER BY expensive_quote limit $limit";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_DETAILS_TOP_FIVE_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   $index = 0;

   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index] = $this->dbh->GetFieldValue("site_id");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetExpensiveDetailsTopFiveSitesQuoteDetails
//
// [DESCRIPTION]:   Read data from quote_details table and put it into an array variable
//
// [PARAMETERS]:    $logID
//
// [RETURN VALUE]:  Array(withe the parameters of the $logID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetExpensiveDetailsTopFiveSitesQuoteDetails($logID='', $limit=5)
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_DETAILS_LOG_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "select MAX(annual_premium) as expensive_quote,qs.site_id from ".SQL_QUOTE_STATUS." qs,".SQL_QUOTES." q,".SQL_QUOTE_DETAILS." qd WHERE qs.log_id='$logID' AND qs.id = q.quote_status_id AND q.id = qd.quote_id GROUP BY (qd.id)  ORDER BY expensive_quote DESC limit $limit";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_DETAILS_TOP_FIVE_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   $index = 0;

   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index] = $this->dbh->GetFieldValue("site_id");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCheapestSiteQuoteDetails
//
// [DESCRIPTION]:   Read data from quote_details table and put it into an array variable
//
// [PARAMETERS]:    $logID,$siteID
//
// [RETURN VALUE]:  Array or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCheapestSiteQuoteDetails($logID='',$siteID='')
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_DETAILS_LOG_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT MIN(annual_premium) as cheapest_quote,qs.site_id,q.quote_ref,q.password FROM ".SQL_QUOTE_STATUS." qs,".SQL_QUOTES." q,".SQL_QUOTE_DETAILS." qd WHERE qs.log_id='$logID' AND qs.id = q.quote_status_id AND q.id = qd.quote_id";

   if(preg_match("/^\d+$/", $siteID))
   {
      $this->lastSQLCMD .= " AND qs.site_id='$siteID' ";
   }

   $this->lastSQLCMD .= " GROUP BY (qs.site_id) ORDER BY cheapest_quote limit 1";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTE_DETAILS_CHEAPEST_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   $arrayResult["cheapest_quote"]   = $this->dbh->GetFieldValue("cheapest_quote");
   $arrayResult["site_id"]          = $this->dbh->GetFieldValue("site_id");
   $arrayResult["quote_ref"]        = $this->dbh->GetFieldValue("quote_ref");
   $arrayResult["password"]         = $this->dbh->GetFieldValue("password");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetExpensiveSiteQuoteDetails
//
// [DESCRIPTION]:   Read data from quote_details table and put it into an array variable
//
// [PARAMETERS]:    $logID, $siteID
//
// [RETURN VALUE]:  Array or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetExpensiveSiteQuoteDetails($logID='', $siteID='')
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_DETAILS_LOG_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT MAX(annual_premium) as expensive_quote,qs.site_id,q.quote_ref,q.password FROM ".SQL_QUOTE_STATUS." qs,".SQL_QUOTES." q,".SQL_QUOTE_DETAILS." qd WHERE qs.log_id='$logID' AND qs.id = q.quote_status_id AND q.id = qd.quote_id";

   if(preg_match("/^\d+$/", $siteID))
   {
      $this->lastSQLCMD .= " AND qs.site_id='$siteID' ";
   }

   $this->lastSQLCMD .= " GROUP BY (qs.site_id) ORDER BY expensive_quote DESC limit 1";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTE_DETAILS_EXPENSIVE_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   $arrayResult["site_id"]          = $this->dbh->GetFieldValue("site_id");
   $arrayResult["expensive_quote"]  = $this->dbh->GetFieldValue("expensive_quote");
   $arrayResult["quote_ref"]        = $this->dbh->GetFieldValue("quote_ref");
   $arrayResult["password"]         = $this->dbh->GetFieldValue("password");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteDetailsByQuoteStatusID
//
// [DESCRIPTION]:   Read data from quote_details table and put it into an array variable
//
// [PARAMETERS]:    $quoteStatusID
//
// [RETURN VALUE]:  Array(withe the parameters of the $quoteStatusID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteDetailsByQuoteStatusID($quoteStatusID='')
{

   if(! preg_match("/^\d+$/", $quoteStatusID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT q.quote_ref,q.password,qd.* FROM ".SQL_QUOTES." q,".SQL_QUOTE_DETAILS." qd WHERE q.quote_status_id ='$quoteStatusID' AND q.id = qd.quote_id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_DETAILS_QUOTE_STATUS_ID_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index]["password"]             = $this->dbh->GetFieldValue("password");
      $arrayResult[$index]["quote_ref"]            = $this->dbh->GetFieldValue("quote_ref");
      $arrayResult[$index]["insurer"]              = $this->dbh->GetFieldValue("insurer");
      $arrayResult[$index]["annual_premium"]       = $this->dbh->GetFieldValue("annual_premium");
      $arrayResult[$index]["monthly_premium"]      = $this->dbh->GetFieldValue("monthly_premium");
      $arrayResult[$index]["voluntary_excess"]     = $this->dbh->GetFieldValue("voluntary_excess");
      $arrayResult[$index]["voluntary_excess_sec"] = $this->dbh->GetFieldValue("voluntary_excess_sec");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Print the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCheapestTopQuotesDetails
//
// [DESCRIPTION]:   Read data from quote_details table and put it into an array variable
//
// [PARAMETERS]:    $logID='', $limit=5
//
// [RETURN VALUE]:  Array(withe the parameters of the $logID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCheapestTopQuotesDetails($logID='', $limit=5)
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_DETAILS_LOG_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $limit))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_DETAILS_LIMIT_FIELD");
      return false;
   }

   $this->lastSQLCMD = "select MIN(annual_premium) as cheapest_quote,quote_status.site_id, quotes.quote_ref, quote_details.insurer from quote_status,quotes,quote_details WHERE quote_status.log_id='$logID' AND quote_status.id = quotes.quote_status_id AND quotes.id = quote_details.quote_id GROUP BY (quote_status.site_id) ORDER BY cheapest_quote , quote_status.site_id ASC limit $limit";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_DETAILS_TOP_FIVE_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   $index = 0;

   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index]["site_id"]           = $this->dbh->GetFieldValue("site_id");
      $arrayResult[$index]["cheapest_quote"]    = $this->dbh->GetFieldValue("cheapest_quote");
      $arrayResult[$index]["insurer"]           = $this->dbh->GetFieldValue("insurer");
      $arrayResult[$index]["company reference"] = $this->dbh->GetFieldValue("quote_ref");
   }

   return $arrayResult;
}

} // end of CQuote class
?>

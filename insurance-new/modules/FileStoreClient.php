<?php

/*****************************************************************************/
/*                                                                           */
/*  CFileStoreClient class interface                                         */
/*                                                                           */
/*  (C) 2007 Istvancsek Gabriel (gabi@acrux.biz)                             */
/*                                                                           */
/*****************************************************************************/
error_reporting(1);
include_once "File.php";
include_once "MySQL.php";

/*
used with debug_backtrace();

function parse_backtrace($raw){

        $output="";
       
        foreach($raw as $entry){
                $output.="File: ".$entry['file']." (Line: ".$entry['line'].") | ";
                $output.="Function: ".$entry['function']." | ";
                $output.="Args: ".implode(", ", $entry['args'])." # ";
        }

        return $output;
    } 
*/

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CFileStoreClient
//
// [DESCRIPTION]:  CFileStoreClient class interface, client file storage system
//                 PHP version
//
// [FUNCTIONS]:    void             SetFileType($fileType)
//                 void             SetSystemType($systemType)
//                 false | true     AssertFile($fileName="", $crTime="")
//                 false | integer  PutFile($fileName="", $compressType='bzip2', $compressLevel=9)
//                 false | string   GetFile($fileName="", $compressType="bzip2")
//                 false | integer  GetFileTime($fileName="")
//                 false | true     SaveCacheFile($fileName="", $crTime="", $fileContent="")
//                 false | true     SaveQueueFile($fileName="", $crTime="", $fileContent="")
//                 false | true     DeleteQueueFile($fileName="")
//                 false | true     DeleteCacheFile($fileName="")
//                 false | array    GetQueueFiles($filterFileType="")
//                 void             PutQueueFiles($filterFileType="")
//                 false | string   CompressData($data="", $compressType="bzip2", $compressLevel="9")
//                 false | string   DecompressData($data="", $compressType="bzip2")
//                 false | array    GetHeaderDetails($header="")
//                 false | true     CreateDirectory($path)
//                 string           GetError()
//                 void             ShowError()
//                 false | array    GetAllFilesByFilesIn($filesName)
//                 void            CheckCachedFiles($unixTime=0)
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz)
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CFileStoreClient
{
   var $systemType;           // system type : car, van, home ...
   var $fileType;             // file type : in, out, data ...
   var $compressionTypes;     // compression types : bzip2, gzip
 
   var $pathFile;             // path file
   var $queuePathFile;        // queue path file

   var $file;                 // file handle
   var $fsConfig;             // file store server
   var $clients;              // clients 

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CFileStoreClient
   //
   // [DESCRIPTION]:   Default class constructor. Initialization goes here.
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CFileStoreClient($systemType="car",$fileType="in")
   {
      $this->systemType    = $systemType;
      $this->fileType      = $fileType;

      $this->pathFile      = FS_ROOT_PATH;
      $this->queuePathFile = FS_ROOT_PATH.$systemType."/queue/";

      $this->file     = new CFile();

      $this->CreateDirectory($this->queuePathFile.$fileType);
      
      $this->clients['10.2.12.14'] = '10.2.12.19';
      $this->clients['10.2.12.17'] = '10.2.12.19';
      $this->clients['10.2.12.18'] = '10.2.12.19';
      $this->clients['10.2.12.22'] = '10.2.12.19';
      $this->clients['10.2.12.72'] = '10.2.12.19';

      $this->clients['192.168.12.14'] = '10.2.12.19';
      $this->clients['192.168.12.17'] = '10.2.12.19';
      $this->clients['192.168.12.18'] = '10.2.12.19';
      $this->clients['192.168.12.22'] = '10.2.12.19';
      $this->clients['192.168.12.72'] = '10.2.12.19';

      $this->clients['192.168.3.14'] = '10.2.12.19';
      $this->clients['192.168.3.17'] = '10.2.12.19';
      $this->clients['192.168.3.18'] = '10.2.12.19';
      $this->clients['192.168.3.22'] = '10.2.12.19';
      $this->clients['192.168.3.72'] = '10.2.12.19';

      if(! $this->fsConfig['host'] = $this->clients[$_SERVER['SERVER_ADDR']])
         $this->fsConfig['host'] = '10.2.12.19';

      $this->fsConfig['url']                = '/index.php';
      $this->fsConfig['port']               = '80';
      $this->fsConfig['timeout']            = '3';
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: SetFileType
   //
   // [DESCRIPTION]:   set file type
   //
   // [PARAMETERS]:    $fileType
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SetFileType($fileType)
   {
      $this->fileType = $fileType;

      $this->CreateDirectory($this->queuePathFile.$fileType);
   }
   
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: SetSystemType
   //
   // [DESCRIPTION]:   set system type
   //
   // [PARAMETERS]:    $systemType
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SetSystemType($systemType)
   {
      $this->systemType = $systemType;

      $this->queuePathFile = FS_ROOT_PATH.$systemType."/queue/";

      $this->CreateDirectory($this->queuePathFile);
   }
   
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AssertFile
   //
   // [DESCRIPTION]:   assert parameters 
   //
   // [PARAMETERS]:    $fileName, $fileContent
   //
   // [RETURN VALUE]:  false | true
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AssertFile($fileName="", $crTime="")
   {
      if(empty($fileName))
      {
         $this->strERR = "INVALID_FILE_NAME";
         return false;
      }

      if(! empty($crTime))
      {
         if(! preg_match("/^\d+$/", $crTime))
         {
            $this->strERR = "INVALID_FILE_CURRENT_TIME";
            return false;
         }
      }

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PutFile
   //
   // [DESCRIPTION]:   send compressed file to file store server
   //
   // [PARAMETERS]:    $fileName, $fileContent
   //
   // [RETURN VALUE]:  false | true
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PutFile($fileName="", $compressType='bzip2', $compressLevel=9)
   {
      if(! $this->AssertFile($fileName))
         return false;

      if(! $this->file->Open($this->pathFile.$this->fileType."/".$fileName))
      {
         $this->strERR = $this->file->strERR;
         return false;
      }

      $data = $this->file->Read($this->file->GetSize());

      $this->file->Close();

      if(! $resFileStat = $this->file->Stat($this->pathFile.$this->fileType."/".$fileName))
      {
         $this->strERR = $this->file->strERR;
         return false;
      }

      $tries = 0;
      while(! $fp = fsockopen($this->fsConfig['host'], $this->fsConfig['port'], $errno, $errstr, $this->fsConfig['timeout']))
      {
         $tries++;
         usleep(200000);

         if($tries == 5)
            break;
      }

      if(! $fp)
      {
         $this->SaveQueueFile($fileName, $resFileStat['mtime'], $data);

         return false;
      }

      if(! $compressedData = $this->CompressData($data, $compressType, $compressLevel))
         return false;

      $data = rawurlencode($compressedData);

      $vars  = "file_name=$fileName&";
      $vars .= "action=send&";
      $vars .= "cr_time=".$resFileStat['mtime']."&";
      $vars .= "file_type=".$this->fileType."&";
      $vars .= "system_type=".$this->systemType."&";
      $vars .= "file_content=$data&";
      $vars .= "compression_type=$compressType&";
      $vars .= "compression_level=$compressLevel";

      $header  = "Host: ".$this->fsConfig['host']."\r\n";
//      $header .= "User-Agent: ".$this->systemType." client PutFile [".$this->systemType."] [".$this->fileType."]  [".$fileName."] script\r\n";
      $header .= "User-Agent: [".$this->systemType."] [".$this->fileType."] client PutFile script\r\n";
      $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
      $header .= "Content-Length: ".strlen($vars)."\r\n";
      $header .= "Connection: close\r\n\r\n";

      fputs($fp, "POST ".$this->fsConfig['url']." HTTP/1.1\r\n");
      fputs($fp, $header.$vars);
      fwrite($fp, $out);

      while(! feof($fp))
         $result .= fgets($fp, 128);

      fclose($fp);

      list($header, $response) = split("\r\n\r\n", $result);

      //print "#$response#";

      if(! preg_match("/\<\<\[\%(.+)\%\]\>\>/is", $response, $matches))
         return false;

      return $matches[1];
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetFile
   //
   // [DESCRIPTION]:   get file content from db/archive
   //
   // [PARAMETERS]:    $fileName
   //
   // [RETURN VALUE]:  false | string
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetFile($fileName="", $compressType="bzip2")
   {
      if(! $this->AssertFile($fileName))
         return false;

      // get file from cache
      if($this->file->Exists($this->pathFile.$this->fileType."/".$fileName))
      {
         if(! $this->file->Open($this->pathFile.$this->fileType."/".$fileName))
         {
            $this->strERR = $this->file->strERR;
            return false;
         }

         $data = $this->file->Read($this->file->GetSize());

         $this->file->Close();

         return $data;
      }

/*
// dont check the cr time of files from fstore
      // get file from cache
      if($this->file->Exists($this->pathFile.$this->fileType."/".$fileName))
      {
         if(! $serverCurrentTime = $this->GetFileTime($fileName))
            return false;

         if(! $resFileStat = $this->file->Stat($this->pathFile.$this->fileType."/".$fileName))
         {
            $this->strERR = $this->file->strERR;
            return false;
         }

         // we return the file content from cache only if the times are the sames
         if($resFileStat['mtime'] == $serverCurrentTime)
         {
            if(! $this->file->Open($this->pathFile.$this->fileType."/".$fileName))
            {
               $this->strERR = $this->file->strERR;
               return false;
            }

            $data = $this->file->Read($this->file->GetSize());

            $this->file->Close();

            return $data;
         }
      }
*/

      // get file from the store server
      $tries = 0;
      while(! $fp = fsockopen($this->fsConfig['host'], $this->fsConfig['port'], $errno, $errstr, $this->fsConfig['timeout']))
      {
         $tries++;
         usleep(200000);

         if($tries == 5)
            break;
      }

      if(! $fp)
      {
         $this->strERR = "$errstr ($errno)";
         return false;
      }

      $vars  = "file_name=$fileName&";
      $vars .= "action=get&";
      $vars .= "file_type=".$this->fileType."&";
      $vars .= "system_type=".$this->systemType;

      $header  = "Host: ".$this->fsConfig['host']."\r\n";
//      $header .= "User-Agent: ".$this->systemType." client GetFile [".$this->systemType."] [".$this->fileType."]  [".$fileName."] script\r\n";
      $header .= "User-Agent: [".$this->systemType."] [".$this->fileType."] client GetFile script\r\n";
      $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
      $header .= "Content-Length: ".strlen($vars)."\r\n";
      $header .= "Connection: close\r\n\r\n";

      fputs($fp, "POST ".$this->fsConfig['url']." HTTP/1.1\r\n");
      fputs($fp, $header.$vars);
      fwrite($fp, $out);

      while(! feof($fp))
         $result .= fgets($fp, 128);

      fclose($fp);

      list($header, $response) = split("\r\n\r\n", $result);

      if(! preg_match("/\<\<\[\%(.+)\%\]\>\>/is", $response, $matches))
         return false;

      $compressedData = $matches[1];

      // we save getted file into cache directories
      if(! $headerDetails = $this->GetHeaderDetails($header))
         return false;

      if(! $data = $this->DecompressData($compressedData, $headerDetails['Compression-type']))
         return false;

      $this->SaveCacheFile($fileName, $headerDetails['Current-time'], $data);

      return $data;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetCacheFile
   //
   // [DESCRIPTION]:   get file content from cache
   //
   // [PARAMETERS]:    $fileName
   //
   // [RETURN VALUE]:  false | string
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2009-01-12
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetCacheFile($fileName="")
   {
      if(! $this->AssertFile($fileName))
         return false;

      // get file from cache
      if(! $this->file->Exists($this->pathFile.$this->fileType."/".$fileName))
         return false;

      if(! $this->file->Open($this->pathFile.$this->fileType."/".$fileName))
      {
         $this->strERR = $this->file->strERR;
         return false;
      }

      $data = $this->file->Read($this->file->GetSize());

      $this->file->Close();

      return $data;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetFileTime
   //
   // [DESCRIPTION]:   get file unix time from db/archive
   //
   // [PARAMETERS]:    $fileName
   //
   // [RETURN VALUE]:  false | string
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetFileTime($fileName="")
   {
      if(! $this->AssertFile($fileName))
         return false;

      // get file from the store server
      $tries = 0;
      while(! $fp = fsockopen($this->fsConfig['host'], $this->fsConfig['port'], $errno, $errstr, $this->fsConfig['timeout']))
      {
         $tries++;
         usleep(200000);

         if($tries == 5)
            break;
      }

      if(! $fp)
      {
         $this->strERR = "$errstr ($errno)";
         return false;
      }

      $vars  = "file_name=$fileName&";
      $vars .= "action=time&";
      $vars .= "file_type=".$this->fileType."&";
      $vars .= "system_type=".$this->systemType;

      $header  = "Host: ".$this->fsConfig['host']."\r\n";
//      $header .= "User-Agent: ".$this->systemType." client GetFileTime [".$this->systemType."] [".$this->fileType."]  [".$fileName."] script\r\n";
      $header .= "User-Agent: [".$this->systemType."] [".$this->fileType."] client GetFileTime script\r\n";
      $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
      $header .= "Content-Length: ".strlen($vars)."\r\n";
      $header .= "Connection: close\r\n\r\n";

      fputs($fp, "POST ".$this->fsConfig['url']." HTTP/1.1\r\n");
      fputs($fp, $header.$vars);
      fwrite($fp, $out);

      while(! feof($fp))
         $result .= fgets($fp, 128);

      fclose($fp);

      list($header, $response) = split("\r\n\r\n", $result);

      if(! preg_match("/\<\<\[\%(.+)\%\]\>\>/is", $response, $matches))
         return false;

      $crTime = $matches[1];

      return $crTime;
   }


   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: SaveCacheFile
   //
   // [DESCRIPTION]:   save queued file to be sent laters
   //
   // [PARAMETERS]:    $fileName, $fileContent
   //
   // [RETURN VALUE]:  false | true
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SaveCacheFile($fileName="", $crTime="", $fileContent="")
   {

      if(! $this->AssertFile($fileName, $crTime))
         return false;

      if(! $this->file->Open($this->pathFile.$this->fileType."/".$fileName.".tmp","w+"))
      {
         $this->strERR = $this->file->strERR;
         return false;
      }

      if(! $this->file->Write($fileContent))
      {
         $this->strERR = $this->file->strERR;
         return false;
      }

      $this->file->Close();

      // rename temp file to real name
      if(! $this->file->Rename($this->pathFile.$this->fileType."/".$fileName.".tmp", $this->pathFile.$this->fileType."/".$fileName, true))
      {
         $this->strERR = $this->file->strERR;
         return false;
      }

      if(! $this->file->Touch($this->pathFile.$this->fileType."/".$fileName, $crTime))
      {
         $this->strERR = $this->file->strERR;
         return false;
      }

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: SaveQueueFile
   //
   // [DESCRIPTION]:   save queued file to be sent laters
   //
   // [PARAMETERS]:    $fileName, $fileContent
   //
   // [RETURN VALUE]:  false | true
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SaveQueueFile($fileName="", $crTime="", $fileContent="")
   {
      if(! $this->AssertFile($fileName, $crTime))
         return false;

      $this->CreateDirectory($this->queuePathFile.$this->fileType);

      if(! $this->file->Open($this->queuePathFile.$this->fileType."/".$fileName.".tmp","w+"))
      {
         $this->strERR = $this->file->strERR;
         return false;
      }

      if(! $this->file->Write($fileContent))
      {
         $this->strERR = $this->file->strERR;
         return false;
      }

      $this->file->Close();

      // rename temp file to real name
      if(! $this->file->Rename($this->queuePathFile.$this->fileType."/".$fileName.".tmp", $this->queuePathFile.$this->fileType."/".$fileName, true))
      {
         $this->strERR = $this->file->strERR;
         return false;
      }

      if(! $this->file->Touch($this->queuePathFile.$this->fileType."/".$fileName, $crTime))
      {
         $this->strERR = $this->file->strERR;
         return false;
      }

      return true;
   }


   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: DeleteQueueFile
   //
   // [DESCRIPTION]:   remove queued file after the file sent succesfully
   //
   // [PARAMETERS]:    $fileName
   //
   // [RETURN VALUE]:  false | true
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function DeleteQueueFile($fileName="")
   {
      if(! $this->AssertFile($fileName))
         return false;

      if(! $this->file->Delete($this->queuePathFile.$this->fileType."/".$fileName))
         return false;

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: DeleteCacheFile
   //
   // [DESCRIPTION]:   remove cached file after the file checked succesfully
   //
   // [PARAMETERS]:    $fileName
   //
   // [RETURN VALUE]:  false | true
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function DeleteCacheFile($fileName="")
   {
      if(! $this->AssertFile($fileName))
         return false;

       // real mode
       if(! $this->file->Delete($this->pathFile.$this->fileType."/".$fileName))
          return false;

      return true;

       // debug mode -  we move file into file type don bkp
      if(! is_dir($this->pathFile.$this->fileType.".bkp/"))
         mkdir($this->pathFile.$this->fileType.".bkp/");
      
      if(! $this->file->Rename($this->pathFile.$this->fileType."/".$fileName,$this->pathFile.$this->fileType.".bkp/".$fileName, true))
         return false;

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: DeleteFile
   //
   // [DESCRIPTION]:   delete file from file store server
   //
   // [PARAMETERS]:    $fileName
   //
   // [RETURN VALUE]:  false | true
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function DeleteFile($fileName="")
   {
      if(! $this->AssertFile($fileName))
         return false;

      $tries = 0;
      while(! $fp = fsockopen($this->fsConfig['host'], $this->fsConfig['port'], $errno, $errstr, $this->fsConfig['timeout']))
      {
         $tries++;
         usleep(200000);

         if($tries == 5)
            break;
      }

      $vars  = "file_name=$fileName&";
      $vars .= "action=delete&";
      $vars .= "file_type=".$this->fileType."&";
      $vars .= "system_type=".$this->systemType;

      $header  = "Host: ".$this->fsConfig['host']."\r\n";
//      $header .= "User-Agent: ".$this->systemType." client DeleteFile [".$this->systemType."] [".$this->fileType."]  [".$fileName."] script\r\n";
      $header .= "User-Agent: [".$this->systemType."] [".$this->fileType."] client DeleteFile script\r\n";
      $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
      $header .= "Content-Length: ".strlen($vars)."\r\n";
      $header .= "Connection: close\r\n\r\n";

      fputs($fp, "POST ".$this->fsConfig['url']." HTTP/1.1\r\n");
      fputs($fp, $header.$vars);
      fwrite($fp, $out);

      while(! feof($fp))
         $result .= fgets($fp, 128);

      fclose($fp);

      list($header, $response) = split("\r\n\r\n", $result);

      //print "#$response#";

      if(! preg_match("/\<\<\[\%(.+)\%\]\>\>/is", $response, $matches))
         return false;

      return $matches[1];
   }


   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetQueueFiles
   //
   // [DESCRIPTION]:   get queued files to be sent to the file server
   //
   // [PARAMETERS]:    $fileName
   //
   // [RETURN VALUE]:  false | array
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetQueueFiles($filterFileType="")
   {
      $results = array();
      if(! $fileTypes = $this->file->ListDirs($this->queuePathFile))
         return false;

      foreach($fileTypes as $fileType => $null)
      {
        if(! empty($filterFileType))
           if($fileType != $filterFileType)
             continue;

         $fileNames = array();

         if(! $fileNames = $this->file->ListFiles($this->queuePathFile.$fileType))
            continue;

         if(! empty($fileNames))
         {
            foreach($fileNames as $index => $fileName)
            {

               if(preg_match("/\.tmp$/i", $fileName))
                  continue;

               $results[$fileType][] = $fileName;
            }
         }
      }

      if( empty($results))
         return false;

      return $results;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PutQueueFiles
   //
   // [DESCRIPTION]:   send queued files to to be stored on the file server
   //
   // [PARAMETERS]:    $filterFileType
   //
   // [RETURN VALUE]:  void
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PutQueueFiles($filterFileType="")
   {

      if(! $queueFiles = $this->GetQueueFiles($filterFileType))
         return ;
    
      $this->pathFile = $this->queuePathFile;
 
      foreach($queueFiles as $fileType => $resFiles)
      {
         $this->fileType = $fileType;

         foreach($resFiles as $index => $fileName)
         {
            $returnedCode = $this->PutFile($fileName);

            usleep(200000);

            if(! preg_match("/^\d+$/", $returnedCode))
               continue;

           $this->DeleteQueueFile($fileName);
         }
      }

      return ;
   }
   
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CompressData($data="")
   //
   // [DESCRIPTION]:   Compreses data
   //
   // [PARAMETERS]:    $data="",$compressType="bzip2", $compressLevel="9"
   //
   // [RETURN VALUE]:  Error if compress fails, compressed data otherwise.
   //
   // [CREATED BY]:    Gabriel Istvancsek (gabi@acrux.biz) 2007-06-04
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CompressData($data="", $compressType="bzip2", $compressLevel="9")
   {
      if(empty($data))
      {
         $this->strERR = "DATA_TO_DECOMPRES_IS_EMPTY";
         return false;
      }

      switch($compressType)
      {
         case "bzip2":
            $compressedData = bzcompress($data, $compressLevel);
            break;

         case "gzip":
            $compressedData = gzcompress($data, $compressLevel);
            break;

         default:
            $this->strERR = "INVALID_COMRESSION_TYPE";
            return false;
            break;
      }

      return $compressedData;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: DecompressData($data="")
   //
   // [DESCRIPTION]:   Decompreses data
   //
   // [PARAMETERS]:    $data="",$compressType="9"
   //
   // [RETURN VALUE]:  Error if decompressfails, decompressed data otherwise.
   //
   // [CREATED BY]:    Gabriel Istvancsek (gabi@acrux.biz) 2007-06-04
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function DecompressData($data="", $compressType="bzip2")
   {
      if(empty($data))
      {
         $this->strERR = "DATA_TO_DECOMPRES_IS_EMPTY";
         return false;
      }

      switch($compressType)
      {
         case "bzip2":
            $decompressedData = bzdecompress($data);
            break;

         case "gzip":
            $decompressedData = gzuncompress($data);
            break;

         default:
            $this->strERR = "INVALID_COMRESSION_TYPE";
            return false;
            break;
      }

      return $decompressedData;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetHeaderDetails
   //
   // [DESCRIPTION]:   get header details
   //
   // [PARAMETERS]:    $header
   //
   // [RETURN VALUE]:  false | array
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetHeaderDetails($header="")
   {
      if(empty($header))
      {
         $this->strERR = "EMPTY_HEADER";
         return false;
      }

      $result = array();
      $lines = explode("\n",$header);
 
      foreach($lines as $index => $line)
      {
         if(empty($line))
            continue;

         list($key, $value) = split(":", $line);
         $result[trim($key)] = trim($value);
      }

      return $result;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CreateDirectory
   //
   // [DESCRIPTION]:   create recursive directory
   //
   // [PARAMETERS]:    $path
   //
   // [RETURN VALUE]:  false | ture
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-07-11
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CreateDirectory($path)
   {
      if(is_dir($path))
        return true;

      $directories = explode("/",$path);

      $dir = "";
      if(preg_match("/^\//", $path))
         $dir = "/";

      $oldUmask = umask(0);
      foreach($directories as $index => $dirName)
      {
         if(empty($dirName))
            continue;

         $dir .= $dirName."/";

         if(is_dir($dir))
            continue;

          if(! mkdir($dir))
          {
            umask($oldUmask);
            $this->strERR = 'CANNOT_CREATE_DIRECTORY : '.$dir;
            return false;
          }
      }

      umask($oldUmask);

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetError()
   //
   // [DESCRIPTION]:   get error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  string
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetError()
   {
      return $this->strERR;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ShowError()
   //
   // [DESCRIPTION]:   show error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  string
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-05-30
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function ShowError()
   {
      print $this->strERR;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetAllFilesByFilesIn()
   //
   // [DESCRIPTION]:   get all files by files name in to be send to the file store
   //
   // [PARAMETERS]:    $fileName
   //
   // [RETURN VALUE]:  false | array
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-07-12
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetAllFilesByFilesIn($filesName)
   {
      if(! is_array($filesName))
      {
        $this->strERR = "INVALID_FILES_NAME_ARRAY";
        return false;
      }
      
      $objMySql = new CMySQL();
      
      if(! $objMySql->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $objMySql->GetError();
        return false;
      }
      
      foreach($filesName as $index => $fileName)
      {
          if(empty($fileName))
             continue;
             
          $objMySql->sqlCMD = "select qt.name as system_type, l.filename as files_in,if(qs.status='SUCCESS',CONCAT(l.filename,'_',qs.site_id,'.html'),if(qs.status='FAILURE',CONCAT(l.filename,'_',qs.site_id,'.err'),NULL)) as files_out, CONCAT(l.filename,'_',qs.site_id,'.htm') as files_data, CONCAT(l.filename,'_',qs.site_id,'.htm') as files_logs, if(qs.status='FAILURE' OR qs.status='NO QUOTE',CONCAT(l.filename,'_',qs.site_id,'.htm'),NULL) as files_errors from ".SQL_QZQUOTES." qz, ".SQL_LOGS." l, ".SQL_QZQUOTE_TYPES." qt ,".SQL_QUOTE_STATUS." qs where l.id=qz.log_id and l.filename='$fileName' and qz.quote_type_id=qt.id and qs.log_id=l.id";
   
         if(! $objMySql->Exec($objMySql->sqlCMD))
         {
           $this->strERR = $objMysql->GetError();
           return false;
         }
         
         while($objMySql->MoveNext())
         {
            $systemType = $objMySql->GetFieldValue("system_type");
            $in         = $objMySql->GetFieldValue("files_in");
            $out        = $objMySql->GetFieldValue("files_out");
            $data       = $objMySql->GetFieldValue("files_data");
            $logs       = $objMySql->GetFieldValue("files_logs");
            $errors     = $objMySql->GetFieldValue("files_errors");
   
            if($out) $results[$systemType]['out'][]       = $out;
            if($data) $results[$systemType]['data'][]     = $data ;
            if($logs) $results[$systemType]['logs'][]     = $logs ;
            if($errors) $results[$systemType]['errors'][] = $errors ;
         }
   
         if($in) $results[$systemType]['in'][]     = $in ;
      }
      
      return $results;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PutUpdatedFile
   //
   // [DESCRIPTION]:   send compressed file to file store server, updated but with same creation time from session
   //
   // [PARAMETERS]:    $fileName, $fileContent
   //
   // [RETURN VALUE]:  false | true
   //
   // [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2013-02-19
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PutUpdatedFile($fileName="", $compressType='bzip2', $compressLevel=9)
   {
      if(! $this->AssertFile($fileName))
         return false;

      if(! $this->file->Open($this->pathFile.$this->fileType."/".$fileName))
      {
         $this->strERR = $this->file->strERR;
         return false;
      }

      $data = $this->file->Read($this->file->GetSize());

      $this->file->Close();

      if(! $resFileStat = $this->file->Stat($this->pathFile.$this->fileType."/".$fileName))
      {
         $this->strERR = $this->file->strERR;
         return false;
      }

      // get the initial time of creation from session
      $Session = array();
      eval("\$Session = $data;");

      $resFileStat['mtime'] = $Session["TIME"];

      $tries = 0;
      while(! $fp = fsockopen($this->fsConfig['host'], $this->fsConfig['port'], $errno, $errstr, $this->fsConfig['timeout']))
      {
         $tries++;
         usleep(200000);

         if($tries == 5)
            break;
      }

      if(! $fp)
      {
         $this->SaveQueueFile($fileName, $resFileStat['mtime'], $data);

         return false;
      }

      if(! $compressedData = $this->CompressData($data, $compressType, $compressLevel))
         return false;

      $data = rawurlencode($compressedData);

      $vars  = "file_name=$fileName&";
      $vars .= "action=send&";
      $vars .= "cr_time=".$resFileStat['mtime']."&";
      $vars .= "file_type=".$this->fileType."&";
      $vars .= "system_type=".$this->systemType."&";
      $vars .= "file_content=$data&";
      $vars .= "compression_type=$compressType&";
      $vars .= "compression_level=$compressLevel";

      $header  = "Host: ".$this->fsConfig['host']."\r\n";
//      $header .= "User-Agent: ".$this->systemType." client PutFile [".$this->systemType."] [".$this->fileType."]  [".$fileName."] script\r\n";
      $header .= "User-Agent: [".$this->systemType."] [".$this->fileType."] client PutFile script\r\n";
      $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
      $header .= "Content-Length: ".strlen($vars)."\r\n";
      $header .= "Connection: close\r\n\r\n";

      fputs($fp, "POST ".$this->fsConfig['url']." HTTP/1.1\r\n");
      fputs($fp, $header.$vars);
      fwrite($fp, $out);

      while(! feof($fp))
         $result .= fgets($fp, 128);

      fclose($fp);

      list($header, $response) = split("\r\n\r\n", $result);

      //print "#$response#";

      if(! preg_match("/\<\<\[\%(.+)\%\]\>\>/is", $response, $matches))
         return false;

      return $matches[1];
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CheckCachedFiles()
   //
   // [DESCRIPTION]:   check cached files by last access time and and check if exists on file store server,
   //                  in case of exists delete it else we send it to the file store and leave untouched
   //
   // [PARAMETERS]:    $fileName
   //
   // [RETURN VALUE]:  false | array
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-07-12
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CheckCachedFiles($unixTime=0)
   {
      // set default unix time 3 months ago
      if(empty($unixTime))
      {
         $unixTime = mktime() - 90 * 24 * 60 * 60;
      }
      
      $maxEntries = 10;
      $offset     = 0;
      while(true)
      {
         $fileCounter = 0;
  
         // we check the only the in file stats
         if(! $cachedFiles = $this->file->ListFiles($this->pathFile."in/", $maxEntries,false,$fileCounter,$offset))
            break;

//         print_r($cachedFiles);

        $tempArray = array();

         if(! $resCachedFiles = $this->GetAllFilesByFilesIn($cachedFiles))
            continue;

//          print_r($resCachedFiles);

         foreach($resCachedFiles as $systemType => $resFileTypes)
         {
            $this->SetSystemType($systemType);
            
            foreach($resFileTypes as $fileType => $resFiles)
            {
               $this->SetFileType($fileType);

               foreach($resFiles as $index => $fileName)
               {
                  // get atime from cached files
                  if(! $resFileStat = $this->file->Stat($this->pathFile.$fileType."/".$fileName))
                  {
                     print ",";
                     $this->strERR = $this->file->strERR;
//                      print $this->file->strERR."\n";

                     continue;
                  }

//                  // we ignore file with the access time less than 3 months
//                  if($resFileStat['atime'] > $unixTime)
//                     continue;

//                  // debuig mode - move if file mod time is less than 3 months
//                 if($resFileStat['mtime'] > $unixTime)
//                    continue;

                  // before delete file from cache we have to 
                  // check if exists file on the file store server and compare mtime
                  if(! $mTime = $this->GetFileTime($fileName))
                  {
                     // we have to send it to the file store server 
                     $returnedCode = $this->PutFile($fileName);
 
                     print ".";  
 
                     continue;
                  }
                  
                  // TO DO : we have to resend the file from the client to File Store Server
                  if($resFileStat['mtime'] != $mTime)
                  {
                     // we have to send it to the file store server 
                     $returnedCode = $this->PutFile($fileName);
                 
                     print "s";

                     continue;
                  }
                  
                  // delete cached file
                  $deleted = '?';
                  if($this->DeleteCacheFile($fileName))
                     $deleted = '0';
                  
                  print "$deleted";
                  
               } // foreach($resFiles as $index => $fileName)
            } // foreach($resFileTypes as $fileType => $resFiles)
         } // foreach($resCachedFiles as $systemType => $resFileTypes)
         
         $offset += $maxEntries;

         sleep(5);
      }

      return;
   }

}

?>

<?php

include_once "/home/www/quotezone.co.uk/insurance-new/car/modules/globals.inc";
include_once "CAREngine.php";
include_once "errors.inc";
include_once "MySQL.php";
include_once "EmailStore.php";
include_once "EmailReminder.php";
include_once "EmailStoreCodes.php";
include_once "QuoteDetails.php";
include_once "/home/www/quotezone.co.uk/insurance-new/car/modules/Vehicle.php";
include_once "Occupation.php";
include_once "Business.php";
include_once "Session.php";
include_once "EmailOutboundsEngine.php";
include_once "EmailOutbounds.php";
include_once "Postcode.php";
include_once "/home/www/quotezone.co.uk/common/modules/CashBack.php";
include_once "FileStoreClient.php";

/*start of driver details */
$_PROPOSER = array(



    "type_of_driving" => array(
    
      "F" => "Full UK",
      "P" => "Provisional UK",
      "A" => "Automatic UK",
      "EU" => "EU",
      "E" => "EEC Full",
      "EP" => "EEC Provisional",
      "O" => "Other European",
      "M" => "International Must Exchange",
      "N" => "International Non-Exchangeable",

    ),

    "marital_status" => array(
     "M" => "Married",
      "S" => "Single",
      "D" => "Divorced",
      "W" => "Widowed",
      "C" => "Common law",
      "V" => "Civil partnership",
      "T" => "Separated",
    ),

    "use_of_other_vehicles" => array(
      "X" => "No access to any other vehicles",
      "N" => "Named driver on another person's policy",
      "O" => "Owner of another car",
      "M" => "Own/Use a motorcycle",
      "V" => "Own/Use a Van",
      "S" => "Company car (including social domestic and pleasure use)",
      "C" => "Company car (within working hours)",

    ),

    "who_drive_the_vehicle" => array(
      "IO" => "Yourself Only",
      "IS" => "Yourself and your spouse",
      "02" => "Yourself & 1 other person",
      "03" => "Yourself & 2 other persons",
      "04" => "Yourself & 3 other persons",
    ),

    "employment_status" => array(      
      "E" => "Employed",
      "F" => "Full-time education",
      "S" => "Self Employed",
      "R" => "Retired",
      "U" => "Unemployed",
      "D" => "Director",
      "P" => "Proprietor or Partner",
      "H" => "Houseperson",
      "I" => "Independent Means",
      "N" => "Not employed due to disability",
      "G" => "Government",
      "C" => "Club or Association",

    ),



);

$_ADDITIONAL = array(
    "additional_sex" => array(
      "F" => "Female",
      "M" => "Male",
    ),

    "additional_type_of_driving" => array(
         "F" => "Full UK",
      "P" => "Provisional UK",
      "A" => "Automatic UK",
      "EU" => "EU",
      "E" => "EEC Full",
      "EP" => "EEC Provisional",
      "O" => "Other European",
      "M" => "International Must Exchange",
      "N" => "International Non-Exchangeable",
    ),
);

$_VEHICLE =array(
"vehicle_make" => array (  
      "AC" => "AC",
      "AI" => "AIXAM",
      "AR" => "ALFA ROMEO",
      "A1" => "AMC",
      "A2" => "ANT",
      "AQ" => "ALLARD",
      "AL" => "ALVIS",
      "AS" => "ARM SIDDELEY",
      "AO" => "ARO",
      "AY" => "ASIA",
      "AM" => "ASTON MARTIN",
      "AU" => "AUDI",
      "AN" => "AUSTIN",
      "DK" => "AUTO UNION",
      "BN" => "BENTLEY",
      "BZ" => "BITTER",
      "BM" => "BMW",
      "BO" => "BOND",
      "BW" => "BORGWARD",
      "BR" => "BRISTOL",
      "BG" => "BUGATTI",
      "BU" => "BUICK",
      "CA" => "CADILLAC",
      "CT" => "CATERHAM",
      "CH" => "CHEVROLET",
      "CB" => "CHRYSLER",
      "C1" => "CHRYS AUS",
      "CN" => "CITROEN",
      "C2" => "CLUB CAR",
      "C3" => "COLT",
      "DF" => "DAF",
      "DC" => "DACIA",
      "DW" => "DAEWOO",
      "DH" => "DAIHATSU",
      "DM" => "DAIMLER",
      "DA" => "DALLAS",
      "DS" => "DATSUN",
      "DT" => "DE TOMASO",
      "DE" => "DELOREAN",
      "DO" => "DODGE",
 "ER" => "ERA",
      "FS" => "FSO",
      "FV" => "FACEL VEGA",
      "FP" => "FAIRTHORPE",
      "FE" => "FERRARI",
      "FT" => "FIAT",
      "FO" => "FORD",
      "FA" => "FORD (USA)",
      "FN" => "FORD (AUS)",
      "GE" => "GEM",
      "GL" => "GILBERN",
      "GN" => "GINETTA",
      "GO" => "GOGGOMOBIL",
      "HM" => "HILLMAN",
      "HI" => "HINDUSTAN",
      "HO" => "HOLDEN",
      "HD" => "HONDA",
      "HU" => "HUMBER",
      "HV" => "HUMMER",
      "HY" => "HYUNDAI",
      "IS" => "ISO RIVOLTA",
      "IZ" => "ISUZU",
      "I1" => "INFINITI",
      "JA" => "JAGUAR",
      "JE" => "JEEP",
      "JN" => "JENSEN",
      "JO" => "JOWETT",
      "KH" => "KHALEEJ",
      "KA" => "KIA",
      "LR" => "LANDROVER",
      "LA" => "LADA",
      "LM" => "LAMBORGHINI",
      "LL" => "LANCHESTER",
      "LN" => "LANCIA",
      "LX" => "LEXUS",
      "L1" => "LEXUS (USA)",
      "LI" => "LIGIER",
      "LC" => "LINCOLN",
      "LO" => "LONSDALE",
      "LT" => "LOTUS",
      "MH" => "MAHINDRA",
      "MX" => "MARANELLO",
"MR" => "MARCOS",
      "ML" => "MARLIN",
      "MS" => "MASERATI",
      "MB" => "MAYBACH",
      "MZ" => "MAZDA",
      "MC" => "MERCEDES-BENZ",
      "MY" => "MERCURY",
      "MG" => "MG",
      "M1" => "MG-MOTOR UK",
      "MA" => "MICROCAR",
      "MI" => "MINI",
      "CO" => "MITSUBISHI",
      "MN" => "MORGAN",
      "MO" => "MORRIS",
      "MK" => "MOSKVICH",
      "ME" => "MOSLER",
      "MD" => "MYCAR",
      "NM" => "NAC MG",
      "NA" => "NISSAN",
      "NO" => "NOBLE",
      "NS" => "NSU",
      "OK" => "OKA",
      "OP" => "OPEL",
      "PZ" => "PANHARD",
      "PT" => "PANTHER",
      "PE" => "PERODUA",
      "PU" => "PEUGEOT",
      "PS" => "PORSCHE",
      "PR" => "PORTARO",
      "P1" => "PGO",
      "PN" => "PROTON",
      "RE" => "RELIANT",
      "RN" => "RENAULT",
      "RA" => "REVA",
      "RL" => "RILEY",
      "RR" => "ROLLS ROYCE",
      "RV" => "ROVER",
      "SA" => "SAAB",
      "SC" => "SAN",
      "SN" => "SANTANA",
      "SO" => "SAO",
      "SE" => "SEAT",
 "SM" => "SIMCA",
      "SX" => "SECMA",
      "SI" => "SINGER",
      "SK" => "SKODA",
      "MM" => "SMART",
      "SY" => "SSANGYONG",
      "SD" => "STANDARD",
      "ST" => "STEVENS",
      "SU" => "SUBARU",
      "SB" => "SUNBEAM",
      "SZ" => "SUZUKI",
      "TB" => "TALBOT",
      "TT" => "TATA",
      "TD" => "TD",
      "TE" => "TESLA",
      "TK" => "THINK",
      "TY" => "TOYOTA",
      "TR" => "TRIUMPH",
      "TV" => "TVR",
      "VA" => "VALIANT",
      "VP" => "VANDEN PLAS",
      "VX" => "VAUXHALL",
      "VN" => "VENTURI",
      "VE" => "VEXEL",
      "VL" => "VOLGA",
      "VO" => "VOLVO",
      "VW" => "VOLKSWAGEN",
      "WT" => "WARTBURG",
      "WS" => "WOLSELEY",
      "YU" => "YUGO/ZASTAVA",

    ),

    "vehicle_kept" => array( 
    "R" => "On the road",
      "D" => "On a private driveway",
      "G" => "In a locked garage",
      "U" => "In an unlocked garage",
      "P" => "Residential Parking",
      "N" => "Non-secure Car Park",
      "S" => "Secure Car Park",
      "C" => "Carport",
      "RH" => "Road away from home",
      "RP" => "Railway car park",
      "W" => "Work car park",
      "F" => "Forces base car park",

    ),

);

$_COVER = array(
    "type_of_cover" => array( 
      "1" => "Comprehensive",
      "2" => "Third Party Fire & Theft",
      "3" => "Third Party Only",

    ),

    "type_of_use" => array( 
      "4" => "Social, Domestic, Pleasure and Commuting",
      "5" => "Social, Domestic and Pleasure Only",
      "1" => "Business Use (by you)",
      "6" => "Business use by you and spouse",
      "7" => "Business use for all drivers",
      "8" => "Business use by spouse",
      "9" => "Commercial travelling",

    ),

    "protect_bonus" => array( 
    "Y" => "Yes",
      "N" => "No",

    ),

    "owner_of_vehicle" => array(
        "1"=>"Proposer",
      "2"=>"Spouse",
      "3" => "Parents",
      "4" => "Company",
      "5" => "Privately Leased",
      "7" => "Common Law Partner",
      "8" => "Civil Partner",
      "9" => "Society or Club",
      "6" => "Other",

    ),
);
/* end of driver details */

      
$objFSClient        = new CFileStoreClient('car',"in");
$objEmailStore      = new CEmailStore();
$objEmailStoreCodes = new CEmailStoreCodes();
$objEmailReminder   = new CEmailReminder();
$objQuoteDetails    = new CQuoteDetails();
$objVehicle         = new CVehicle();
$objOccupation      = new COccupation();
$objBusiness        = new CBusiness();
$objSession         = new CSession();
$objEmailOutbounds  = new CEmailOutbounds();
$objPostcode        = new CPostcode();


$dbh = new CMySQL();
$dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS);
      
$fd = fopen('/tmp/EMV_logs.txt','r');
      
if(! $fd)
  die("cannot open file");

// load data
$data = array();
$dataLine = '';

// skip header
fgets($fd);

// get the metrics here
while(! feof($fd))
{
   $dataLine = fgets($fd);
   $dataLine = trim($dataLine);
   $filename = $dataLine;

   if(! $data = $objFSClient->GetFile($filename))
   {
     print "Cannot get the file form filestore \n\n\n"; 
     continue;
   }

   eval("\$Session = $data;");

   $logID       = $Session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];
   $wlUserID    = $Session["_QZ_QUOTE_DETAILS_"]["wlUserID"];
   $emailAddr   = $Session["_DRIVERS_"][0]["email_address"];
   $quoteUserID = $Session["_QZ_QUOTE_DETAILS_"]["quote_user_id"];

   // DO NOT SEND IF TEST QUOTE
   if(isset($Session["TESTING SITE ID"]))
     continue;

   if(preg_match("/seopa\d+@seopa\.com/",$Session['_DRIVERS_'][0]['email_address']) && $wlUserID != "279")
     continue;

   $logID          = $Session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];
   $emailAddress   = $Session["_DRIVERS_"]["0"]["email_address"];
   $quoteUserID    = $Session["_QZ_QUOTE_DETAILS_"]["quote_user_id"];

   if (!$resEmailStoreDetails = $objEmailStore->GetEmailStoreByEmailAndQuoteUserId($emailAddress,$quoteUserID))
     print $objEmailStore->GetError();

   if(!$link = $objEmailStoreCodes->GenerateLink($resEmailStoreDetails['id'],$resEmailStoreDetails['email'],1))
     print $objEmailStoreCodes->GetError();

   $cheappestSiteId         = "0";
   $cheapeastQuotePrice     = "0";
   $cheappestQuoteArray     = array();
   $cheappestSiteQuoteArray = array();

   $cheappestSiteQuoteArray = $objQuoteDetails->GetCheapestTopFiveSitesQuoteDetails($logID, 1);
   $cheappestSiteId         = $cheappestSiteQuoteArray[1];
   $cheappestQuoteArray     = $objQuoteDetails->GetCheapestSiteQuoteDetails($logID,$cheappestSiteId);
   $cheapeastQuotePrice     = $cheappestQuoteArray["cheapest_quote"];

   $PostArray = array();

   // new fields - ticket ZTH-983371
   $PostArray["emv_tag"]     = "2056A92D0100000C";
   $PostArray["emv_ref"]     = "EdX7CqkmjHZG8SA9MKJPUB7VKEl8baTBjjjce6k2WMCqK4s";
   $PostArray["EMAIL_FIELD"] = $Session["_DRIVERS_"]["0"]["email_address"];

   $date = date("Y-m-d");

    if(! $emStoreID = $objEmailStore->GetEmailStoreIDByEmailAndQuoteUserId($PostArray["EMAIL_FIELD"],$quoteUserID))
    {
	print "CANNOT_GET_THE_EMAIL_STORE_ID \n\n";
    }

    while (1)
    {
       $authInfo = GenerateMimeBoundary();
       $authInfo = str_replace("----=_NextPart_","",$authInfo);
       $authInfo = substr($authInfo, 0, 16);

       if(! $objEmailReminder->GetEmRemByAuthInfo($authInfo, $array))
          break;
    }

    if(! $objEmailReminder->AddEmailRem($quoteUserID,$emStoreID,"1",$date,"","1", $authInfo,'1','1','0'))
    {
       print $objEmailReminder->GetError();
    }

    $PostArray["RENEW_LINK_FIELD"]      = $link;
    $PostArray["emv_bounceback"]        = "1";
    $PostArray["emv_pageok"]            = "http://www.quotezone.co.uk/emailpostsuccesscar.htm";
    $PostArray["emv_pageerror"]         = "http://www.quotezone.co.uk/emailpostfailurecar.htm";
    $PostArray["emv_webformtype"]       = "0";
    $PostArray["emv_clientid"]          = "35812";

// changed this because of the ticketID HBF-721253
    //$campaignID = "2101993";
    //$campaignID = "5793286";
    $campaignID = "7952067";

    $wlUserID = $Session['_QZ_QUOTE_DETAILS_']['wlUserID'];

    //powerseeker wl id`s
    // 15 18 35 42 102
    if($wlUserID)
    {
	switch($wlUserID)
	{
	  case '15':
	  case '18':
	  case '35':
	  case '42':
	  case '102':
	      $campaignID = "5659205";
	  break;
	  
	  case '258':
	      $campaignID = "6673620";
	  break;

	  case '239':
	      $campaignID = "6830658";
	  break;

	  case '258':
	      $campaignID = "6673620";
	  break;

	  case '272':
	      $campaignID = "6954084";
	  break;

	  case '237':
	      if ($Session['RETRIEVE_QUOTES']['ISIS_NEW_QUOTE'])
		      $campaignID = "6954084";
	  break;

	  case '279':
	      if (!$Session['RETRIEVE_QUOTES']['ISIS_NEW_QUOTE'])
	      {
		$PostArray["EMAIL_FIELD"]   = $Session["_DRVTMP_"]["email_address"];
		
		$campaignID = "7307953";
		      
		$randNoToCheck = make_seed();
		
		if($randNoToCheck % 2 == 0)
		    $campaignID = "7458349";
	      }
	  break;

	  default:
	      //$campaignID = "5793286";
	      $campaignID = "7952067";
	  break;

	}
    }

    //set the campaingn id for modified vehciels and an extra parameter MOD 1
    if($Session['_AFFILIATES_']['modified'] == "1" || $Session['_QZ_QUOTE_DETAILS_']['wl_modified'] == "1")
    {
	$campaignID                       = "5777165";
	$PostArray["CAMPAIGN_CODE_FIELD"] = "QZMOD1";
    }

    //set the campaingn id for modified vehciels and an extra parameter MOD 2
    if($Session['_AFFILIATES_']['modified'] == "2" || $Session['_QZ_QUOTE_DETAILS_']['wl_modified'] == "2")
    {
	$campaignID                       = "5777170";
	$PostArray["CAMPAIGN_CODE_FIELD"] = "QZMOD2";
    }

    //modified due to ticket id : KBV-596330
    //if($_COOKIE['AFFID'] == "D1B9C2-001")
    if($Session['_AFFILIATES_']['id'] == "D1B9C2-001")
	$campaignID = "6113055";

    $PostArray["emv_campaignid"]        = $campaignID;

    $PostArray["TITLE_FIELD"]           = $Session["_DRIVERS_"]["0"]["title"];
    $PostArray["FIRSTNAME_FIELD"]       = $Session["_DRIVERS_"]["0"]["first_name"];
    $PostArray["LASTNAME_FIELD"]        = $Session["_DRIVERS_"]["0"]["surname"];
    $PostArray["DATEOFBIRTH_FIELD"]     = $Session["_DRIVERS_"]["0"]["date_of_birth_mm"]."/".$Session["_DRIVERS_"]["0"]["date_of_birth_dd"]."/".$Session["_DRIVERS_"]["0"]["date_of_birth_yyyy"];

    //vehicle
    $codeMake                                = $Session["_VEHICLE_"]["vehicle_make"];
    $make                                    = $_VEHICLE["vehicle_make"][$codeMake];
    $PostArray["VEHICLE_MAKE_FIELD"]         = $make;
    $PostArray["VEHICLE_MODEL_FIELD"]        = $Session["_VEHICLE_"]["vehicle_model"];
    $PostArray["YEAR_OF_MANUFACTURE_FIELD"]  = $Session["_VEHICLE_"]["year_of_manufacture"];
    $engine                                  = explode(" ",$Session["_VEHICLE_"]["engine_size"]);
    switch($engine[1])
    {
	case "P":
	  $engineFuel = "Petrol";
	break;

	case "D":
	  $engineFuel = "Diesel";
	break;
    }
    $PostArray["ENGINE_SIZE_AND_TYPE_FIELD"] = $engine[0]."cc ".$engineFuel;
    
    //vehicle abicode description
    $PostArray["ABI_DESCRIPTION_FIELD"]      = $Session["_VEHICLE_"]["engine_size"];
    $vehicleElementsArray                    = $objVehicle->GetVehicle($objVehicle->GetVehicleCode($Session["_VEHICLE_"]["vehicle_confirm"]));
    $PostArray["ABI_DESCRIPTION_FIELD"]      =  $vehicleElementsArray["make_description"]." ".$vehicleElementsArray["model_description"];

    $maritalStatus                           = $Session["_DRIVERS_"]["0"]["marital_status"];
    $PostArray["MARITAL_STATUS_FIELD"]       = $_PROPOSER["marital_status"][$maritalStatus];
    $PostArray["NUMBER_OF_CHILDREN_FIELD"]   = $Session["_DRIVERS_"]["0"]["children"];
    $PostArray["POSTCODE_FIELD"]             = $Session["_DRIVERS_"]["0"]["postcode_prefix"]." ".$Session["_DRIVERS_"]["0"]["postcode_number"];
    $PostArray["HOUSE_NUMBER_FIELD"]         = $Session["_DRIVERS_"]["0"]["house_number_or_name"];

    $postCode          = $Session["_DRIVERS_"]["0"]["postcode_prefix"]." ".$Session["_DRIVERS_"]["0"]["postcode_number"];
    $houseNumnerOrName = $Session["_DRIVERS_"]["0"]["house_number_or_name"];

    $postCodeDetailsArray = array();
    $postCodeDetailsArray = $objPostcode->GeoPostCodeLookup($postCode,$houseNumnerOrName);

    $PostArray["STREET_FIELD"]               = $postCodeDetailsArray["line1"];
    $PostArray["TOWN_CITY_FIELD"]            = $postCodeDetailsArray["post_town"];
    $PostArray["COUNTY_FIELD"]               = $postCodeDetailsArray["county"];

    $homeOwner                               = $Session["_DRIVERS_"]["0"]["home_owner"];
    if($homeOwner == "N")
    {
	$PostArray["HOMEOWNER_FIELD"]         = "No";
	$PostArray["HOME_RENEW_DATE_FIELD"]   = "";
    }
    if($homeOwner == "Y")
    {
	$PostArray["HOMEOWNER_FIELD"]         = "Yes";

	// home renewal date (empy string if is not home owner)
	$curYear = date("Y");

	$homeRenewDay   = "01";
	$homeRenewMonth = $Session["_DRIVERS_"]["0"]["home_renewal"];
	$homeRenewYear  = $curYear;

	$PostArray["HOME_RENEW_DATE_FIELD"]   = $homeRenewMonth."/".$homeRenewDay."/".$homeRenewYear;
    }

    $PostArray["INSURANCE_START_DATE_FIELD"] = $Session["_DRIVERS_"]["0"]["date_of_insurance_start_mm"]."/".$Session["_DRIVERS_"]["0"]["date_of_insurance_start_dd"]."/".$Session["_DRIVERS_"]["0"]["date_of_insurance_start_yyyy"];
    $PostArray["QUOTE_DATE_FIELD"]           = date('m')."/".date('d')."/".date('Y');

    $useOfOtherVehicles                       = $Session["_DRIVERS_"]["0"]["use_of_other_vehicles"];
    $PostArray["USE_OF_OTHER_VEHICLES_FIELD"] = $_PROPOSER["use_of_other_vehicles"][$useOfOtherVehicles];

    $driversToBeInsured                       = $Session["_DRIVERS_"]["0"]["who_drive_the_vehicle"];
    $PostArray["DRIVERS_TO_BE_INSURED_FIELD"] = $_PROPOSER["who_drive_the_vehicle"][$driversToBeInsured];

    $employmentStatus                         = $Session["_DRIVERS_"]["0"]["employment_status"];
    $PostArray["EMPLOYMENT_STATUS_FIELD"]     = $_PROPOSER["employment_status"][$employmentStatus];

    $objOccupation->GetOccupation($Session["_DRIVERS_"]["0"]["occupation_list"],&$ftOccArrayResult);
    $objBusiness->GetBusiness($Session["_DRIVERS_"]["0"]["business_list"], &$ftBusArrayResult);

    $objOccupation->GetOccupation($Session["_DRIVERS_"]["0"]["occupation_list_pt"],&$ptOccArrayResult);
    $objBusiness->GetBusiness($Session["_DRIVERS_"]["0"]["business_list_pt"], &$ptBusArrayResult);

    $PostArray["FULL_TIME_OCCUPATION_FIELD"]   = $ftOccArrayResult["name"];
    $PostArray["FULL_TIME_BUSINESS_FIELD"]     = $ftBusArrayResult["name"];

    $PostArray["PART_TIME_OCCUPATION_FIELD"]   = $ptOccArrayResult["name"];
    $PostArray["PART_TIME_BUSINESS_FIELD"]     = $ptBusArrayResult["name"];

    $typeOfDriving                             = $Session["_DRIVERS_"]["0"]["type_of_driving"];
    $PostArray["DRIVING_LICENCE_TYPE_FIELD"]   = $_PROPOSER["type_of_driving"][$typeOfDriving];
    $passPluss                                 = $Session["_DRIVERS_"]["0"]["driver_pass_plus"];

    if($passPluss == "N")
	$PostArray["PASS_PLUS_FIELD"] = "No";
    if($passPluss == "Y")
	$PostArray["PASS_PLUS_FIELD"] = "Yes";

    $PostArray["LICENCE_DATE_FIELD"]  = $Session["_DRIVERS_"]["0"]["period_of_licence_held_mm"]."/".$Session["_DRIVERS_"]["0"]["period_of_licence_held_dd"]."/".$Session["_DRIVERS_"]["0"]["period_of_licence_held_yyyy"];

    $NrConv                                    = $objSession->GetSessionConvictionsDriverNumber($Session,0);
    $NrClaims                                  = $objSession->GetSessionClaimsDriverNumber($Session,0);

    if(! $NrConv)
	$NrConv = "0";
    if(! $NrClaims)
	$NrClaims = "0";

    $PostArray["NUMBER_OF_CLAIMS_FIELD"]        = $NrClaims;
    $PostArray["NUMBER_OF_CONVICTIONS_FIELD"]   = $NrConv;

    $coverType                                  = $Session["_COVER_"]["type_of_cover"];
    $PostArray["COVER_TYPE_FIELD"]              = $_COVER["type_of_cover"][$coverType];

    $typeOfUse                                  = $Session["_COVER_"]["type_of_use"];
    $PostArray["TYPE_OF_USE_FIELD"]             = $_COVER["type_of_use"][$typeOfUse];

    $PostArray["ANNUAL_MILEAGE_FIELD"]          = $Session["_COVER_"]["annual_mileage"];
    $PostArray["ANNUAL_BUSINESS_MILEAGE_FIELD"] = $Session["_COVER_"]["business_mileage"];
    $PostArray["VOLUNTARY_EXCESS_FIELD"]        = $Session["_COVER_"]["voluntary_excess"];
    $PostArray["YEAR_NCB_FIELD"]                = $Session["_COVER_"]["no_claims_bonus"];

    $mainUserOfVehicle                          = $Session["_COVER_"]["main_user_of_vehicle"];
    switch($mainUserOfVehicle)
    {
	case "1":
	  $mainUserText                    = $Session["_DRIVERS_"]["0"]["title"]." ".$Session["_DRIVERS_"]["0"]["first_name"]." ".$Session["_DRIVERS_"]["0"]["surname"];
	break;

	case "2":
	  $mainUserText                     = $Session["_DRIVERS_"]["1"]["additional_title"]." ".$Session["_DRIVERS_"]["1"]["additional_first_name"]." ".$Session["_DRIVERS_"]["1"]["additional_surname"];
	break;

	case "3":
	  $mainUserText                     = $Session["_DRIVERS_"]["2"]["additional_title"]." ".$Session["_DRIVERS_"]["2"]["additional_first_name"]." ".$Session["_DRIVERS_"]["2"]["additional_surname"];
	break;

	case "4":
	  $mainUserText                     = $Session["_DRIVERS_"]["3"]["additional_title"]." ".$Session["_DRIVERS_"]["3"]["additional_first_name"]." ".$Session["_DRIVERS_"]["3"]["additional_surname"];
	break;

    }
    $PostArray["MAIN_USER_FIELD"]     = $mainUserText;

    $ownerOfVehicle                        = $Session["_COVER_"]["owner_of_vehicle"];

    switch($ownerOfVehicle)
    {
	case "1":
	  $ownerOfVehicleText              = $Session["_DRIVERS_"]["0"]["title"]." ".$Session["_DRIVERS_"]["0"]["first_name"]." ".$Session["_DRIVERS_"]["0"]["surname"];
	break;

	case "2":
	  $ownerOfVehicleText              = $Session["_DRIVERS_"]["1"]["additional_title"]." ".$Session["_DRIVERS_"]["1"]["additional_first_name"]." ".$Session["_DRIVERS_"]["1"]["additional_surname"];
	break;

	case "3":
	  $ownerOfVehicleText              = "Parents";
	break;

	case "4":
	  $ownerOfVehicleText              = "Company";
	break;

	case "5":
	  $ownerOfVehicleText              = "Privately Leased";
	break;

	case "6":
	  $ownerOfVehicleText              = "Other";
	break;
    }
    $PostArray["OWNER_FIELD"]         = $ownerOfVehicleText;

    $registeredOfVehicle                   = $Session["_COVER_"]["registered_of_vehicle"];
    switch($ownerOfVehicle)
    {
	case "1":
	  $registeredOfVehicleText         = $Session["_DRIVERS_"]["0"]["title"]." ".$Session["_DRIVERS_"]["0"]["first_name"]." ".$Session["_DRIVERS_"]["0"]["surname"];
	break;

	case "2":
	  $registeredOfVehicleText         = $Session["_DRIVERS_"]["1"]["additional_title"]." ".$Session["_DRIVERS_"]["1"]["additional_first_name"]." ".$Session["_DRIVERS_"]["1"]["additional_surname"];
	break;

	case "3":
	  $registeredOfVehicleText         = "Parents";
	break;

	case "4":
	  $registeredOfVehicleText         = "Company";
	break;

	case "5":
	  $registeredOfVehicleText         = "Privately Leased";
	break;

	case "6":
	  $registeredOfVehicleText         = "Other";
	break;
    }

    $PostArray["REGISTERED_KEEPER_FIELD"]    = $registeredOfVehicleText;

    $PostArray["NUMBER_OF_OTHER_VEHICLES_FIELD"] = $Session["_COVER_"]["number_of_other_vehicles"];

    $sid = str_replace("_","",$filename);

    $PostArray["QUOTE_TYPE_FIELD"]               = "car";
    $PostArray["QUOTE_RESULTS_LINK_FIELD"]       = "https://car-insurance.quotezone.co.uk/car/index.php?sid=$sid";
    $PostArray["EMVADMIN1_FIELD"]                = $sid;

    // MoneyMaxim (ticket - XEV-793421)
    //Updated 23-03-2011 ticket id : ZTH-983371
    //EMV_TAG: AFAB1BE22E0100AF
    //EMV_REF: EdX7CqkmjGiK8SA9MKJPUB6mXj0IHd-2-Dqoe6g2WLGvK7s
    if($wlUserID == 136)
    {         
	$PostArray["emv_tag"]          = "AFAB1BE22E0100AF";
	$PostArray["emv_ref"]          = "EdX7CqkmjGiK8SA9MKJPUB6mXj0IHd-2-Dqoe6g2WLGvK7s";
	$PostArray["emv_campaignid"]   = "7887135";
	$PostArray["QUOTE_TYPE_FIELD"] = "moneymaxim car";
    }

    // Gogreencompare Car (ticket id : ZTH-983371)
    //Updated 23-03-2011 ticket id : ZTH-983371
    //EMV_TAG: C45C45C020001B2C
    //EMV_REF: EdX7CqkmjGiS8SA9MKJPUB6kLEkJGKiw-jrde6k3KsKqK4E
    if($wlUserID == 239)
    {         
	$PostArray["emv_tag"]        = "C45C45C020001B2C";
	$PostArray["emv_ref"]        = "EdX7CqkmjGiS8SA9MKJPUB6kLEkJGKiw-jrde6k3KsKqK4E";
	$PostArray["emv_campaignid"] = "6830658";
    }

    //saveitbudy 488
    //EMV_TAG: 931800800026F230 
    //EMV_REF: EdX7CqkmjF-G8SA9MKJPUB7eK01yHK3L-jjdea9AWsPZK9U 
    if($wlUserID == 488)
    {         
	$PostArray["emv_tag"]     = "931800800026F230";
	$PostArray["emv_ref"]     = "EdX7CqkmjF-G8SA9MKJPUB7eK01yHK3L-jjdea9AWsPZK9U";
    }

    //easyfundsraising.co.uk (easyf1) 517
    //EMV_TAG: 40000C71FA998C44 
    //EMV_REF: EdX7CqkmjLLe8SA9MKJPUB7TKEx6HN7E-06scqA-K8TdK_0 
    //EMVADMIN3_FIELD: memberID
    if($wlUserID == 517)
    {         
	$memberID                              = trim($Session["_QZ_QUOTE_DETAILS_"]["memberID"]);
	$PostArray["emv_tag"]                  = "40000C71FA998C44";
	$PostArray["emv_ref"]                  = "EdX7CqkmjLLe8SA9MKJPUB7TKEx6HN7E-06scqA-K8TdK_0";
	$PostArray["EMVADMIN3_FIELD"]          = $memberID;
	$PostArray["QUOTE_TYPE_FIELD"]         = "easyfundraising_car";
	$PostArray["QUOTE_RESULTS_LINK_FIELD"] = "http://www.easyfundraising.org.uk/compare-car-insurance/?qid=$sid&uid=$memberID";
    }

    //Powerseeker (Adrian Pitt) Car (ticket id : ZTH-983371)
    //Updated 23-03-2011 ticket id : ZTH-983371
    //EMV_TAG: 200007BEC71C45C0
    //EMV_REF: EdX7CqkmjGhi8SA9MKJPUB7VKEx6HKqxj0vaetoyXbPZK6w
    //wlid= 4198c5c727a8c42d07f4584d2e53e797 -> id 41
    if(($wlUserID == '15') || ($wlUserID == '18') || ($wlUserID == '35') || ($wlUserID == '41') || ($wlUserID == '42') || ($wlUserID == '102'))
    {         
	$PostArray["emv_tag"]        = "200007BEC71C45C0";
	$PostArray["emv_ref"]        = "EdX7CqkmjGhi8SA9MKJPUB7VKEx6HKqxj0vaetoyXbPZK6w";
    }

    if($wlUserID == 409)
    {
	$PostArray["emv_campaignid"]   = 8105580;
	$PostArray["QUOTE_TYPE_FIELD"] = "neil gee car";
	$PostArray["QUOTE_RESULTS_LINK_FIELD"] = "https://wl3.quotezone.co.uk/car/index.php?sid=$sid";
    }

    if ($wlUserID == 239)
    {
	//$PostArray["QUOTE_RESULTS_LINK_FIELD"]       = "http://www.gogreencompare.com/Retrieve.aspx?fn=$sid&type=car";
	$PostArray["QUOTE_RESULTS_LINK_FIELD"]       = "http://www.ukgreencompare.com/Retrieve.aspx?fn=$sid&type=car";
    }

    if (($wlUserID == 237 && $Session['RETRIEVE_QUOTES']['ISIS_NEW_QUOTE']) || $wlUserID == 272)
	$PostArray["QUOTE_RESULTS_LINK_FIELD"]       = "http://www.carinsurancemegastore.co.uk/retrieve.php?sid=$sid&type=car";

    if ($wlUserID == 279 && !$Session['RETRIEVE_QUOTES']['ISIS_NEW_QUOTE'])
	$PostArray["QUOTE_RESULTS_LINK_FIELD"]       = "https://wl3.quotezone.co.uk/car/index.php?isid=$sid&rerun=1";

    if ($wlUserID == 484)
	$PostArray["QUOTE_RESULTS_LINK_FIELD"]       = "http://carinsurance.saveyourselfmoney.com/?sid=$sid";

    if ($wlUserID == 432)
    {
	$memberID                              = trim($Session["_QZ_QUOTE_DETAILS_"]["memberID"]);
	$PostArray["emv_tag"]                  = "6202000076548170";
	$PostArray["emv_ref"]                  = "EdX7CqkmjLbC8SA9MKJPUB7RKkx4HK3D-j_bfq0-WcfZKyg";
	$PostArray["QUOTE_TYPE_FIELD"]         = "ilovecashback_car";
	$PostArray["QUOTE_RESULTS_LINK_FIELD"] = "https://www.ilovecashback.com/?qid=$sid&uid=$memberID";
    }

    //Pigsback 
    if ($wlUserID == 516)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "pigsback car";
	$PostArray["emv_tag"]          = "60064020000DDF98";
	$PostArray["emv_ref"]          = "EdX7CqkmjLyq8SA9MKJPUB7RKEx8GK3B-jjde91CLsnRK_k"; 
    }

    //payingtoomuch 
    if ($wlUserID == 496)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "PayingTooMuch_car";
	$PostArray["emv_tag"]          = "188412C046602188";
	$PostArray["emv_ref"]          = "EdX7CqkmjK9m8SA9MKJPUB7WIER-Ha-w-jzbfak0WcjRK7c"; 
    }

    //nhscashback 
    if ($wlUserID == 584)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "nhsCashback_car";
	$PostArray["emv_tag"]          = "268D033010000C09";
	$PostArray["emv_ref"]          = "EdX7CqkmjJ1C8SA9MKJPUB7VLkQOHK7A-jnde6k2K8DQKGw"; 
    }

    //mirrorcashback 
    if ($wlUserID == 586)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "mirrorCashback_car";
	$PostArray["emv_tag"]          = "CC04000006BFC34C";
	$PostArray["emv_ref"]          = "EdX7CqkmjJ0G8SA9MKJPUB6kW0x-HK3D-jjbCd9FW8SqKNI"; 
    }

    //froggybank 
    if ($wlUserID == 580)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "froggyBank_car";
	$PostArray["emv_tag"]          = "BCF40CC0400007E3";
	$PostArray["emv_ref"]          = "EdX7CqkmjJ0S8SA9MKJPUB6lWzp-HN6w-jzde6k2X7XaKPs"; 
    }

    //giveortake 
    if ($wlUserID == 587)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "giveOrtake_car";
	$PostArray["emv_tag"]          = "20000DE51E9A0660";
	$PostArray["emv_ref"]          = "EdX7CqkmjJ0R8SA9MKJPUB7VKEx6HNm2_zmoctg2XsbZKNY"; 
    }

    //psdiscount 
    if ($wlUserID == 585)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "psDiscount_car";
	$PostArray["emv_tag"]          = "100004FC9F4D0331";
	$PostArray["emv_ref"]          = "EdX7CqkmjJ0Q8SA9MKJPUB7WKEx6HKm1iTGrf902W8PYKNU"; 
    }

    //vacmedia 
    if ($wlUserID == 588)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "vacMedia_car";
	$PostArray["emv_tag"]          = "631D6330100007DA";
	$PostArray["emv_ref"]          = "EdX7CqkmjJ_68SA9MKJPUB7RK00OGq7A-jnde6k2X7SoK9I"; 
    }
    
    //topcashback 
    if ($wlUserID == 604)
    {
	$cheapestSiteID  = $cheappestQuoteArray['site_id'];
	$cheapestPremium = $cheappestQuoteArray['cheapest_quote'];

	// TO DO
	$cashbackAmount = CalculateCashBack($cheapestSiteID,$cheapestPremium,"","",$Session['CPA'][$cheapestSiteID],$wlUserID);

	$memberID                           = trim($Session["_QZ_QUOTE_DETAILS_"]["memberID"]);
	$PostArray["EMVADMIN3_FIELD"]       = $memberID;
	$PostArray["QUOTE_TYPE_FIELD"]      = "topCashBack_car";
	$PostArray["emv_tag"]               = "60200019479EBA06";
	$PostArray["emv_ref"]               = "EdX7CqkmjJzv8SA9MKJPUB7RKE56HK3C8zzactxEKcDfK9s"; 
	$PostArray["CASHBACK_AMOUNT_FIELD"] = $cashbackAmount;
	$PostArray["CASHBACK_TOTAL_FIELD"]  = $cheapestPremium-$cashbackAmount;
    }

    //leedscompare 
    if ($wlUserID == 608)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "lufc_car";
	$PostArray["emv_tag"]          = "8CC040000D6C5F7F";
	$PostArray["emv_ref"]          = "EdX7CqkmjIFG8SA9MKJPUB7fWz96GK3D-jipfdozLsevKDw"; 
    }

    //electric shopping 
    if ($wlUserID == 610)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "electricshopping_car";
	$PostArray["emv_tag"]          = "906BB04634010090";
	$PostArray["emv_ref"]          = "EdX7CqkmjITy8SA9MKJPUB7eKEoIbq3H_DvZe6g2WMnZK-w"; 
    }

    //andybiggsiar 
    if ($wlUserID == 633)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "carinsurancecomparison_car";
	$PostArray["emv_tag"]          = "80007FDFAED49A08";
	$PostArray["emv_ref"]          = "EdX7CqkmjImK8SA9MKJPUB7fKEx6G9u3jEmoD60_KcDRKAA"; 
    }

    //quoteyquotey 
    if ($wlUserID == 631)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "quoteyquotey_car";
	$PostArray["emv_tag"]          = "36E9F1AB4D004003";
	$PostArray["emv_ref"]          = "EdX7CqkmjImW8SA9MKJPUB7ULjlzaqyyiDype6kyWMDaKHs"; 
    }

    //msmithfreelance 
    if ($wlUserID == 632)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "cheapcarinsurance_car";
	$PostArray["emv_tag"]          = "57A8340100005665";
	$PostArray["emv_ref"]          = "EdX7CqkmjIn68SA9MKJPUB7SLz1yH6nD-zjde6kzXsbcK9w"; 
    }

    //msmithfreelance2 
    if ($wlUserID == 634)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "cheapvaninsurance_car";
	$PostArray["emv_tag"]          = "7B350100000FA2A2";
	$PostArray["emv_ref"]          = "EdX7CqkmjPD-8SA9MKJPUB7QWk9_HKzD-jjde99HWrHbK_w"; 
    }

    //cheap
    if ($wlUserID == 644)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "cheapcouk_car";
	$PostArray["emv_tag"]          = "D4040000BEDF4724";
	$PostArray["emv_ref"]          = "EdX7CqkmjPR18SA9MKJPUB6jLEx-HK3D-kqoD98yX8LdKC8"; 
    }

    //moneymedia
    if ($wlUserID == 601)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "moneymedia_car";
	$PostArray["emv_tag"]          = "26CEED3501000099";
	$PostArray["emv_ref"]          = "EdX7CqkmjPaW8SA9MKJPUB7VLj8PadnA_zjce6k2WMnQKHg"; 
    }

    //mycheapcar
    if ($wlUserID == 537)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "mycheapcar_car";
	$PostArray["emv_tag"]          = "4D6F0746C020001D";
	$PostArray["emv_ref"]          = "EdX7CqkmjP2W8SA9MKJPUB7TXEoMHKrH_Evdeak2WMGtKHg"; 
    }

    //bestbuymoney
    if ($wlUserID == 640)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "bestbuymoney_car";
	$PostArray["emv_tag"]          = "5D52D6E71A808005";
	$PostArray["emv_ref"]          = "EdX7CqkmjPQS8SA9MKJPUB7SXEl4aKu2_Tmsc6k-WMDcKI0"; 
    }

    //clickcompare
    if ($wlUserID == 643)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "clickcompare_car";
	$PostArray["emv_tag"]          = "20001CDFB32E66C0";
	$PostArray["emv_ref"]          = "EdX7CqkmjP9G8SA9MKJPUB7VKEx6Hd63jEreedwwXrPZKMo"; 
    }

    //cpcomparisons
    if ($wlUserID == 652)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "cpcomparisons_car";
	$PostArray["emv_tag"]          = "D9B0080005D49575";
	$PostArray["emv_ref"]          = "EdX7CqkmjOO68SA9MKJPUB6jIT56HKXD-jjYD60_XcfcKFc"; 
    }

    //uswitch
    if ($wlUserID == 658)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "uswitch_car";
	$PostArray["emv_tag"]          = "20000617984106E2";
	$PostArray["emv_ref"]          = "EdX7CqkmjOl68SA9MKJPUB7VKEx6HKvC_THVf6g2XrXbKHw"; 
    }

    //savoo
    if ($wlUserID == 661)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "savoo_car";
	$PostArray["emv_tag"]          = "3AA9B80800055A52";
	$PostArray["emv_ref"]          = "EdX7CqkmjO1W8SA9MKJPUB7UWT1zbqXD8jjde6wzKcXbKMU"; 
    }

    //moneyhelpline
    if ($wlUserID == 657)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "moneyhelpline_car";
	$PostArray["emv_tag"]          = "332857E10DC04003";
	$PostArray["emv_ref"]          = "EdX7CqkmjO928SA9MKJPUB7UK05yGaq2-zipCKkyWMDaKBU"; 
    }

    //carinscomp
    if ($wlUserID == 533)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "carinscomp_car";
	$PostArray["emv_tag"]          = "19B808000715845D";
	$PostArray["emv_ref"]          = "EdX7CqkmjNHF8SA9MKJPUB7WIT5yHKXD-jjaeqw-XMWtK6Q"; 
    }

    //maximiles
    if ($wlUserID == 666)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "maximiles_car";
	$PostArray["emv_tag"]          = "799BD7079C008007";
	$PostArray["emv_ref"]          = "EdX7CqkmjNe68SA9MKJPUB7QIUUIaKrD_TGue6k-WMDeKHk"; 
    }

    //heritage
    if ($wlUserID == 664)
    {
	$filename = $Session['TEMPORARY FILE NAME'];
	$heritageSQL = "SELECT * FROM logs l, quote_status qs WHERE l.id=qs.log_id AND site_id = '771' AND l.filename='$filename'";

	$dbh->Exec($heritageSQL, true);

	$dbh->FetchRows();

	$waitingQuotes = $dbh->GetFieldValue('status');

	if ($waitingQuotes == 'SUCCESS')
	{
	    $PostArray["QUOTE_TYPE_FIELD"] = "heritage_only_car";
	    $PostArray["emv_tag"]          = "E004000305E87974";
	    $PostArray["emv_ref"]          = "EdX7CqkmjNp98SA9MKJPUB6iKEx-HK3D-TjYDqExUcfdKBY";
	    $PostArray["QUOTE_RESULTS_LINK_FIELD"]       = "http://www.heritage-quote.co.uk/quotezone.cfm";
	}
	else
	{
	  $PostArray["QUOTE_TYPE_FIELD"] = "heritage_car";
	  $PostArray["emv_tag"]          = "40003203C7D74E04";
	  $PostArray["emv_ref"]          = "EdX7CqkmjNp88SA9MKJPUB7TKEx6H6_D-UvaD64yLcDdKBA";
	  $PostArray["QUOTE_RESULTS_LINK_FIELD"]       = "http://www.heritage-quote.co.uk/quotezone.cfm";
	}
    }

    //comparecomiar
    if ($wlUserID == 681)
    {
	$PostArray["QUOTE_TYPE_FIELD"] = "comparecomiar_car";
	$PostArray["emv_tag"]          = "10000DE4B28D8381";
	$PostArray["emv_ref"]          = "EdX7CqkmjMBu8SA9MKJPUB7WKEx6HNm2_krfc90-W8jYKCg"; 
    }
    
    $unsubscribeLink = CreateUnsubscribeLinkParams($Session["_DRIVERS_"]["0"]["email_address"],'top3','car')."&sid=$sid&type=1&action=unsubscribe&campaignId=".$PostArray["emv_campaignid"];
    $PostArray["UNSUBSCRIBE_LINK_FIELD"]          = "https://www.quotezone.co.uk/customer/unsubscribe.php?$unsubscribeLink";

    if($wlUserID)
      $PostArray["UNSUBSCRIBE_LINK_FIELD"] = "https://www.quotezone.co.uk/customer/wl-unsubscribe.php?emailaddress=".$Session["_DRIVERS_"]["0"]["email_address"];

    if($Session["_DRIVERS_"]["0"]["advanced_motorist"] == "Y")
	$PostArray["iam_member"]            = "Yes";
    if($Session["_DRIVERS_"]["0"]["advanced_motorist"] == "N")
	$PostArray["IAM_MEMBER_FIELD"]            = "No";
    $PostArray["QUOTE_TIME_FIELD"]               = date("Gis");
    if($Session["_VEHICLE_"]["vehicle_modified_from_manufacturer"] == "N")
	$PostArray["MODIFIED_FIELD"]      = "No";
    if($Session["_VEHICLE_"]["vehicle_modified_from_manufacturer"] == "Y")
	$PostArray["MODIFIED_FIELD"]      = "Yes";
    if($Session["_VEHICLE_"]["vehicle_grey_or_import"] != "N")
	$PostArray["IMPORT_FIELD"]      = "Yes";
    if($Session["_VEHICLE_"]["vehicle_grey_or_import"] == "N")
	$PostArray["IMPORT_FIELD"]      = "No";
    if($Session["_VEHICLE_"]["vehicle_side"] == "N")
	$PostArray["LEFT_RIGHT_FIELD"]          = "Right";
    if($Session["_VEHICLE_"]["vehicle_side"] == "Y")
	$PostArray["LEFT_RIGHT_FIELD"]          = "Left";
    $vehicleKept                           = $Session["_VEHICLE_"]["vehicle_kept"];
    $PostArray["KEPT_OVERNIGHT_FIELD"]             = $_VEHICLE["vehicle_kept"][$vehicleKept];
    if($Session["_VEHICLE_"]["vehicle_alarm"] == "N")
	$PostArray["ALARM_FIELD"]         = "No";
    if($Session["_VEHICLE_"]["vehicle_alarm"] == "Y")
	$PostArray["ALARM_FIELD"]         = "Yes";
    if($Session["_VEHICLE_"]["vehicle_immobiliser"] == "N")
	$PostArray["IMMOBILISER_FIELD"]         = "No";
    if($Session["_VEHICLE_"]["vehicle_immobiliser"] == "Y")
	$PostArray["IMMOBILISER_FIELD"]         = "Yes";
    if($Session["_VEHICLE_"]["vehicle_tracking_device"] == "N")
	$PostArray["TRACKER_FIELD"]         = "No";
    if($Session["_VEHICLE_"]["vehicle_tracking_device"] == "Y")
	$PostArray["TRACKER_FIELD"]         = "Yes";

    $PostArray["PURCHASED_FIELD"]          = "Yes";
    if($Session["_VEHICLE_"]["vehicle_purchased"] == "N")
	$PostArray["PURCHASED_FIELD"]          = "No";

    $vehBought                              = explode("/",$Session["_VEHICLE_"]["vehicle_bought"]);
    $PostArray["PURCHASE_DATE_FIELD"]       = $vehBought[1]."/".$vehBought[0]."/".$vehBought[2];
    $PostArray["ESTIMATED_VALUE_FIELD"]     = $Session["_VEHICLE_"]["estimated_value"];
    $PostArray["REGISTRATION_NUMBER_FIELD"] = strtoupper($Session["_VEHICLE_"]["vehicle_registration_number"]);

    if($Session["_COVER_"]["protect_bonus"] == "N")
	$PostArray["PROTECTED_NCB_FIELD"]            = "No";
    if($Session["_COVER_"]["protect_bonus"] == "Y")
	$PostArray["PROTECTED_NCB_FIELD"]            = "Yes";
    $PostArray["MOBILE_NUMBER_FIELD"]     = $Session["_DRIVERS_"][0]["mobile_telephone_prefix"]."".$Session["_DRIVERS_"][0]["mobile_telephone_number"];
    if(! $cheapeastQuotePrice)
	$cheapeastQuotePrice = "0";
    $PostArray["PREMIUM_FIELD"]              = $cheapeastQuotePrice;

    if($cheapeastQuotePrice == 0)
    {
	$PostArray["emv_tag"]          = "800052848AF01D80";
	$PostArray["emv_ref"]          = "EdX7CqkmjzyW8SA9MKJPUB7fKEx6Ga_L_jCsDak3LMjZKz4";
    }

    if($Session["_DRIVERS_"][0]["update_news"] == "on")
	$PostArray["OPT_IN_FIELD"] = "Yes";
    else
	$PostArray["OPT_IN_FIELD"] = "No";
    $PostArray["QUOTE_COMPLETED_FIELD"] = "Yes";
    $PostArray["QUOTE_REF_FIELD"]       = $Session["_QZ_QUOTE_DETAILS_"]["quote_reference"];

    // NEW FIELDS

    // set defaults
    // company name for top quote, this will be used to retrieve an image, so lower case and no spaces please
    $PostArray["COMPANY_FIELD"]          = '';

    // company name for 2nd quote, same as above
    $PostArray["COMPANY_2_FIELD"]        = '';

    // company name for 3rd quote, same as above
    $PostArray["COMPANY_3_FIELD"]        = '';

    // Annual premium of 2nd quote
    $PostArray["PREMIUM_2_FIELD"]        = '';

    // Annual premium of 3rd quote
    $PostArray["PREMIUM_3_FIELD"]        = '';

    // if top quote has "courtesy car" included - "yes","no" or "extra"
    $PostArray["QUOTE_DETAIL_A_FIELD"]   = '';

    // if 2nd quote has "courtesy car" included - "yes","no" or "extra"
    $PostArray["QUOTE_DETAIL_A_2_FIELD"] = '';

    // if 3rd quote has "courtesy car" included - "yes","no" or "extra"
    $PostArray["QUOTE_DETAIL_A_3_FIELD"] = '';

    // if top quote has "windscreen cover" included - "yes","no" or "extra"
    $PostArray["QUOTE_DETAIL_B_FIELD"]   = '';

    // if 2nd quote has "windscreen cover" included - "yes","no" or "extra"
    $PostArray["QUOTE_DETAIL_B_2_FIELD"] = '';

    // if 3rd quote has "windscreen cover" included - "yes","no" or "extra"
    $PostArray["QUOTE_DETAIL_B_3_FIELD"] = '';

    // if top quote has "personal accident" included - "yes","no" or "extra"
    $PostArray["QUOTE_DETAIL_C_FIELD"]   = '';

    // if 2nd quote has "personal accident" included - "yes","no" or "extra"
    $PostArray["QUOTE_DETAIL_C_2_FIELD"] = '';

    // if 3rd quote has "personal accident" included - "yes","no" or "extra"
    $PostArray["QUOTE_DETAIL_C_3_FIELD"] = '';

    // buyOnline link for top quote
    $PostArray["QUOTE_DETAIL_D_FIELD"]   = '';

    // buyOnline link for 2nd quote
    $PostArray["QUOTE_DETAIL_D_2_FIELD"] = '';

    // buyOnline link for 3rd quote
    $PostArray["QUOTE_DETAIL_D_3_FIELD"] = '';

    $PostArray["CONVICTION_TYPE_FIELD"] = '';
    $PostArray["CONVICTION_DATE_FIELD"] = '';

    // look for the first speeding related conviction, eg "SP..." if none, then just the first conviction

    if(count($Session["_CONVICTIONS_"]) > 0)
    {
	$nrOfDrivers = count($Session["_DRIVERS_"]);

	for($i=0;$i<$nrOfDrivers;$i++)
	{
	  $nrOfConv = count($Session["_CONVICTIONS_"][$i]);

	  if($nrOfConv > 0)
	  {
	      $PostArray["CONVICTION_TYPE_FIELD"] = $Session["_CONVICTIONS_"][$i][0]["offence_code"];
	      $PostArray["CONVICTION_DATE_FIELD"] = $Session["_CONVICTIONS_"][$i][0]["date_of_conviction_mm"]."/".$Session["_CONVICTIONS_"][$i][0]["date_of_conviction_dd"]."/".$Session["_CONVICTIONS_"][$i][0]["date_of_conviction_yyyy"];

	      for($x=0;$x<$nrOfConv;$x++)
	      {
		if( preg_match("/SP/i",$Session["_CONVICTIONS_"][$i][$x]["offence_code"]))
		{
		    $PostArray["CONVICTION_TYPE_FIELD"] = $Session["_CONVICTIONS_"][$i][$x]["offence_code"];
		    $PostArray["CONVICTION_DATE_FIELD"] = $Session["_CONVICTIONS_"][$i][$x]["date_of_conviction_mm"]."/".$Session["_CONVICTIONS_"][$i][$x]["date_of_conviction_dd"]."/".$Session["_CONVICTIONS_"][$i][$x]["date_of_conviction_yyyy"];

		    continue;
		}
	      }
	  }
	}
    }

    $topQuoteArray = array();

    // get top quotes
    if( $topQuoteArray = $objQuoteDetails->GetCheapestTopFiveSitesQuoteDetails($logID, 3))
    {
	$_SESSION                = $Session;
	$cheappestSiteQuoteArray = array();

	for($k=1;$k<=3;$k++)
	{
	  $topPremium          = '';
	  $topSiteDetailsArray = array();
	  $_resSite            = array();
	  $courtesy            = '';
	  $windscreen          = '';
	  $accident            = '';

	  $topSiteID           = $topQuoteArray[$k];
	  $topSiteDetailsArray = $objQuoteDetails->GetCheapestSiteQuoteDetails($logID,$topSiteID);

	  if($k == 1)
	  {
	      // get company name
	      $siteFileName = $Session["SHOW_SITES"][$topSiteID];
	      //include_once "/home/www/quotezone.co.uk/insurance-new/car/sites/showsites/$siteFileName";
	      // RAMDRIVE
	      include_once "../car/sites/showsites/$siteFileName";

	      //$PostArray["COMPANY_FIELD"] = strtolower(str_replace(" ","",$_resSite['brokerFullName']));
	      $PostArray["COMPANY_FIELD"] = "https://car-insurance.quotezone.co.uk/car/".$_resSite['imageSiteRemote'];

	      // if top quote has "courtesy car" included - "yes","no" or "extra"
	      if(preg_match("/Courtesy car included/i",$_resSite['_COURTESY_']))
		$courtesy = "yes";
	      elseif(preg_match("/Courtesy car not included/i",$_resSite['_COURTESY_']))
		$courtesy = "no";
	      elseif(preg_match("/Available for an extra/i",$_resSite['_COURTESY_']))
		$courtesy = "extra";
	      elseif(preg_match("/Check with insurer/i",$_resSite['_COURTESY_']))
		$courtesy = "check";

	      $PostArray["QUOTE_DETAIL_A_FIELD"]   = $courtesy;

	      //if top quote has "windscreen cover" included - "yes","no" or "extra"
	      if(preg_match("/Windscreen included/i",$_resSite['_WINDSCREEN_']))
		$windscreen = "yes";
	      elseif(preg_match("/Windscreen not included/i",$_resSite['_WINDSCREEN_']))
		$windscreen = "no";
	      elseif(preg_match("/Available for an extra/i",$_resSite['_WINDSCREEN_']))
		$windscreen = "extra";
	      elseif(preg_match("/Check with insurer/i",$_resSite['_WINDSCREEN_']))
		$windscreen = "check";

	      $PostArray["QUOTE_DETAIL_B_FIELD"]   = $windscreen;

	      //if top quote has "personal accident" included - "yes","no" or "extra"
	      if(preg_match("/Personal accident included/i",$_resSite['_ACCIDENT_']))
		$accident = "yes";
	      elseif(preg_match("/Personal accident not included/i",$_resSite['_ACCIDENT_']))
		$accident = "no";
	      elseif(preg_match("/Available for an extra/i",$_resSite['_ACCIDENT_']))
		$accident = "extra";
	      elseif(preg_match("/Check with insurer/i",$_resSite['_ACCIDENT_']))
		$accident = "check";

	      $PostArray["QUOTE_DETAIL_C_FIELD"]   = $accident;

	      // buy online link
	      $buyOnlineLink = '';
	      if($_resSite['proceedBuyOnlineLink'])
		$buyOnlineLink = $_resSite['proceedBuyOnlineLink'];
	      elseif($_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0])
		$buyOnlineLink = $_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0];

	      if($buyOnlineLink)
	      {
		$PostArray["QUOTE_DETAIL_D_FIELD"]   = str_replace("sites/clickIn.php?","",$buyOnlineLink);

		if(preg_match("/quote_ref\.value/i",$PostArray["QUOTE_DETAIL_D_FIELD"]))
		{
		    // extract quote_ref from out file
		    //include_once("/home/www/quotezone.co.uk/insurance-new/car/steps/functions.php");

		    // RAMDRIVE
		    include_once("../car/steps/functions.php");
		    if($openResultFile = OpenResultFile($topSiteID))
		    {
		      $quote_ref = $openResultFile[0]["quote reference"];

		      if($quote_ref)
			  $PostArray["QUOTE_DETAIL_D_FIELD"] = str_replace("quote_ref.value",$quote_ref,$PostArray["QUOTE_DETAIL_D_FIELD"]);
		    }
		}
	      }


	  }

	  if($k == 2)
	  {
	      // get company name
	      $siteFileName = $Session["SHOW_SITES"][$topSiteID];
	      //include_once "/home/www/quotezone.co.uk/insurance-new/car/sites/showsites/$siteFileName";

	      // RAMDRIVE
	      include_once "../car/sites/showsites/$siteFileName";

	      // company premium
	      $topPremium  = $topSiteDetailsArray["cheapest_quote"];

	      //$PostArray["COMPANY_2_FIELD"] = strtolower(str_replace(" ","",$_resSite['brokerFullName']));
	      $PostArray["COMPANY_2_FIELD"] = "https://car-insurance.quotezone.co.uk/car/".$_resSite['imageSiteRemote'];

	      $PostArray["PREMIUM_2_FIELD"] = $topPremium;

	      // if top quote has "courtesy car" included - "yes","no" or "extra"
	      if(preg_match("/Courtesy car included/i",$_resSite['_COURTESY_']))
		$courtesy = "yes";
	      elseif(preg_match("/Courtesy car not included/i",$_resSite['_COURTESY_']))
		$courtesy = "no";
	      elseif(preg_match("/Available for an extra/i",$_resSite['_COURTESY_']))
		$courtesy = "extra";
	      elseif(preg_match("/Check with insurer/i",$_resSite['_COURTESY_']))
		$courtesy = "check";

	      $PostArray["QUOTE_DETAIL_A_2_FIELD"]   = $courtesy;

	      //if top quote has "windscreen cover" included - "yes","no" or "extra"
	      if(preg_match("/Windscreen included/i",$_resSite['_WINDSCREEN_']))
		$windscreen = "yes";
	      elseif(preg_match("/Windscreen not included/i",$_resSite['_WINDSCREEN_']))
		$windscreen = "no";
	      elseif(preg_match("/Available for an extra/i",$_resSite['_WINDSCREEN_']))
		$windscreen = "extra";
	      elseif(preg_match("/Check with insurer/i",$_resSite['_WINDSCREEN_']))
		$windscreen = "check";

	      $PostArray["QUOTE_DETAIL_B_2_FIELD"]   = $windscreen;

	      //if top quote has "personal accident" included - "yes","no" or "extra"
	      if(preg_match("/Personal accident included/i",$_resSite['_ACCIDENT_']))
		$accident = "yes";
	      elseif(preg_match("/Personal accident not included/i",$_resSite['_ACCIDENT_']))
		$accident = "no";
	      elseif(preg_match("/Available for an extra/i",$_resSite['_ACCIDENT_']))
		$accident = "extra";
	      elseif(preg_match("/Check with insurer/i",$_resSite['_ACCIDENT_']))
		$accident = "check";

	      $PostArray["QUOTE_DETAIL_C_2_FIELD"]   = $accident;

	      // buy online link
	      $buyOnlineLink = '';
	      if($_resSite['proceedBuyOnlineLink'])
		$buyOnlineLink = $_resSite['proceedBuyOnlineLink'];
	      elseif($_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0])
		$buyOnlineLink = $_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0];


	      if($buyOnlineLink)
	      {
		$PostArray["QUOTE_DETAIL_D_2_FIELD"]   = str_replace("sites/clickIn.php?","",$buyOnlineLink);

		if(preg_match("/quote_ref\.value/i",$PostArray["QUOTE_DETAIL_D_2_FIELD"]))
		{
		    // extract quote_ref from out file
		    //include_once("/home/www/quotezone.co.uk/insurance-new/car/steps/functions.php");

		    // RAMDRIVE
		    include_once("../car/steps/functions.php");
		    if($openResultFile = OpenResultFile($topSiteID))
		    {
		      $quote_ref = $openResultFile[0]["quote reference"];

		      if($quote_ref)
			  $PostArray["QUOTE_DETAIL_D_2_FIELD"] = str_replace("quote_ref.value",$quote_ref,$PostArray["QUOTE_DETAIL_D_2_FIELD"]);
		    }
		}
	      }

	  }

	  if($k == 3)
	  {


	      // get company name
	      $siteFileName = $Session["SHOW_SITES"][$topSiteID];
	      //include_once "/home/www/quotezone.co.uk/insurance-new/car/sites/showsites/$siteFileName";

	      // RAMDRIVE
	      include_once "../car/sites/showsites/$siteFileName";

	      // company premium
	      $topPremium  = $topSiteDetailsArray["cheapest_quote"];

	      //$PostArray["COMPANY_3_FIELD"] = strtolower(str_replace(" ","",$_resSite['brokerFullName']));
	      $PostArray["COMPANY_3_FIELD"] = "https://car-insurance.quotezone.co.uk/car/".$_resSite['imageSiteRemote'];

	      $PostArray["PREMIUM_3_FIELD"] = $topPremium;

	      // if top quote has "courtesy car" included - "yes","no" or "extra"
	      if(preg_match("/Courtesy car included/i",$_resSite['_COURTESY_']))
		$courtesy = "yes";
	      elseif(preg_match("/Courtesy car not included/i",$_resSite['_COURTESY_']))
		$courtesy = "no";
	      elseif(preg_match("/Available for an extra/i",$_resSite['_COURTESY_']))
		$courtesy = "extra";
	      elseif(preg_match("/Check with insurer/i",$_resSite['_COURTESY_']))
		$courtesy = "check";

	      $PostArray["QUOTE_DETAIL_A_3_FIELD"]   = $courtesy;

	      //if top quote has "windscreen cover" included - "yes","no" or "extra"
	      if(preg_match("/Windscreen included/i",$_resSite['_WINDSCREEN_']))
		$windscreen = "yes";
	      elseif(preg_match("/Windscreen not included/i",$_resSite['_WINDSCREEN_']))
		$windscreen = "no";
	      elseif(preg_match("/Available for an extra/i",$_resSite['_WINDSCREEN_']))
		$windscreen = "extra";
	      elseif(preg_match("/Check with insurer/i",$_resSite['_WINDSCREEN_']))
		$windscreen = "check";

	      $PostArray["QUOTE_DETAIL_B_3_FIELD"]   = $windscreen;

	      //if top quote has "personal accident" included - "yes","no" or "extra"
	      if(preg_match("/Personal accident included/i",$_resSite['_ACCIDENT_']))
		$accident = "yes";
	      elseif(preg_match("/Personal accident not included/i",$_resSite['_ACCIDENT_']))
		$accident = "no";
	      elseif(preg_match("/Available for an extra/i",$_resSite['_ACCIDENT_']))
		$accident = "extra";
	      elseif(preg_match("/Check with insurer/i",$_resSite['_ACCIDENT_']))
		$accident = "check";

	      $PostArray["QUOTE_DETAIL_C_3_FIELD"]   = $accident;

	      // buy online link
	      $buyOnlineLink = '';
	      if($_resSite['proceedBuyOnlineLink'])
		$buyOnlineLink = $_resSite['proceedBuyOnlineLink'];
	      elseif($_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0])
		$buyOnlineLink = $_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0];

	      if($buyOnlineLink)
	      {
		$PostArray["QUOTE_DETAIL_D_3_FIELD"]   = str_replace("sites/clickIn.php?","",$buyOnlineLink);

		if(preg_match("/quote_ref\.value/i",$PostArray["QUOTE_DETAIL_D_3_FIELD"]))
		{
		    // extract quote_ref from out file
		    //include_once("/home/www/quotezone.co.uk/insurance-new/car/steps/functions.php");

		    // RAMDRIVE
		    include_once("../car/steps/functions.php");
		    if($openResultFile = OpenResultFile($topSiteID))
		    {
		      $quote_ref = $openResultFile[0]["quote reference"];

		      if($quote_ref)
			  $PostArray["QUOTE_DETAIL_D_3_FIELD"] = str_replace("quote_ref.value",$quote_ref,$PostArray["QUOTE_DETAIL_D_3_FIELD"]);
		    }
		}
	      }


	  }
	}//for

    }// if get top quotes

    // NEW FIELDS 

    // END 
    $string = "";

    $allArray[] = $PostArray;
    foreach($PostArray as $key=>$val)
    {
	$string .= $key."=".urlencode($val)."&";
    }

    $string = rtrim($string, "&");

    //print $string."\n";

}

//print_r($allArray);

foreach($allArray as $id => $value)
{
   foreach($value as $name => $value1)
      $finalArray[$name][$id] = $value1;
}

$c = 0;
foreach($finalArray as $filedName => $fieldValues)
{
   $c++;
   print($filedName.",");
}

print("\n");

for($i=1;$i<$c;$i++)
{
   foreach($finalArray as $filedName => $fieldValues)
   {
      print($fieldValues[$i].",");
   }
   print("\n");
}


//////////////////////////////////////////////////////////////////////////////FB
// [FUNCTION NAME]: GenerateMimeBoundary
//
// [DESCRIPTION]:   Generates a MIME boundary string, outlook express like
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the MIME boundary string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GenerateMimeBoundary()
{
  $garbleText = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','R','S','T','U','V','X','Y','W','Z');

  $maxIndex = count($garbleText) - 1;
  $mimeBoundary = "----=_NextPart_";

  mt_srand(make_seed());
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= "_";

  mt_srand(make_seed());
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= "_";

  mt_srand(make_seed());
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= ".";

  mt_srand(make_seed());
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
  $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];

  return $mimeBoundary;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: make_seed
//
// [DESCRIPTION]:   generate random string
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  return string
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function make_seed()
{
  list($usec, $sec) = explode(' ', microtime());
  return (float) $sec + ((float) $usec * 100000000);
}

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CreateUnsubscribeLinkParams($email,$top,$systemType)
   //
   // [DESCRIPTION]:   creates the users unsubscribe link
   //
   // [PARAMETERS]:    $email,$top,$systemType
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Florin Menghea (florin@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CreateUnsubscribeLinkParams($email,$top,$systemType)
   {
      if(empty($email))
      {
         //$this->strERR .= GetErrorString("INVALID_EMAIL_OUTBOUNDS_ENGINE_EMAIL");
         return false;
      }

      if(empty($top))
      {
         //$this->strERR .= GetErrorString("INVALID_EMAIL_OUTBOUNDS_ENGINE_EMAIL_PARAMS");
         return false;
      }

      if(empty($systemType))
      {
         //$this->strERR .= GetErrorString("INVALID_EMAIL_OUTBOUNDS_ENGINE_QUOTE_PARAMS");
         return false;
      }

      $authcode   = GenerateAuthenticityCode(str_replace(";",".",$top)."@".str_replace(";",".",$systemType));
      $emauthcode = GenerateAuthenticityCode($email);

      $resultString = "emailaddress=$email&emauthcode=$emauthcode&email=$top&quote=$systemType&authcode=$authcode";

      return $resultString;
   }
   
      //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GenerateAuthenticityCode
   //
   // [DESCRIPTION]:   generate auth code
   //
   // [PARAMETERS]:    $email
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Florin Menghea (florin@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GenerateAuthenticityCode($email)
   {
      $asciiCodes = array(
         "42" => "-",
         "46" => ".",
         "48" => "0",
         "49" => "1",
         "50" => "2",
         "51" => "3",
         "52" => "4",
         "53" => "5",
         "54" => "6",
         "55" => "7",
         "56" => "8",
         "57" => "9",
         "64" => "@",
         "65" => "A",
         "66" => "B",
         "67" => "C",
         "68" => "D",
         "69" => "E",
         "70" => "F",
         "71" => "G",
         "72" => "H",
         "73" => "I",
         "74" => "J",
         "75" => "K",
         "76" => "L",
         "77" => "M",
         "78" => "N",
         "79" => "O",
         "80" => "P",
         "81" => "Q",
         "82" => "R",
         "83" => "S",
         "84" => "T",
         "85" => "U",
         "86" => "V",
         "87" => "W",
         "88" => "X",
         "89" => "Y",
         "90" => "Z",
         "95" => "_",
         "97" => "a",
         "98" => "b",
         "99" => "c",
         "100" => "d",
         "101" => "e",
         "102" => "f",
         "103" => "g",
         "104" => "h",
         "105" => "i",
         "106" => "j",
         "107" => "k",
         "108" => "l",
         "109" => "m",
         "110" => "n",
         "111" => "o",
         "112" => "p",
         "113" => "q",
         "114" => "r",
         "115" => "s",
         "116" => "t",
         "117" => "u",
         "118" => "v",
         "119" => "w",
         "120" => "x",
         "121" => "y",
         "122" => "z",
      );

    $ourCodes = array(
         "01" => "A",
         "02" => "B",
         "03" => "C",
         "04" => "D",
         "05" => "E",
         "06" => "F",
         "07" => "G",
         "08" => "H",
         "09" => "I",
         "10" => "J",
         "11" => "K",
         "12" => "L",
         "13" => "M",
         "14" => "N",
         "15" => "O",
         "16" => "P",
         "17" => "Q",
         "18" => "R",
         "19" => "S",
         "20" => "T",
         "21" => "U",
         "22" => "V",
         "23" => "W",
         "24" => "X",
         "25" => "Y",
         "26" => "Z",
         "27" => "a",
         "28" => "b",
         "29" => "c",
         "30" => "d",
         "31" => "e",
         "32" => "f",
         "33" => "g",
         "34" => "h",
         "35" => "i",
         "36" => "j",
         "37" => "k",
         "38" => "l",
         "39" => "m",
         "40" => "n",
         "41" => "o",
         "42" => "p",
         "43" => "q",
         "44" => "r",
         "45" => "s",
         "46" => "t",
         "47" => "u",
         "48" => "v",
         "49" => "w",
         "50" => "x",
         "51" => "y",
         "52" => "z",
         "53" => "0",
         "54" => "1",
         "55" => "2",
         "56" => "3",
         "57" => "4",
         "58" => "5",
         "59" => "6",
         "60" => "7",
         "61" => "8",
         "62" => "9",
         "63" => "A",
         "64" => "B",
         "65" => "C",
         "66" => "D",
         "67" => "E",
         "68" => "F",
         "69" => "G",
         "70" => "H",
         "71" => "I",
         "72" => "J",
         "73" => "K",
         "74" => "L",
         "75" => "M",
         "76" => "N",
         "77" => "O",
         "78" => "P",
         "79" => "Q",
         "80" => "R",
         "81" => "S",
         "82" => "T",
         "83" => "U",
         "84" => "V",
         "85" => "W",
         "86" => "X",
         "87" => "Y",
         "88" => "Z",
         "89" => "a",
         "90" => "b",
         "91" => "c",
         "92" => "d",
         "93" => "e",
         "94" => "f",
         "95" => "g",
         "96" => "h",
         "97" => "i",
         "98" => "j",
         "99" => "k",
      );

   $invAsciiCodes = array_flip($asciiCodes);

   // email conversion

   if (!$email)
      $email = "test@test.com";

      $splitEmail = preg_split('//', $email, -1, PREG_SPLIT_NO_EMPTY);
      $encodedMail = '';
      foreach ($splitEmail as $key=>$char)
      {
         $encodedMail .= $invAsciiCodes[$char];
      }

      $password = "quotezone";

      $splitPassw = preg_split('//', $password, -1, PREG_SPLIT_NO_EMPTY);

      $encodedPassw = "";

      foreach ($splitPassw as $key=>$char)
      {
         $encodedPassw .= $invAsciiCodes[$char];
      }

      $splitEncEmail = preg_split('//', $encodedMail, -1, PREG_SPLIT_NO_EMPTY);
      $splitEncPassw = preg_split('//', $encodedPassw, -1, PREG_SPLIT_NO_EMPTY);

      $intercalation1st = "";

      for ($i=0; $i<count($splitEncEmail); $i++)
      {
         if (array_key_exists($i, $splitEncEmail))
            $intercalation1st .= $splitEncEmail[$i];

         if (array_key_exists($i, $splitEncPassw))
            $intercalation1st .= $splitEncPassw[$i];
      }
      $splitInterc1st = preg_split('//', $intercalation1st, -1, PREG_SPLIT_NO_EMPTY);

      $emailParts = explode("@",$email);
      $nrToMultiply = strlen($emailParts[0]);

      $interc1final = "";

      $counter = 1;

      foreach ($splitInterc1st as $key=>$value)
      {
         $interc1final .= $value * $counter;

         $counter++;
         if ($counter>$nrToMultiply)
            $counter = 1;
      }
      $splitInterc1 = preg_split('//', $interc1final, -1, PREG_SPLIT_NO_EMPTY);

      $intercalation2nd = '';

      for ($i=0; $i<count($splitInterc1); $i++)
      {
         if (array_key_exists($i, $splitInterc1))
            $intercalation2nd .= $splitInterc1[$i];

         if (array_key_exists($i, $splitEncPassw))
            $intercalation2nd .= $splitEncPassw[$i];
      }
      $emailParts1 = explode(".", $emailParts[1]);

      $nrToMultiply2 = '';

      for ($i=0; $i<count($emailParts1)-1;$i++)
      {
         $nrToMultiply2 += strlen($emailParts1[$i]);
      }

      $interc2final = "";

      $splitInterc2nd = preg_split('//', $intercalation2nd, -1, PREG_SPLIT_NO_EMPTY);

      $counter = 1;

      foreach ($splitInterc2nd as $key=>$value)
      {
         $interc2final .= $value * $counter;

         $counter++;
         if ($counter>$nrToMultiply2)
            $counter = 1;
      }
      $finalArray = preg_split('//', $interc2final, -1, PREG_SPLIT_NO_EMPTY);
      $resultString = "";
      $counter = count($finalArray)-1;

      for ($i=0;$i<(count($finalArray)/2)-1; $i++)
      {
         $index = $finalArray[$counter].$finalArray[$counter-1];
         $resultString .= $ourCodes[$index];
         $counter-=2;
      }

      return $resultString;

   }
   
?>

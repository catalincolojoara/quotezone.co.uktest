<?php
/*****************************************************************************/
/*                                                                           */
/*  CBusiness class interface                                                 */
/*                                                                           */
/*  (C) 2004 ISTVNACSEK Gabriel (jkl@acrux.biz)                              */
/*                                                                           */
/*****************************************************************************/
//                MySQL table definitions

define("SESSION_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";
define("SESSION_INCLUDED", "1");

//if(DEBUG_MODE)
//   error_reporting(1);
//else
//   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CBusiness
//
// [DESCRIPTION]:  CBusiness class interface
//
// [FUNCTIONS]:    
//                 int  function AddBusiness($paramName='')
//                 bool function UpdateBusiness($paramBusinessID=0, $paramName='')
//                 bool function DeleteBusiness($paramBusinessID=0)
//                 bool function GetBusiness($paramBusinessID=0, &$arrayResult)
//                 bool GetAllBusinesss(&$arrayResult)
// 
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Gabriel ISTVNACSEK (jkl@acrux.biz) 2004-07-07
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CBusiness
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last url error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CUrl
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CBusiness($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddBusiness
//
// [DESCRIPTION]:   Add new entry to the EMP_CODES table
//
// [PARAMETERS]:    $paramName=''
//
// [RETURN VALUE]:  ParamID or 0 in case of failure
//
// [CREATED BY]:    Gabi ISTVNACSEK (jkl@acrux.biz) 2004-09-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddBusiness($id="", $name="")
{

   if(empty($id))
   {
      $this->strERR = GetErrorString("INVALID_ID_BUSINESS_FIELD");
      return false;
   }

   if(empty($name))
   {
      $this->strERR = GetErrorString("INVALID_NAME_BUSINESS_FIELD");
      return false;
   }
   
   // check if param session name already exists
   $this->lastSQLCMD = "SELECT id,name FROM ".SQL_BUSINESS_CODES." WHERE id='$id' OR name='$name'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
      return false;
   }

   if($this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("PARAM_ID_OR_NAME_BUSINESS_ALREADY_EXISTS");
      return false;
   }
   // insert param session name into EMP_CODES table
   $this->lastSQLCMD = "INSERT INTO ".SQL_BUSINESS_CODES." (id,name) VALUES ('$id','$name')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateBusiness
//
// [DESCRIPTION]:   Update EMP_CODES table
//
// [PARAMETERS]:    $paramBusinessID=0, $paramName=''
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi ISTVNACSEK (jkl@acrux.biz) 2004-11-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateBusiness($id='', $name='')
{

   if(empty($id))
   {
      $this->strERR = GetErrorString("INVALID_ID_BUSINESS_FIELD");
      return false;
   }

   if(empty($name))
   {
      $this->strERR = GetErrorString("INVALID_NAME_BUSINESS_FIELD");
      return false;
   }

   // check if paramBusinessID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_BUSINESS_CODES." WHERE id='$paramBusinessID'";
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   // check if param session name already exists
   $this->lastSQLCMD = "SELECT id,name FROM ".SQL_BUSINESS_CODES." WHERE id='$id' OR name='$name'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("PARAM_ID_OR_NAME_BUSINESS_NOT_EXISTS");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_BUSINESS_CODES." SET name='$name' WHERE id='$id'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteBusiness
//
// [DESCRIPTION]:   Delete an entry from EMP_CODES table
//
// [PARAMETERS]:    $paramBusinessID=0
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi Istvnacsek (jkl@acrux.biz) 2004-11-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteBusiness($id='')
{
   if(empty($id))
   {
      $this->strERR = GetErrorString("INVALID_ID_BUSINESS_FIELD");
      return false;
   }

   // check if paramBusinessID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_BUSINESS_CODES." WHERE id='$id'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("ID_BUSINESS_NOT_EXIST");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_BUSINESS_CODES." WHERE id='$id'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetBusiness
//
// [DESCRIPTION]:   Read data from EMP_CODES table and put it into an array variable
//
// [PARAMETERS]:    $paramBusinessID=0, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi ISTVNACSEK (jkl@acrux.biz) 2004-11-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetBusiness($id='', &$arrayResult)
{
   if(empty($id))
   {
      $this->strERR = GetErrorString("INVALID_ID_BUSINESS_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_BUSINESS_CODES." WHERE id='$id'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("ID_BUSINESS_NOT_EXIST");
      return false;
   }

   $arrayResult["id"]     = $this->dbh->GetFieldValue("id");
   $arrayResult["name"]   = $this->dbh->GetFieldValue("name");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllBusinesss
//
// [DESCRIPTION]:   Read data from EMP_CODES table and put it into an array variable
//                  key = paramBusinessID, value = paramBusinessName
//
// [PARAMETERS]:    &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi ISTVNACSEK (gabi@acrux.biz) 2004-11-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllBusiness(&$arrayResult)
{
   $this->lastSQLCMD = "SELECT id,name FROM ".SQL_BUSINESS_CODES."";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("ID_BUSINESS_NOT_EXIST");
      return false;
   }

   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("name");

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllBusinesssByName
//
// [DESCRIPTION]:   Read data from EMP_CODES table and put it into an array variable
//                  key = occupationID, value = occupationName
//
// [PARAMETERS]:    &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi ISTVNACSEK (gabi@acrux.biz) 2004-11-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllBusinessByName($name='', &$arrayResult)
{
   
   if(empty($name))
   {
      $this->strERR = GetErrorString("INVALID_NAME_BUSINESS_FIELD");
      return false;
   }

   $nameOccExplode = explode(" ",$name);
   
   foreach($nameOccExplode as $nameKey => $nameValue)
   {   
   
      if(strlen($nameValue) < 3)
      {
         $this->strERR = GetErrorString("INVALID_KEY_BUSINESS_LENGTH_sFIELD");
         //return false;
         continue;
      }
      
      if(strlen($nameValue) > 3)
         $this->lastSQLCMD = "SELECT id,name FROM ".SQL_BUSINESS_CODES." WHERE name LIKE '%".substr($nameValue,0,strlen($nameValue)-1)."%'";
      else
         $this->lastSQLCMD = "SELECT id,name FROM ".SQL_BUSINESS_CODES." WHERE name LIKE '%".$nameValue."%'";
   
      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
      $this->strERR = $this->dbh->GetError();
      return false;
      }
   
      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("ID_BUSINESS_NOT_EXIST");
         continue;
      }
   
      
      while($this->dbh->MoveNext())
         $arrayResultTemp[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("name");
   
      foreach($arrayResultTemp as $key => $value)
      {
         $valueExplode = explode(" ", $value);
         foreach($valueExplode as $k => $v)
         {
            if(preg_match("/^".substr($nameValue,0,strlen($nameValue)-1).".*$/i",$v))
            {
               $arrayResult[$key] = $value;
               break;
            }
         }
      }
   }
   
   if(! count($arrayResult))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-11-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2004-11-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

} // end CBusiness
?>

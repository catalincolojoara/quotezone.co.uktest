<?php
/*****************************************************************************/
/*                                                                           */
/*  EmailOutboundsEngineVan class interface                                     */
/*                                                                           */
/*  (C) 2008 Gabriel ISTVANCSEK (gabi@acrux.biz)                             */
/*                                                                           */
/*****************************************************************************/
include_once "errors.inc";
include_once "MySQL.php";
include_once "EmailStore.php";
include_once "EmailReminder.php";
include_once "EmailStoreCodes.php";
include_once "QuoteDetails.php";
include_once "VanVehicle.php";
include_once "Occupation.php";
include_once "Business.php";
include_once "Session.php";
include_once "EmailOutboundsEngine.php";
include_once "EmailOutbounds.php";
include_once "/home/www/quotezone.co.uk/common/modules/CashBack.php";

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CEmailOutboundsEngineVan
//
// [DESCRIPTION]:  CEmailOutboundsEngineVan class interface, email outbounds support,
//                 PHP version
//
// [FUNCTIONS]:    FormatUserDetails($Session)
//                 CheckEmailOutboundResponse()
//                 SendUserDetails()
//                 ShowError()
//                 GetError()
//
//
//  (C) 2008 Gabriel ISTVANCSEK (gabi@acrux.biz) 2008-02-01
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CEmailOutboundsEngineVan Extends CEmailOutboundsEngine
{
    var $fileName;           // the fileName
    var $strERR;             // last error string
    var $systemType;         // system type(car,van,home,bike)
    var $quoteTypeID;        // quote type id (1-car , 2-van, 4-home, 5-bike)
    var $objEmailStore;      // email store id field
    var $objEmailStoreCodes; // EmailStoreCodes.php class object
    var $objEmailReminder;   // EmailReminder.php class object
    var $objQuoteDetails;    // QuoteDetails.php class object
    var $objVanVehicle;      // Vehicle.php class object
    var $objOccupation;      // Occupation.php class ojject
    var $objBusiness;        // Business.php class object
    var $objSession;         // Session.php class object
    var $objEmailOutbounds;  //email outbound object
    var $host;               //emv host
    var $path;               //emv path
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CEmailOutboundsEngineVan
   //
   // [DESCRIPTION]:   Default class constructor. Initialization goes here.
   //
   // [PARAMETERS]:    $fileName, $dbh
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CEmailOutboundsEngineVan($fileName, $dbh)
   {
      CEmailOutboundsEngine::CEmailOutboundsEngine('van','in');

      $this->fileName = $fileName;

      $this->systemType  = 'van';
      $this->quoteTypeID = '2';

      // ticket - ZTH-983371
      //http://trc.emv2.com/D2UTF8
      //$this->host = "as1.emv2.com";
      //$this->path = "/D";

      $this->host = "trc.emv2.com";
      $this->path = "/D2UTF8";

      $this->strERR  = "";

      if($dbh)
      {
         $this->dbh = $dbh;
         $this->closeDB = false;
      }
      else
      {
         // default configuration
         $this->dbh = new CMySQL();

         if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
         {
           $this->strERR = $this->dbh->GetError();
           return;
         }
      }

      $this->closeDB = true;

      $this->objQuoteDetails    = new CQuoteDetails($this->dbh);
      $this->objEmailStore      = new CEmailStore($this->dbh);
      $this->objEmailStoreCodes = new CEmailStoreCodes($this->dbh);
      $this->objEmailReminder   = new CEmailReminder($this->dbh);
      $this->objVanVehicle      = new CVanVehicle($this->dbh);
      $this->objOccupation      = new COccupation($this->dbh);
      $this->objBusiness        = new CBusiness($this->dbh);
      $this->objSession         = new CSession($this->dbh);
      $this->objEmailOutbounds  = new CEmailOutbounds($this->dbh);

   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: FormatUserDetails
   //
   // [DESCRIPTION]:   Form user data details to be sent it
   //
   // [PARAMETERS]:    $Session
   //
   // [RETURN VALUE]:  formated $string or false if failure
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function FormatUserDetails($Session)
   {
      /*start of driver details */
   $_PROPOSER = array(

      "type_of_driving" => array(
         "F"=>"Full UK",
         "P"=>"Provisional",
         "A"=>"Automatic",
         "E"=>"EU",
         "O"=>"Other European", 
      ),

      "marital_status" => array(
         "M"=>"Married/Civil Partnered",
         "S"=>"Single",
         "D"=>"Divorced",
         "W"=>"Widowed",
      ),

      "use_of_other_vehicles" => array(
         "X"=>"No access to any other vehicles",
         "O"=>"Owner of another vehicle",
         "N"=>"Named driver on another person's policy",
         "S"=>"Company car (including social domestic and pleasure use)",
         "C"=>"Company car (within working hours)",
         "M"=>"Motorcycle",
         "V"=>"Van",
      ),

      "who_drive_the_vehicle" => array(
         "IO"=>"Yourself Only",
         "IS"=>"Yourself and your spouse",
         "02"=>"Yourself & 1 other person",
         "03"=>"Yourself & 2 other persons",
         "04"=>"Yourself & 3 other persons",
      ),

      "employment_status" => array(
         "E"=>"Employed",
         "F"=>"Full-time education",
         "S"=>"Self Employed",
         "R"=>"Retired",
         "U"=>"Unemployed",
         "D"=>"Director",   
         "P"=>"Proprietor or Partner",
         "H"=>"Houseperson",
         "G"=>"Government",
         "C"=>"Club or Association",
      ),

   );

   $_ADDITIONAL = array(
      "additional_sex" => array(
         "F" => "Female",
         "M" => "Male",
      ),

      "additional_type_of_driving" => array(
         "F" => "Full",
         "P" => "Provisional",
         "A" => "Automatic",
         "E" => "EU",
         "O" => "Other European"
      ),
   );

   $_VEHICLE =array(
   "vehicle_make" => array (
         "AI" => "AIXAM",
         "AY" => "ASIA",
         "AN" => "AUSTIN",
         "AB" => "AUSTIN/MORRIS",
         "BD" => "BEDFORD",
         "CH" => "CHEVROLET",
         "CN" => "CITROEN",
         "DC" => "DACIA",
         "DH" => "DAIHATSU",
         "DO" => "DODGE",
         "EB" => "EBRO",
         "FT" => "FIAT",
         "FO" => "FORD",
         "FR" => "FREIGHT ROVER",
         "FS" => "FSO",
         "GE" => "GEM",
         "HD" => "HONDA",
         "HY" => "HYUNDAI",
         "IZ" => "ISUZU",
         "IV" => "IVECO",
         "JE" => "JEEP",
         "KA" => "KIA",
         "LA" => "LADA",
         "LR" => "LAND ROVER",
         "LD" => "LDV",
         "LY" => "LEYLAND DAF",
         "MZ" => "MAZDA",
         "MC" => "MERCEDES-BENZ",
         "MG" => "MG",
         "CO" => "MITSUBISHI",
         "DS" => "NISSAN",
         "ND" => "NISSAN/DATSUN",
         "PU" => "PEUGEOT",
         "PG" => "PIAGGIO",
         "PN" => "PROTON",
         "RE" => "RELIANT",
         "RN" => "RENAULT",
         "RD" => "RENAULT/DODGE",
         "RV" => "ROVER",
         "SN" => "SANTANA",
         "SE" => "SEAT",
         "SK" => "SKODA",
         "SY" => "SSANGYONG",
         "ST" => "STEVENS",
         "SU" => "SUBARU",
         "SZ" => "SUZUKI",
         "TB" => "TALBOT",
         "TT" => "TATA",
         "TY" => "TOYOTA",
         "VX" => "VAUXHALL",
         "VW" => "VOLKSWAGEN",
         "YE" => "YUEJIN",
         "YU" => "YUGO"
      ),

      "vehicle_kept" => array(
         "R" => "Public Road",
         "D" => "Drive",
         "P" => "Private Property",
         "C" => "Car Park",
         "G" => "Locked compound",
         "GR"=> "Garaged",
      ),

   );

   $_COVER = array(
      "type_of_cover" => array(
         "1"=>"Comprehensive",
         "2"=>"Third Party Fire & Theft",
         "3"=>"Third Party Only",
      ),

      "type_of_use" => array(
         "1"=>"Social, Domestic, Pleasure and Commuting",
         "2"=>"Social, Domestic and Pleasure Only",
         "3"=>"Business Use for Carriage of Own Goods",
         "4"=>"Business Use for Haulage",
      ),

      "protect_bonus" => array(
         "Y" => "Yes",
         "N" => "No",
      ),

      "owner_of_vehicle" => array(
         "1"=>"Proposer", 
         "2"=>"Spouse",
         "3"=>"Parents",
         "4"=>"Company",
         "5"=>"Privately Leased",
         "6"=>"Other",
      ),
   );

      /* end of driver details */

      $fileName = $this->fileName;

      $logID          = $Session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];
      $emailAddress   = $Session["_DRIVERS_"]["0"]["email_address"];
      $quoteUserID    = $Session["_QZ_QUOTE_DETAILS_"]["quote_user_id"];

      if (!$resEmailStoreDetails = $this->objEmailStore->GetEmailStoreByEmailAndQuoteUserId($emailAddress,$quoteUserID))
         print $this->objEmailStore->GetError();

      if (!$link = $this->objEmailStoreCodes->GenerateLink($resEmailStoreDetails['id'],$resEmailStoreDetails['email'],$this->quoteTypeID))
         print $this->objEmailStoreCodes->GetError();

      $cheappestSiteId         = "0";
      $cheapeastQuotePrice     = "0";
      $cheappestQuoteArray     = array();
      $cheappestSiteQuoteArray = array();

      $cheappestSiteQuoteArray = $this->objQuoteDetails->GetCheapestTopFiveSitesQuoteDetails($logID, 1);
      $cheappestSiteId         = $cheappestSiteQuoteArray[1]; 
      $cheappestQuoteArray     = $this->objQuoteDetails->GetCheapestSiteQuoteDetails($logID,$cheappestSiteId);
      $cheapeastQuotePrice     = $cheappestQuoteArray["cheapest_quote"];

      $PostArray = array();

      $PostArray["EMAIL_FIELD"] = $Session["_DRIVERS_"]["0"]["email_address"];

      $date = date("Y-m-d");

      if(! $emStoreID = $this->objEmailStore->GetEmailStoreIDByEmailAndQuoteUserId($PostArray["EMAIL_FIELD"],$quoteUserID))
      {
         print "CANNOT_GET_THE_EMAIL_STORE_ID \n\n";
      }
      

      while (1)
      {
         $authInfo = $this->GenerateMimeBoundary();
         $authInfo = str_replace("----=_NextPart_","",$authInfo);
         $authInfo = substr($authInfo, 0, 16);

         if(! $this->objEmailReminder->GetEmRemByAuthInfo($authInfo, $array))
            break;

      }

      if(! $this->objEmailReminder->AddEmailRem($quoteUserID,$emStoreID,"1",$date,"","1", $authInfo,'2','1','0'))
      {
         print $this->objEmailReminder->GetError();
      }

      //new fields: ZTH-983371
      //emv_tag=FFBB549680800054
      //emv_ref=EdX7CqkmjHZF8SA9MKJPUB6hXj4IGanK_DDdc6k2WMXdK7o
      //emv_tag=FFBB549680800054&emv_ref=EdX7CqkmjHZF8SA9MKJPUB6hXj4IGanK_DDdc6k2WMXdK7o

      $PostArray["emv_ref"]                   = "EdX7CqkmjHZF8SA9MKJPUB6hXj4IGanK_DDdc6k2WMXdK7o";
      $PostArray["emv_tag"]                   = "FFBB549680800054";

      $PostArray["RENEW_LINK_FIELD"]          = $link;
      $PostArray["emv_bounceback"]            = "1";
      //$PostArray["emv_bounceback"]            = "0";
      $PostArray["emv_pageok"]                = "http://www.quotezone.co.uk/emailpostsuccessvan.htm";
      $PostArray["emv_pageerror"]             = "http://www.quotezone.co.uk/emailpostfailurevan.htm";
      $PostArray["emv_webformtype"]           = "0";
      $PostArray["emv_clientid"]              = "35812";
      //$PostArray["emv_campaignid"]            = "2187600"; //TODO - $this->emv_campaignid
      $PostArray["emv_campaignid"]            = "5958610"; //XDO-232932

      $PostArray["TITLE_FIELD"]               = $Session["_DRIVERS_"]["0"]["title"];
      $PostArray["FIRSTNAME_FIELD"]           = $Session["_DRIVERS_"]["0"]["first_name"];
      $PostArray["LASTNAME_FIELD"]            = $Session["_DRIVERS_"]["0"]["surname"];
      $PostArray["DATEOFBIRTH_FIELD"]         = $Session["_DRIVERS_"]["0"]["date_of_birth_mm"]."/".$Session["_DRIVERS_"]["0"]["date_of_birth_dd"]."/".$Session["_DRIVERS_"]["0"]["date_of_birth_yyyy"];
      $codeMake                               = $Session["_VEHICLE_"]["vehicle_make"];
      $make                                   = $_VEHICLE["vehicle_make"][$codeMake];
      $PostArray["VEHICLE_MAKE_FIELD"]        = $make;
      $PostArray["VEHICLE_MODEL_FIELD"]       = $Session["_VEHICLE_"]["vehicle_model"];
      $PostArray["YEAR_OF_MANUFACTURE_FIELD"] = $Session["_VEHICLE_"]["year_of_manufacture"];
      $engine                                 = explode(" ",$Session["_VEHICLE_"]["engine_size"]);
      switch($engine[1])
      {
         case "P":
            $engineFuel = "Petrol";
         break;

         case "D":
            $engineFuel = "Diesel";
         break;
      }

      $PostArray["ENGINE_SIZE_AND_TYPE_FIELD"]  = $engine[0]."cc ".$engineFuel;
      $PostArray["ABI_DESCRIPTION_FIELD"]       = $Session["_VEHICLE_"]["engine_size"];
      $vehicleElementsArray                     = $this->objVanVehicle->GetVehicle($this->objVanVehicle->GetVehicleCode($Session["_VEHICLE_"]["vehicle_confirm"]));

      $PostArray["ABI_DESCRIPTION_FIELD"]       =  $vehicleElementsArray["make_description"]." ".$vehicleElementsArray["model_description"];

      $maritalStatus                            = $Session["_DRIVERS_"]["0"]["marital_status"];
      $PostArray["MARITAL_STATUS_FIELD"]        = $_PROPOSER["marital_status"][$maritalStatus];
      $PostArray["NUMBER_OF_CHILDREN_FIELD"]    = $Session["_DRIVERS_"]["0"]["children"];
      $PostArray["POSTCODE_FIELD"]              = $Session["_DRIVERS_"]["0"]["postcode_prefix"]." ".$Session["_DRIVERS_"]["0"]["postcode_number"];
      $PostArray["HOUSE_NUMBER_FIELD"]          = $Session["_DRIVERS_"]["0"]["house_number_or_name"];
      $PostArray["STREET_FIELD"]                = $Session["_DRIVERS_"]["0"]["address_line2"];
      $PostArray["TOWN_CITY_FIELD"]             = $Session["_DRIVERS_"]["0"]["address_line3"];
      $PostArray["COUNTY_FIELD"]                = $Session["_DRIVERS_"]["0"]["address_line4"];
      $homeOwner                                = $Session["_DRIVERS_"]["0"]["home_owner"];
      if($homeOwner == "N")
         $PostArray["HOMEOWNER_FIELD"]          = "No";
      if($homeOwner == "Y")
         $PostArray["HOMEOWNER_FIELD"]          = "Yes";

      $PostArray["INSURANCE_START_DATE_FIELD"]  = $Session["_DRIVERS_"]["0"]["date_of_insurance_start_mm"]."/".$Session["_DRIVERS_"]["0"]["date_of_insurance_start_dd"]."/".$Session["_DRIVERS_"]["0"]["date_of_insurance_start_yyyy"];
      $PostArray["QUOTE_DATE_FIELD"]            = date('m')."/".date('d')."/".date('Y');

      $useOfOtherVehicles                       = $Session["_DRIVERS_"]["0"]["use_of_other_vehicles"];
      $PostArray["USE_OF_OTHER_VEHICLES_FIELD"] = $_PROPOSER["use_of_other_vehicles"][$useOfOtherVehicles];

      $driversToBeInsured                       = $Session["_DRIVERS_"]["0"]["who_drive_the_vehicle"];
      $PostArray["DRIVERS_TO_BE_INSURED_FIELD"] = $_PROPOSER["who_drive_the_vehicle"][$driversToBeInsured];

      $employmentStatus                         = $Session["_DRIVERS_"]["0"]["employment_status"];
      $PostArray["EMPLOYMENT_STATUS_FIELD"]     = $_PROPOSER["employment_status"][$employmentStatus];

      $this->objOccupation->GetOccupation($Session["_DRIVERS_"]["0"]["occupation_list"],&$ftOccArrayResult);
      $this->objBusiness->GetBusiness($Session["_DRIVERS_"]["0"]["business_list"], &$ftBusArrayResult);

      $this->objOccupation->GetOccupation($Session["_DRIVERS_"]["0"]["occupation_list_pt"],&$ptOccArrayResult);
      $this->objBusiness->GetBusiness($Session["_DRIVERS_"]["0"]["business_list_pt"], &$ptBusArrayResult);

      $PostArray["FULL_TIME_OCCUPATION_FIELD"]   = $ftOccArrayResult["name"];
      $PostArray["FULL_TIME_BUSINESS_FIELD"]     = $ftBusArrayResult["name"];

      $PostArray["PART_TIME_OCCUPATION_FIELD"]   = $ptOccArrayResult["name"];
      $PostArray["PART_TIME_BUSINESS_FIELD"]     = $ptBusArrayResult["name"];

      $typeOfDriving                             = $Session["_DRIVERS_"]["0"]["type_of_driving"];
      $PostArray["DRIVING_LICENCE_TYPE_FIELD"]   = $_PROPOSER["type_of_driving"][$typeOfDriving];
      $passPluss                                 = $Session["_DRIVERS_"]["0"]["driver_pass_plus"];

      if($passPluss == "N")
         $PostArray["PASS_PLUS_FIELD"] = "No";
      if($passPluss == "Y")
         $PostArray["PASS_PLUS_FIELD"] = "Yes";

      $PostArray["LICENCE_DATE_FIELD"]           = $Session["_DRIVERS_"]["0"]["period_of_licence_held_mm"]."/".$Session["_DRIVERS_"]["0"]["period_of_licence_held_dd"]."/".$Session["_DRIVERS_"]["0"]["period_of_licence_held_yyyy"];

      $NrConv                                    = $this->objSession->GetSessionConvictionsDriverNumber($Session,0);
      $NrClaims                                  = $this->objSession->GetSessionClaimsDriverNumber($Session,0);

      if(! $NrConv)
         $NrConv = "0";
      if(! $NrClaims)
         $NrClaims = "0";

      $PostArray["NUMBER_OF_CLAIMS_FIELD"]        = $NrClaims;
      $PostArray["NUMBER_OF_CONVICTIONS_FIELD"]   = $NrConv;

      $coverType                                  = $Session["_COVER_"]["type_of_cover"];
      $PostArray["COVER_TYPE_FIELD"]              = $_COVER["type_of_cover"][$coverType];
      $typeOfUse                                  = $Session["_COVER_"]["type_of_use"];
      $PostArray["TYPE_OF_USE_FIELD"]             = $_COVER["type_of_use"][$typeOfUse];
      $PostArray["ANNUAL_MILEAGE_FIELD"]          = $Session["_COVER_"]["annual_mileage"];
      $PostArray["ANNUAL_BUSINESS_MILEAGE_FIELD"] = $Session["_COVER_"]["business_mileage"];
      $PostArray["VOLUNTARY_EXCESS_FIELD"]        = $Session["_COVER_"]["voluntary_excess"];
      $PostArray["YEAR_NCB_FIELD"]                = $Session["_COVER_"]["no_claims_bonus"];

      $mainUserOfVehicle                          = $Session["_COVER_"]["main_user_of_vehicle"];
      switch($mainUserOfVehicle)
      {
         case "1":
            $mainUserText                         = $Session["_DRIVERS_"]["0"]["title"]." ".$Session["_DRIVERS_"]["0"]["first_name"]." ".$Session["_DRIVERS_"]["0"]["surname"];
         break;

         case "2":
            $mainUserText                         = $Session["_DRIVERS_"]["1"]["additional_title"]." ".$Session["_DRIVERS_"]["1"]["additional_first_name"]." ".$Session["_DRIVERS_"]["1"]["additional_surname"];
         break;

         case "3":
            $mainUserText                         = $Session["_DRIVERS_"]["2"]["additional_title"]." ".$Session["_DRIVERS_"]["2"]["additional_first_name"]." ".$Session["_DRIVERS_"]["2"]["additional_surname"];
         break;

         case "4":
            $mainUserText                         = $Session["_DRIVERS_"]["3"]["additional_title"]." ".$Session["_DRIVERS_"]["3"]["additional_first_name"]." ".$Session["_DRIVERS_"]["3"]["additional_surname"];
         break;

      }
      $PostArray["MAIN_USER_FIELD"]               = $mainUserText;
      $ownerOfVehicle                             = $Session["_COVER_"]["owner_of_vehicle"];
      switch($ownerOfVehicle)
      {
         case "1":
            $ownerOfVehicleText                   = $Session["_DRIVERS_"]["0"]["title"]." ".$Session["_DRIVERS_"]["0"]["first_name"]." ".$Session["_DRIVERS_"]["0"]["surname"];
         break;

         case "2":
            $ownerOfVehicleText                   = $Session["_DRIVERS_"]["1"]["additional_title"]." ".$Session["_DRIVERS_"]["1"]["additional_first_name"]." ".$Session["_DRIVERS_"]["1"]["additional_surname"];
         break;

         case "3":
            $ownerOfVehicleText                    = "Parents";
         break;

         case "4":
            $ownerOfVehicleText                    = "Company";
         break;

         case "5":
            $ownerOfVehicleText                    = "Privately Leased";
         break;

         case "6":
            $ownerOfVehicleText                    = "Other";
         break;
      }
      $PostArray["OWNER_FIELD"]                    = $ownerOfVehicleText;

      $registeredOfVehicle                         = $Session["_COVER_"]["registered_of_vehicle"];
      switch($ownerOfVehicle)
      {
         case "1":
            $registeredOfVehicleText               = $Session["_DRIVERS_"]["0"]["title"]." ".$Session["_DRIVERS_"]["0"]["first_name"]." ".$Session["_DRIVERS_"]["0"]["surname"];
         break;

         case "2":
            $registeredOfVehicleText               = $Session["_DRIVERS_"]["1"]["additional_title"]." ".$Session["_DRIVERS_"]["1"]["additional_first_name"]." ".$Session["_DRIVERS_"]["1"]["additional_surname"];
         break;

         case "3":
            $registeredOfVehicleText               = "Parents";
         break;

         case "4":
            $registeredOfVehicleText               = "Company";
         break;

         case "5":
            $registeredOfVehicleText               = "Privately Leased";
         break;

         case "6":
            $registeredOfVehicleText               = "Other";
         break;
      }

      $PostArray["REGISTERED_KEEPER_FIELD"]        = $registeredOfVehicleText;

      $PostArray["NUMBER_OF_OTHER_VEHICLES_FIELD"] = $Session["_COVER_"]["number_of_other_vehicles"];

      $sid = str_replace("_","",$fileName);

      $PostArray["QUOTE_TYPE_FIELD"]               = "van";
      $PostArray["QUOTE_RESULTS_LINK_FIELD"]       = "https://van-insurance.quotezone.co.uk/van/index.php?sid=$sid";
      $PostArray["EMVADMIN1_FIELD"]                = $sid;

      $wlUserID    = $Session["_QZ_QUOTE_DETAILS_"]["wlUserID"];
      
      // MoneyMaxim (ticket - XEV-793421)
      if($wlUserID == 136)
      {
         $PostArray["emv_campaignid"]              = "7887139";
         $PostArray["QUOTE_TYPE_FIELD"]            = "moneymaxim van";

         $PostArray["emv_ref"]                   = "EdX7CqkmjGi-8SA9MKJPUB7VKEx6HK3HjzzfDdoyXbPZK9w";
         $PostArray["emv_tag"]                   = "2000004E42FC45C0";
      }

      //payingtoomuch
      if ($wlUserID == 496)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "PayingTooMuch_van";
         $PostArray["emv_tag"]          = "1198080005B25CE0";
         $PostArray["emv_ref"]          = "EdX7CqkmjK9j8SA9MKJPUB7WKUVyHKXD-jjYCaszK7XZKEE";
      }

      //simplybusiness
      if ($wlUserID == 535)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "SimplyBusiness_van";
         $PostArray["emv_tag"]          = "20000CB77722C662";
         $PostArray["emv_ref"]          = "EdX7CqkmjJGG8SA9MKJPUB7VKEx6HN6x_T_aeatFXsbbK6I";
      }

      //nhscashback 
      if ($wlUserID == 584)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "nhsCashback_van";
         $PostArray["emv_tag"]          = "9A1E03AC66020009";
         $PostArray["emv_ref"]          = "EdX7CqkmjJ_r8SA9MKJPUB7eWU0PHK6yiT7be6s2WMDQK-w"; 
      }

      //mirrorcashback 
      if ($wlUserID == 586)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "mirrorCashback_van";
         $PostArray["emv_tag"]          = "877EEA9980800008";
         $PostArray["emv_ref"]          = "EdX7CqkmjJ_u8SA9MKJPUB7fL0sPadzK8zDdc6k2WMDRK-I"; 
      }

      //froggybank 
      if ($wlUserID == 580)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "froggyBank_van";
         $PostArray["emv_tag"]          = "80002DF50A6A9980";
         $PostArray["emv_ref"]          = "EdX7CqkmjJxm8SA9MKJPUB7fKEx6Htm1_zisfdg_UcjZKHs"; 
      }

      //giveortake 
      if ($wlUserID == 587)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "giveOrtake_van";
         $PostArray["emv_tag"]          = "A998080001C7FFBE";
         $PostArray["emv_ref"]          = "EdX7CqkmjJwC8SA9MKJPUB6mIUVyHKXD-jjcCK5ALrKsKPE"; 
      }

      //psdiscount 
      if ($wlUserID == 585)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "psDiscount_van";
         $PostArray["emv_tag"]          = "2C0F58CC04000012";
         $PostArray["emv_ref"]          = "EdX7CqkmjJ_o8SA9MKJPUB7VW0wMGaWwiTjZe6k2WMHbK9I"; 
      }

      //vacmedia 
      if ($wlUserID == 588)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "vacMedia_van";
         $PostArray["emv_tag"]          = "60200009ACC65AC6";
         $PostArray["emv_ref"]          = "EdX7CqkmjJ_58SA9MKJPUB7RKE56HK3D80muCK8zKbPfK4s"; 
      }
      
      //topcashback 
      if ($wlUserID == 604)
      {
         $cheapestSiteID  = $cheappestQuoteArray['site_id'];
         $cheapestPremium = $cheappestQuoteArray['cheapest_quote'];

         $cashbackAmount = CalculateCashBack($cheapestSiteID,$cheapestPremium,"","",$Session['CPA'][$cheapestSiteID],$wlUserID);

	 $memberID                           = trim($Session["_QZ_QUOTE_DETAILS_"]["memberID"]);
         $PostArray["EMVADMIN3_FIELD"]       = $memberID;
         $PostArray["QUOTE_TYPE_FIELD"]      = "topCashBack_van";
         $PostArray["emv_tag"]               = "98080003BE9AF689";
         $PostArray["emv_ref"]               = "EdX7CqkmjJyi8SA9MKJPUB7eIExyHK3D-UqocthAXsjQKEg"; 
         $PostArray["CASHBACK_AMOUNT_FIELD"] = $cashbackAmount;
         $PostArray["CASHBACK_TOTAL_FIELD"]  = $cheapestPremium-$cashbackAmount;
      }

      //leedscompare 
      if ($wlUserID == 608)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "lufc_van";
         $PostArray["emv_tag"]          = "F8CC0400011D5E0F";
         $PostArray["emv_ref"]          = "EdX7CqkmjIFE8SA9MKJPUB6hID8JHKnD-jjcet0zLcCvKCM"; 
      }


	//andybiggsiar 
      if ($wlUserID == 633)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "carinsurancecomparison_van";
         $PostArray["emv_tag"]          = "74D4C15268020007";
         $PostArray["emv_ref"]          = "EdX7CqkmjImI8SA9MKJPUB7QLDh-b6zG-D7Ve6s2WMDeK7g"; 
      }



	//quoteyquotey 
      if ($wlUserID == 631)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "quoteyquotey_van";
         $PostArray["emv_tag"]          = "664569A00800063D";
         $PostArray["emv_ref"]          = "EdX7CqkmjImU8SA9MKJPUB7RLkh_GqSy-jjVe6k2XsOtKF8"; 
      }


	//msmithfreelance 
      if ($wlUserID == 632)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "cheapcarinsurance_van";
         $PostArray["emv_tag"]          = "2D541A0080003C16";
         $PostArray["emv_ref"]          = "EdX7CqkmjIn48SA9MKJPUB7VXEl-HdzD-jDde6k1K8HfK-c"; 
      }


      if ($wlUserID == 634)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "cheapvaninsurance_van";
         $PostArray["emv_tag"]          = "FE1A0080002A7940";
         $PostArray["emv_ref"]          = "EdX7CqkmjI1e8SA9MKJPUB6hXU0LHK3L-jjdedgxUcTZKBM"; 
      }

      // cheap
      if ($wlUserID == 644)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "cheapcouk_van";
         $PostArray["emv_tag"]          = "D4C935010000B1E8";
         $PostArray["emv_ref"]          = "EdX7CqkmjPR08SA9MKJPUB6jLD9zH6jD-zjde6lEWbXRKB8"; 
      }


      // moneymedia
      if ($wlUserID == 601)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "moneymedia_van";
         $PostArray["emv_tag"]          = "D404000345CB43B4";
         $PostArray["emv_ref"]          = "EdX7CqkmjPaV8SA9MKJPUB6jLEx-HK3D-TzYCNsyW7LdKGE"; 
      }





      // bestbuymoney
      if ($wlUserID == 640)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "bestbuymoney_van";
         $PostArray["emv_tag"]          = "BBABB38D40400009";
         $PostArray["emv_ref"]          = "EdX7CqkmjPQR8SA9MKJPUB6lWj0Ibq7Ljjzdf6k2WMDQKIA"; 
      }



      //cpcomparisons
      if ($wlUserID == 652)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "cpcomparisons_van";
         $PostArray["emv_tag"]          = "CFB5BB3601000065";
         $PostArray["emv_ref"]          = "EdX7CqkmjOO38SA9MKJPUB6kXj5_bt_A_Djce6k2WMbcKGk"; 
      }

      //savoo
      if ($wlUserID == 661)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "savoo_van";
         $PostArray["emv_tag"]          = "DC040001D2AD3954";
         $PostArray["emv_ref"]          = "EdX7CqkmjO1U8SA9MKJPUB6jW0x-HK3D-0zfCt01UcXdKNk"; 
      }



      //moneyhelpline
      if ($wlUserID == 657)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "moneyhelpline_van";
         $PostArray["emv_tag"]          = "DC040003B84D9E10";
         $PostArray["emv_ref"]          = "EdX7CqkmjO9z8SA9MKJPUB6jW0x-HK3D-UrVf90_LcHZKDM"; 
      }

      //carinscomp
      if ($wlUserID == 533)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "carinscomp_van";
         $PostArray["emv_tag"]          = "17A89A3370100017";
         $PostArray["emv_ref"]          = "EdX7CqkmjNHG8SA9MKJPUB7WLz1yFdzA-T_deqk2WMHeK5I"; 
      }

      //maximiles
      if ($wlUserID == 666)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "maximiles_van";
         $PostArray["emv_tag"]          = "9C00800058515887";
         $PostArray["emv_ref"]          = "EdX7CqkmjNe38SA9MKJPUB7eW0x6FK3D-j3VfqgzUMjeKFs"; 
      }



      //comparecomiar
      if ($wlUserID == 681)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "comparecomiar_van";
         $PostArray["emv_tag"]          = "22A8966C1C008022";
         $PostArray["emv_ref"]          = "EdX7CqkmjMBt8SA9MKJPUB7VKj1yFavFiTmue6k-WMLbKBQ"; 
      }

      //soswitch
      if ($wlUserID == 690)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "soswitch_van";
         $PostArray["emv_tag"]          = "C008000716D875F9";
         $PostArray["emv_ref"]          = "EdX7CqkmjMDk8SA9MKJPUB6kKExyHK3D_TnbD6ExXbbQK6A"; 
      }



      $unsubscribeLink = $this->CreateUnsubscribeLinkParams($Session["_DRIVERS_"]["0"]["email_address"],'top3','van')."&sid=$sid&type=1&action=unsubscribe&campaignId=".$PostArray["emv_campaignid"];
      //$PostArray["UNSUBSCRIBE_LINK_FIELD"]          = "http://van-insurance.quotezone.co.uk/customer/unsubscribe.php?$unsubscribeLink";
      $PostArray["UNSUBSCRIBE_LINK_FIELD"]          = "https://www.quotezone.co.uk/customer/unsubscribe.php?$unsubscribeLink";

      if($wlUserID)
	$PostArray["UNSUBSCRIBE_LINK_FIELD"]          = "https://www.quotezone.co.uk/customer/wl-unsubscribe.php?emailaddress=".$Session["_DRIVERS_"]["0"]["email_address"];

      if($Session["_DRIVERS_"]["0"]["advanced_motorist"] == "Y")
         $PostArray["iam_member"]                   = "Yes";
      if($Session["_DRIVERS_"]["0"]["advanced_motorist"] == "N")
         $PostArray["IAM_MEMBER_FIELD"]             = "No";
      $PostArray["QUOTE_TIME_FIELD"]                = date("Gis");
      if($Session["_VEHICLE_"]["vehicle_modified_from_manufacturer"] == "N")
         $PostArray["MODIFIED_FIELD"]               = "No";
      if($Session["_VEHICLE_"]["vehicle_modified_from_manufacturer"] == "Y")
         $PostArray["MODIFIED_FIELD"]               = "Yes";
      if($Session["_VEHICLE_"]["vehicle_grey_or_import"] == "Y")
         $PostArray["IMPORT_FIELD"]                 = "Yes";
      if($Session["_VEHICLE_"]["vehicle_grey_or_import"] == "N")
         $PostArray["IMPORT_FIELD"]                 = "No";
      if($Session["_VEHICLE_"]["vehicle_side"] == "N")
         $PostArray["LEFT_RIGHT_FIELD"]             = "Right";
      if($Session["_VEHICLE_"]["vehicle_side"] == "Y")
         $PostArray["LEFT_RIGHT_FIELD"]             = "Left";
      $vehicleKept                                  = $Session["_VEHICLE_"]["vehicle_kept"];
      $PostArray["KEPT_OVERNIGHT_FIELD"]            = $_VEHICLE["vehicle_kept"][$vehicleKept];
      if($Session["_VEHICLE_"]["vehicle_alarm"] == "N")
         $PostArray["ALARM_FIELD"]                  = "No";
      if($Session["_VEHICLE_"]["vehicle_alarm"] == "Y")
         $PostArray["ALARM_FIELD"]                  = "Yes";
      if($Session["_VEHICLE_"]["vehicle_immobiliser"] == "N")
         $PostArray["IMMOBILISER_FIELD"]            = "No";
      if($Session["_VEHICLE_"]["vehicle_immobiliser"] == "Y")
         $PostArray["IMMOBILISER_FIELD"]            = "Yes";
      if($Session["_VEHICLE_"]["vehicle_tracking_device"] == "N")
         $PostArray["TRACKER_FIELD"]                = "No";
      if($Session["_VEHICLE_"]["vehicle_tracking_device"] == "Y")
         $PostArray["TRACKER_FIELD"]                = "Yes";
      $PostArray["PURCHASED_FIELD"]                 = "Yes";
      $vehBought = explode("/",$Session["_VEHICLE_"]["vehicle_bought"]);
      $PostArray["PURCHASE_DATE_FIELD"]             = $vehBought[1]."/".$vehBought[0]."/".$vehBought[2];
      $PostArray["ESTIMATED_VALUE_FIELD"]           = $Session["_VEHICLE_"]["estimated_value"];
      $PostArray["REGISTRATION_NUMBER_FIELD"]       = strtoupper($Session["_VEHICLE_"]["vehicle_registration_number"]);

      if($Session["_COVER_"]["protect_bonus"] == "N")
         $PostArray["PROTECTED_NCB_FIELD"]          = "No";
      if($Session["_COVER_"]["protect_bonus"] == "Y")
         $PostArray["PROTECTED_NCB_FIELD"]          = "Yes";
      $PostArray["MOBILE_NUMBER_FIELD"]             = $Session["_DRIVERS_"][0]["mobile_telephone_prefix"]."".$Session["_DRIVERS_"][0]["mobile_telephone_number"];
      if(! $cheapeastQuotePrice)
         $cheapeastQuotePrice = "0";

      if($cheapeastQuotePrice == "0")
      {
	 $PostArray["emv_tag"]          = "800052848AF01D80";
         $PostArray["emv_ref"]          = "EdX7CqkmjzyW8SA9MKJPUB7fKEx6Ga_L_jCsDak3LMjZKz4";
      }

      $PostArray["PREMIUM_FIELD"]                   = $cheapeastQuotePrice;
      if($Session["_DRIVERS_"][0]["update_news"] == "on")
         $PostArray["OPT_IN_FIELD"]                 = "Yes";
      else
         $PostArray["OPT_IN_FIELD"]                 = "No";
      $PostArray["QUOTE_COMPLETED_FIELD"]           = "Yes";
      $PostArray["QUOTE_REF_FIELD"]                 = $Session["_QZ_QUOTE_DETAILS_"]["quote_reference"];

















































 // NEW FIELDS

      // set defaults
      // company name for top quote, this will be used to retrieve an image, so lower case and no spaces please
      $PostArray["COMPANY_FIELD"]          = '';

      // company name for 2nd quote, same as above
      $PostArray["COMPANY_2_FIELD"]        = '';

      // company name for 3rd quote, same as above
      $PostArray["COMPANY_3_FIELD"]        = '';

      // Annual premium of 2nd quote
      $PostArray["PREMIUM_2_FIELD"]        = '';

      // Annual premium of 3rd quote
      $PostArray["PREMIUM_3_FIELD"]        = '';

      // if top quote has "courtesy car" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_A_FIELD"]   = '';

      // if 2nd quote has "courtesy car" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_A_2_FIELD"] = '';

      // if 3rd quote has "courtesy car" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_A_3_FIELD"] = '';

      // if top quote has "windscreen cover" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_B_FIELD"]   = '';

      // if 2nd quote has "windscreen cover" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_B_2_FIELD"] = '';

      // if 3rd quote has "windscreen cover" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_B_3_FIELD"] = '';

      // if top quote has "personal accident" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_C_FIELD"]   = '';

      // if 2nd quote has "personal accident" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_C_2_FIELD"] = '';

      // if 3rd quote has "personal accident" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_C_3_FIELD"] = '';

      // buyOnline link for top quote
      $PostArray["QUOTE_DETAIL_D_FIELD"]   = '';

      // buyOnline link for 2nd quote
      $PostArray["QUOTE_DETAIL_D_2_FIELD"] = '';

      // buyOnline link for 3rd quote
      $PostArray["QUOTE_DETAIL_D_3_FIELD"] = '';


      $topQuoteArray = array();

      // get top quotes
      if( $topQuoteArray = $this->objQuoteDetails->GetCheapestTopFiveSitesQuoteDetails($logID, 3))
      {
         $_SESSION                = $Session;
         $cheappestSiteQuoteArray = array();

         for($k=1;$k<=3;$k++)
         {
            $topPremium          = '';
            $topSiteDetailsArray = array();
            $_resSite            = array();
            $courtesy            = '';
            $windscreen          = '';
            $accident            = '';

            $topSiteID           = $topQuoteArray[$k];
            $topSiteDetailsArray = $this->objQuoteDetails->GetCheapestSiteQuoteDetails($logID,$topSiteID);

            if($k == 1)
            {
               // get company name
               $siteFileName = $Session["SHOW_SITES"][$topSiteID];
               //include_once "/home/www/quotezone.co.uk/insurance-new/van/sites/showsites/$siteFileName";
               // RAMDRIVE
               include_once "../van/sites/showsites/$siteFileName";

               //$PostArray["COMPANY_FIELD"] = strtolower(str_replace(" ","",$_resSite['brokerFullName']));
               $PostArray["COMPANY_FIELD"] = "https://van-insurance.quotezone.co.uk/van/".$_resSite['imageSiteRemote'];

               // if top quote has "courtesy van" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_COURTESY_']))
                  $courtesy = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_COURTESY_']))
                  $courtesy = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_COURTESY_']))
                  $courtesy = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_COURTESY_']))
                  $courtesy = "check";

               $PostArray["QUOTE_DETAIL_A_FIELD"]   = $courtesy;

               //if top quote has "windscreen cover" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_WINDSCREEN_']))
                  $windscreen = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_WINDSCREEN_']))
                  $windscreen = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_WINDSCREEN_']))
                  $windscreen = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_WINDSCREEN_']))
                  $windscreen = "check";

               $PostArray["QUOTE_DETAIL_B_FIELD"]   = $windscreen;

               //if top quote has "personal accident" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_ACCIDENT_']))
                  $accident = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_ACCIDENT_']))
                  $accident = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_ACCIDENT_']))
                  $accident = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_ACCIDENT_']))
                  $accident = "check";

               $PostArray["QUOTE_DETAIL_C_FIELD"]   = $accident;

               // buy online link
               $buyOnlineLink = '';
               if($_resSite['proceedBuyOnlineLink'])
                  $buyOnlineLink = $_resSite['proceedBuyOnlineLink'];
               elseif($_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0])
                  $buyOnlineLink = $_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0];

               if($buyOnlineLink)
               {
                  $PostArray["QUOTE_DETAIL_D_FIELD"]   = str_replace("sites/clickIn.php?","",$buyOnlineLink);

                  if(preg_match("/quote_ref\.value/i",$PostArray["QUOTE_DETAIL_D_FIELD"]))
                  {
                     // extract quote_ref from out file
                     include_once("/home/www/quotezone.co.uk/insurance-new/van/steps/functions.php");

                     // RAMDRIVE
                     //include_once("../van/steps/functions.php");
                     if($openResultFile = OpenResultFile($topSiteID))
                     {
                        $quote_ref = $openResultFile[0]["quote reference"];

                        if($quote_ref)
                           $PostArray["QUOTE_DETAIL_D_FIELD"] = str_replace("quote_ref.value",$quote_ref,$PostArray["QUOTE_DETAIL_D_FIELD"]);
                     }
                  }
               }


            }

            if($k == 2)
            {
               // get company name
               $siteFileName = $Session["SHOW_SITES"][$topSiteID];
               //include_once "/home/www/quotezone.co.uk/insurance-new/van/sites/showsites/$siteFileName";

               // RAMDRIVE
               include_once "../van/sites/showsites/$siteFileName";
 
               // company premium
               $topPremium  = $topSiteDetailsArray["cheapest_quote"];

               //$PostArray["COMPANY_2_FIELD"] = strtolower(str_replace(" ","",$_resSite['brokerFullName']));
               $PostArray["COMPANY_2_FIELD"] = "https://van-insurance.quotezone.co.uk/van/".$_resSite['imageSiteRemote'];

               $PostArray["PREMIUM_2_FIELD"] = $topPremium;

              // if top quote has "courtesy van" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_COURTESY_']))
                  $courtesy = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_COURTESY_']))
                  $courtesy = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_COURTESY_']))
                  $courtesy = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_COURTESY_']))
                  $courtesy = "check";

               $PostArray["QUOTE_DETAIL_A_2_FIELD"]   = $courtesy;

               //if top quote has "windscreen cover" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_WINDSCREEN_']))
                  $windscreen = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_WINDSCREEN_']))
                  $windscreen = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_WINDSCREEN_']))
                  $windscreen = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_WINDSCREEN_']))
                  $windscreen = "check";

               $PostArray["QUOTE_DETAIL_B_2_FIELD"]   = $windscreen;

               //if top quote has "personal accident" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_ACCIDENT_']))
                  $accident = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_ACCIDENT_']))
                  $accident = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_ACCIDENT_']))
                  $accident = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_ACCIDENT_']))
                  $accident = "check";

               $PostArray["QUOTE_DETAIL_C_2_FIELD"]   = $accident;

               // buy online link
               $buyOnlineLink = '';
               if($_resSite['proceedBuyOnlineLink'])
                  $buyOnlineLink = $_resSite['proceedBuyOnlineLink'];
               elseif($_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0])
                  $buyOnlineLink = $_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0];


               if($buyOnlineLink)
               {
                  $PostArray["QUOTE_DETAIL_D_2_FIELD"]   = str_replace("sites/clickIn.php?","",$buyOnlineLink);

                  if(preg_match("/quote_ref\.value/i",$PostArray["QUOTE_DETAIL_D_2_FIELD"]))
                  {
                     // extract quote_ref from out file
                     include_once("/home/www/quotezone.co.uk/insurance-new/van/steps/functions.php");

                     // RAMDRIVE
                     //include_once("../van/steps/functions.php");
                     if($openResultFile = OpenResultFile($topSiteID))
                     {
                        $quote_ref = $openResultFile[0]["quote reference"];

                        if($quote_ref)
                           $PostArray["QUOTE_DETAIL_D_2_FIELD"] = str_replace("quote_ref.value",$quote_ref,$PostArray["QUOTE_DETAIL_D_2_FIELD"]);
                     }
                  }
               }

            }

            if($k == 3)
            {
               // get company name
               $siteFileName = $Session["SHOW_SITES"][$topSiteID];
               //include_once "/home/www/quotezone.co.uk/insurance-new/van/sites/showsites/$siteFileName";

               // RAMDRIVE
               include_once "../van/sites/showsites/$siteFileName";
 
               // company premium
               $topPremium  = $topSiteDetailsArray["cheapest_quote"];

               //$PostArray["COMPANY_3_FIELD"] = strtolower(str_replace(" ","",$_resSite['brokerFullName']));
               $PostArray["COMPANY_3_FIELD"] = "https://van-insurance.quotezone.co.uk/van/".$_resSite['imageSiteRemote'];

               $PostArray["PREMIUM_3_FIELD"] = $topPremium;

               // if top quote has "courtesy van" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_COURTESY_']))
                  $courtesy = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_COURTESY_']))
                  $courtesy = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_COURTESY_']))
                  $courtesy = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_COURTESY_']))
                  $courtesy = "check";

               $PostArray["QUOTE_DETAIL_A_3_FIELD"]   = $courtesy;

               //if top quote has "windscreen cover" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_WINDSCREEN_']))
                  $windscreen = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_WINDSCREEN_']))
                  $windscreen = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_WINDSCREEN_']))
                  $windscreen = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_WINDSCREEN_']))
                  $windscreen = "check";

               $PostArray["QUOTE_DETAIL_B_3_FIELD"]   = $windscreen;

               //if top quote has "personal accident" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_ACCIDENT_']))
                  $accident = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_ACCIDENT_']))
                  $accident = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_ACCIDENT_']))
                  $accident = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_ACCIDENT_']))
                  $accident = "check";

               $PostArray["QUOTE_DETAIL_C_3_FIELD"]   = $accident;

               // buy online link
               $buyOnlineLink = '';
               if($_resSite['proceedBuyOnlineLink'])
                  $buyOnlineLink = $_resSite['proceedBuyOnlineLink'];
               elseif($_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0])
                  $buyOnlineLink = $_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0];

               if($buyOnlineLink)
               {
                  $PostArray["QUOTE_DETAIL_D_3_FIELD"]   = str_replace("sites/clickIn.php?","",$buyOnlineLink);

                  if(preg_match("/quote_ref\.value/i",$PostArray["QUOTE_DETAIL_D_3_FIELD"]))
                  {
                     // extract quote_ref from out file
                     include_once("/home/www/quotezone.co.uk/insurance-new/van/steps/functions.php");

                     // RAMDRIVE
                     //include_once("../van/steps/functions.php");
                     if($openResultFile = OpenResultFile($topSiteID))
                     {
                        $quote_ref = $openResultFile[0]["quote reference"];

                        if($quote_ref)
                           $PostArray["QUOTE_DETAIL_D_3_FIELD"] = str_replace("quote_ref.value",$quote_ref,$PostArray["QUOTE_DETAIL_D_3_FIELD"]);
                     }
                  }
               }


            }
         }//for

      }// if get top quotes

      // NEW FIELDS 














































      $string = "";

      foreach($PostArray as $key=>$val)
      {
         $string .= $key."=".urlencode($val)."&";
      }

      $string = rtrim($string, "&");

      //$fh = fopen("/home/www/quotezone.co.uk/insurance-new/van/VanEmailOutbounds.log","a+");
      //fwrite($fh,"String is :  $string  \n");
      //fclose($fh);

      return $string;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: SendUserDetails
   //
   // [DESCRIPTION]:   Form user data details to be sent it
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SendUserDetails()
   {
      $fh = fopen("/home/www/quotezone.co.uk/insurance-new/van/VanEmailOutbounds.log","a+");
      
      if(! $Session = $this->LoadSessionFile($this->fileName))
      {
         $this->strERR = GetErrorString('CANT_GET_FILENAME_CONTENT');
         return false;
      }

      $logID       = $Session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];
      $wlUserID    = $Session["_QZ_QUOTE_DETAILS_"]["wlUserID"];
      $emailAddr   = $Session["_DRIVERS_"][0]["email_address"];
      $quoteUserID = $Session["_QZ_QUOTE_DETAILS_"]["quote_user_id"];

      // DO NOT SEND IF TEST QUOTE
      if(isset($Session["TESTING SITE ID"]))
         return;

      // moneymaxim
      //if($wlUserID == "136")
      //   return;
      
      if(! $this->LogWasSentOk($logID))
      {
         fwrite($fh, "POST ALLREADY SENT [$logID] file [$this->fileName]\n");
         fclose($fh);
         return false;
      }
      
      //commented due to ticket id : XEV-793421   
      /*
      $emailRule = $this->objEmailStore->GetEmailStoreRuleByEmailAndQuoteUserID($emailAddr,$quoteUserID);
      
      if($emailRule != '65535')
      {
         fwrite($fh, "Email address [$emailAddr] has rule [$emailRule] for file [$this->fileName] and log_id [$logID]\n");
         fwrite($fh, "Details wont be posted because the user has unsubscribed\n");
         fclose($fh);
         return false;
      }
      */

//      if(! $this->isNotWaitingQuotes($logID))
//      {
//         fwrite($fh, "QUOTE STILL WAITING: $this->fileName logID=$logID \n");
//         fclose($fh);
//         return false;
//      }

      if(! $this->objEmailOutbounds->AddEmailOutbounds($logID))
      {
         fwrite($fh, "CANNOT ADD  log_id [$logID] to database\n");
         fclose($fh);
         return false;
      }
     
      if(! $string = $this->FormatUserDetails($Session))
      {
         fwrite($fh, "CANNOT FORMAT USER DETAILS log_id [$logID] file [$this->fileName]\n");
         fclose($fh);
         return false;
      }
      
      fwrite($fh, "STRING IS : $string \n");
      
      if(! $response = $this->EmailOutboundsEngineSend($this->path, $string))
      {
         fwrite($fh, "RESPONSE WAS A FAILURE : $this->fileName logID=$logID [$response]\n");
         fclose($fh);
         return false;
      }

      if(! $this->objEmailOutbounds->SetEmailOutboundsStatus($logID, "1"))
      {
         fwrite($fh, "CANNOT SE STATUS for : logID=$logID\n");
         fclose($fh);
         return false;
      }

      if(! $this->CheckEmailOutboundResponse($response))
      {
         fwrite($fh, "RESPONSE WAS AN ERROR for : $logID [$response]\n");
         fclose($fh);
         return false;
      }
      else
      {
         fwrite($fh, "ALL DONE SUCCESFULLY : $logID [$response] - Settign details in DB \n");
         fclose($fh);
         return true;
      }
      

   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: SendUserDetailsScanner
   //
   // [DESCRIPTION]:   Send user details from scanner
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SendUserDetailsScanner($fileName)
   {
      $fh = fopen("/home/www/quotezone.co.uk/insurance-new/van/VanEmailOutbounds.log","a+");
      
      if(! $Session = $this->LoadSessionFile($fileName))
      {
         fwrite($fh, "-- SCANNER -- FAILURE load session: $fileName ...\n");
         fclose($fh);
         $this->strERR = GetErrorString('CANT_GET_FILENAME_CONTENT');
         return false;
      }

      $logID       = $Session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];
      $wlUserID    = $Session["_QZ_QUOTE_DETAILS_"]["wlUserID"];
      $emailAddr   = $Session["_DRIVERS_"][0]["email_address"];
      $quoteUserID = $Session["_QZ_QUOTE_DETAILS_"]["quote_user_id"];

      // MoneyMaxim
      if($wlUserID == "136")
      { 
         fwrite($fh, "-- SCANNER -- THIS IS A MONEY MAXIM WL - SENDING CANCELLED: $fileName ...\n");
         fclose($fh);
         return;
      }

      //if($Session['_DRIVERS_'][0]['email_address'] == 'test@testfake.com')
      if(preg_match("/seopa\d+@seopa\.com/",$Session['_DRIVERS_'][0]['email_address']) && $wlUserID != "279")
        return;
      
      /*
      if(! $this->LogWasSentOk($logID))
      {
         fwrite($fh, "POST ALLREADY SENT [$logID] file [$this->fileName]\n");
         fclose($fh);
         return false;
      }
      */
      
      //commented due to ticket id : XEV-793421   
      /*
      $emailRule = $this->objEmailStore->GetEmailStoreRuleByEmailAndQuoteUserID($emailAddr,$quoteUserID);
      
      if($emailRule != '65535')
      {
         fwrite($fh, "Email address [$emailAddr] has rule [$emailRule] for file [$this->fileName] and log_id [$logID]\n");
         fwrite($fh, "Details wont be posted because the user has unsubscribed\n");
         fclose($fh);
         return false;
      }
      */

     if(! $this->isNotWaitingQuotes($logID))
     {
        fwrite($fh, "--SCANNER --QUOTE STILL WAITING: $fileName logID=$logID \n");
        fclose($fh);
        return false;
     }

      /*
      if(! $this->objEmailOutbounds->AddEmailOutbounds($logID))
      {
         fwrite($fh, "CANNOT ADD  log_id [$logID] to database\n");
         fclose($fh);
         return false;
      }
      */
     
      if(! $string = $this->FormatUserDetails($Session))
      {
         fwrite($fh, "--SCANNER-- CANNOT FORMAT USER DETAILS log_id [$logID] file [$this->fileName]\n");
         fclose($fh);
         return false;
      }
      
      fwrite($fh, "--SCANNER-- STRING IS : $string \n");
      
      if(! $response = $this->EmailOutboundsEngineSend($this->path, $string))
      {
         fwrite($fh, "--SCANNER-- RESPONSE WAS A FAILURE : $fileName logID=$logID [$response]\n");
         fclose($fh);
         return false;
      }

      if(! $this->objEmailOutbounds->SetEmailOutboundsStatus($logID, "1"))
      {
         fwrite($fh, "--SCANNER-- CANNOT SE STATUS for : logID=$logID\n");
         fclose($fh);
         return false;
      }

      if(! $this->CheckEmailOutboundResponse($response))
      {
         fwrite($fh, "--SCANNER-- RESPONSE WAS AN ERROR for : $logID [$response]\n");
         fclose($fh);
         return false;
      }
      else
      {
         fwrite($fh, "--SCANNER-- ALL DONE SUCCESFULLY : $logID [$response] - Settign details in DB \n");
         fclose($fh);
         return true;
      }
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CheckEmailOutboundResponse
   //
   // [DESCRIPTION]:   checks the response from the server
   //
   // [PARAMETERS]:    $response=""
   //
   // [RETURN VALUE]:  true or false in case of failure
   //
   // [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CheckEmailOutboundResponse($response="")
   {

      if(empty($response))
      {
         $this->strERR = GetErrorString("INVALID_RESPONSE_FROM_EMAILVISION");
            return false;
      }

      if(! preg_match("/emailpostsuccessvan\.htm/isU",$response))
      {
         $this->strERR = GetErrorString("INVALID_RESPONSE_PREGMATCH");
            return false;
      }

      return true;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetError
   //
   // [DESCRIPTION]:   Retrieve the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetError()
   {
       return $this->strERR;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ShowError
   //
   // [DESCRIPTION]:   Print the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function ShowError()
   {
       return $this->strERR;
   }
}



?>

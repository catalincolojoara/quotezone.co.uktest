<?php
/////////////////////////////////////////////////////////////////////////////////////////////
//
// class CCachePluginErrors
//
// (C) 2011 Alexandru Furtuna (alex@acrux.biz)
//
/////////////////////////////////////////////////////////////////////////////////////////////

/*
CREATE TABLE `quote_errors_message_cache` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `site_id` bigint(20) unsigned NOT NULL default '0',
  `error_message` text NOT NULL default '',
  `error_no` mediumint(7) NOT NULL default '0',
  `date` date default '0000-00-00',
  PRIMARY KEY  (`id`),
  KEY `site_id` (`site_id`),
  KEY `d_key` (`date`)
) ENGINE=InnoDB
*/

define("SQL_PLUGIN_CACHE_ERRORS",'quote_errors_message_cache');

//include_once "car/modules/globals.inc";
//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CCachePluginErrors
//
// [DESCRIPTION]:  CCachePluginErrors class interface
//
// [FUNCTIONS]:      true  | false AssertCachePluginError()
//                   int   | false AddCachePluginError()
//                   true  | false UpdateCachePluginError()
//                   true  | false DeleteCachePluginError()
//                   array | false GetCachePluginErrorDetailsByErrorMessageSiteIDAndDate()
//                   array | false GetAllCachePluginErrorDetailsByErrorSiteIDAndDate()
//
//                   Close()
//                   GetError()
//                   ShowError()
//
// [CREATED BY]:   Alexandru Furtuna (alex@acrux.biz) 2011-07-26
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CCachePluginErrors
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last Error error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CCachePluginErrors
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    $dbh
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2011-07-26
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CCachePluginErrors($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertCachePluginError
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    $siteID=0, $errorMessage='', $errorNo=0, $date='0000-00-00'
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2011-07-26
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertCachePluginError($siteID=0, $errorMessage='', $errorNo=0, $date='0000-00-00')
{
   $this->strERR = '';

   if(! preg_match("/^\d+$/", $siteID))
      $this->strERR  = GetErrorString("INVALID_PLUGIN_CACHE_PLUGIN_SITE_ID_FIELD");

   if(empty($errorMessage))
      $this->strERR .= GetErrorString("INVALID_PLUGIN_CACHE_ERROR_MESSAGE_FIELD");

   if(! preg_match("/^\d+$/", $errorNo))
      $this->strERR .= GetErrorString("INVALID_PLUGIN_CACHE_ERROR_NO_FIELD");

   if(! preg_match("/\d{4}\-\d{2}\-\d{2}/i",$date))
      $this->strERR .= GetErrorString("INVALID_PLUGIN_CACHE_DATE_FIELD");

   $errorMessage = str_replace("\\","", $errorMessage);
   $errorMessage = str_replace("'","\'", $errorMessage);

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddCachePluginError
//
// [DESCRIPTION]:   Add new entry to the Cache Errors table
//
// [PARAMETERS]:    $siteID=0, $errorMessage='', $errorNo=0, $date='0000-00-00'
//
// [RETURN VALUE]:  false in case of failure or ID in case of success
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2011-07-26
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddCachePluginError($siteID=0, $errorMessage='', $errorNo=0, $date='0000-00-00',$fileName='')
{
   if(! $this->AssertCachePluginError($siteID, $errorMessage, $errorNo, $date))
      return false;

   $this->lastSQLCMD = "INSERT INTO ".SQL_PLUGIN_CACHE_ERRORS." (`site_id`,`error_message`,`error_no`,`date`,`filename`) VALUES ('$siteID','$errorMessage','$errorNo','$date','$fileName')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateCachePluginError
//
// [DESCRIPTION]:   Update Error table
//
// [PARAMETERS]:    $siteID=0, $errorMessage='', $errorNo=0, $date='0000-00-00',$cachePluginErrorID='0'
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2011-07-26
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateCachePluginError($siteID=0, $errorMessage='', $errorNo=0, $date='0000-00-00',$fileName='',$cachePluginErrorID='0')
{
   if(! $this->AssertCachePluginError($siteID, $errorMessage, $errorNo, $date))
      return false;

   if(! preg_match("/^\d+$/", $cachePluginErrorID))
   {
      $this->strERR = GetErrorString("INVALID_CACHE_PLUGIN_ERROR_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_PLUGIN_CACHE_ERRORS." WHERE id='$cachePluginErrorID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("ERRORID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_PLUGIN_CACHE_ERRORS." SET site_id='$siteID',error_message='$errorMessage',error_no='$errorNo',date='$date',filename='$fileName' WHERE id='$cachePluginErrorID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteCachePluginError
//
// [DESCRIPTION]:   Delete an entry from Cache Plugin Error table
//
// [PARAMETERS]:    $cachePluginErrorID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2011-07-26
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteCachePluginError($cachePluginErrorID='')
{
   if(! preg_match("/^\d+$/", $cachePluginErrorID))
   {
      $this->strERR = GetErrorString("INVALID_CACHE_PLUGIN_ERROR_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_PLUGIN_CACHE_ERRORS." WHERE id='$cachePluginErrorID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("CACHE_PLUGIN_ERROR_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_PLUGIN_CACHE_ERRORS." WHERE id='$cachePluginErrorID' limit 1";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCachePluginErrorDetailsByErrorMessageSiteIDAndDate
//
// [DESCRIPTION]:   Read data from Cache Plugin Errors table by error_message 
//                  site_id and date
//
// [PARAMETERS]:    $errorMessage='',$siteID=0,$date='0000-00-00'
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2011-07-26
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCachePluginErrorDetailsByErrorMessageSiteIDAndDate($errorMessage='',$siteID=0,$date='0000-00-00')
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ERROR_ID_FIELD");
      return false;
   }

   if(empty($errorMessage))
   {
      $this->strERR = GetErrorString("INVALID_PLUGIN_CACHE_ERROR_MESSAGE_FIELD");
      return false;
   }

   if(! preg_match("/\d{4}\-\d{2}\-\d{2}/i",$date))
   {
      $this->strERR = GetErrorString("INVALID_PLUGIN_CACHE_DATE_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_PLUGIN_CACHE_ERRORS." WHERE error_message='$errorMessage' AND site_id='$siteID' AND date='$date'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ERROR_ID_FIELD");
      return false;
   }

   $arrayResult = array();
   
   $arrayResult["id"]            = $this->dbh->GetFieldValue("id");
   $arrayResult["site_id"]       = $this->dbh->GetFieldValue("site_id");
   $arrayResult["error_message"] = $this->dbh->GetFieldValue("error_message");
   $arrayResult["error_no"]      = $this->dbh->GetFieldValue("error_no");
   $arrayResult["date"]          = $this->dbh->GetFieldValue("date");
   $arrayResult["filename"]      = $this->dbh->GetFieldValue("filename");

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllCachePluginErrorDetailsByErrorSiteIDAndDate
//
// [DESCRIPTION]:   Read data from Cache Plugin Errors table by site_id and date
//
// [PARAMETERS]:    $errorMessage='',$siteID=0,$date='0000-00-00'
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2011-07-26
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllCachePluginErrorDetailsByErrorSiteIDAndDate($siteID=0,$date='0000-00-00')
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ERROR_ID_FIELD");
      return false;
   }

   if(! preg_match("/\d{4}\-\d{2}\-\d{2}/i",$date))
   {
      $this->strERR = GetErrorString("INVALID_PLUGIN_CACHE_DATE_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_PLUGIN_CACHE_ERRORS." WHERE AND site_id='$siteID' AND date='$date'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ERROR_ID_FIELD");
      return false;
   }

   $arrayResult = array();

   $i = 0;
   while($this->dbh->MoveNext())
   {
      
      $arrayResult[$i]["id"]            = $this->dbh->GetFieldValue("id");
      $arrayResult[$i]["site_id"]       = $this->dbh->GetFieldValue("site_id");
      $arrayResult[$i]["error_message"] = $this->dbh->GetFieldValue("error_message");
      $arrayResult[$i]["error_no"]      = $this->dbh->GetFieldValue("error_no");
      $arrayResult[$i]["date"]          = $this->dbh->GetFieldValue("date");
      $arrayResult[$i]["filename"]      = $this->dbh->GetFieldValue("filename");
   
      $i++;
   }

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllErrorDetailsByInterval
//
// [DESCRIPTION]:   Read data from Cache Plugin Errors table by site_id and date
//                  
// [PARAMETERS]:    $startDate='0000-00-00', $endDate='0000-00-00'
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2011-07-26
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllErrorDetailsByInterval($startDate='0000-00-00', $endDate='0000-00-00')
{

   if(! preg_match("/\d{4}\-\d{2}\-\d{2}/i",$startDate))
   {
      $this->strERR = GetErrorString("INVALID_PLUGIN_CACHE_START_DATE_FIELD");
      return false;
   }

   if(! preg_match("/\d{4}\-\d{2}\-\d{2}/i",$endDate))
   {
      $this->strERR = GetErrorString("INVALID_PLUGIN_CACHE_END_DATE_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT pce.site_id,s.name, pce.error_message, pce.error_no as total_errors,pce.date,pce.filename FROM ".SQL_PLUGIN_CACHE_ERRORS." pce, ".SQL_SITES." s WHERE s.id= pce.site_id AND pce.date BETWEEN '$startDate' AND '$endDate' ORDER BY total_errors DESC";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ERROR_ID_FIELD");
      return false;
   }
   
   $arrayResult     = array();
   $arrayResultInit = array();
   $i               = 0;

   while($this->dbh->MoveNext())
   {
      $arrayResultInit[$this->dbh->GetFieldValue("name")][$this->dbh->GetFieldValue("date")][$i]["site_id"]       = $this->dbh->GetFieldValue("site_id");
      $arrayResultInit[$this->dbh->GetFieldValue("name")][$this->dbh->GetFieldValue("date")][$i]["error_message"] = $this->dbh->GetFieldValue("error_message");
      $arrayResultInit[$this->dbh->GetFieldValue("name")][$this->dbh->GetFieldValue("date")][$i]["total_errors"]  = $this->dbh->GetFieldValue("total_errors");
      $arrayResultInit[$this->dbh->GetFieldValue("name")][$this->dbh->GetFieldValue("date")][$i]["filename"]      = $this->dbh->GetFieldValue("filename");

      $i++;
   }


   foreach($arrayResultInit as $siteName => $siteErrorsDateDetailsArray)
   {
      foreach($siteErrorsDateDetailsArray as $dateOfErros => $siteDetailsContentArray)
      {
         foreach($siteDetailsContentArray as $cnt => $siteDetailsArray)
         {

            $siteError      = "";
            $noOfOccurences = "";
            $fileName       = "";
            $siteIDDb       = "";

            $siteError      = $siteDetailsArray["error_message"];
            $noOfOccurences = $siteDetailsArray["total_errors"];
            $fileName       = $siteDetailsArray["filename"];
            $siteIDDb       = $siteDetailsArray["site_id"];

            $arrayResult[$siteName][$siteError]["total"]   += $noOfOccurences;
            $arrayResult[$siteName][$siteError]["filename"] = $fileName;
            $arrayResult[$siteName][$siteError]["site_id"]  = $siteIDDb;

         }
      }
   }


   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllErrorDetailsBySiteIDAndInterval
//
// [DESCRIPTION]:   Read data from Cache Plugin Errors table 
//                  
// [PARAMETERS]:    $siteID='0', $startDate='0000-00-00', $endDate='0000-00-00'
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2011-07-26
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllErrorDetailsBySiteIDAndInterval($siteID='0', $startDate='0000-00-00', $endDate='0000-00-00')
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ERROR_ID_FIELD");
      return false;
   }

   if(! preg_match("/\d{4}\-\d{2}\-\d{2}/i",$startDate))
   {
      $this->strERR = GetErrorString("INVALID_PLUGIN_CACHE_START_DATE_FIELD");
      return false;
   }

   if(! preg_match("/\d{4}\-\d{2}\-\d{2}/i",$endDate))
   {
      $this->strERR = GetErrorString("INVALID_PLUGIN_CACHE_END_DATE_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT pce.site_id,s.name, pce.error_message, pce.error_no as total_errors,pce.date,pce.filename FROM ".SQL_PLUGIN_CACHE_ERRORS." pce, ".SQL_SITES." s WHERE s.id=pce.site_id AND pce.date BETWEEN '$startDate' and '$endDate' AND pce.site_id='$siteID' ORDER BY pce.site_id,total_errors DESC";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ERROR_ID_FIELD");
      return false;
   }
   
   $arrayResult     = array();
   $arrayResultInit = array();
   $i               = 0;
   while($this->dbh->MoveNext())
   {
      $arrayResultInit[$this->dbh->GetFieldValue("name")][$this->dbh->GetFieldValue("date")][$i]["site_id"]       = $this->dbh->GetFieldValue("site_id");
      $arrayResultInit[$this->dbh->GetFieldValue("name")][$this->dbh->GetFieldValue("date")][$i]["error_message"] = $this->dbh->GetFieldValue("error_message");
      $arrayResultInit[$this->dbh->GetFieldValue("name")][$this->dbh->GetFieldValue("date")][$i]["total_errors"]  = $this->dbh->GetFieldValue("total_errors");
      $arrayResultInit[$this->dbh->GetFieldValue("name")][$this->dbh->GetFieldValue("date")][$i]["filename"]      = $this->dbh->GetFieldValue("filename");

      $i++;
   }

   foreach($arrayResultInit as $siteName => $siteErrorsDateDetailsArray)
   {
      foreach($siteErrorsDateDetailsArray as $dateOfErros => $siteDetailsContentArray)
      {
         foreach($siteDetailsContentArray as $cnt => $siteDetailsArray)
         {

            $siteError      = "";
            $noOfOccurences = "";
            $fileName       = "";
            $siteIDDb       = "";

            $siteError      = $siteDetailsArray["error_message"];
            $noOfOccurences = $siteDetailsArray["total_errors"];
            $fileName       = $siteDetailsArray["filename"];
            $siteIDDb       = $siteDetailsArray["site_id"];

            $arrayResult[$siteName][$siteError]["total"]   += $noOfOccurences;
            $arrayResult[$siteName][$siteError]["filename"] = $fileName;
            $arrayResult[$siteName][$siteError]["site_id"]  = $siteIDDb;

         }
      }
   }


   return $arrayResult;;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMasteSiteIDBySiteID
//
// [DESCRIPTION]:   Get the master site id for a specified site
//
// [PARAMETERS]:    $siteID
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2011-07-26
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMasteSiteIDBySiteID($siteID=0)
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ERROR_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT master_site_id FROM ".SQL_SITES." WHERE id = $siteID";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("INVALID_MASTER_SITE_ID_FIELD");
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $masteSiteID = $this->dbh->GetFieldValue("master_site_id");
   }

   return $masteSiteID;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2011-07-26
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2011-07-26
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Print the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Alexandru Furtuna (alex@acrux.biz) 2011-07-26
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}



}


?>
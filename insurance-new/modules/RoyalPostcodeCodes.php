<?php


//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CSalesData
//
// [DESCRIPTION]:  CSalesData class interface
//
// [FUNCTIONS]:    bool  AssertSales($type)
//                 int   AddSales($type)
//                 bool  DeleteSales($id)
//                 bool  UpdateSales($id,$type)
//                 array GetSaleByID($id)
//                 array GetAllSales()
//
//                 string GetError()
//
// [CREATED BY]:   Adrian AXENTE (adi@acrux.biz) 2006-08-29
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
error_reporting(E_ALL);

class CRoyalPostcodeCodes
{
   var $dbh;
   var $strERR;
   var $closeDB;
   
   function CRoyalPostcodeCodes($dbh=0)
   {  
      if($dbh)
      {
         $this->dbh = $dbh;
         $this->closeDB = false;
      }
      else
      {
         // default configuration
         $this->dbh = new CMySQL();
         
         if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
         {
            $this->strERR = $this->dbh->GetError();
            return;
         }
      
         $this->closeDB = true;
      }
   
      $this->strERR  = "";
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AssertSalesData
   //
   // [DESCRIPTION]:   Verify to see if the requested fields are completed
   //
   // [PARAMETERS]:
   //
   // [RETURN VALUE]:  true or false in case of failure
   //
   // [CREATED BY]:    Adrian AXENTE (adi@acrux.biz) 2006-08-29
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AssertCode($code,$name)
   {
       if(! preg_match("/^\d+/",$code))
       {
          $this->strERR = GetErrorString("INVALID_CODE");
          return false;
       }
      
      return true;
   }
 

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AddSales
   //
   // [DESCRIPTION]:   Add new entry into the trips table
   //
   // [PARAMETERS]: 
   // [RETURN VALUE]:  id | false
   //
   // [CREATED BY]:    Adrian AXENTE (adi@acrux.biz) 2006-08-29
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE 
   function AddCode($code,$name)
   {  

      if(! $this->AssertCode($code,$name))
      {
         $this->strERR=GetErrorString("INVALID_CODE_FIELD");
         return false;
      }  
      
      $this->lastSQLCMD ="INSERT INTO ".SQL_POSTCODE_CODES_TABLE."(`id`,`name`)  VALUES(\"$code\",\"$name\")";
      
      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
          $this->strERR = $this->dbh->GetError();
          return 0;
      }      
      
      $sqlCmd= "SELECT LAST_INSERT_ID() AS id";
   
      if(! $this->dbh->Exec($sqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }
   
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }
   
      $Id = $this->dbh->GetFieldValue("id");

      return $Id;
         
      }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AddSales
   //
   // [DESCRIPTION]:   Add new entry into the trips table
   //
   // [PARAMETERS]: 
   // [RETURN VALUE]:  id | false
   //
   // [CREATED BY]:    Adrian AXENTE (adi@acrux.biz) 2006-08-29
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE 
   function AddCodeAndType($code,$name,$type)
   {  

      if(! $this->AssertCode($code,$name))
      {
         $this->strERR=GetErrorString("INVALID_CODE_FIELD");
         return false;
      }  
      
      $this->lastSQLCMD ="INSERT INTO ".SQL_POSTCODE_CODES_TABLE."(`id`,`name`,`type`)  VALUES(\"$code\",\"$name\",\"$type\")";
      
      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
          $this->strERR = $this->dbh->GetError();
          return 0;
      }      
      
      $sqlCmd= "SELECT LAST_INSERT_ID() AS id";
   
      if(! $this->dbh->Exec($sqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }
   
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }
   
      $Id = $this->dbh->GetFieldValue("id");
      return $Id;
         
      }

  //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AddSales
   //
   // [DESCRIPTION]:   Add new entry into the trips table
   //
   // [PARAMETERS]: 
   // [RETURN VALUE]:  id | false
   //
   // [CREATED BY]:    Adrian AXENTE (adi@acrux.biz) 2006-08-29
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE 
   function AddPostcodeCounties($postcode,$postalCounty,$traditionalCounty,$administrativeCounty)
   {  

      $this->lastSQLCMD ="INSERT INTO ".SQL_POSTCODE_CODES_TABLE."(`postcode`,`postal_county`,`traditional_county`,`administrative_county`)  VALUES(\"$postcode\",\"$postalCounty\",\"$traditionalCounty\",\"$administrativeCounty\")";
      
      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
          $this->strERR = $this->dbh->GetError();
          return 0;
      }      
      
      $sqlCmd= "SELECT LAST_INSERT_ID() AS id";
   
      if(! $this->dbh->Exec($sqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }
   
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }
   
      $Id = $this->dbh->GetFieldValue("id");
      return $Id;
         
      }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AddSales
   //
   // [DESCRIPTION]:   Add new entry into the trips table
   //
   // [PARAMETERS]: 
   // [RETURN VALUE]:  id | false
   //
   // [CREATED BY]:    Adrian AXENTE (adi@acrux.biz) 2006-08-29
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE 
   function AddPostcodes($postCode,$AddressKey,$LocalityKey,$ThoroughfareKey,$ThoroughfareDescriptorKey,$DependentThoroughfareKey,$DependentThoroughfareDescriptorKey,$BuildingNumber,$BuildingNameKey,$SubBuildingNameKey,$NumberofHouseholds,$OrganisationKey,$PostcodeType,$ConcatenationIndicator,$DeliveryPointSufix,$SmallUserOrganisationIndicator,$POBoxnumber)
   {

      $this->lastSQLCMD ="INSERT INTO ".SQL_POSTCODES_TABLE."(`postCode`,`AddressKey`,`LocalityKey`,`ThoroughfareKey`,`ThoroughfareDescriptorKey`,`DependentThoroughfareKey`,`DependentThoroughfareDescriptorKey`,`BuildingNumber`,`BuildingNameKey`,`SubBuildingNameKey`,`NumberofHouseholds`,`OrganisationKey`,`PostcodeType`,`ConcatenationIndicator`,`DeliveryPointSufix`,`SmallUserOrganisationIndicator`,`POBoxnumber`)  VALUES(\"$postCode\",\"$AddressKey\",\"$LocalityKey\",\"$ThoroughfareKey\",\"$ThoroughfareDescriptorKey\",\"$DependentThoroughfareKey\",\"$DependentThoroughfareDescriptorKey\",\"$BuildingNumber\",\"$BuildingNameKey\",\"$SubBuildingNameKey\",\"$NumberofHouseholds\",\"$OrganisationKey\",\"$PostcodeType\",\"$ConcatenationIndicator\",\"$DeliveryPointSufix\",\"$SmallUserOrganisationIndicator\",\"$POBoxnumber\")";
      
      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
          $this->strERR = $this->dbh->GetError();
          return 0;
      }      
      
      $sqlCmd= "SELECT LAST_INSERT_ID() AS id";
   
      if(! $this->dbh->Exec($sqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }
   
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return 0;
      }
   
      $Id = $this->dbh->GetFieldValue("id");
      return $Id;
         
      }
 
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: UpdateTrip
   //
   // [DESCRIPTION]:   Update data from trips table 
   //
   // [PARAMETERS]:    
   // [RETURN VALUE]:  true|false
   //
   // [CREATED BY]:    Adrian AXENTE (adi@acrux.biz) 2006-08-29
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function UpdateCode($code,$name)
   {  
      if(empty($code))
      {
         $this->strERR = GetErrorString("INVALID_CODE");
         return false;
      }
         
      if(! $this->dbh->Exec("UPDATE ".SQL_POSTCODE_CODES_TABLE." SET name='$name' WHERE id='$code'"))
      {
         $this->strERR=GetErrorString("INVALID_CODE");;
         return false;
      }  

      return true;
   }

  
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetCodeByID
   //
   // [DESCRIPTION]:   Read data from trips table and put them into an array variable
   //
   // [PARAMETERS]:    $id=0
   //
   // [RETURN VALUE]:  array|false
   //
   // [CREATED BY]:    Adrian AXENTE (adi@acrux.biz) 2006-08-29
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetCodeByID($code=0)
   {  
      if(empty($code))
      {
         $this-> strERR = GetErrorString("INVALID_CODE");
         return false;
      }
      
      if(! $this->dbh->Exec("SELECT * FROM ".SQL_POSTCODE_CODES_TABLE." WHERE id='$code'"))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }  
       
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }
   
      $arrayResult = array();
      $arrayResult["code"]           = $this->dbh->GetFieldValue("code");
      $arrayResult["name"]           = $this->dbh->GetFieldValue("name");

      return $arrayResult;
   }


  //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetCodeByID
   //
   // [DESCRIPTION]:   Read data from trips table and put them into an array variable
   //
   // [PARAMETERS]:    $id=0
   //
   // [RETURN VALUE]:  array|false
   //
   // [CREATED BY]:    Adrian AXENTE (adi@acrux.biz) 2006-08-29
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetCountyByPostcode($postcode=0)
   {  
      if(empty($postcode))
      {
         $this-> strERR = GetErrorString("INVALID_CODE");
         return false;
      }

      $this->lastSQLCMD = "SELECT postcode,name,type FROM ".POSTCODE_COUNTIES_CODES_TABLE." AS pcc left join ".POSTCODE_COUNTIES_TABLE." as pc on (pcc.postal_county=pc.id or pcc.traditional_county=pc.id or pcc.administrative_county=pc.id ) WHERE pcc.postcode='$postcode'";
      
      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
         $this->strERR=$this->dbh->GetError();
         return false;
      }  
      
      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("SQL_CODES_NOT_FOUND");
         return false;
      }

      $arrayResult = array();
      
      while($this->dbh->MoveNext())
      {   
         $type                     = $this->dbh->GetFieldValue("type");
         $arrayResult[$type]       = $this->dbh->GetFieldValue("name");

      }
  
      return $arrayResult;

   }

  //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetCodeByID
   //
   // [DESCRIPTION]:   Read data from trips table and put them into an array variable
   //
   // [PARAMETERS]:    $id=0
   //
   // [RETURN VALUE]:  array|false
   //
   // [CREATED BY]:    Adrian AXENTE (adi@acrux.biz) 2006-08-29
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetAllCodes()
   {
      $this->lastSQLCMD = "SELECT * FROM ".SQL_POSTCODE_CODES_TABLE;
      
      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
         $this->strERR=$this->dbh->GetError();
         return false;
      }  
      
      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("SQL_CODES_NOT_FOUND");
         return false;
      }
      
      $arrayResult = array();
      
      while($this->dbh->MoveNext())
      {   
         $code                                 = $this->dbh->GetFieldValue("id");
         $arrayResult[$code]["name"]           = $this->dbh->GetFieldValue("name");

      }
  
      return $arrayResult;
   }
  //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetCodeByID
   //
   // [DESCRIPTION]:   Read data from trips table and put them into an array variable
   //
   // [PARAMETERS]:    $id=0
   //
   // [RETURN VALUE]:  array|false
   //
   // [CREATED BY]:    Adrian AXENTE (adi@acrux.biz) 2006-08-29
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetAddresses($postcode,$limit=0)
   {

      //$this->lastSQLCMD = "SELECT pc.id as id ,pc.postCode as postCode, pcl.name as locality, pcth.name as thoroughfares,pcthd.name as thoroughfares_description, BuildingNumber,pcbn.name as building_name,pcsb.name as subbuildings,pco.name as organisation,pc.POBoxnumber as POBoxnumber FROM ".POSTCODES_TABLE." AS pc LEFT JOIN ".POSTCODE_LOCALITIES_TABLE." as pcl ON pc.LocalityKey=pcl.id LEFT JOIN ".POSTCODES_THOROUGHFARES_TABLE." AS pcth on pc.ThoroughfareKey = pcth.id LEFT JOIN ".POSTCODES_THOROUGHFARES_DESCRIPTION_TABLE." AS pcthd ON pc.ThoroughfareDescriptorKey = pcthd.id LEFT JOIN ".POSTCODES_BUILDING_NAME_TABLE." AS pcbn ON pc.BuildingNameKey = pcbn.id LEFT JOIN ".POSTCODES_SUBBUILDINGS_TABLE." AS pcsb ON pc.SubBuildingNameKey = pcsb.id LEFT JOIN ".POSTCODES_ORGANISATIONS_TABLE." As pco ON CONCAT(pc.OrganisationKey,pc.PostcodeType) = pco.id WHERE postCode='$postcode' ORDER BY pc.BuildingNumber ASC";      
      $this->lastSQLCMD = "SELECT 
                              pc.id as id ,
                              pc.postCode as postCode, 
                              pcl.name as locality, 
                              pcth.name as thoroughfares,
                              pcthd.name as thoroughfares_description, 
                              pcdth.name as dependend_thoroughfares,
                              pcdthd.name as dependend_thoroughfares_description, 
                              BuildingNumber,
                              pcbn.name as building_name,
                              pcsb.name as subbuildings,
                              pco.name as organisation,
                              pc.POBoxnumber as POBoxnumber 
                           FROM ".POSTCODES_TABLE." AS pc 
                              LEFT JOIN ".POSTCODE_LOCALITIES_TABLE." as pcl ON pc.LocalityKey=pcl.id 
                              LEFT JOIN ".POSTCODES_THOROUGHFARES_TABLE." AS pcth on pc.ThoroughfareKey = pcth.id 
                              LEFT JOIN ".POSTCODES_THOROUGHFARES_DESCRIPTION_TABLE." AS pcthd ON pc.ThoroughfareDescriptorKey = pcthd.id 
                              LEFT JOIN ".POSTCODES_THOROUGHFARES_TABLE." AS pcdth on pc.DependentThoroughfareKey = pcdth.id 
                              LEFT JOIN ".POSTCODES_THOROUGHFARES_DESCRIPTION_TABLE." AS pcdthd ON pc.DependentThoroughfareDescriptorKey = pcdthd.id 
                              LEFT JOIN ".POSTCODES_BUILDING_NAME_TABLE." AS pcbn ON pc.BuildingNameKey = pcbn.id 
                              LEFT JOIN ".POSTCODES_SUBBUILDINGS_TABLE." AS pcsb ON pc.SubBuildingNameKey = pcsb.id 
                              LEFT JOIN ".POSTCODES_ORGANISATIONS_TABLE." As pco ON CONCAT(pc.OrganisationKey,pc.PostcodeType) = pco.id 
                           WHERE postCode='$postcode' ORDER BY pc.BuildingNumber ASC";      

      //$fd = fopen("/tmp/a.txt","a+");
      //$a = var_export($this->dbh,true);
      //fwrite($fd,$a);
      //fwrite($fd,$this->lastSQLCMD);
      
      if($limit > 0)
          $this->lastSQLCMD .= $this->lastSQLCMD." limit 1";

      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
         $this->strERR=$this->dbh->GetError();
         return false;
      }  
      
      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("SQL_ADDRESS_NOT_FOUND");
         return false;
      }
      
      $arrayResult = array();
      
      while($this->dbh->MoveNext())
      {   
         $id                                                      = $this->dbh->GetFieldValue("id");
         $arrayResult[$id]["postCode"]                            = $this->dbh->GetFieldValue("postCode");
         $arrayResult[$id]["locality"]                            = $this->dbh->GetFieldValue("locality");
         $arrayResult[$id]["thoroughfares"]                       = $this->dbh->GetFieldValue("thoroughfares");
         $arrayResult[$id]["thoroughfares_description"]           = $this->dbh->GetFieldValue("thoroughfares_description");
         $arrayResult[$id]["dependend_thoroughfares"]             = $this->dbh->GetFieldValue("dependend_thoroughfares");
         $arrayResult[$id]["dependend_thoroughfares_description"] = $this->dbh->GetFieldValue("dependend_thoroughfares_description");
         $arrayResult[$id]["BuildingNumber"]                      = $this->dbh->GetFieldValue("BuildingNumber");
         $arrayResult[$id]["building_name"]                       = $this->dbh->GetFieldValue("building_name");
         $arrayResult[$id]["subbuildings"]                        = $this->dbh->GetFieldValue("subbuildings");
         $arrayResult[$id]["organisation"]                        = $this->dbh->GetFieldValue("organisation");
         $arrayResult[$id]["POBoxnumber"]                         =  $this->dbh->GetFieldValue("POBoxnumber");

      }
  
      foreach($arrayResult as $id => $details)
      { 
         $postCode = trim(preg_replace("/\'/",'',$details["postCode"]));
         $locality = trim(preg_replace("/\'/",'',$details["locality"]));
         $thoroughfares = trim(preg_replace("/\'/",'',$details["thoroughfares"]));
         $thoroughfares_description = trim(preg_replace("/\'/",'',$details["thoroughfares_description"]));
         $dependend_thoroughfares = trim(preg_replace("/\'/",'',$details["dependend_thoroughfares"]));
         $dependend_thoroughfares_description = trim(preg_replace("/\'/",'',$details["dependend_thoroughfares_description"]));
         $BuildingNumber = trim(preg_replace("/\'/",'',$details["BuildingNumber"]));
         $building_name =trim(preg_replace("/\'/",'',$details["building_name"]));
         $subbuildings = trim(preg_replace("/\'/",'',$details["subbuildings"]));
         $organisation = trim(preg_replace("/\'/",'',$details["organisation"]));   
         $POBoxnumber = trim(preg_replace("/\'/",'',$details["POBoxnumber"]));
      
         $SingleAddresstext = '';

         if(!empty($organisation))
            $SingleAddresstext .= $organisation." ";
      
         $POBoxnumber = trim($POBoxnumber);
      
         if(!empty($POBoxnumber))
            $SingleAddresstext .= "PO Box ".$POBoxnumber." ";
      
         if(!empty($subbuildings))
            $SingleAddresstext .= $subbuildings." ";
      
         if(!empty($building_name))
         {
            if( ! preg_match("/(\d)+/",$building_name))
               $SingleAddresstext .= $building_name." ";
            else
               $SingleAddresstext .= $building_name." ";
         }     

         if(!empty($BuildingNumber) AND $BuildingNumber != '0000')
         {   
            $BuildingNumber = intval($BuildingNumber);
            $SingleAddresstext .= "$BuildingNumber ";
      
         }
      
         if(!empty($dependend_thoroughfares))
            $SingleAddresstext .= $dependend_thoroughfares." ";
      
         if(!empty($dependend_thoroughfares_description))
            $SingleAddresstext .= $dependend_thoroughfares_description." ";

         if(!empty($thoroughfares))
            $SingleAddresstext .= $thoroughfares." ";
      
         if(!empty($thoroughfares_description))
            $SingleAddresstext .= $thoroughfares_description." ";
      
         if(!empty($locality))
         {
            $LocalArray = preg_split("/\s\s/", trim($locality));
            for($i=count($LocalArray);$i>=0;$i--) 
            {   
               
               if(!empty($LocalArray[$i]))
                  $SingleAddresstext .= $LocalArray[$i]." ";
            
            }
         }

         $addressSplit = preg_split("/(\s)+/", $SingleAddresstext);
         
         $SingleAddress = '';

         foreach($addressSplit as $wordId => $word)
         {

            if(! preg_match("/(\d)+/",$word))
               $word = ucwords(strtolower($word));

            $SingleAddress .= trim($word)." ";
         }

         $arrayResult[$id]['address_line'] = trim(preg_replace("/\'/",'',$SingleAddress));
      }
//print_r($arrayResult);
     return $arrayResult;
   }
 
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: DeleteTrip
   //
   // [DESCRIPTION]:   Delete data from trips table 
   //
   // [PARAMETERS]:    $id
   //
   // [RETURN VALUE]:  true|false
   //
   // [CREATED BY]:    Adrian AXENTE (adi@acrux.biz) 2006-08-29
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function DeleteCode($code=0)
   {
      if(empty($code))
      {
         $this-> strERR = GetErrorString("INVALID_CODE");
         return false;
      }
      
      if(! $this->dbh->Exec("DELETE FROM ".SQL_POSTCODE_CODES_TABLE." WHERE id='$code'"))
      {
         $this->strERR=$this->dbh->GetError();
         return false;
      }  
      
      if($this->dbh->GetAffectedRows()==0)
      {
        $this->strERR = GetErrorString("SQL_CODE_NOT_FOUND");
        return false;
      }     
    
      return true;
      
   }



   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetError
   //
   // [DESCRIPTION]:   Retrieve the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Adrian AXENTE (adi@acrux.biz) 2006-08-29
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetError()
   {
      return $this->strERR;
   }

}

?>

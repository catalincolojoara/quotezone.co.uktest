<?php
/*****************************************************************************/
/*                                                                           */
/*  EmailOutboundsEngine class interface                                           */
/*                                                                           */
/*  (C) 2008 Gabriel ISTVANCSEK (gabi@acrux.biz)                             */
/*                                                                           */
/*****************************************************************************/

include_once "errors.inc";
include_once "FileStoreClient.php";

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CEmailOutboundsEngine
//
// [DESCRIPTION]:  CEmailOutboundsEngine class interface, email outbounds support,
//                 PHP version
//
// [FUNCTIONS]:    CEmailOutboundsEngine($systemType, $fileType)
//                 EmailOutboundsEngineConnect($host="", $port="", $timeout=30)
//                 EmailOutboundsEngineSend($path="", $data="")
//                 SetEmailOutboundsEngineHost($host="")
//                 SetEmailOutboundsEnginePort($port="")
//                 SetEmailOutboundsEnginePath($path="")
//                 GenerateAuthenticityCode
//                 CreateUnsubscribeLinkParams($email,$top,$systemType)
//                 GenerateMimeBoundary()
//                 make_seed()
//                 isNotWaitingQuotes($logID)
//                 LoadSessionFile($fileName)
//                 GetError()
//                 ShowError()
//
//
//  (C) 2008 Gabriel ISTVANCSEK (gabi@acrux.biz) 2008-02-01
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CEmailOutboundsEngine
{
    var $host;        //host where we post data
    var $port;        // the host port
    var $path;        // the host path

    var $fp;

    var $objFileStore;

    var $strERR;      // last error string

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CEmailOutboundsEngine
   //
   // [DESCRIPTION]:   Default class constructor. Initialization goes here.
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CEmailOutboundsEngine($systemType, $fileType)
   {
      // set email vision default configs

      $this->host = "as1.emv2.com";
      $this->path = "/D";

      // ticket - ZDB-464403
      //$this->host = "trc.emv2.com";
      //$this->path = "/D2UTF8";

      $this->port = 80;

      $this->fp   = "";

      $this->strERR  = "";

      $this->objFileStore       = new CFileStoreClient($systemType, $fileType);
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: EmailOutboundsEngineConnect
   //
   // [DESCRIPTION]:   try to connect to the host
   //
//    // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  true or false
   //
   // [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function EmailOutboundsEngineConnect($host="", $port="", $timeout=30)
   {
      if(! empty($host))
         $this->SetEmailOutboundsEngineHost($host);

      if(! empty($port))
         $this->SetEmailOutboundsEnginePort($port);

      if(! $this->fp = fsockopen($this->host, $this->port, $errno, $errstr, $timeout))
      {
         print "Cannot open conection to ".$this->host." on port ".$this->port." with timeout ".$timeout."<br><br>\n\n";
         $this->strERR = "$errno ( $errstr )";
         return false;
      }

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: EmailOutboundsEngineSend($path="", $data="")
   //
   // [DESCRIPTION]:   sendt the details
   //
   // [PARAMETERS]:    $path="", $data=""
   //
   // [RETURN VALUE]:  $response in case of success  false otherwise
   //
   // [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function EmailOutboundsEngineSend($path="", $data="")
   {
      if(! empty($path))
         $this->SetEmailOutboundsEnginePath($path);

      if(empty($data))
      {
         $this->strERR = GetErrorString("INVALID_EMAIL_OUTBOUNDS_ENGINE_DATA");
         return false;
      }

      if(! $this->fp)
      {
         if(! $this->EmailOutboundsEngineConnect())
         {
            $this->strERR = GetErrorString("INVALID_EMAIL_OUTBOUNDS_ENGINE_NOT_CONNECTED_TO_HOST");
            return false;
         }
      }

      fputs($this->fp, "POST ".$this->path." HTTP/1.1\r\n");
      fputs($this->fp, "Host: ".$this->host."\r\n");
      fputs($this->fp, "Content-type: application/x-www-form-urlencoded\r\n");
      fputs($this->fp, "Content-length: ".strlen($data)."\r\n");
      fputs($this->fp, "Connection: close\r\n\r\n");
      fputs($this->fp, $data . "\r\n\r\n");

      $response = "";

      //store the response
      while (!feof($this->fp))
      {
         $response .= fread($this->fp,4096);
      }

      //$date = date("Y-m-d H:i:s");
      //$fp = fopen('/tmp/emv.log', 'a');
      //fwrite($fp, "$date {$this->host}/{$this->path} - $data - $response\n");
      
      fclose($fp);

      return $response;
   }
   
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ScannUnsentEmailOutbounds
   //
   // [DESCRIPTION]:   Checks the usent emails
   //
   // [PARAMETERS]:    $response=""
   //
   // [RETURN VALUE]:  true or false in case of failure
   //
   // [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function ScannUnsentEmailOutbounds($startTime="", $endTime="")
   {

   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: SetEmailOutboundsEngineHost
   //
   // [DESCRIPTION]:   set host details
   //
   // [PARAMETERS]:    $host
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SetEmailOutboundsEngineHost($host="")
   {
      if(empty($host))
      {
         $this->strERR .= GetErrorString("INVALID_EMAIL_OUTBOUNDS_HOST");
         return false;
      }

      $this->host = $host;

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: SetEmailOutboundsEnginePort
   //
   // [DESCRIPTION]:   set host details
   //
   // [PARAMETERS]:    $port
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SetEmailOutboundsEnginePort($port="")
   {
      if(empty($port))
      {
         $this->strERR .= GetErrorString("INVALID_EMAIL_OUTBOUNDS_PORT");
         return false;
      }

      $this->port = $port;

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: SetEmailOutboundsEnginePath
   //
   // [DESCRIPTION]:   set host details
   //
   // [PARAMETERS]:    $path
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SetEmailOutboundsEnginePath($path="")
   {
      if(empty($path))
      {
         $this->strERR .= GetErrorString("INVALID_EMAIL_OUTBOUNDS_PATH");
         return false;
      }

      $this->path = $path;

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GenerateAuthenticityCode
   //
   // [DESCRIPTION]:   generate auth code
   //
   // [PARAMETERS]:    $email
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Florin Menghea (florin@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GenerateAuthenticityCode($email)
   {
      $asciiCodes = array(
         "42" => "-",
         "46" => ".",
         "48" => "0",
         "49" => "1",
         "50" => "2",
         "51" => "3",
         "52" => "4",
         "53" => "5",
         "54" => "6",
         "55" => "7",
         "56" => "8",
         "57" => "9",
         "64" => "@",
         "65" => "A",
         "66" => "B",
         "67" => "C",
         "68" => "D",
         "69" => "E",
         "70" => "F",
         "71" => "G",
         "72" => "H",
         "73" => "I",
         "74" => "J",
         "75" => "K",
         "76" => "L",
         "77" => "M",
         "78" => "N",
         "79" => "O",
         "80" => "P",
         "81" => "Q",
         "82" => "R",
         "83" => "S",
         "84" => "T",
         "85" => "U",
         "86" => "V",
         "87" => "W",
         "88" => "X",
         "89" => "Y",
         "90" => "Z",
         "95" => "_",
         "97" => "a",
         "98" => "b",
         "99" => "c",
         "100" => "d",
         "101" => "e",
         "102" => "f",
         "103" => "g",
         "104" => "h",
         "105" => "i",
         "106" => "j",
         "107" => "k",
         "108" => "l",
         "109" => "m",
         "110" => "n",
         "111" => "o",
         "112" => "p",
         "113" => "q",
         "114" => "r",
         "115" => "s",
         "116" => "t",
         "117" => "u",
         "118" => "v",
         "119" => "w",
         "120" => "x",
         "121" => "y",
         "122" => "z",
      );

    $ourCodes = array(
         "01" => "A",
         "02" => "B",
         "03" => "C",
         "04" => "D",
         "05" => "E",
         "06" => "F",
         "07" => "G",
         "08" => "H",
         "09" => "I",
         "10" => "J",
         "11" => "K",
         "12" => "L",
         "13" => "M",
         "14" => "N",
         "15" => "O",
         "16" => "P",
         "17" => "Q",
         "18" => "R",
         "19" => "S",
         "20" => "T",
         "21" => "U",
         "22" => "V",
         "23" => "W",
         "24" => "X",
         "25" => "Y",
         "26" => "Z",
         "27" => "a",
         "28" => "b",
         "29" => "c",
         "30" => "d",
         "31" => "e",
         "32" => "f",
         "33" => "g",
         "34" => "h",
         "35" => "i",
         "36" => "j",
         "37" => "k",
         "38" => "l",
         "39" => "m",
         "40" => "n",
         "41" => "o",
         "42" => "p",
         "43" => "q",
         "44" => "r",
         "45" => "s",
         "46" => "t",
         "47" => "u",
         "48" => "v",
         "49" => "w",
         "50" => "x",
         "51" => "y",
         "52" => "z",
         "53" => "0",
         "54" => "1",
         "55" => "2",
         "56" => "3",
         "57" => "4",
         "58" => "5",
         "59" => "6",
         "60" => "7",
         "61" => "8",
         "62" => "9",
         "63" => "A",
         "64" => "B",
         "65" => "C",
         "66" => "D",
         "67" => "E",
         "68" => "F",
         "69" => "G",
         "70" => "H",
         "71" => "I",
         "72" => "J",
         "73" => "K",
         "74" => "L",
         "75" => "M",
         "76" => "N",
         "77" => "O",
         "78" => "P",
         "79" => "Q",
         "80" => "R",
         "81" => "S",
         "82" => "T",
         "83" => "U",
         "84" => "V",
         "85" => "W",
         "86" => "X",
         "87" => "Y",
         "88" => "Z",
         "89" => "a",
         "90" => "b",
         "91" => "c",
         "92" => "d",
         "93" => "e",
         "94" => "f",
         "95" => "g",
         "96" => "h",
         "97" => "i",
         "98" => "j",
         "99" => "k",
      );

   $invAsciiCodes = array_flip($asciiCodes);

   // email conversion

   if (!$email)
      $email = "test@test.com";

      $splitEmail = preg_split('//', $email, -1, PREG_SPLIT_NO_EMPTY);
      $encodedMail = '';
      foreach ($splitEmail as $key=>$char)
      {
         $encodedMail .= $invAsciiCodes[$char];
      }

      $password = "quotezone";

      $splitPassw = preg_split('//', $password, -1, PREG_SPLIT_NO_EMPTY);

      $encodedPassw = "";

      foreach ($splitPassw as $key=>$char)
      {
         $encodedPassw .= $invAsciiCodes[$char];
      }

      $splitEncEmail = preg_split('//', $encodedMail, -1, PREG_SPLIT_NO_EMPTY);
      $splitEncPassw = preg_split('//', $encodedPassw, -1, PREG_SPLIT_NO_EMPTY);

      $intercalation1st = "";

      for ($i=0; $i<count($splitEncEmail); $i++)
      {
         if (array_key_exists($i, $splitEncEmail))
            $intercalation1st .= $splitEncEmail[$i];

         if (array_key_exists($i, $splitEncPassw))
            $intercalation1st .= $splitEncPassw[$i];
      }
      $splitInterc1st = preg_split('//', $intercalation1st, -1, PREG_SPLIT_NO_EMPTY);

      $emailParts = explode("@",$email);
      $nrToMultiply = strlen($emailParts[0]);

      $interc1final = "";

      $counter = 1;

      foreach ($splitInterc1st as $key=>$value)
      {
         $interc1final .= $value * $counter;

         $counter++;
         if ($counter>$nrToMultiply)
            $counter = 1;
      }
      $splitInterc1 = preg_split('//', $interc1final, -1, PREG_SPLIT_NO_EMPTY);

      $intercalation2nd = '';

      for ($i=0; $i<count($splitInterc1); $i++)
      {
         if (array_key_exists($i, $splitInterc1))
            $intercalation2nd .= $splitInterc1[$i];

         if (array_key_exists($i, $splitEncPassw))
            $intercalation2nd .= $splitEncPassw[$i];
      }
      $emailParts1 = explode(".", $emailParts[1]);

      $nrToMultiply2 = '';

      for ($i=0; $i<count($emailParts1)-1;$i++)
      {
         $nrToMultiply2 += strlen($emailParts1[$i]);
      }

      $interc2final = "";

      $splitInterc2nd = preg_split('//', $intercalation2nd, -1, PREG_SPLIT_NO_EMPTY);

      $counter = 1;

      foreach ($splitInterc2nd as $key=>$value)
      {
         $interc2final .= $value * $counter;

         $counter++;
         if ($counter>$nrToMultiply2)
            $counter = 1;
      }
      $finalArray = preg_split('//', $interc2final, -1, PREG_SPLIT_NO_EMPTY);
      $resultString = "";
      $counter = count($finalArray)-1;

      for ($i=0;$i<(count($finalArray)/2)-1; $i++)
      {
         $index = $finalArray[$counter].$finalArray[$counter-1];
         $resultString .= $ourCodes[$index];
         $counter-=2;
      }

      return $resultString;

   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CreateUnsubscribeLinkParams($email,$top,$systemType)
   //
   // [DESCRIPTION]:   creates the users unsubscribe link
   //
   // [PARAMETERS]:    $email,$top,$systemType
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Florin Menghea (florin@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CreateUnsubscribeLinkParams($email,$top,$systemType)
   {
      if(empty($email))
      {
         $this->strERR .= GetErrorString("INVALID_EMAIL_OUTBOUNDS_ENGINE_EMAIL");
         return false;
      }

      if(empty($top))
      {
         $this->strERR .= GetErrorString("INVALID_EMAIL_OUTBOUNDS_ENGINE_EMAIL_PARAMS");
         return false;
      }

      if(empty($systemType))
      {
         $this->strERR .= GetErrorString("INVALID_EMAIL_OUTBOUNDS_ENGINE_QUOTE_PARAMS");
         return false;
      }

      $authcode   = $this->GenerateAuthenticityCode(str_replace(";",".",$top)."@".str_replace(";",".",$systemType));
      $emauthcode = $this->GenerateAuthenticityCode($email);

      $resultString = "emailaddress=$email&emauthcode=$emauthcode&email=$top&quote=$systemType&authcode=$authcode";

      return $resultString;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   // [FUNCTION NAME]: GenerateMimeBoundary
   //
   // [DESCRIPTION]:   Generates a MIME boundary string, outlook express like
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  the MIME boundary string
   //
   // [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GenerateMimeBoundary()
   {
      $garbleText = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','R','S','T','U','V','X','Y','W','Z');

      $maxIndex = count($garbleText) - 1;
      $mimeBoundary = "----=_NextPart_";

      mt_srand($this->make_seed());
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= "_";

      mt_srand($this->make_seed());
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= "_";

      mt_srand($this->make_seed());
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= ".";

      mt_srand($this->make_seed());
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
      $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];

      return $mimeBoundary;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: make_seed
   //
   // [DESCRIPTION]:   generate random string
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  return string
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function make_seed()
   {
      list($usec, $sec) = explode(' ', microtime());
      return (float) $sec + ((float) $usec * 100000000);
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: isNotWaitingQuotes($logID)
   //
   // [DESCRIPTION]:   Checks if the quote status is waiting
   //
   // [PARAMETERS]:    $logID
   //
   // [RETURN VALUE]:  true or false
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function isNotWaitingQuotes($logID)
   {

      if(! preg_match("/^\d+$/", $logID))
      {
         $this->strERR = GetErrorString('INVALID_EMAIL_OUTBOUNDS_LOG_ID');
         return false;
      }

      $sqlCmd = "SELECT count(*) AS cnt FROM quote_status WHERE log_id='$logID' AND status='WAITING'";

      if(! $this->dbh->Exec($sqlCmd,true))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      $totalWaitingQuotes = $this->dbh->GetFieldValue('cnt');

      if($totalWaitingQuotes)
         return false;

      return true;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: LogWasSentOk($logID)
   //
   // [DESCRIPTION]:   Checks if the log_id is in the email_outbounds table and was 
   //                  sent correctly (in case of duplicate posts)
   //
   // [PARAMETERS]:    $logID
   //
   // [RETURN VALUE]:  true or false
   //
   // [CREATED BY]:    Furtuna Alexandru (alex@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function LogWasSentOk($logID)
   {

      if(! preg_match("/^\d+$/", $logID))
      {
         $this->strERR = GetErrorString('INVALID_EMAIL_OUTBOUNDS_LOG_ID');
         return false;
      }

      $sqlCmd = "SELECT count(*) AS cnt FROM email_outbounds WHERE log_id='$logID' AND status='1'";

      if(! $this->dbh->Exec($sqlCmd,true))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      $totalSent = $this->dbh->GetFieldValue('cnt');

      if($totalSent)
         return false;

      return true;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: LoadSessionFile($fileName)
   //
   // [DESCRIPTION]:   Loads the file details in the session
   //
   // [PARAMETERS]:    $fileName
   //
   // [RETURN VALUE]:  $Session or false in case it cannot find the file
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function LoadSessionFile($fileName)
   {
      if(! $data = $this->objFileStore->GetFile($fileName))
      {
         print "Cannot get the file form filestore \n\n\n"; 
         return false;
      }
      eval("\$Session = $data;");

      return $Session;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetError
   //
   // [DESCRIPTION]:   Retrieve the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetError()
   {
       return $this->strERR;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ShowError
   //
   // [DESCRIPTION]:   Print the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function ShowError()
   {
       return $this->strERR;
   }
}

?>

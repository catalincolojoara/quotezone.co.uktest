<?php

/*****************************************************************************/
/*                                                                           */
/*  CSiteFilter class interface                                                */
/*                                                                           */
/*  (C) 2008 Istvancsek Gabriel (gabi@acrux.biz)                             */
/*                                                                           */
/*****************************************************************************/

include_once "errors.inc";
include_once "MySQL.php";

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CSiteFilter
//
// [DESCRIPTION]:  CSiteFilter class interface
//
// [FUNCTIONS]:      CSiteFilter($dbh=0)
//                   AssertSiteFilter($siteID, $filteredQuotes, $totalQuotes, $top)
//                   AddSiteFilter($siteID, $filteredQuotes, $totalQuotes, $top)
//                   UpdateSiteFilter($siteFilterID, $siteID, $filteredQuotes, $totalQuotes, $top)
//                   DeleteSiteFilter($siteFilterID)
//                   DeleteAllSiteFilter()
//                   GetSiteFilter($siteFilterID)
//                   GetAllSiteFilter($quoteTypeID)
//                   Close()
//                   GetError()
//                   ShowError()
// 
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CSiteFilter
{

    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string
   

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CSiteFilter
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function CSiteFilter($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertSiteFilter
//
// [DESCRIPTION]:   assert data fields
//
// [PARAMETERS]:    $userID, $systemType,$quoteTypeID,$rate,$quoteLevel
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function AssertSiteFilter($siteID, $filteredQuotes, $totalQuotes, $top, $status)
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITE_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $filteredQuotes))
   {
      $this->strERR = GetErrorString("INVALID_FILTERED_QUOTES_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $totalQuotes))
   {
      $this->strERR = GetErrorString("INVALID_TOTAL_QUOTES_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $top))
   {
      $this->strERR = GetErrorString("INVALID_TOP_FIELD");
      return false;
   }
   
   if(! preg_match("/^ON|OFF$/", $status))
   {
      $this->strERR = GetErrorString("INVALID_STATUS_FIELD");
      return false;
   }
   
   
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddSiteFilter
//
// [DESCRIPTION]:   add data into user rates
//
// [PARAMETERS]:    $siteID, $filteredQuotes, $totalQuotes, $top
//
// [RETURN VALUE]:  int | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2010-06-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function AddSiteFilter($siteID, $filteredQuotes, $totalQuotes, $top, $status)
{
   if(! $this->AssertSiteFilter($siteID, $filteredQuotes, $totalQuotes, $top, $status))
      return false;
   
   $sqlCmd = " SELECT * FROM ".SQL_SITE_FILTERS." WHERE site_id='$siteID' ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if($this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITE_FILTER_FOUND");
      return false;
   }

   $sqlCmd = " INSERT INTO ".SQL_SITE_FILTERS." (site_id,filtered_quotes,total_quotes,top,status) VALUES('$siteID','$filteredQuotes','$totalQuotes','$top','$status') ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddSiteFilter
//
// [DESCRIPTION]:   add data into user rates
//
// [PARAMETERS]:    $siteID, $filteredQuotes, $totalQuotes, $top
//
// [RETURN VALUE]:  int | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2010-06-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function UpdateSiteFilter($siteFilterID, $siteID, $filteredQuotes, $totalQuotes, $top, $status)
{
   if(! preg_match("/^\d+$/", $siteFilterID))
   {
      $this->strERR = GetErrorString("INVALID_SITE_FILTER_ID_FIELD");
      return false;
   }

   if(! $this->AssertSiteFilter($siteID, $filteredQuotes, $totalQuotes, $top, $status))
      return false;
   
   $sqlCmd = " UPDATE ".SQL_SITE_FILTERS." SET site_id='$siteID',filtered_quotes='$filteredQuotes',total_quotes='$totalQuotes',top='$top',status='$status' WHERE id='$siteFilterID' ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteSiteFilter
//
// [DESCRIPTION]:   delete entry from USER_SYSTEMs by user rate id
//
// [PARAMETERS]:    $siteFilterID = ""
//
// [RETURN VALUE]:  false|true
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function DeleteSiteFilter($siteFilterID)
{
   if(! preg_match("/^\d+$/", $siteFilterID))
   {
      $this->strERR = GetErrorString("INVALID_SITE_FILTER_ID_FIELD");
      return false;
   }

   $sqlCmd = " DELETE FROM ".SQL_SITE_FILTERS." WHERE id='$siteFilterID' ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteAllSiteFilter
//
// [DESCRIPTION]:   delete all entries from USER_SYSTEMs by userID
//
// [PARAMETERS]:    $userID = ""
//
// [RETURN VALUE]:  false|true
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function DeleteAllSiteFilter()
{

   $sqlCmd = " DELETE FROM ".SQL_SITE_FILTERS." ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSiteFilter
//
// [DESCRIPTION]:   Read data from USER_SYSTEMs table and put it into an array variable
//
// [PARAMETERS]:    $siteFilterID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function GetSiteFilter($siteFilterID)
{
   if(! preg_match("/^\d+$/", $siteFilterID))
   {
      $this->strERR = GetErrorString("INVALID_SITE_FILTER_ID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT sf.id as site_filter_id, s.*, qz.name as type, sf.filtered_quotes, sf.total_quotes, sf.top, sf.site_id, sf.status FROM ".SQL_SITE_FILTERS." sf, ".SQL_SITES." s, ".SQL_QZQUOTE_TYPES." qz WHERE sf.site_id=s.id and s.quote_type_id=qz.id and sf.id='$siteFilterID' ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITE_FILTER_ID_NOT_FOUND");
      return false;
   }
  
   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("USER_SYSTEM_ID_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]              = $this->dbh->GetFieldValue("site_filter_id");
   $arrayResult["site_id"]         = $this->dbh->GetFieldValue("site_id");
   $arrayResult["name"]            = $this->dbh->GetFieldValue("name");
   $arrayResult["quote_type_id"]   = $this->dbh->GetFieldValue("quote_type_id");
   $arrayResult["proto"]           = $this->dbh->GetFieldValue("proto");
   $arrayResult["description"]     = $this->dbh->GetFieldValue("description");
   $arrayResult["results"]         = $this->dbh->GetFieldValue("results");
   $arrayResult["master_site_id"]  = $this->dbh->GetFieldValue("master_site_id");
   $arrayResult["status"]          = $this->dbh->GetFieldValue("status");
   $arrayResult["type"]            = $this->dbh->GetFieldValue("type");
   $arrayResult["returns"]         = $this->dbh->GetFieldValue("returns");
   $arrayResult["broker"]          = $this->dbh->GetFieldValue("broker");
   $arrayResult["broker_id"]       = $this->dbh->GetFieldValue("broker_id");

   $arrayResult["filtered_quotes"] = $this->dbh->GetFieldValue("filtered_quotes");
   $arrayResult["total_quotes"]    = $this->dbh->GetFieldValue("total_quotes");
   $arrayResult["top"]             = $this->dbh->GetFieldValue("top");
   $arrayResult["status"]          = $this->dbh->GetFieldValue("status");
   
   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSiteFilter
//
// [DESCRIPTION]:   Read data from USER_SYSTEMs table and put it into an array variable
//
// [PARAMETERS]:    $quoteTypeID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function GetAllSiteFilter($quoteTypeID)
{
   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT sf.id, s.name FROM ".SQL_SITE_FILTERS." sf LEFT JOIN ".SQL_SITES." s ON (sf.site_id=s.id) WHERE s.quote_type_id='$quoteTypeID' ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
  
   $arrayResult = array();
   
   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("name");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllSiteFilterDetails
//
// [DESCRIPTION]:   Read data from USER_SYSTEMs table and put it into an array variable
//
// [PARAMETERS]:    $quoteTypeID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function GetAllSiteFilterDetails($quoteTypeID)
{
   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT s.id as site_id, sf.* FROM ".SQL_SITE_FILTERS." sf LEFT JOIN ".SQL_SITES." s ON (sf.site_id=s.id) WHERE s.quote_type_id='$quoteTypeID' AND sf.status='ON' ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
  
   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("site_id")]['filtered_quotes'] = $this->dbh->GetFieldValue("filtered_quotes");
      $arrayResult[$this->dbh->GetFieldValue("site_id")]['total_quotes']    = $this->dbh->GetFieldValue("total_quotes");
      $arrayResult[$this->dbh->GetFieldValue("site_id")]['top']             = $this->dbh->GetFieldValue("top");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSiteFilter
//
// [DESCRIPTION]:   Read data from USER_SYSTEMs table and put it into an array variable
//
// [PARAMETERS]:    $quoteTypeID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function GetAllSite($quoteTypeID)
{
   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT s.id, s.name FROM ".SQL_SITE_FILTERS." sf LEFT JOIN ".SQL_SITES." s ON (sf.site_id=s.id) WHERE s.quote_type_id='$quoteTypeID' ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
  
   $arrayResult = array();
   
   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("name");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSiteFilterCount
//
// [DESCRIPTION]:   Get number of sites
//
// [PARAMETERS]:    $cnt
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSiteFilterCount($quoteTypeID="0")
{
   $sqlCmd = "SELECT COUNT(sf.id) AS cnt FROM ".SQL_SITE_FILTERS." sf LEFT JOIN ".SQL_SITES." s ON (sf.site_id=s.id) ";

   if($quoteTypeID)
      $sqlCmd .= "WHERE s.quote_type_id=$quoteTypeID";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
      return 0;

   $cnt = $this->dbh->GetFieldValue("cnt");

   if(! $cnt)
      $cnt = 0;

   return $cnt;

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SearchFilterSiteByName
//
// [DESCRIPTION]:   Read data from sites table and put it into an array variable
//
// [PARAMETERS]:    $siteName="", $limit=0, $offset=0, $quoteTypeID=0
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Ciprian Sturza (cipi@acrux.biz) 2007-02-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SearchFilterSiteByName($siteName="", $limit=0, $offset=0, $quoteTypeID=0)
{
   if($quoteTypeID != "0")
      $type = " AND s.quote_type_id=$quoteTypeID";

   $sqlCmd= "SELECT sf.id as site_filter_id, s.*, qz.name as type, sf.filtered_quotes, sf.total_quotes, sf.top, sf.status FROM ".SQL_SITE_FILTERS." sf, ". SQL_SITES." s, ".SQL_QZQUOTE_TYPES." qz WHERE sf.site_id=s.id AND s.quote_type_id=qz.id $type AND s.name LIKE '%$siteName%' ORDER BY s.name";

   if($limit)
   {
      if($offset)
         $sqlCmd .= " LIMIT $offset,$limit";
      else
         $sqlCmd .= " LIMIT $limit";
   }

   if(! $this->dbh->Exec($sqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITENAME_NOT_FOUND");
      return false;
   }

    while($this->dbh->MoveNext())
   {
      $id                                  = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["id"]              = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["quote_type_id"]   = $this->dbh->GetFieldValue("quote_type_id");
      $arrayResult[$id]["name"]            = $this->dbh->GetFieldValue("name");
      $arrayResult[$id]["proto"]           = $this->dbh->GetFieldValue("proto");
      $arrayResult[$id]["description"]     = $this->dbh->GetFieldValue("description");
      $arrayResult[$id]["results"]         = $this->dbh->GetFieldValue("results");
      $arrayResult[$id]["master_site_id"]  = $this->dbh->GetFieldValue("master_site_id");
      $arrayResult[$id]["status"]          = $this->dbh->GetFieldValue("status");
      $arrayResult[$id]["type"]            = $this->dbh->GetFieldValue("type");
      $arrayResult[$id]["returns"]         = $this->dbh->GetFieldValue("returns");
      $arrayResult[$id]["broker"]          = $this->dbh->GetFieldValue("broker");
      $arrayResult[$id]["broker_id"]       = $this->dbh->GetFieldValue("broker_id");

      $arrayResult[$id]["filtered_quotes"] = $this->dbh->GetFieldValue("filtered_quotes");
      $arrayResult[$id]["total_quotes"]    = $this->dbh->GetFieldValue("total_quotes");
      $arrayResult[$id]["top"]             = $this->dbh->GetFieldValue("top");
      $arrayResult[$id]["status"]          = $this->dbh->GetFieldValue("status");
   }

   return $arrayResult;

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllSiteFilters
//
// [DESCRIPTION]:   Read data from sites table and put it into an array variable
//                  key = siteID, value = siteName
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllSiteFilters($offset=0, $limit=0, $quoteTypeID="0")
{
   if(! preg_match("/^\d+$/", $offset))
   {
      $this->strERR = GetErrorString("INVALID_OFFSET_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $limit))
   {
      $this->strERR = GetErrorString("INVALID_LIMIT_FIELD");
      return false;
   }

   if($quoteTypeID != "0")
      $type = " AND s.quote_type_id=$quoteTypeID";

   $sqlCmd = "SELECT sf.id as site_filter_id, s.*, qz.name as type, sf.filtered_quotes, sf.total_quotes, sf.top, sf.status FROM ".SQL_SITE_FILTERS." sf, ".SQL_SITES." s, ".SQL_QZQUOTE_TYPES." qz WHERE sf.site_id=s.id and s.quote_type_id=qz.id $type ORDER BY s.name";

   if($offset || $limit)
   {
      $sqlCmd.= " LIMIT $offset";

      if($limit)
         $sqlCmd.= ",$limit";
      else
         $sqlCmd.= ",-1";
   }

   if(! $this->dbh->Exec($sqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITES_NOT_FOUND");
      return false;
   }

   $cnt = 0;
   while($this->dbh->MoveNext())
   {
      $cnt++;
      $arrayResult[$cnt]["id"]              = $this->dbh->GetFieldValue("id");
      $arrayResult[$cnt]["site_filter_id"]  = $this->dbh->GetFieldValue("site_filter_id");
      $arrayResult[$cnt]["name"]            = $this->dbh->GetFieldValue("name");
      $arrayResult[$cnt]["quote_type_id"]   = $this->dbh->GetFieldValue("quote_type_id");
      $arrayResult[$cnt]["proto"]           = $this->dbh->GetFieldValue("proto");
      $arrayResult[$cnt]["type"]            = $this->dbh->GetFieldValue("type");
      $arrayResult[$cnt]["description"]     = $this->dbh->GetFieldValue("description");
      $arrayResult[$cnt]["results"]         = $this->dbh->GetFieldValue("results");
      $arrayResult[$cnt]["master_site_id"]  = $this->dbh->GetFieldValue("master_site_id");
      $arrayResult[$cnt]["status"]          = $this->dbh->GetFieldValue("status");
      $arrayResult[$cnt]["returns"]         = $this->dbh->GetFieldValue("returns");
      $arrayResult[$cnt]["broker"]          = $this->dbh->GetFieldValue("broker");
      $arrayResult[$cnt]["broker_id"]       = $this->dbh->GetFieldValue("broker_id");

      $arrayResult[$cnt]["filtered_quotes"] = $this->dbh->GetFieldValue("filtered_quotes");
      $arrayResult[$cnt]["total_quotes"]    = $this->dbh->GetFieldValue("total_quotes");
      $arrayResult[$cnt]["top"]             = $this->dbh->GetFieldValue("top");
      $arrayResult[$cnt]["status"]          = $this->dbh->GetFieldValue("status");
   }

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSearchSiteFilterCount
//
// [DESCRIPTION]:   Get number of searched 
//
// [PARAMETERS]:    IN:  $search=''
//                  OUT: $cnt
//
// [RETURN VALUE]:  int | false in case search failed
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSearchSiteFilterCount($search='')
{
   $sqlCmd= "SELECT count(*) AS cnt FROM ".SQL_SITE_FILTERS." LEFT JOIN WHERE ip LIKE '%$search%'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
      return 0;

   $cnt = $this->dbh->GetFieldValue("cnt");

   if(! $cnt)
      $cnt = 0;

   return $cnt;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

}

?>

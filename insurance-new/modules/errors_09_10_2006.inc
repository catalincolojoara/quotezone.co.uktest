<?php

////////////////////////////////////////////////////////////////////////////////
///                              Errors area                                 ///
////////////////////////////////////////////////////////////////////////////////

function GetErrorString($error="")
{
   $_errMsg = array();

   $_errMsg["UNDEFINED_ERROR"] = "Undefined error: .";

   // User errors
   $_errMsg["INVALID_USERID_FIELD"]            = "Invalid \"user ID\" field. Must be > 0 and it must contain only numbers!.";
   $_errMsg["INVALID_EMAIL_FIELD"]             = "Invalid \"email\" field!.";
   $_errMsg["INVALID_PASSWORD_FIELD"]          = "Invalid \"password\" field!.";
   $_errMsg["PASSWORD_NOT_MATCH"]              = "Sorry, passwords do not match!.";
   $_errMsg["PASSWORDS_NOT_MATCH"]             = "Sorry, passwords do not match!.";
   $_errMsg["INVALID_ADDRESS_FIELD"]           = "Invalid \"address\" field!.";
   $_errMsg["INVALID_USER_NAME_FIELD"]         = "Invalid \"user name\" field!.";
   $_errMsg["INVALID_POSTAL_CODE_FIELD"]       = "Invalid \"postal code\" field!.";
   $_errMsg["INVALID_STATE_FIELD"]             = "Invalid \"state\" field!.";
   $_errMsg["INVALID_COUNTRY_FIELD"]           = "Invalid \"country\" field!.";
   $_errMsg["INVALID_PHONE_FIELD"]             = "Invalid \"phone\" field!.";
   $_errMsg["INVALID_FAX_FIELD"]               = "Invalid \"fax\" field!.";
   $_errMsg["INVALID_LOCKED_FIELD"]            = "Invalid \"locked\" field!.";
   $_errMsg["USERID_NOT_FOUND"]                = "User ID not found!.";
   $_errMsg["USERS_NOT_FOUND"]                 = "No users found in database!.";
   $_errMsg["USER_PROFILES_NOT_FOUND"]         = "No profiles found for the specified User ID!.";
   $_errMsg["USERID_ALREADY_EXISTS"]           = "User ID already exists!.";
   $_errMsg["USER_FAILED_UPDATE"]              = "Update user failed!.";
   $_errMsg["USER_FAILED_DELETE"]              = "Delete user failed!.";
   $_errMsg["AUTHENTICATION_FAILED"]           = "Authentication failed!.";

   // Profile errors
   $_errMsg["INVALID_PROFILEID_FIELD"]         = "Invalid \"profile ID\" field. Must be > 0 and it must contain only numbers!.";
   $_errMsg["INVALID_PROFILE_NAME_FIELD"]      = "Invalid \"profile name\" field!.";
   $_errMsg["PROFILEID_NOT_FOUND"]             = "Profile ID not found!.";
   $_errMsg["PROFILES_NOT_FOUND"]              = "No profiles found for the specified user ID!.";
   $_errMsg["PROFILEID_ALREADY_EXISTS"]        = "Profile ID already exists!.";
   $_errMsg["PROFILE_NAME_ALREADY_EXISTS"]     = "Profile name already exists!.";
   $_errMsg["PROFILE_FAILED_UPDATE"]           = "Update profile failed!.";
   $_errMsg["PROFILE_FAILED_DELETE"]           = "Delete profile failed!.";

   // Profile settings errors
   $_errMsg["INVALID_PRSETTINGS_NAME_FIELD"]   = "Invalid \"profile settings name\" field!.";
   $_errMsg["INVALID_PRSETTINGS_VALUE_FIELD"]  = "Invalid \"profile settings value\" field!.";
   $_errMsg["PRSETTINGSID_ALREADY_EXISTS"]     = "Profile settings ID already exists!.";
   $_errMsg["PRSETTINGS_KEY_ALREADY_EXISTS"]   = "Profile settings key already exists!.";
   $_errMsg["PRSETTINGS_FAILED_UPDATE"]        = "Update profile settings failed!.";
   $_errMsg["PRSETINGS_FAILED_DELETE"]         = "Delete profile settings failed!.";

   // Site errors
   $_errMsg["INVALID_SITEID_FIELD"]            = "Invalid \"site ID\" field. Must be > 0 and it must contain only numbers!.";
   $_errMsg["INVALID_SITE_NAME_FIELD"]         = "Invalid \"site name\" field. Must be a valid web address without http:// or https:// !.";
   $_errMsg["SITEID_NOT_FOUND"]                = "Site ID not found!.";
   $_errMsg["SITEID_ALREADY_EXISTS"]           = "Site ID already exists!.";
   $_errMsg["SITE_FAILED_UPDATE"]              = "Update site failed!.";
   $_errMsg["SITE_FAILED_DELETE"]              = "Delete site failed!.";
   $_errMsg["SITES_NOT_FOUND"]                 = "No sites found in database!.";

   // Log errors
   $_errMsg["INVALID_LOGID_FIELD"]             = "Invalid \"log ID\" field. Must be > 0 and it must contain only numbers!.";
   $_errMsg["INVALID_LOG_URL_FIELD"]           = "Invalid \"log URL\" field. Must be a valid web address without http:// or https:// !.";
   $_errMsg["INVALID_LOG_HOSTIP_FIELD"]        = "Invalid \"log Host IP\" field. Must be a valid host IP address: 192.168.1.1  !.";
   $_errMsg["INVALID_LOG_DATE_FIELD"]          = "Invalid \"log DATE\" field. Must be a valid ISO date: YYYY-MM-DD hh:mm:ss !.";
   $_errMsg["LOGID_NOT_FOUND"]                 = "Log ID not found!.";
   $_errMsg["LOG_FAILED_UPDATE"]               = "Update log failed!.";
   $_errMsg["LOG_FAILED_DELETE"]               = "Delete log failed!.";

   // Url errors
   $_errMsg["INVALID_URLID_FIELD"]             = "Invalid \"url ID\" field. Must be > 0 and it must contain only numbers!.";
   $_errMsg["INVALID_URL_FIELD"]               = "Invalid \"url\" field. Must be a valid web address!.";
   $_errMsg["INVALID_SITEID_FIELD"]            = "Invalid \"site ID\" field. Must be > 0 and it must contain only numbers!.";
   $_errMsg["INVALID_URL_PARAMID_FIELD"]       = "Invalid url \"param ID\" field. Must be > 0 and it must contain only numbers!.";
   $_errMsg["INVALID_URL_RESPONSEID_FIELD"]    = "Invalid url \"response ID\" field. Must be > 0 and it must contain only numbers!.";
   $_errMsg["INVALID_PARAM_TYPE_FIELD"]        = "Invalid param \"type\" field. Must be either \"GET\" or \"POST\"!.";
   $_errMsg["INVALID_PARAM_NAME_FIELD"]        = "Invalid param \"name\" field!.";
   $_errMsg["INVALID_RESPONSE_FIELD"]          = "Invalid response field!.";
   $_errMsg["INVALID_RESPONSE_TYPE_FIELD"]     = "Invalid response \"type\" field. Must be either \"SUCCESS\" or \"FAILURE\"!.";
   $_errMsg["URLID_NOT_FOUND"]                 = "Url ID not found!.";
   $_errMsg["URLS_NOT_FOUND"]                  = "No urls found in database for the specified site ID!.";
   $_errMsg["URL_PARAMID_NOT_FOUND"]           = "Url param ID not found!.";
   $_errMsg["URL_RESPONSEID_NOT_FOUND"]        = "Url response ID not found!.";
   $_errMsg["URL_PARAMS_NOT_FOUND"]            = "No params found for the specified Url ID!.";
   $_errMsg["URL_RESPONSES_NOT_FOUND"]         = "No responses found for the specified Url ID!.";
   $_errMsg["URL_ASSUMPTSIONS_NOT_FOUND"]      = "No assumption found for the specified Url ID!.";
   $_errMsg["URLID_PARAM_NAME_NOT_FOUND"]      = "Url ID param name not found!.";
   $_errMsg["URLID_ALREADY_EXISTS"]            = "Url ID already exists!.";
   $_errMsg["URL_PARAM_NAME_ALREADY_EXISTS"]   = "Url param \"name\" already exists!.";
   $_errMsg["URL_RESPONSE_ALREADY_EXISTS"]     = "Url response already exists!.";
   $_errMsg["URL_FAILED_UPDATE"]               = "Update url failed!.";
   $_errMsg["URL_FAILED_DELETE"]               = "Delete url failed!.";
   $_errMsg["URL_PARAM_FAILED_UPDATE"]         = "Update url param failed!.";
   $_errMsg["URL_PARAM_FAILED_DELETE"]         = "Delete url param failed!.";
   $_errMsg["URL_RESPONSE_FAILED_UPDATE"]      = "Update url response failed!.";
   $_errMsg["URL_RESPONSE_FAILED_DELETE"]      = "Delete url response failed!.";

   // Params Session errors
   $_errMsg["INVALID_PARAM_SESSION_NAME_FIELD"] = "Invalid param session \"name\" field!.";
   $_errMsg["PARAM_SESSION_NAME_ALREADY_EXISTS"]= "Param session \"name\" exist!.";
   $_errMsg["PARAM_SESSION_ID_NOT_FOUND"]      = "Param session ID not found!.";

   // RSite errors
   $_errMsg["PARSE_EMPTY_HTML_PAGE"]           = "Cannot parse an empty HTML page!.";


// Params Errors
// DRIVERS
// About You
   $_errMsg["INVALID_TITLE"]                                                     = "Please choose a title.";
   $_errMsg["INVALID_FIRST_NAME"]                                                = "Please enter your name.";
   $_errMsg["INVALID_SURNAME"]                                                   = "Please enter your surname.";
   $_errMsg["INVALID_DATE_OF_BIRTH_DAY"]                                         = "Please ensure the day of your birth is correct.";
   $_errMsg["INVALID_DATE_OF_BIRTH_MONTH"]                                       = "Please ensure the month of your birth is correct.";
   $_errMsg["INVALID_DATE_OF_BIRTH_YEAR"]                                        = "Please ensure the year of your birth is correct.";
   $_errMsg["INVALID_BORN_IN_THE_UK"]                                            = "Please choose an answer.";
   $_errMsg["INVALID_DATE_YOU_BECAME_UK_RESIDENT_DAY"]                           = "Please ensure the day you became UK resident is correct.";
   $_errMsg["INVALID_DATE_YOU_BECAME_UK_RESIDENT_MONTH"]                         = "Please ensure the month you became UK resident is correct.";
   $_errMsg["INVALID_DATE_YOU_BECAME_UK_RESIDENT_YEAR"]                          = "Please ensure the year you became UK resident is correct.";
   $_errMsg["INVALID_SEX_TYPE"]                                                  = "Please choose a type.";
   $_errMsg["INVALID_MARITAL_STATUS"]                                            = "Please choose a status.";
   $_errMsg["INVALID_RELATIONSHIP_TO_PROPOSER"]                                  = "Please choose a status.";
   $_errMsg["INVALID_NUMBER_OF_CHILDREN"]                                        = "Please choose a number.";
// end About You
// Your Contact Details
   $_errMsg["INVALID_HOME_TELEPHONE_PREFIX"]      = "Please enter your telephone number prefix.";
   $_errMsg["INVALID_HOME_TELEPHONE_NUMBER"]      = "Please enter your telephone number.";
   $_errMsg["INVALID_DAYTIME_TELEPHONE_PREFIX"]   = "Please enter your telephone number prefix.";
   $_errMsg["INVALID_DAYTIME_TELEPHONE_NUMBER"]   = "Please enter your telephone number.";
   $_errMsg["INVALID_MOBILE_TELEPHONE_PREFIX"]    = "Please enter your mobile number prefix or leave empty.";
   $_errMsg["INVALID_MOBILE_TELEPHONE_NUMBER"]    = "Please enter your mobile number or leave empty.";
   $_errMsg["INVALID_MOBILE_TELEPHONE"]           = "Please enter a valid mobile phone number or leave empty.";

   $_errMsg["INVALID_DAYTIME_PHONE_NUMBER_LENGTH"]    = "Please enter a valid daytime telephone number.";
   $_errMsg["INVALID_HOME_PHONE_NUMBER_LENGTH"]       = "Please enter a valid evening telephone number.";

   $_errMsg["INVALID_DAYTIME_PHONE_NUMBER_MANDATORY"] = "Please enter a valid daytime or evening telephone number.";
   $_errMsg["INVALID_HOME_PHONE_NUMBER_MANDATORY"]    = "Please enter a valid evening or daytime telephone number.";

   $_errMsg["INVALID_DAYTIME_DIAL_CODE"]              = "Please enter a valid daytime telephone number.";
   $_errMsg["INVALID_HOME_DIAL_CODE"]                 = "Please enter a valid evening telephone number.";

   $_errMsg["INVALID_DAYTIME_PHONE_AS_MOBILE"]        = "We cannot accept mobile or pager numbers.";
   $_errMsg["INVALID_HOME_PHONE_AS_MOBILE"]           = "We cannot accept mobile or pager numbers.";

   $_errMsg["INVALID_HOUSE_NUMBER_OR_NAME"]           = "Please choose house number or name.";

   $_errMsg["INVALID_HOUSE_NUMBER_OR_NAME_MANUALLY"]  = "Please enter your address manually.";

   $_errMsg["INVALID_HOME_OWNER"]                     = "Please choose an answer.";
   $_errMsg["INVALID_POSTCODE_PREFIX"]                = "Please enter the prefix postcode.";
   $_errMsg["INVALID_POSTCODE_NUMBER"]                = "Please enter the number postcode.";
   $_errMsg["INVALID_POSTCODE_ENTERED"]               = "Postcode appears to be not valid. Please check the postcode and confirm the address.";
   $_errMsg["INVALID_EMAIL_FIELD"]                    = "Please enter your email address.";
// end Your Contact Details
// Cover Details
   $_errMsg["INVALID_START_INSURANCE_DATE_DAY"]       = "Please ensure the day you starting the insurance is correct.";
   $_errMsg["INVALID_START_INSURANCE_DATE_MONTH"]     = "Please ensure the month you starting the insurance is correct.";
   $_errMsg["INVALID_START_INSURANCE_DATE_YEAR"]      = "Please ensure the year you starting the insurance is correct.";
   $_errMsg["INVALID_DAYS_OF_MONTH"]                  = "Please ensure the date is correct";

   $_errMsg["INVALID_WHO_WILL_DRIVE_THE_VEHICLE"] = "Please choose an answer.";
// end Cover Details
// Your Occupation
   $_errMsg["INVALID_EMPLOYMENT_STATUS"]                                       = "Please choose your employment status.";
   $_errMsg["INVALID_EMPLOYER'S_BUSINESS_OR_YOUR_SELF-EMPLOYED_BUSINESS"]      = "Please choose your area of business.";
   $_errMsg["INVALID_OCCUPATION"]                                              = "Please choose your occupation type.";
   $_errMsg["INVALID__PART_TIME_OCCUPATION"]                                   = "Please choose an occupation.";


   $_errMsg["INVALID_EMPLOYMENT_STATUS_PART_TIME"]                                       = "Please choose your part-time employment status.";
   $_errMsg["INVALID_EMPLOYER'S_BUSINESS_OR_YOUR_SELF-EMPLOYED_BUSINESS_PART_TIME"]      = "Please choose your part-time area of business.";
   $_errMsg["INVALID_OCCUPATION_PART_TIME"]                                              = "Please choose your part-time occupation type.";
// end Your Occupation
// Your Licence
   $_errMsg["INVALID_TYPE_OF_DRIVING_LICENCE"]         = "Please choose a type.";
   $_errMsg["INVALID_DATE_OF_OBTAINING_LICENCE_DAY"]   = "Please ensure the day of your licence date is correct.";
   $_errMsg["INVALID_DATE_OF_OBTAINING_LICENCE_MONTH"] = "Please ensure the month of your licence date is correct.";
   $_errMsg["INVALID_DATE_OF_OBTAINING_LICENCE_YEAR"]  = "Please ensure the year of your licence date is correct.";
   // extra
   $_errMsg["INVALID_LICENCE_DATE_OF_DRIVER"]          = "You cannot passed the test before 17 years age.";
   $_errMsg["INVALID_LICENCE_DATE_OF_DRIVER_FUTURE"]   = "You cannot passed the test in the future.";
// end Your Licence
// Your Driving History
   $_errMsg["INVALID_ANY_OTHER_ACCIDENTS_IN_THE_LAST_FIVE_YEARS"]                     = "Please choose an answer.";
   $_errMsg["INVALID_HAD_ANY_CONVICTIONS_OR_PENDING_WITHIN_THE_LAST_FIVE_YEARS"]      = "Please choose an answer.";
// end Your Driving History

// Extra Error
   $_errMsg["INVALID_PROPOSER_NOT_MARRIED"]                                      = "Proposer declared not married.";
   $_errMsg["INVALID_SEX_TYPE_DRIVER"]                                           = "Sex type is not the same type with title.";
   $_errMsg["INVALID_MARITAL_STATUS_FOR_YOUR_SPOUSE"]                            = "You declared this person is your spouse.";
   $_errMsg["INVALID_TITLE_FOR_YOUR_SPOUSE"]                                     = "Invalid title for your spouse.";
   $_errMsg["INVALID_SPOUSE_ALREADY_DECLARED"]                                   = "You allready have a spouse declared.";
   $_errMsg["INVALID_SEX_FOR_YOUR_SPOUSE"]                                       = "Invalid sex type for your spouse.";
   $_errMsg["INVALID_AGE_OF_DRIVER"]                                             = "The driver must be older than 17 years.";
   $_errMsg["INVALID_INSURANCE_CANNOT_START_IN_THE_PAST"]                        = "We cannot quote for a past date.";
   $_errMsg["INVALID_INSURANCE_CANNOT_START_IN_THE_FUTURE_MORE_THAN_ONE_MONTH"]  = "We cannot quote for more than 30 days in the future.";
   $_errMsg["INVALID_DATE_OF_UK_RESIDENT_AFTER_CURRENT_DAY"]                     = "You cannot have been resident in the UK after current day.";
   $_errMsg["INVALID_DATE_OF_UK_RESIDENT_BEFORE_YOU_BORN"]                       = "You cannot have been resident in the UK before you were born.";
   $_errMsg["INVALID_DATE_OF_UK_RESIDENT_DAYS_IN_MONTH"] = "Please ensure the date is correct";
   $_errMsg['INVALID_POSTCODE_SIZE']                                             = "The length of postcode must be between 6 and 8 characters.";
   $_errMsg['INVALID_POSTCODE_FIRST_FP_CHAR']                                    = "First char of postcode must be a letter.";

   $_errMsg["INVALID_LICENCE_DATE_DAYS_IN_MONTH"] 				 = "Please ensure the date is correct";
   $_errMsg["INVALID_INSURANCE_DAYS_IN_MONTH"]                                   = "Please ensure the date is correct";
   $_errMsg['INVALID_POSTCODE_FIRST_SP_CHAR']                                    = "First char of the second substring must be a number.";
   $_errMsg['INVALID_POSTCODE_SECOND_SP_CHAR']                                   = "Second char of the second substring must be a letter.";
   $_errMsg['INVALID_POSTCODE_LAST_SP_CHAR']                                     = "Last char of the second substring must be a letter.";

// end DRIVERS
// VEHICLE
   $_errMsg["INVALID_YEAR_OF_MANUFACTURE"]                  = "Please enter vehicle year of manufacture.";
   $_errMsg["INVALID_VEHICLE_MAKE"]                         = "Please choose a vehicle.";
   $_errMsg["INVALID_VEHICLE_MODEL"]                        = "Please choose a model.";
   $_errMsg["INVALID_ENGINE_SIZE"]                          = "Please enter engine size.";
   $_errMsg["INVALID_SEATS_OF_VEHICLE"]                     = "Please choose a number.";
   $_errMsg["INVALID_ESTIMATED_VALUE"]                      = "Please enter estimated value of vehicle.";
   $_errMsg["INVALID_VEHICLE_SIDE"]                         = "Please choose an answer.";
   $_errMsg["INVALID_VEHICLE_KEPT"]                         = "Please choose an answer.";
   $_errMsg["INVALID_VEHICLE_IMMOBILISER"]                  = "Please choose an answer.";
   $_errMsg["INVALID_VEHICLE_TRACKING_DEVICE"]              = "Please choose an answer.";
   $_errMsg["INVALID_VEHICLE_GREY_OR_IMPORT"]               = "Please choose an answer.";
   $_errMsg["INVALID_VEHICLE_MODIFIED_FROM_MANUFACTURER"]   = "Please choose an answer.";
   $_errMsg["INVALID_VEHICLE_BOUGHT_DAY"]                   = "Please ensure the day you bought the vehicle is correct.";
   $_errMsg["INVALID_VEHICLE_BOUGHT_MONTH"]                 = "Please ensure the month you bought the vehicle is correct.";
   $_errMsg["INVALID_VEHICLE_BOUGHT_YEAR"]                  = "Please ensure the year you bought the vehicle is correct.";

   // Extra error vehicle
   $_errMsg["INVALID_MATCHING_VEHICLES"]                      = "No car matching, please review the vehicle form.";
   $_errMsg["CONFIRM_VEHICLE"]                                = "Please confirm your vehicle.";
   $_errMsg["NOT_YET_CONFIRM_VEHICLE"]                        = "Your vehicle not yet confirmed !.";
   $_errMsg["INVALID_YEAR_OF_MANUFACTURE_IN_THE_FUTURE"]      = "Your vehicle cannot be made in the future.";
   $_errMsg["INVALID_VEHICLE_BOUGHT_IN_THE_FUTURE"]           = "Your vehicle cannot be bought in the future.";
   $_errMsg["INVALID_VEHICLE_BOUGHT_BEFORE_IT_MADE"]          = "Your 
vehicle cannot be bought before it was made.";
   $_errMsg["INVALID_VEHICLE_ESTIMATED_VALUE"]                = "Value of the vehicle must be greater or equal with 50 &pound.";
   $_errMsg["INVALID_VEHIVLE_BOUGHT_DAYS_OF_MONTH"] = "Please ensure the date is correct";

// end VEHICLE
// COVER
   $_errMsg["INVALID_TYPE_OF_COVER"]                = "Please chooose a type.";
   $_errMsg["INVALID_TYPE_OF_USE"]                  = "Please chooose a type.";
   $_errMsg["INVALID_ANNUAL_MILEAGE"]               = "Please enter a number.";
   $_errMsg["INVALID_BUSINESS_MILEAGE"]             = "Please enter a number.";
   $_errMsg["INVALID_NO_CLAIMS_BONUS"]              = "Please choose a value.";
   $_errMsg["INVALID_PROTECT_BONUS"]                = "Please choose a value.";
   $_errMsg["INVALID_POLICY_EXPIRE_DAY"]            = "Please enter a date when policy expire.";
   $_errMsg["INVALID_POLICY_EXPIRE_MONTH"]          = "Please enter a date when policy expire.";
   $_errMsg["INVALID_POLICY_EXPIRE_YEAR"]           = "Please enter a date when policy expire.";
   $_errMsg["INVALID_OWNER_OF_VEHICLE"]             = "Please choose an answer.";
   $_errMsg["INVALID_REGISTERED_OF_VEHICLE"]        = "Please choose an answer.";
   $_errMsg["INVALID_NUMBER_OF_OTHER_VEHICLES"]     = "Please enter a number.";
   // extra errors
   $_errMsg["INVALID_BUSINESS_MILEAGE_OF_VEHICLE"]  = "Your business mileage cannot be greater then annual mileage.";
   $_errMsg["INVALID_AGE_OF_DRIVER_AT_START_NCB"]   = "According to your main driver licence date, the maximum NCB can be ";

   $_errMsg["INVALID_POLICY_EXPIRE_DAYS_OF_MONTH"]  = "Please ensure the date is correct";
   $_errMsg["INVALID_MAIN_USER_OF_VEHICLE"]         = "Please choiose an answer.";
// end COVER

// CLAIMS
   $_errMsg["INVALID_DATE_OF_CLAIM_DAY"]             = "Please ensure the day you starting the insurance is correct.";
   $_errMsg["INVALID_DATE_OF_CLAIM_MONTH"]           = "Please enter a valid month date.";
   $_errMsg["INVALID_DATE_OF_CLAIM_YEAR"]            = "Please enter a valid year date.";
   $_errMsg["INVALID_CAUSE_OF_DESCRIPTION"]          = "Please choose a cause or description.";
   $_errMsg["INVALID_COST_OF_REPAIRS"]               = "Please enter a number.";
   $_errMsg["INVALID_LIABLE_FOR_THE_CLAIM"]          = "Please choose an answer.";
   $_errMsg["INVALID_HAD_ANY_OTHER_CLAIM"]           = "Please choose an answer.";
   $_errMsg["INVALID_AT_FAULT_CLAIM"]                = "Please choose a person.";
   $_errMsg["INVALID_ANY_INJURIES_OF_CLAIM"]         = "Please choose an answer.";
   $_errMsg["INVALID_HAD_ANY_OTHER_CLAIM"]           = "Please choose an answer.";
   // extra errors
   $_errMsg["INVALID_CLAIM_CANNOT_BE_IN_THE_FUTURE"] = "The accident cannot be in the future.";
   $_errMsg["INVALID_CLAIM_CANNOT_BE_IN_THE_PAST_MORE_THAN_FIVE_YEARS"] = "The accident must be in the last five years.";
   $_errMsg["INVALID_MAXIMUM_CLAIMS_DECLARED_BY_DRIVER"] = "Maximum number of claims must be 5 in the last 5 years.";
   $_errMsg["INVALID_DATE_OF_CLAIM_DAYS_OF_MONTH"] = "Please ensure the date is correct";
// end CLAIMS
// CONVICTION
   $_errMsg["INVALID_DATE_OF_CONVICTION_DAY"]             = "Please enter a valid day of date.";
   $_errMsg["INVALID_DATE_OF_CONVICTION_MONTH"]           = "Please enter a valid month of date.";
   $_errMsg["INVALID_DATE_OF_CONVICTION_YEAR"]            = "Please enter a valid year of date.";
   $_errMsg["INVALID_CONVICTION_DECLARED_ACCIDENT"]       = "Please choose an answer.";
   $_errMsg["INVALID_OFFENCE_CODE"]                       = "Please choose an offence code.";
   $_errMsg["INVALID_ALCOHOL_METHOD_TEST"]                = "Please choose the alcohol test method.";
   $_errMsg["INVALID_ALCOHOL_LEVEL"]                      = "Please enter the alcohol level.";
   $_errMsg["INVALID_POINTS_RECEIVED"]                    = "Please enter a number.";
   $_errMsg["INVALID_HOW_MUCH_YOU_FINED"]                 = "Please enter a number.";
   $_errMsg["INVALID_RECEIVED_BAN_PERIOD"]                = "Please enter a number.";
   $_errMsg["INVALID_HAD_ANY_OTHER_CONVICTION"]           = "Please choose an answer.";
   // extra errors
   $_errMsg["INVALID_CONVICTION_CANNOT_BE_IN_THE_FUTURE"]    = "The conviction cannot be in the future.";
   $_errMsg["INVALID_CONVICTION_CANNOT_BE_IN_THE_PAST_MORE_THAN_FIVE_YEARS"] = "The conviction must be in the last five years.";
   $_errMsg["INVALID_NUMBER_OF_POINTS_RECEIVED"]             = "Maximum points received must be 12.";
   $_errMsg["INVALID_MAXIMUM_CONVICTONS_DECLARED_BY_DRIVER"] = "Maximum number of convictions must be 5 in the last 5 years.";
   $_errMsg["INVALID_DATE_OF_CONVICTION_DAYS_OF_MONTH"]      = "Please ensure the date is correct";
// end CONVICTION

// engine errors

   $_errMsg["PERSONALIZED_CANNOT_GET_REMOTE_URL"]              = "The server is too busy at the moment.";
   $_errMsg["PERSONALIZED_SERVER_QUOTING_IS_OFFLINE"]          = "The quoting system is temporarily offline.";
   $_errMsg["PERSONALIZED_INTERNAL_SERVER_ERROR"]              = "Unable to get a quote . This may be because the server is too busy at present.Use the \"get quote\" link to go directly to the site and get a quote.";

   $_errMsg["PERSONALIZED_SERVER_CANNOT_PROVIDE_ONLINE_QUOTE"] = "Unfortunately, the insurer is unable to quote for drivers with your circumstances.";

   $_errMsg['PERSONALIZED_SERVER_CANNOT_PROVIDE_ONLINE_QUOTE_HOME'] = "Unfortunately, the insurer is unable to quote for people with your circumstances.";

   $_errMsg["CANNOT_GET_REMOTE_URL"]                           = "Cannot get remote url at this moment.";
   $_errMsg["SERVER_QUOTING_IS_OFFLINE"]                       = "The quoting server is offline.";
   $_errMsg["INTERNAL_SERVER_ERROR"]                           = "The quotezone server error.";
   $_errMsg["SERVER_CANNOT_PROVIDE_ONLINE_QUOTE"]              = "The server cannot provide quote online.";

   $_errMsg["CANNOT_OPEN_LOG_FILE"]                            = "Cannot open the log file from car directory";
   $_errMsg["CANNOT_OPEN_DATA_FILE"]                           = "Cannot open the data file from data directory";

   $_errMsg["CANNOT_OPEN_ERROR_FILE"]                          = "Cannot open error file from error directory";
   $_errMsg["CANNOT_OPEN_ERROR_MESSAGE_FILE"]                  = "Cannot open error message file from out directory";

      //Endsleighs server is currently offline. We will automatically retry this quote when it is back online and email the results to you

   $_errMsg["PARSE_EMPTY_HTML_PAGE"]              = "The server is too busy at the moment.";
   $_errMsg["SERVER_CANNOT_PROVIDE_ONLINE_QUOTE"] = "Unfortunately, the insurer is unable to quote for drivers with your circumstances.";

   $_errMsg["SERVER_CANNOT_PROVIDE_ONLINE_QUOTE_HOME"] = "Unfortunately, the insurer is unable to quote for people with your circumstances.";

   // retrieving page error's declaration
   $_errMsg["INVALID_EMAIL_OR_QUOTE_REFERENCE_FIELD"]         = "Please enter your email address or the quote reference";
   $_errMsg["INVALID_USER_PASSWORD_FIELD"]                    = "Please enter your password";
   $_errMsg["CANT_FIND_EMAIL_ADDRESS"]                        = "Please ensure the email address is correct";
   $_errMsg["DID_NOT_FIND_ANY_ENTRY_WITH_THIS_DATE_OF_BIRTH"] = "Please ensure the date you birth is correct";
   $_errMsg["DID_NOT_FIND_ANY_ENTRY_WITH_THIS_PASSWORD"]      = "Please ensure your password is correct";
   $_errMsg["CANT_FIND_ANY_QUOTES_ON_THIS_EMAIL_ADDRESS"]     = "We did not find any quotes on this email";
   $_errMsg["INVALID_EMAIL_ADDRESS_FIELD"]                    = "Please enter your email address";


   // HOME ERRORS KEY DEFINITIONS
      // HOME SECTION
      $_errMsg["INVALID_PROPERTY_TYPE"]                    = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_OWNED"]                   = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_OWNED_BUILDING"]          = "Buildings cover is not available if you are renting the property.";
      $_errMsg["INVALID_PROPERTY_YEAR"]                    = "Please ensure the year is correct";
      $_errMsg["INVALID_PROPERTY_ROOMS"]                   = "Please enter the number of rooms expire.";
      $_errMsg["INVALID_PROPERTY_BEDROOMS"]                = "Please enter the number of bedrooms expire.";

      $_errMsg["INVALID_PROPERTY_UNOCCUPIED"]              = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_ALERT_ALARM"]             = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_BUSINESS"]                = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_WORK"]                    = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_WATCH"]                   = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_PROF_ALARM"]              = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_DOOR_LOCK"]               = "Please choose an answer.";
      $_errMsg['INVALID_PROPERTY_FRENCH_LOCK']             = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_WINDOW_LOCK"]             = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_SMOKE_ALARM"]             = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_ROOMS_BEDROOMS"]          = "Numer of rooms must be greater than number of bedrooms";
      $_errMsg["INVALID_PROPERTY_BEDROOMS_ROOMS"]          = "Numer of rooms must be greater than number of bedrooms";

      $_errMsg["INVALID_PROPERTY_PERMANENT_RESIDENCE"]     = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_UNOCCUPIED_DAYS"]         = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_OWN_LOCKABLE_ENTRANCE"]   = "Please choose an answer.";

   // end HOME SECTION

      // EXTRA HOME
      $_errMsg["INVALID_PROPERTY_STATE_OF_REPAIR"]    = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_LISTED"]             = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_ROOF_MADE"]          = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_FLAT_ROOF"]          = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_WALLS_MADE"]         = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_SUBSIDENCE"]         = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_LANDSLIP_HEAVE"]     = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_FLOODING"]           = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_NEAREST_RIVER"]      = "Please choose an answer.";
      $_errMsg["INVALID_PROPERTY_NEAREST_TREE"]       = "Please choose an answer.";


      // end HOME EXTRA

      // PROPOSER SECTION
      $_errMsg["INVALID_AGE_OF_HOME"]                            = "The person must be older than 17 years.";
      $_errMsg["INVALID_JOINT_HOME_INSURER"]                     = "Please choose an answer.";
      // PROPOSEER SECTION
      $_errMsg["INVALID_PART_TIME_OCCUPATION"]                   = "Please choose an answer.";
      $_errMsg["NO_OCCUPATION_BUSINESS_ENTRY_FOUND_IN_DATABASE"] = "No matches found. Please try a different description.";
      $_errMsg["INVALID_OCCUPATION_BUSINESS_SEARCH_WORD_LENGTH"] = "Please enter at least 3 characters.";

      $_errMsg["INVALID_PROPERTY_BANKRUPT"]           = "Please choose an answer.";
      $_errMsg["INVALID_SPECIAL_TERMS_POLICY"]        = "Please choose an answer.";
      $_errMsg["INVALID_POLICY_DECLINED"]             = "Please choose an answer.";
      $_errMsg["INVALID_OCCUPANTS_CONVICTIONS"]       = "Please choose an answer.";

      // COVER SECTION
      $_errMsg["INVALID_REQUIRE_BUILDING_COVER"]                         = "Please choose an answer.";
      $_errMsg["INVALID_PREVIOUS_BUILDING_COVER"]                        = "Please choose an answer.";
      $_errMsg["INVALID_BUILDING_SUM_INSURED"]                           = "Please enter the sum insured.";
      $_errMsg["INVALID_BUILDING_ACCIDENTAL_DAMAGE"]                     = "Please choose an answer.";
      $_errMsg["INVALID_CONTENTS_ACCIDENTAL_DAMAGE"]                     = "Please choose an answer.";
      $_errMsg["INVALID_BUILD_NO_CLAIMS_BONUS"]                          = "Please choose an answer.";
      $_errMsg["INVALID_REQUIRE_COVER_COVER"]                            = "Please choose an answer.";
      $_errMsg["INVALID_PREVIOUS_CONTENTS_COVER"]                        = "Please choose an answer.";
      $_errMsg["INVALID_CONTENTS_SUM_INSURED"]                           = "Please enter the sum insured.";
      $_errMsg["INVALID_ALL_RISK_UNSPECIFIED"]                           = "Please choose an answer.";
      $_errMsg["INVALID_CONTENTS_NO_CLAIMS_BONUS"]                       = "Please choose an answer.";
      $_errMsg["INVALID_ANY_BUILDING_CLAIMS_WITHIN_THE_LAST_FIVE_YEARS"] = "Please choose an answer.";
      $_errMsg["INVALID_ANY_CONTENTS_CLAIMS_WITHIN_THE_LAST_FIVE_YEARS"] = "Please choose an answer.";
      $_errMsg["INVALID_BUILD_VOLUNTARY_EXCESS"]                         = "Please choose an answer.";
      $_errMsg["INVALID_CONTENTS_VOLUNTARY_EXCESS"]                      = "Please choose an answer.";

      // extra Errors
      $_errMsg["INVALID_COVER_BULIDING_OR_CONTENTS_CHOOSED"] = "Please choose at least one of cover type.";
      $_errMsg["INVALID_COVER_BULIDING_OR_CONTENTS_CHOOSED"] = "Please choose at least one of cover type.";
      $_errMsg["INVALID_COVER_BULIDING_SUMS_INSURED"]        = "The sum insured is between 50000 and 1000000 pounds";
      $_errMsg["INVALID_COVER_CONTENTS_SUMS_INSURED"]        = "The sum insured is between 10000 and 50000 pounds";

      $_errMsg["MAXIMUM_BUILDING_NO_CLAIMS_BONUS"]           = "Maximum no claims bonus";
      $_errMsg["MAXIMUM_CONTENTS_NO_CLAIMS_BONUS"]           = "Maximum no claims bonus";
      // end COVER SECTION

     // CLAIM per LOOSES  

     $_errMsg['INVALID_CLAIM_OR_LOSSES_CANNOT_BE_IN_THE_PAST_MORE_THAN_FIVE_YEARS'] = "The claims or losses must be in the last five years.";
     $_errMsg['INVALID_TYPE_OF_CLAIM']                                              = 'Please choose an answer.';
     $_errMsg['INVALID_CLAIM_STATUS']                                               = 'Please choose an answer.';
     $_errMsg['INVALID_CLAIM_AMOUNT']                                               = 'Please tell us the value of the loos.';
   //end HOME ERRORS KEY DEFINITIONS

     $_errMsg['INVALID_GROUP_MEMBER']                                               = 'Please tell us which group do you belong';

   if(empty($_errMsg[$error]))
      return $_errMsg["UNDEFINED_ERROR"].$error;
   
   return $_errMsg[$error];
}

?>

<?php
/*****************************************************************************/
/*                                                                           */
/*  CTracking class interface                                            */
/*                                                                           */
/*  (C) 2005 Gabriel Istvancsek (gabi@acrux.biz)                             */
/*                                                                           */
/*****************************************************************************/

include_once "MySQL.php";
include_once "errors.inc";

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CTracking
//
// [DESCRIPTION]:  CTracking class interface
//
// [FUNCTIONS]:
// 
//                 void Close();
//                 string GetError();
//
// [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-12-19
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CTracking
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

   var $referer;
   var $server;
   var $remoteAddr;
   var $cookieUserRef;
   var $cookieQuoteRef;
   var $qzQuoteID;
   var $keywordID;

   var $strERR;

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CTracking
   //
   // [DESCRIPTION]:   Default class constructor. Initialization goes here.
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CTracking($referer='',$server='', $uRef='', $qRef='', $remoteAddr='', $qzQuoteID=0, $keywordID=0, $dbh=0)
   {

      if($dbh)
      {
         $this->dbh = $dbh;
         $this->closeDB = false;
      }
      else
      {
         // default configuration
         $this->dbh = new CMySQL();
   
         if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
         {
            $this->strERR = $this->dbh->GetError();
            return;
         }
   
         $this->closeDB = true;
      }
   
      // these params are sent by GET from mainsite
      $this->referer        = $referer;
      $this->server         = $server;

      $this->remoteAddr     = $remoteAddr;

      $this->cookieUserRef  = $uRef;
      $this->cookieQuoteRef = $qRef;

      $this->qzQuoteID      = $qzQuoteID;
      $this->keywordID      = $keywordID;

      $this->strERR         = '';

   }// end function CQzProtection()

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CheckCookie
   //
   // [DESCRIPTION]:   Checking tracking 
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function _updateTracking()
   {
      if(empty($this->qzQuoteID))
         return;

      if(empty($this->cookieQuoteRef))
         return;

      $sqlCMD = "UPDATE ".SQL_TRACKINGS." SET qzquote_id='".$this->qzQuoteID."' WHERE cuRef='".$this->cookieUserRef."' AND cqRef='".$this->cookieQuoteRef."'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      return true;
      
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CheckCookie
   //
   // [DESCRIPTION]:   Checking tracking 
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function _updateKeywordTracking()
   {
      if(empty($this->keywordID))
         return;

      if(empty($this->cookieQuoteRef))
         return;

      $sqlCMD = "UPDATE ".SQL_TRACKINGS." SET keyword_id='".$this->keywordID."' WHERE cuRef='".$this->cookieUserRef."' AND cqRef='".$this->cookieQuoteRef."'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      return true;
      
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: _addTracking
   //
   // [DESCRIPTION]:   add new record into table trackings
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function _addTracking()
   {
      if(! $fromID = $this->_addTrackingFrom())
         return false;

      if(! $toID = $this->_addTrackingTo())
         return false;

      $sqlCMD = "INSERT INTO ".SQL_TRACKINGS." (curef,cqref,t_in_id,t_out_id,date,time,host_ip) VALUES('".$this->cookieUserRef."','".$this->cookieQuoteRef."','$fromID','$toID',now(),now(), '$this->remoteAddr')";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      $sqlCMD = "SELECT LAST_INSERT_ID() AS id";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      return $this->dbh->GetFieldValue('id');
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: _addTrackingFrom
   //
   // [DESCRIPTION]:   add new record into table trackings_inout
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function _addTrackingFrom()
   {
      $resReferer = parse_url($this->referer);

      $scheme = $resReferer['scheme'];
      $host   = $resReferer['host'];
      $path   = $resReferer['path'];
      $query  = $resReferer['query'];

      if(! $schemeID = $this->_addTrackingScheme($scheme))
         return false;

      if(! $hostID = $this->_addTrackingHost($host))
         return false;

      if(! $pathID = $this->_addTrackingPath($path))
         return false;

      if(! $queryID = $this->_addTrackingQuery($query))
         return false;

      $sqlCMD = "INSERT INTO ".SQL_TRACKINGS_INOUT." (scheme_id,host_id,path_id,query_id) VALUES('$schemeID','$hostID','$pathID','$queryID')";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      $sqlCMD = "SELECT LAST_INSERT_ID() AS id";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      return $this->dbh->GetFieldValue('id');
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: _addTrackingTo
   //
   // [DESCRIPTION]:   add new record into table trackings_inout
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function _addTrackingTo()
   {

      $resServer = parse_url($this->server);

      $scheme = $resServer['scheme'];
      $host   = $resServer['host'];
      $path   = $resServer['path'];
      $query  = $resServer['query'];

      if(! $schemeID = $this->_addTrackingScheme($scheme))
         return false;

      if(! $hostID = $this->_addTrackingHost($host))
         return false;

      if(! $pathID = $this->_addTrackingPath($path))
         return false;

      if(! $queryID = $this->_addTrackingQuery($query))
         return false;

      $sqlCMD = "INSERT INTO ".SQL_TRACKINGS_INOUT." (scheme_id,host_id,path_id,query_id) VALUES('$schemeID','$hostID','$pathID','$queryID')";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      $sqlCMD = "SELECT LAST_INSERT_ID() AS id";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      return $this->dbh->GetFieldValue('id');
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: _addTrackingScheme
   //
   // [DESCRIPTION]:   Add new record to db
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function _addTrackingScheme($scheme)
   {
      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_SCHEMES." WHERE scheme='$scheme'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if($this->dbh->FetchRows())
      {
         return $this->dbh->GetFieldValue('id');
      }

      $sqlCMD = "INSERT INTO ".SQL_TRACKING_SCHEMES." (scheme) VALUES('$scheme')";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      $sqlCMD = "SELECT LAST_INSERT_ID() AS id";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      return $this->dbh->GetFieldValue('id');

   }
   
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: _addTrackingHost
   //
   // [DESCRIPTION]:   Add new record to db
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function _addTrackingHost($host)
   {
      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_HOSTS." WHERE host='$host'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if($this->dbh->FetchRows())
      {
         return $this->dbh->GetFieldValue('id');
      }

      $sqlCMD = "INSERT INTO ".SQL_TRACKING_HOSTS." (host) VALUES('$host')";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      $sqlCMD = "SELECT LAST_INSERT_ID() AS id";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      return $this->dbh->GetFieldValue('id');

   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: _addTrackingPath
   //
   // [DESCRIPTION]:   Add new record to db
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function _addTrackingPath($path)
   {
      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_PATHS." WHERE path='$path'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if($this->dbh->FetchRows())
      {
         return $this->dbh->GetFieldValue('id');
      }

      $sqlCMD = "INSERT INTO ".SQL_TRACKING_PATHS." (path) VALUES('$path')";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      $sqlCMD = "SELECT LAST_INSERT_ID() AS id";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      return $this->dbh->GetFieldValue('id');

   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: _addTrackingPath
   //
   // [DESCRIPTION]:   Add new record to db
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function _addTrackingQuery($query)
   {
      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_QUERIES." WHERE query='$query'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if($this->dbh->FetchRows())
      {
         return $this->dbh->GetFieldValue('id');
      }

      $sqlCMD = "INSERT INTO ".SQL_TRACKING_QUERIES." (query) VALUES('$query')";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      $sqlCMD = "SELECT LAST_INSERT_ID() AS id";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      return $this->dbh->GetFieldValue('id');

   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetAllTrackingSchemes
   //
   // [DESCRIPTION]:   Retrieve schemes entries from db
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  false | array
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function  GetAllTrackingSchemes()
   {
      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_SCHEMES."";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("NO_TRACKING_SCHEMES_FOUND");
         return false;
      }

      $result = array();
      while($this->dbh->MoveNext())
      {
         $result[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("scheme");
      }

      return $result;
   }
   
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetAllTrackingHosts
   //
   // [DESCRIPTION]:   Retrieve hosts entries from db
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  false | array
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function  GetAllTrackingHosts()
   {
      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_HOSTS."";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("NO_TRACKING_HOSTS_FOUND");
         return false;
      }

      $result = array();
      while($this->dbh->MoveNext())
      {
         $result[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("host");
      }

      return $result;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetAllTrackingHosts
   //
   // [DESCRIPTION]:   Retrieve hosts entries from db
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  false | array
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function  GetTopTrackingHosts($dateFrom='', $dateTo='')
   {
      $sqlCMD = "SELECT ".SQL_TRACKING_HOSTS.".*, count(".SQL_TRACKINGS_INOUT.".id) as total FROM ".SQL_TRACKINGS." LEFT JOIN ".SQL_TRACKINGS_INOUT." ON (".SQL_TRACKINGS_INOUT.".id= ".SQL_TRACKINGS.".t_in_id) LEFT JOIN ".SQL_TRACKING_HOSTS." ON ".SQL_TRACKINGS_INOUT.".host_id=".SQL_TRACKING_HOSTS.".id WHERE 1 ";

      if(! empty($dateFrom))
      {
         if(! $dateFrom = $this->StdToMysqlDate($dateFrom))
            return false;

         $sqlCMD .= " AND ".SQL_TRACKINGS.".date>='$dateFrom' ";
      }

      if(! empty($dateTo))
      {
         if(! $dateTo = $this->StdToMysqlDate($dateTo))
            return false;

         $sqlCMD .= " AND ".SQL_TRACKINGS.".date<='$dateTo' ";
      }

      $sqlCMD .= " GROUP BY (".SQL_TRACKING_HOSTS.".host) ORDER BY total DESC ";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("NO_TRACKING_HOSTS_FOUND");
         return false;
      }


      $result = array();
      while($this->dbh->MoveNext())
      {
         
         if(! $host = $this->dbh->GetFieldValue("host"))
            $host = ' < direct hits > ';
            
         $result[$this->dbh->GetFieldValue("id")] = $host." ( ".$this->dbh->GetFieldValue("total")." ) ";
      }

      foreach($result as $hostID => $resHost)
      {
         $sqlCMD = "SELECT count(".SQL_TRACKINGS.".id) as total FROM ".SQL_TRACKINGS." LEFT JOIN ".SQL_TRACKINGS_INOUT." ON (".SQL_TRACKINGS_INOUT.".id= ".SQL_TRACKINGS.".t_in_id) WHERE 1 ";

         if(! empty($dateFrom))
         {
            $sqlCMD .= " AND ".SQL_TRACKINGS.".date>='$dateFrom' ";
         }
   
         if(! empty($dateTo))
         {
            $sqlCMD .= " AND ".SQL_TRACKINGS.".date<='$dateTo' ";
         }

         $sqlCMD .= " AND ".SQL_TRACKINGS_INOUT.".host_id='$hostID' AND ".SQL_TRACKINGS.".qzquote_id <> 0";

         if(! $this->dbh->Exec($sqlCMD))
         {
            $this->strERR = $this->dbh->strERR;
            return false;
         }

         if(! $this->dbh->FetchRows())
         {
            $this->strERR = GetErrorString("NO_TRACKING_HOSTS_FOUND");
            return false;
         }
         
         $result[$hostID] = $resHost." < ".$this->dbh->GetFieldValue("total")." > ";
         
      }

      return $result;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetAllTrackingPaths
   //
   // [DESCRIPTION]:   Retrieve paths entries from db
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  false | array
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function  GetAllTrackingPaths()
   {
      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_PATHS."";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("NO_TRACKING_PATHS_FOUND");
         return false;
      }

      $result = array();
      while($this->dbh->MoveNext())
      {
         $result[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("path");
      }

      return $result;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetAllTrackingQueries
   //
   // [DESCRIPTION]:   Retrieve queries entries from db
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  false | array
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function  GetAllTrackingQueries()
   {
      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_QUERIES."";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("NO_TRACKING_QUERIES_FOUND");
         return false;
      }

      $result = array();
      while($this->dbh->MoveNext())
      {
         $result[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("query");
      }

      return $result;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetAllTrackings
   //
   // [DESCRIPTION]:   Retrieve queries entries from db
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  false | array
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetAllTrackings($dateFrom='', $dateTo='', $schemeID='', $hostID='', $pathID='', $queryID='')
   {

      $sqlCMD = "SELECT ".SQL_TRACKINGS.".*,".SQL_TRACKINGS_INOUT.".id as tracking_id,".SQL_TRACKING_SCHEMES.".scheme,".SQL_TRACKING_HOSTS.".host,".SQL_TRACKING_PATHS.".path,".SQL_TRACKING_QUERIES.".query FROM ".SQL_TRACKINGS." LEFT JOIN ".SQL_TRACKINGS_INOUT." ON (".SQL_TRACKINGS_INOUT.".id= ".SQL_TRACKINGS.".t_in_id) LEFT JOIN ".SQL_TRACKING_SCHEMES." ON ".SQL_TRACKINGS_INOUT.".scheme_id=".SQL_TRACKING_SCHEMES.".id LEFT JOIN ".SQL_TRACKING_HOSTS." ON ".SQL_TRACKINGS_INOUT.".host_id=".SQL_TRACKING_HOSTS.".id LEFT JOIN ".SQL_TRACKING_PATHS." ON ".SQL_TRACKINGS_INOUT.".path_id=".SQL_TRACKING_PATHS.".id  LEFT JOIN ".SQL_TRACKING_QUERIES." ON ".SQL_TRACKINGS_INOUT.".query_id=".SQL_TRACKING_QUERIES.".id WHERE 1 ";

      if(! empty($dateFrom))
      {
         if(! $dateFrom = $this->StdToMysqlDate($dateFrom))
            return false;

         $sqlCMD .= " AND ".SQL_TRACKINGS.".date>='$dateFrom' ";
      }

      if(! empty($dateTo))
      {
         if(! $dateTo = $this->StdToMysqlDate($dateTo))
            return false;

         $sqlCMD .= " AND ".SQL_TRACKINGS.".date<='$dateTo' ";
      }

      $noPair = false;
      if(! empty($schemeID))
      {
         if(! preg_match("/^\d+$/", $schemeID))
         {
            $this->strERR = GetErrorString("INVALID_TRACKING_SCHEME_ID");
            return false;
         }

         $sqlCMD .= " AND ".SQL_TRACKING_SCHEMES.".id='$schemeID' ";
         $noPair = true;
      }
      
      if(! empty($hostID))
      {
         if(! preg_match("/^\d+$/", $hostID))
         {
            $this->strERR = GetErrorString("INVALID_TRACKING_HOST_ID");
            return false;
         }

         $sqlCMD .= " AND ".SQL_TRACKING_HOSTS.".id='$hostID' ";
         $noPair = true;
      }
      
      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("NO_TRACKINGS_FOUND");
         return false;
      }


      $result = array();

      while($this->dbh->MoveNext())
      {
         $result[$this->dbh->GetFieldValue("id")]['t_in_id']    = $this->dbh->GetFieldValue("t_in_id");
         $result[$this->dbh->GetFieldValue("id")]['t_out_id']   = $this->dbh->GetFieldValue("t_out_id");
         $result[$this->dbh->GetFieldValue("id")]['cuRef']      = $this->dbh->GetFieldValue("cuRef");
         $result[$this->dbh->GetFieldValue("id")]['cqRef']      = $this->dbh->GetFieldValue("cqRef");
         $result[$this->dbh->GetFieldValue("id")]['qzquote_id'] = $this->dbh->GetFieldValue("qzquote_id");
         $result[$this->dbh->GetFieldValue("id")]['keyword_id'] = $this->dbh->GetFieldValue("keyword_id");
         $result[$this->dbh->GetFieldValue("id")]['date']       = $this->MysqlToStdDate($this->dbh->GetFieldValue("date"));
         $result[$this->dbh->GetFieldValue("id")]['time']       = $this->dbh->GetFieldValue("time");

         $schemeSep = "";
         if($this->dbh->GetFieldValue("host") != "")
            $schemeSep = "://";

         $querySep  = "";
         if($this->dbh->GetFieldValue("query") != "")
            $querySep  = "?";

            $result[$this->dbh->GetFieldValue("id")]['in']         = $this->dbh->GetFieldValue("scheme").$schemeSep.$this->dbh->GetFieldValue("host").$this->dbh->GetFieldValue("path").$querySep.$this->dbh->GetFieldValue("query");

      }

      foreach($result as $trackingID => $resResult)
      {
         $sqlCMD = "SELECT ".SQL_TRACKINGS.".*,".SQL_TRACKINGS_INOUT.".id as tracking_id,".SQL_TRACKING_SCHEMES.".scheme,".SQL_TRACKING_HOSTS.".host,".SQL_TRACKING_PATHS.".path,".SQL_TRACKING_QUERIES.".query FROM ".SQL_TRACKINGS." LEFT JOIN ".SQL_TRACKINGS_INOUT." ON (".SQL_TRACKINGS_INOUT.".id= ".SQL_TRACKINGS.".t_out_id ) LEFT JOIN ".SQL_TRACKING_SCHEMES." ON ".SQL_TRACKINGS_INOUT.".scheme_id=".SQL_TRACKING_SCHEMES.".id LEFT JOIN ".SQL_TRACKING_HOSTS." ON ".SQL_TRACKINGS_INOUT.".host_id=".SQL_TRACKING_HOSTS.".id LEFT JOIN ".SQL_TRACKING_PATHS." ON ".SQL_TRACKINGS_INOUT.".path_id=".SQL_TRACKING_PATHS.".id  LEFT JOIN ".SQL_TRACKING_QUERIES." ON ".SQL_TRACKINGS_INOUT.".query_id=".SQL_TRACKING_QUERIES.".id WHERE ".SQL_TRACKINGS.".t_out_id='".$resResult['t_out_id']."'";
   
   
         if(! $this->dbh->Exec($sqlCMD))
         {
            $this->strERR = $this->dbh->strERR;
            return false;
         }
   
         if(! $this->dbh->FetchRows())
         {
            $this->strERR = GetErrorString("NO_TRACKINGS_FOUND");
            return false;
         }

         $schemeSep = "";
         if($this->dbh->GetFieldValue("host") != "")
            $schemeSep = "://";

         $querySep  = "";
         if($this->dbh->GetFieldValue("query") != "")
            $querySep  = "?";

            $result[$this->dbh->GetFieldValue("id")]['out']         = $this->dbh->GetFieldValue("scheme").$schemeSep.$this->dbh->GetFieldValue("host").$this->dbh->GetFieldValue("path").$querySep.$this->dbh->GetFieldValue("query");

      }

      return $result;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetTrackingKeywordByName
   //
   // [DESCRIPTION]:   Get keyword ID by name
   //
   // [PARAMETERS]:    $name
   //
   // [RETURN VALUE]:  int
   //
   // [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2006-11-14
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //
   //////////////////////////////////////////////////////////////////////////////FE
   function GetTrackingKeywordByName($name="")
   {
      if(empty($name))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_KEYWORD_NAME")."\n";
         return false;
      }
      
      $sqlCMD = "SELECT id FROM ".SQL_TRACKING_KEYWORDS." WHERE name='$name' ";
      
      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("NO_TRACKING_KEYWORDS_FOUND");
         return false;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      return $this->dbh->GetFieldValue('id');
      
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetTrackingKeywordByName
   //
   // [DESCRIPTION]:   Get keyword ID by name
   //
   // [PARAMETERS]:    $name
   //
   // [RETURN VALUE]:  int
   //
   // [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2006-11-14
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //
   //////////////////////////////////////////////////////////////////////////////FE
   function GetTrackingKeyword($keywordID="")
   {
      if(! preg_match("/^([0-9]+)$/", $keywordID))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_KEYWORD_ID")."\n";
         return false;
      }
      
      $sqlCMD = "SELECT name FROM ".SQL_TRACKING_KEYWORDS." WHERE id='$keywordID' ";
      
      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("NO_TRACKING_KEYWORDS_FOUND");
         return false;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      return $this->dbh->GetFieldValue('name');
      
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetTrackingKeywordByName
   //
   // [DESCRIPTION]:   Get keyword ID by name
   //
   // [PARAMETERS]:    $name
   //
   // [RETURN VALUE]:  int
   //
   // [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2006-11-14
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //
   //////////////////////////////////////////////////////////////////////////////FE
   function GetAllTrackingKeyword()
   {
      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_KEYWORDS." ORDER BY name";
      
      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("NO_TRACKING_KEYWORDS_FOUND");
         return false;
      }

      $result = array();
      while($this->dbh->MoveNext())
      {
         $result[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("name");
      }

      return $result;
      
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: StdToMysqlDate
   //
   // [DESCRIPTION]:   Transform the date into MySql date format
   //
   // [PARAMETERS]:    $date : dd/mm/yyyy
   //
   // [RETURN VALUE]:  $date : yyyy-mm-dd
   //
   // [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-14
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function StdToMysqlDate($date='')
   {
      $this->strERR = '';
   
      if(! preg_match("/\d{1,2}\/\d{1,2}\/\d{4}/i",$date))
         $this->strERR = GetErrorString("INVALID_TRACKING_STD_TO_MYSQL_DATE")."\n";
   
      if(! empty($this->strERR))
         return false;
   
      list($day,$month,$year) = split("/",$date);
      $date = "$year-$month-$day";
   
      return $date;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: MysqlToStdDate
   //
   // [DESCRIPTION]:   Transform the date into MySql date format
   //
   // [PARAMETERS]:    $date : yyyy-mm-dd
   //
   // [RETURN VALUE]:  $date : dd/mm/yyyy
   //
   // [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-14
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function MysqlToStdDate($date='')
   {
      $this->strERR = '';
   
      if(! preg_match("/\d{4}\-\d{1,2}\-\d{1,2}/i",$date))
         $this->strERR = GetErrorString("INVALID_TRACKING_MYSQL_TO_STD_DATE")."\n";
   
      if(! empty($this->strERR))
         return false;
   
      list($year,$month,$day) = split("-",$date);
      $date = "$day/$month/$year";
   
      return $date;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetError
   //
   // [DESCRIPTION]:   get error 
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-14
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetError()
   {
      return $this->strERR;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AddTrackingSourceName
   //
   // [DESCRIPTION]:   Add source name
   //
   // [PARAMETERS]:    $sourceName
   //
   // [RETURN VALUE]:  false | integer
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AddTrackingSourceName($sourceName='')
   {
      if(empty($sourceName))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_SOURCE_NAME");
         return false;
      }

      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_SOURCE_NAMES." WHERE name='$sourceName'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if($this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("TRACKING_SOURCE_NAME_EXIST");
         return false;
      }

      $sqlCMD = "INSERT INTO ".SQL_TRACKING_SOURCE_NAMES." (name) VALUES('$sourceName')";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      $sqlCMD = "SELECT LAST_INSERT_ID() AS id";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      return $this->dbh->GetFieldValue('id');

   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AddTrackingSource
   //
   // [DESCRIPTION]:   Add source by sourceNameID
   //
   // [PARAMETERS]:    $sourceNameID='', $source=''
   //
   // [RETURN VALUE]:  false | integer
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AddTrackingSource($sourceNameID='', $source='', $kword='')
   {
      if(! preg_match("/\d+/", $sourceNameID))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_SOURCE_NAME_ID");
         return false;
      }

      if(empty($source))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_SOURCE");
         return false;
      }

      if(empty($kword))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_KEYWORD");
         return false;
      }

      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_SOURCE." WHERE name_id='$sourceNameID' AND source='$source' AND kword='$kword'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if($this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("TRACKING_SOURCE_EXIST");
         return false;
      }

      $sqlCMD = "INSERT INTO ".SQL_TRACKING_SOURCE." (name_id,source,kword) VALUES('$sourceNameID','$source','$kword')";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      $sqlCMD = "SELECT LAST_INSERT_ID() AS id";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }
      
      return $this->dbh->GetFieldValue('id');

   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: UpdateTrackingSourceName
   //
   // [DESCRIPTION]:   Update an existing source name
   //
   // [PARAMETERS]:    $sourceNameID='', $sourceName=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function UpdateTrackingSourceName($sourceNameID='', $sourceName='')
   {
      if(! preg_match("/\d+/", $sourceNameID))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_SOURCE_NAME_ID");
         return false;
      }

      if(empty($sourceName))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_SOURCE_NAME");
         return false;
      }

      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_SOURCE_NAMES." WHERE name='$sourceName'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if($this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("TRACKING_SOURCE_NAME_ID_NOT_EXIST");
         return false;
      }

      $sqlCMD = "UPDATE ".SQL_TRACKING_SOURCE_NAMES." SET name='$sourceName' WHERE id='$sourceNameID'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      return true;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: UpdateTrackingSource
   //
   // [DESCRIPTION]:   Update an existing source 
   //
   // [PARAMETERS]:    $sourceID='',$sourceNameID='', $source=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function UpdateTrackingSource($sourceID='',$sourceNameID='', $source='', $kword='')
   {
      if(! preg_match("/\d+/", $sourceID))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_SOURCE_ID");
         return false;
      }

      if(! preg_match("/\d+/", $sourceNameID))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_SOURCE_NAME_ID");
         return false;
      }

      if(empty($source))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_SOURCE_NAME");
         return false;
      }

      if(empty($kword))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_SOURCE_NAME");
         return false;
      }

      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_SOURCE." WHERE name_id='$sourceNameID' AND source='$source' AND kword='$kword'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if($this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("TRACKING_SOURCE_EXIST");
         return false;
      }

      $sqlCMD = "UPDATE ".SQL_TRACKING_SOURCE." SET name_id='$sourceNameID',source='$source',kword='$kword' WHERE id='$sourceID'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetTrackingSourceName
   //
   // [DESCRIPTION]:   Retrieve an existing source name
   //
   // [PARAMETERS]:    $sourceNameID=''
   //
   // [RETURN VALUE]:  false | string
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetTrackingSourceName($sourceNameID='')
   {
      if(! preg_match("/\d+/", $sourceNameID))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_SOURCE_NAME_ID");
         return false;
      }

      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_SOURCE_NAMES." WHERE id='$sourceNameID'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = GetErrorString("TRACKING_SOURCE_NAME_ID_NOT_EXIST");
         return false;
      }

      return $this->dbh->GetFieldValue('name');

   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetTrackingSource
   //
   // [DESCRIPTION]:   Retrieve an existing source
   //
   // [PARAMETERS]:    $sourceID=''
   //
   // [RETURN VALUE]:  false | array
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetTrackingSource($sourceID='')
   {
      if(! preg_match("/\d+/", $sourceID))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_SOURCE_ID");
         return false;
      }

      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_SOURCE." WHERE id='$sourceID'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = GetErrorString("TRACKING_SOURCE_ID_NOT_EXIST");
         return false;
      }

      $result = array();

      $result['name_id'] = $this->dbh->GetFieldValue("name_id");
      $result['source']  = $this->dbh->GetFieldValue("source");
      $result['kword']   = $this->dbh->GetFieldValue("kword");

      return $result;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetAllTrackingSourceName
   //
   // [DESCRIPTION]:   Retrieve all existing source name
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  false | array
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetAllTrackingSourceName()
   {

      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_SOURCE_NAMES."";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("TRACKING_SOURCE_NAME_ID_NOT_EXIST");
         return false;
      }

      $result = array();
      while($this->dbh->MoveNext())
      {
         $result[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("name");
      }
   
      return $result;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetAllTrackingSource
   //
   // [DESCRIPTION]:   Retrieve all existing sources by sourceNameID
   //
   // [PARAMETERS]:    $sourceNameID=''
   //
   // [RETURN VALUE]:  false | array
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetAllTrackingSource($sourceNameID='')
   {
      if(! preg_match("/\d+/", $sourceNameID))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_SOURCE_NAME_ID");
         return false;
      }

      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_SOURCE." WHERE name_id='$sourceNameID'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("TRACKING_SOURCE_NAME_ID_NOT_EXIST");
         return false;
      }

      $result = array();
      while($this->dbh->MoveNext())
      {
         $result[$this->dbh->GetFieldValue("id")]['source'] = $this->dbh->GetFieldValue("source");
         $result[$this->dbh->GetFieldValue("id")]['kword']  = $this->dbh->GetFieldValue("kword");
      }
   
      return $result;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: DeleteTrackingSourceName
   //
   // [DESCRIPTION]:   Delete existing source name
   //
   // [PARAMETERS]:    $sourceNameID=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function DeleteTrackingSourceName($sourceNameID='')
   {
      if(! preg_match("/\d+/", $sourceNameID))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_SOURCE_NAME_ID");
         return false;
      }

      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_SOURCE_NAMES." WHERE id='$sourceNameID'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("TRACKING_SOURCE_NAME_ID_NOT_EXIST");
         return false;
      }

      $sqlCMD = "DELETE FROM ".SQL_TRACKING_SOURCE_NAMES." WHERE id='$sourceNameID'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      return true;

   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: DeleteTrackingSource
   //
   // [DESCRIPTION]:   Delete existing source
   //
   // [PARAMETERS]:    $sourceID=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function DeleteTrackingSource($sourceID='')
   {
      if(! preg_match("/\d+/", $sourceID))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_SOURCE_ID");
         return false;
      }

      $sqlCMD = "SELECT * FROM ".SQL_TRACKING_SOURCE." WHERE id='$sourceID'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("TRACKING_SOURCE_ID_NOT_EXIST");
         return false;
      }

      $sqlCMD = "DELETE FROM tracking_source WHERE id='$sourceID'";

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      return true;

   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: DeleteTrackingSource
   //
   // [DESCRIPTION]:   Delete existing source
   //
   // [PARAMETERS]:    $sourceID=''
   //
   // [RETURN VALUE]:  true | false
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetTrackingBySourceName($sourceNameID='', $dateFrom='', $dateTo='')
   {
      if(! preg_match("/\d+/", $sourceNameID))
      {
         $this->strERR = GetErrorString("INVALID_TRACKING_SOURCE_NAME_ID");
         return false;
      }

      // GET THE NUMBER OF CLICKS AND QUOTES
      $sqlCMD = "SELECT ".SQL_TRACKINGS.".*,".SQL_TRACKINGS_INOUT.".id as tracking_id,".SQL_TRACKING_SCHEMES.".scheme,".SQL_TRACKING_HOSTS.".host,".SQL_TRACKING_PATHS.".path,".SQL_TRACKING_QUERIES.".query FROM ".SQL_TRACKINGS." LEFT JOIN ".SQL_TRACKINGS_INOUT." ON (".SQL_TRACKINGS_INOUT.".id= ".SQL_TRACKINGS.".t_out_id) LEFT JOIN ".SQL_TRACKING_SCHEMES." ON ".SQL_TRACKINGS_INOUT.".scheme_id=".SQL_TRACKING_SCHEMES.".id LEFT JOIN ".SQL_TRACKING_HOSTS." ON ".SQL_TRACKINGS_INOUT.".host_id=".SQL_TRACKING_HOSTS.".id LEFT JOIN ".SQL_TRACKING_PATHS." ON ".SQL_TRACKINGS_INOUT.".path_id=".SQL_TRACKING_PATHS.".id  LEFT JOIN ".SQL_TRACKING_QUERIES." ON ".SQL_TRACKINGS_INOUT.".query_id=".SQL_TRACKING_QUERIES.".id WHERE 1 ";

      // GET SOURCES AND KEYWORDS
      if(! $resSources = $this->GetAllTrackingSource($sourceNameID))
      {
         return false; // no sources and keywords defined
      }

      $i = 0;
      foreach($resSources as $sourceID => $resSource)
      {
         $i++;
         $sqlCMD .= " AND ((".SQL_TRACKING_QUERIES.".query like '%".$resSource['source']."%' AND ".SQL_TRACKING_QUERIES.".query like '%".$resSource['kword']."%') ";

         if(count($resSources) == $i)
            $sqlCMD .= " )";
         else if($i > 1 )
            $sqlCMD .= " OR (".SQL_TRACKING_QUERIES.".query like '%".$resSource['source']."%' AND ".SQL_TRACKING_QUERIES.".query like '%".$resSource['kword']."=%') ";
      }

      if(! empty($dateFrom))
      {
         if(! $dateFrom = $this->StdToMysqlDate($dateFrom))
            return false;

         $sqlCMD .= " AND ".SQL_TRACKINGS.".date>='$dateFrom' ";
      }

      if(! empty($dateTo))
      {
         if(! $dateTo = $this->StdToMysqlDate($dateTo))
            return false;

         $sqlCMD .= " AND ".SQL_TRACKINGS.".date<='$dateTo' ";
      }

      if(! $this->dbh->Exec($sqlCMD))
      {
         $this->strERR = $this->dbh->strERR;
         return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("NO_TRACKINGS_FOUND");
         return false;
      }

      $result = array();

      while($this->dbh->MoveNext())
      {
         $result[$this->dbh->GetFieldValue("id")]['t_in_id']    = $this->dbh->GetFieldValue("t_in_id");
         $result[$this->dbh->GetFieldValue("id")]['t_out_id']   = $this->dbh->GetFieldValue("t_out_id");
         $result[$this->dbh->GetFieldValue("id")]['cuRef']      = $this->dbh->GetFieldValue("cuRef");
         $result[$this->dbh->GetFieldValue("id")]['cqRef']      = $this->dbh->GetFieldValue("cqRef");
         $result[$this->dbh->GetFieldValue("id")]['qzquote_id'] = $this->dbh->GetFieldValue("qzquote_id");
         $result[$this->dbh->GetFieldValue("id")]['keyword_id'] = $this->dbh->GetFieldValue("keyword_id");
         $result[$this->dbh->GetFieldValue("id")]['date']       = $this->MysqlToStdDate($this->dbh->GetFieldValue("date"));
         $result[$this->dbh->GetFieldValue("id")]['time']       = $this->dbh->GetFieldValue("time");

         $schemeSep = "";
         if($this->dbh->GetFieldValue("host") != "")
            $schemeSep = "://";

         $querySep  = "";
         if($this->dbh->GetFieldValue("query") != "")
            $querySep  = "?";

            $result[$this->dbh->GetFieldValue("id")]['in']         = $this->dbh->GetFieldValue("scheme").$schemeSep.$this->dbh->GetFieldValue("host").$this->dbh->GetFieldValue("path").$querySep.$this->dbh->GetFieldValue("query");

      }

      foreach($result as $id => $res)
      {
         if(empty($res['qzquote_id']) && empty($res['keyword_id']))
         {
            $result[$id]['type'] = "no quote";
            continue;
         }

         if(empty($res['qzquote_id']) && ! empty($res['keyword_id']))
         {
            $result[$id]['type'] = 'main site';
            continue;
         }

         $sqlCMD = "SELECT qzquote_types.name FROM qzquotes LEFT JOIN qzquote_types ON (qzquote_types.id= qzquotes.quote_type_id) WHERE qzquotes.id='".$res['qzquote_id']."'";

         if(! $this->dbh->Exec($sqlCMD))
         {
            $this->strERR = $this->dbh->strERR;
            return false;
         }

         if(! $this->dbh->FetchRows())
         {
            $this->strERR = GetErrorString("NO_QZQUOTE_ID_FOUND");
            return false;
         }

         $result[$id]['type'] = $this->dbh->GetFieldValue("name");
      }

      return $result;

   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: make_seed
   //
   // [DESCRIPTION]:   Generate unique number
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  integer
   //
   // [CREATED BY]:   Gabriel Istvancsek (gabi@acrux.biz) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function make_seed()
   {
      list($usec, $sec) = explode(' ', microtime());
         return (float) $sec + ((float) $usec * 100000000);
   }

}

?>

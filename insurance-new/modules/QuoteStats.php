<?php

/*****************************************************************************/
/*                                                                           */
/*  CQuoteStats class interface
/*                                                                           */
/*  (C) 2007 Istvancsek Gabi(gabi@acrux.biz)                                     */
/*                                                                           */
/*****************************************************************************/

define("QUOTES_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);


//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuoteStats
//
// [DESCRIPTION]:  CQuoteStats class interface
//
// [FUNCTIONS]:
//                 Close()
//                 GetError()
//                 ShowError()
//
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CQuoteStats
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last Quote error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuoteStats
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQuoteStats($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuoteStats
//
// [DESCRIPTION]:   Verify to see if the requested fields are completed
//
// [PARAMETERS]:    $siteID, $type, $counter
//
// [RETURN VALUE]:  true or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-01-04
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertQuoteStats($siteID='', $type='', $counter='')
{
   $this->strERR = "";

   if(! preg_match("/^\d+$/", $siteID))
     $this->strERR = GetErrorString("INVALID_QUOTE_STATS_SITE_ID_FIELD");

   /*
   (s)|(ss)|(sd)|(f)|(fs)|(fd)|(nq)|(sk)|(a)
   s  - success
   ss - success scan
   sd - success debug
   f  - failure
   fs - failure scan
   fd - failure debug
   nq - no quote
   sk - skipped quote
   a  - actrive
   */

//   if(! preg_match("/^(s)|(ss)|(sd)|(f)|(fs)|(fd)|(nq)|(sk)|(a)$/", $type))
//     $this->strERR = GetErrorString("INVALID_QUOTE_STATS_TYPE_FIELD");

   if(! empty($counter))
      if(! preg_match("/^[0-9-]+$/", $counter))
         $this->strERR = GetErrorString("INVALID_QUOTE_STATS_COUNTER_FIELD");

   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuoteStatsDetails
//
// [DESCRIPTION]:   Verify to see if the requested fields are completed
//
// [PARAMETERS]:    $logID, $siteID, $quoteTypeID, $insurer, $annualPremium
//
// [RETURN VALUE]:  true or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-01-04
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//
//////////////////////////////////////////////////////////////////////////////FE
function AssertQuoteStatsDetails($logID='', $siteID='', $quoteTypeID='', $type='', $status='', &$insurer, &$annualPremium)
{
   $this->strERR = "";

   if(! preg_match("/^\d+$/", $logID))
     $this->strERR = GetErrorString("INVALID_QUOTE_STATS_LOG_ID_FIELD");

   if(! preg_match("/^\d+$/", $siteID))
     $this->strERR = GetErrorString("INVALID_QUOTE_STATS_SITE_ID_FIELD");

   if(! preg_match("/^\d+$/", $quoteTypeID))
     $this->strERR = GetErrorString("INVALID_QUOTE_STATS_QUOTE_TYPE_ID_FIELD");

   if(! preg_match("/^(NORMAL)|(SCAN)|(DEBUG)$/", $type))
     $this->strERR = GetErrorString("INVALID_QUOTE_STATS_STATUS_FIELD");

   if(! preg_match("/^(MIN)|(MAX)$/", $status))
     $this->strERR = GetErrorString("INVALID_QUOTE_STATS_TYPE_FIELD");

   $insurer = str_replace("'", "", trim($insurer));
   $annualPremium = str_replace(",", "", trim($annualPremium));

   if(! preg_match("/([0-9\.]+)/", $annualPremium, $matchesAnnualPremium))
      $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_DETAILS_ANNUAL_PREMIUM_FIELD");

   $annualPremium = $matchesAnnualPremium[1];

   $annualPremium = number_format($annualPremium, 2, '.', '');

   if($annualPremium <= 0)
      $this->strERR .= "\n".GetErrorString("QUOTE_STATS_ANNUAL_PREMIUM_FIELD_IS_NULL");

   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteStats
//
// [DESCRIPTION]:   Add new entry to the quote_stats table
//
// [PARAMETERS]:    $quoteID, $insurer, $annualPremium, $monthlyPremium, $voluntaryExcess
//
// [RETURN VALUE]:  QuoteStatsID or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteStats($siteID='', $type='', $quoteTypeID='', $date='')
{
   if(! $this->AssertQuoteStats($siteID, $type))
      return false;

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
     $this->strERR = GetErrorString("INVALID_QUOTE_STATS_SITE_ID_FIELD");
      return false;
   }

   if(empty($date))
   {
      $date = date('Y-m-d');
   }

   // we have to check if we have already an entry for that site and date
   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_STATS." WHERE site_id='$siteID' AND date='$date'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
      return false;
   }

   // if exist return quote_stats_id
   if($this->dbh->GetRows())
   {
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      // we have to update counter
      $quoteStatsID = $this->dbh->GetFieldValue('id');

      $this->lastSQLCMD = "UPDATE ".SQL_QUOTE_STATS." SET $type=$type + 1 WHERE id='$quoteStatsID'";
   
      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      return true;
   }

   // add an entry by site id and date
   $this->lastSQLCMD = "INSERT INTO ".SQL_QUOTE_STATS." (site_id,quote_type_id,$type,date) VALUES('$siteID','$quoteTypeID', '1', '$date')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQuoteStats
//
// [DESCRIPTION]:   Update quote_stats table
//
// [PARAMETERS]:    $QuoteStatsID, $quoteID, $insurer, $annualPremium, $monthlyPremium, $voluntaryExcess
//
// [RETURN VALUE]:  true or false in case o failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQuoteStats($siteID='', $type='', $counter='', $date='')
{
   if(! $this->AssertQuoteStats($siteID, $type))
      return false;

   if(empty($date))
   {
      $date = date('Y-m-d');
   }

   $this->lastSQLCMD = "UPDATE ".SQL_QUOTE_STATS." SET $type=$type + $counter WHERE site_id='$siteID' AND date='$date'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQuoteStats
//
// [DESCRIPTION]:   Delete an entry from quote_stats table
//
// [PARAMETERS]:    $QuoteStatsID
//
// [RETURN VALUE]:  true or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteQuoteStats($quoteStatsID='')
{
   if(! preg_match("/^\d+$/", $quoteStatsID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATS_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTE_STATS." WHERE id='$quoteStatsID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATS_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_QUOTE_STATS." WHERE id='$quoteStatsID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteStatsByID
//
// [DESCRIPTION]:   Read data from quote_stats table and put it into an array variable
//
// [PARAMETERS]:    $QuoteStatsID
//
// [RETURN VALUE]:  Array(withe the parameters of the $QuoteStatsID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteStatsByID($quoteStatsID='')
{
   if(! preg_match("/^\d+$/", $quoteStatsID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATS_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_STATS." WHERE id='$quoteStatsID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_STATS_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]                   = $this->dbh->GetFieldValue("id");

   $columns = $this->dbh->GetAllFieldsName(SQL_QUOTE_STATS);

   foreach($columns as $index => $name)
   {
      $arrayResult[$name] = $this->dbh->GetFieldValue($name);
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteStats
//
// [DESCRIPTION]:   Add new entry to the quote_stats_cheapest table
//
// [PARAMETERS]:    $quoteID
//
// [RETURN VALUE]:  Array(with the parameters of the $quoteID) or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-01-04
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteStatsDetails($logID='', $siteID='', $quoteTypeID='', $type='', $status='', $insurer='', $annualPremium='', $date='',$time='')
{
   if(! $this->AssertQuoteStatsDetails($logID, $siteID, $quoteTypeID, $type, $status, $insurer, $annualPremium))
      return false;

   if(empty($date))
      $date=date('Y-m-d');

   if(empty($time))
      $time=date('H:i:s');

   // we have to check if we have unique set of values by quoteID
   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTE_STATS_DETAILS." WHERE log_id='$logID' AND site_id='$siteID' AND status='$status'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   // if exist return quote_details_id
   if($this->dbh->GetRows())
   {
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      return $this->dbh->GetFieldValue("id");
   }

   $this->lastSQLCMD = "INSERT INTO ".SQL_QUOTE_STATS_DETAILS." (log_id,site_id,quote_type_id,insurer,annual_premium,status,type,date,time) VALUES('$logID','$siteID','$quoteTypeID','$insurer','$annualPremium','$status','$type','$date','$time')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetQuoteStatsDetailsTop
//
// [DESCRIPTION]:   Add new entry to the quote_stats_cheapest table
//
// [PARAMETERS]:    $logID, $scanMode
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2007-01-04
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetQuoteStatsDetailsTop($logID='', $scanMode='0')
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_STATS_LOG_ID_FIELD");
      return false;
   }

   if(! $scanMode)
   {
      $this->lastSQLCMD = "SELECT count(status) as total FROM ".SQL_QUOTE_STATUS." WHERE log_id='$logID' AND status='WAITING'";
   
      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }
   
      if(! $this->dbh->GetRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }
   
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }
   
      $totalWaiting = $this->dbh->GetFieldValue("total");
   
      if($totalWaiting > 0)
         return true;
   }

   $this->lastSQLCMD = "SELECT id,annual_premium  FROM ".SQL_QUOTE_STATS_DETAILS." WHERE log_id='$logID' AND status='MIN' ORDER BY annual_premium asc";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $result = array();
   while($this->dbh->MoveNext())
   {
      $result[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("annual_premium");
   }

   $counter = 0;
   foreach($result as $quoteStatsDetailsID => $premium)
   {
      $counter++;
      $this->lastSQLCMD = "UPDATE ".SQL_QUOTE_STATS_DETAILS." set position='$counter' WHERE id='$quoteStatsDetailsID'";

      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }
   }

   $this->lastSQLCMD = "SELECT id,annual_premium  FROM ".SQL_QUOTE_STATS_DETAILS." WHERE log_id='$logID' AND status='MAX' ORDER BY annual_premium desc";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $result = array();
   while($this->dbh->MoveNext())
   {
      $result[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("annual_premium");
   }

   $counter = 0;
   foreach($result as $quoteStatsDetailsID => $premium)
   {
      $counter++;
      $this->lastSQLCMD = "UPDATE ".SQL_QUOTE_STATS_DETAILS." set position='$counter' WHERE id='$quoteStatsDetailsID'";

      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteStats
//
// [DESCRIPTION]:   Add quote stats to db
//
// [PARAMETERS]:    $siteID, $quoteTypeID, $status, $type, $date, $prevStatus, $prevType
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-01-08
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteStatsToDB($siteID='', $quoteTypeID='', $status='', $type='', $date='', $prevStatus='', $prevType='')
{
   if(! preg_match("/^\d+$/", $siteID))
   {
     $this->strERR = GetErrorString("INVALID_QUOTE_STATS_SITE_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
     $this->strERR = GetErrorString("INVALID_QUOTE_STATS_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   switch($status)
   {
      case 'WAITING':
         //increment succes counter
         if(! $this->AddQuoteStats($siteID,'a',$quoteTypeID, $date))
            return false;
         break;

      case 'SUCCESS':
         switch($type)
         {
            case 'NORMAL':
               // decrement active counter
               if(! $this->UpdateQuoteStats($siteID,'a','-1', $date))
                  return false;

               // increment success counter
               if(! $this->UpdateQuoteStats($siteID,'s','1', $date))
                  return false;

               break;

            case 'SCAN':

               // increment success counter
               if(! $this->UpdateQuoteStats($siteID,'s','1', $date))
                  return false;
               
               // increment success scan counter
               if(! $this->UpdateQuoteStats($siteID,'ss','1', $date))
                  return false;

               switch($prevStatus)
               {
                  case 'FAILURE':

                     // decrement failure counter
                     if(! $this->UpdateQuoteStats($siteID,'f','-1', $date))
                        return false;

                     switch($prevType)
                     {
                        case 'SCAN':
                           // decrement failure scan counter
                           if(! $this->UpdateQuoteStats($siteID,'fs','-1', $date))
                              return false;
                        break;
                     }

                     break;

                  case 'NO QUOTE':
                     // decrement failure counter
                     if(! $this->UpdateQuoteStats($siteID,'f','-1', $date))
                        return false;

                     switch($prevType)
                     {
                        case 'SCAN':
                           // decrement no quote counter
                           if(! $this->UpdateQuoteStats($siteID,'nq','-1', $date))
                              return false;
                        break;
                     }

                     break;
               }

               break;

            case 'DEBUG':

               // decrement active counter
               if(! $this->UpdateQuoteStats($siteID,'a','-1', $date))
                  return false;

               // increment success counter
               if(! $this->UpdateQuoteStats($siteID,'s','1', $date))
                  return false;
               
               // increment success scan counter
               if(! $this->UpdateQuoteStats($siteID,'sd','1', $date))
                  return false;

               break;
         }
         
         break;

      case 'FAILURE':

         switch($type)
         {
            case 'NORMAL':
               // decrement active counter
               if(! $this->UpdateQuoteStats($siteID,'a','-1', $date))
                  return false;

               // increment failure counter
               if(! $this->UpdateQuoteStats($siteID,'f','1', $date))
                  return false;

               break;

            case 'SCAN':

               switch($prevType)
               {
                  case 'NORMAL':

                     // increment failure scan counter
                     if(! $this->UpdateQuoteStats($siteID,'fs','1', $date))
                        return false;

                     break;
               }

               break;

            case 'DEBUG':

               // decrement active counter
               if(! $this->UpdateQuoteStats($siteID,'a','-1', $date))
                  return false;

               // increment failure debug counter
               if(! $this->UpdateQuoteStats($siteID,'fd','1', $date))
                  return false;
               
               // increment failure counter
               if(! $this->UpdateQuoteStats($siteID,'f','1', $date))
                  return false;

               break;
         }
         
         break;

      case 'NO QUOTE':

         switch($prevStatus)
         {
            case 'WAITING':
               // decrement active counter
               if(! $this->UpdateQuoteStats($siteID,'a','-1', $date))
                  return false;
      
               // increment no quote counter
               if(! $this->UpdateQuoteStats($siteID,'nq','1', $date))
                  return false;

               break;

            case 'FAILURE':

               switch($prevType)
               {
                  case 'NORMAL':
                     
                     // increment no quote counter
                     if(! $this->UpdateQuoteStats($siteID,'nq','1', $date))
                        return false;

                     // decrement failure counter
                     if(! $this->UpdateQuoteStats($siteID,'f','-1', $date))
                        return false;

                     break;

                  case 'SCAN':
                     
                     // increment no quote counter
                     if(! $this->UpdateQuoteStats($siteID,'nq','1', $date))
                        return false;

                     // decrement failure counter
                     if(! $this->UpdateQuoteStats($siteID,'f','-1', $date))
                        return false;

                     // decrement failure scan counter
                     if(! $this->UpdateQuoteStats($siteID,'fs','-1', $date))
                        return false;

                     break;
               }

               break;
         }

         break;

      case 'SKIPPED':

         // increment skipped counter
         if(! $this->AddQuoteStats($siteID,'sk',$quoteTypeID, $date))
            return false;

         break;
   }

   return true;

}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Print the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    return $this->strERR;
}

} // end of CQuote class
?>

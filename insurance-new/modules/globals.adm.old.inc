<?php
include_once("/home/www/wl-config.quotezone.co.uk/insurance-new/system/GetConfFileData.php");
GetConfFileData::getInstance();

////////////////////////////////////////////////////////////////////////////////
///                       Global definitions area                            ///
////////////////////////////////////////////////////////////////////////////////

define("ROOT_PATH", "/home/www/quotezone.co.uk/insurance-new/");

// set init path
ini_set("include_path", ROOT_PATH.":".ROOT_PATH."modules");

include_once "sql.adm.inc";

// debug options
define("DEBUG_MODE", "1");

// language options
define("LANGUAGE", ""); // language type en,sp

if(DEBUG_MODE)
{

   // database options
   define("DBHOST", "localhost");
   define("DBNAME", "ci_new");
   define("DBUSER", getenv("DBUSER"));
   define("DBPASS", getenv("DBPASS"));

   // web site settings
   define("WEB_SITE_ROOT", "http://darkstar");
   define("TEMPLATES_DIR", ROOT_PATH."templates");
   define("PRELOAD_TEMPLATES", "Site.tmpl Header.tmpl Footer.tmpl");
   define("WEB_SITE_FIRST_TMPL", "Site.tmpl");

}

// database options
define("DBHOST", "localhost");
define("DBNAME", "ci_new");
define("DBUSER", getenv("DBUSER"));
define("DBPASS", getenv("DBPASS"));

?>

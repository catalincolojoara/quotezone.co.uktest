<?php
/*****************************************************************************/
/*                                                                           */
/*  CTrackDetails class interface                                               */
/*                                                                           */
/*  (C) 2007 Istvancsek Gabriel (gabi@acrux.biz)                                   */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CTrackDetails
//
// [DESCRIPTION]:  CTrackDetails class interface
//
// [FUNCTIONS]:    bool AssertTrakerUrls($trackUrl='',$name='')
//                 int  AddTrackDetails($trackUrl='', $name='')
//                 bool UpdateTrackDetails($trackUrlID=0,$trackUrl='',$name='')
//                 bool DeleteTrackDetails($trackUrlID=0)
//                 bool GetAllTrackDetails()
//                 bool GetTrackDetailsByID($trackUrlID=0)
//                 bool GetTrackDetailsByUrl($trackUrl="")
//                 bool GetTrackUrlByName($name="")
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CTrackDetails
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CTrackDetails
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 08-10-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CTrackDetails($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertTrackerDetails
//
// [DESCRIPTION]:   Verify to see if the params are completed
//
// [PARAMETERS]:    $logID='',$siteID='', $details=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 08-10-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertTrackDetails($logID='',$siteID='', $details='')
{
   $this->strERR = "";

   if(! preg_match("/^\d+$/", $logID))
      $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");
   
   if(! preg_match("/^\d+$/", $siteID))
      $this->strERR = GetErrorString("INVALID_SITE_ID_FIELD");
   
   if(empty($details))
      $this->strERR = GetErrorString("INVALID_TRACK_DETAILS_FIELD");

    if($this->strERR != "")
      return true;

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddTrackDetails
//
// [DESCRIPTION]:   Add new entry to the tracker_details table
//
// [PARAMETERS]:    $trackUrl='',$name=''
//
// [RETURN VALUE]:  trUrlID or 0 in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddTrackDetails($logID='',$siteID='', $details='')
{
   if($this->AssertTrackDetails($logID,$siteID, $details))
      return 0;

   $this->lastSQLCMD = "INSERT INTO ".SQL_TRACKER_DETAILS." (log_id, site_id, details, date, time) VALUES ('$logID','$siteID','$details',now(),now())";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateTrackDetails
//
// [DESCRIPTION]:   Update a entry to the track_urls table
//
// [PARAMETERS]:    $trackUrlID=0, $trackUrl='', $name=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateTrackDetails($trackUrlID=0, $trackUrl='', $name='')
{
   // TO DO
   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteTrackDetails
//
// [DESCRIPTION]:   Delete an entry from track_urls table by ID
//
// [PARAMETERS]:    $trackUrlID=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteTrackDetails($trackUrlID=0)
{
   // TO DO 
   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTotalTrackDetailsBySiteID
//
// [DESCRIPTION]:   Get all content of the table tracker_details
//
// [PARAMETERS]:    $siteID, $startDate, $endDate
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 08-10-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTotalTrackDetailsBySiteID($siteID, $startDate, $endDate)
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");
      return false;
   }

   if(empty($startDate))
   {
      $this->strERR = GetErrorString("INVALID_START_DATE_FIELD");
      return false;
   }
   
   if(empty($endDate))
   {
      $this->strERR = GetErrorString("INVALID_END_DATE_FIELD");
      return false;
   }
   
   if(! $startDate =  $this->StdToMysqlDate($startDate))
      return false;
      
   if(! $endDate =  $this->StdToMysqlDate($endDate))
      return false;

   $this->lastSQLCMD = "SELECT count(*) as total, details FROM ".SQL_TRACKER_DETAILS." WHERE site_id='$siteID' AND date between '$startDate' AND '$endDate' GROUP BY details";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("TRACK_TRACKER_DETAILS_FOUND");
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("details")]   = $this->dbh->GetFieldValue("total");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   if(! preg_match("/\d{1,2}\/\d{1,2}\/\d{4}/i",$date))
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_STD_TO_MYSQL_DATE")."\n";

   if(! empty($this->strERR))
      return false;
   
   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print  $this->strERR;
}

}

?>

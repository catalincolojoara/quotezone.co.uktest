<?php
/*****************************************************************************/
/*                                                                           */
/*  CAAVehicle class interface                                            */
/*                                                                           */
/*  (C) 2005 Velnic Daniel (dan@acrux.biz)                                   */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";



if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CAAVehicle
//
// [DESCRIPTION]:  CAAVehicle class interface
//
// [FUNCTIONS]:   true   | false AssertAAVehicle($MAKEID="", $MAKENAME="", $MODELID="", $MODELNAME="", $TBLMODELNAME="", $TBLBVCCODE="", $TBLCC="", 
//                                               $TBLFIRSTYEAR="", $TBLLASTYEAR="", $TBLDOORS="", $TBLFUEL="", $TBLTRANS="", $TBLBODY="")
//
//                int    | false AddAAVehicle$MAKEID="", $MAKENAME="", $MODELID="", $MODELNAME="", $TBLMODELNAME="", $TBLBVCCODE="", $TBLCC="", 
//                                               $TBLFIRSTYEAR="", $TBLLASTYEAR="", $TBLDOORS="", $TBLFUEL="", $TBLTRANS="", $TBLBODY="")
//
//                array  | false GetAAVehicleByID($aaVehicleID='')
//                int    | false GetAAVehicleByAbiCode($aaVehicleAbi='')
//
//                Close();
//                GetError();
//                ShowError();
//
// [CREATED BY]:  Velnic Daniel (dan@acrux.biz)
//
// [MODIFIED]:    - [programmer (email) date]
//                  [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CAAVehicle
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CAAVehicle
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CAAVehicle($dbh=0)
{

   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertAAVehicle
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    $MAKEID="", $MAKENAME="", $MODELID="", $MODELNAME="", 
// 					  $TBLMODELNAME="", $TBLBVCCODE="", $TBLCC="", $TBLFIRSTYEAR="", 
//                  $TBLLASTYEAR="", $TBLDOORS="", $TBLFUEL="", $TBLTRANS="", $TBLBODY=""
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertAAVehicle($MAKEID="", $MAKENAME="", $MODELID="", $MODELNAME="", $TBLMODELNAME="", $TBLBVCCODE="", $TBLCC="", $TBLFIRSTYEAR="", $TBLLASTYEAR="", $TBLDOORS="", $TBLFUEL="", $TBLTRANS="", $TBLBODY="")
{
   $this->strERR = '';

   if(! preg_match('/^\d+$/i',$MAKEID))
      $this->strERR .= GetErrorString("INVALID_MAKE_ID")."\n";

   if(empty($MAKENAME))
      $this->strERR .= GetErrorString("INVALID_MAKE_NAME")."\n";

   if(! preg_match('/^\d+$/i',$MODELID))
      $this->strERR .= GetErrorString("INVALID_MODEL_ID")."\n";

   if(empty($MODELNAME))
      $this->strERR .= GetErrorString("INVALID_MODEL_NAME")."\n";

   if(empty($TBLMODELNAME))
      $this->strERR .= GetErrorString("INVALID_TBL_MODEL_NAME")."\n";

   if(! preg_match('/[\d]{8}/i',$TBLBVCCODE))
      $this->strERR .= GetErrorString("INVALID_ABI_CODE")."\n";

   if(! preg_match('/^\d+$/i',$TBLCC))
      $this->strERR .= GetErrorString("INVALID_CC")."\n";

   if(! preg_match('/^\d+$/i',$TBLFIRSTYEAR))
      $this->strERR .= GetErrorString("INVALID_FROM_YEAR")."\n";

   if(! preg_match('/^\d+$/i',$TBLLASTYEAR))
      $this->strERR .= GetErrorString("INVALID_LAST_YEAR")."\n";

   if(! preg_match('/^\d+$/i',$TBLDOORS))
      $this->strERR .= GetErrorString("INVALID_DORS")."\n";

   if(empty($TBLFUEL))
      $this->strERR .= GetErrorString("INVALID_FUEL")."\n";

   if(empty($TBLTRANS))
      $this->strERR .= GetErrorString("INVALID_FUEL")."\n";

   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddAAVehicle
//
// [DESCRIPTION]:   Check if exist a car and add if not exist into AA_VEHICLE table
//
// [PARAMETERS]:    $MAKEID="", $MAKENAME="", $MODELID="", $MODELNAME="", 
//               	  $TBLMODELNAME="", $TBLBVCCODE="", $TBLCC="", $TBLFIRSTYEAR="", 
//                  $TBLLASTYEAR="", $TBLDOORS="", $TBLFUEL="", $TBLTRANS="", $TBLBODY=""
//
// [RETURN VALUE]:  $aaVehicleID | false
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddAAVehicle($MAKEID="", $MAKENAME="", $MODELID="", $MODELNAME="", $TBLMODELNAME="", $TBLBVCCODE="", $TBLCC="", $TBLFIRSTYEAR="", $TBLLASTYEAR="", $TBLDOORS="", $TBLFUEL="", $TBLTRANS="", $TBLBODY="")
{

   if(! $this->AssertAAVehicle($MAKEID, $MAKENAME, $MODELID, $MODELNAME, $TBLMODELNAME, $TBLBVCCODE, $TBLCC, $TBLFIRSTYEAR, $TBLLASTYEAR, $TBLDOORS, $TBLFUEL, $TBLTRANS))
      return false;

   $lastSqlCmd = "SELECT id FROM AA_VEHICLE WHERE TBLBVCCODE='$TBLBVCCODE'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   if($this->dbh->GetRows())
   {
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      $aaVehicleID = $this->dbh->GetFieldValue("id");

      return $aaVehicleID;
   }


   $lastSqlCmd = "INSERT INTO AA_VEHICLE VALUES ('','$MAKEID', '".addslashes($MAKENAME)."', '".addslashes($MODELID)."','".addslashes($MODELNAME)."', '".addslashes($TBLMODELNAME)."', '$TBLBVCCODE', '$TBLCC', '$TBLFIRSTYEAR', '$TBLLASTYEAR', '$TBLDOORS', '$TBLFUEL', '$TBLTRANS', '$TBLBODY');";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $lastSqlCmd = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAAVehicleByID
//
// [DESCRIPTION]:   Read data of a car from AA_VEHICLE table and put it into an array variable
//
// [PARAMETERS]:    $aaVehicleID
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Velnic Daniel  (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAAVehicleByID($aaVehicleID='')
{
   if(! preg_match("/^\d+$/", $aaVehicleID))
   {
      $this->strERR = GetErrorString("INVALID_AA_VEHICEL_ID");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM AA_VEHICLE WHERE id='$aaVehicleID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AA_VEHICLE_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

	$arrayResult["MAKEID"]       = trim($this->dbh->GetFieldValue("MAKEID"));
	$arrayResult["MAKENAME"]     = stripslashes(trim($this->dbh->GetFieldValue("MAKENAME")));
	$arrayResult["MODELID"]      = stripslashes(trim($this->dbh->GetFieldValue("MODELID")));
	$arrayResult["MODELNAME"]    = stripslashes(trim($this->dbh->GetFieldValue("MODELNAME")));
	$arrayResult["TBLMODELNAME"] = stripslashes(trim($this->dbh->GetFieldValue("TBLMODELNAME")));
	$arrayResult["TBLBVCCODE"]   = trim($this->dbh->GetFieldValue("TBLBVCCODE"));
	$arrayResult["TBLCC"]        = trim($this->dbh->GetFieldValue("TBLCC"));
	$arrayResult["TBLFIRSTYEAR"] = trim($this->dbh->GetFieldValue("TBLFIRSTYEAR"));
	$arrayResult["TBLLASTYEAR"]  = trim($this->dbh->GetFieldValue("TBLLASTYEAR"));
	$arrayResult["TBLDOORS"]     = trim($this->dbh->GetFieldValue("TBLDOORS"));
	$arrayResult["TBLFUEL"]      = trim($this->dbh->GetFieldValue("TBLFUEL"));
	$arrayResult["TBLTRANS"]     = trim($this->dbh->GetFieldValue("TBLTRANS"));
	$arrayResult["TBLBODY"]      = trim($this->dbh->GetFieldValue("TBLBODY"));

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAAVehicleByAbiCode
//
// [DESCRIPTION]:   Read data of a car from AA_VEHICLE table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Velnic Daniel  (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAAVehicleByAbiCode($aaVehicleAbi='')
{
   if(! preg_match("/^\d+$/", $aaVehicleAbi))
   {
      $this->strERR = GetErrorString("INVALID_AA_VEHICEL_ABI");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM AA_VEHICLE WHERE TBLBVCCODE='$aaVehicleAbi'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AA_VEHICLE_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

	$arrayResult["MAKEID"]       = trim($this->dbh->GetFieldValue("MAKEID"));
	$arrayResult["MAKENAME"]     = stripslashes(trim($this->dbh->GetFieldValue("MAKENAME")));
	$arrayResult["MODELID"]      = stripslashes(trim($this->dbh->GetFieldValue("MODELID")));
	$arrayResult["MODELNAME"]    = stripslashes(trim($this->dbh->GetFieldValue("MODELNAME")));
	$arrayResult["TBLMODELNAME"] = stripslashes(trim($this->dbh->GetFieldValue("TBLMODELNAME")));
	$arrayResult["TBLBVCCODE"]   = trim($this->dbh->GetFieldValue("TBLBVCCODE"));
	$arrayResult["TBLCC"]        = trim($this->dbh->GetFieldValue("TBLCC"));
	$arrayResult["TBLFIRSTYEAR"] = trim($this->dbh->GetFieldValue("TBLFIRSTYEAR"));
	$arrayResult["TBLLASTYEAR"]  = trim($this->dbh->GetFieldValue("TBLLASTYEAR"));
	$arrayResult["TBLDOORS"]     = trim($this->dbh->GetFieldValue("TBLDOORS"));
	$arrayResult["TBLFUEL"]      = trim($this->dbh->GetFieldValue("TBLFUEL"));
	$arrayResult["TBLTRANS"]     = trim($this->dbh->GetFieldValue("TBLTRANS"));
	$arrayResult["TBLBODY"]      = trim($this->dbh->GetFieldValue("TBLBODY"));

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Show the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Velnic Daniel (dan@acrux.biz) 2006-02-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

}//end class CAAVehicle

?>
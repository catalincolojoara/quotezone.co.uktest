<?php
/*****************************************************************************/
/*                                                                           */
/*  CVanMenu class interface                                                 */
/*                                                                           */
/*  (C) 2006 Eugen Savin (seugen@acrux.biz) 2006-11-14                       */
/*                                                                           */
/*****************************************************************************/
define("VANMENU_INCLUDED", "1");

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CVanMenu
//
// [DESCRIPTION]:  CVanMenu class interface
//
// [FUNCTIONS]:    void    SetMenu($menuArray=array());
//
//                 int     HaveClaims();
//                 bool    DriverDeclaredClaims($driver=0);
//                 bool    DriverHasMoreClaims($driver=0, $claim=0);
//                 int     GetClaimsNr($driver=0);
//
//                 int     HaveConvictions();
//                 bool    DriverDeclaredConvictions($driver=0);
//                 bool    DriverHasMoreConvictions($driver=0);
//                 int     GetConvictionsNr($driver=0);
//
//                 int     GetDriversNr();
//                 int     GetDeclaredDriversNr();
//                 int     GetMenuDriver();
//                 int     GetMenuCatIndex();
//                 int     GetMenuCategory();
//                 string  GetFormAction();
//                 string  GetPageAction();
//                 void    GetMenuNav();
//
//                 void    SetForwardStep($activeStep=-1);
//                 void    SetBackwardStep();
//                 void    SetFirstStep();
//                 void    SetLastStep();
//
//                 void    SetActiveStep($category=0, $driver=0, $catIndex=0);
//                 void    GetActiveStep();
//                 array   GetStepsHistory();
//
//                 void    SaveSessionMenu();
//                 void    ReadSession();
//                 void    RebuildStepsHistory();
//
// [CREATED BY]:   Eugen Savin (seugen@acrux.biz) 2006-11-14
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CVANMenu
{
   var $session;
   var $activeStep;
   var $menuNav;
   var $strErr;

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CVanMenu
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CVanMenu($session=array())
{
   $this->session    = $session;
   $this->activeStep = "0:0:0";
   $this->strErr     = "";

   $this->menuNav = array ('vehicle',
                           'drivers',
                           'cover',
                           'claims',
                           'convictions',
                           'quote summary',
                           'quote results');

   if(! is_array($this->session['_ACTIVE_STEP_HISTORY_']))
   {
      $this->session['_ACTIVE_STEP_HISTORY_'] = array();
      array_push($this->session['_ACTIVE_STEP_HISTORY_'], $this->activeStep);
   }

   if($this->session['_ACTIVE_STEP_'])
      $this->activeStep = $this->session['_ACTIVE_STEP_'];


   $this->SaveSessionMenu();
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: HaveClaims
//
// [DESCRIPTION]:   return the first driver that has claim(s)
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  integer (driver id)
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function HaveClaims()
{
   $driver = -1;

   for($i=0; $i<$this->GetDriversNr(); $i++)
   {
      if($this->DriverDeclaredClaims($i))
      {
         $driver = $i;
         break;
      }
   }

   return $driver;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DriverDeclaredClaims
//
// [DESCRIPTION]:   check to see if the specified driver has claims
//
// [PARAMETERS]:    $driver=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DriverDeclaredClaims($driver=0)
{
   $additionalDriver = '';

   if($driver > 0)   
      $additionalDriver = "additional_";

   if($this->session['_DRIVERS_'][$driver][$additionalDriver.'had_any_accidents'] == "Y")
      return true;

   return false;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DriverHasMoreClaims
//
// [DESCRIPTION]:   check to see if the specified driver has another claim
//
// [PARAMETERS]:    $driver=0, $claim=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DriverHasMoreClaims($driver=0, $claim=0)
{
   if($this->session['_CLAIMS_'][$driver][$claim]['any_other_acc'] == "Y")
      return true;

   return false;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DriverHasMoreConvictions
//
// [DESCRIPTION]:   check to see if the specified driver has another conviction
//
// [PARAMETERS]:    $driver=0, $convictionNr=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DriverHasMoreConvictions($driver=0, $convictionNr=0)
{
   if($this->session['_CONVICTIONS_'][$driver][ $convictionNr]['any_other_conv'] == "Y")
      return true;

   return false;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetClaimsNr
//
// [DESCRIPTION]:   return the number of claims that the specified driver has
//
// [PARAMETERS]:    $driver=0
//
// [RETURN VALUE]:  integer
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetClaimsNr($driver=0)
{
   return count($this->session['_CLAIMS_'][$driver]);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: HaveConvictions
//
// [DESCRIPTION]:   return the first driver number that has conviction(s)
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  integer
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function HaveConvictions()
{
   $driver = -1;

   for($i=0; $i<$this->GetDriversNr(); $i++)
   {
      if($this->DriverDeclaredConvictions($i))
      {
         $driver = $i;
         break;
      }
   }

   return $driver;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DriverDeclaredConvictions
//
// [DESCRIPTION]:   check to see if the specified driver has convictions
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DriverDeclaredConvictions($driver=0)
{
  $additionalDriver = '';

   if($driver > 0)
      $additionalDriver = "additional_";
   
   if($this->session['_DRIVERS_'][$driver][$additionalDriver.'had_any_convictions'] == "Y")
      return true;

   return false;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetConvictionsNr
//
// [DESCRIPTION]:   return the number of convictions that the specified driver has
//
// [PARAMETERS]:    $driver=0
//
// [RETURN VALUE]:  integer
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetConvictionsNr($driver=0)
{
   return count($this->session['_CONVICTIONS_'][$driver]);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDriversNr
//
// [DESCRIPTION]:   return the number of drivers
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  integer
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDriversNr()
{
   return count($this->session['_DRIVERS_']);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDeclaredDriversNr
//
// [DESCRIPTION]:   return the number of drivers declared by the proposer
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  integer
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDeclaredDriversNr()
{
   $nrOfDrivers = 0;

   if($this->session['_DRIVERS_'][0]['who_drive_the_vehicle'] == "IO")
      $nrOfDrivers = 1;
   else if($this->session['_DRIVERS_'][0]['who_drive_the_vehicle'] == "IS")
      $nrOfDrivers = 2;
   else // 02 03 04
      $nrOfDrivers = intval($this->session['_DRIVERS_'][0]['who_drive_the_vehicle']);

   return $nrOfDrivers;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetForwardStep
//
// [DESCRIPTION]:   sets the next step as active
//
// [PARAMETERS]:    $activeStep=-1
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetForwardStep($activeStep=-1)
{
   // refresh session content
   $this->ReadSession();

   $foundNextStep = true;
   
   if($activeStep < 0)
      $activeStep = $this->activeStep;
      
   list($category, $driver, $catIndex) = split(":", $activeStep);
   
   switch($this->menuNav[$category])
   {
      // 0
      case 'vehicle':
         $this->SetActiveStep($category + 1);
      break;

      // 1
      case 'drivers':
      	if($this->session['_QZ_QUOTE_DETAILS_']['wlUserID'] == '496' || $this->session['NEWSTD_WL'])
      	 {
		  		 if($catIndex == 0)
		  		 {
		  			$this->SetActiveStep($category, $driver, $catIndex+1);
		  		    break;
		  		 }   
		  		
		  	     if($driver < $this->GetDeclaredDriversNr()-1)
		         {
		            $this->SetActiveStep($category, $driver+1);
		            break;
		         }            
      	 }
         else 
         {
            if($driver < $this->GetDeclaredDriversNr()-1)
            {
               $this->SetActiveStep($category, $driver+1);
               break;
            }   
         }
         $this->SetActiveStep($category + 1);
      break;

      // 2
      case 'cover':
         // do we have claims?
         $category++;
         $driver = $this->HaveClaims();
         if($driver >= 0)
         {
            $this->SetActiveStep($category, $driver);
            break;
         }

         // do we have convictions?
         $category++;
         $driver = $this->HaveConvictions();
         if($driver >= 0)
         {
            $this->SetActiveStep($category, $driver);
            break;
         }

         // quote summary
         $this->SetActiveStep($category + 1);

      break;

      // 3
      case 'claims':
         // this driver has more claims
         if($this->DriverHasMoreClaims($driver, $catIndex))
         {
            $this->SetActiveStep($category, $driver, $catIndex+1);
            break;
         }

         // check if any of the other drivers have claims
         $driver++;
         $haveMoreClaims = false;

         for($driver; $driver < $this->GetDriversNr(); $driver++)
         {
            if($this->DriverDeclaredClaims($driver))
            {
               $haveMoreClaims = true;
               break;
            }
         }

         // we still have claims
         if($haveMoreClaims)
         {
            $this->SetActiveStep($category, $driver);
            break;
         }

         // do we have convictions?
         $category++;
         $driver = $this->HaveConvictions();
         if($driver >= 0)
         {
             $this->SetActiveStep($category, $driver);
             break;
         }

         // quote summary
         $this->SetActiveStep($category + 1);
      break;

      case 'convictions':
         // this driver has more convictions
         if($this->DriverHasMoreConvictions($driver, $catIndex))
         {
            $this->SetActiveStep($category, $driver, $catIndex+1);
            break;
         }

         // check if any of the other drivers have convictions
         $driver++;
         $haveMoreConvictions = false;

         for($driver; $driver < $this->GetDriversNr(); $driver++)
         {
            if($this->DriverDeclaredConvictions($driver))
            {
               $haveMoreConvictions = true;
               break;
            }
         }

         // we still have convictions
         if($haveMoreConvictions)
         {
            $this->SetActiveStep($category, $driver);
            break;
         }

         // quote summary
         $this->SetActiveStep($category + 1);
      break;

      case 'quote summary':
         $this->SetActiveStep($category + 1);
      break;

      case 'quote results':
         $this->SetActiveStep($category + 1);
      break;

      default:
         $foundNextStep = false;
      break;

   } // switch($menu[$category])

   if(! $foundNextStep)
      return false;
      
   array_push($this->session['_ACTIVE_STEP_HISTORY_'], $this->activeStep);   
   $this->SaveSessionMenu();

   return true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetBackwardStep
//
// [DESCRIPTION]:   sets the previous step as active
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetBackwardStep()
{
   // remove the last element
   array_pop($this->session['_ACTIVE_STEP_HISTORY_']);
   
   // get the last element in the list
   $lastHistIndex = count($this->session['_ACTIVE_STEP_HISTORY_']) - 1;

   // set new active step
   $this->activeStep = $this->session['_ACTIVE_STEP_HISTORY_'][$lastHistIndex];

   // save session
   $this->SaveSessionMenu();
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetFirstStep
//
// [DESCRIPTION]:   sets the first step as active
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetFirstStep()
{
   $this->activeStep = "0:0:0";  
   $this->session['_ACTIVE_STEP_HISTORY_'] = array();
   array_push($this->session['_ACTIVE_STEP_HISTORY_'], "0:0:0");

   // save session
   $this->SaveSessionMenu();
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetLastStep
//
// [DESCRIPTION]:   sets the last step as active
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetLastStep()
{
   // get the last element in the list
   $lastHistIndex = count($this->session['_ACTIVE_STEP_HISTORY_']) - 1;

   // set new active step
   $this->activeStep = $this->session['_ACTIVE_STEP_HISTORY_'][$lastHistIndex];

   // save session
   $this->SaveSessionMenu();
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetActiveStep
//
// [DESCRIPTION]:   set the active step
//
// [PARAMETERS]:    $category=0, $driver=0, $catIndex=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetActiveStep($category=0, $driver=0, $catIndex=0)
{
   $this->activeStep = "$category:$driver:$catIndex";
}



//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetActiveStep
//
// [DESCRIPTION]:   return the active step
//
// [PARAMETERS]:    none 
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetActiveStep()
{
   return $this->activeStep;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetStepsHistory
//
// [DESCRIPTION]:   return the steps history
//
// [PARAMETERS]:    none 
//
// [RETURN VALUE]:  array of history steps
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetStepsHistory()
{
   return $this->session['_ACTIVE_STEP_HISTORY_'];
}



//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SaveSessionMenu
//
// [DESCRIPTION]:   saves the menu into the session
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SaveSessionMenu()
{
   $_SESSION["_ACTIVE_STEP_"]         = $this->GetActiveStep();
   $_SESSION["_ACTIVE_STEP_HISTORY_"] = $this->session['_ACTIVE_STEP_HISTORY_'];
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ReadSession
//
// [DESCRIPTION]:   read the session
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ReadSession()
{
   $this->session = $_SESSION;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: RebuildStepsHistory
//
// [DESCRIPTION]:   rebuilds the history of the forward steps
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function RebuildStepsHistory()
{
   $this->ReadSession();
   $this->SetFirstStep();
   
   while(true)
      if(! $this->SetForwardStep())
         break;

   // move to the previous step as this was the last OK step
   $this->SetBackwardStep();
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMenuDriver
//
// [DESCRIPTION]:   get the active driver
//
// [PARAMETERS]:    $activeStep
//
// [RETURN VALUE]:  none | array
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMenuDriver()
{
   list($category, $driver, $catIndex) = split(":", $this->activeStep);
   return $driver;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMenuCatIndex
//
// [DESCRIPTION]:   get the active catindex
//
// [PARAMETERS]:    $activeStep
//
// [RETURN VALUE]:  none | array
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMenuCatIndex()
{
   list($category, $driver, $catIndex) = split(":", $this->activeStep);
   return $catIndex;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMenuCategory()
//
// [DESCRIPTION]:   get the active category
//
// [PARAMETERS]:    $activeStep
//
// [RETURN VALUE]:  none | string
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMenuCategory()
{
   list($category, $driver, $catIndex) = split(":", $this->activeStep);
   return $this->menuNav[$category];
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFormAction
//
// [DESCRIPTION]:   get the form action
//
// [PARAMETERS]:    $formAction
//
// [RETURN VALUE]:  none | string
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFormAction()
{
   $formAction = strtolower($_POST["form_action"]);

   if(! $formAction)
      $formAction = "show";

   return $formAction;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetPageAction
//
// [DESCRIPTION]:   get the content of a driver. Can be either next or back
//
// [PARAMETERS]:    $driver
//
// [RETURN VALUE]:  none | array
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetPageAction()
{
   $action = $_POST["step"];

   if(! $action)
      $action = "next";

   return $action;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMenuNav
//
// [DESCRIPTION]:   get the navigation bar
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2006-11-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMenuNav()
{
   return $this->menuNav;
}

} // end CVanMenu

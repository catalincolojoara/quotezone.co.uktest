<?php
/*****************************************************************************/
/*                                                                           */
/*  EmailOutboundsEngineHome class interface                                 */
/*                                                                           */
/*  (C) 2008 Gabriel ISTVANCSEK (gabi@acrux.biz)                             */
/*                                                                           */
/*****************************************************************************/
include_once "errors.inc";
include_once "MySQL.php";
include_once "EmailStore.php";
include_once "EmailReminder.php";
include_once "EmailStoreCodes.php";
include_once "QuoteDetails.php";
include_once "Occupation.php";
include_once "Business.php";
include_once "Session.php";
include_once "EmailOutboundsEngine.php";
include_once "EmailOutbounds.php";
include_once "/home/www/quotezone.co.uk/common/modules/CashBack.php";

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CEmailOutboundsEngineHome
//
// [DESCRIPTION]:  CEmailOutboundsEngineVan class interface, email outbounds support,
//                 PHP version
//
// [FUNCTIONS]:    FormatUserDetails($Session)
//                 CheckEmailOutboundResponse()
//                 SendUserDetails()
//                 ShowError()
//                 GetError()
//
//
//  (C) 2008 Gabriel ISTVANCSEK (gabi@acrux.biz) 2008-02-01
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CEmailOutboundsEngineHome Extends CEmailOutboundsEngine
{
    var $fileName;           // the fileName
    var $strERR;             // last error string
    var $systemType;         // system type(car,van,home,bike)
    var $quoteTypeID;        // quote type id (1-car , 2-van, 4-home, 5-bike)
    var $objEmailStore;      // email store id field
    var $objEmailStoreCodes; // EmailStoreCodes.php class object
    var $objEmailReminder;   // EmailReminder.php class object
    var $objQuoteDetails;    // QuoteDetails.php class object
    var $objOccupation;      // Occupation.php class ojject
    var $objBusiness;        // Business.php class object
    var $objSession;         // Session.php class object
    var $objEmailOutbounds;  //email outbound object
    var $host;               //emv host
    var $path;               //emv path
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CEmailOutboundsEngineVan
   //
   // [DESCRIPTION]:   Default class constructor. Initialization goes here.
   //
   // [PARAMETERS]:    $fileName, $dbh
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CEmailOutboundsEngineHome($fileName, $dbh)
   {
      CEmailOutboundsEngine::CEmailOutboundsEngine('home','in');

      $this->fileName = $fileName;

      $this->systemType  = 'home';
      $this->quoteTypeID = '4';

       // ticket - ZTH-983371
      $this->host = "trc.emv2.com";
      $this->path = "/D2UTF8";

      $this->strERR  = "";

      if($dbh)
      {
         $this->dbh = $dbh;
         $this->closeDB = false;
      }
      else
      {
         // default configuration
         $this->dbh = new CMySQL();

         if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
         {
           $this->strERR = $this->dbh->GetError();
           return;
         }
      }

      $this->closeDB = true;

      $this->objQuoteDetails    = new CQuoteDetails($this->dbh);
      $this->objEmailStore      = new CEmailStore($this->dbh);
      $this->objEmailStoreCodes = new CEmailStoreCodes($this->dbh);
      $this->objEmailReminder   = new CEmailReminder($this->dbh);
      $this->objOccupation      = new COccupation($this->dbh);
      $this->objBusiness        = new CBusiness($this->dbh);
      $this->objSession         = new CSession($this->dbh);
      $this->objEmailOutbounds  = new CEmailOutbounds($this->dbh);

   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: FormatUserDetails
   //
   // [DESCRIPTION]:   Form user data details to be sent it
   //
   // [PARAMETERS]:    $Session
   //
   // [RETURN VALUE]:  formated $string or false if failure
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function FormatUserDetails($Session)
   {
      /*start of driver details */
   $_PROPOSER = array(

      "marital_status" => array(
         "M"=>"Married",
         "S"=>"Single",
         "D"=>"Divorced",
         "W"=>"Widowed",
      ),
   
      "employment_status" => array(
         "E"=>"Employed",
         "F"=>"Full-time education",
         "S"=>"Self Employed",
         "R"=>"Retired",
         "U"=>"Unemployed",
         ),
   
      "occupation_list" => array (
            "A01"=>"Accountant",
            "002"=>"Accounts Assistant",
            "003"=>"Accounts Clerk",
            "M07"=>"Accounts Manager",
            "007"=>"Administration Assistant",
            "008"=>"Administration Clerk",
            "M08"=>"Administration Manager",
            "A07"=>"Administration Staff",
            "009"=>"Administrator",
            "559"=>"Aircraft Engineer",
            "394"=>"Applications Programmer",
            "395"=>"Assistant Manager",
            "032"=>"Bank Clerk",
            "033"=>"Bank Manager",
            "B04"=>"Bank Staff",
            "B13"=>"Bricklayer",
            "B15"=>"Builder",
            "049"=>"Builders Labourer",
            "051"=>"Bus Driver",
            "B22"=>"Buyer",
            "C49"=>"Care Assistant",
            "C03"=>"Carpenter",
            "C06"=>"Cashier",
            "063"=>"Chartered Accountant",
            "C13"=>"Chef",
            "E13"=>"Civil Engineer",
            "C18"=>"Civil Servant",
            "C20"=>"Cleaner",
            "619"=>"Clerical Assistant",
            "496"=>"Clerical Officer",
            "C21"=>"Clerk",
            "084"=>"Company Director",
            "086"=>"Computer Analyst",
            "C55"=>"Computer Consultant",
            "087"=>"Computer Engineer",
            "688"=>"Computer Manager",
            "C56"=>"Computer Operator",
            "C57"=>"Computer Programmer",
            "C58"=>"Computer Technician",
            "088"=>"Consultant",
            "409"=>"Customer Advisor",
            "637"=>"Delivery Driver",
            "411"=>"Design Engineer",
            "115"=>"Doctor",
            "E28"=>"Electrical Engineer",
            "E04"=>"Electrician",
            "121"=>"Electronic Engineer",
            "657"=>"Electronics Technician",
            "E09"=>"Engineer",
            "F01"=>"Factory Worker",
            "135"=>"Firefighter",
            "F10"=>"Fitter",
            "D28"=>"Fork Lift Truck Driver",
            "D10"=>"Graphic Designer",
            "H01"=>"Hairdresser",
            "D29"=>"HGV Driver",
            "678"=>"Hospital Doctor",
            "163"=>"Househusband",
            "H09"=>"Housewife",
            "787"=>"Insurance Consultant",
            "870"=>"IT Consultant",
            "871"=>"IT Manager",
            "J03"=>"Joiner",
            "L01"=>"Laboratory Technician",
            "L02"=>"Labourer",
            "L06"=>"Lecturer",
            "383"=>"Local Government Officer",
            "189"=>"Lorry Driver",
            "M01"=>"Machine Operator",
            "M02"=>"Machinist",
            "832"=>"Maintenance Engineer",
            "M05"=>"Management Consultant",
            "M06"=>"Manager",
            "194"=>"Managing Director",
            "836"=>"Manufacturing Technician",
            "199"=>"Marketing Manager",
            "558"=>"Mature Student",
            "E16"=>"Mechanical Engineer",
            "215"=>"Motor Mechanic",
            "N02"=>"Nurse",
            "N10"=>"Nursery Nurse",
            "882"=>"Office Administrator",
            "225"=>"Office Manager",
            "226"=>"Operations Manager",
            "P03"=>"Painter And Decorator",
            "P08"=>"Personal Assistant",
            "P19"=>"Plumber",
            "P20"=>"Police Officer",
            "245"=>"Postman",
            "P25"=>"Printer",
            "P26"=>"Prison Officer",
            "949"=>"Production Hand",
            "254"=>"Project Manager",
            "259"=>"Proprietor",
            "R04"=>"Receptionist",
            "R13"=>"Recruitment Consultant",
            "487"=>"Research Scientist",
            "R09"=>"Retired",
            "455"=>"Sales Administrator",
            "S04"=>"Sales Assistant",
            "281"=>"Sales Executive",
            "282"=>"Sales Manager",
            "284"=>"Salesman",
            "S09"=>"Secretary",
            "459"=>"Secretary And PA",
            "S10"=>"Security Guard",
            "S11"=>"Security Officer",
            "S14"=>"Shop Assistant",
            "305"=>"Shop Keeper",
            "306"=>"Shop Manager",
            "S19"=>"Social Worker",
            "463"=>"Software Engineer",
            "313"=>"Soldier",
            "S20"=>"Solicitor",
            "489"=>"Staff Nurse",
            "328"=>"Storeman",
            "S34"=>"Student",
            "S42"=>"Supervisor",
            "332"=>"Systems Analyst",
            "D34"=>"Taxi Driver",
            "T03"=>"Teacher",
            "338"=>"Telecommunications Engineer",
            "S02"=>"Telesales Person",
            "U03"=>"Unemployed",
            "D46"=>"Van Driver",
            "362"=>"Warehouseman",
            "W04"=>"Welder"
            ),

         "business_list" => array(
            "001"=>"Accountancy",
            "002"=>"Advertising",
            "798"=>"Aerospace Industry",
            "203"=>"Airline",
            "009"=>"Architecture",
            "224"=>"Bakery",
            "015"=>"Banking",
            "246"=>"Builder",
            "021"=>"Building Trade",
            "248"=>"Business Consultancy",
            "249"=>"Business Training",
            "260"=>"Car Sales",
            "266"=>"Carpentry",
            "025"=>"Catering - Unlicensed",
            "026"=>"Charity",
            "810"=>"Chemical Industry",
            "817"=>"Chemical Manufacturer",
            "854"=>"Child Minding",
            "281"=>"Civil Engineering",
            "029"=>"Civil Service",
            "282"=>"Cleaning Services",
            "293"=>"Communications",
            "812"=>"Computer Sales",
            "295"=>"Computer Services",
            "181"=>"Computers",
            "035"=>"Computers - Hardware",
            "036"=>"Computers - Software",
            "160"=>"Construction Industry",
            "298"=>"Consulting Engineering",
            "313"=>"Delivery Service",
            "042"=>"Dentistry",
            "833"=>"Design Consultancy",
            "813"=>"Distribution",
            "172"=>"Double Glazing",
            "323"=>"Education",
            "047"=>"Education - Private",
            "138"=>"Education - State",
            "326"=>"Electrical Contractors",
            "048"=>"Electricity Industry",
            "049"=>"Electronics",
            "050"=>"Emergency Services",
            "328"=>"Employment Agency",
            "139"=>"Engineering",
            "330"=>"Engineering Consultants",
            "052"=>"Estate Agency",
            "054"=>"Farming",
            "841"=>"Fast Food",
            "349"=>"Finance Company",
            "056"=>"Financial Services",
            "361"=>"Food Manufacturer",
            "814"=>"Food Production",
            "058"=>"Garage",
            "380"=>"Gardening",
            "059"=>"Gas Industry",
            "387"=>"Government - UK",
            "895"=>"Graphic Design",
            "066"=>"Hairdressing",
            "401"=>"Hardware Retailer",
            "174"=>"Haulage Contractors",
            "402"=>"Health Authority",
            "068"=>"Health Care - NHS",
            "069"=>"Health Care - Private",
            "408"=>"HM Forces",
            "413"=>"Hospital",
            "072"=>"Hotel - Licensed",
            "415"=>"Housing Association",
            "904"=>"Information Technology",
            "077"=>"Insurance",
            "431"=>"Insurance Broker",
            "432"=>"Insurance Company",
            "078"=>"Investment",
            "439"=>"Joinery",
            "079"=>"Law and Order",
            "453"=>"Lawyers",
            "175"=>"Leisure Centre",
            "063"=>"Local Government",
            "464"=>"Local Government Authority",
            "473"=>"Maintenance Services",
            "474"=>"Management Consultancy",
            "084"=>"Manufacturing",
            "480"=>"Marketing",
            "845"=>"Motor Factor/Parts",
            "142"=>"Motor Manufacture",
            "090"=>"Motor Trade",
            "186"=>"Not In Employment",
            "095"=>"Nursery",
            "503"=>"Nursing Home",
            "508"=>"Oil Company",
            "098"=>"Painter And Decorator",
            "528"=>"Pharmaceutical Supplier",
            "805"=>"Plastics Manufacture",
            "103"=>"Plumbing",
            "104"=>"Post Office",
            "176"=>"Printer",
            "560"=>"Printing",
            "105"=>"Prison Service",
            "578"=>"Public House",
            "894"=>"Public Transport",
            "108"=>"Publishing",
            "114"=>"Railway",
            "595"=>"Recruitment Agency",
            "602"=>"Residential Home",
            "117"=>"Retailing",
            "605"=>"Road Haulage",
            "609"=>"Roofing Services",
            "632"=>"School",
            "851"=>"Scientific Research",
            "120"=>"Security Services",
            "124"=>"Social Services",
            "178"=>"Solicitors",
            "127"=>"Steel Industry",
            "179"=>"Supermarket",
            "703"=>"Taxi Service",
            "708"=>"Technical College",
            "130"=>"Telecommunications",
            "717"=>"Textile Manufacturer",
            "132"=>"Transport - Road",
            "145"=>"Travel 1And Tourism",
            "749"=>"University",
            "136"=>"Water Industry",
            "777"=>"Welding",
            "137"=>"Wholesaler",
            ),
      );

      /* end of driver details */


      $fileName = $this->fileName;

      $logID          = $Session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];
      $emailAddress   = $Session["_INSURED_"]["0"]["email_address"];
      $quoteUserID    = $Session["_QZ_QUOTE_DETAILS_"]["quote_user_id"];

      if (!$resEmailStoreDetails = $this->objEmailStore->GetEmailStoreByEmailAndQuoteUserId($emailAddress,$quoteUserID))
         print $this->objEmailStore->GetError();

      if (!$link = $this->objEmailStoreCodes->GenerateLink($resEmailStoreDetails['id'],$resEmailStoreDetails['email'],$this->quoteTypeID))
         print $this->objEmailStoreCodes->GetError();

      $cheappestSiteId          = "0";
      $cheapeastQuotePrice      = "0";
      
      $cheappestSiteId2         = "0";
      $cheapeastQuotePrice2     = "0";
      
      $cheappestSiteId3         = "0";
      $cheapeastQuotePrice3     = "0";
      
      $cheappestQuoteArray      = array();
      $cheappestQuoteArray2     = array();
      $cheappestQuoteArray3     = array();
      
      $cheappestSiteQuoteArray = array();
      
      $cheappestSiteQuoteArray = $this->objQuoteDetails->GetCheapestTopFiveSitesQuoteDetails($logID, 3);
      
      $cheappestSiteId         = $cheappestSiteQuoteArray[1]; 
      $cheappestQuoteArray     = $this->objQuoteDetails->GetCheapestSiteQuoteDetails($logID,$cheappestSiteId);
      $cheapeastQuotePrice     = $cheappestQuoteArray["cheapest_quote"];

      $cheappestSiteId2         = $cheappestSiteQuoteArray[2];      
      $cheappestQuoteArray2     = $this->objQuoteDetails->GetCheapestSiteQuoteDetails($logID,$cheappestSiteId2);
      $cheapeastQuotePrice2     = $cheappestQuoteArray2["cheapest_quote"];
      
      $cheappestSiteId3         = $cheappestSiteQuoteArray[3];      
      $cheappestQuoteArray3     = $this->objQuoteDetails->GetCheapestSiteQuoteDetails($logID,$cheappestSiteId3);
      $cheapeastQuotePrice3     = $cheappestQuoteArray3["cheapest_quote"];
      
      $PostArray = array();

      // new fields - ticket ZTH-983371
      $PostArray["emv_tag"]     = "808000699E0E6196";
      $PostArray["emv_ref"]     = "EdX7CqkmjHl28SA9MKJPUB7fKER6HK3F8zGoe9wwWcnfK4M";

      $PostArray["EMAIL_FIELD"] = $Session["_INSURED_"]["0"]["email_address"];

      $date = date("Y-m-d");

      if(! $emStoreID = $this->objEmailStore->GetEmailStoreIDByEmailAndQuoteUserId($PostArray["EMAIL_FIELD"],$quoteUserID))
      {
         print "CANNOT_GET_THE_EMAIL_STORE_ID \n\n";
      }

      while (1)
      {
         $authInfo = $this->GenerateMimeBoundary();
         $authInfo = str_replace("----=_NextPart_","",$authInfo);
         $authInfo = substr($authInfo, 0, 16);

         if(! $this->objEmailReminder->GetEmRemByAuthInfo($authInfo, $array))
            break;

      }

      if(! $this->objEmailReminder->AddEmailRem($quoteUserID,$emStoreID,"1",$date,"","1", $authInfo,'4','1','0'))
      {
         print $this->objEmailReminder->GetError();
      }

      $PostArray["RENEW_LINK_FIELD"] = $link;
      $PostArray["emv_bounceback"]  = "1";
      $PostArray["emv_pageok"]      = "http://www.quotezone.co.uk/emailpostsuccesshome.htm";
      $PostArray["emv_pageerror"]   = "http://www.quotezone.co.uk/emailpostfailurehome.htm";
      $PostArray["emv_webformtype"] = "0";
      $PostArray["emv_clientid"]    = "35812";

      //$PostArray["emv_campaignid"]        = "2094558";
      $campaignID = "5934111";  // set by ticket FJG-809944
      
      $wlUserID = $Session['_QZ_QUOTE_DETAILS_']['wlUserID'];

      //moneyandme wl id
      // 258
      if($wlUserID)
      {
         switch($wlUserID)
         {
            case '258':
               $campaignID = "6677997";
            break;

            default:
               $campaignID = "5934111";
           break;
         }
      }
      
      $PostArray["emv_campaignid"]        = $campaignID;
   
      $PostArray["TITLE_FIELD"]           = $Session["_INSURED_"]["0"]["title"];
      $PostArray["FIRSTNAME_FIELD"]       = $Session["_INSURED_"]["0"]["first_name"];
      $PostArray["LASTNAME_FIELD"]        = $Session["_INSURED_"]["0"]["surname"];
      $PostArray["DATEOFBIRTH_FIELD"]     = $Session["_INSURED_"]["0"]["date_of_birth_mm"]."/".$Session["_INSURED_"]["0"]["date_of_birth_dd"]."/".$Session["_INSURED_"]["0"]["date_of_birth_yyyy"];
   
      $PostArray["VEHICLE_MAKE_FIELD"]         = "";
      $PostArray["VEHICLE_MODEL_FIELD"]        = "";
      $PostArray["YEAR_OF_MANUFACTURE_FIELD"]  = "";
      $PostArray["ENGINE_SIZE_AND_TYPE_FIELD"] = "";
      $PostArray["ABI_DESCRIPTION_FIELD"]      = "";
   
      $maritalStatus                             = $Session["_INSURED_"]["0"]["marital_status"];
      $PostArray["MARITAL_STATUS_FIELD"]         = $_PROPOSER["marital_status"][$maritalStatus];
      $PostArray["NUMBER_OF_CHILDREN_FIELD"]     = "";
      $PostArray["POSTCODE_FIELD"]               = $Session["_INSURED_"]["0"]["postcode_prefix"]." ".$Session["_INSURED_"]["0"]["postcode_number"];
      $PostArray["HOUSE_NUMBER_FIELD"]           = $Session["_INSURED_"]["0"]["house_number_or_name"];
      $PostArray["STREET_FIELD"]                 = $Session["_INSURED_"]["0"]["address_line2"];
      $PostArray["TOWN_CITY_FIELD"]              = $Session["_INSURED_"]["0"]["address_line3"];
      $PostArray["COUNTY_FIELD"]                 = $Session["_INSURED_"]["0"]["address_line4"];
      $PostArray["HOMEOWNER_FIELD"]              = "";
   
      $PostArray["INSURANCE_START_DATE_FIELD"]   = $Session["_INSURED_"]["0"]["date_of_insurance_start_mm"]."/".$Session["_INSURED_"]["0"]["date_of_insurance_start_dd"]."/".$Session["_INSURED_"]["0"]["date_of_insurance_start_yyyy"];
      $PostArray["QUOTE_DATE_FIELD"]             = date('m')."/".date('d')."/".date('Y');
   
      $PostArray["USE_OF_OTHER_VEHICLES_FIELD"]  = "";
      $PostArray["DRIVERS_TO_BE_INSURED_FIELD"]  = "";
   
      $employmentStatus                          = $Session["_INSURED_"]["0"]["employment_status"];
      $PostArray["EMPLOYMENT_STATUS_FIELD"]      = $_PROPOSER["employment_status"][$employmentStatus];

      $this->objOccupation->GetOccupation($Session["_INSURED_"]["0"]["occupation_list"],&$ftOccArrayResult);
      $this->objBusiness->GetBusiness($Session["_INSURED_"]["0"]["business_list"], &$ftBusArrayResult);

      $this->objOccupation->GetOccupation($Session["_INSURED_"]["0"]["occupation_list_pt"],&$ptOccArrayResult);
      $this->objBusiness->GetBusiness($Session["_INSURED_"]["0"]["business_list_pt"], &$ptBusArrayResult);

      $PostArray["FULL_TIME_OCCUPATION_FIELD"]    = $ftOccArrayResult["name"];
      $PostArray["FULL_TIME_BUSINESS_FIELD"]      = $ftBusArrayResult["name"];

      $PostArray["PART_TIME_OCCUPATION_FIELD"]    = $ptOccArrayResult["name"];
      $PostArray["PART_TIME_BUSINESS_FIELD"]      = $ptBusArrayResult["name"];

      $PostArray["DRIVING_LICENCE_TYPE_FIELD"]     = "";
      $PostArray["LICENCE_DATE_FIELD"]             = "";
      $PostArray["NUMBER_OF_CLAIMS_FIELD"]         = "";
      $PostArray["NUMBER_OF_CONVICTIONS_FIELD"]    = "";
      $PostArray["COVER_TYPE_FIELD"]               = "";
      $PostArray["TYPE_OF_USE_FIELD"]              = "";
      //$PostArray["PASS_PLUS_FIELD"]                = "";
      $PostArray["ANNUAL_MILEAGE_FIELD"]           = "";
      $PostArray["ANNUAL_BUSINESS_MILEAGE_FIELD"]  = "";
      $PostArray["VOLUNTARY_EXCESS_FIELD"]         = "";
      $PostArray["YEAR_NCB_FIELD"]                 = "";
      $PostArray["MAIN_USER_FIELD"]                = "";
      $PostArray["OWNER_FIELD"]                    = "";
      $PostArray["REGISTERED_KEEPER_FIELD"]        = "";
      $PostArray["NUMBER_OF_OTHER_VEHICLES_FIELD"] = "";

      $sid = str_replace("_","",$fileName);

      $PostArray["QUOTE_TYPE_FIELD"]               = "home";
      $PostArray["QUOTE_RESULTS_LINK_FIELD"]       = "https://home-insurance.quotezone.co.uk/home/index.php?sid=$sid";
      $PostArray["EMVADMIN1_FIELD"]                = $sid;

      // MoneyMaxim (ticket - XEV-793421)
      if($wlUserID == 136)
      {
         $PostArray["emv_campaignid"]              = "7887142";
         $PostArray["QUOTE_TYPE_FIELD"]            = "moneymaxim home";

         $PostArray["emv_ref"]                     = "EdX7CqkmjGiJ8SA9MKJPUB7TKEx6H97KiDDaeKE-KsjdK_g";
         $PostArray["emv_tag"]                     = "40003C9B87388B84";
      }

      if($wlUserID == 409)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "neil gee home";
         $PostArray["QUOTE_RESULTS_LINK_FIELD"]       = "https://wl3.quotezone.co.uk/home/index.php?sid=$sid";
      }

      //payingtoomuch
      if ($wlUserID == 496)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "PayingTooMuch_home";
         $PostArray["emv_tag"]          = "423CD81198080003";
         $PostArray["emv_ref"]          = "EdX7CqkmjK9k8SA9MKJPUB7TKk8JaKXC-zHVe6E2WMDaKE0";
      }

      //nhscashback 
      if ($wlUserID == 584)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "nhsCashback_home";
         $PostArray["emv_tag"]          = "8CC0400028D040B5";
         $PostArray["emv_ref"]          = "EdX7CqkmjJ_p8SA9MKJPUB7fWz96GK3D-jrVD6kyWLLcK-0"; 
      }

      //mirrorcashback 
      if ($wlUserID == 586)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "mirrorCashback_home";
         $PostArray["emv_tag"]          = "4CC040001ACFBFF5";
         $PostArray["emv_ref"]          = "EdX7CqkmjJ_s8SA9MKJPUB7TWz96GK3D-jmsCN9ELrbcK68"; 
      }

      //froggybank 
      if ($wlUserID == 580)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "froggyBank_home";
         $PostArray["emv_tag"]          = "A6F11ED533010000";
         $PostArray["emv_ref"]          = "EdX7CqkmjJxx8SA9MKJPUB6mLjp7Hdi3_zvee6g2WMDZKH0"; 
      }

      //giveortake 
      if ($wlUserID == 587)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "giveOrtake_home";
         $PostArray["emv_tag"]          = "5925FDEA99808592";
         $PostArray["emv_ref"]          = "EdX7CqkmjJwA8SA9MKJPUB7SIU5_atm2izHUc6k-XcnbKPk"; 
      }

      //psdiscount 
      if ($wlUserID == 585)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "psDiscount_home";
         $PostArray["emv_tag"]          = "1358CC040003A304";
         $PostArray["emv_ref"]          = "EdX7CqkmjJ_m8SA9MKJPUB7WK0lyb97D_jjde6pHW8DdK98"; 
      }

      //vacmedia 
      if ($wlUserID == 588)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "vacMedia_home";
         $PostArray["emv_tag"]          = "9433D633010000D3";
         $PostArray["emv_ref"]          = "EdX7CqkmjJ_38SA9MKJPUB7eLE95aKvA-Tjce6k2WLTaK9s"; 
      }
      
      //topcashback 
      if ($wlUserID == 604)
      {
         $cheapestSiteID  = $cheappestQuoteArray['site_id'];
         $cheapestPremium = $cheappestQuoteArray['cheapest_quote'];
         $cashbackAmount  = CalculateCashBack($cheapestSiteID,$cheapestPremium,"","",$Session['CPA'][$cheapestSiteID],$wlUserID);
         
         $cheapestSiteID2  = $cheappestQuoteArray2['site_id'];
         $cheapestPremium2 = $cheappestQuoteArray2['cheapest_quote'];
         $cashbackAmount2  = CalculateCashBack($cheapestSiteID2,$cheapestPremium2,"","",$Session['CPA'][$cheapestSiteID2],$wlUserID);
         
         $cheapestSiteID3  = $cheappestQuoteArray3['site_id'];
         $cheapestPremium3 = $cheappestQuoteArray3['cheapest_quote'];
         $cashbackAmount3  = CalculateCashBack($cheapestSiteID3,$cheapestPremium3,"","",$Session['CPA'][$cheapestSiteID3],$wlUserID);         

         $memberID                           = trim($Session["_QZ_QUOTE_DETAILS_"]["memberID"]);
         $PostArray["EMVADMIN3_FIELD"]       = $memberID;
         $PostArray["QUOTE_TYPE_FIELD"]      = "topCashBack_home";
         $PostArray["emv_tag"]               = "44CC0400009BB583";
         $PostArray["emv_ref"]               = "EdX7CqkmjJyg8SA9MKJPUB7TLD8JHKnD-jjdcttEXcjaK6E";
                  
         $PostArray["CASHBACK_AMOUNT_FIELD"] = $cashbackAmount;
         $PostArray["CASHBACK_TOTAL_FIELD"]  = $cheapestPremium-$cashbackAmount;
         
         $PostArray["CASHBACK_AMOUNT2_FIELD"] = $cashbackAmount2;
         $PostArray["CASHBACK_TOTAL2_FIELD"]  = $cheapestPremium2-$cashbackAmount2;
         
         $PostArray["CASHBACK_AMOUNT3_FIELD"] = $cashbackAmount3;
         $PostArray["CASHBACK_TOTAL3_FIELD"]  = $cheapestPremium3-$cashbackAmount3;
      }

      //quidco 
      if ($wlUserID == 336)
      {  
         $cheapestSiteID  = $cheappestQuoteArray['site_id'];
         $cheapestPremium = $cheappestQuoteArray['cheapest_quote'];
         $cashbackAmount  = CalculateCashBackQuidco($cheapestSiteID,$cheapestPremium,$Session);

         $cheapestSiteID2  = $cheappestQuoteArray2['site_id'];
         $cheapestPremium2 = $cheappestQuoteArray2['cheapest_quote'];
         $cashbackAmount2  = CalculateCashBackQuidco($cheapestSiteID2,$cheapestPremium2,$Session);
         
         $cheapestSiteID3  = $cheappestQuoteArray3['site_id'];
         $cheapestPremium3 = $cheappestQuoteArray3['cheapest_quote'];
         $cashbackAmount3  = CalculateCashBackQuidco($cheapestSiteID3,$cheapestPremium3,$Session);
         
         $PostArray["QUOTE_TYPE_FIELD"]      = "Quidco_Home";
         $PostArray["emv_tag"]               = "3390100003CC3DCD";
         $PostArray["emv_ref"]               = "EdX7CqkmjMyh8SA9MKJPUB7UK0V6Ha3D-jjeCNo1LLOtKAo";
         
         $PostArray["CASHBACK_AMOUNT_FIELD"] = $cashbackAmount;
         $PostArray["CASHBACK_TOTAL_FIELD"]  = $cheapestPremium-$cashbackAmount;
         
         $PostArray["CASHBACK_AMOUNT2_FIELD"] = $cashbackAmount2;
         $PostArray["CASHBACK_TOTAL2_FIELD"]  = $cheapestPremium2-$cashbackAmount2;
         
         $PostArray["CASHBACK_AMOUNT3_FIELD"] = $cashbackAmount3;
         $PostArray["CASHBACK_TOTAL3_FIELD"]  = $cheapestPremium3-$cashbackAmount3;
      }
      
      //leedscompare 
      if ($wlUserID == 608)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "lufc_home";
         $PostArray["emv_tag"]          = "602000122DD09FC6";
         $PostArray["emv_ref"]          = "EdX7CqkmjIFD8SA9MKJPUB7RKE56HK3C-DqpD6k_LrPfKAM"; 
      }


	//electric shopping 
      if ($wlUserID == 610)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "electricshopping_home";
         $PostArray["emv_tag"]          = "B24634010000FC23";
         $PostArray["emv_ref"]          = "EdX7CqkmjITe8SA9MKJPUB6lKkh8H6nD-zjde6lAK8LaK_I"; 
      }


	//andybiggsiar 
      if ($wlUserID == 633)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "carinsurancecomparison_home";
         $PostArray["emv_tag"]          = "5E4C15268020001C";
         $PostArray["emv_ref"]          = "EdX7CqkmjImH8SA9MKJPUB7SXUgJHajB_DDdeak2WMGqKE8"; 
      }


	//quoteyquotey 
      if ($wlUserID == 631)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "quoteyquotey_home";
         $PostArray["emv_tag"]          = "D69A00800077CE64";
         $PostArray["emv_ref"]          = "EdX7CqkmjImT8SA9MKJPUB6jLkULHK3L-jjdfK5FLcbdKHw"; 
      }


      //msmithfreelance 
      if ($wlUserID == 632)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "cheapcarinsurance_home";
         $PostArray["emv_tag"]          = "48B45AA8340148B4";
         $PostArray["emv_ref"]          = "EdX7CqkmjIn38SA9MKJPUB7TID5-Gdyy8jvZe6gyULLdK4E"; 
      }
	

      //msmithfreelance2 
      if ($wlUserID == 634)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "cheapvaninsurance_home";
         $PostArray["emv_tag"]          = "DF14E1803501DF14";
         $PostArray["emv_ref"]          = "EdX7CqkmjPCN8SA9MKJPUB6jXk1-aazL-jvYe6hCLsHdKGQ"; 
      }
	
      //cheap 
      if ($wlUserID == 644)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "cheapcouk_home";
         $PostArray["emv_tag"]          = "3B1926A02000100D";
         $PostArray["emv_ref"]          = "EdX7CqkmjPRC8SA9MKJPUB7UWk1zHquy-jrde6k3WMCtKAs"; 
      }

      //moneymedia 
      if ($wlUserID == 601)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "moneymedia_home";
         $PostArray["emv_tag"]          = "1FF6B47B4D404001";
         $PostArray["emv_ref"]          = "EdX7CqkmjPaU8SA9MKJPUB7WXjp8bqnEiDypf6kyWMDYKAc"; 
      }




      //bestbuymoney 
      if ($wlUserID == 640)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "bestbuymoney_home";
         $PostArray["emv_tag"]          = "38D40400027A66BF";
         $PostArray["emv_ref"]          = "EdX7CqkmjPQQ8SA9MKJPUB7UIDh-HKnD-jjffNgwXrKvKPM"; 
      }



	      //cpcomparisons
      if ($wlUserID == 652)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "cpcomparisons_home";
         $PostArray["emv_tag"]          = "F81169766C02F811";
         $PostArray["emv_ref"]          = "EdX7CqkmjOO48SA9MKJPUB6hIE17GqTE_D6ue6tAUMHYKGw"; 
      }




      //uswitch
      if ($wlUserID == 658)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "uswitch_home";
         $PostArray["emv_tag"]          = "8000359DE2841B88";
         $PostArray["emv_ref"]          = "EdX7CqkmjOl58SA9MKJPUB7fKEx6H6jKjk3fc603KsjRKCs"; 
      }

      //savoo
      if ($wlUserID == 661)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "savoo_home";
         $PostArray["emv_tag"]          = "4ED2262A9B8084ED";
         $PostArray["emv_ref"]          = "EdX7CqkmjO1V8SA9MKJPUB7TXTh4HqvBizGvc6k-XLWtKPQ"; 
      }

      //moneyhelpline
      if ($wlUserID == 657)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "moneyhelpline_home";
         $PostArray["emv_tag"]          = "698E10DC0400014A";
         $PostArray["emv_ref"]          = "EdX7CqkmjO908SA9MKJPUB7RIUQPHa23iTjZe6k2WcSoKCU"; 
      }

      //maximiles
      if ($wlUserID == 666)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "maximiles_home";
         $PostArray["emv_tag"]          = "80000BD0D8079C00";
         $PostArray["emv_ref"]          = "EdX7CqkmjNe48SA9MKJPUB7fKEx6HN-3-kzVe64_K8DZKGI"; 
      }

      //richerair
      if ($wlUserID == 689)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "richerair_home";
         $PostArray["emv_tag"]          = "40400007D064E84E";
         $PostArray["emv_ref"]          = "EdX7CqkmjMoI8SA9MKJPUB7TKEh6HK3D_Uzdfa1DUMSsKIc"; 
      }

      //soswitch
      if ($wlUserID == 690)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "soswitch_home";
         $PostArray["emv_tag"]          = "59E20DBF380159E2";
         $PostArray["emv_ref"]          = "EdX7CqkmjMDl8SA9MKJPUB7SITl4HNmxjDvVe6gzUbXbKF8"; 
      }

      //comparecomiar
      if ($wlUserID == 681)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "comparecomiar_home";
         $PostArray["emv_tag"]          = "60E00400021B24B7";
         $PostArray["emv_ref"]          = "EdX7CqkmjMBs8SA9MKJPUB7RKDl6HKnD-jjfets0XLLeKAU"; 
      }
    
      //fairinvestmentWL
      if($wlUserID == 692)
      {
        $PostArray["QUOTE_TYPE_FIELD"] = "fairinvestment_home";
        $PostArray["emv_tag"]          = "228DA720200005CD";
        $PostArray["emv_ref"]          = "EdX7CqkmjMmh8SA9MKJPUB7VKkQObarB-jrde6k2XbOtKH4";

      }
      //aspectwebmedia
      if ($wlUserID == 695)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "aspectwebmedia_home";
         $PostArray["emv_tag"]          = "40003DF77D484E84";
         $PostArray["emv_ref"]          = "EdX7CqkmjzBf8SA9MKJPUB7TKEx6H9m1_T-pf6EyLcjdK_Y";
      }


      $unsubscribeLink = $this->CreateUnsubscribeLinkParams($Session["_INSURED_"]["0"]["email_address"],'top3','home')."&sid=$sid&type=1&action=unsubscribe&campaignId=".$PostArray["emv_campaignid"];
      //$PostArray["UNSUBSCRIBE_LINK_FIELD"]         = "http://home-insurance.quotezone.co.uk/customer/unsubscribe.php?$unsubscribeLink";
      $PostArray["UNSUBSCRIBE_LINK_FIELD"]         = "https://www.quotezone.co.uk/customer/unsubscribe.php?$unsubscribeLink";

      if($wlUserID)
	$PostArray["UNSUBSCRIBE_LINK_FIELD"]         = "https://www.quotezone.co.uk/customer/wl-unsubscribe.php?emailaddress=".$Session["_INSURED_"]["0"]["email_address"];

      //$PostArray["iam_member"]           = "";
      $PostArray["IAM_MEMBER_FIELD"]     = "";
      $PostArray["QUOTE_TIME_FIELD"]     = date("Gis");
      $PostArray["MODIFIED_FIELD"]       = "";
      $PostArray["MODIFIED_FIELD"]       = "";
      $PostArray["IMPORT_FIELD"]         = "";
      $PostArray["IMPORT_FIELD"]         = "";
      $PostArray["LEFT_RIGHT_FIELD"]     = "";
      $PostArray["LEFT_RIGHT_FIELD"]     = "";
      $PostArray["KEPT_OVERNIGHT_FIELD"] = "";
      $PostArray["ALARM_FIELD"]          = "";
      $PostArray["ALARM_FIELD"]          = "";
      $PostArray["IMMOBILISER_FIELD"]    = "";
      $PostArray["IMMOBILISER_FIELD"]    = "";
      $PostArray["TRACKER_FIELD"]        = "";
      $PostArray["TRACKER_FIELD"]        = "";
      $PostArray["PURCHASED_FIELD"]      = "";
      $PostArray["PURCHASED_FIELD"]      = "";

      $PostArray["PURCHASE_DATE_FIELD"]       = "";
      $PostArray["ESTIMATED_VALUE_FIELD"]     = "";
      $PostArray["REGISTRATION_NUMBER_FIELD"] = "";

      $PostArray["PROTECTED_NCB_FIELD"]       = "";
      $PostArray["PROTECTED_NCB_FIELD"]       = "";
      $PostArray["MOBILE_NUMBER_FIELD"]       = $Session["_INSURED_"][0]["mobile_telephone_prefix"]."".$Session["_INSURED_"][0]["mobile_telephone_number"];

      if(! $cheapeastQuotePrice)
         $cheapeastQuotePrice = "0";

      if($cheapeastQuotePrice == "0")
      {
         $PostArray["emv_tag"]          = "800052848AF01D80";
         $PostArray["emv_ref"]          = "EdX7CqkmjzyW8SA9MKJPUB7fKEx6Ga_L_jCsDak3LMjZKz4";
      }


      $PostArray["PREMIUM_FIELD"]             = $cheapeastQuotePrice;      

      if($Session["_INSURED_"][0]["update_news"] == "on")
         $PostArray["OPT_IN_FIELD"] = "Yes";
      else
         $PostArray["OPT_IN_FIELD"] = "No";

      $PostArray["QUOTE_COMPLETED_FIELD"] = "Yes";
      $PostArray["QUOTE_REF_FIELD"]       = $Session["_QZ_QUOTE_DETAILS_"]["quote_reference"];































 // NEW FIELDS

      // set defaults
      // company name for top quote, this will be used to retrieve an image, so lower case and no spaces please
      $PostArray["COMPANY_FIELD"]          = '';

      // company name for 2nd quote, same as above
      $PostArray["COMPANY_2_FIELD"]        = '';

      // company name for 3rd quote, same as above
      $PostArray["COMPANY_3_FIELD"]        = '';

      // Annual premium of 2nd quote
      $PostArray["PREMIUM_2_FIELD"]        = '';

      // Annual premium of 3rd quote
      $PostArray["PREMIUM_3_FIELD"]        = '';

      // if top quote has "courtesy car" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_A_FIELD"]   = '';

      // if 2nd quote has "courtesy car" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_A_2_FIELD"] = '';

      // if 3rd quote has "courtesy car" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_A_3_FIELD"] = '';

      // if top quote has "windscreen cover" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_B_FIELD"]   = '';

      // if 2nd quote has "windscreen cover" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_B_2_FIELD"] = '';

      // if 3rd quote has "windscreen cover" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_B_3_FIELD"] = '';

      // if top quote has "personal accident" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_C_FIELD"]   = '';

      // if 2nd quote has "personal accident" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_C_2_FIELD"] = '';

      // if 3rd quote has "personal accident" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_C_3_FIELD"] = '';

      // buyOnline link for top quote
      $PostArray["QUOTE_DETAIL_D_FIELD"]   = '';

      // buyOnline link for 2nd quote
      $PostArray["QUOTE_DETAIL_D_2_FIELD"] = '';

      // buyOnline link for 3rd quote
      $PostArray["QUOTE_DETAIL_D_3_FIELD"] = '';


      $topQuoteArray = array();

      // get top quotes
      if( $topQuoteArray = $this->objQuoteDetails->GetCheapestTopFiveSitesQuoteDetails($logID, 3))
      {
         $_SESSION                = $Session;
         $cheappestSiteQuoteArray = array();

         for($k=1;$k<=3;$k++)
         {
            $topPremium          = '';
            $topSiteDetailsArray = array();
            $_resSite            = array();
            $legal            = '';
            $building          = '';
            $content            = '';

            $topSiteID           = $topQuoteArray[$k];
            $topSiteDetailsArray = $this->objQuoteDetails->GetCheapestSiteQuoteDetails($logID,$topSiteID);

            if($k == 1)
            {


		

               // get company name
               $siteFileName = $Session["SHOW_SITES"][$topSiteID];
               include_once "/home/www/quotezone.co.uk/insurance-new/home/sites/showsites/$siteFileName";
               // RAMDRIVE
               //include_once "../home/sites/showsites/$siteFileName";

               //$PostArray["COMPANY_FIELD"] = strtolower(str_replace(" ","",$_resSite['brokerFullName']));
               $PostArray["COMPANY_FIELD"] = "https://home-insurance.quotezone.co.uk/home/".$_resSite['imageSiteRemote'];
		
               if(preg_match("/not_included.gif/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $legal = "no";	
               // if top quote has "legal assistance" included - "yes","no" or "extra"
               elseif(preg_match("/included.gif/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $legal = "yes";

               elseif(preg_match("/plus.gif/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $legal = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $legal = "check";

               $PostArray["QUOTE_DETAIL_A_FIELD"] = $legal;

               // if top quote has "legal assistance" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_BUILDING_A/DAMAGE_']))
                  $building          = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_BUILDING_A/DAMAGE_']))
                  $building          = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_BUILDING_A/DAMAGE_']))
                  $building          = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_BUILDING_A/DAMAGE_']))
                  $building          = "check";

               $PostArray["QUOTE_DETAIL_B_FIELD"] = $building;



               // if top quote has "legal assistance" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_CONTENTS_A/DAMAGE_']))
                  $content            = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_CONTENTS_A/DAMAGE_']))
                  $content            = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_CONTENTS_A/DAMAGE_']))
                  $content            = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_CONTENTS_A/DAMAGE_']))
                  $content            = "check";


               $PostArray["QUOTE_DETAIL_C_FIELD"]   = $content;

               // buy online link
               $buyOnlineLink = '';
               if($_resSite['proceedBuyOnlineLink'])
                  $buyOnlineLink = $_resSite['proceedBuyOnlineLink'];
               elseif($_resSite['_EXTRA_PARAMS_']['quoteBuyOnline']['proceedBuyOnlineLink'][0])
                  $buyOnlineLink = $_resSite['_EXTRA_PARAMS_']['quoteBuyOnline']['proceedBuyOnlineLink'][0];

               if($buyOnlineLink)
               {
                  $PostArray["QUOTE_DETAIL_D_FIELD"]   = str_replace("sites/clickIn.php?","",$buyOnlineLink);

                  if(preg_match("/quote_ref\.value/i",$PostArray["QUOTE_DETAIL_D_FIELD"]))
                  {
                     // extract quote_ref from out file
                     include_once("/home/www/quotezone.co.uk/insurance-new/home/steps/functions.php");

                     // RAMDRIVE
                     //include_once("../home/steps/functions.php");
                     if($openResultFile = OpenResultFile($topSiteID))
                     {
                        $quote_ref = $openResultFile[0]["quote reference"];

                        if($quote_ref)
                           $PostArray["QUOTE_DETAIL_D_FIELD"] = str_replace("quote_ref.value",$quote_ref,$PostArray["QUOTE_DETAIL_D_FIELD"]);
                     }
                  }
               }


            }

            if($k == 2)
            {
               // get company name
               $siteFileName = $Session["SHOW_SITES"][$topSiteID];
               include_once "/home/www/quotezone.co.uk/insurance-new/home/sites/showsites/$siteFileName";

               // RAMDRIVE
               //include_once "../home/sites/showsites/$siteFileName";
 
               // company premium
               $topPremium  = $topSiteDetailsArray["cheapest_quote"];

               //$PostArray["COMPANY_2_FIELD"] = strtolower(str_replace(" ","",$_resSite['brokerFullName']));
               $PostArray["COMPANY_2_FIELD"] = "https://home-insurance.quotezone.co.uk/home/".$_resSite['imageSiteRemote'];

               $PostArray["PREMIUM_2_FIELD"] = $topPremium;


               // if top quote has "legal assistance" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $legal = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $legal = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $legal = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $legal = "check";

               $PostArray["QUOTE_DETAIL_A_2_FIELD"] = $legal;



               // if top quote has "legal assistance" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_BUILDING_A/DAMAGE_']))
                  $building          = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_BUILDING_A/DAMAGE_']))
                  $building          = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_BUILDING_A/DAMAGE_']))
                  $building          = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_BUILDING_A/DAMAGE_']))
                  $building          = "check";


               $PostArray["QUOTE_DETAIL_B_2_FIELD"] = $building;

               // if top quote has "legal assistance" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_CONTENTS_A/DAMAGE_']))
                  $content            = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_CONTENTS_A/DAMAGE_']))
                  $content            = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_CONTENTS_A/DAMAGE_']))
                  $content            = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_CONTENTS_A/DAMAGE_']))
                  $content            = "check";

               $PostArray["QUOTE_DETAIL_C_2_FIELD"] = $content;

               // buy online link
               $buyOnlineLink = '';
               if($_resSite['proceedBuyOnlineLink'])
                  $buyOnlineLink = $_resSite['proceedBuyOnlineLink'];
               elseif($_resSite['_EXTRA_PARAMS_']['quoteBuyOnline']['proceedBuyOnlineLink'][0])
                  $buyOnlineLink = $_resSite['_EXTRA_PARAMS_']['quoteBuyOnline']['proceedBuyOnlineLink'][0];


               if($buyOnlineLink)
               {
                  $PostArray["QUOTE_DETAIL_D_2_FIELD"]   = str_replace("sites/clickIn.php?","",$buyOnlineLink);

                  if(preg_match("/quote_ref\.value/i",$PostArray["QUOTE_DETAIL_D_2_FIELD"]))
                  {
                     // extract quote_ref from out file
                     include_once("/home/www/quotezone.co.uk/insurance-new/home/steps/functions.php");

                     // RAMDRIVE
                     //include_once("../home/steps/functions.php");
                     if($openResultFile = OpenResultFile($topSiteID))
                     {
                        $quote_ref = $openResultFile[0]["quote reference"];

                        if($quote_ref)
                           $PostArray["QUOTE_DETAIL_D_2_FIELD"] = str_replace("quote_ref.value",$quote_ref,$PostArray["QUOTE_DETAIL_D_2_FIELD"]);
                     }
                  }
               }

            }

            if($k == 3)
            {
               // get company name
               $siteFileName = $Session["SHOW_SITES"][$topSiteID];
               include_once "/home/www/quotezone.co.uk/insurance-new/home/sites/showsites/$siteFileName";

               // RAMDRIVE
               //include_once "../home/sites/showsites/$siteFileName";
 
               // company premium
               $topPremium  = $topSiteDetailsArray["cheapest_quote"];

               //$PostArray["COMPANY_3_FIELD"] = strtolower(str_replace(" ","",$_resSite['brokerFullName']));
               $PostArray["COMPANY_3_FIELD"] = "https://home-insurance.quotezone.co.uk/home/".$_resSite['imageSiteRemote'];

               $PostArray["PREMIUM_3_FIELD"] = $topPremium;


               // if top quote has "legal assistance" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $legal = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $legal = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $legal = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $legal = "check";

               $PostArray["QUOTE_DETAIL_A_3_FIELD"] = $legal;

               // if top quote has "legal assistance" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_BUILDING_A/DAMAGE_']))
                  $building          = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_BUILDING_A/DAMAGE_']))
                  $building          = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_BUILDING_A/DAMAGE_']))
                  $building          = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_BUILDING_A/DAMAGE_']))
                  $building          = "check";

               $PostArray["QUOTE_DETAIL_B_3_FIELD"] = $building;

               // if top quote has "legal assistance" included - "yes","no" or "extra"
               if(preg_match("/not_included.gif/i",$_resSite['_CONTENTS_A/DAMAGE_']))
                  $content            = "no";
               elseif(preg_match("/included.gif/i",$_resSite['_CONTENTS_A/DAMAGE_']))
                  $content            = "yes";
               elseif(preg_match("/plus.gif/i",$_resSite['_CONTENTS_A/DAMAGE_']))
                  $content            = "extra";
               elseif(preg_match("/ask.gif/i",$_resSite['_CONTENTS_A/DAMAGE_']))
                  $content            = "check";

               $PostArray["QUOTE_DETAIL_C_3_FIELD"] = $content;

               // buy online link
               $buyOnlineLink = '';
               if($_resSite['proceedBuyOnlineLink'])
                  $buyOnlineLink = $_resSite['proceedBuyOnlineLink'];
               elseif($_resSite['_EXTRA_PARAMS_']['quoteBuyOnline']['proceedBuyOnlineLink'][0])
                  $buyOnlineLink = $_resSite['_EXTRA_PARAMS_']['quoteBuyOnline']['proceedBuyOnlineLink'][0];

               if($buyOnlineLink)
               {
                  $PostArray["QUOTE_DETAIL_D_3_FIELD"]   = str_replace("sites/clickIn.php?","",$buyOnlineLink);

                  if(preg_match("/quote_ref\.value/i",$PostArray["QUOTE_DETAIL_D_3_FIELD"]))
                  {
                     // extract quote_ref from out file
                     include_once("/home/www/quotezone.co.uk/insurance-new/home/steps/functions.php");

                     // RAMDRIVE
                     //include_once("../home/steps/functions.php");
                     if($openResultFile = OpenResultFile($topSiteID))
                     {
                        $quote_ref = $openResultFile[0]["quote reference"];

                        if($quote_ref)
                           $PostArray["QUOTE_DETAIL_D_3_FIELD"] = str_replace("quote_ref.value",$quote_ref,$PostArray["QUOTE_DETAIL_D_3_FIELD"]);
                     }
                  }
               }


            }
         }//for

      }// if get top quotes

      // NEW FIELDS 

      $string = "";

      foreach($PostArray as $key=>$val)
      {
         $string .= $key."=".urlencode($val)."&";
      }

      $string = rtrim($string, "&");

      //if ($wlUserID == 604)
      //{
      //   mail('acrux@ymail.com', 'WL3 EMV HOME DEBUG STRING', $string);
      //}
      
      //$fh = fopen("/home/www/quotezone.co.uk/insurance-new/home/HomeEmailOutbounds.log","a+");
      //fwrite($fh,"String is :  $string  \n");
      //fclose($fh);

      return $string;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: SendUserDetails
   //
   // [DESCRIPTION]:   Form user data details to be sent it
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SendUserDetails()
   {
      $fh = fopen("/home/www/quotezone.co.uk/insurance-new/home/HomeEmailOutbounds.log","a+");

      if(! $Session = $this->LoadSessionFile($this->fileName))
      {
         $this->strERR = GetErrorString('CANT_GET_FILENAME_CONTENT');
         return false;
      }

      $logID       = $Session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];
      $wlUserID    = $Session["_QZ_QUOTE_DETAILS_"]["wlUserID"];
      $emailAddr   = $Session["_INSURED_"][0]["email_address"];
      $quoteUserID = $Session["_QZ_QUOTE_DETAILS_"]["quote_user_id"];

      // DO NOT SEND IF TEST QUOTE
      if(isset($Session["TESTING SITE ID"]))
         return;

      // MoneyMaxim
      //if($wlUserID == "136")
      //   return;

      if(! $this->LogWasSentOk($logID))
      {
         fwrite($fh, "POST ALLREADY SENT [$logID] file [$this->fileName]\n");
         fclose($fh);
         return false;
      }

      //Commented due to ticket id XEV-793421
      /*
      $emailRule = $this->objEmailStore->GetEmailStoreRuleByEmailAndQuoteUserID($emailAddr,$quoteUserID);
      
      if($emailRule != '65535')
      {
         fwrite($fh, "Email address [$emailAddr] has rule [$emailRule] for file [$this->fileName] and log_id [$logID]\n");
         fwrite($fh, "Details wont be posted because the user has unsubscribed\n");
         fclose($fh);
         return false;
      }
      */

//      if(! $this->isNotWaitingQuotes($logID))
//      {
//         fwrite($fh, "QUOTE STILL WAITING: $this->fileName logID=$logID \n");
//         fclose($fh);
//         return false;
//      }

      if(! $this->objEmailOutbounds->AddEmailOutbounds($logID))
      {
         fwrite($fh, "CANNOT ADD  log_id [$logID] to database\n");
         fclose($fh);
         return false;
      }
      
      
      if(! $string = $this->FormatUserDetails($Session))
      {
         fwrite($fh, "FAILURE Formating user details for: $this->fileName logID=$logID \n");
         fclose($fh);
         return false;
      }

      if(! $response = $this->EmailOutboundsEngineSend($this->path, $string))
      {
         fwrite($fh, "FAILURE Send process for : $this->fileName logID=$logID \n");
         fclose($fh);
         return false;
      }

      if(! $this->objEmailOutbounds->SetEmailOutboundsStatus($logID, "1"))
      {
         fwrite($fh, "FAILURE Cannot set the email status : $this->fileName logID=$logID \n");
         fclose($fh);
         return false;
      }

      if(! $this->CheckEmailOutboundResponse($response))
      {
         fwrite($fh, "The post response was an error - please check the details to repost\n");
         fwrite($fh, "response:\n ".$response." \n");
         fclose($fh);
         return false;
      }
      else
      {
         fwrite($fh, "The post response IS SUCCESS - $this->fileName logID=$logID DONE ! \n");
         fclose($fh);
         return true;
      }

   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CheckEmailOutboundResponse
   //
   // [DESCRIPTION]:   checks the response from the server
   //
   // [PARAMETERS]:    $response=""
   //
   // [RETURN VALUE]:  true or false in case of failure
   //
   // [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CheckEmailOutboundResponse($response="")
   {

      if(empty($response))
      {
         $this->strERR = GetErrorString("INVALID_RESPONSE_FROM_EMAILVISION");
            return false;
      }

      if(! preg_match("/emailpostsuccess/isU",$response))
      {
         $this->strERR = GetErrorString("INVALID_RESPONSE_PREGMATCH");
            return false;
      }

      return true;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetError
   //
   // [DESCRIPTION]:   Retrieve the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetError()
   {
       return $this->strERR;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ShowError
   //
   // [DESCRIPTION]:   Print the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function ShowError()
   {
       return $this->strERR;
   }
}



?>

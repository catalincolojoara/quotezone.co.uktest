<?php
/*****************************************************************************/
/*                                                                           */
/*  CSplitConfiguration class interface                                               */
/*                                                                           */
/*  (C) 2007 Istvancsek Gabriel (gabi@acrux.biz)                                   */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CSplitConfiguration
//
// [DESCRIPTION]:  CSplitConfiguration class interface
//
// [FUNCTIONS]:    bool AssertTrakerUrls($trackUrl='',$name='')
//                 int  AddSplitConfiguration($trackUrl='', $name='')
//                 bool UpdateSplitConfiguration($trackUrlID=0,$trackUrl='',$name='')
//                 bool DeleteSplitConfiguration($trackUrlID=0)
//                 bool GetAllSplitConfiguration()
//                 bool GetSplitConfigurationByID($trackUrlID=0)
//                 bool GetSplitConfigurationByUrl($trackUrl="")
//                 bool GetTrackUrlByName($name="")
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CSplitConfiguration
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CSplitConfiguration
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 08-10-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CSplitConfiguration($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertTrackerDetails
//
// [DESCRIPTION]:   Verify to see if the params are completed
//
// [PARAMETERS]:    $logID='',$siteID='', $details=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 08-10-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertSplitConfiguration($logID='',$siteID='', $details='')
{
   $this->strERR = "";

   if(! preg_match("/^\d+$/", $logID))
      $this->strERR = "INVALID_LOG_ID_FIELD";

   if(! preg_match("/^\d+$/", $siteID))
      $this->strERR = "INVALID_SITE_ID_FIELD";

   if(empty($details))
      $this->strERR = "INVALID_TRACK_DETAILS_FIELD";

    if($this->strERR != "")
      return true;

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddSplitConfiguration
//
// [DESCRIPTION]:   Add new entry to the tracker_details table
//
// [PARAMETERS]:    $trackUrl='',$name=''
//
// [RETURN VALUE]:  trUrlID or 0 in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddSplit($splitName)
{
//   if($this->AssertSplitConfiguration($logID,$siteID, $details))
//      return 0;

   $this->lastSQLCMD = "select id from split where name='$splitName'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   if ($this->dbh->GetRows() > 0)
   {
	   if(! $this->dbh->FetchRows())
	   {
	      $this->strERR = $this->dbh->GetError();
	      return 0;
	   }
	
	   $id = $this->dbh->GetFieldValue('id');
	   return $id;
   }

   $this->lastSQLCMD = "INSERT INTO split (name) VALUES ('$splitName')";
//print $this->lastSQLCMD."<br>";
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   //$splitId = $this->dbh->GetFieldValue("id");

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddSplitConfiguration
//
// [DESCRIPTION]:   Add new entry to the tracker_details table
//
// [PARAMETERS]:    $trackUrl='',$name=''
//
// [RETURN VALUE]:  trUrlID or 0 in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddSplitConfiguration($splitId,$destination,$url,$enabled)
{
//   if($this->AssertSplitConfiguration($logID,$siteID, $details))
//      return 0;

   $this->lastSQLCMD = "select * from split_configuration where split_id='$splitId' and destination='$destination'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if ($this->dbh->GetRows() > 0)
   {
	   if(! $this->dbh->FetchRows())
	   {
	      $this->strERR = $this->dbh->GetError();
	      return 0;
	   }

	   $splitConfId = $this->dbh->GetFieldValue('id');

	   return $this->UpdateSplitConfiguration($splitConfId,$splitId,$destination,$url,$enabled);
   }

   $this->lastSQLCMD = "INSERT INTO split_configuration (split_id,destination,url,enabled) VALUES ('$splitId','$destination','$url','$enabled')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   //$splitId = $this->dbh->GetFieldValue("id");

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddSplitConfiguration
//
// [DESCRIPTION]:   Add new entry to the tracker_details table
//
// [PARAMETERS]:    $trackUrl='',$name=''
//
// [RETURN VALUE]:  trUrlID or 0 in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddSplitRotation($splitId,$nextUrl)
{
//   if($this->AssertSplitConfiguration($logID,$siteID, $details))
//      return 0;

   $this->lastSQLCMD = "select id from split_rotation where split_id='$splitId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   if ($this->dbh->GetRows() > 0)
   {
           if(! $this->dbh->FetchRows())
           {
              $this->strERR = $this->dbh->GetError();
              return 0;
           }

           $id = $this->dbh->GetFieldValue('id');
           return $id;
   }
	
   if (!$nextUrl)
   {
	   $this->lastSQLCMD = "select destination from split_configuration where split_id='$splitId' and enabled='on' order by id limit 1";

	   if(! $this->dbh->Exec($this->lastSQLCMD))
	   {
	       $this->strERR = $this->dbh->GetError();
	       return 0;
	   }

	   if(! $this->dbh->FetchRows())
	   {
	      $this->strERR = $this->dbh->GetError();
	      return 0;
	   }
	
	   $nextUrl = $this->dbh->GetFieldValue("destination");
   }

   $this->lastSQLCMD = "INSERT INTO split_rotation (split_id,next_url) VALUES ('$splitId','$nextUrl')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddSplitConfiguration
//
// [DESCRIPTION]:   Add new entry to the tracker_details table
//
// [PARAMETERS]:    $trackUrl='',$name=''
//
// [RETURN VALUE]:  trUrlID or 0 in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddSplitTracker($splitId,$landedUrl,$cookieValue)
{
//   if($this->AssertSplitConfiguration($logID,$siteID, $details))
//      return 0;

   $this->lastSQLCMD = "INSERT INTO split_tracker (split_id,landed_url,cookie,date,time) VALUES ('$splitId','$landedUrl','$cookieValue',now(),now())";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddSplitConfiguration
//
// [DESCRIPTION]:   Add new entry to the tracker_details table
//
// [PARAMETERS]:    $trackUrl='',$name=''
//
// [RETURN VALUE]:  trUrlID or 0 in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddSplitQuotes($splitTrackerId,$qzQuoteID)
{
//   if($this->AssertSplitConfiguration($logID,$siteID, $details))
//      return 0;

   $this->lastSQLCMD = "INSERT INTO split_quotes (split_tracker_id,qzquote_id) VALUES ('$splitTrackerId','$qzQuoteID')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateSplitConfiguration
//
// [DESCRIPTION]:   Update a entry to the track_urls table
//
// [PARAMETERS]:    $trackUrlID=0, $trackUrl='', $name=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateSplitConfiguration($splitConfId,$splitId=0, $destination='', $url='',$enabled='off')
{

   $this->lastSQLCMD = "update split_configuration set split_id='$splitId',destination='$destination',url='$url',enabled='$enabled' where id='$splitConfId'";
//print $this->lastSQLCMD;
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateSplitConfiguration
//
// [DESCRIPTION]:   Update a entry to the track_urls table
//
// [PARAMETERS]:    $trackUrlID=0, $trackUrl='', $name=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateSplitRotation($splitId,$nextUrl)
{

   $this->lastSQLCMD = "update split_rotation set next_url='$nextUrl' where split_id='$splitId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateSplitConfiguration
//
// [DESCRIPTION]:   Update a entry to the track_urls table
//
// [PARAMETERS]:    $trackUrlID=0, $trackUrl='', $name=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSplitRotationBySplitId($splitId)
{

   $this->lastSQLCMD = "SELECT * FROM split_rotation where split_id='$splitId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = "SPLIT_ROTATION_NOT_FOUND";
      return false;
   }

   $id = $this->dbh->GetFieldValue('id');
   return $id;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteSplitConfiguration
//
// [DESCRIPTION]:   Delete an entry from track_urls table by ID
//
// [PARAMETERS]:    $trackUrlID=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateDeleteSplit($splitId=0)
{
   $this->lastSQLCMD = "update split set enabled='off' where id='$splitId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTotalSplitConfigurationBySiteID
//
// [DESCRIPTION]:   Get all content of the table tracker_details
//
// [PARAMETERS]:    $siteID, $startDate, $endDate
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 08-10-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSplitRotation($splitId)
{
   $this->lastSQLCMD = "SELECT * FROM split_rotation where split_id='$splitId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = "SPLIT_ROTATION_NOT_FOUND";
      return false;
   }

   $nextUrl   = $this->dbh->GetFieldValue("next_url");

   return $nextUrl;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTotalSplitConfigurationBySiteID
//
// [DESCRIPTION]:   Get all content of the table tracker_details
//
// [PARAMETERS]:    $siteID, $startDate, $endDate
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 08-10-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSplitTrackerSplitIdByCookie($cookieName)
{
   $this->lastSQLCMD = "SELECT split_id FROM split_tracker where cookie='$cookieName'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = "SPLIT_CONFIGURATION_DETAILS_FOUND";
      return false;
   }


   return $this->dbh->GetFieldValue('split_id');
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTotalSplitConfigurationBySiteID
//
// [DESCRIPTION]:   Get all content of the table tracker_details
//
// [PARAMETERS]:    $siteID, $startDate, $endDate
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 08-10-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSplitTrackerByCookieAndSplitId($splitId,$cookieName)
{
   $this->lastSQLCMD = "SELECT * FROM split_tracker where split_id='$splitId' and cookie='$cookieName'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = "SPLIT_CONFIGURATION_DETAILS_FOUND";
      return false;
   }

   $arrayResult = array();

      $arrayResult['split_id']   = $this->dbh->GetFieldValue("split_id");
      $arrayResult['landed_url']   = $this->dbh->GetFieldValue("landed_url");
      $arrayResult['cookie']   = $this->dbh->GetFieldValue("cookie");
      $arrayResult['date']   = $this->dbh->GetFieldValue("date");
      $arrayResult['time']   = $this->dbh->GetFieldValue("time");

   return $arrayResult;
}



//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTotalSplitConfigurationBySiteID
//
// [DESCRIPTION]:   Get all content of the table tracker_details
//
// [PARAMETERS]:    $siteID, $startDate, $endDate
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 08-10-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFirstValidDestinationBySplitId($splitId)
{
           $this->lastSQLCMD = "select destination from split_configuration where split_id='$splitId' and enabled='on' order by id limit 1";

           if(! $this->dbh->Exec($this->lastSQLCMD))
           {
               $this->strERR = $this->dbh->GetError();
               return 0;
           }

           if(! $this->dbh->FetchRows())
           {
              $this->strERR = $this->dbh->GetError();
              return 0;
           }

           $nextUrl = $this->dbh->GetFieldValue("destination");

	   return $nextUrl;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTotalSplitConfigurationBySiteID
//
// [DESCRIPTION]:   Get all content of the table tracker_details
//
// [PARAMETERS]:    $siteID, $startDate, $endDate
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 08-10-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSplitTotal()
{
   $this->lastSQLCMD = "SELECT * FROM split where enabled='on'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = "SPLIT_CONFIGURATION_DETAILS_FOUND";
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("id")]['name']   = $this->dbh->GetFieldValue("name");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTotalSplitConfigurationBySiteID
//
// [DESCRIPTION]:   Get all content of the table tracker_details
//
// [PARAMETERS]:    $siteID, $startDate, $endDate
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 08-10-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllValidDestinationsBySplitId($splitId)
{
   $this->lastSQLCMD = "SELECT * FROM split_configuration where enabled='on' and url!='' and split_id='$splitId' order by destination";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = "SPLIT_CONFIGURATION_DETAILS_FOUND";
      return false;
   }

   $arrayResult = array();
   $cnt = 0;
   while($this->dbh->MoveNext())
   {
      $arrayResult[$cnt] = $this->dbh->GetFieldValue("destination");
      $cnt++;	
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTotalSplitConfigurationBySiteID
//
// [DESCRIPTION]:   Get all content of the table tracker_details
//
// [PARAMETERS]:    $siteID, $startDate, $endDate
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 08-10-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSplitById($splitId)
{
   $this->lastSQLCMD = "SELECT * FROM split where id='$splitId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = "SPLIT_CONFIGURATION_DETAILS_FOUND";
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("id")]['name'] = $this->dbh->GetFieldValue("name");
   }

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTotalSplitConfigurationBySiteID
//
// [DESCRIPTION]:   Get all content of the table tracker_details
//
// [PARAMETERS]:    $siteID, $startDate, $endDate
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 08-10-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSplitConfigurationEnabledById($splitId)
{
   $this->lastSQLCMD = "SELECT destination,url FROM split_configuration WHERE split_id='$splitId' and url!='' and enabled='on'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = "SPLIT_CONFIGURATION_DETAILS_FOUND";
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("destination")] = $this->dbh->GetFieldValue("url");
   }

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTotalSplitConfigurationBySiteID
//
// [DESCRIPTION]:   Get all content of the table tracker_details
//
// [PARAMETERS]:    $siteID, $startDate, $endDate
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 08-10-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSplitConfigurationById($splitId)
{
   $this->lastSQLCMD = "SELECT * FROM split_configuration WHERE split_id='$splitId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SPLIT_CONFIGURATION_DETAILS_FOUND");
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("id")]['split_id']    = $this->dbh->GetFieldValue("split_id");
      $arrayResult[$this->dbh->GetFieldValue("id")]['destination'] = $this->dbh->GetFieldValue("destination");
      $arrayResult[$this->dbh->GetFieldValue("id")]['url']         = $this->dbh->GetFieldValue("url");
      $arrayResult[$this->dbh->GetFieldValue("id")]['enabled']     = $this->dbh->GetFieldValue("enabled");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTotalSplitConfigurationBySiteID
//
// [DESCRIPTION]:   Get all content of the table tracker_details
//
// [PARAMETERS]:    $siteID, $startDate, $endDate
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 08-10-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSplitDestNumberBySplitId($splitId)
{
   $this->lastSQLCMD = "SELECT count(*) as cnt FROM split_configuration WHERE split_id='$splitId' and url!='' and enabled='on'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = "SPLIT_CONFIGURATION_DETAILS_FOUND";
      return false;
   }

	$cnt = $this->dbh->GetFieldValue('cnt');

   return $cnt;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   if(! preg_match("/\d{1,2}\/\d{1,2}\/\d{4}/i",$date))
      $this->strERR = "INVALID_QUOTE_STATUS_STD_TO_MYSQL_DATE"."\n";

   if(! empty($this->strERR))
      return false;

   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 19-09-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print  $this->strERR;
}

}

?>

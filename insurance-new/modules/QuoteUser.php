<?php
/*****************************************************************************/
/*                                                                           */
/*  CQuoteUsers class interface                                              */
/*                                                                           */
/*  (C) 2005 Istvancsek Gabriel (gabi@acrux.biz)                              */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";
include_once "QuoteDetails.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuoteUsers
//
// [DESCRIPTION]:  CQuoteUser class interface
//
// [FUNCTIONS]:    true   | false AssertQuoteUser(&$firstName, &$lastName, $birthDate='', $password='')
//                 string | false StdToMysqlDate($date='')
//                 int    | false AddQuoteUser($firstName='', $lastName='', $birthDate='', $password='')
//                 true   | false UpdateQuoteUser($quoteUserID='', $firstName='', $lastName='', $birthDate='', $password='')
//                 true   | false DeleteQuoteUser($quoteUserID='')
//                 array  | false GetQuoteUserByID($quoteUserID='')
//                 int    | false GetQuoteUserByEmail($userEmail='')
//                 array  | false CheckEmailUnicity($quoteUserID='')
//                 array  | false GetAllQuoteUsers()
//                 array  | false GetUserByEmailDateOfBirth($userEmail='', $dateOfBirth='')
//
//                 array  | false GetUserByFNameSNameDateOfBirthEmail($fName='', $sName='', $dateOfBirth='',$userEmail='')
//                 string | false GetUserPassword($userID='')
//                 array  | false GetAllQuotesByUserID($userID = '', $limit = '')
//                 array  | false GetAllQuotesByUserIDEmail($userID = '', $userEmail='', $showDeletedQuotes=true)
//                 array  | false GetAllQuoteDetailsByUserID($userID = '')
//                 array  | false GetAllQuoteDetailsByUserIDEmail($userID = '', $userEmail='', $showDeletedQuotes=true)
//                 true   | false MarkQuoteAsDeleted($type='',$userID='',$fileName='',$userEmail='')
//
//                 Close();
//                 GetError();
//                 ShowError();
//
// [CREATED BY]:   Istvancsek (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CQuoteUser
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuoteUsers
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQuoteUser($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuoteUser
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    &$firstName, &$lastName, $birthDate='', $password=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertQuoteUser(&$firstName, &$lastName, &$birthDate, &$password)
{
   $this->strERR = '';

   $firstName = addslashes($firstName);
   $lastName  = addslashes($lastName);
   $password  = addslashes($password);

   if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$firstName))
      $this->strERR = GetErrorString("INVALID_FIRST_NAME_FIELD")."\n";

   if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$lastName))
      $this->strERR .= GetErrorString("INVALID_LAST_NAME_FIELD")."\n";

   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$birthDate))
      $this->strERR .= GetErrorString("INVALID_BIRTH_DATE_FIELD")."\n";

// TO DO -> DEBUG UPDATE TO NEW VERSION
//    if(empty($password))
//       $this->strERR .= GetErrorString("NVALID_PASSWORD_FIELD")."\n";

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   //if(! preg_match("\/", $date))
   //  return $date;

   if(! preg_match("/\d{1,2}\/\d{1,2}\/\d{4}/i",$date))
      $this->strERR = GetErrorString("INVALID_QUOTE_USERS_STD_TO_MYSQL_DATE")."\n";

   if(! empty($this->strERR))
      return false;

   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteUser
//
// [DESCRIPTION]:   Check if exist a user and add if not exist into quote_users table
//
// [PARAMETERS]:    $firstName, $lastName, $birthDate, $password
//
// [RETURN VALUE]:  $quoteUserID | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteUser($firstName='', $lastName='', $birthDate='', $password='')
{
   // we use in case of update quote users !!!!
   $birthDateStd = $birthDate;

   if(! $birthDate = $this->StdToMysqlDate($birthDate))
      return false;

   if(! $this->AssertQuoteUser($firstName, $lastName, $birthDate, $password))
      return false;

   // first check if we already have this user in our DB
   $lastSqlCmd = "SELECT id FROM ".SQL_QUOTE_USERS." WHERE first_name='$firstName' AND last_name='$lastName' AND birth_date='$birthDate' LIMIT 1";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   if($this->dbh->GetRows())
   {
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      $quoteUserID = $this->dbh->GetFieldValue("id");

      // we have to make update into db for old password  so we save the last password
      // UPDATE MODE !!!
      if(! empty($password))
      {
         if(! $this->UpdateQuoteUser($quoteUserID, $firstName, $lastName, $birthDateStd, $password))
            return false;
      }
      // end UPDATE MODE;

      return $quoteUserID;
   }

   $lastSqlCmd = "INSERT INTO ".SQL_QUOTE_USERS." (first_name, last_name, birth_date, password) VALUES ('$firstName', '$lastName', '$birthDate', '$password')";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
      //Duplicate entry 'Ttttestabcd-Ttttestxyza-1974-05-03' for key 2 (1062)
      if($this->dbh->GetErrorNum($this->dbh->masterLinkID) == '1062')
      {
         // first check if we already have this user in our DB
         $lastSqlCmd = "SELECT id FROM ".SQL_QUOTE_USERS." WHERE first_name='$firstName' AND last_name='$lastName' AND birth_date='$birthDate' LIMIT 1";

         if(! $this->dbh->Exec($lastSqlCmd, true))
         {
            $this->strERR = $this->dbh->GetError();
            return false;
         }

         if($this->dbh->GetRows())
         {
            if(! $this->dbh->FetchRows())
            {
               $this->strERR = $this->dbh->GetError();
               return false;
            }

            return $this->dbh->GetFieldValue("id");
         }
      }

      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $lastSqlCmd = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQuoteUser
//
// [DESCRIPTION]:   Update a user data from the quote_users table
//
// [PARAMETERS]:    $quoteUserID, $firstName, $lastName, $birthDate, $password
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQuoteUser($quoteUserID='', $firstName='', $lastName='', $birthDate='', $password='')
{

   if(! preg_match("/^\d+$/", $quoteUserID))
   {
         $this->strERR = GetErrorString("INVALID_USER_ID_FIELD")."\n";
      return false;
   }

   if(! $birthDate=$this->StdToMysqlDate($birthDate))
      return false;

   if(! $this->AssertQuoteUser($firstName, $lastName, $birthDate, $password))
      return false;

   // check if userID exists
   $lastSqlCmd = "SELECT id FROM ".SQL_QUOTE_USERS." WHERE id='$quoteUserID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_ID_NOT_FOUND");
      return false;
   }

   $lastSqlCmd = "UPDATE ".SQL_QUOTE_USERS." SET first_name='$firstName', last_name='$lastName', birth_date='$birthDate', password='$password' WHERE id='$quoteUserID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
/////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQuoteUser
//
// [DESCRIPTION]:   Delete an entry from the quote_users table
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvcansek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteQuoteUser($quoteUserID='')
{
   if(! preg_match("/^\d+$/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_USER_ID_FIELD");
      return false;
   }

   $lastSqlCmd = "SELECT id FROM ".SQL_QUOTE_USERS." WHERE id='$quoteUserID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERID_NOT_FOUND");
      return false;
   }

   $lastSqlCmd = "DELETE FROM ".SQL_QUOTE_USERS." WHERE id='$quoteUserID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteUserByID
//
// [DESCRIPTION]:   Read data of a user from quote_users table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Istvacsek Gabriel (dan@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteUserByID($quoteUserID='')
{
   if(! preg_match("/^\d+$/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_USER_ID_FIELD");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM ".SQL_QUOTE_USERS." WHERE id='$quoteUserID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]         = $this->dbh->GetFieldValue("id");
   $arrayResult["first_name"] = $this->dbh->GetFieldValue("first_name");
   $arrayResult["last_name"]  = $this->dbh->GetFieldValue("last_name");
   $arrayResult["birth_date"] = $this->dbh->GetFieldValue("birth_date");
   $arrayResult["password"]   = $this->dbh->GetFieldValue("password");

   return $arrayResult;
}

function GetQuoteUser($quoteUserID=0)
{
   return $this->GetQuoteUserByID($quoteUserID);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteUserIDByEmail
//
// [DESCRIPTION]:   Read data of a user from quote_users table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  int or false in case of failure
//
// [CREATED BY]:    Istvacsek Gabriel (dan@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteUserByEmail($userEmail='')
{
   if(! preg_match("/\.*\@.*\.[a-zA-Z]{2,3}/i", $userEmail))
   {
      $this->strERR = GetErrorString("INVALID_USER_EMAIL_FIELD");
      return false;
   }

   $lastSqlCmd = "SELECT ".SQL_QUOTE_USERS.".id FROM ".SQL_QUOTE_USERS.",".SQL_QUOTE_USER_DETAILS." WHERE ".SQL_QUOTE_USERS.".id = ".SQL_QUOTE_USER_DETAILS.".quote_user_id AND ".SQL_QUOTE_USER_DETAILS.".email='$userEmail'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckEmailUnicity
//
// [DESCRIPTION]:   Read data from qzquotes table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
//
function CheckEmailUnicity($quoteUserID='')
{
   if(! preg_match("/\d+/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTE_USER_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT  count(distinct email) as entries FROM ".SQL_QUOTE_USERS.",".SQL_QUOTE_USER_DETAILS." WHERE ".SQL_QUOTE_USERS.".id='$quoteUserID' AND ".SQL_QUOTE_USER_DETAILS.".quote_user_id=".SQL_QUOTE_USERS.".id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QZ_quote_user_details_id_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $entries = $this->dbh->GetFieldValue("entries");

   if($entries == 1)
      return true;

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuoteUsers
//
// [DESCRIPTION]:   Read data from quote_users table and put it into an array variable
//                  key = userID, value = Array(that contain all the record for that user)
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (dan@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuoteUsers()
{
   $lastSqlCmd = "SELECT * FROM ".SQL_QUOTE_USERS."";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERS_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index]["id"]                  = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["first_name"]          = $this->dbh->GetFieldValue("first_name");
      $arrayResult[$index]["last_name"]           = $this->dbh->GetFieldValue("last_name");
      $arrayResult[$index]["birth_date"]          = $this->dbh->GetFieldValue("birth_date");
      $arrayResult[$index]["password"]            = $this->dbh->GetFieldValue("password");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserByEmailDateOfBirth($userEmail='', $dateOfBirth='')
//
// [DESCRIPTION]: get user id if exist
//
// [PARAMETERS]:  $userEmail, $dateOfBirth
//
// [RETURN VALUE]: int | false
//
// [CREATED BY]:    Istvancsek Gabriel (dan@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserByEmailDateOfBirth($userEmail='', $dateOfBirth='')
{
   if(! preg_match("/\.*\@.*\.[a-zA-Z]{2,3}/i", $userEmail))
   {
      $this->strERR = GetErrorString("INVALID_USER_EMAIL_FIELD");
      return false;
   }

   if(! $dateOfBirth = $this->StdToMysqlDate($dateOfBirth))
      return false;

   $lastSqlCmd = "SELECT ".SQL_QUOTE_USERS.".id FROM ".SQL_QUOTE_USERS.",".SQL_QUOTE_USER_DETAILS." WHERE ".SQL_QUOTE_USER_DETAILS.".email='$userEmail' AND ".SQL_QUOTE_USERS.".birth_date='$dateOfBirth' AND ".SQL_QUOTE_USERS.".id=".SQL_QUOTE_USER_DETAILS.".quote_user_id";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("INVALID_USER_DATE_OF_BIRTH_FIELD");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserByFNameSNameDateOfBirthEmail($fName='', $sName='', $dateOfBirth='', $userEmail='')
//
// [DESCRIPTION]:   get user id if exist
//
// [PARAMETERS]:    $fName, $sName, $dateOfBirth, $userEmail
//
// [RETURN VALUE]:  int | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-01-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserByFNameSNameDateOfBirthEmail($fName='', $sName='', $dateOfBirth='', $userEmail='')
{
   if(! preg_match("/[A-Za-z\-\s]+/i", $fName))
   {
      $this->strERR = GetErrorString("INVALID_USER_FIRST_NAME_FIELD");
      return false;
   }

   if(! preg_match("/[A-Za-z\-\s]+/i", $sName))
   {
      $this->strERR = GetErrorString("INVALID_USER_SURNAME_FIELD");
      return false;
   }

   if(! preg_match("/\.*\@.*\.[a-zA-Z]{2,3}/i", $userEmail))
   {
      $this->strERR = GetErrorString("INVALID_USER_EMAIL_FIELD");
      return false;
   }

   if(! $dateOfBirth = $this->StdToMysqlDate($dateOfBirth))
      return false;

   // determine the quote_user_details table(type of quotes that user made)-by first name,surname,date od birth
   $firstSqlCMD = "SELECT DISTINCT(".SQL_QZQUOTE_TYPES.".name) FROM ".SQL_QUOTE_USERS.",".SQL_LOGS.",".SQL_QUOTE_STATUS.",".SQL_SITES.",".SQL_QZQUOTE_TYPES." WHERE LOWER(".SQL_QUOTE_USERS.".first_name)='".strtolower($fName)."' AND LOWER(".SQL_QUOTE_USERS.".last_name)='".strtolower($sName)."' AND ".SQL_QUOTE_USERS.".birth_date='$dateOfBirth' AND ".SQL_QUOTE_USERS.".id=".SQL_LOGS.".quote_user_id AND ".SQL_LOGS.".id=".SQL_QUOTE_STATUS.".log_id AND ".SQL_QUOTE_STATUS.".status='SUCCESS' AND ".SQL_QUOTE_STATUS.".site_id=".SQL_SITES.".id AND ".SQL_SITES.".quote_type_id=".SQL_QZQUOTE_TYPES.".id LIMIT 1";
//print $firstSqlCMD."<br>";
   if(! $this->dbh->Exec($firstSqlCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   //$quoteTypeID = $this->dbh->GetFieldValue("id");
   $quoteType   = $this->dbh->GetFieldValue("name");

   switch($quoteType)
   {
      case 'car':
         $detailsTable = 'quote_user_details';
         $email_field  = 'email';
         break;

      case 'van':
         $detailsTable = 'quote_user_details';
         $email_field  = 'email';
         break;

      case 'home':
         $detailsTable = 'home_quote_user_details';
         $email_field  = 'email';
         break;

      case 'bike':
         $detailsTable = 'bike_quote_user_details';
         $email_field  = 'email';
         break;

      case 'travel':
         $detailsTable = 'travel_quote_user_details';
         $email_field  = 'email_address';
         break;
   }

   $lastSqlCmd = "SELECT DISTINCT(".SQL_QUOTE_USERS.".id) FROM ".SQL_QUOTE_USERS.",".$detailsTable." WHERE LOWER(".$detailsTable.".".$email_field.")='".strtolower($userEmail)."' AND ".SQL_QUOTE_USERS.".first_name='$fName' AND ".SQL_QUOTE_USERS.".last_name='$sName' AND ".SQL_QUOTE_USERS.".birth_date='$dateOfBirth' AND ".SQL_QUOTE_USERS.".id=".$detailsTable.".quote_user_id";
//print $lastSqlCmd."<br>";
   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("INVALID_USER_DATA_FIELDS");
      return false;
   }

   $result = "";
   while($this->dbh->MoveNext())
      $result .= $this->dbh->GetFieldValue("id").",";

   // extract last coma
   $result = substr($result, 0, -1);

   return $result;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserPassword($userID='')
//
// [DESCRIPTION]:   get user pass
//
// [PARAMETERS]:    $userID
//
// [RETURN VALUE]:  string | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-01-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserPassword($userID='')
{
   $userIDArray = explode(",", $userID);

   for($i=0;$i<count($userIDArray);$i++)
   {
      if(! preg_match("/\d+/i", $userIDArray[$i]))
      {
         $this->strERR = GetErrorString("INVALID_USER_ID_FIELD");
         return false;
      }
   }

   // get the last password
   $lastSqlCmd = "SELECT password FROM ".SQL_QUOTE_USERS." WHERE id IN (".$userID.") ORDER BY id DESC LIMIT 1";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("INVALID_USER");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $password = $this->dbh->GetFieldValue("password");

   return $password;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuotesByUserID($userID = '', $limit = '')
//
// [DESCRIPTION]:   Read data from logs table
//                  and put it into an array variable
//                  key = log id, value = Array(filename,date.time)
//
// [PARAMETERS]:    $userID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-01-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetAllQuotesByUserID($userID = '', $limit = '')
{
   if(! preg_match("/\d+/i", $userID))
   {
      $this->strERR = GetErrorString("INVALID_USER_ID_FIELD");
      return false;
   }

   // all quotes (possibly with other emails)
   $lastSqlCmd = "SELECT id,filename,date,time FROM ".SQL_LOGS." WHERE quote_user_id='$userID' ORDER BY id DESC";

   if(isset($limit) AND $limit != '')
      $lastSqlCmd .=" LIMIT $limit ";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_ID_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $logID    = $this->dbh->GetFieldValue("id");
      $filename = $this->dbh->GetFieldValue("filename");
      $date     = $this->dbh->GetFieldValue("date");
      $time     = $this->dbh->GetFieldValue("time");

      $arrayResult[$logID]["filename"] = $filename;
      $arrayResult[$logID]["date"]     = $date;
      $arrayResult[$logID]["time"]     = $time;
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuotesByUserIDEmail($userID='', $userEmail='', $showDeletedQuotes=true)
//
// [DESCRIPTION]:   Read data from logs table
//                  and put it into an array variable
//                  key = log id, value = Array(filename,date.time)
//
// [PARAMETERS]:    $userID,$userEmail,$showDeletedQuotes
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-01-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuotesByUserIDEmail($userID='', $userEmail='', $showDeletedQuotes=true)
{
   $userIDArray = explode(",",$userID);

   for($i=0;$i<count($userIDArray);$i++)
   {
      if(! preg_match("/\d+/i", $userIDArray[$i]))
      {
         $this->strERR = GetErrorString("INVALID_USER_ID_FIELD");
         return false;
      }
   }

   if(empty($userEmail))
   {
      $this->strERR = GetErrorString("INVALID_EMAIL_ADDRESS");
      return false;
   }

   $arrayResult = array();

   // only quotes with specified email

   //car,van
   $lastSqlCmd = "SELECT quote_user_details.quote_user_id as quote_user_id, ".SQL_QZQUOTES.".log_id AS log_id,".SQL_LOGS.".filename,".SQL_LOGS.".date,".SQL_LOGS.".time FROM ".SQL_LOGS.",quote_user_details,".SQL_QZQUOTES." WHERE ".SQL_LOGS.".quote_user_id IN (".$userID.") AND quote_user_details.quote_user_id IN (".$userID.") AND quote_user_details.email = '$userEmail' AND ".SQL_QZQUOTES.".quote_user_details_id=quote_user_details.id AND ".SQL_LOGS.".id= ".SQL_QZQUOTES.".log_id ";

   if(! $showDeletedQuotes)
   {
      $lastSqlCmd .=  " AND ".SQL_QZQUOTES.".quote_del='0' ";
   }

   $lastSqlCmd .=  " ORDER BY ".SQL_LOGS.".id DESC ";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }
/*
   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_ID_NOT_FOUND");
      return false;
   }
*/

   while($this->dbh->MoveNext())
   {
      $logID    = $this->dbh->GetFieldValue("log_id");
      $filename = $this->dbh->GetFieldValue("filename");
      $date     = $this->dbh->GetFieldValue("date");
      $time     = $this->dbh->GetFieldValue("time");

      $arrayResult[$logID]["filename"] = $filename;
      $arrayResult[$logID]["date"]     = $date;
      $arrayResult[$logID]["time"]     = $time;
   }

   // bike
   $lastSqlCmd = "SELECT bike_quote_user_details.quote_user_id as quote_user_id, ".SQL_QZQUOTES.".log_id AS log_id,".SQL_LOGS.".filename,".SQL_LOGS.".date,".SQL_LOGS.".time FROM ".SQL_LOGS.",bike_quote_user_details,".SQL_QZQUOTES." WHERE ".SQL_LOGS.".quote_user_id IN (".$userID.") AND bike_quote_user_details.quote_user_id IN (".$userID.")  AND bike_quote_user_details.email = '$userEmail' AND ".SQL_QZQUOTES.".quote_user_details_id=bike_quote_user_details.id AND ".SQL_LOGS.".id= ".SQL_QZQUOTES.".log_id ";

   if(! $showDeletedQuotes)
   {
      $lastSqlCmd .=  " AND ".SQL_QZQUOTES.".quote_del='0' ";
   }

   $lastSqlCmd .=  " ORDER BY ".SQL_LOGS.".id DESC ";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }
/*
   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_ID_NOT_FOUND");
      return false;
   }
*/

   while($this->dbh->MoveNext())
   {
      $logID    = $this->dbh->GetFieldValue("log_id");
      $filename = $this->dbh->GetFieldValue("filename");
      $date     = $this->dbh->GetFieldValue("date");
      $time     = $this->dbh->GetFieldValue("time");

      $arrayResult[$logID]["filename"] = $filename;
      $arrayResult[$logID]["date"]     = $date;
      $arrayResult[$logID]["time"]     = $time;
   }

   // home
   $lastSqlCmd = "SELECT home_quote_user_details.quote_user_id as quote_user_id, ".SQL_QZQUOTES.".log_id AS log_id,".SQL_LOGS.".filename,".SQL_LOGS.".date,".SQL_LOGS.".time FROM ".SQL_LOGS.",home_quote_user_details,".SQL_QZQUOTES." WHERE ".SQL_LOGS.".quote_user_id IN (".$userID.") AND home_quote_user_details.quote_user_id IN (".$userID.") AND home_quote_user_details.email = '$userEmail' AND ".SQL_QZQUOTES.".quote_user_details_id=home_quote_user_details.id AND ".SQL_LOGS.".id= ".SQL_QZQUOTES.".log_id ";

   if(! $showDeletedQuotes)
   {
      $lastSqlCmd .=  " AND ".SQL_QZQUOTES.".quote_del='0' ";
   }

   $lastSqlCmd .=  " ORDER BY ".SQL_LOGS.".id DESC ";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }
/*
   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_ID_NOT_FOUND");
      return false;
   }
*/

   while($this->dbh->MoveNext())
   {
      $logID       = $this->dbh->GetFieldValue("log_id");
      $filename    = $this->dbh->GetFieldValue("filename");
      $date        = $this->dbh->GetFieldValue("date");
      $time        = $this->dbh->GetFieldValue("time");

      $arrayResult[$logID]["filename"] = $filename;
      $arrayResult[$logID]["date"]     = $date;
      $arrayResult[$logID]["time"]     = $time;
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuoteDetailsByUserID($userID = '')
//
// [DESCRIPTION]:   Read data from logs,quote_status,quotes,quote_users,quote_user_details(for each type of quote)tables
//                  and put it into an array variable
//                  key = type of quote, value = Array(that contain all the record for that type of quotes)
//
// [PARAMETERS]:     $userID
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-01-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuoteDetailsByUserID($userID = '')
{
   $objQuote = new CQuoteDetails();

   if(! preg_match("/\d+/",$userID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_USER_ID_FIELD");
      return false;
   }

   // get all log ids for this user

   //set limit 50 in case we have some user tests
   $limit = 50;

   $quotesArray = array();

   // all quotes posibly with different emails
   $quotesArray = $this->GetAllQuotesByUserID($userID,$limit);

   $arrayResult = array();
   $index       = array();

   $lastFileName  = "";
   $lastQuoteType = "";
   $lastQuoteType = "";
   $lastTime      = "1900-01-27 00:00:00";     //date from the past

   //foreach quote get the user details
   foreach($quotesArray as $logID => $quoteDetailsArray )
   {
      // get the ceapest quote
      $cheappestSiteId         = "0";
      $cheappestQuotePrice     = "0";
      $cheappestQuoteArray     = array();
      $cheappestSiteQuoteArray = array();

      $cheappestSiteQuoteArray = $objQuote->GetCheapestTopFiveSitesQuoteDetails($logID, 1);
      $cheappestSiteId         = $cheappestSiteQuoteArray[1];
      $cheappestQuoteArray     = $objQuote->GetCheapestSiteQuoteDetails($logID,$cheappestSiteId);
      $cheappestQuotePrice     = $cheappestQuoteArray["cheapest_quote"];

      $filename = $quoteDetailsArray["filename"];
      $date     = $quoteDetailsArray["date"];
      $time     = $quoteDetailsArray["time"];

      $firstSqlCMD = "SELECT qzqt.id,qzqt.name FROM ".SQL_QZQUOTE_TYPES." qzqt, ".SQL_QZQUOTES." qzq,".SQL_LOGS." l,".SQL_QUOTE_STATUS." qs WHERE qs.log_id=l.id AND l.id=qzq.log_id AND qzq.quote_type_id=qzqt.id AND l.id='$logID'";

      if(! $this->dbh->Exec($firstSqlCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      $quoteTypeID = $this->dbh->GetFieldValue("id");
      $quoteType   = $this->dbh->GetFieldValue("name");

      // determine last quote filename and quote type
      $quoteTime = $date." ".$time;
      if($quoteTime > $lastTime)
      {
         $lastTime      = $quoteTime;
         $lastFileName  = $filename;
         $lastQuoteType = $quoteType;
      }

      switch($quoteType)
      {
         case 'car':
            $detailsTable = 'quote_user_details';
            $email_field  = 'email';
            break;

         case 'van':
            $detailsTable = 'quote_user_details';
            $email_field  = 'email';
            break;

         case 'home':
            $detailsTable = 'home_quote_user_details';
            $email_field  = 'email';
            break;

         case 'bike':
            $detailsTable = 'bike_quote_user_details';
            $email_field  = 'email';
            break;

         case 'travel':
            $detailsTable = 'travel_quote_user_details';
            $email_field  = 'email_address';
            break;
      }

      // get the fields from $detailsTable
      $fieldsArray = array();
      if(! $fieldsArray = $this->dbh->GetAllFieldsName($detailsTable))
         $this->strERR = $this->dbh->GetError();

      $lastSqlCmd = "SELECT ".$detailsTable.".*,".SQL_QZQUOTES.".quote_ref,".SQL_QZQUOTES.".affiliate_id FROM ".SQL_LOGS.",".SQL_QUOTE_STATUS.",".SQL_QUOTE_USERS.",".$detailsTable.",".SQL_QZQUOTES." WHERE ".SQL_QUOTE_STATUS.".log_id=".SQL_LOGS.".id AND ".SQL_QUOTE_STATUS.".status='SUCCESS' AND ".SQL_LOGS.".quote_user_id=".SQL_QUOTE_USERS.".id AND ".$detailsTable.".quote_user_id=".SQL_QUOTE_USERS.".id AND ".SQL_QZQUOTES.".log_id=".SQL_LOGS.".id AND ".SQL_QZQUOTES.".quote_user_details_id=".$detailsTable.".id  AND ".SQL_LOGS.".id='$logID' LIMIT 1";

      if(! $this->dbh->Exec($lastSqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = GetErrorString("LOG_ID_NOT_FOUND");
         return false;
      }

      if(!isset($index[$quoteType]))
            $index[$quoteType] = 0;

      $indexType = $index[$quoteType];

      $arrayResult[$quoteType][$indexType]["filename"]              = $filename;
      $arrayResult[$quoteType][$indexType]["date"]                  = $date;
      $arrayResult[$quoteType][$indexType]["time"]                  = $time;
      $arrayResult[$quoteType][$indexType]["cheappest_quote_price"] = $cheappestQuotePrice;

      // data from qzquotes table
      $arrayResult[$quoteType][$indexType]["quote_ref"]     = $this->dbh->GetFieldValue("quote_ref");
      $arrayResult[$quoteType][$indexType]["affiliate_id"]  = $this->dbh->GetFieldValue("affiliate_id");
      $arrayResult[$quoteType][$indexType]["wl_user_id"]    = $this->dbh->GetFieldValue("wl_user_id");

      //user details
      foreach($fieldsArray as $indexNr => $fieldName)
      {
         $arrayResult[$quoteType][$indexType]["user_details"][$fieldName] = $this->dbh->GetFieldValue($fieldName);
      }

      $index[$quoteType]++;
   }

   $arrayResult["last_file_name"]  = $lastFileName;
   $arrayResult["last_quote_type"] = $lastQuoteType;

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuoteDetailsByUserIDEmail($userID = '', $userEmail='', $showDeletedQuotes=true)
//
// [DESCRIPTION]:   Read data from logs,quote_status,quotes,quote_users,quote_user_details(for each type of quote)tables
//                  and put it into an array variable
//                  key = type of quote, value = Array(that contain all the record for that type of quotes)
//
// [PARAMETERS]:     $userID,$userEmail,$showDeletedQuotes
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-01-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuoteDetailsByUserIDEmail($userID = '', $userEmail='', $showDeletedQuotes=true)
{
   $objQuote = new CQuoteDetails();

   $userIDArray = explode(",",$userID);

   for($i=0;$i<count($userIDArray);$i++)
   {
      if(! preg_match("/\d+/",$userIDArray[$i]))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_USER_ID_FIELD");
         return false;
      }
   }

   if(empty($userEmail))
   {
      $this->strERR = GetErrorString("INVALID_EMAIL_ADDRESS");
      return false;
   }

   // get all log ids for this user with specified email

   $quotesArray = array();

   // all quotes with specified email
   $quotesArray = $this->GetAllQuotesByUserIDEmail($userID,$userEmail,$showDeletedQuotes);

   $arrayResult = array();
   $index       = array();

   $lastFileName  = "";
   $lastQuoteType = "";
   $lastQuoteType = "";
   $lastTime      = "1900-01-20 00:00:00";     //date from the past

   //foreach quote get the user details
   foreach($quotesArray as $logID => $quoteDetailsArray )
   {
      // get the ceapest quote
      $cheappestSiteId         = "0";
      $cheappestQuotePrice     = "0";
      $cheappestQuoteArray     = array();
      $cheappestSiteQuoteArray = array();

      $cheappestSiteQuoteArray = $objQuote->GetCheapestTopFiveSitesQuoteDetails($logID, 1);
      $cheappestSiteId         = $cheappestSiteQuoteArray[1];
      $cheappestQuoteArray     = $objQuote->GetCheapestSiteQuoteDetails($logID,$cheappestSiteId);
      $cheappestQuotePrice     = $cheappestQuoteArray["cheapest_quote"];

      $filename = $quoteDetailsArray["filename"];
      $date     = $quoteDetailsArray["date"];
      $time     = $quoteDetailsArray["time"];

      $firstSqlCMD = "SELECT qzqt.id,qzqt.name FROM ".SQL_QZQUOTE_TYPES." qzqt, ".SQL_QZQUOTES." qzq,".SQL_LOGS." l,".SQL_QUOTE_STATUS." qs WHERE qs.log_id=l.id AND l.id=qzq.log_id AND qzq.quote_type_id=qzqt.id AND l.id='$logID'";

      if(! $this->dbh->Exec($firstSqlCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         continue;
         //return false;
      }

      $quoteTypeID = $this->dbh->GetFieldValue("id");
      $quoteType   = $this->dbh->GetFieldValue("name");

      // determine last quote filename and quote type
      $quoteTime = $date." ".$time;
      if($quoteTime > $lastTime)
      {
         $lastTime      = $quoteTime;
         $lastFileName  = $filename;
         $lastQuoteType = $quoteType;
      }

      switch($quoteType)
      {
         case 'car':
            $detailsTable = 'quote_user_details';
            $email_field  = 'email';
            break;

         case 'van':
            $detailsTable = 'quote_user_details';
            $email_field  = 'email';
            break;

         case 'home':
            $detailsTable = 'home_quote_user_details';
            $email_field  = 'email';
            break;

         case 'bike':
            $detailsTable = 'bike_quote_user_details';
            $email_field  = 'email';
            break;

         case 'travel':
            $detailsTable = 'travel_quote_user_details';
            $email_field  = 'email_address';
            break;
      }

      // get the fields from $detailsTable
      $fieldsArray = array();
      if(! $fieldsArray = $this->dbh->GetAllFieldsName($detailsTable))
         $this->strERR = $this->dbh->GetError();

      $lastSqlCmd = "SELECT ".$detailsTable.".*,".SQL_QZQUOTES.".quote_ref,".SQL_QZQUOTES.".affiliate_id FROM ".SQL_LOGS.",".SQL_QUOTE_STATUS.",".SQL_QUOTE_USERS.",".$detailsTable.",".SQL_QZQUOTES." WHERE ".SQL_QUOTE_STATUS.".log_id=".SQL_LOGS.".id AND ".SQL_LOGS.".quote_user_id=".SQL_QUOTE_USERS.".id AND ".$detailsTable.".quote_user_id=".SQL_QUOTE_USERS.".id AND ".SQL_QZQUOTES.".log_id=".SQL_LOGS.".id AND ".SQL_QZQUOTES.".quote_user_details_id=".$detailsTable.".id  AND ".SQL_LOGS.".id='$logID' LIMIT 1";

      if(! $this->dbh->Exec($lastSqlCmd))
      {
         print "<br>errrrr:".$this->strERR = $this->dbh->GetError();
         return false;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = GetErrorString("LOG_ID_NOT_FOUND");
         return false;
      }

      if(!isset($index[$quoteType]))
            $index[$quoteType] = 0;

      $indexType = $index[$quoteType];

      $arrayResult[$quoteType][$indexType]["filename"]              = $filename;
      $arrayResult[$quoteType][$indexType]["date"]                  = $date;
      $arrayResult[$quoteType][$indexType]["time"]                  = $time;
      $arrayResult[$quoteType][$indexType]["cheappest_quote_price"] = $cheappestQuotePrice;

      // data from qzquotes table
      $arrayResult[$quoteType][$indexType]["quote_ref"]     = $this->dbh->GetFieldValue("quote_ref");
      $arrayResult[$quoteType][$indexType]["affiliate_id"]  = $this->dbh->GetFieldValue("affiliate_id");
      $arrayResult[$quoteType][$indexType]["wl_user_id"]    = $this->dbh->GetFieldValue("wl_user_id");

      //user details
      foreach($fieldsArray as $indexNr => $fieldName)
      {
         $arrayResult[$quoteType][$indexType]["user_details"][$fieldName] = $this->dbh->GetFieldValue($fieldName);
      }

      $index[$quoteType]++;
   }//foreach quote get the user details

   $arrayResult["last_file_name"]  = $lastFileName;
   $arrayResult["last_quote_type"] = $lastQuoteType;

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MarkQuoteAsDeleted($type='',$userID='',$fileName='',$userEmail='')
//
// [DESCRIPTION]:   Mark quote as deleted
//
// [PARAMETERS]:    $type,$userID,$fileName,$userEmail
//
// [RETURN VALUE]:  true or false in case of failure
//
// [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2008-04-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MarkQuoteAsDeleted($type='',$userID='',$fileName='',$userEmail='')
{
   $typeArray = array("car", "van", "bike", "home", "travel");
   if (! in_array($type, $typeArray))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_TYPE");
      return false;
   }

   if(! preg_match("/\d+/i", $userID))
   {
      $this->strERR = GetErrorString("INVALID_USER_ID_FIELD");
      return false;
   }

   if(! preg_match("/\d+_\d+/",$fileName))
   {
      $this->strERR = GetErrorString("INVALID_FILE_NAME_FIELD");
      return false;
   }

   if(! preg_match("/^[A-Za-z0-9]{1}((\.)?[A-Za-z0-9_-]+)*@[A-Za-z0-9-\.]+(\.[A-Za-z]{2,4})$/",$userEmail))
   {
      $this->strERR = GetErrorString("INVALID_EMAIL_ADDRESS_FIELD");
      return false;
   }

   //print "should mark deleted :type :".$type."  userID - ".$userID."  quote :".$fileName. " email :".$userEmail;

   switch($type)
   {
      case 'car':
      case 'van':
                  $quoteDetailsTable = "quote_user_details";
                  break;
      case 'bike':
                  $quoteDetailsTable = "bike_quote_user_details";
                  break;
      case 'home':
                  $quoteDetailsTable = "home_quote_user_details";
                  break;
      case 'travel':
                  $quoteDetailsTable = "travel_quote_user_details";
                  break;
   }

   // get the number of active quotes
   $lastSqlCmd = "SELECT COUNT(".SQL_QZQUOTES.".log_id) AS nr_quotes FROM ".SQL_LOGS.",".$quoteDetailsTable.",".SQL_QZQUOTES.",".SQL_QZQUOTE_TYPES." WHERE ".SQL_QZQUOTES.".quote_type_id = ".SQL_QZQUOTE_TYPES.".id AND ".SQL_QZQUOTE_TYPES.".name ='".$type."' AND ".SQL_LOGS.".quote_user_id IN (".$userID.") AND ".$quoteDetailsTable.".quote_user_id IN (".$userID.")  AND ".$quoteDetailsTable.".email = '$userEmail' AND ".SQL_QZQUOTES.".quote_user_details_id=".$quoteDetailsTable.".id AND ".SQL_LOGS.".id= ".SQL_QZQUOTES.".log_id ";

   $lastSqlCmd .=  " AND ".SQL_QZQUOTES.".quote_del='0' ";
   $lastSqlCmd .=  " ORDER BY ".SQL_LOGS.".id DESC ";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $nrActiveQuotes = $this->dbh->GetFieldValue("nr_quotes");

   if($nrActiveQuotes == 1)
      return false;

   //mark the quote as deleted
   $lastSqlCmd = "UPDATE ".SQL_QZQUOTES.",".SQL_LOGS.",".$quoteDetailsTable." SET ".SQL_QZQUOTES.".quote_del = '1' WHERE ".SQL_QZQUOTES.".log_id = ".SQL_LOGS.".id AND ".SQL_LOGS.".filename = '".$fileName."' AND ".SQL_QZQUOTES.".quote_user_details_id = ".$quoteDetailsTable.".id AND ".$quoteDetailsTable.".quote_user_id IN (".$userID.") ";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserByFNameSNameDateOfBirth($fName='', $sName='', $dateOfBirth='')
//
// [DESCRIPTION]: get user id if exist
//
// [PARAMETERS]:  $fName='', $sName='', $dateOfBirth=''
//
// [RETURN VALUE]: $arrayResult or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (dan@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserByFNameSNameDateOfBirth($fName='', $sName='', $dateOfBirth='')
{

   if(! $birthDate = $this->StdToMysqlDate($dateOfBirth))
      return false;

   if(! $this->AssertQuoteUser($fName, $sName, $birthDate))
      return false;

   $lastSqlCmd = "SELECT * FROM ".SQL_QUOTE_USERS." WHERE first_name='$fName' AND last_name='$sName' AND birth_date='$birthDate'";

   if(! $this->dbh->Exec($lastSqlCmd, 1))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult['id']       = $this->dbh->GetFieldValue("id");
   $arrayResult['password'] = $this->dbh->GetFieldValue("password");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserByEmailPassword
//
// [DESCRIPTION]: get user id if exist
//
// [PARAMETERS]:  $userEmail, $dateOfBirth
//
// [RETURN VALUE]: int | false
//
// [CREATED BY]:    Listeveanu Cezar (dan@acrux.biz) 2006-03-30
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserByEmailPassword($userEmail='', $password='')
{
   if(! preg_match("/\.*\@.*\.[a-zA-Z]{2,3}/i", $userEmail))
   {
       $this->strERR = GetErrorString("INVALID_USER_EMAIL_FIELD");
       return false;
   }

    //if(! $this->AssertQuoteUser($password))
    //      return false;

    $lastSqlCmd = "SELECT qu.id FROM ".SQL_QUOTE_USERS." qu, ".SQL_QUOTE_USER_DETAILS." qud WHERE qud.email='$userEmail' AND qu.password='$password' AND qu.id=qud.quote_user_id GROUP by qu.id";

    if(! $this->dbh->Exec($lastSqlCmd))
    {
        $this->strERR = $this->dbh->GetError();
        return false;
    }

    if(! $this->dbh->GetRows())
    {
        $this->strERR = GetErrorString("INVALID_USER_PASSWORD");
        return false;
    }

    $arrayResult = array();
    $index = 0;
    while($this->dbh->MoveNext())
    {
        $index++;
        $arrayResult[$index]["id"] = $this->dbh->GetFieldValue("id");
    }

    return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Show the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

}//end class CQuoteUsers

?>

<?php

/*****************************************************************************/
/*                                                                           */
/*  CTellAFriend class interface
/*                                                                           */
/*  (C) 2004 Eugen Savin (seugen@abc.ro)                                     */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CTellAFriend
//
// [DESCRIPTION]:  CTellAFriend class interface
//
// [FUNCTIONS]:    int  AddTellAFriend($name='', $email='',$date='now()')
//                 bool DeleteTellAFriend($friendID=0)
//                 bool GetTellAFriend($friendID=0, &$arrayResult)
//                 bool GetAllTellAFriends(&$arrayResult)
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Eugen SAVIN (seugen@abc.ro) 2004-07-12
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CTellAFriend
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CTellAFriend
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function CTellAFriend($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddTellAFriend
//
// [DESCRIPTION]:   Add new entry to the tell_a_friend table
//
// [PARAMETERS]:    $name='', $email=''
//
// [RETURN VALUE]:  friendID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function AddTellAFriend($name="",$email="",$date='now()')
{
   if(empty($name))
   {
      $this->strERR = GetErrorString("INVALID_NAME_FIELD");
      return 0;
   }

   if(empty($email))
   {
      $this->strERR = GetErrorString("INVALID_EMAIL_FIELD");
   }

   $this->lastSQLCMD = "INSERT INTO ".SQL_TELL_A_FRIEND." (name,email,cr_date) VALUES ('$name','$email',$date)";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteTellAFriend
//
// [DESCRIPTION]:   Delete an entry from tell_a_friend table
//
// [PARAMETERS]:    $friendID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function DeleteTellAFriend($friendID)
{
   if(! preg_match("/^\d+$/", $friendID))
   {
      $this->strERR = GetErrorString("INVALID_FRIENDID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_TELL_A_FRIEND." WHERE id='$friendID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("FRIENDID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_TELL_A_FRIEND." WHERE id='$friendID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllTellAFriends
//
// [DESCRIPTION]:   Read data from tell_a_friend table and put it into an array variable
//                  key = friendID, value = emailAddress
//
// [PARAMETERS]:    &$arrayResult
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetAllTellAFriends(&$arrayResult)
{
   $this->lastSQLCMD = "SELECT * FROM ".SQL_TELL_A_FRIEND."";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("FRIENDS_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $friendResult["name"] = $this->dbh->GetFieldValue("name");
      $friendResult["email"] = $this->dbh->GetFieldValue("email");

      $arrayResult[$this->dbh->GetFieldValue("id")] = $friendResult;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTellAFriend
//
// [DESCRIPTION]:   Read data from tell_a_friend table and put it into an array variable
//
// [PARAMETERS]:    $friendID, &$arrayResult
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetTellAFriend($friendID=0,&$arrayResult)
{
   if(! preg_match("/^\d+$/", $friendID))
   {
      $this->strERR = GetErrorString("INVALID_FRIENDID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_TELL_A_FRIEND." WHERE id='$friendID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("FRIENDID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]    = $this->dbh->GetFieldValue("id");
   $arrayResult["name"]  = $this->dbh->GetFieldValue("name");
   $arrayResult["email"] = $this->dbh->GetFieldValue("email");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

}// end of CTellAFriends class

?>
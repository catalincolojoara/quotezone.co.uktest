<?php
/*****************************************************************************/
/*                                                                           */
/*  EmailOutboundsEngineBike class interface                                 */
/*                                                                           */
/*  (C) 2008 Gabriel ISTVANCSEK (gabi@acrux.biz)                             */
/*                                                                           */
/*****************************************************************************/
include_once "errors.inc";
include_once "MySQL.php";
include_once "EmailStore.php";
include_once "EmailReminder.php";
include_once "EmailStoreCodes.php";
include_once "QuoteDetails.php";
include_once "Vehicle.php";
include_once "Occupation.php";
include_once "Business.php";
include_once "Session.php";
include_once "EmailOutboundsEngine.php";
include_once "EmailOutbounds.php";
include_once "/home/www/quotezone.co.uk/common/modules/CashBack.php";

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CEmailOutboundsEngineBike
//
// [DESCRIPTION]:  CEmailOutboundsEngineBike class interface, email outbounds support,
//                 PHP version
//
// [FUNCTIONS]:    FormatUserDetails($Session)
//                 CheckEmailOutboundResponse()
//                 SendUserDetails()
//                 ShowError()
//                 GetError()
//
//
//  (C) 2008 Gabriel ISTVANCSEK (gabi@acrux.biz) 2008-02-01
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CEmailOutboundsEngineBike Extends CEmailOutboundsEngine
{
    var $fileName;           // the fileName
    var $strERR;             // last error string
    var $systemType;         // system type(car,van,home,bike)
    var $quoteTypeID;        // quote type id (1-car , 2-van, 4-home, 5-bike)
    var $objEmailStore;      // email store id field
    var $objEmailStoreCodes; // EmailStoreCodes.php class object
    var $objEmailReminder;   // EmailReminder.php class object
    var $objQuoteDetails;    // QuoteDetails.php class object
    var $objVehicle;         // Vehicle.php class object
    var $objOccupation;      // Occupation.php class ojject
    var $objBusiness;        // Business.php class object
    var $objSession;         // Session.php class object
    var $objEmailOutbounds;  //email outbound object
    var $host;               //emv host
    var $path;               //emv path
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CEmailOutboundsEngineBike
   //
   // [DESCRIPTION]:   Default class constructor. Initialization goes here.
   //
   // [PARAMETERS]:    $fileName, $dbh
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CEmailOutboundsEngineBike($fileName, $dbh)
   {
      CEmailOutboundsEngine::CEmailOutboundsEngine('bike','in');

      $this->fileName = $fileName;

      $this->systemType  = 'bike';
      $this->quoteTypeID = '5';

      // ticket - ZTH-983371
      $this->host = "trc.emv2.com";
      $this->path = "/D2UTF8";

      $this->strERR  = "";

      if($dbh)
      {
         $this->dbh = $dbh;
         $this->closeDB = false;
      }
      else
      {
         // default configuration
         $this->dbh = new CMySQL();

         if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
         {
           $this->strERR = $this->dbh->GetError();
           return;
         }
      }

      $this->closeDB = true;

      $this->objQuoteDetails    = new CQuoteDetails($this->dbh);
      $this->objEmailStore      = new CEmailStore($this->dbh);
      $this->objEmailStoreCodes = new CEmailStoreCodes($this->dbh);
      $this->objEmailReminder   = new CEmailReminder($this->dbh);
      $this->objVehicle         = new CVehicle($this->dbh);
      $this->objOccupation      = new COccupation($this->dbh);
      $this->objBusiness        = new CBusiness($this->dbh);
      $this->objSession         = new CSession($this->dbh);
      $this->objEmailOutbounds  = new CEmailOutbounds($this->dbh);

   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: FormatUserDetails
   //
   // [DESCRIPTION]:   Form user data details to be sent it
   //
   // [PARAMETERS]:    $Session
   //
   // [RETURN VALUE]:  formated $string or false if failure
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function FormatUserDetails($Session)
   {
      /*start of driver details */
   $_PROPOSER = array(

      "type_of_driving" => array(
        "F"  =>"Full UK Motorcycle",
        "M"  =>"Full UK Moped",
        "P"  =>"Provisional UK Motorcycle",
        "EEC"=>"Full EEC Motorcycle",
        "E"  =>"Full European (Ex EEC) Motorcycle",
        "I"  =>"Full International Motorcycle",
        "PE" =>"Provisional European (Ex EEC) Motorcycle",
        "PI" =>"Provisional International Motorcycle"
      ),

      "marital_status" => array(
         "M"=>"Married",
         "S"=>"Single",
         "D"=>"Divorced",
         "W"=>"Widowed",
      ),

      "use_of_other_vehicles" => array(
         "X"=>"No access to any other vehicles",
         "O"=>"Owner of another vehicle",
         "N"=>"Named driver on another person's policy",
         "S"=>"Company car (including social domestic and pleasure use)",
         "C"=>"Company car (within working hours)",
         "M"=>"Own/have use of a Motorcycle",
         "V"=>"Own/have use of a Van",
      ),

      "who_drive_the_vehicle" => array(
         "IO"=>"Yourself Only",
         "IS"=>"Yourself and your spouse",
         "02"=>"Yourself & 1 other person",
         "03"=>"Yourself & 2 other persons",
         "04"=>"Yourself & 3 other persons",
      ),

      "employment_status" => array(
         "E"=>"Employed",
         "F"=>"Full-time education",
         "S"=>"Self Employed",
         "R"=>"Retired",
         "U"=>"Unemployed",
         "D"=>"Director",
         "P"=>"Proprietor or Partner",
         "H"=>"Houseperson",
         "G"=>"Government",
         "C"=>"Club or Association",
      ),

   );

   $_ADDITIONAL = array(
      "additional_sex" => array(
         "F" => "Female",
         "M" => "Male",
      ),

      "additional_type_of_driving" => array(
         "F"  =>"Full UK Motorcycle",
         "M"  =>"Full UK Moped",
         "P"  =>"Provisional UK Motorcycle",
         "EEC"=>"Full EEC Motorcycle",
         "E"  =>"Full European (Ex EEC) Motorcycle",
         "I"  =>"Full International Motorcycle",
         "PE" =>"Provisional European (Ex EEC) Motorcycle",
         "PI" =>"Provisional International Motorcycle"
      ),
   );

   $_VEHICLE =array(
   "vehicle_make" => array (
         "AX" => "Adly",
         "AO" => "Aeon",
         "AJ" => "Ajs",
         "AW" => "Ajw",
         "AD" => "Ambassador",
         "AP" => "Aprilia",
         "AK" => "Arctic Cat",
         "AE" => "Ariel",
         "AR" => "Armstrong",
         "AT" => "Atala",
         "AV" => "Avanti",
         "BD" => "Baimo",
         "BJ" => "Bajaj",
         "BN" => "Baotian",
         "BA" => "Barron",
         "BF" => "Bashan",
         "BV" => "Batavus",
         "BC" => "Battistinis",
         "BZ" => "Battle",
         "BE" => "Benelli",
         "BT" => "Beta",
         "BI" => "Bimota",
         "BQ" => "Blaney",
         "BB" => "Bmw",
         "BO" => "Borile",
         "BX" => "Boxer",
         "BR" => "Branson",
         "BS" => "Bsa",
         "BY" => "Buell",
         "BL" => "Bultaco",
         "BU" => "Bzr Motorcycles",
         "CG" => "Cagiva",
         "CL" => "Calthorpe",
         "CM" => "Can-Am",
         "CA" => "Cannondale",
         "CS" => "Casal",
         "CC" => "Ccm",
         "CF" => "Cf Moto",
         "CN" => "Ch Racing",
         "CH" => "Champ",
         "CB" => "Chituma",
         "CU" => "Chunlan",
         "CI" => "Cimatti",
         "CR" => "Ckr",
         "CK" => "Cossack",
         "CX" => "Cotton",
         "CP" => "Cpi",
         "CV" => "Cv",
         "CZ" => "Cz",
         "DA" => "Daelim",
         "DL" => "Dalesman",
         "DJ" => "Demm",
         "DR" => "Derbi",
         "DO" => "Diablo",
         "DI" => "Diblasi",
         "DN" => "Dinli",
         "DB" => "Dirtbike",
         "DX" => "Dkw",
         "DD" => "Dot",
         "DZ" => "Dresda",
         "DU" => "Ducati",
         "DK" => "Dukar",
         "EA" => "Easy Rider",
         "EN" => "Enfield",
         "EP" => "Evo",
         "EV" => "Evt",
         "EX" => "Excelsior",
         "FA" => "Factory Bike",
         "FM" => "Famel",
         "FC" => "Fantic",
         "FR" => "Forever",
         "FO" => "Fosti",
         "FB" => "Francis Barnett",
         "GA" => "Garelli",
         "GG" => "Gas Gas",
         "GE" => "Geely",
         "GN" => "Generic",
         "GC" => "Giantco",
         "GI" => "Gilera",
         "GT" => "Gitane",
         "GR" => "Greeves",
         "HA" => "Harley Davidson",
         "HH" => "Harris",
         "HF" => "Hartford",
         "HE" => "Her Chee",
         "HK" => "Hero",
         "HP" => "Hero Puch",
         "HS" => "Hesketh",
         "HI" => "Himotor",
         "HM" => "Hm",
         "HO" => "Honda",
         "HN" => "Hongdon",
         "HW" => "Hongdou",
         "HG" => "Hongdu",
         "HJ" => "Hongyi",
         "HX" => "Horex",
         "HT" => "Hostivar",
         "HQ" => "Hrd",
         "HR" => "Husaberg",
         "HV" => "Husqvarna",
         "HY" => "Hyosung",
         "HU" => "Hyundai",
         "ID" => "Indian",
         "IN" => "Infineon",
         "IT" => "Italjet",
         "IA" => "Italvel",
         "JM" => "James",
         "JW" => "Jawa",
         "JI" => "Jialing",
         "JA" => "Jianshe",
         "JN" => "Jinchenge",
         "JL" => "Jinlun",
         "JG" => "Jlangmen Giantco",
         "JO" => "Jordan Motors",
         "KN" => "Kangda",
         "KI" => "Kanuni",
         "KB" => "Kari-bike",
         "KW" => "Kawasaki",
         "KA" => "Kawata",
         "KZ" => "Kazuma",
         "KE" => "Keen",
         "KK" => "Kinetic",
         "KG" => "Kington",
         "KL" => "Kinlon",
         "KS" => "Kinroad Shmoot",
         "KX" => "Kinroad Xintian",
         "KR" => "Krauser",
         "KT" => "Ktm",
         "KY" => "Kymco",
         "LB" => "Lambretta",
         "LY" => "Lanying",
         "LV" => "Laverda",
         "LE" => "Lem",
         "LI" => "Lifan Hongda",
         "LH" => "Linhai",
         "LM" => "Linmax",
         "LL" => "Lml",
         "LN" => "Loncin",
         "MQ" => "Magni",
         "MA" => "Maico",
         "ML" => "Malaguti",
         "MM" => "Matchless",
         "QM" => "Mbk",
         "TC" => "Mecatecno",
         "MF" => "Mig",
         "MN" => "Milano",
         "MP" => "Minsk",
         "MY" => "Moby",
         "MW" => "Mobylette",
         "MS" => "Modenas",
         "MO" => "Monnier",
         "MU" => "Montesa",
         "LO" => "Mopeds Slovakia",
         "MI" => "Morini",
         "MK" => "Moto Aupa",
         "GU" => "Moto Guzzi",
         "MG" => "Moto Morini",
         "MR" => "Moto Roma",
         "MX" => "Motobecane",
         "MB" => "Motobeno",
         "ME" => "Motograziella",
         "MT" => "Motomel",
         "MJ" => "Motor Jikov",
         "QZ" => "Motor Manet",
         "QQ" => "Motorhispania",
         "PO" => "Polaris",
         "PQ" => "Motron",
         "MV" => "M.V. Agusta",
         "MC" => "Mainbon",
         "MD" => "Muz/Mz",
         "NV" => "Neval",
         //      "NI" => "New Imperial",
         "NM" => "Norman",
         "NO" => "Norton",
         "NT" => "Nvt",
         //      "OK" => "OK",
         //      "OS" => "Ossa",
         //      "PA" => "Panther",
         //      "PV" => "Parker Vincent",
         "BH" => "PBH Precision",
         "PE" => "Pedallec",
         "PT" => "Petronas",
         "PD" => "Peugeot",
         "PB" => "Pgo",
         "PI" => "Pioneer",
         "PH" => "Puch",
         "QU" => "Qinggi",
         "QI" => "Qingqi",
         "RA" => "Raleigh",
         "RG" => "Regent",
         "RC" => "Rickman",
         "RJ" => "Rieju",
         "RM" => "Rmc",
         "RV" => "Rovigo",
         "RT" => "Rtx",
         "RU" => "Rudge",
         "SA" => "Sachs",
         "SN" => "Sanglas",
         "SQ" => "Sanyang",
         "SB" => "Scoot Electric",
         "SD" => "Scootermaster",
         "SR" => "Scorpa",
         "SV" => "Serveta",
         "SG" => "Shanghai Meitian M/C",
         "SE" => "She Lung",
         "SO" => "Sherco",
         "SI" => "Siamoto",
         "SW" => "Silk",
         "SS" => "Simson",
         "SJ" => "Sinski",
         "SY" => "Skyteam",
         "SM" => "Standard Motor Co",
         "ST" => "Starway",
         "SL" => "Stella",
         "SK" => "Sukida",
         "SX" => "Sunbeam",
         "SC" => "Sundiro",
         "SU" => "Superbyke",
         "SF" => "Suzuki",
         "SH" => "Sym",
         "TS" => "Taishan",
         "TK" => "Taizhou Kaitong",
         "TA" => "Tayako",
         "TG" => "Tgb",
         "TH" => "Thumpstar",
         "TN" => "Titan",
         "TM" => "Tm",
         "TO" => "Tomos",
         //      "TL" => "Trike",
         "TI" => "Trisba",
         "TP" => "Triumph",
         "TJ" => "Trojan",
         "UR" => "Uralmoto",
         "UV" => "U.V.M",
         //      "VC" => "VCR Motor",
         "VV" => "Velocette",
         "VE" => "Velosolex",
         "VR" => "Vertemati",
         "PG" => "Vespa-Piaggio",
         "VL" => "Vialli",
         "VI" => "Victory",
         "VN" => "Vincent",
         "VO" => "Vor Motori",
         "VU" => "Vulcan",
         "WA" => "Warrior (Honda)",
         "WK" => "White Knuckle",
         "YH" => "Yamaha",
         "YM" => "Yamoto",
         "YZ" => "Yezdi",
         "YO" => "Yongwang",
         "ZB" => "Zebretta",
         "ZH" => "Zhejiang",
         "ZX" => "Zhixi",
         "ZO" => "Zhongyu",
         "ZI" => "Zipstar",
         //"ZP" => "Zundapp",
         "BM" => "BTS Motorsport",
         "GM" => "Gamax",
         "KQ" => "Keeway",
         "MZ" => "Matrix",
         "WU" => "Wuyang",
         "ZG" => "Zongshen"
      ),

      "vehicle_kept" => array(
         "R"=>"On the road",
         "D"=>"On a private driveway",
         "G"=>"In a locked garage",
      ),

   );

   $_COVER = array(
      "type_of_cover" => array(
         "1"=>"Comprehensive",
         "2"=>"Third Party Fire & Theft",
         "3"=>"Third Party Only",
      ),

      "type_of_use" => array(
         "4"=>"Social, Domestic, Pleasure and Commuting",
         "5"=>"Social, Domestic and Pleasure Only",
         "1"=>"Business Use (by the proposer)",
      ),

      "protect_bonus" => array(
         "Y" => "Yes",
         "N" => "No",
      ),

      "owner_of_vehicle" => array(
         "1"=>"Proposer", 
         "2"=>"Spouse",
         "3"=>"Parents",
         "4"=>"Company",
         "5"=>"Privately Leased",
         "6"=>"Other",
      ),
   );



      /* end of driver details */


      $fileName = $this->fileName;

      $logID          = $Session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];
      $emailAddress   = $Session["_DRIVERS_"]["0"]["email_address"];
      $quoteUserID    = $Session["_QZ_QUOTE_DETAILS_"]["quote_user_id"];

      if (!$resEmailStoreDetails = $this->objEmailStore->GetEmailStoreByEmailAndQuoteUserId($emailAddress,$quoteUserID))
         print $this->objEmailStore->GetError();

      if (!$link =  $this->objEmailStoreCodes->GenerateLink($resEmailStoreDetails['id'],$resEmailStoreDetails['email'],$this->quoteTypeID))
         print $this->objEmailStoreCodes->GetError();

      $cheappestSiteId          = "0";
      $cheapeastQuotePrice      = "0";
      
      $cheappestSiteId2         = "0";
      $cheapeastQuotePrice2     = "0";
      
      $cheappestSiteId3         = "0";
      $cheapeastQuotePrice3     = "0";
      
      $cheappestQuoteArray      = array();
      $cheappestQuoteArray2     = array();
      $cheappestQuoteArray3     = array();
      
      $cheappestSiteQuoteArray = array();

      $cheappestSiteQuoteArray = $this->objQuoteDetails->GetCheapestTopFiveSitesQuoteDetails($logID, 3);
            
      $cheappestSiteId         = $cheappestSiteQuoteArray[1]; 
      $cheappestQuoteArray     = $this->objQuoteDetails->GetCheapestSiteQuoteDetails($logID,$cheappestSiteId);
      $cheapeastQuotePrice     = $cheappestQuoteArray["cheapest_quote"];

      $cheappestSiteId2         = $cheappestSiteQuoteArray[2];      
      $cheappestQuoteArray2     = $this->objQuoteDetails->GetCheapestSiteQuoteDetails($logID,$cheappestSiteId2);
      $cheapeastQuotePrice2     = $cheappestQuoteArray2["cheapest_quote"];

      $cheappestSiteId3         = $cheappestSiteQuoteArray[3];      
      $cheappestQuoteArray3     = $this->objQuoteDetails->GetCheapestSiteQuoteDetails($logID,$cheappestSiteId3);
      $cheapeastQuotePrice3     = $cheappestQuoteArray3["cheapest_quote"];
      
      $PostArray = array();

      // new fields - ticket ZTH-983371
      $PostArray["emv_tag"]     = "100004E37E4C22D0";
      $PostArray["emv_ref"]     = "EdX7CqkmjHln8SA9MKJPUB7WKEx6HKm2-T-of9o0WrTZK_Q";

      $PostArray["EMAIL_FIELD"] = $Session["_DRIVERS_"]["0"]["email_address"];

      $date = date("Y-m-d");

      
      if(! $emStoreID = $this->objEmailStore->GetEmailStoreIDByEmailAndQuoteUserId($PostArray["EMAIL_FIELD"],$quoteUserID))
      {
         print "CANNOT_GET_THE_EMAIL_STORE_ID \n\n";
      }

      while (1)
      {
         $authInfo = $this->GenerateMimeBoundary();
         $authInfo = str_replace("----=_NextPart_","",$authInfo);
         $authInfo = substr($authInfo, 0, 16);

         if(! $this->objEmailReminder->GetEmRemByAuthInfo($authInfo, $array))
            break;

      }

      if(! $this->objEmailReminder->AddEmailRem($quoteUserID,$emStoreID,"1",$date,"","1", $authInfo,'5','1','0'))
      {
         print $this->objEmailReminder->GetError();
      }

      $PostArray["RENEW_LINK_FIELD"]          = $link;
      $PostArray["emv_bounceback"]            = "1";
      //$PostArray["emv_bounceback"]            = "0";
      $PostArray["emv_pageok"]                = "http://www.quotezone.co.uk/emailpostsuccessbike.htm";
      $PostArray["emv_pageerror"]             = "http://www.quotezone.co.uk/emailpostfailurebike.htm";
      $PostArray["emv_webformtype"]           = "0";
      $PostArray["emv_clientid"]              = "35812";
      //$PostArray["emv_campaignid"]            = "2992601";
      $PostArray["emv_campaignid"]            = "5959848";

      // change campaign id for gogreencompare
      $wlUserID = $Session['_QZ_QUOTE_DETAILS_']['wlUserID'];

      if ($wlUserID == 239)
	      $PostArray["emv_campaignid"] = 7180902;

      $PostArray["TITLE_FIELD"]               = $Session["_DRIVERS_"]["0"]["title"];
      $PostArray["FIRSTNAME_FIELD"]           = $Session["_DRIVERS_"]["0"]["first_name"];
      $PostArray["LASTNAME_FIELD"]            = $Session["_DRIVERS_"]["0"]["surname"];
      $PostArray["DATEOFBIRTH_FIELD"]         = $Session["_DRIVERS_"]["0"]["date_of_birth_mm"]."/".$Session["_DRIVERS_"]["0"]["date_of_birth_dd"]."/".$Session["_DRIVERS_"]["0"]["date_of_birth_yyyy"];
      $codeMake                               = $Session["_VEHICLE_"]["vehicle_make"];
      $make                                   = $_VEHICLE["vehicle_make"][$codeMake];
      $PostArray["VEHICLE_MAKE_FIELD"]        = $make;
      $PostArray["VEHICLE_MODEL_FIELD"]       = $Session["_VEHICLE_"]["vehicle_model"];
      $PostArray["YEAR_OF_MANUFACTURE_FIELD"] = $Session["_VEHICLE_"]["year_of_manufacture"];
      $engine                                 = explode(" ",$Session["_VEHICLE_"]["engine_size"]);
      switch($engine[1])
      {
         case "P":
            $engineFuel = "Petrol";
         break;

         case "D":
            $engineFuel = "Diesel";
         break;

      }

      $PostArray["ENGINE_SIZE_AND_TYPE_FIELD"]  = $engine[0]."cc ".$engineFuel;
      $PostArray["ABI_DESCRIPTION_FIELD"]       = $Session["_VEHICLE_"]["engine_size"];
      $vehicleElementsArray                     = $this->objVehicle->GetVehicle($this->objVehicle->GetVehicleCode($Session["_VEHICLE_"]["vehicle_confirm"]));

      $PostArray["ABI_DESCRIPTION_FIELD"]       =  $vehicleElementsArray["make_description"]." ".$vehicleElementsArray["model_description"];

      $maritalStatus                            = $Session["_DRIVERS_"]["0"]["marital_status"];
      $PostArray["MARITAL_STATUS_FIELD"]        = $_PROPOSER["marital_status"][$maritalStatus];
      $PostArray["NUMBER_OF_CHILDREN_FIELD"]    = $Session["_DRIVERS_"]["0"]["children"];
      $PostArray["POSTCODE_FIELD"]              = $Session["_DRIVERS_"]["0"]["postcode_prefix"]." ".$Session["_DRIVERS_"]["0"]["postcode_number"];
      $PostArray["HOUSE_NUMBER_FIELD"]          = $Session["_DRIVERS_"]["0"]["house_number_or_name"];
      $PostArray["STREET_FIELD"]                = $Session["_DRIVERS_"]["0"]["address_line2"];
      $PostArray["TOWN_CITY_FIELD"]             = $Session["_DRIVERS_"]["0"]["address_line3"];
      $PostArray["COUNTY_FIELD"]                = $Session["_DRIVERS_"]["0"]["address_line4"];
      $homeOwner                                = $Session["_DRIVERS_"]["0"]["home_owner"];
      if($homeOwner == "N")
         $PostArray["HOMEOWNER_FIELD"]          = "No";
      if($homeOwner == "Y")
         $PostArray["HOMEOWNER_FIELD"]          = "Yes";

      $PostArray["INSURANCE_START_DATE_FIELD"]  = $Session["_DRIVERS_"]["0"]["date_of_insurance_start_mm"]."/".$Session["_DRIVERS_"]["0"]["date_of_insurance_start_dd"]."/".$Session["_DRIVERS_"]["0"]["date_of_insurance_start_yyyy"];
      $PostArray["QUOTE_DATE_FIELD"]            = date('m')."/".date('d')."/".date('Y');

      $useOfOtherVehicles                       = $Session["_DRIVERS_"]["0"]["use_of_other_vehicles"];
      $PostArray["USE_OF_OTHER_VEHICLES_FIELD"] = $_PROPOSER["use_of_other_vehicles"][$useOfOtherVehicles];

      $driversToBeInsured                       = $Session["_DRIVERS_"]["0"]["who_drive_the_vehicle"];
      $PostArray["DRIVERS_TO_BE_INSURED_FIELD"] = $_PROPOSER["who_drive_the_vehicle"][$driversToBeInsured];

      $employmentStatus                         = $Session["_DRIVERS_"]["0"]["employment_status"];
      $PostArray["EMPLOYMENT_STATUS_FIELD"]     = $_PROPOSER["employment_status"][$employmentStatus];

      $this->objOccupation->GetOccupation($Session["_DRIVERS_"]["0"]["occupation_list"],&$ftOccArrayResult);
      $this->objBusiness->GetBusiness($Session["_DRIVERS_"]["0"]["business_list"], &$ftBusArrayResult);

      $this->objOccupation->GetOccupation($Session["_DRIVERS_"]["0"]["occupation_list_pt"],&$ptOccArrayResult);
      $this->objBusiness->GetBusiness($Session["_DRIVERS_"]["0"]["business_list_pt"], &$ptBusArrayResult);

      $PostArray["FULL_TIME_OCCUPATION_FIELD"]   = $ftOccArrayResult["name"];
      $PostArray["FULL_TIME_BUSINESS_FIELD"]     = $ftBusArrayResult["name"];

      $PostArray["PART_TIME_OCCUPATION_FIELD"]   = $ptOccArrayResult["name"];
      $PostArray["PART_TIME_BUSINESS_FIELD"]     = $ptBusArrayResult["name"];

      $typeOfDriving                             = $Session["_DRIVERS_"]["0"]["type_of_driving"];
      $PostArray["DRIVING_LICENCE_TYPE_FIELD"]   = $_PROPOSER["type_of_driving"][$typeOfDriving];
      $passPluss                                 = $Session["_DRIVERS_"]["0"]["driver_pass_plus"];

      if($passPluss == "N")
         $PostArray["PASS_PLUS_FIELD"] = "No";
      if($passPluss == "Y")
         $PostArray["PASS_PLUS_FIELD"] = "Yes";

      $PostArray["LICENCE_DATE_FIELD"]           = $Session["_DRIVERS_"]["0"]["period_of_licence_held_mm"]."/".$Session["_DRIVERS_"]["0"]["period_of_licence_held_dd"]."/".$Session["_DRIVERS_"]["0"]["period_of_licence_held_yyyy"];

      $NrConv                                    = $this->objSession->GetSessionConvictionsDriverNumber($Session,0);
      $NrClaims                                  = $this->objSession->GetSessionClaimsDriverNumber($Session,0);

      if(! $NrConv)
         $NrConv = "0";
      if(! $NrClaims)
         $NrClaims = "0";

      $PostArray["NUMBER_OF_CLAIMS_FIELD"]        = $NrClaims;
      $PostArray["NUMBER_OF_CONVICTIONS_FIELD"]   = $NrConv;

      $coverType                                  = $Session["_COVER_"]["type_of_cover"];
      $PostArray["COVER_TYPE_FIELD"]              = $_COVER["type_of_cover"][$coverType];
      $typeOfUse                                  = $Session["_COVER_"]["type_of_use"];
      $PostArray["TYPE_OF_USE_FIELD"]             = $_COVER["type_of_use"][$typeOfUse];
      $PostArray["ANNUAL_MILEAGE_FIELD"]          = $Session["_COVER_"]["annual_mileage"];

      if($Session["_COVER_"]["business_mileage"] == "" or ! isset($Session["_COVER_"]["business_mileage"]))
         $PostArray["ANNUAL_BUSINESS_MILEAGE_FIELD"] = 0;
      else
         $PostArray["ANNUAL_BUSINESS_MILEAGE_FIELD"] = $Session["_COVER_"]["business_mileage"];

      $PostArray["VOLUNTARY_EXCESS_FIELD"]        = $Session["_COVER_"]["voluntary_excess"];
      $PostArray["YEAR_NCB_FIELD"]                = $Session["_COVER_"]["no_claims_bonus"];

      $mainUserOfVehicle                          = $Session["_COVER_"]["main_user_of_vehicle"];
      switch($mainUserOfVehicle)
      {
         case "1":
            $mainUserText                         = $Session["_DRIVERS_"]["0"]["title"]." ".$Session["_DRIVERS_"]["0"]["first_name"]." ".$Session["_DRIVERS_"]["0"]["surname"];
         break;

         case "2":
            $mainUserText                         = $Session["_DRIVERS_"]["1"]["additional_title"]." ".$Session["_DRIVERS_"]["1"]["additional_first_name"]." ".$Session["_DRIVERS_"]["1"]["additional_surname"];
         break;

         case "3":
            $mainUserText                         = $Session["_DRIVERS_"]["2"]["additional_title"]." ".$Session["_DRIVERS_"]["2"]["additional_first_name"]." ".$Session["_DRIVERS_"]["2"]["additional_surname"];
         break;

         case "4":
            $mainUserText                         = $Session["_DRIVERS_"]["3"]["additional_title"]." ".$Session["_DRIVERS_"]["3"]["additional_first_name"]." ".$Session["_DRIVERS_"]["3"]["additional_surname"];
         break;

      }
      $PostArray["MAIN_USER_FIELD"]               = $mainUserText;
      $ownerOfVehicle                             = $Session["_COVER_"]["owner_of_vehicle"];
      switch($ownerOfVehicle)
      {
         case "1":
            $ownerOfVehicleText                   = $Session["_DRIVERS_"]["0"]["title"]." ".$Session["_DRIVERS_"]["0"]["first_name"]." ".$Session["_DRIVERS_"]["0"]["surname"];
         break;

         case "2":
            $ownerOfVehicleText                   = $Session["_DRIVERS_"]["1"]["additional_title"]." ".$Session["_DRIVERS_"]["1"]["additional_first_name"]." ".$Session["_DRIVERS_"]["1"]["additional_surname"];
         break;

         case "3":
            $ownerOfVehicleText                    = "Parents";
         break;

         case "4":
            $ownerOfVehicleText                    = "Company";
         break;

         case "5":
            $ownerOfVehicleText                    = "Privately Leased";
         break;

         case "6":
            $ownerOfVehicleText                    = "Other";
         break;
      }
      $PostArray["OWNER_FIELD"]                    = $ownerOfVehicleText;

      $registeredOfVehicle                         = $Session["_COVER_"]["registered_of_vehicle"];
      switch($ownerOfVehicle)
      {
         case "1":
            $registeredOfVehicleText               = $Session["_DRIVERS_"]["0"]["title"]." ".$Session["_DRIVERS_"]["0"]["first_name"]." ".$Session["_DRIVERS_"]["0"]["surname"];
         break;

         case "2":
            $registeredOfVehicleText               = $Session["_DRIVERS_"]["1"]["additional_title"]." ".$Session["_DRIVERS_"]["1"]["additional_first_name"]." ".$Session["_DRIVERS_"]["1"]["additional_surname"];
         break;

         case "3":
            $registeredOfVehicleText               = "Parents";
         break;

         case "4":
            $registeredOfVehicleText               = "Company";
         break;

         case "5":
            $registeredOfVehicleText               = "Privately Leased";
         break;

         case "6":
            $registeredOfVehicleText               = "Other";
         break;
      }

      $PostArray["REGISTERED_KEEPER_FIELD"]        = $registeredOfVehicleText;

      $PostArray["NUMBER_OF_OTHER_VEHICLES_FIELD"] = $Session["_COVER_"]["number_of_other_vehicles"];

      $sid = str_replace("_","",$fileName);

      $PostArray["QUOTE_TYPE_FIELD"]               = "bike";
      $PostArray["QUOTE_RESULTS_LINK_FIELD"]       = "https://bike-insurance.quotezone.co.uk/bike/index.php?sid=$sid";
      $PostArray["EMVADMIN1_FIELD"]                = $sid;

      // MoneyMaxim (ticket - XEV-793421)
      if($wlUserID == 136)
      {
         $PostArray["emv_campaignid"]              = "7887147";
         $PostArray["QUOTE_TYPE_FIELD"]            = "moneymaxim bike";

         $PostArray["emv_ref"]                     = "EdX7CqkmjGiW8SA9MKJPUB7UKEp7GKrE8jCvc6kyWMDZK9A";
         $PostArray["emv_tag"]                     = "306147788B804000";
      }

      // change sid link for gogreencompare
      $wlUserID = $Session['_QZ_QUOTE_DETAILS_']['wlUserID'];
      if ($wlUserID == 239)
      {
         //$PostArray["QUOTE_RESULTS_LINK_FIELD"]       = "http://www.gogreencompare.com/Retrieve.aspx?fn=$sid&type=bike";
         $PostArray["QUOTE_RESULTS_LINK_FIELD"]       = "http://www.ukgreencompare.com/Retrieve.aspx?fn=$sid&type=bike";

         $PostArray["emv_ref"]                        = "EdX7CqkmjGiR8SA9MKJPUB7VKjl6Ha3D-jjcfqg-WsWsK9A";
         $PostArray["emv_tag"]                        = "22E010000151825E";
      }

      //payingtoomuch
      if ($wlUserID == 496)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "PayingTooMuch_bike";
         $PostArray["emv_tag"]          = "B64CC81198080000";
         $PostArray["emv_ref"]          = "EdX7CqkmjK9l8SA9MKJPUB6lLkgJb6XC-zHVe6E2WMDZKF8";
      }

      //nhscashback 
      if ($wlUserID == 584)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "nhsCashback_bike";
         $PostArray["emv_tag"]          = "C66020001CD3203A";
         $PostArray["emv_ref"]          = "EdX7CqkmjJ_q8SA9MKJPUB6kLkp6Hq3D-jmuD6o0WMOoK9c"; 
      }

      //mirrorcashback 
      if ($wlUserID == 586)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "mirrorCashback_bike";
         $PostArray["emv_tag"]          = "3F46BFB54CC04003";
         $PostArray["emv_ref"]          = "EdX7CqkmjJ_t8SA9MKJPUB7UXkh8btux_zyuCKkyWMDaK4k"; 
      }

      //froggybank 
      if ($wlUserID == 580)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "froggyBank_bike";
         $PostArray["emv_tag"]          = "1F038F6A998081F0";
         $PostArray["emv_ref"]          = "EdX7CqkmjJxy8SA9MKJPUB7WXkx5FNvFizHUc6k-WbbZKBo"; 
      }

      //giveortake 
      if ($wlUserID == 587)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "giveOrtake_bike";
         $PostArray["emv_tag"]          = "B54CC0400033DA7E";
         $PostArray["emv_ref"]          = "EdX7CqkmjJwB8SA9MKJPUB6lLUgJb63H-jjdeKpCKcesKOU"; 
      }

      //psdiscount 
      if ($wlUserID == 585)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "psDiscount_bike";
         $PostArray["emv_tag"]          = "B94026B198080005";
         $PostArray["emv_ref"]          = "EdX7CqkmjJ_n8SA9MKJPUB6lIUh6Hqux-zHVe6E2WMDcK94"; 
      }

      //vacmedia 
      if ($wlUserID == 588)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "vacMedia_bike";
         $PostArray["emv_tag"]          = "A77996B19808A779";
         $PostArray["emv_ref"]          = "EdX7CqkmjJ_48SA9MKJPUB6mL0tzFaux-zHVe6FHX8fQK5w"; 
      }
      
      //topcashback 
      if ($wlUserID == 604)
      {
         $cheapestSiteID   = $cheappestQuoteArray['site_id'];
         $cheapestPremium  = $cheappestQuoteArray['cheapest_quote'];
         $cashbackAmount   = CalculateCashBack($cheapestSiteID,$cheapestPremium,"","",$Session['CPA'][$cheapestSiteID],$wlUserID);
         
         $cheapestSiteID2  = $cheappestQuoteArray2['site_id'];
         $cheapestPremium2 = $cheappestQuoteArray2['cheapest_quote'];
         $cashbackAmount2  = CalculateCashBack($cheapestSiteID2,$cheapestPremium2,"","",$Session['CPA'][$cheapestSiteID2],$wlUserID);
         
         $cheapestSiteID3  = $cheappestQuoteArray3['site_id'];
         $cheapestPremium3 = $cheappestQuoteArray3['cheapest_quote'];
         $cashbackAmount3  = CalculateCashBack($cheapestSiteID3,$cheapestPremium3,"","",$Session['CPA'][$cheapestSiteID3],$wlUserID);

         $memberID                           = trim($Session["_QZ_QUOTE_DETAILS_"]["memberID"]);
         $PostArray["EMVADMIN3_FIELD"]       = $memberID;
         $PostArray["QUOTE_TYPE_FIELD"]      = "topCashBack_bike";

         //$PostArray["emv_tag"]               = "D13301000003186D";
         //$PostArray["emv_ref"]               = "EdX7CqkmjJyh8SA9MKJPUB6jKU95HKzD-jjde6o3UMatK48";

         // new
         $PostArray["emv_tag"]               = "56C473C010000F3E";
         $PostArray["emv_ref"]               = "EdX7Cqkmjy7a8SA9MKJPUB7SLj9-G66w-jnde6k2LsOsKwk";
                  
         $PostArray["CASHBACK_AMOUNT_FIELD"] = $cashbackAmount;
         $PostArray["CASHBACK_TOTAL_FIELD"]  = $cheapestPremium-$cashbackAmount;
         
         $PostArray["CASHBACK_AMOUNT2_FIELD"] = $cashbackAmount2;
         $PostArray["CASHBACK_TOTAL2_FIELD"]  = $cheapestPremium2-$cashbackAmount2;
         
         $PostArray["CASHBACK_AMOUNT3_FIELD"] = $cashbackAmount3;
         $PostArray["CASHBACK_TOTAL3_FIELD"]  = $cheapestPremium3-$cashbackAmount3;
      }

      //leedscompare 
      if ($wlUserID == 608)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "lufc_bike";
         $PostArray["emv_tag"]          = "3010000144A7FFE3";
         $PostArray["emv_ref"]          = "EdX7CqkmjIFF8SA9MKJPUB7UKE16HK3D-zzZCq5ALrXaKAk"; 
      }


	 //andybiggsiar 
      if ($wlUserID == 633)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "carinsurancecomparison_bike";
         $PostArray["emv_tag"]          = "1D78CBF52680201D";
         $PostArray["emv_ref"]          = "EdX7CqkmjImJ8SA9MKJPUB7WXEtyb9-1_zrbc6k0WMGtKGM"; 
      }


      //quoteyquotey 
      if ($wlUserID == 631)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "quoteyquotey_bike";
         $PostArray["emv_tag"]          = "1000078E2C7AD341";
         $PostArray["emv_ref"]          = "EdX7CqkmjImV8SA9MKJPUB7WKEx6HKrLjzqufNhCW8TYKGw"; 
      }



	 //msmithfreelance 
      if ($wlUserID == 632)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "cheapcarinsurance_bike";
         $PostArray["emv_tag"]          = "80003378AC541A00";
         $PostArray["emv_ref"]          = "EdX7CqkmjIn58SA9MKJPUB7fKEx6H67E8kmufq03KcDZK-I"; 
      }

		 //msmithfreelance2 
      if ($wlUserID == 634)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "cheapvaninsurance_bike";
         $PostArray["emv_tag"]          = "80800076AAEFC01A";
         $PostArray["emv_ref"]          = "EdX7CqkmjPCO8SA9MKJPUB7fKER6HK3E_EmsDt9FWMGoKHs"; 
      }

      //cheap
      if ($wlUserID == 644)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "cheapcouk_bike";
         $PostArray["emv_tag"]          = "20001C1ABAD926A2";
         $PostArray["emv_ref"]          = "EdX7CqkmjPRz8SA9MKJPUB7VKEx6Hd7Ci0qsD6A0XrHbKCg"; 
      }



      //moneymedia
      if ($wlUserID == 601)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "moneymedia_bike";
         $PostArray["emv_tag"]          = "1000048A8D3ED350";
         $PostArray["emv_ref"]          = "EdX7CqkmjPaT8SA9MKJPUB7WKEx6HKnLizCpeNxCW8XZKHs"; 
      }



      //bestbuymoney
      if ($wlUserID == 640)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "bestbuymoney_bike";
         $PostArray["emv_tag"]          = "71A8080000D13586";
         $PostArray["emv_ref"]          = "EdX7CqkmjPfv8SA9MKJPUB7QKT1yHKXD-jjdD6g1XcjfK9I"; 
      }



      //cpcomparisons
      if ($wlUserID == 652)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "cpcomparisons_bike";
         $PostArray["emv_tag"]          = "5D9B0080003B4B59";
         $PostArray["emv_ref"]          = "EdX7CqkmjOO58SA9MKJPUB7SXEUIHK3L-jjdeNsyKsXQKG8"; 
      }

      //savoo
      if ($wlUserID == 661)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "savoo_bike";
         $PostArray["emv_tag"]          = "96D4154DC0400035";
         $PostArray["emv_ref"]          = "EdX7CqkmjO1T8SA9MKJPUB7eLjh-HajHjkvdf6k2WMPcKDA"; 
      }



      //moneyhelpline
      if ($wlUserID == 657)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "moneyhelpline_bike";
         $PostArray["emv_tag"]          = "31421B8080002E11";
         $PostArray["emv_ref"]          = "EdX7CqkmjO918SA9MKJPUB7UKUh4Hd_L-jDde6k0LcHYKA4"; 
      }

      //maximiles
      if ($wlUserID == 666)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "maximiles_bike";
         $PostArray["emv_tag"]          = "4ED7879C00800034";
         $PostArray["emv_ref"]          = "EdX7CqkmjNe58SA9MKJPUB7TXTh9FKrKiTjdc6k2WMPdKGI"; 
      }

      //richerair
      if ($wlUserID == 689)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "richerair_bike";
         $PostArray["emv_tag"]          = "9010000A6D994A13";
         $PostArray["emv_ref"]          = "EdX7CqkmjMoH8SA9MKJPUB7eKE16HK3Diz6pcqAyKcHaKIk"; 
      }

      //soswitch
      if ($wlUserID == 690)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "soswitch_bike";
         $PostArray["emv_tag"]          = "F0CBF3801000085A";
         $PostArray["emv_ref"]          = "EdX7CqkmjMDm8SA9MKJPUB6hKD8Iaq7L-jnde6k2UMWoK7c"; 
      }


      //comparecomiar
      if ($wlUserID == 681)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "comparecomiar_bike";
         $PostArray["emv_tag"]          = "799E2FD838010000";
         $PostArray["emv_ref"]          = "EdX7CqkmjMBr8SA9MKJPUB7QIUUPHtu38jvVe6g2WMDZKCk"; 
      }
     
      //aspectwebmedia
      if ($wlUserID == 695)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "aspectwebmedia_bike";
         $PostArray["emv_tag"]          = "F5FE84274020001A";
         $PostArray["emv_ref"]          = "EdX7CqkmjzBA8SA9MKJPUB6hLToPFKnB_Tzdeak2WMGoK-g";
      }
      //pouringpounds
      if ($wlUserID == 701)
      {
         $PostArray["QUOTE_TYPE_FIELD"] = "pouringpounds_bike";
         $PostArray["emv_tag"]          = "BCEC040002EA4CC6";
         $PostArray["emv_ref"]          = "EdX7Cqkmjyt18SA9MKJPUB6lWzkJHKnD-jjfDtgyK7PfK64";
      }

      //policybuddy
      if ($wlUserID == 697)
      {
       	$PostArray["QUOTE_TYPE_FIELD"] = "policybuddy_bike";
         $PostArray["emv_tag"]          = "C040003802451B0E";
         $PostArray["emv_ref"]          = "EdX7CqkmjyQu8SA9MKJPUB6kKEh6HK3A8jjff6w3KsCsK-A";
      }


//$PostArray["emv_campaignid"]
      $unsubscribeLink = $this->CreateUnsubscribeLinkParams($Session["_DRIVERS_"]["0"]["email_address"],'top3','bike')."&sid=$sid&type=1&action=unsubscribe&campaignId=".$PostArray["emv_campaignid"];
      //$PostArray["UNSUBSCRIBE_LINK_FIELD"]          = "http://bike-insurance.quotezone.co.uk/customer/unsubscribe.php?$unsubscribeLink";
      $PostArray["UNSUBSCRIBE_LINK_FIELD"]          = "https://www.quotezone.co.uk/customer/unsubscribe.php?$unsubscribeLink";

      if($wlUserID)
	$PostArray["UNSUBSCRIBE_LINK_FIELD"]          = "https://www.quotezone.co.uk/customer/wl-unsubscribe.php?emailaddress=".$Session["_DRIVERS_"]["0"]["email_address"];

      if($Session["_DRIVERS_"]["0"]["advanced_motorist"] == "Y")
         $PostArray["iam_member"]                   = "Yes";

      if($Session["_DRIVERS_"]["0"]["advanced_motorist"] == "N")
         $PostArray["IAM_MEMBER_FIELD"]             = "No";

      $PostArray["QUOTE_TIME_FIELD"]                = date("Gis");

      if($Session["_VEHICLE_"]["vehicle_modified"] == "N")
         $PostArray["MODIFIED_FIELD"]               = "No";

      if($Session["_VEHICLE_"]["vehicle_modified"] == "Y")
         $PostArray["MODIFIED_FIELD"]               = "Yes";

      if($Session["_VEHICLE_"]["vehicle_grey_or_import"] == "Y")
         $PostArray["IMPORT_FIELD"]                 = "Yes";

      if($Session["_VEHICLE_"]["vehicle_grey_or_import"] == "N")
         $PostArray["IMPORT_FIELD"]                 = "No";

      if($Session["_VEHICLE_"]["vehicle_side"] == "N")
         $PostArray["LEFT_RIGHT_FIELD"]             = "Right";

      if($Session["_VEHICLE_"]["vehicle_side"] == "Y")
         $PostArray["LEFT_RIGHT_FIELD"]             = "Left";

      $vehicleKept                                  = $Session["_VEHICLE_"]["vehicle_kept"];
      $PostArray["KEPT_OVERNIGHT_FIELD"]            = $_VEHICLE["vehicle_kept"][$vehicleKept];

      //alarm and immobiliser
      switch($Session["_VEHICLE_"]["vehicle_immobiliser"])
      {
         // Thatcham Category 1
         case 'TC1' :
               $PostArray["ALARM_FIELD"]            = "No";
               $PostArray["IMMOBILISER_FIELD"]      = "Yes";
            break;

        //  Thatcham Category 2
         case 'TC2' :
               $PostArray["ALARM_FIELD"]            = "No";
               $PostArray["IMMOBILISER_FIELD"]      = "Yes";
            break;

         // Factory Fitted Alarm
         case 'FA' :
               $PostArray["ALARM_FIELD"]            = "Yes";
               $PostArray["IMMOBILISER_FIELD"]      = "No";
            break;

         // Factory Fitted Immobiliser
         case 'FI' :
               $PostArray["ALARM_FIELD"]            = "No";
               $PostArray["IMMOBILISER_FIELD"]      = "Yes";
            break;

         // Factory Fitted Alarm/Immobiliser
         case 'FAI' :
               $PostArray["ALARM_FIELD"]            = "Yes";
               $PostArray["IMMOBILISER_FIELD"]      = "Yes";
            break;

         // Other Alarm
         case 'OA' :
               $PostArray["ALARM_FIELD"]            = "Yes";
               $PostArray["IMMOBILISER_FIELD"]      = "No";
            break;

         // Other Immobiliser
         case 'OI' :
               $PostArray["ALARM_FIELD"]            = "No";
               $PostArray["IMMOBILISER_FIELD"]      = "Yes";
            break;

         // Other Alarm/Immobiliser
         case 'OAI' :
               $PostArray["ALARM_FIELD"]            = "Yes";
               $PostArray["IMMOBILISER_FIELD"]      = "Yes";
            break;

         // None
         case 'N' :
               $PostArray["ALARM_FIELD"]            = "No";
               $PostArray["IMMOBILISER_FIELD"]      = "No";
            break;
      }

      if($Session["_VEHICLE_"]["vehicle_tracking_device"] == "N")
         $PostArray["TRACKER_FIELD"]                = "No";

      if($Session["_VEHICLE_"]["vehicle_tracking_device"] != "N")
         $PostArray["TRACKER_FIELD"]                = "Yes";

      $PostArray["PURCHASED_FIELD"]                 = "Yes";
      $vehBought = explode("/",$Session["_VEHICLE_"]["vehicle_bought"]);
      $PostArray["PURCHASE_DATE_FIELD"]             = $vehBought[1]."/".$vehBought[0]."/".$vehBought[2];
      $PostArray["ESTIMATED_VALUE_FIELD"]           = $Session["_VEHICLE_"]["estimated_value"];
      $PostArray["REGISTRATION_NUMBER_FIELD"]       = strtoupper($Session["_VEHICLE_"]["vehicle_registration_number"]);

      if($Session["_COVER_"]["protect_bonus"] == "N")
         $PostArray["PROTECTED_NCB_FIELD"]          = "No";

      if($Session["_COVER_"]["protect_bonus"] == "Y")
         $PostArray["PROTECTED_NCB_FIELD"]          = "Yes";

      $PostArray["MOBILE_NUMBER_FIELD"]             = $Session["_DRIVERS_"][0]["mobile_telephone_prefix"]."".$Session["_DRIVERS_"][0]["mobile_telephone_number"];

      if(! $cheapeastQuotePrice)
         $cheapeastQuotePrice = "0";

      if($cheapeastQuotePrice == "0")
      {
         $PostArray["emv_tag"]          = "800052848AF01D80";
         $PostArray["emv_ref"]          = "EdX7CqkmjzyW8SA9MKJPUB7fKEx6Ga_L_jCsDak3LMjZKz4";
      }

      $PostArray["PREMIUM_FIELD"]                   = $cheapeastQuotePrice;

      if($Session["_DRIVERS_"][0]["update_news"] == "on")
         $PostArray["OPT_IN_FIELD"]                 = "Yes";
      else
         $PostArray["OPT_IN_FIELD"]                 = "No";

      $PostArray["QUOTE_COMPLETED_FIELD"]           = "Yes";
      $PostArray["QUOTE_REF_FIELD"]                 = $Session["_QZ_QUOTE_DETAILS_"]["quote_reference"];

      //print "<!-- <pre>";
      //print_r($PostArray);
      //print "</pre> -->";























 // NEW FIELDS

      // set defaults
      // company name for top quote, this will be used to retrieve an image, so lower case and no spaces please
      $PostArray["COMPANY_FIELD"]          = '';

      // company name for 2nd quote, same as above
      $PostArray["COMPANY_2_FIELD"]        = '';

      // company name for 3rd quote, same as above
      $PostArray["COMPANY_3_FIELD"]        = '';

      // Annual premium of 2nd quote
      $PostArray["PREMIUM_2_FIELD"]        = '';

      // Annual premium of 3rd quote
      $PostArray["PREMIUM_3_FIELD"]        = '';

      // if top quote has "courtesy car" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_A_FIELD"]   = '';

      // if 2nd quote has "courtesy car" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_A_2_FIELD"] = '';

      // if 3rd quote has "courtesy car" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_A_3_FIELD"] = '';

      // if top quote has "windscreen cover" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_B_FIELD"]   = '';

      // if 2nd quote has "windscreen cover" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_B_2_FIELD"] = '';

      // if 3rd quote has "windscreen cover" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_B_3_FIELD"] = '';

      // if top quote has "personal accident" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_C_FIELD"]   = '';

      // if 2nd quote has "personal accident" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_C_2_FIELD"] = '';

      // if 3rd quote has "personal accident" included - "yes","no" or "extra"
      $PostArray["QUOTE_DETAIL_C_3_FIELD"] = '';

      // buyOnline link for top quote
      $PostArray["QUOTE_DETAIL_D_FIELD"]   = '';

      // buyOnline link for 2nd quote
      $PostArray["QUOTE_DETAIL_D_2_FIELD"] = '';

      // buyOnline link for 3rd quote
      $PostArray["QUOTE_DETAIL_D_3_FIELD"] = '';


      $topQuoteArray = array();

      // get top quotes
      if( $topQuoteArray = $this->objQuoteDetails->GetCheapestTopFiveSitesQuoteDetails($logID, 3))
      {
         $_SESSION                = $Session;
         $cheappestSiteQuoteArray = array();

         for($k=1;$k<=3;$k++)
         {
            $topPremium          = '';
            $topSiteDetailsArray = array();
            $_resSite            = array();
            $courtesy            = '';
            $windscreen          = '';
            $accident            = '';

            $topSiteID           = $topQuoteArray[$k];
            $topSiteDetailsArray = $this->objQuoteDetails->GetCheapestSiteQuoteDetails($logID,$topSiteID);

            if($k == 1)
            {

               // get company name
               $siteFileName = $Session["SHOW_SITES"][$topSiteID];
               include_once "/home/www/quotezone.co.uk/insurance-new/bike/sites/showsites/$siteFileName";
               // RAMDRIVE
               //include_once "../bike/sites/showsites/$siteFileName";

               //$PostArray["COMPANY_FIELD"] = strtolower(str_replace(" ","",$_resSite['brokerFullName']));
               $PostArray["COMPANY_FIELD"] = "https://bike-insurance.quotezone.co.uk/bike/".$_resSite['imageSiteRemote'];

               // if top quote has "courtesy van" included - "yes","no" or "extra"
               if(preg_match("/not_included/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $courtesy = "no";
               elseif(preg_match("/included/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $courtesy = "yes";
               elseif(preg_match("/plus/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $courtesy = "extra";
               elseif(preg_match("/ask/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $courtesy = "check";

               $PostArray["QUOTE_DETAIL_A_FIELD"]   = $courtesy;

               //if top quote has "windscreen cover" included - "yes","no" or "extra"
               if(preg_match("/not_included/i",$_resSite['_HELMET_AND_LEATHERS_']))
                  $windscreen = "no";
               elseif(preg_match("/included/i",$_resSite['_HELMET_AND_LEATHERS_']))
                  $windscreen = "yes";
               elseif(preg_match("/plus/i",$_resSite['_HELMET_AND_LEATHERS_']))
                  $windscreen = "extra";
               elseif(preg_match("/ask/i",$_resSite['_HELMET_AND_LEATHERS_']))
                  $windscreen = "check";

               $PostArray["QUOTE_DETAIL_B_FIELD"]   = $windscreen;

               //if top quote has "personal accident" included - "yes","no" or "extra"
               if(preg_match("/not_included/i",$_resSite['_PERSONAL_ACCIDENT_']))
                  $accident = "no";
               elseif(preg_match("/included/i",$_resSite['_PERSONAL_ACCIDENT_']))
                  $accident = "yes";
               elseif(preg_match("/plus/i",$_resSite['_PERSONAL_ACCIDENT_']))
                  $accident = "extra";
               elseif(preg_match("/ask/i",$_resSite['_PERSONAL_ACCIDENT_']))
                  $accident = "check";

               $PostArray["QUOTE_DETAIL_C_FIELD"]   = $accident;

               // buy online link
               $buyOnlineLink = '';
               if($_resSite['proceedBuyOnlineLink'])
                  $buyOnlineLink = $_resSite['proceedBuyOnlineLink'];
               elseif($_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0])
                  $buyOnlineLink = $_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0];

               if($buyOnlineLink)
               {
                  $PostArray["QUOTE_DETAIL_D_FIELD"]   = str_replace("sites/clickIn.php?","",$buyOnlineLink);

                  if(preg_match("/quote_ref\.value/i",$PostArray["QUOTE_DETAIL_D_FIELD"]))
                  {
                     // extract quote_ref from out file
                     include_once("/home/www/quotezone.co.uk/insurance-new/bike/steps/functions.php");

                     // RAMDRIVE
                     //include_once("../bike/steps/functions.php");
                     if($openResultFile = OpenResultFile($topSiteID))
                     {
                        $quote_ref = $openResultFile[0]["quote reference"];

                        if($quote_ref)
                           $PostArray["QUOTE_DETAIL_D_FIELD"] = str_replace("quote_ref.value",$quote_ref,$PostArray["QUOTE_DETAIL_D_FIELD"]);
                     }
                  }
               }


            }

            if($k == 2)
            {
               // get company name
               $siteFileName = $Session["SHOW_SITES"][$topSiteID];
               include_once "/home/www/quotezone.co.uk/insurance-new/bike/sites/showsites/$siteFileName";

               // RAMDRIVE
               //include_once "../bike/sites/showsites/$siteFileName";
 
               // company premium
               $topPremium  = $topSiteDetailsArray["cheapest_quote"];

               //$PostArray["COMPANY_2_FIELD"] = strtolower(str_replace(" ","",$_resSite['brokerFullName']));
               $PostArray["COMPANY_2_FIELD"] = "https://bike-insurance.quotezone.co.uk/bike/".$_resSite['imageSiteRemote'];

               $PostArray["PREMIUM_2_FIELD"] = $topPremium;

              // if top quote has "courtesy van" included - "yes","no" or "extra"
               if(preg_match("/not_included/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $courtesy = "no";
               elseif(preg_match("/included/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $courtesy = "yes";
               elseif(preg_match("/plus/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $courtesy = "extra";
               elseif(preg_match("/ask/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $courtesy = "check";

               $PostArray["QUOTE_DETAIL_A_2_FIELD"]   = $courtesy;

               //if top quote has "windscreen cover" included - "yes","no" or "extra"
               if(preg_match("/not_included/i",$_resSite['_HELMET_AND_LEATHERS_']))
                  $windscreen = "no";
               elseif(preg_match("/included/i",$_resSite['_HELMET_AND_LEATHERS_']))
                  $windscreen = "yes";
               elseif(preg_match("/plus/i",$_resSite['_HELMET_AND_LEATHERS_']))
                  $windscreen = "extra";
               elseif(preg_match("/ask/i",$_resSite['_HELMET_AND_LEATHERS_']))
                  $windscreen = "check";

               $PostArray["QUOTE_DETAIL_B_2_FIELD"]   = $windscreen;

               //if top quote has "personal accident" included - "yes","no" or "extra"
               if(preg_match("/not_included/i",$_resSite['_PERSONAL_ACCIDENT_']))
                  $accident = "no";
               elseif(preg_match("/not_included/i",$_resSite['_PERSONAL_ACCIDENT_']))
                  $accident = "yes";
               elseif(preg_match("/plus/i",$_resSite['_PERSONAL_ACCIDENT_']))
                  $accident = "extra";
               elseif(preg_match("/ask/i",$_resSite['_PERSONAL_ACCIDENT_']))
                  $accident = "check";

               $PostArray["QUOTE_DETAIL_C_2_FIELD"]   = $accident;

               // buy online link
               $buyOnlineLink = '';
               if($_resSite['proceedBuyOnlineLink'])
                  $buyOnlineLink = $_resSite['proceedBuyOnlineLink'];
               elseif($_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0])
                  $buyOnlineLink = $_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0];


               if($buyOnlineLink)
               {
                  $PostArray["QUOTE_DETAIL_D_2_FIELD"]   = str_replace("sites/clickIn.php?","",$buyOnlineLink);

                  if(preg_match("/quote_ref\.value/i",$PostArray["QUOTE_DETAIL_D_2_FIELD"]))
                  {
                     // extract quote_ref from out file
                     include_once("/home/www/quotezone.co.uk/insurance-new/bike/steps/functions.php");

                     // RAMDRIVE
                     //include_once("../bike/steps/functions.php");
                     if($openResultFile = OpenResultFile($topSiteID))
                     {
                        $quote_ref = $openResultFile[0]["quote reference"];

                        if($quote_ref)
                           $PostArray["QUOTE_DETAIL_D_2_FIELD"] = str_replace("quote_ref.value",$quote_ref,$PostArray["QUOTE_DETAIL_D_2_FIELD"]);
                     }
                  }
               }

            }

            if($k == 3)
            {
               // get company name
               $siteFileName = $Session["SHOW_SITES"][$topSiteID];
               include_once "/home/www/quotezone.co.uk/insurance-new/bike/sites/showsites/$siteFileName";

               // RAMDRIVE
               //include_once "../bike/sites/showsites/$siteFileName";
 
               // company premium
               $topPremium  = $topSiteDetailsArray["cheapest_quote"];

               //$PostArray["COMPANY_3_FIELD"] = strtolower(str_replace(" ","",$_resSite['brokerFullName']));
               $PostArray["COMPANY_3_FIELD"] = "https://bike-insurance.quotezone.co.uk/bike/".$_resSite['imageSiteRemote'];

               $PostArray["PREMIUM_3_FIELD"] = $topPremium;

              // if top quote has "courtesy van" included - "yes","no" or "extra"
               if(preg_match("/not_included/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $courtesy = "no";
               elseif(preg_match("/included/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $courtesy = "yes";
               elseif(preg_match("/plus/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $courtesy = "extra";
               elseif(preg_match("/ask/i",$_resSite['_LEGAL_ASSISTANCE_']))
                  $courtesy = "check";

               $PostArray["QUOTE_DETAIL_A_3_FIELD"]   = $courtesy;

               //if top quote has "windscreen cover" included - "yes","no" or "extra"
               if(preg_match("/not_included/i",$_resSite['_HELMET_AND_LEATHERS_']))
                  $windscreen = "no";
               elseif(preg_match("/included/i",$_resSite['_HELMET_AND_LEATHERS_']))
                  $windscreen = "yes";
               elseif(preg_match("/plus/i",$_resSite['_HELMET_AND_LEATHERS_']))
                  $windscreen = "extra";
               elseif(preg_match("/ask/i",$_resSite['_HELMET_AND_LEATHERS_']))
                  $windscreen = "check";

               $PostArray["QUOTE_DETAIL_B_3_FIELD"]   = $windscreen;

               //if top quote has "personal accident" included - "yes","no" or "extra"
               if(preg_match("/not_included/i",$_resSite['_PERSONAL_ACCIDENT_']))
                  $accident = "no";
               elseif(preg_match("/not_included/i",$_resSite['_PERSONAL_ACCIDENT_']))
                  $accident = "yes";
               elseif(preg_match("/plus/i",$_resSite['_PERSONAL_ACCIDENT_']))
                  $accident = "extra";
               elseif(preg_match("/ask/i",$_resSite['_PERSONAL_ACCIDENT_']))
                  $accident = "check";

               $PostArray["QUOTE_DETAIL_C_3_FIELD"]   = $accident;

               // buy online link
               $buyOnlineLink = '';
               if($_resSite['proceedBuyOnlineLink'])
                  $buyOnlineLink = $_resSite['proceedBuyOnlineLink'];
               elseif($_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0])
                  $buyOnlineLink = $_resSite['_EXTRA_PARAMS_']['buyOnline']['proceedBuyOnlineLink'][0];

               if($buyOnlineLink)
               {
                  $PostArray["QUOTE_DETAIL_D_3_FIELD"]   = str_replace("sites/clickIn.php?","",$buyOnlineLink);

                  if(preg_match("/quote_ref\.value/i",$PostArray["QUOTE_DETAIL_D_3_FIELD"]))
                  {
                     // extract quote_ref from out file
                     include_once("/home/www/quotezone.co.uk/insurance-new/bike/steps/functions.php");

                     // RAMDRIVE
                     //include_once("../bike/steps/functions.php");
                     if($openResultFile = OpenResultFile($topSiteID))
                     {
                        $quote_ref = $openResultFile[0]["quote reference"];

                        if($quote_ref)
                           $PostArray["QUOTE_DETAIL_D_3_FIELD"] = str_replace("quote_ref.value",$quote_ref,$PostArray["QUOTE_DETAIL_D_3_FIELD"]);
                     }
                  }
               }


            }
         }//for

      }// if get top quotes

      // NEW FIELDS 

      $string = "";


      foreach($PostArray as $key=>$val)
      {
         $string .= $key."=".urlencode($val)."&";
      }

      $string = rtrim($string, "&");

      fwrite($fh,"\n\nString is :  $string  \n\n");
      //if ($wlUserID == 604)
      //{
      //   mail('acrux@ymail.com', 'WL3 EMV BIKE DEBUG STRING', $string);
      //}
      
      //$fh = fopen("/home/www/quotezone.co.uk/insurance-new/bike/BikeEmailOutbounds.log","a+");
      //fwrite($fh,"String is :  $string  \n");
      //fclose($fh);

      return $string;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: SendUserDetails
   //
   // [DESCRIPTION]:   Form user data details to be sent it
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SendUserDetails()
   {
      $fh = fopen("/home/www/quotezone.co.uk/insurance-new/bike/BikeEmailOutbounds.log","a+");

      if(! $Session = $this->LoadSessionFile($this->fileName))
      {
         fwrite($fh, "FAILURE load session: $this->fileName ...\n");
         fclose($fh);
         $this->strERR = GetErrorString('CANT_GET_FILENAME_CONTENT');
         return false;
      }

      $logID       = $Session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];
      $wlUserID    = $Session["_QZ_QUOTE_DETAILS_"]["wlUserID"];
      $emailAddr   = $Session["_DRIVERS_"][0]["email_address"];
      $quoteUserID = $Session["_QZ_QUOTE_DETAILS_"]["quote_user_id"];

      // DO NOT SEND IF TEST QUOTE
      if(isset($Session["TESTING SITE ID"]))
         return;

      // MoneyMaxim
      //if($wlUserID == "136")
      //   return;
         
      if(! $this->LogWasSentOk($logID))
      {
         fwrite($fh, "POST ALLREADY SENT [$logID] file [$this->fileName]\n");
         fclose($fh);
         return false;
      }
         
      //Commented due to ticket id : XEV-793421
      /*
      $emailRule = $this->objEmailStore->GetEmailStoreRuleByEmailAndQuoteUserID($emailAddr,$quoteUserID);
      
      if($emailRule != '65535')
      {
         fwrite($fh, "Email address [$emailAddr] has rule [$emailRule] for file [$this->fileName] and log_id [$logID]\n");
         fwrite($fh, "Details wont be posted because the user has unsubscribed\n");
         fclose($fh);
         return false;
      }
      */

//      if(! $this->isNotWaitingQuotes($logID))
//      {
//         fwrite($fh, "QUOTE STILL WAITING: $this->fileName logID=$logID \n");
//         fclose($fh);
//         return false;
//      }

      if(! $this->objEmailOutbounds->AddEmailOutbounds($logID))
      {
         fwrite($fh, "CANNOT ADD  log_id [$logID] to database\n");
         fclose($fh);
         return false;
      }

      if(! $string = $this->FormatUserDetails($Session))
      {
         fwrite($fh, "FAILURE Formating user details for: $this->fileName logID=$logID \n");
         fclose($fh);
         return false;
      }

      if(! $response = $this->EmailOutboundsEngineSend($this->path, $string))
      {
         fwrite($fh, "FAILURE Send process for : $this->fileName logID=$logID \n");
         fclose($fh);
         return false;
      }

      if(! $this->objEmailOutbounds->SetEmailOutboundsStatus($logID, "1"))
      {
         fwrite($fh, "FAILURE Cannot set the email status : $this->fileName logID=$logID \n");
         fclose($fh);
         return false;
      }

      if(! $this->CheckEmailOutboundResponse($response))
      {
         fwrite($fh, "The post response was an error - please check the details to repost\n");
         fclose($fh);
         return false;
      }
      else
      {
         fwrite($fh, $response);
         fwrite($fh, "Done \n");
         fclose($fh);
   
         return true;
      }
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CheckEmailOutboundResponse
   //
   // [DESCRIPTION]:   checks the response from the server
   //
   // [PARAMETERS]:    $response=""
   //
   // [RETURN VALUE]:  true or false in case of failure
   //
   // [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CheckEmailOutboundResponse($response="")
   {

      if(empty($response))
      {
         $this->strERR = GetErrorString("INVALID_RESPONSE_FROM_EMAILVISION");
            return false;
      }

      if(! preg_match("/emailpostsuccessbike\.htm/isU",$response))
      {
         $this->strERR = GetErrorString("INVALID_RESPONSE_PREGMATCH");
            return false;
      }

      return true;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetError
   //
   // [DESCRIPTION]:   Retrieve the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetError()
   {
       return $this->strERR;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ShowError
   //
   // [DESCRIPTION]:   Print the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function ShowError()
   {
       return $this->strERR;
   }
}



?>

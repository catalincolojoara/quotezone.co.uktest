<?php
/*****************************************************************************/
/*                                                                           */
/*  CTemplate class interface                                                */
/*                                                                           */
/*  (C) 2004 Eugen Savin (eugen@claspsolutions.com)                          */
/*                                                                           */
/*****************************************************************************/

define("TEMPLATE_INCLUDED", "1");

//common files
include_once "errors.inc";
//include_once "globals.inc";

//if(DEBUG_MODE)
//   error_reporting(1);
//else
//   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CTemplate
//
// [DESCRIPTION]:  CTemplate class interface
//
// [FUNCTIONS]:    void Initialise();
//                 void SetRunMode($runMode = 0);
//                 int  GetRunMode();
//                 void SetFormDefValues($setDefValues = 0);
//                 bool Load($fileName="");
//                 void InitFormElements();
//                 bool Validate($fieldsArray = array(), $setFormValues = 1);
//                 void Prepare($keysArray = array());
//                 void Show();
//
//                 string MakeSelectBox(&$selBoxArray, $selBoxName="", $selectedKey="", $sortOptions=true);
//                 string MakeSelBoxOptions(&$selBoxArray, $selectedKey="", $sortOptions=true);
//
//                 GetError();
//
// [CREATED BY]:   Eugen SAVIN (eugen@claspsolutions.com) 2004-08-20
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CTemplate
{
    // class-internal variables
    var $runMode;         // run mode
    var $fileName;        // the file name we work with

    var $formElements;    // form elements related to this template
    var $prepArray;       // preparation array, change template values with these ones
    var $tmplDirectory;   // set templates directory where the templates should be
    var $tmplContent;     // template content
    var $xmlContent;      // xml config file

    var $setDefaultValues; // set default form elements value
    var $strERR;           // last TEMPLATE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CTemplate
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CTemplate()
{
   //if($fileName)
   //   $this->fileName = $fileName;

   $this->runMode          = 2;
   $this->fileName         = "";

   $this->formElements     = array();
   $this->prepArray        = array();

   $this->tmplContent      = "";
   $this->tmplDirectory    = TEMPLATES_DIR;
   $this->xmlContent       = "";

   $this->setDefaultValues = 0;
   $this->strERR           = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRunMode
//
// [DESCRIPTION]:   Return the current running mode
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  run mode as a number (int)
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetTemplateDirectory($path='')
{
   if(empty($path))
   {
      $this->tmplDirectory    = TEMPLATES_DIR;

      return true;
   }

   $this->tmplDirectory    = $path;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetRunMode
//
// [DESCRIPTION]:   Set the run mode for CTemplate
//                  Possible mode values are:
//                  0 - in this mode all keys are left untouched (debug mode)
//                  1 - in this mode undefined keys are left untouched (normal mode)
//                  2 - in this mode undefined keys are replaced with
//                      the null string: ""
//
// [PARAMETERS]:    - run mode
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-08-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetRunMode($runMode = 0)
{
   $this->runMode = $runMode;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRunMode
//
// [DESCRIPTION]:   Return the current running mode
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  run mode as a number (int)
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetRunMode()
{
   return $this->runMode;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetFormDefValues
//
// [DESCRIPTION]:   Set default form elements value
//
// [PARAMETERS]:    $setDefValues
//                  1 - sets element default values from the XML config file
//                  0 - do not set element default values from the XML config file
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetFormDefValues($setDefValues = 0)
{
   if($setDefValues != 0)
      $setDefValues = 1;

   $this->setDefaultValues = $setDefValues;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Load
//
// [DESCRIPTION]:   Load a template file
//
// [PARAMETERS]:    $fileName = ""
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Load($fileName="")
{
   $this->tmplContent  = "";
   $this->xmlContent   = "";
   $this->formElements = array();
   $this->prepArray    = array();

   if(empty($fileName))
      $fileName = $this->fileName;

   $fileName = $this->tmplDirectory."/".$fileName;

   if(! file_exists($fileName))
   {
      $this->strERR = GetErrorString("FILE_NOT_FOUND").": [$fileName]";
      return false;
   }

   if(! is_readable($fileName))
   {
      $this->strERR = GetErrorString("FILE_NOT_READABLE").": [$fileName]";
      return false;
   }

   $this->fileName = $fileName;

   $fh = fopen($fileName, "r");
   $this->tmplContent = fread($fh, filesize($fileName));
   fclose($fh);

   // read XML config file contents
   // $fileName = basename($fileName, ".tmpl");
   $filePath = pathinfo($fileName);
   $fileName = $filePath['dirname'].'/'.basename($filePath['basename'], '.tmpl').'.xml';

   if(! file_exists($fileName))
   {
      return true;
   }

   if(! is_readable($fileName))
   {
      return true;
   }

   $fh = fopen($fileName, "r");
   $this->xmlContent = fread($fh, filesize($fileName));
   fclose($fh);

   $parser = xml_parser_create();
   xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING, 0);
   xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE, 1);

   if(! xml_parse_into_struct($parser, $this->xmlContent, $values, $tags))
   {
      $this->strERR = xml_error_string(xml_get_error_code($parser));
      return false;
   }

   xml_parser_free($parser);

   //$index = 0;
   foreach($tags as $key => $val)
   {
      if($key == 'element')
      {
         for($i=0; $i<count($val); $i+=2)
         {
            $start = $val[$i] + 1;
            $end   = $val[$i+1];

            $elements = array();
            for($j=$start; $j<$end; $j++)
               $elements[$values[$j]['tag']] = $values[$j]['value'];

            //$this->formElements[$index++] = $elements;
            $this->formElements[$elements['name']] = $elements;
         }
      }
   }

   $this->InitFormElements();

   //print_r($this->formElements);
   return true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetElementsQuestions
//
// [DESCRIPTION]: Get elements questions from xml file tag name question and associate with param name
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  false | array
//
// [CREATED BY]:    COOLA (gabi@acrux.biz) 2006-07-05
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetElementsQuestions()
{
   if(empty($this->formElements))
      return false;

   $results = array();
   foreach($this->formElements as $paramName => $resParam)
      $results[$paramName] = $resParam['question'];

   return $results;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InitFormElements
//
// [DESCRIPTION]:   Initialise the form elements according to the XML config file.
//                  If $setDefaultValues flag is set, this will also set the default
//                  values for each form element defined within the XML config file.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InitFormElements()
{
   if(! $this->xmlContent)
      return;

   $prepArray = array();

   foreach($this->formElements as $key => $el)
   {
      switch($el['type'])
      {
         case 'input':
               $prepArray["$key.input_type"] = $el['input_type'];

               if($this->setDefaultValues)
               {
                  if($el['input_type'] == 'checkbox' || $el['input_type'] == 'radio')
                  {
                     if($el["checked"] == "yes")
                        $prepArray["$key.checked"] = "checked";
                     else if($el["checked"])
                        $prepArray["$key.checked"] = $el["checked"];
                  }

                  $prepArray["$key.value"] = $el['value'];
               }

               $prepArray["$key.size"]      = $el['size'];
               $prepArray["$key.maxlength"] = $el['maxlength'];

               if($el['readonly'] == 'yes')
                  $prepArray["$key.readonly"]   = "readonly";
               else if($el['readonly'] == 'no')
                  $prepArray["$key.readonly"]   = "";
               else
                  $prepArray["$key.readonly"]  = $el['readonly'];

               if($el['disable'] == 'yes')
                  $prepArray["$key.disable"]   = "disabled";
               else if($el['disable'] == 'no')
                  $prepArray["$key.disable"]   = "";
               else
                  $prepArray["$key.disable"]   = $el['disable'];

               $prepArray["$key.tabindex"]  = $el['tabindex'];
            break;

         case 'select':
               if($this->setDefaultValues)
                  $prepArray["$key.".$el['value'].".select"] = "selected";

               $prepArray["$key.size"]     = $el['size'];

               if($el['disable'] == 'yes')
                  $prepArray["$key.disable"]   = "disabled";
               else if($el['disable'] == 'no')
                  $prepArray["$key.disable"]   = "";
               else
                  $prepArray["$key.disable"]   = $el['disable'];

               $prepArray["$key.multiple"] = $el['multiple'];
               $prepArray["$key.tabindex"] = $el['tabindex'];
            break;

         case 'textarea':
               if($this->setDefaultValues)
                     $prepArray["$key.value"]  = $el['value'];

               $prepArray["$key.rows"] = $el['rows'];
               $prepArray["$key.cols"] = $el['cols'];

               if($el['readonly'] == 'yes')
                  $prepArray["$key.readonly"]   = "readonly";
               else if($el['readonly'] == 'no')
                  $prepArray["$key.readonly"]   = "";
               else
                  $prepArray["$key.readonly"]  = $el['readonly'];

               if($el['disable'] == 'yes')
                  $prepArray["$key.disable"]   = "disabled";
               else if($el['disable'] == 'no')
                  $prepArray["$key.disable"]   = "";
               else
                  $prepArray["$key.disable"]   = $el['disable'];

               $prepArray["$key.tabindex"] = $el['tabindex'];
            break;

         default:
            break;
      }
   }

   if(count($prepArray))
   {
      $runMode = $this->GetRunMode();
      $this->SetRunMode(1);
      $this->Prepare($prepArray);
      $this->SetRunMode($runMode);
   }

   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Validate
//
// [DESCRIPTION]:   Check this template for errors according to the XML config
//                  file
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Validate(&$fieldsArray, $setFormValues = 1)
{
   $retCode = true;

   if(! $this->xmlContent)
      return $retCode;

   foreach($fieldsArray as $key => $value)
   {
      if(array_key_exists($key, $this->formElements))
      {
         if(empty($value) && ($this->formElements[$key]['mandatory'] == 'no'))
            continue;

         $foundError = false;

         switch($this->formElements[$key]['type'])
         {
            case 'input':
               if(! preg_match("/^(".$this->formElements[$key]['regexp'].")$/s", $value))
              $foundError = true;

               if($setFormValues)
               {
                  if($this->formElements[$key]["input_type"] == 'checkbox' || $this->formElements[$key]["input_type"] == 'radio')
                  {
                     if($value)
                        $fieldsArray["$key.checked"] = "checked";
                     else
                        $value = $this->formElements[$key]["value"];
                  }

                $fieldsArray["$key.value"] = $value;
               }
               break;

            case 'select':
               if(! preg_match("/^(".$this->formElements[$key]['regexp'].")$/s", $value))
                  $foundError = true;

               if($setFormValues)
                  $fieldsArray["$key.$value.select"] = "selected";
               break;

            case 'textarea':
               if(! preg_match("/^(".$this->formElements[$key]['regexp'].")$/s", $value))
                  $foundError = true;

               if($setFormValues)
                     $fieldsArray["$key.value"] = $value;
               break;
            default:
               break;
         }

         // set error for this matching
         if($foundError)
         {
            // set error content
            $fieldsArray["$key.error"] = HTML_BEGIN_ERROR.GetErrorString($this->formElements[$key]['error_code']).HTML_END_ERROR;

            if(preg_match("/\_dd$/i",$key))
            {
               $key = preg_replace("/\_dd/i","",$key);
            }

            if(preg_match("/\_mm$/i",$key))
            {
               $key = preg_replace("/\_mm/i","",$key);
            }

            if(preg_match("/\_yyyy$/i",$key))
            {
               $key = preg_replace("/\_yyyy/i","",$key);
            }

            if(preg_match("/\_prefix$/i",$key))
            {
               $key = preg_replace("/\_prefix/i","",$key);
            }

            if(preg_match("/\_number$/i",$key))
            {
               $key = preg_replace("/\_number/i","",$key);
            }


            // set error border style and color
            $fieldsArray["$key.error_b"] = HTML_BORDER_ERROR;

            // set error background color
            $fieldsArray["$key.error_bg"] = HTML_BACKGROUND_ERROR;

            $retCode = false;
         }
      }
   }

   return $retCode;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Prepare
//
// [DESCRIPTION]:   Preparfe the template contents. Replace template vars
//                  with the values from the array param.
//
// [PARAMETERS]:    $prepArray
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Prepare($keysArray = array())
{
   if($this->GetRunMode() == 0)
      return;

   foreach($keysArray as $key => $value)
   {
      $key = preg_replace("/\//","\\/", $key);
      $this->tmplContent = preg_replace("/\[\%\s*$key\s*\%\]/", $value, $this->tmplContent);
   }

   foreach($this->prepArray as $key => $value)
   {
      $key = preg_replace("/\//","\\/", $key);
      $this->tmplContent = preg_replace("/\[\%\s*$key\s*\%\]/", $value, $this->tmplContent);
   }

   if($this->GetRunMode() == 2)
      $this->tmplContent = preg_replace("/\[\%.*\%\]/U", "", $this->tmplContent);

   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetContent
//
// [DESCRIPTION]:   Get template's content
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Template's content as a text string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetContent()
{
   return $this->tmplContent;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Show
//
// [DESCRIPTION]:   Show template content
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Show()
{
   print $this->tmplContent;
   flush();

   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MakeSelectBox
//
// [DESCRIPTION]:   Build a select box
//
// [PARAMETERS]:    $selectBoxArray=(), $selBoxName="", $selectedKey="", $sortOptions=true
//
// [RETURN VALUE]:  select box option contents as string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MakeSelectBox(&$selBoxArray, $selBoxName="", $selectedKey="", $sortOptions=true)
{
   $selectBoxText = "   <select name=\"$selBoxName\">\n";

   if($sortOptions)
   {
      asort($selBoxArray);
      reset($selBoxArray);
   }

   foreach($selBoxArray as $key => $value)
   {
      if($key == $selectedKey)
         $selected = "SELECTED";
      else
         $selected = "";

      $selectBoxText .= "      <option value=\"$key\" $selected>$value</option>\n";
   }

   $selectBoxText .= "   </select>\n";

   return $selectBoxText;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MakeSelectBox
//
// [DESCRIPTION]:   Build a select box
//
// [PARAMETERS]:    $selectBoxArray=(), $selBoxName="", $selectedKey="", $sortOptions=true
//
// [RETURN VALUE]:  select box option contents as string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MakeSelectBoxJS(&$selBoxArray, $selBoxName="", $selectedKey="", $sortOptions=true, $event="", $function="")
{
   $selectBoxText = "   <select $event=\"$function\" name=\"$selBoxName\">\n";

   if($sortOptions)
   {
      asort($selBoxArray);
      reset($selBoxArray);
   }

   foreach($selBoxArray as $key => $value)
   {
      if($key == $selectedKey)
         $selected = "SELECTED";
      else
         $selected = "";

      $selectBoxText .= "      <option value=\"$key\" $selected>$value</option>\n";
   }

   $selectBoxText .= "   </select>\n";

   return $selectBoxText;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MakeSelBoxOptions
//
// [DESCRIPTION]:   Build a select box option contents
//
// [PARAMETERS]:    $selectBoxArray=(), $selectedKey="", $sortOptions=true
//
// [RETURN VALUE]:  select box option contents as string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MakeSelBoxOptions(&$selBoxArray, $selectedKey="", $sortOptions=true)
{
   $selBoxOptions = "";

   if($sortOptions)
   {
      asort($selBoxArray);
      reset($selBoxArray);
   }

   foreach($selBoxArray as $key => $value)
   {

      if(strval($key) === strval($selectedKey))
         $selected = "SELECTED";
      else
         $selected = "";

      $selBoxOptions .= "   <option value=\"$key\" $selected>$value</option>\n";
   }

   return $selBoxOptions;
}

//////////////////////////////////////////////////////////////////////////////FB
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

}// end of CTemplate class

?>

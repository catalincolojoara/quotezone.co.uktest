<?php
/*****************************************************************************/
/*                                                                           */
/*  MySQL Function Wrapper Class                                             */
/*                                                                           */
/*  (C) 2003 Eugen Savin (seugen@abc.ro)                                     */
/*                                                                           */
/*****************************************************************************/

//if($_SERVER['REMOTE_ADDR'] == '86.125.114.56')
//{   print("---");
//}

define("MYSQL_INCLUDED", "1");
define("MYSQL_DEBUG", "0");

if(! defined("USE_MYSQL_HOST"))
   DEFINE("USE_MYSQL_HOST", false);

if(! defined("IS_SKELETON"))
   DEFINE("IS_SKELETON", false);

if(! defined("USE_MYSQL_STATS_HOST"))
   DEFINE("USE_MYSQL_STATS_HOST", false);

if(! defined("USE_MYSQL_STATS_2_HOST"))
   DEFINE("USE_MYSQL_STATS_2_HOST", false);

if(DEBUG_MODE)
  error_reporting(1);
else
  error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CMySQL
//
// [DESCRIPTION]:  Database interface for the MySQL server, PHP version
//
// [FUNCTIONS]:    Open($dbname="",$host="",$user="",$pass="",$pcon=0);
//                 OpenOnDemand($dbname="",$host="",$user="",$pass="",$pcon=0, $connectToMaster=false)
//                 Close();
//                 GetLastCmd();
//                 GetError();
//                 GetErrorNum();
//                 Exec($sqlCMD="");
//                 GetRows();
//                 GetAffectedRows();
//                 FetchRows();
//                 FetchRowsA();
//                 GetFieldValue($fieldNAME="");
//                 GetFieldNr($fieldINDEX=0);
//                 GetFieldNames($tableNAME="")
//                 MoveTo($rowNUM=0);
//                 MoveFirst();
//                 MoveLast();
//                 MoveNext();
//                 BeginTrans();
//                 Commit();
//                 RollBack();
//                 Free();
//
// [CREATED BY]:   Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CMySQL
{
    // default database configuration
    var $host;         // host name
    var $user;         // user name
    var $pass;         // password
    var $dbname;       // database name
    var $pcon;         //persistent connection

    var $masterHost;   // master host name
    var $masterUser;   // master user name
    var $masterPass;   // master password
    var $masterDbname; // master database name

    // class-internal variables
    var $linkID;       // mysql link id - slave/local db usually
    var $masterLinkID; // mysql link id - master db

    var $strERR;       // last mysql error string
    var $lastSQLCMD;   // last mysql command/query
    var $lastRESULT;   // last mysql query result
    var $lastROW;      // last row fetched

    var $alternativeSlaveServers; //alternative local servers array
    var $lostConnectionCounter;//lost connection counter
    var $databaseNotFoundCounter;//database not found counter
    var $reconnectingErrorNumbers;// reconnecting error numbers


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CMySQL
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CMySQL()
{
   // default database configuration
   $this->host         = DBHOST;
   $this->dbname       = DBNAME;
   $this->user         = DBUSER;
   $this->pass         = DBPASS;
   $this->pcon         = 0;

   // master database configuration
   $this->masterHost   = MASTER_DBHOST;
   $this->masterDbname = MASTER_DBNAME;
   $this->masterUser   = MASTER_DBUSER;
   $this->masterPass   = MASTER_DBPASS;

   if(! USE_MYSQL_HOST)
   {
      /////////// overwrite hosts
      $this->host         = ":/var/lib/mysql/mysql-slave-proxy.sock";
      $this->masterHost   = ":/var/lib/mysql/mysql-master-proxy.sock";
      /////////// end overwriting 
   }

   if(USE_MYSQL_STATS_HOST)
   {
      /////////// overwrite hosts
      $this->host         = ":/var/lib/mysql/mysql-stats-proxy.sock";
      $this->masterHost   = ":/var/lib/mysql/mysql-master-proxy.sock";
      /////////// end overwriting 
   }

   if(USE_MYSQL_STATS_2_HOST)
   {
      /////////// overwrite hosts
      $this->host         = ":/var/lib/mysql/mysql-stats2-proxy.sock";
      $this->masterHost   = ":/var/lib/mysql/mysql-master-proxy.sock";
      /////////// end overwriting 
   }

   //set alternative master servers parameters - use same login details as the primary master
   $this->alternativeMasterServers[1]['host']   = '10.2.12.19';
   $this->alternativeMasterServers[1]['dbname'] = DBNAME;
   $this->alternativeMasterServers[1]['user']   = DBUSER;
   $this->alternativeMasterServers[1]['pass']   = DBPASS;

   //set alternative slave servers parameters - use same login details as the local slave
   $this->alternativeSlaveServers[1]['host']   = 'localhost';
   $this->alternativeSlaveServers[1]['dbname'] = DBNAME;
   $this->alternativeSlaveServers[1]['user']   = DBUSER;
   $this->alternativeSlaveServers[1]['pass']   = DBPASS;

   $this->alternativeSlaveServers[2]['host']   = '10.2.12.19';
   $this->alternativeSlaveServers[2]['dbname'] = DBNAME;
   $this->alternativeSlaveServers[2]['user']   = DBUSER;
   $this->alternativeSlaveServers[2]['pass']   = DBPASS;


   //set alternative Slave for .17 Skeleton
   if(IS_SKELETON || (isset($_SERVER['HOSTNAME']) && trim($_SERVER['HOSTNAME']) == 'ip-10-2-12-17.eu-west-1.compute.internal'))
   {
      //set alternative slave servers parameters - use same login details as the local slave
      $this->alternativeSlaveServers[1]['host']   = '10.2.13.20';
      $this->alternativeSlaveServers[1]['dbname'] = DBNAME;
      $this->alternativeSlaveServers[1]['user']   = DBUSER;
      $this->alternativeSlaveServers[1]['pass']   = DBPASS;
   }


   // in testing mode, both master and local/slave db are set according to the test configuration definition
   if(defined('TESTING_MODE'))
   {
      if(TESTING_MODE)
      {
         // default database configuration
         $this->host            = TEST_DBHOST;
         $this->dbname          = TEST_DBNAME;
         $this->user            = TEST_DBUSER;
         $this->pass            = TEST_DBPASS;

         // master database configuration
         $this->masterHost      = TEST_DBHOST;
         $this->masterDbname    = TEST_DBNAME;
         $this->masterUser      = TEST_DBUSER;
         $this->masterPass      = TEST_DBPASS;
      }
   }

   if(defined('SIMULATION_MODE'))
   {
      if(SIMULATION_MODE)
      {
         // default database configuration
         $this->host            = DBHOST;
         $this->dbname          = DBNAME;
         $this->user            = DBUSER;
         $this->pass            = DBPASS;

         // master database configuration
         $this->masterHost      = DBHOST;
         $this->masterDbname    = DBNAME;
         $this->masterUser      = DBUSER;
         $this->masterPass      = DBPASS;

      }
   }
   // class-internal variables
   $this->linkID       = 0;
   $this->masterLinkID = 0;

   $this->strERR       = "";
   $this->lastSQLCMD   = "";
   $this->lastRESULT   = "";
   $this->lastROW      = "";

   $this->lostConnectionCounter   = 0;//lost connection counter
   $this->databaseNotFoundCounter = 0;//database not found counter

   $this->reconnectingErrorNumbers[] = '1205';
   $this->reconnectingErrorNumbers[] = '1213';
   $this->reconnectingErrorNumbers[] = '2013';
   $this->reconnectingErrorNumbers[] = '2006';


   $this->strERR       = "No connection";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Open
//
// [DESCRIPTION]:   Open a connection with the MySQL database server
//
// [PARAMETERS]:    dbname, host, user, pass, [pcon]
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Open($dbname="",$host="",$user="",$pass="",$pcon=0)
{
    // everything OK
    $this->strERR = "";

    // return with success
    return 1;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: OpenOnDemand
//
// [DESCRIPTION]:   Open a connection with the MySQL database server
//
// [PARAMETERS]:    dbname, host, user, pass, [pcon], [connectToMaster]
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function OpenOnDemand($dbname="",$host="",$user="",$pass="",$pcon=0, $connectToMaster=false)
{
   // force persistent connections
   $pcon = 0;

   // connect to the master DB server if we have it defined
   if(! empty($this->masterDbname) && $connectToMaster)
   {
      // open persistent or normal connection
      if($pcon)
         $this->masterLinkID = mysql_pconnect($this->masterHost, $this->masterUser, $this->masterPass);
      else
         $this->masterLinkID = mysql_connect($this->masterHost, $this->masterUser, $this->masterPass, true);

      // connection to mysql server failed
      if(! $this->masterLinkID)
      {
            $date = date("Y-m-d H:i:s");
            $fd = fopen("/tmp/mysql.log", "a+");
            fwrite($fd, "$this->masterHost,$this->masterDbname,$this->masterUser,$this->masterPass");
            fclose($fd);

         //trying to connect to an alternative server
         foreach($this->alternativeMasterServers as $index => $resLoginDetails)
         {
            $this->masterHost   = $resLoginDetails['host'];
            $this->masterDbname = $resLoginDetails['dbname'];
            $this->masterUser   = $resLoginDetails['user'];
            $this->masterPass   = $resLoginDetails['pass'];

            $date = date("Y-m-d H:i:s");
            $fd = fopen("/tmp/mysql.log", "a+");
            fwrite($fd, "$this->masterHost,$this->masterDbname,$this->masterUser,$this->masterPass");
            fwrite($fd, "$date falling back to ".$this->masterHost."\n");
            fclose($fd);

            if($pcon)
               $this->masterLinkID = mysql_pconnect($this->masterHost, $this->masterUser, $this->masterPass);
            else
               $this->masterLinkID = mysql_connect($this->masterHost, $this->masterUser, $this->masterPass, true);

            if($this->masterLinkID)
               break;
         }

         if(! $this->masterLinkID)
         {
            $currentPath = getcwd();

            $this->strERR  = getmypid()." ";
            $this->strERR .= $argv[0]." ";
            $this->strERR .= "MASTER Path : $currentPath => ";
            $this->strERR .= $pcon ? "persistent " : "";
            $this->strERR .= "connect failed ('{$this->masterUser}@{$this->masterHost}')";

            $date = date("Y-m-d H:i:s");
            $fd = fopen("/tmp/mysql.log", "a+");
            fwrite($fd, "--------------------------------------------\n");
            fwrite($fd, $date." ".$this->strERR."\n");
            fwrite($fd, "--------------------------------------------\n");
            ob_start();
            system("ps auxww");
            $processList = ob_get_contents();
            ob_end_clean();
            fwrite($fd, $processList."\n");
            fclose($fd);

   	    return 0; // error
         }
      }

      // select the specified database
      $result = mysql_select_db($this->masterDbname, $this->masterLinkID);

      if(! $result)
      {
         $this->strERR = "database not found ('{$this->masterDbname}')";

         $date = date("Y-m-d H:i:s");
         $fd = fopen("/tmp/mysql.log", "a+");
         fwrite($fd, "--------------------------------------------\n");
         fwrite($fd, $date." ".$this->strERR."\n");
         fwrite($fd, "--------------------------------------------\n");
         ob_start();
         system("ps auxww");
         $processList = ob_get_contents();
         ob_end_clean();
         fwrite($fd, $processList."\n");
         fclose($fd);

         $this->databaseNotFoundCounter = 0;

         $alternativeMasterServers = array();

         while(! ($result = mysql_select_db($this->masterDbname, $this->masterLinkID)) && $this->databaseNotFoundCounter < 4)
         {
            $this->databaseNotFoundCounter++;

            if($this->databaseNotFoundCounter > 1)
            {

               if(empty($alternativeMasterServers))
                  $alternativeMasterServers = $this->alternativeMasterServers;

               //close the current connection
               mysql_close($this->masterLinkID);

               //trying to connect to an alternative server
               foreach($alternativeMasterServers as $index => $resLoginDetails)
               {
                  $this->masterHost   = $resLoginDetails['host'];
                  $this->masterDbname = $resLoginDetails['dbname'];
                  $this->masterUser   = $resLoginDetails['user'];
                  $this->masterPass   = $resLoginDetails['pass'];

                  $date = date("Y-m-d H:i:s");
                  $fd = fopen("/tmp/mysql.log", "a+");
                  fwrite($fd, "$date reselecting falling back to ".$this->masterHost."\n");
                  fclose($fd);

                  if($pcon)
                     $this->masterLinkID = mysql_pconnect($this->masterHost, $this->masterUser, $this->masterPass);
                  else
                     $this->masterLinkID = mysql_connect($this->masterHost, $this->masterUser, $this->masterPass, true);

                  if($this->masterLinkID)
                  {
                     //remove the connected server, in case it cant select db need to check some other server.
                     unset($alternativeMasterServers[$index]);
                     break;
                  }
               }
            }

            $date = date("Y-m-d H:i:s");
            $fd = fopen("/tmp/mysqle.log", "a+");
            fwrite($fd, "------------------------------------------------\n");
            fwrite($fd, $date." Reselecting DB Master #" . $this->databaseNotFoundCounter . " \n");
            fclose($fd);
         }

         if(! $result)
         {
            // db selection failed
            mysql_close($this->masterLinkID);

            return 0; // error
         }

         $this->databaseNotFoundCounter = 0;

      }
   }

   // only overwrite if not in TESTING MODE
   if(! TESTING_MODE)
   {
      // overwrite default database definitions
      if($dbname != "")
         $this->dbname = $dbname;
      else
         $dbname = $this->dbname;

      if($host != "")
         $this->host = $host;
      else
         $host = $this->host;

      if($user != "")
         $this->user = $user;
      else
         $user = $this->user;

      if($pass != "")
         $this->pass = $pass;
      else
         $pass = $this->pass;

      if($pcon != "")
         $this->pcon = $pcon;
      else
         $pcon = $this->pcon;

      if(! USE_MYSQL_HOST)
      {
         /////////// overwrite hosts
         $this->host         = ":/var/lib/mysql/mysql-slave-proxy.sock";
         $this->masterHost   = ":/var/lib/mysql/mysql-master-proxy.sock";
         /////////// end overwriting
      }

      if(USE_MYSQL_STATS_HOST)
      {
         /////////// overwrite hosts
         $this->host         = ":/var/lib/mysql/mysql-stats-proxy.sock";
         $this->masterHost   = ":/var/lib/mysql/mysql-master-proxy.sock";
         /////////// end overwriting
      }

      if(USE_MYSQL_STATS_2_HOST)
      {
         /////////// overwrite hosts
         $this->host         = ":/var/lib/mysql/mysql-stats2-proxy.sock";
         $this->masterHost   = ":/var/lib/mysql/mysql-master-proxy.sock";
         /////////// end overwriting
      }
   }

   $simulationMODE = false;
   if(defined('SIMULATION_MODE'))
   {
      $simulationMODE = true;
   }
   if($simulationMODE)
   {
      // overwrite default database definitions
      if($dbname != "")
         $this->dbname = $dbname;
      else
         $dbname = $this->dbname;

      if($host != "")
         $this->host = $host;
      else
         $host = $this->host;

      if($user != "")
         $this->user = $user;
      else
         $user = $this->user;

      if($pass != "")
         $this->pass = $pass;
      else
         $pass = $this->pass;

      if($pcon != "")
         $this->pcon = $pcon;
      else
         $pcon = $this->pcon;

      if(! USE_MYSQL_HOST)
      {
         /////////// overwrite hosts
         $this->host         = ":/var/lib/mysql/mysql-slave-proxy.sock";
         $this->masterHost   = ":/var/lib/mysql/mysql-master-proxy.sock";
         /////////// end overwriting
      }

      if(USE_MYSQL_STATS_HOST)
      {
         /////////// overwrite hosts
         $this->host         = ":/var/lib/mysql/mysql-stats-proxy.sock";
         $this->masterHost   = ":/var/lib/mysql/mysql-master-proxy.sock";
         /////////// end overwriting
      }

      if(USE_MYSQL_STATS_2_HOST)
      {
         /////////// overwrite hosts
         $this->host         = ":/var/lib/mysql/mysql-stats2-proxy.sock";
         $this->masterHost   = ":/var/lib/mysql/mysql-master-proxy.sock";
         /////////// end overwriting
      }
   }

    // open persistent or normal connection
    if($pcon)
        $this->linkID = mysql_pconnect($this->host, $this->user, $this->pass);
    else
        $this->linkID = mysql_connect($this->host, $this->user, $this->pass, true);

    // connection to mysql slave server failed
    if(! $this->linkID)
    {

      //trying to connect to an alternative server
      foreach($this->alternativeSlaveServers as $index => $resLoginDetails)
      {
         $this->host   = $resLoginDetails['host'];
         $this->dbname = $resLoginDetails['dbname'];
         $this->user   = $resLoginDetails['user'];
         $this->pass   = $resLoginDetails['pass'];

         $date = date("Y-m-d H:i:s");
         $fd = fopen("/tmp/mysql.log", "a+");
         fwrite($fd, "$date falling back to ".$this->host." " . $this->GetError() . "\n");
         //fwrite($fd, "$date falling back to ".$this->host." " . $this->GetError() . " " . var_export($this, true). "\n");
         fclose($fd);

         if($pcon)
            $this->linkID = mysql_pconnect($this->host, $this->user, $this->pass);
         else
            $this->linkID = mysql_connect($this->host, $this->user, $this->pass, true);

         if($this->linkID)
            break;
      }

      if(! $this->linkID)
      {
         $currentPath = getcwd();

         $this->strERR  = getmypid()." ";
         $this->strERR .= $argv[0]." ";
         $this->strERR .= "SLAVE Path : $currentPath => ";
         $this->strERR .= $pcon ? "persistent " : "";
         $this->strERR .= "connect failed ('$user:$pass@$host')";

         $date = date("Y-m-d H:i:s");
         $fd = fopen("/tmp/mysql.log", "a+");
         fwrite($fd, "--------------------------------------------\n");
         fwrite($fd, $date." ".$this->strERR."\n");
         fwrite($fd, "--------------------------------------------\n");
         ob_start();
         system("ps auxww");
         $processList = ob_get_contents();
         ob_end_clean();
         fwrite($fd, $processList."\n");
         fclose($fd);

         return 0; // error
      }
   }

    // select database
    $result = mysql_select_db($this->dbname, $this->linkID);

    if(! $result)
    {
        $this->strERR = "database not found ('$dbname')";

        $date = date("Y-m-d H:i:s");
        $fd = fopen("/tmp/mysql.log", "a+");
        fwrite($fd, "--------------------------------------------\n");
        fwrite($fd, $date." ".$this->strERR."\n");
        fwrite($fd, "--------------------------------------------\n");
        ob_start();
        system("ps auxww");
        $processList = ob_get_contents();
        ob_end_clean();
        fwrite($fd, $processList."\n");
        fclose($fd);

         $this->databaseNotFoundCounter = 0;

         $alternativeSlaveServers = array();

         while(! ($result = mysql_select_db($this->dbname, $this->linkID)) && $this->databaseNotFoundCounter < 4)
         {
            $this->databaseNotFoundCounter++;

            if($this->databaseNotFoundCounter > 1)
            {
               if(empty($alternativeSlaveServers))
                  $alternativeSlaveServers =  $this->alternativeSlaveServers;

               //close the current connection
               mysql_close($this->linkID);

               //trying to connect to an alternative server
               foreach($alternativeSlaveServers as $index => $resLoginDetails)
               {
                  $this->host   = $resLoginDetails['host'];
                  $this->dbname = $resLoginDetails['dbname'];
                  $this->user   = $resLoginDetails['user'];
                  $this->pass   = $resLoginDetails['pass'];

                  $date = date("Y-m-d H:i:s");
                  $fd = fopen("/tmp/mysql.log", "a+");
                  fwrite($fd, "$date reselecting falling back to ".$this->host." " . $this->GetError() . "\n");
                  //fwrite($fd, "$date falling back to ".$this->host." " . $this->GetError() . " " . var_export($this, true). "\n");
                  fclose($fd);

                  if($pcon)
                     $this->linkID = mysql_pconnect($this->host, $this->user, $this->pass);
                  else
                     $this->linkID = mysql_connect($this->host, $this->user, $this->pass, true);

                  if($this->linkID)
                  {
                     //remove the connected server, in case it cant select db need to check some other server.
                     unset($alternativeSlaveServers[$index]);
                     break;
                  }
               }
            }

            $date = date("Y-m-d H:i:s");
            $fd = fopen("/tmp/mysqle.log", "a+");
            fwrite($fd, "------------------------------------------------\n");
            fwrite($fd, $date." Reselecting DB Slave #" . $this->databaseNotFoundCounter . " \n");
            fclose($fd);
         }

         if(! $result)
         {
            // db selection failed
            mysql_close($this->linkID);

            return 0; // error
         }

         $this->databaseNotFoundCounter = 0;
    }

    // everything OK
    $this->strERR = "";

    // return with success
    return 1;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLastInsertedId
//
// [DESCRIPTION]:   Retrieve the last id inserted
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]: integer
//
// [CREATED BY]:    Ando Ciupav Cristian, SC Acrux Software SRL, Timisoara, 2010
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLastInsertedId()
{
    return mysql_insert_id($this->masterLinkID);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the connection with the database server
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
   $this->Free();

   if($this->linkID)
      mysql_close($this->linkID);

   if($this->masterLinkID)
      mysql_close($this->masterLinkID);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLastCmd
//
// [DESCRIPTION]:   Retrieve the last SQL command
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error number if there is any, 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLastCmd()
{
    return $this->lastSQLCMD;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError($linkID)
{
    //if(! empty($this->strERR))
    //   return $this->strERR;

    if(! $linkID)
       $linkID = $this->linkID;

    $errorMSG = mysql_error($linkID);
    $errorNUM = mysql_errno($linkID);

    if(!empty($errorMSG) && $errorNUM)
       $this->strERR = "$errorMSG ($errorNUM)";

    if(MYSQL_DEBUG)
       $this->strERR .= "\n".$this->lastSQLCMD;

    return $this->strERR;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetErrorNum
//
// [DESCRIPTION]:   Retrieve the last error number
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error number if there is any, 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetErrorNum($linkID)
{
    if(! $linkID)
       $linkID = $this->linkID;

    //return mysql_errno($this->linkID);
    return mysql_errno($linkID);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Exec
//
// [DESCRIPTION]:   Opens a database connection, and executes an SQL command , depends by type of sqlCMD : ^SELECT => from localhost(slave) or
//                  ^INSERT,UPDATE,DELETE => from master. In case we set linkMaster => all queries from master.
//
// [PARAMETERS]:    sqlCMD, [linkMaster]
//
// [RETURN VALUE]:  Data struct (non 0) if success, 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Exec($sqlCMD="", $linkMaster=false)
{
   $this->Free();

   $this->lastSQLCMD = $sqlCMD;

   if(! $this->linkID)
      $this->OpenOnDemand($this->dbname,$this->host,$this->user,$this->pass,$this->pcon);

   // default we have the local/slave linkID
   $linkID = $this->linkID;

   $sqlCMD = trim($sqlCMD);

   if(! preg_match("/^[\(|\s]*(SELECT|SET)/i",$sqlCMD))
   {
      if(! $this->masterLinkID)
   	   $this->OpenOnDemand($this->masterDbname,$this->masterHost,$this->masterUser,$this->masterPass,$this->pcon,true);

   	$linkID = $this->masterLinkID;
   }

   // last insert ID from master
   if(preg_match("/LAST_INSERT_ID/i",$sqlCMD))
   {
      if(! $this->masterLinkID)
   	   $this->OpenOnDemand($this->masterDbname,$this->masterHost,$this->masterUser,$this->masterPass,$this->pcon,true);

      $linkID = $this->masterLinkID;
   }

   // force query from master db
   if($linkMaster)
   {
   	//$fd = fopen("/tmp/mysqle.log", "a+");
   	//fwrite($fd, $date." Query [$sqlCMD] forced on master.\n");
   	//fclose($fd);

   	if(! $this->masterLinkID)
   	   $this->OpenOnDemand($this->masterDbname,$this->masterHost,$this->masterUser,$this->masterPass,$this->pcon,true);

      $linkID = $this->masterLinkID;
   }

// todo

   //echo "#############\nexecuting $sqlCMD\nusing link id: $linkID\nmaster link ID: {$this->masterLinkID}\n";
   $this->lastRESULT = mysql_query($sqlCMD, $linkID);

   //$this->LogQuery($sqlCMD);

   if(! $this->lastRESULT)
   {
      $this->strERR = "Command failed ('$sqlCMD')";

      $date = date("Y-m-d H:i:s");
      $fd = fopen("/tmp/mysqle.log", "a+");
      fwrite($fd, "---------------------Exec-----------------------\n");
      fwrite($fd, $date." ".$this->strERR."\n");
      fwrite($fd, $date." ".$this->GetError($linkID)."\n");
      fclose($fd);

      $errorNumber = $this->GetErrorNum($linkID);

      while(in_array($errorNumber, $this->reconnectingErrorNumbers) && $this->lostConnectionCounter < 4)
      {
         $this->lostConnectionCounter++;

         $this->Close();

         if($errorNumber == 1213)
         {
         	$retryDelay = 20*$this->lostConnectionCounter;
            sleep($retryDelay);
         }

         $fd = fopen("/tmp/mysqle.log", "a+");
         fwrite($fd, $date." [linkID] [this->linkID] [this->masterLinkID]\n");
         fwrite($fd, $date." [$linkID] [".$this->linkID."] [".$this->masterLinkID."]\n");

         if($linkID == $this->masterLinkID)
         {
         	$fd = fopen("/tmp/mysqle.log", "a+");
            fwrite($fd, $date." Try to open connection on master\n");
            fclose($fd);

            $this->OpenOnDemand($this->masterDbname,$this->masterHost,$this->masterUser,$this->masterPass,$this->pcon,true);
         }
         else
         {
         	$fd = fopen("/tmp/mysqle.log", "a+");
            fwrite($fd, $date." Try to open connection on slave\n");
            fclose($fd);

            $this->OpenOnDemand($this->dbname, $this->host, $this->user, $this->pass, $this->pcon);
         }

         $date = date("Y-m-d H:i:s");
         $fd = fopen("/tmp/mysqle.log", "a+");
         fwrite($fd, "---------------------Exec-----------------------\n");
         fwrite($fd, $date." Reconnecting #" . $this->lostConnectionCounter . " \n");
         fclose($fd);

         if($this->Exec($sqlCMD, $linkMaster))
            break;

      }

      $this->lostConnectionCounter = 0;
   }

   if(! $this->lastRESULT)
      return 0;

   return $this->lastRESULT;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRows
//
// [DESCRIPTION]:   The number of fetched rows, mostly use with the SELECT command
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Number of rows
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetRows()
{
    if(! $this->lastRESULT)
       return 0;

    return mysql_num_rows($this->lastRESULT);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffectedRows
//
// [DESCRIPTION]:   The number of affected rows, mostly use with the UPDATE,
//                  DELETE, INSERT, DROP commands
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Number of affected rows
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAffectedRows()
{
    if(! $this->linkID)
       return 0;

    return mysql_affected_rows($this->linkID);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: FetchRows
//
// [DESCRIPTION]:   Fetch the rows retrieved as a result of an Exec() operation
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  An associative array (hash) of the type key = value if success,
//                  0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function FetchRows()
{
    if($this->lastRESULT)
        $row = mysql_fetch_object($this->lastRESULT);
    else
        $row = 0;

    $this->lastROW = $row;
    return $row;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: FetchRowsA
//
// [DESCRIPTION]:   Fetch the rows retrieved as a result of an Exec() operation
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  An array (hash) containing the field values if success,
//                  0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function FetchRowsA()
{
    if ($this->lastRESULT)
        $row = mysql_fetch_array($this->lastRESULT);
    else
        $row = 0;

    $this->lastROW = $row;
    return $row;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFieldValue
//
// [DESCRIPTION]:   Retrieve the value of a field name after a FetchRows()
//                  operation has been launched against a result set
//
// [PARAMETERS]:    fieldNAME
//
// [RETURN VALUE]:  String containing the field value if success,
//                  empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFieldValue($fieldNAME="")
{
    if(! $this->lastROW)
       return "";

    return $this->lastROW->$fieldNAME;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFieldNr
//
// [DESCRIPTION]:   Retrieve the value of a field name after a FetchRowsA()
//                  operation has been launched against a result set
//
// [PARAMETERS]:    fieldINDEX
//
// [RETURN VALUE]:  String containing the field value if success,
//                  empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFieldNr($fieldINDEX=0)
{
    if(! $this->lastROW)
       return "";

    return $this->lastROW[$fieldINDEX];
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFieldNames
//
// [DESCRIPTION]:   Retrieve the column name of all fields from a sepcified table
//
// [PARAMETERS]:    $fieldNAME
//
// [RETURN VALUE]:  an array with fields names
//
// [CREATED BY]:    Gabi ISTVANCSEK (gabi@acrux.biz) 2006-22-02
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllFieldsName($tableNAME="")
{
   if($tableNAME == '')
      return;

   $fieldNames = array();

   $this->Exec("SHOW COLUMNS FROM $tableNAME");

   while($this->MoveNext())
   {
      array_push($fieldNames, $this->GetFieldValue("Field"));
   }

   return $fieldNames;

}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MoveTo
//
// [DESCRIPTION]:   Move the cursor position within the rows at a specified
//                  row number
//
// [PARAMETERS]:    rowNUM
//
// [RETURN VALUE]:  True if success, False otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MoveTo($rowNUM=0)
{
    if(! $this->lastRESULT)
       return FALSE;

    if($rowNUM < 0 || $rowNUM > $this->GetRows())
       return FALSE;

    return mysql_data_seek($this->lastRESULT, $rowNUM);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MoveFirst
//
// [DESCRIPTION]:   Move the cursor position to the first row entry
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, False otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MoveFirst()
{
    if(! $this->lastRESULT)
       return FALSE;

    if(! $this->MoveTo(0))
       return FALSE;

    return TRUE;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MoveLast
//
// [DESCRIPTION]:   Move the cursor position to the last row entry
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, False otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MoveLast()
{
    if(! $this->lastRESULT)
       return FALSE;

    if(! $this->MoveTo($this->GetRows()-1))
       return FALSE;

    return TRUE;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MoveNext
//
// [DESCRIPTION]:   Move the cursor position to the last next row entry
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, False otherwise or if there are no more rows
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MoveNext()
{
    if(! $this->lastRESULT)
       return FALSE;

    if(! $this->FetchRows())
       return FALSE;

    return TRUE;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: BeginTrans
//
// [DESCRIPTION]:   Start an SQL transaction
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Non 0 if success, 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function BeginTrans()
{
    return $this->Exec("BEGIN");
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Commit
//
// [DESCRIPTION]:   Commit an SQL transaction
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Non 0 if success, 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Commit()
{
    return $this->Exec("COMMIT");
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: RollBack
//
// [DESCRIPTION]:   Roll back an SQL transaction
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Non 0 if success, 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function RollBack()
{
    return $this->Exec("ROLLBACK");
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Free
//
// [DESCRIPTION]:   Free the memory used after some results have been retrieved
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, False otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Free()
{
    $this->strERR = "";
    if($this->lastRESULT)
       return mysql_free_result($this->lastRESULT);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: LogQuery
//
// [DESCRIPTION]:   Loging the sql query to sql.log file
//
// [PARAMETERS]:    sqlCmd
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2003-08-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function LogQuery($sqlCmd='')
{
   // debuging
   if($_SERVER['REMOTE_ADDR'] != '86.125.114.56')
      return ;

   $errorLeft  = '';
   $errorRight = '';
   if(preg_match("/SQL_/", $sqlCmd))
   {
      $errorLeft  = "--- ERROR : ";
      $errorRight = " ---";
   }

   $fh = fopen(ROOT_PATH."sql.log", "a+");
   fwrite($fh, $errorLeft.$sqlCmd.";$errorRight\n");
   fclose($fh);
}

   /**
   /*****************************************************
    *
    * [FUNCTION NAME]: OpenConnection
    *
    * [DESCRIPTION]: Avoid the redirects and nonsense the openondemand function does
    *
    * [PARAMETERS]:    * @param bool $masterExec
    *
    * [RETURN VALUE]:  void
    *
    * [CREATED BY]:    Huminiuc Adrian (adrian.huminiuc@acrux.biz)
    *
    * [MODIFIED]:      - [programmer (email) date]
    *                    [short description]
    ******************************************************
    */
function OpenConnection($masterExec = false)
{

   if ($masterExec)
   { 
      if (!$this->masterLinkID = mysql_connect($this->masterHost, $this->masterUser, $this->masterPass, true))
      {
         return "Error:" . mysql_error();
      }
      else
      {
         if (!$result = mysql_select_db($this->masterDbname, $this->masterLinkID))
         {
            return "Error:" . mysql_error();
         }
      }
   }
   else
   {
      if (!$this->linkID = mysql_connect($this->host, $this->user, $this->pass))
      {
         return "Error:" . mysql_error();

      }
      else
      {
         if (!$result = mysql_select_db($this->dbname, $this->linkID))
         {
            return "Error:" . mysql_error();
         }
      }
   }

}

} // end of CMySQL class

?>

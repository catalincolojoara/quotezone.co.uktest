<?php

/*****************************************************************************/
/*                                                                           */
/*  CUserQuoteFilter class interface                                                */
/*                                                                           */
/*  (C) 2008 Istvancsek Gabriel (gabi@acrux.biz)                             */
/*                                                                           */
/*****************************************************************************/

include_once "errors.inc";
include_once "MySQL.php";

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CUserQuoteFilter
//
// [DESCRIPTION]:  CUserQuoteFilter class interface
//
// [FUNCTIONS]:      CUserQuoteFilter($dbh=0)
//                   AssertUserQuoteFilter($logID, $siteID, &$quoteReference, &$quotePassword, &$insurer, &$annualPremium, &$monthlyPremium, &$voluntaryExcess, &$voluntaryExcess2nd, $compulsoryExcess, &$compulsoryExcess2nd)
//                   AddUserQuoteFilter($logID, $siteID, $quoteReference, $quotePassword, $insurer, $annualPremium, $monthlyPremium=0, $voluntaryExcess=0, $voluntaryExcess2nd=0, $compulsoryExcess=0, $compulsoryExcess2nd=0)
//                   UpdateUserQuoteFilterPosition($logID, $siteID, $insurer, $annualPremium, $position)
//                   DeleteUserQuoteFilter($quoteFilterID)
//                   GetUserQuoteFilter($quoteFilterID)
//                   Close()
//                   GetError()
//                   ShowError()
//
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CUserQuoteFilter
{

    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string
   

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CUserQuoteFilter
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function CUserQuoteFilter($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertUserQuoteFilter
//
// [DESCRIPTION]:   assert data fields
//
// [PARAMETERS]:    $logID, $siteID, &$quoteReference, &$quotePassword, &$insurer, &$annualPremium, &$monthlyPremium, &$voluntaryExcess, &$voluntaryExcess2nd, $compulsoryExcess, &$compulsoryExcess2nd
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function AssertUserQuoteFilter($quoteUserID, $quoteTypeID, $siteID, $filtered)
{
   $this->strERR = "";
   
   if(! preg_match("/^\d+$/", $quoteUserID))
   {
      $this->strERR = "\n".GetErrorString("INVALID_QUOTE_USER_ID_FIELD");
   }

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR .= "\n".GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
   }

   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR .= "\n".GetErrorString("INVALID_SITE_ID_FIELD");
   }
   
   if(! preg_match("/^\d+$/", $filtered))
   {
      $this->strERR .= "\n".GetErrorString("INVALID_FILTERED_FIELD");
   }

   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddUserQuoteFilter
//
// [DESCRIPTION]:   add data into user rates
//
// [PARAMETERS]:    $siteID, $filteredQuotes, $totalQuotes, $top
//
// [RETURN VALUE]:  int | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2010-06-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function AddUserQuoteFilter($quoteUserID, $quoteTypeID, $siteID, $filtered)
{
   if(! $this->AssertUserQuoteFilter($quoteUserID, $quoteTypeID, $siteID, $filtered))
      return false;
   
   $sqlCmd = " INSERT INTO ".SQL_USER_QUOTE_FILTER." (quote_user_id, quote_type_id, site_id, filtered) VALUES('$quoteUserID', '$quoteTypeID', '$siteID', '$filtered') ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserQuoteFilter
//
// [DESCRIPTION]:   Read data from USER_SYSTEMs table and put it into an array variable
//
// [PARAMETERS]:    $siteFilterID = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2008-02-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
// 
//////////////////////////////////////////////////////////////////////////////FE
function GetAllUserQuoteFilterByUser($quoteUserID, $quoteTypeID)
{
   if(! preg_match("/^\d+$/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_USER_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   $date = date('Y-m-d', mktime() - 30 * 86400);

   $sqlCmd = "SELECT sf.*, uqf.filtered FROM ".SQL_USER_QUOTE_FILTER." uqf LEFT JOIN ".SQL_SITE_FILTERS." sf ON (sf.site_id=uqf.site_id)  LEFT JOIN ".SQL_LOGS." l ON (l.quote_user_id=uqf.quote_user_id) WHERE l.quote_user_id='$quoteUserID' AND sf.site_id is NOT NULL AND l.date >= '$date' AND sf.status='ON' AND uqf.quote_type_id=1 GROUP BY uqf.site_id ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITE_FILTER_ID_NOT_FOUND");
      return false;
   }
  
   $arrayResult = array();
   
   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("site_id")]['filtered_quotes'] = $this->dbh->GetFieldValue("filtered_quotes");
      $arrayResult[$this->dbh->GetFieldValue("site_id")]['total_quotes']    = $this->dbh->GetFieldValue("total_quotes");
      $arrayResult[$this->dbh->GetFieldValue("site_id")]['top']             = $this->dbh->GetFieldValue("top");
      $arrayResult[$this->dbh->GetFieldValue("site_id")]['filtered']        = $this->dbh->GetFieldValue("filtered");
   }
   
   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

}

?>

<?php

/*****************************************************************************/
/*                                                                           */
/*  CIpBan class interface
/*                                                                           */
/*  (C) 2004 Eugen Savin (gabi@acrux.biz)                                     */
/*                                                                           */
/*****************************************************************************/

define("IP_BAN_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

//if(DEBUG_MODE)
//   error_reporting(1);
//else
//   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CIpBan
//
// [DESCRIPTION]:  CIpBan class interface
//
// [FUNCTIONS]:    int  AddIpBan($name='', $proto='HTTP', $quoteTypeID='');
//                 bool UpdateIpBan($siteID=0, $name='', $proto='');
//                 bool DeleteIpBan($siteID=0);
//                 bool GetIpBan($siteID=0);
//                 bool GetAllIpBans();
//
//                 Close();
//                 GetError();
//                 ShowError();
//
// [CREATED BY]:   Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CIpBan
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CIpBan
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CIpBan($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ValidateIpBan
//
// [DESCRIPTION]:   validate entries
//
// [PARAMETERS]:    $ip='', $description='', $banned_time='', $trusted='' 
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ValidateIpBan($ip='', $range='', $description='', $ban='', $trusted='')
{
   if(! preg_match("/[0-9\.\*]+/", $ip))
   {
      $this->strERR = 'INVALID_IP_BAN';
      return false;
   }

   if(! preg_match("/[0-9\.\*]+/", $range))
   {
      $this->strERR = 'INVALID_RANGE_BAN';
      return false;
   }
   
   $description = trim($description);
   
   if(! preg_match("/\d+/", $ban))
   {
      $this->strERR = 'INVALID_BAN_TIME';
      return false;
   }
   
   if(! preg_match("/Y|N/", $trusted))
   {
      $this->strERR = 'INVALID_IP_BAN_TRUSTED';
      return false;
   }
   
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddIpBan
//
// [DESCRIPTION]:   Add new entry to the ip_block table
//
// [PARAMETERS]:    $name='', $proto=''
//
// [RETURN VALUE]:  siteID or 0 in case of failure
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddIpBan($ip='', $range='', $description='', $ban='', $trusted='N')
{
   if(! $this->ValidateIpBan($ip, $range, $description, $ban, $trusted))
      return false;

   $this->lastSQLCMD = "INSERT INTO ".SQL_IP_BAN." (`ip`,`range`,`description`,`ban`,`trusted`) VALUES ('$ip','$range','$description','$ban','$trusted')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateIpBan
//
// [DESCRIPTION]:   Update ip_block table
//
// [PARAMETERS]:    $siteID=0, $name='', $proto=''
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateIpBan($ipBanID='', $ip='', $range='', $description='', $ban='', $trusted='N')
{
   if(! preg_match("/^\d+$/", $ipBanID))
   {
      $this->strERR = GetErrorString("INVALID_IP_BAN_ID_FIELD");
      return false;
   }

   if(! $this->ValidateIpBan($ip, $range, description, $ban, $trusted))
      return false;

   $this->lastSQLCMD = "UPDATE ".SQL_IP_BAN." SET `ip`='$ip',`range`='$range',`description`='$description',`ban`='$ban',`trusted`='$trusted',`ts`=now() WHERE id='$ipBanID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteIpBan
//
// [DESCRIPTION]:   Delete an entry from ip_block table
//
// [PARAMETERS]:    $siteID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteIpBan($ipBanID='')
{
   if(! preg_match("/^\d+$/", $ipBanID))
   {
      $this->strERR = GetErrorString("INVALID_IP_BAN_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_IP_BAN." WHERE id='$ipBanID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetIpBan
//
// [DESCRIPTION]:   Read data from ip_block table and put it into an array variable
//
// [PARAMETERS]:    $siteID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetIpBan($ipBanID=0)
{
   if(! preg_match("/^\d+$/", $ipBanID))
   {
      $this->strERR = GetErrorString("INVALID_IP_BAN_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT if(ban=0,'No',IF((UNIX_TIMESTAMP(CURRENT_TIMESTAMP())-UNIX_TIMESTAMP(ts))/60 > ban,'Yes','No')) as expired, ".SQL_IP_BAN.".* FROM ".SQL_IP_BAN." WHERE id='$ipBanID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("IP_BAN_ID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]            = $this->dbh->GetFieldValue("id");
   $arrayResult["ts"]            = $this->dbh->GetFieldValue("ts");
   $arrayResult["ip"]            = $this->dbh->GetFieldValue("ip");
   $arrayResult["range"]         = $this->dbh->GetFieldValue("range");
   $arrayResult["expired"]       = $this->dbh->GetFieldValue("expired");
   $arrayResult["description"]   = $this->dbh->GetFieldValue("description");
   $arrayResult["ban"]           = $this->dbh->GetFieldValue("ban");
   $arrayResult["trusted"]       = $this->dbh->GetFieldValue("trusted");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetIpBanByIp
//
// [DESCRIPTION]:   Read data from ip_ban table and put it into an array variable
//
// [PARAMETERS]:    $ip
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetIpBanByIp($ip='')
{
   if(! preg_match("/[0-9\.\*]+/", $ip))
   {
      $this->strERR = 'INVALID_IP_BAN';
      return false;
   }

   $this->lastSQLCMD = "SELECT if(ban=0,'No',IF((UNIX_TIMESTAMP(CURRENT_TIMESTAMP())-UNIX_TIMESTAMP(ts))/60 > ban,'Yes','No')) as expired, ".SQL_IP_BAN.".* FROM ".SQL_IP_BAN." WHERE ip='$ip'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("IP_BAN_ID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]            = $this->dbh->GetFieldValue("id");
   $arrayResult["ip"]            = $this->dbh->GetFieldValue("ip");
   $arrayResult["ts"]            = $this->dbh->GetFieldValue("ts");
   $arrayResult["description"]   = $this->dbh->GetFieldValue("description");
   $arrayResult["expired"]       = $this->dbh->GetFieldValue("expired");
   $arrayResult["ban"]           = $this->dbh->GetFieldValue("ban");
   $arrayResult["trusted"]       = $this->dbh->GetFieldValue("trusted");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: IsIpBanned
//
// [DESCRIPTION]:   check if the ip is banned
//
// [PARAMETERS]:    $ip
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function IsIpBanned($ip='')
{
   if(! preg_match("/[0-9\.\*]+/", $ip))
   {
      $this->strERR = 'INVALID_IP_BAN';
      return false;
   }

   $this->lastSQLCMD = "SELECT  if(ban=0,'No',IF((UNIX_TIMESTAMP(CURRENT_TIMESTAMP())-UNIX_TIMESTAMP(ts))/60 > ban,'Yes','No')) as expired FROM ".SQL_IP_BAN." WHERE INET_ATON('$ip') BETWEEN INET_ATON(`ip`) AND INET_ATON(if(`range`<>'',`range`,`ip`)) AND ((UNIX_TIMESTAMP(CURRENT_TIMESTAMP())-UNIX_TIMESTAMP(ts))/60 < ban OR ban='0') and trusted='No'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("IP_BAN_ID_NOT_FOUND");
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: IsIpTrusted
//
// [DESCRIPTION]:   check if the ip is banned
//
// [PARAMETERS]:    $ip
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function IsIpTrusted($ip='')
{
   if(! preg_match("/[0-9\.\*]+/", $ip))
   {
      $this->strERR = 'INVALID_IP_BAN';
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_IP_BAN." WHERE ip='$ip' AND trusted='Yes'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("IP_BAN_ID_NOT_FOUND");
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllIpBans
//
// [DESCRIPTION]:   Read data from ip_block table and put it into an array variable
//                  key = siteID, value = siteName
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllIpBan($limit=0, $offset=0)
{
   $this->lastSQLCMD = "SELECT id,ip FROM ".SQL_IP_BAN." WHERE 1 ";
   
   if($limit)
   {
      if($offset)
         $this->lastSQLCMD .= " LIMIT $offset,$limit";
      else
         $this->lastSQLCMD .= " LIMIT $limit";
   }


   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("IP_BAN_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("ip");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSearchIpBanCount
//
// [DESCRIPTION]:   Get number of searched users
//
// [PARAMETERS]:    IN:  $search=''
//                  OUT: $cnt
//
// [RETURN VALUE]:  int | false in case search failed
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSearchIpBanCount($search='')
{
   $sqlCmd= "SELECT count(*) AS cnt FROM ".SQL_IP_BAN." WHERE ip LIKE '%$search%'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
      return 0;

   $cnt = $this->dbh->GetFieldValue("cnt");

   if(! $cnt)
      $cnt = 0;

   return $cnt;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetIpBanCount
//
// [DESCRIPTION]:   Get number of ips
//
// [PARAMETERS]:    OUT: $cnt
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetIpBanCount()
{
   $this->lastSQLCMD  = "SELECT count(id) AS cnt FROM ".SQL_IP_BAN."";

   if(! $this->dbh->Exec($this->lastSQLCMD ))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
      return 0;

   $cnt = $this->dbh->GetFieldValue("cnt");

   if(! $cnt)
      $cnt = 0;


   return $cnt;

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SearchUsersLikeName
//
// [DESCRIPTION]:   Read data from users table and put it into an array variable
//
// [PARAMETERS]:    IN:  $name='', $limit=0, $offset=0
//                  OUT: $arrayResult
//
// [RETURN VALUE]:  array | false in case search failed
//
// [CREATED BY]:    Ciprian Sturza (cipi@acrux.biz) 2007-01-31
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SearchIpBanByIp($ip='',$limit=0,$offset=0)
{
   $this->lastSQLCMD = "SELECT * FROM ".SQL_IP_BAN." WHERE ip LIKE '%$ip%'";

   if($limit)
   {
      if($offset)
         $this->lastSQLCMD .= " LIMIT $offset,$limit";
      else
         $this->lastSQLCMD .= " LIMIT $limit";
   }

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_IP_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $userID                               = $this->dbh->GetFieldValue("id");
      $arrayResult[$userID]["id"]           = $this->dbh->GetFieldValue("id");
      $arrayResult[$userID]["ip"]           = $this->dbh->GetFieldValue("ip");
      $arrayResult[$userID]["description"]  = $this->dbh->GetFieldValue("description");
      $arrayResult[$userID]["ban"]          = $this->dbh->GetFieldValue("ban");
      $arrayResult[$userID]["trusted"]      = $this->dbh->GetFieldValue("trusted");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AutoIpBan
//
// [DESCRIPTION]:   Add new entry to the ip_block table
//
// [PARAMETERS]:    $name='', $proto=''
//
// [RETURN VALUE]:  siteID or 0 in case of failure
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AutoIpBan($ip='', $numberOfQuotes="20")
{
   if(! preg_match("/[0-9\.\*]+/", $ip))
   {
      $this->strERR = 'INVALID_IP_BAN';
      return false;
   }
   
   if(! preg_match("/^\d+$/", $numberOfQuotes))
   {
      $this->strERR = 'INVALID_NUMBER_OF_QUOTES_IP_BAN';
      return false;
   }
   
   if($this->IsIpTrusted($ip))
      return true;

   $today     = date('Y-m-d');
   $time      = date('H:i:s');
   $yesterday = date('Y-m-d', mktime() - 86400);

   $this->lastSQLCMD = "SELECT count(*) as total from ".SQL_LOGS." WHERE ((date='$yesterday' AND time>=$time) OR date='$today') AND host_ip='$ip' HAVING total > $numberOfQuotes";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return true;
   }

   $hostName = gethostbyaddr($ip);
   // ignore next hosts
   if(preg_match("/aol\.com/", strtolower($hostName)))
      return true;

   $bannedTime = ($this->dbh->GetFieldValue("total") - 20) * 30;

// TODO EMAIL
//    if($resIpBan = $this->GetIpBanByIp($ip))
//    {
//       return $this->UpdateIpBan($resIpBan['id'], $ip, 'Auto Ban System', $bannedTime);
//    }
//    
//    //add new ip ban
//    return $this->AddIpBan($ip, 'Auto Ban System', $bannedTime);

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Print the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Gabriel ISTVANCSEK (gabi@acrux.biz) 2007-10-29
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

} // end of CIpBan class
?>

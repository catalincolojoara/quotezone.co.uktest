<?php

$personalizedFilters = array (
  'GENERAL_NON_MOTORING_CONDITIONS_CANNOT_QUOTE' => 
  array (
    'msg' => 'Cannot quote for any non-motoring convictions.',
    'prio' => '1',
  ),
  'GENERAL_LIC_TYPE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the licence type(s) specified.',
    'prio' => '1',
  ),
  'GENERAL_REGKEEP_OWNER_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the registered keeper and/or owner of the vehicle.',
    'prio' => '2',
  ),
  'GENERAL_MODIF_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the modification details.',
    'prio' => '1',
  ),
  'GENERAL_NI_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote for Northern Ireland at present.',
    'prio' => '1',
  ),
  'GENERAL_IMPORT_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote for this type of import.',
    'prio' => '1',
  ),
  'GENERAL_QPLATE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if the vehicle has a Q plate.',
    'prio' => '1',
  ),
  'GENERAL_EMPTY_REGISTRATION_NUMBER_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to vehicle registration number.',
    'prio' => '1',
  ),
  'GENERAL_NO_REGISTRATION_NUMBER_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote without the vehicle registration.',
    'prio' => '1',
  ),
  'GENERAL_CLAIMS_NO_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the number of claims.',
    'prio' => '1',
  ),
  'GENERAL_CONVS_NO_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the number of convictions.',
    'prio' => '1',
  ),
  'GENERAL_OCC_TYPE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Unable to get a quote. This may be because the server is too busy at present.',
    'prio' => '3',
  ),
  'PERSONALIZED_OCC_TYPE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Unable to get a quote. This is due to the occupation you have selected.',
    'prio' => '1',
  ),

    'GENERAL_OCC_TYPE_STUDENT_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote for your employment status when occupation is student.',
    'prio' => '1',
  ),
  'GENERAL_BUSS_TYPE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Unable to get a quote. This may be because the server is too busy at present.',
    'prio' => '3',
  ),
  'GENERAL_DRV_AGE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the age of one or more of the drivers.',
    'prio' => '1',
  ),
  'GENERAL_CLAIM_DETAILS_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the claim details.',
    'prio' => '1',
  ),
  'GENERAL_CLAIM_CONV_DETAILS_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the claim/conviction details.',
    'prio' => '1',
  ),
  'GENERAL_UK_RESID_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the UK residency details.',
    'prio' => '1',
  ),
  'GENERAL_MAIN_USER_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the main user of the vehicle.',
    'prio' => '2',
  ),
  'GENERAL_PROP_JOINT_AGE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the age of the proposer/joint proposer.',
    'prio' => '1',
  ),

  'GENERAL_VEH_LEFT_SIDE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if vehicle is not right hand drive.',
    'prio' => '1',
  ),
  'GENERAL_VEH_FUEL_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the fuel type of the vehicle.',
    'prio' => '1',
  ),

  'GENERAL_BUSS_TYPE_USE_TYPE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your type of business and because you require business use.',
    'prio' => '1',
  ),
  'GENERAL_EMP_STATUS_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your employment status.',
    'prio' => '1',
  ),
  'GENERAL_VEH_VALUE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the vehicle value.',
    'prio' => '1',
  ),
  'GENERAL_ANN_MILEAGE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the annual mileage.',
    'prio' => '2',
  ),
  'GENERAL_POSTCODE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote for your postcode.',
    'prio' => '1',
  ),
  'GENERAL_COVER_TYPE_VEH_VALUE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the cover type and vehicle value.',
    'prio' => '2',
  ),
  'GENERAL_INCEPTION_DATE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the insurance start date.',
    'prio' => '2',
  ),
  'GENERAL_BUSINESS_DATE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the business established year.',
    'prio' => '2',
  ),
  'GENERAL_LIC_YEARS_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the licence years.',
    'prio' => '1',
  ),
  'GENERAL_VEH_AGE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the vehicle age.',
    'prio' => '1',
  ),
  'GENERAL_SEATS_NUMBER_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the number of seats.',
    'prio' => '1',
  ),
  'GENERAL_CONV_PROSECUTION_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote for your conviction/pending prosecution details.',
    'prio' => '1',
  ),
  'GENERAL_DRV_NUMBER_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the number of drivers.',
    'prio' => '2',
  ),
  'GENERAL_TYPE_OF_USE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the type of use.',
    'prio' => '2',
  ),
  'GENERAL_UNPROT_NCB_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the unprotected NCB.',
    'prio' => '2',
  ),
  'GENERAL_LIC_TYPE_UK_RESID_TIME_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the license type and the UK residency time.',
    'prio' => '1',
  ),
  'GENERAL_OCC_NUMBER_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the number of occupations.',
    'prio' => '2',
  ),
  'GENERAL_VEH_MAKE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the vehicle make.',
    'prio' => '1',
  ),
  'GENERAL_HAVE_QUOTE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because the driver must not have already had a quote.',
    'prio' => '3',
  ),
  'GENERAL_PT_OCC_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your part time occupation.',
    'prio' => '2',
  ),
  'GENERAL_VOL_EXC_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the voluntary excess.',
    'prio' => '2',
  ),
  'GENERAL_COVER_TYPE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your cover type.',
    'prio' => '2',
  ),
  'GENERAL_MARITAL_RELATIONSHIP_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the marital status and relationship.',
    'prio' => '3',
  ),
  'KEY_CONNECT_PERSONAL_MARITAL_RELATIONSHIP_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote for your circumstances.',
    'prio' => '3',
  ),
  'GENERAL_MARITAL_RELATIONSHIP_CANNOT_QUOTE_AMEX' =>
  array (
    // WRKT-7EX
    'msg' => 'We could not return a price.',
    'prio' => '3',
  ),
  'GENERAL_SAME_SEX_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because spouse is the same sex as you.',
    'prio' => '3',
  ),
  'GENERAL_TITLE_MARITAL_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the title and marital status.',
    'prio' => '3',
  ),
  'GENERAL_TITLE_COMMON_LAW_MARITAL_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the selected relationship to the Proposer.',
    'prio' => '3',
  ),
  'GENERAL_BANKRUPCY_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the bankruptcy details provided.',
    'prio' => '1',
  ),
  'GENERAL_FAR_FUTURE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote this far in the future.',
    'prio' => '1',
  ),
  'GENERAL_VEH_KEPT_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the overnight location of the vehicle.',
    'prio' => '2',
  ),
  'GENERAL_NAME_LENGTH_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the length of your name.',
    'prio' => '3',
  ),
  'GENERAL_GENDER_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of your gender.',
    'prio' => '1',
  ),
  'GENERAL_GENDER_AGE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of your gender and age.',
    'prio' => '1',
  ),
  'GENERAL_LIC_TYPE_AGE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the licence type and age of one or more of the drivers.',
    'prio' => '1',
  ),
  //WRS7-G09 - Kev
  'GENERAL_NCB_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of your current NCB.',
    'prio' => '2',
  ),
  'GENERAL_PROT_NCB_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of your protected NCB.',
    'prio' => '2',
  ),
  'GENERAL_ADD_DRV_PT_OCC_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if any of the additional driver(s) have a part time occupation.',
    'prio' => '2',
  ),
  'GENERAL_ADD_RELATIONSHIP_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the additional drivers relationship to the proposer.',
    'prio' => '2',
  ),
  'GENERAL_ADD_INSURED_RELATIONSHIP_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the joint proposer relationship.',
    'prio' => '2',
  ),
  'GENERAL_INSURANCE_DECLINED_CANCELLED_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote as insurance has been declined/cancelled before.',
    'prio' => '1',
  ),
  'GENERAL_NON_MOT_CONV_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if any driver has had a non-motoring conviction',
    'prio' => '1',
  ),
  'BIKE_RIDERS_NO_CBT_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if all riders have not passed their CBT.',
    'prio' => '1',
  ),
  'BIKE_SIDECAR_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if the vehicle has a sidecar.',
    'prio' => '1',
  ),
  'BIKE_ACCESORIES_VALUE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your estimated value of fitted accessories.',
    'prio' => '2',
  ),
  'BIKE_RIDERS_TOTTING_UP_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if any riders have been disqualified under the \'totting up\' procedure (TT99 or XX99), within the last five years.',
    'prio' => '1',
  ),
  'BIKE_RIDERS_AGE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the age of one or more of the riders.',
    'prio' => '1',
  ),
  'VAN_VEH_WEIGHT_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of the vehicle weight.',
    'prio' => '2',
  ),
  'VAN_TRAILER_COVER_REQ_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if trailer cover is required.',
    'prio' => '2',
  ),

  'VAN_TRAILER_COVER_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your trailer cover type.',
    'prio' => '2',
  ),

  'VAN_TRAILER_DIFF_POLICY_COVER_REQ_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if the trailer cover type is different than the policy cover type.',
    'prio' => '2',
  ),
  'VAN_TYPE_OF_VAN_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote for this type of van.',
    'prio' => '1',
  ),
  'HOME_FLOODING_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your flooding status.',
    'prio' => '1',
  ),
  'HOME_BANKRUPTCY_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if anyone living at the property has ever been made bankrupt.',
    'prio' => '1',
  ),
  'HOME_LANDSLIP_GROUNDHEAVE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your landslip/ground heave status.',
    'prio' => '1',
  ),
  'HOME_SUBSIDENCE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your subsidence status.',
    'prio' => '1',
  ),
  'HOME_SPEC_TERMS_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your special terms status.',
    'prio' => '1',
  ),
  'HOME_REPAIR_STATE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the state of repair of your building.',
    'prio' => '2',
  ),
  'HOME_TREE_SHRUBS_PROXIMITY_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your proximity to trees/shrubs.',
    'prio' => '2',
  ),
  'HOME_RIVER_WATERCOURSE_PROXIMITY_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your proximity to a river/watercourse.',
    'prio' => '2',
  ),
  'HOME_UNOCCUPIED_STATUS_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your property unoccupied details.',
    'prio' => '2',
  ),
  'HOME_OCCUPANCY_STATUS_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your property occupancy details',
    'prio' => '1',
  ),
  'HOME_REGION_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your property region.',
    'prio' => '1',
  ),
  'HOME_EXTERIOR_WALL_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your exterior wall construction.',
    'prio' => '1',
  ),
  'HOME_ROOF_TYPE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your roof type.',
    'prio' => '1',
  ),
  'HOME_PROPERY_BUSS_USE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the business use in your property.',
    'prio' => '1',
  ),
  'HOME_FLAT_ROOF_STATUS_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your flat roof status.',
    'prio' => '2',
  ),
  'HOME_OWNERSHIP_STATUS_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your property ownership status.',
    'prio' => '1',
  ),
  'HOME_YEAR_BUILT_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your property built year.',
    'prio' => '1',
  ),
  'HOME_PROPERTY_TYPE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your property type.',
    'prio' => '1',
  ),
  'HOME_NOT_SELF_CONTAINED_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if your building is not self contained.',
    'prio' => '1',
  ),
  'HOME_WORK_IN_PROGRESS_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if there is any building work in progress.',
    'prio' => '1',
  ),
  'HOME_LISTED_STATUS_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your listed building status.',
    'prio' => '1',
  ),
  'HOME_BC_COMBINED_POLICIES_CANNOT_QUOTE' =>
  array (
    'msg' => 'Can only quote for Buildings & Contents combined policies online.',
    'prio' => '2',
  ),
  'HOME_NO_LOCK_ENTRANCE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if you do not have your own lockable entrance.',
    'prio' => '1',
  ),
  'HOME_PROPERTY_SUM_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to your property sum insured.',
    'prio' => '1',
  ),
  'HOME_YOU_FAMILY_OCCUPANCY_STATUS_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if the property is not occupied solely by you and your family members and within the boundaries of the United Kingdom.',
    'prio' => '1',
  ),
  'HOME_MAIN_RESIDENCE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if the property is not your main residence.',
    'prio' => '1',
  ),
  'HOME_STANDARD_CONSTRUCTION_WORK_PROGRESS_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if the property is not of standard construction, i.e. built of brick, stone or concrete and has no building work in progress.',
    'prio' => '1',
  ),
  'HOME_CLERICAL_WORK_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if the property is used for in any part for business trade or profession, other than occasional clerical work.',
    'prio' => '1',
  ),
  'HOME_MAISONETTE_LISTED_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if the property is a maisonette or a listed building. ',
    'prio' => '1',
  ),
  'HOME_RENTED_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if the property is rented, let or sub-let by or to you. ',
    'prio' => '1',
  ),
  'HOME_DAMAGE_RENTED_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot offer accidental damage cover on rented/let properties.',
    'prio' => '2',
  ),
  'HOME_PERSONAL_POSSESIONS_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot offer cover for your personal possessions.',
    'prio' => '2',
  ),
  'HOME_CONTENTS_SUM_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the contents sum insured.',
    'prio' => '1',
  ),
  'HOME_PREFABRICATED_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote for prefabricated buildings.',
    'prio' => '1',
  ),
  'HOME_NON_FAMILY_SHARED_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if the property is shared with non-family members.',
    'prio' => '1',
  ),
  'HOME_BEDROOM_NUMBER_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the number of bedrooms in the property.',
    'prio' => '1',
  ),
  'GENERAL_COVER_TYPE_AGE_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of your age and selected cover type.',
    'prio' => '2',
  ),
  'CAR_OWN_ONE_CAR_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because you own only one car.',
    'prio' => '1',
  ),
  'CAR_VEH_COMMERCIAL_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if the vehicle is commercial.',
    'prio' => '1',
  ),
  'CAR_VEH_GROUP_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote because of your vehicle group.',
    'prio' => '1',
  ),
  'CAR_COMPANY_CAR_NCB_OVER_ZERO_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if the vehicle is a company car with NCB greater than 0.',
    'prio' => '1',
  ),
  'CAR_INSURANCE_REFUSED_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if you had insurance refused',
    'prio' => '1',
  ),
  'GENERAL_ADD_DRV_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote due to the additional driver/s.',
    'prio' => '2',
  ),
  'GENERAL_VEHICLE_NOT_PURCHASED_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot offer quotations when the vehicle has not yet been purchased.',
    'prio' => '1',
  ),

  'GENERAL_PART_OCC_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote if any driver(s) has a part-time occupation.',
    'prio' => '1',
  ),

  'GENERAL_CANNOT_QUOTE' =>
  array (
    'msg' => 'Cannot quote for your details.',
    'prio' => '3',
  ),
    
   'GENERAL_QUINN_DIRECT' =>
  array (
    'msg' => 'Quinn Direct are unable to offer you a quotation at this time.',
    'prio' => '1',
  ),
  
    'GENERAL_QUINN_DIRECT_LIBERTY' =>
  array (
    'msg' => 'Liberty Insurance are unable to offer you a quotation at this time.',
    'prio' => '1',
  ),
);

function OrderFiltersByPrio($filterKeyArray)
{
	global $personalizedFilters;

	if (!$filterKeyArray)
		return;

	foreach ($filterKeyArray as $key=>$filterKey)
	{
		$filterPrio = GetFilterPrio($filterKey);
		$tempArray[$filterPrio][] = $filterKey;
	}

	ksort($tempArray);
	$temp2Array = array();
	foreach ($tempArray as $prioKey=>$prioMsgList)
	{
		sort($prioMsgList);
		$temp2Array[$prioKey] = array_unique($prioMsgList);
	}

	$finalArray = array();
	foreach ($temp2Array as $prioKey=>$prioMsgList)
		foreach ($prioMsgList as $prioMsg)
			$finalArray[] = $prioMsg;

	return $finalArray;
}

function GetFilterPrio($filterKey)
{
	global $personalizedFilters;

	return $personalizedFilters[$filterKey]['prio'];
}

$personalizedFiltersOnly = array(
	"GENERAL_LIC_TYPE_CANNOT_QUOTE" => "Cannot quote due to the licence type(s) specified.",
	"GENERAL_REGKEEP_OWNER_CANNOT_QUOTE" => "Cannot quote due to the registered keeper and/or owner of the vehicle.",
	"GENERAL_MODIF_CANNOT_QUOTE" => "Cannot quote due to the modification details.",
	"GENERAL_NI_CANNOT_QUOTE" => "Cannot quote for Northern Ireland at present.",
	"GENERAL_IMPORT_CANNOT_QUOTE" => "Cannot quote if the vehicle is an import.",
	"GENERAL_QPLATE_CANNOT_QUOTE" => "Cannot quote if the vehicle has a Q plate.",
	"GENERAL_CLAIMS_NO_CANNOT_QUOTE" => "Cannot quote due to the number of claims.",
	"GENERAL_CONVS_NO_CANNOT_QUOTE" => "Cannot quote due to the number of convictions.",
	"GENERAL_OCC_TYPE_CANNOT_QUOTE" => "Cannot quote due to the type of occupation.",
	"PERSONALIZED_OCC_TYPE_CANNOT_QUOTE" => "Unable to get a quote. This is due to the occupation you have selected.",    
   "GENERAL_OCC_TYPE_STUDENT_CANNOT_QUOTE" => "Cannot quote for your employment status when occupation is student.",
	"GENERAL_BUSS_TYPE_CANNOT_QUOTE" => "Cannot quote due to the type of business.",
	"GENERAL_DRV_AGE_CANNOT_QUOTE" => "Cannot quote due to the age of one or more of the drivers.",
	"GENERAL_CLAIM_DETAILS_CANNOT_QUOTE" => "Cannot quote due to the claim details.",
	"GENERAL_CLAIM_CONV_DETAILS_CANNOT_QUOTE" => "Cannot quote due to the claim/conviction details.",
	"GENERAL_UK_RESID_CANNOT_QUOTE" => "Cannot quote due to the UK residency details.",
	"GENERAL_MAIN_USER_CANNOT_QUOTE" => "Cannot quote due to the main user of the vehicle.",
	"GENERAL_PROP_JOINT_AGE_CANNOT_QUOTE" => "Cannot quote due to the age of the proposer/joint proposer.",
	"GENERAL_VEH_LEFT_SIDE_CANNOT_QUOTE" => "Cannot quote if vehicle is not right hand drive.",
	"GENERAL_VEH_FUEL_CANNOT_QUOTE" => "Cannot quote due to the fuel type of the vehicle.",
        "GENERAL_BUSS_TYPE_USE_TYPE_CANNOT_QUOTE" => "Cannot quote due to your type of business and because you require business use.",
	"GENERAL_EMP_STATUS_CANNOT_QUOTE" => "Cannot quote due to your employment status.",
	"GENERAL_VEH_VALUE_CANNOT_QUOTE" => "Cannot quote because of the vehicle value.",
	"GENERAL_ANN_MILEAGE_CANNOT_QUOTE" => "Cannot quote because of the annual mileage.",
	"GENERAL_POSTCODE_CANNOT_QUOTE" => "Cannot quote for your postcode.",
	"GENERAL_COVER_TYPE_VEH_VALUE_CANNOT_QUOTE" => "Cannot quote because of the cover type and vehicle value.",
	"GENERAL_INCEPTION_DATE_CANNOT_QUOTE" => "Cannot quote because of the insurance start date.",
	"GENERAL_BUSINESS_DATE_CANNOT_QUOTE" => "Cannot quote because of the business established year.",
	"GENERAL_LIC_YEARS_CANNOT_QUOTE" => "Cannot quote because of the licence years.",
	"GENERAL_VEH_AGE_CANNOT_QUOTE" => "Cannot quote because of the vehicle age.",
	"GENERAL_SEATS_NUMBER_CANNOT_QUOTE" => "Cannot quote because of the number of seats.",
	"GENERAL_CONV_PROSECUTION_CANNOT_QUOTE" => "Cannot quote for your conviction/pending prosecution details.",
	"GENERAL_DRV_NUMBER_CANNOT_QUOTE" => "Cannot quote due to the number of drivers.",
	"GENERAL_TYPE_OF_USE_CANNOT_QUOTE" => "Cannot quote because of the type of use.",
	"GENERAL_UNPROT_NCB_CANNOT_QUOTE" => "Cannot quote because of the unprotected NCB.",
	"GENERAL_LIC_TYPE_UK_RESID_TIME_CANNOT_QUOTE" => "Cannot quote because of the license type and the UK residency time.",
	"GENERAL_OCC_NUMBER_CANNOT_QUOTE" => "Cannot quote because of the number of occupations.",
	"GENERAL_VEH_MAKE_CANNOT_QUOTE" => "Cannot quote because of the vehicle make.",
	"GENERAL_HAVE_QUOTE_CANNOT_QUOTE" => "Cannot quote because the driver must not have already had a quote.",
	"GENERAL_PT_OCC_CANNOT_QUOTE" => "Cannot quote due to your part time occupation.",
	"GENERAL_VOL_EXC_CANNOT_QUOTE" => "Cannot quote because of the voluntary excess.",
	"GENERAL_COVER_TYPE_CANNOT_QUOTE" => "Cannot quote due to your cover type.",
	"GENERAL_MARITAL_RELATIONSHIP_CANNOT_QUOTE" => "Cannot quote because of the marital status and relationship.",
    // WRKT-7EX 
	"GENERAL_MARITAL_RELATIONSHIP_CANNOT_QUOTE_AMEX" => "We could not return a price.",
	"GENERAL_SAME_SEX_CANNOT_QUOTE" => "Cannot quote because spouse is the same sex as you.",
	"GENERAL_TITLE_MARITAL_CANNOT_QUOTE" => "Cannot quote because of the title and marital status.",
	//WRA8-8DK
	'GENERAL_TITLE_COMMON_LAW_MARITAL_CANNOT_QUOTE' => "Cannot quote because of the selected relationship to the Proposer.",
	"GENERAL_BANKRUPCY_CANNOT_QUOTE" => "Cannot quote due to the bankruptcy details provided.",
	"GENERAL_FAR_FUTURE_CANNOT_QUOTE" => "Cannot quote this far in the future.",
	"GENERAL_VEH_KEPT_CANNOT_QUOTE" => "Cannot quote due to the overnight location of the vehicle.",
	"GENERAL_NAME_LENGTH_CANNOT_QUOTE" => "Cannot quote due to the length of your name.",
	"GENERAL_GENDER_CANNOT_QUOTE" => "Cannot quote because of your gender.",
	"GENERAL_GENDER_AGE_CANNOT_QUOTE" => "Cannot quote because of your gender and age.",
	"GENERAL_LIC_TYPE_AGE_CANNOT_QUOTE" => "Cannot quote due to the licence type and age of one or more of the drivers.",
	//WRS7-G09 - Kev
	"GENERAL_NCB_CANNOT_QUOTE" => "Cannot quote because of your current NCB.",
	"GENERAL_PROT_NCB_CANNOT_QUOTE" => "Cannot quote because of your protected NCB.",
	"GENERAL_ADD_DRV_PT_OCC_CANNOT_QUOTE" => "Cannot quote if any of the additional driver(s) have a part time occupation.",
	"GENERAL_ADD_RELATIONSHIP_CANNOT_QUOTE" => "Cannot quote due to the additional drivers relationship to the proposer.",
    "GENERAL_ADD_INSURED_RELATIONSHIP_CANNOT_QUOTE" => "Cannot quote due to the joint proposer relationship.",
    "GENERAL_INSURANCE_DECLINED_CANCELLED_CANNOT_QUOTE" => "Cannot quote as insurance has been declined/cancelled before.",
	"BIKE_RIDERS_NO_CBT_CANNOT_QUOTE" => "Cannot quote if all riders have not passed their CBT.",
	"BIKE_SIDECAR_CANNOT_QUOTE" => "Cannot quote if the vehicle has a sidecar.",
	"BIKE_ACCESORIES_VALUE_CANNOT_QUOTE" => "Cannot quote due to your estimated value of fitted accessories.",
	"BIKE_RIDERS_TOTTING_UP_CANNOT_QUOTE" => "Cannot quote if any riders have been disqualified under the 'totting up' procedure (TT99 or XX99), within the last five years.",
	"BIKE_RIDERS_AGE_CANNOT_QUOTE" => "Cannot quote due to the age of one or more of the riders.",
	"VAN_VEH_WEIGHT_CANNOT_QUOTE" => "Cannot quote because of the vehicle weight.",
	"VAN_TRAILER_COVER_REQ_CANNOT_QUOTE" => "Cannot quote if  trailer cover is required.",
	"VAN_TRAILER_COVER_CANNOT_QUOTE" => "Cannot quote due to your trailer cover type.",
    "VAN_TRAILER_DIFF_POLICY_COVER_REQ_CANNOT_QUOTE" => "Cannot quote if the trailer cover type is different than the policy cover type.",
	"HOME_FLOODING_CANNOT_QUOTE" => "Cannot quote due to your flooding status.",
	"HOME_LANDSLIP_GROUNDHEAVE_CANNOT_QUOTE" => "Cannot quote due to your landslip/ground heave status.",
	"HOME_SUBSIDENCE_CANNOT_QUOTE" => "Cannot quote due to your subsidence status.",
	"HOME_SPEC_TERMS_CANNOT_QUOTE" => "Cannot quote due to your special terms status.",
	"HOME_REPAIR_STATE_CANNOT_QUOTE" => "Cannot quote due to the state of repair of your building.",
	"HOME_TREE_SHRUBS_PROXIMITY_CANNOT_QUOTE" => "Cannot quote due to your proximity to trees/shrubs.",
	"HOME_RIVER_WATERCOURSE_PROXIMITY_CANNOT_QUOTE" => "Cannot quote due to your proximity to a river/watercourse.",
	"HOME_UNOCCUPIED_STATUS_CANNOT_QUOTE" => "Cannot quote due to your property unoccupied details.",
	"HOME_OCCUPANCY_STATUS_CANNOT_QUOTE" => "Cannot quote due to your property occupancy details",
	"HOME_REGION_CANNOT_QUOTE" => "Cannot quote due to your property region.",
	"HOME_EXTERIOR_WALL_CANNOT_QUOTE" => "Cannot quote due to your exterior wall construction.",
	"HOME_ROOF_TYPE_CANNOT_QUOTE" => "Cannot quote due to your roof type.",
	"HOME_PROPERY_BUSS_USE_CANNOT_QUOTE" => "Cannot quote due to the business use in your property.",
	"HOME_FLAT_ROOF_STATUS_CANNOT_QUOTE" => "Cannot quote due to your flat roof status.",
	"HOME_OWNERSHIP_STATUS_CANNOT_QUOTE" => "Cannot quote due to your property ownership status.",
	"HOME_YEAR_BUILT_CANNOT_QUOTE" => "Cannot quote due to your property built year.",
	"HOME_PROPERTY_TYPE_CANNOT_QUOTE" => "Cannot quote due to your property type.",
	"HOME_NOT_SELF_CONTAINED_CANNOT_QUOTE" => "Cannot quote if your building is not self contained.",
	"HOME_WORK_IN_PROGRESS_CANNOT_QUOTE" => "Cannot quote if there is any building work in progress.",
	"HOME_LISTED_STATUS_CANNOT_QUOTE" => "Cannot quote due to your listed building status.",
	"HOME_BC_COMBINED_POLICIES_CANNOT_QUOTE" => "Can only quote for Buildings & Contents combined policies online.",
	"HOME_NO_LOCK_ENTRANCE_CANNOT_QUOTE" => "Cannot quote if you do not have your own lockable entrance.",
	"HOME_PROPERTY_SUM_CANNOT_QUOTE" => "Cannot quote due to your property sum insured.",
	"HOME_YOU_FAMILY_OCCUPANCY_STATUS_CANNOT_QUOTE" => "Cannot quote if the property is not occupied solely by you and your family members and within the boundaries of the United Kingdom.",
	"HOME_MAIN_RESIDENCE_CANNOT_QUOTE" => "Cannot quote if the property is not your main residence.",
	"HOME_STANDARD_CONSTRUCTION_WORK_PROGRESS_CANNOT_QUOTE" => "Cannot quote if the property is not of standard construction, i.e. built of brick, stone or concrete and has no building work in progress.",
	"HOME_CLERICAL_WORK_CANNOT_QUOTE" => "Cannot quote if the property is used for in any part for business trade or profession, other than occasional clerical work.",
	"HOME_MAISONETTE_LISTED_CANNOT_QUOTE" => "Cannot quote if the property is a maisonette or a listed building. ",
	"HOME_RENTED_CANNOT_QUOTE" => "Cannot quote if the property is rented, let or sub-let by or to you. ",
	"HOME_DAMAGE_RENTED_CANNOT_QUOTE" => "Cannot offer accidental damage cover on rented/let properties.",
	"HOME_PERSONAL_POSSESIONS_CANNOT_QUOTE" => "Cannot offer cover for your personal possessions.",
	"HOME_CONTENTS_SUM_CANNOT_QUOTE" => "Cannot quote due to the contents sum insured.",
	"HOME_PREFABRICATED_CANNOT_QUOTE" => "Cannot quote for prefabricated buildings.",
	"HOME_NON_FAMILY_SHARED_CANNOT_QUOTE" => "Cannot quote if the property is shared with non-family members.",
	"HOME_BEDROOM_NUMBER_CANNOT_QUOTE" => "Cannot quote due to the number of bedrooms in the property.",
	"GENERAL_COVER_TYPE_AGE_CANNOT_QUOTE" => "Cannot quote because of your age and selected cover type.",
	"CAR_OWN_ONE_CAR_CANNOT_QUOTE" => "Cannot quote because you own only one car.",
	"CAR_VEH_COMMERCIAL_CANNOT_QUOTE" => "Cannot quote if the vehicle is commercial.",
	"CAR_VEH_GROUP_CANNOT_QUOTE" => "Cannot quote because of your vehicle group.",
	"CAR_COMPANY_CAR_NCB_OVER_ZERO_CANNOT_QUOTE" => "Cannot quote if the vehicle is a company car with NCB greater than 0.",    
   "GENERAL_CANNOT_QUOTE" => "Cannot quote for your details.",
   "GENERAL_QUINN_DIRECT" => "Quinn Direct are unable to offer you a quotation at this time.",
   "GENERAL_QUINN_DIRECT_LIBERTY" => "Liberty Insurance are unable to offer you a quotation at this time.",
);



?>

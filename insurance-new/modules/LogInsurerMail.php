<?php
/*****************************************************************************/
/*                                                                           */
/*  CLogInsurerMail class interface
/*                                                                           */
/*  (C) 2005 Istvancsek Gabriel (gabi@acrux.biz)                                     */
/*                                                                           */
/*****************************************************************************/

define("Log_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CLogInsurerMailInsurerMail
//
// [DESCRIPTION]:  CLogInsurerMail class interface
//
// [FUNCTIONS]:      true    | false  AssertLog($quoteUserID='', $hostIp='', $filename='')
//                   string  | false  StdToMysqlDate($date='')
//                   integer | false  AddLog($quoteUserID='', $hostIp='', $filename='', $date='now()', $time='now()')
//                   true    | false  UpdateLog($logID='', $quoteUserID='', $hostIp='', $filename)
//                   true    | false  DeleteLog($logID='')
//                   array   | false  GetLog($logID='')
//                   integer | false  GetLogIdByFilename($fileName='')
//                   array   | false  GetAllLogsByQuoteUserID($quoteUserID = '')
//                   array   | false  GetAllLogsByHostIp($quoteUserID = '')
//                   array   | false  GetAllLogsByInterval($quoteUserID='', $hostIp='', $startDate='', $endDate='', $startTime='',
//                                   $endTime='')
//                   array   | false GetLastQuoteDate($quoteTypeID)
// 
//                   Close()
//                   GetError()
//                   GetError()
//
// [CREATED BY]:   Istvancsek Gabriel (gabi@acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CLogInsurerMail
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last Log error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CLogInsurerMail
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CLogInsurerMail($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertLog
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    $quoteUserID, $hostIp, $filename
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertLogInsurerMail($logId='', $siteId='', $position='')
{
   $this->strERR = '';

   if(! preg_match("/\d+/",$logId))
      $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");


	if(! preg_match("/\d+/",$siteId))
      $this->strERR .= GetErrorString("INVALID_SITE_ID_FIELD");

	if(! preg_match("/\d+/",$position))
      $this->strERR .= GetErrorString("INVALID_POSITION_FIELD");

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   if(! preg_match("/\d{1,2}\/\d{1,2}\/\d{4}/i",$date))
      $this->strERR = GetErrorString("INVALID_LOG_STD_TO_MYSQL_DATE");

   if(! empty($this->strERR))
      return false;
   
   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddLogInsurerMail
//
// [DESCRIPTION]:   Add new entry to the Logs table
//
// [PARAMETERS]:    $logId='', $siteId='', $position='', $date='', $time=''
//
// [RETURN VALUE]:  logID or 0 in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-13
//
// [MODIFIED]:      - Velnic Daniel (dan@acux.biz) 2005-08-15
//                    added quote_user_id
//////////////////////////////////////////////////////////////////////////////FE
function AddLogInsurerMail($logId='', $siteId='', $position='', $date='', $time='')
{
   if(! $this->AssertLogInsurerMail($logId,$siteId,$position))
      return false;

   $this->lastSQLCMD = "SELECT id FROM log_insurer_mail WHERE log_id='$logId' AND site_id='$siteId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }
     
   if ($this->dbh->GetRows() > 0)
   {
   		if (!$this->dbh->FetchRows())
   		{
    	   $this->strERR = $this->dbh->GetError();
	       return false;
   		}
   		
   		return $this->dbh->GetFieldValue('id');
   }
      
      
   if(empty($date) && empty($time))
   {
      $date='now()';
      $time='now()';

      $this->lastSQLCMD = "INSERT INTO log_insurer_mail (log_id,site_id,position,date,time) VALUES  ('$logId','$siteId','$position',$date,$time)";

   }
   else
   {
      $this->lastSQLCMD = "INSERT INTO log_insurer_mail (log_id,site_id,position,date,time) VALUES  ('$logId','$siteId','$position','$date','$time')";
      
   }
   //print "\n".$this->lastSQLCMD."\n";      

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateLogInsurerMail
//
// [DESCRIPTION]:   Update LogInsurerMail table
//
// [PARAMETERS]:    $logId='', $siteId='', $position=''
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateLogInsurerMail($logInsurerMailId='', $logId='', $siteId='', $position='')
{

   if(! $this->AssertLog($logId='', $siteId='', $position=''))
      return false;

   // check if logID exists
   $this->lastSQLCMD = "SELECT id FROM log_insurer_mail WHERE id='$logId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("LOG_INSURER_MAIL_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE log_insurer_mail SET log_id='$logId',site_id='$siteId',position='$position' WHERE id='$logInsurerMailId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteLogInsurerMail
//
// [DESCRIPTION]:   Delete an entry from LogInsurerMail table
//
// [PARAMETERS]:    $logInsurerMailId
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteLogInsurerMail($logInsurerMailId='')
{
   if(! preg_match("/^\d+$/", $logInsurerMailId))
   {
      $this->strERR = GetErrorString("INVALID_LOG_INSURER_MAIL_ID_FIELD");
      return false;
   }

   // check if logID exists
   $this->lastSQLCMD = "SELECT id FROM log_insurer_mail WHERE id='$logInsurerMailId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("LOG_INSURER_MAIL_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM log_insurer_mail WHERE id='$logInsurerMailId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLogInsurerMail
//
// [DESCRIPTION]:   Read data from Logs table and put it into an array variable
//
// [PARAMETERS]:    $logInsurerMailId
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLogInsurerMail($logInsurerMailId='')
{
   if(! preg_match("/^\d+$/", $logInsurerMailId))
   {
      $this->strERR = GetErrorString("INVALID_LOGID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM log_insurer_mail WHERE id='$logInsurerMailId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("LOGINSURERMAILID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]             = $this->dbh->GetFieldValue("id");
   $arrayResult["log_id"]         = $this->dbh->GetFieldValue("log_id");
   $arrayResult["site_id"]        = $this->dbh->GetFieldValue("site_id");
   $arrayResult["position"]       = $this->dbh->GetFieldValue("position");
   $arrayResult["date"]           = $this->dbh->GetFieldValue("date");
   $arrayResult["time"]           = $this->dbh->GetFieldValue("time");


   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLogInsurerMailIdByLogId
//
// [DESCRIPTION]:   get log id from logs table by filename
//
// [PARAMETERS]:    $logId
//
// [RETURN VALUE]:  $logID|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-26
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLogInsurerMailIdByLogId($logId='')
{
   if(empty($logId))
   {
      $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM log_insurer_mail WHERE log_id='$logId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
      return false;

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLogInsurerMailIdByLogId
//
// [DESCRIPTION]:   get log id from logs table by filename
//
// [PARAMETERS]:    $logId
//
// [RETURN VALUE]:  $logID|false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-26
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLogInsurerMailIdByLogIdAndSiteId($logId='',$siteId='')
{
   if(! preg_match("/\d+/",$logId))
   {
      $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");
      return false;
   }

   if(! preg_match("/\d+/",$siteId))
   {
      $this->strERR = GetErrorString("INVALID_SITE_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM log_insurer_mail WHERE log_id='$logId' AND site_id='$siteId'";
   //print "\n".$this->lastSQLCMD."\n";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if (!$this->dbh->GetRows())
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }
   
   if(! $this->dbh->FetchRows())
      return false;

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllLogInsurerMailByInterval
//
// [DESCRIPTION]:   Read data from Logs table and put it into an array variable
//
// [PARAMETERS]:    $logId='', $siteId='', $position='', $startDate='',$endDate='',$startTime='',$endTime=''
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllLogInsurerMailByInterval($logId='', $siteId='', $position='', $startDate='',$endDate='',$startTime='',$endTime='')
{
   if(! empty($logId))
   {
      if(! preg_match("/^\d+$/", $logId))
      {
         $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");
         return false;
      }
   }

   if(! empty($siteId))
   {
      if(! preg_match("/^\d+$/",$siteId))
      {
         $this->strERR = GetErrorString("INVALID_SITE_ID_FIELD");
         return false;
      }
   }

   if(! empty($position))
   {
      if(! preg_match("/^\d+$/",$position))
      {
         $this->strERR = GetErrorString("INVALID_SITE_ID_FIELD");
         return false;
      }
   }

   if(! empty($startDate))
      if(! $startDate = $this->StdToMysqlDate($startDate))
         return false;

   if(! empty($endDate))
      if(! $endDate = $this->StdToMysqlDate($endDate))
         return false;
   
   if(! empty($startTime))
   {
      if(! preg_match("/^\d{2}\:\d{2}\:\d{2}$/", $startTime))
      {
         $this->strERR = GetErrorString("INVALID_START_TIME_FIELD");
         return false;
      }
   }

   if(! empty($endTime))
   {
      if(! preg_match("/^\d{2}\:\d{2}\:\d{2}$/", $endTime))
      {
         $this->strERR = GetErrorString("INVALID_END_TIME_FIELD");
         return false;
      }
   }

   // we have to check if the startDate and endDate are not the same date and remorting the select mysql 
   //check dates

   list($startYear, $startMonth, $startDay) = split("-", $startDate);
   list($endYear, $endMonth, $endDay)       = split("-", $endDate);

   $diffTimeStamp  = mktime(0,0,0,$endMonth, $endDay, $endYear) - mktime(0,0,0,$startMonth, $startDay, $startYear);

   $case = 0;
   // the same date
   if($diffTimeStamp  == 0 )
      $case = 1;
   else if($diffTimeStamp == 24*60*60 ) // the diff is less or equal than 1 day
      $case = 2;
   else // the diff is more than 1 day
      $case = 3;

   $this->lastSQLCMD = "SELECT * FROM log_insurer_mail";

   switch($case)
   {
      case '1':

         if($startDate || $endDate || $startTime || $endTime)
            $this->lastSQLCMD .=" WHERE 1";

         if($startDate)
            $this->lastSQLCMD .= " AND log_insurer_mail.date='$startDate' ";
         else if($endDate)
            $this->lastSQLCMD .= " AND log_insurer_mail.date='$endDate' ";
         
         if($startTime)
            $this->lastSQLCMD .= " AND log_insurer_mail.time>='$startTime' ";
         
         if($endTime)
            $this->lastSQLCMD .= " AND log_insurer_mail.time<'$endTime' ";
         break;

      case '2':
         if($startDate || $endDate || $startTime || $endTime)
            $this->lastSQLCMD .=" WHERE 1";
         
         if($startDate)
            $this->lastSQLCMD .= " AND ((log_insurer_mail.date='$startDate' ";
         
         if($startTime)
            $this->lastSQLCMD .= " AND log_insurer_mail.time>='$startTime') ";
         else
            $this->lastSQLCMD .= " ) ";

         if($endDate)
            $this->lastSQLCMD .= " OR (log_insurer_mail.date='$endDate' ";

         if($endTime)
            $this->lastSQLCMD .= " AND log_insurer_mail.time<'$endTime')) ";
         else
            $this->lastSQLCMD .= " )) ";

         break;

      case '3':
         if($startDate || $endDate || $startTime || $endTime)
            $this->lastSQLCMD .=" WHERE 1";
         
         if($startDate)
            $this->lastSQLCMD .= " AND ((log_insurer_mail.date='$startDate' ";

         if($startTime)
            $this->lastSQLCMD .= " AND log_insurer_mail.time>='$startTime') ";
         else
            $this->lastSQLCMD .= " ) ";

         if($endDate)
            $this->lastSQLCMD .= " OR (log_insurer_mail.date='$endDate' ";

         if($endTime)
            $this->lastSQLCMD .= " AND log_insurer_mail.time<'$endTime') ";
         else
            $this->lastSQLCMD .= " ) ";

         $endDateInter   = date("Y-m-d", mktime(0,0,0,$endMonth, $endDay - 1, $endYear));
         $startDateInter = date("Y-m-d", mktime(0,0,0,$startMonth, $startDay + 1, $startYear));

         if($startDate)
            $this->lastSQLCMD .= " OR (log_insurer_mail.date>='$startDateInter' ";
         else
            $this->lastSQLCMD .= " ) ";

         if($endDate)
            $this->lastSQLCMD .= " AND log_insurer_mail.date<='$endDateInter')) ";
         else
            $this->lastSQLCMD .= " )) ";
         
         break;
   }// end switch($case)

   if(! empty($logId))
      $this->lastSQLCMD .=" AND log_insurer_mail.log_id='".$logId."' "; 
      
   if(! empty($siteId))
      $this->lastSQLCMD .=" AND log_insurer_mail.site_id='".$siteId."' ";

   if(! empty($position))
      $this->lastSQLCMD .=" AND log_insurer_mail.position='".$position."' ";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("LOG_INSURER_MAIL_ID_NOT_FOUND");
      return false;
   }

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;

      $arrayResult[$index]["id"]            = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["log_id"]        = $this->dbh->GetFieldValue("log_id");
      $arrayResult[$index]["site_id"]       = $this->dbh->GetFieldValue("site_id");
      $arrayResult[$index]["position"]      = $this->dbh->GetFieldValue("position");
      $arrayResult[$index]["date"]          = $this->dbh->GetFieldValue("date");
      $arrayResult[$index]["time"]          = $this->dbh->GetFieldValue("time");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Print the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}
} // end of CLogInsurerMail class
?>

<?php

#########################
#                       #
#     HOME SECTION      #
#                       #
#########################

// SQL TABLE DEFINITIONS

//LEAD SYSTEM MENTINANCE TABLES
define("SQL_SET_OFFLINE_SITES", "set_offline_sites"); // Set offline sites - mainly  used for lead systems

//AFFILIATES SYSTEM
define("SQL_COOKIE_STATS", "cookie_stats"); // Cookie stats table
define("SQL_COOKIES", "cookies"); // Cookies table
define("SQL_AFFILIATES", "affiliates"); // Affiliate table
define("SQL_USERS", "users"); // User table
define("SQL_QZQUOTE_AFFILIATES", "qzquote_affiliates"); // Quotezone affiliates table


// QUOTE SYSTEM
define("SQL_QZQUOTE_TYPES", "qzquote_types"); // QzQuotes types table
define("SQL_QZQUOTES", "qzquotes"); // QzQuotes table
define("SQL_COMPARENI_QZQUOTES", "compareni_qzquotes"); // Compareni QzQuotes table
define("SQL_LOGS", "logs"); // Logs table
define("SQL_QUOTE_SERVER_LOGS", "quote_webserver_logs"); // quote webserver logs table
define("SQL_QUOTE_STATUS", "quote_status"); // Quote status table
define("SQL_QUOTE_TIMING", "quote_timing"); // Quote timing table
define("SQL_QUOTE_ERRORS", "quote_errors"); // Quote errors table
define("SQL_PLUGIN_CACHE_ERRORS",'quote_errors_message_cache'); //Quote errors cache table

define("SQL_QUOTES", "quotes"); //  Quotes table
define("SQL_QUOTE_DETAILS", "quote_details");// Quote Details table

// QUOTE MAIL SYSTEM
define("SQL_SKIP_QUOTE_MAIL_INFO", "skip_quote_mail_info");// Skip quote mail info table
define("SQL_MAIL_INFO", "mail_info"); // Mail info table
define("SQL_EMAIL_STORE", "email_store"); // Email store table
define("SQL_EMAIL_STORE_OLD", "email_store_old"); // Email store table
define("SQL_EMAIL_OUTBOUNDS", "email_outbounds"); // Email store table
define("SQL_NEWS_EMAIL_CAMPAIGNS", "news_email_campaigns"); // News email campaings table

// QUOTE USER SYSTEM
define("SQL_QUOTE_USER_DETAILS", "home_quote_user_details"); // Quote user details table
define("SQL_QUOTE_USERS", "quote_users"); //  Quote users table

// QUOTE SCANNINGS SYSTEM
define("SQL_QUOTE_SCANNINGS", "quote_scannings"); // Quote scannings table

//SITE SYSTEM
define("SQL_SESSION_PARAMS", "session_params"); // Session params table
define("SQL_SITES", "sites"); // Sites table
define("SQL_URL_PARAMS", "url_params");// Url params table
define("SQL_URLS", "urls"); // Urls table
define("SQL_BUSINESS_CODES", "EMP_CODES"); // Business codes table
define("SQL_OCCUPATION_CODES", "OCC_CODES"); // Occupation codes table
define("SQL_PCODE_DETAILS", "pcode_details"); // Postcode details table
define("SQL_PCODE_STREETS", "u_pcode_streets"); // Postcode streets table
define("SQL_POST_CODES", "post_codes"); // Postcode table
define("SQL_SKIP_POSTCODES", "skip_postcodes"); // Skip postcodes table
define("SQL_QUIT", "quit"); // Quit messages table

// we have to define into help script of each section like car van home -> table name
//define("SQL_HELP", "car_help"); // Car help table
//define("SQL_HELP", "van_help"); // Van help table
//define("SQL_HELP", "home_help"); // Home help table

define("SQL_TELL_A_FRIEND", "tell_a_friend"); // Tell a friend
define("SQL_NEWS_LETTER", "news_letter");// News letter table

//ESCAPE QUOTE MAIL
define("SQL_EMAIL_STORE", "email_store"); // Email Store
define("SQL_QUOTE_EMAIL_INFO", "skip_quote_mail_info"); // Skip quote email info


// QUOTE TRACKING SYSTEM
define("SQL_TRACK_URLS", "track_urls"); // Track urls table
define("SQL_TRACKER", "tracker"); // Tracker table
define("SQL_TRACKING_HOSTS", "tracking_hosts"); // Tracking hosts table
define("SQL_TRACKING_PATHS", "tracking_paths"); // Tracking paths table
define("SQL_TRACKING_QUERIES", "tracking_queries"); // Tracking queries table
define("SQL_TRACKING_SCHEMES", "tracking_schemes"); // Tracking schemes table
define("SQL_TRACKING_SOURCE", "tracking_source"); // Tracking source table
define("SQL_TRACKING_SOURCE_NAMES", "tracking_source_names"); // Tracking source names table
define("SQL_TRACKINGS", "trackings"); // Trackings table
define("SQL_TRACKINGS_INOUT", "trackings_inout"); // Trackings inout table


// QUOTE STATS
define("SQL_QUOTE_STATS", "quote_stats"); // Quote Stats table
define("SQL_QUOTE_STATS_DETAILS", "quote_stats_details"); // Quote Stats Details table

?>

<?php

/*****************************************************************************/
/*                                                                           */
/*  CNewLetter class interface
/*                                                                           */
/*  (C) 2004 Eugen Savin (seugen@abc.ro)                                     */
/*                                                                           */
/*****************************************************************************/


define("EMAIL_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CNewLetter
//
// [DESCRIPTION]:  CNewLetter class interface
//
// [FUNCTIONS]:    int  AddNewLetter($name='', $email='')
//                 bool DeleteNewLetter($emailID=0)
//                 bool GetNewLetter($emailID=0, &$arrayResult)
//                 bool GetAllNewLetters(&$arrayResult)
//
//                   Close();
//                 GetError();
//
// [CREATED BY]:   Eugen SAVIN (seugen@abc.ro) 2004-07-12
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CNewLetter
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CNewLetter
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function CNewLetter($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddNewLetter
//
// [DESCRIPTION]:   Add new entry to the news_letter table
//
// [PARAMETERS]:    $name='', $email=''
//
// [RETURN VALUE]:  emailID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function AddNewLetter($name="",$email="",$date='now()')
{
   if(empty($name))
   {
      $this->strERR = GetErrorString("INVALID_NAME_FIELD");
      return 0;
   }

   if(empty($email))
   {
      $this->strERR = GetErrorString("INVALID_EMAIL_FIELD");
   }

   $this->lastSQLCMD = "INSERT INTO ".SQL_NEWS_LETTER." (name,email,cr_date) VALUES ('$name','$email',$date)";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteNewLetter
//
// [DESCRIPTION]:   Delete an entry from news_letter table
//
// [PARAMETERS]:    $emailID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]/*   if(! $email->GetAllNewLetters($resNewLetters))
//
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function DeleteNewLetter($emailID)
{
   if(! preg_match("/^\d+$/", $emailID))
   {
      $this->strERR = GetErrorString("INVALID_EMAILID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_NEWS_LETTER." WHERE id='$emailID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("EMAILID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_NEWS_LETTER." WHERE id='$emailID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllNewLetters
//
// [DESCRIPTION]:   Read data from news_letter table and put it into an array variable
//                  key = emailID, value = array ("name"  => name
//                                                  "email" => email)
//
// [PARAMETERS]:    &$arrayResult
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetAllNewLetters(&$arrayResult)
{
   $this->lastSQLCMD = "SELECT * FROM ".SQL_NEWS_LETTER."";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("EMAILS_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $emailResult["name"]  = $this->dbh->GetFieldValue("name");
      $emailResult["email"] = $this->dbh->GetFieldValue("email");

      $arrayResult[$this->dbh->GetFieldValue("id")] = $emailResult;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetNewLetter
//
// [DESCRIPTION]:   Read data from news_letter table and put it into an array variable
//
// [PARAMETERS]:    $emailID, &$arrayResult
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetNewLetter($emailID=0,&$arrayResult)
{
   if(! preg_match("/^\d+$/", $emailID))
   {
      $this->strERR = GetErrorString("INVALID_EMAILID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_NEWS_LETTER." WHERE id='$emailID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("EMAILID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]    = $this->dbh->GetFieldValue("id");
   $arrayResult["name"]  = $this->dbh->GetFieldValue("name");
   $arrayResult["email"] = $this->dbh->GetFieldValue("email");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

}// end of CNewLetter class

?>
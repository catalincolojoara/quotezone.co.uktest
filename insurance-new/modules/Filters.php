<?php
/*****************************************************************************/
/*                                                                           */
/*  CFilters class interface                                              */
/*                                                                           */
/*  (C) 2005  (florin@acrux.biz)                              */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CFilters
//
// [DESCRIPTION]:  CFilters class interface
//
// [FUNCTIONS]:
//
//                 Close();
//                 GetError();
//                 ShowError();
//
// [CREATED BY]:   Florin (florin@acrux.biz)
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CFilters
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CFilters
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:     (florin@acrux.biz)
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CFilters($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]:
//
// [DESCRIPTION]:
//
// [PARAMETERS]:
//
// [RETURN VALUE]:
//
// [CREATED BY]:    Florin (florin@acrux.biz)
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddFilter($type='general',$key="",$msg="",$prio=1)
{

   if (empty($type))
   {
      $this->strERR .= GetErrorString("INVALID_TYPE_FIELD");
      return false;
   }

   if (empty($key))
   {
      $this->strERR .= GetErrorString("INVALID_KEY_FIELD");
      return false;
   }

   if (empty($msg))
   {
      $this->strERR .= GetErrorString("INVALID_MSG_FIELD");
      return false;
   }

   if(! preg_match('/\d/i',$prio))
   {
      $this->strERR .= GetErrorString("INVALID_PRIO_FIELD")."\n";
      return false;
   }

   $this->lastSQLCMD = "INSERT INTO filters (type,`key`,message,priority) VALUES ('$type','$key','$msg','$prio')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFiltersByDate
//
// [DESCRIPTION]:
//
// [PARAMETERS]:
//
// [RETURN VALUE]:
//
// [CREATED BY]:   Florin (florin@acrux.biz)
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateFilter($id,$type='general', $key='', $message='', $prio='1')
{

   $this->lastSQLCMD = "SELECT id FROM filters WHERE id='$id'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE filters SET type='$type',`key`='$key',message='$message',priority='$prio' WHERE id='$id'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFiltersByDate
//
// [DESCRIPTION]:
//
// [PARAMETERS]:
//
// [RETURN VALUE]:
//
// [CREATED BY]:   Florin (florin@acrux.biz)
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFilters()
{

   $this->lastSQLCMD = "SELECT * from filters";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

	$arrayResult = array();

	while ($this->dbh->MoveNext())
	{
		$index = $this->dbh->GetFieldValue('id');
		$arrayResult[$index]['type']     = $this->dbh->GetFieldValue('type');
		$arrayResult[$index]['key']      = $this->dbh->GetFieldValue('key');
		$arrayResult[$index]['message']  = $this->dbh->GetFieldValue('message');
		$arrayResult[$index]['priority'] = $this->dbh->GetFieldValue('priority');
	}


   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFiltersByDate
//
// [DESCRIPTION]:
//
// [PARAMETERS]:
//
// [RETURN VALUE]:
//
// [CREATED BY]:   Florin (florin@acrux.biz)
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFilterByKey($key = "")
{
	if (empty($key))
		return;

   $this->lastSQLCMD = "SELECT * from filters where `key`='$key'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

	$arrayResult = array();

	$arrayResult['id']       = $this->dbh->GetFieldValue('id');
	$arrayResult['type']     = $this->dbh->GetFieldValue('type');
	$arrayResult['key']      = $this->dbh->GetFieldValue('key');
	$arrayResult['message']  = $this->dbh->GetFieldValue('message');
	$arrayResult['priority'] = $this->dbh->GetFieldValue('priority');


   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFiltersByID
//
// [DESCRIPTION]:
//
// [PARAMETERS]:
//
// [RETURN VALUE]:
//
// [CREATED BY]:   Florin (florin@acrux.biz)
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFiltersByType($type)
{

   if(empty($type))
   {
      $this->strERR .= GetErrorString("INVALID_TYPE_FIELD")."\n";
      return false;
   }

   $this->lastSQLCMD = "SELECT * from filters where type='$type'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }


	$arrayResult = array();

	while ($this->dbh->MoveNext())
	{
		$index = $this->dbh->GetFieldValue('id');
		$arrayResult[$index]['type']     = $this->dbh->GetFieldValue('type');
		$arrayResult[$index]['key']      = $this->dbh->GetFieldValue('key');
		$arrayResult[$index]['message']  = $this->dbh->GetFieldValue('message');
		$arrayResult[$index]['priority'] = $this->dbh->GetFieldValue('priority');
	}


   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:     ()
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:     ()
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Show the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:     ()
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}




function OrderFiltersByPrio($filterKeyArray)
{
	if (!$filterKeyArray)
		return;

	foreach ($filterKeyArray as $key=>$filterKey)
	{
		$filterPrio = $this->GetFilterPrio($filterKey);
		$tempArray[$filterPrio][] = $filterKey;
	}

	ksort($tempArray);
	$temp2Array = array();
	foreach ($tempArray as $prioKey=>$prioMsgList)
	{
		sort($prioMsgList);
		//$temp2Array[$prioKey] = array_filter(array_unique($prioMsgList),'strlen');
		$temp2Array[$prioKey] = array_unique($prioMsgList);
	}

	$finalArray = array();
	foreach ($temp2Array as $prioKey=>$prioMsgList)
		foreach ($prioMsgList as $prioMsg)
			$finalArray[] = $prioMsg;

	return $finalArray;
}

function GetFilterPrio($filterKey)
{
	$filter = $this->GetFilterByKey($filterKey);

	return $filter['priority'];
}


}//end class CFilters

?>

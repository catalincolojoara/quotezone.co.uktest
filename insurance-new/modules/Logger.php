<?php

/*****************************************************************************/
/*                                                                           */
/*  Logger class implementation                                               */
/*                                                                           */
/*  (C) 2004 Eugen Savin (eugen@claspsolutions.com)                          */
/*                                                                           */
/*****************************************************************************/

define("LOGGER_INCLUDED", "1");

include_once "File.php";
include_once "Date.php";

// if(DEBUG_MODE)
//    error_reporting(E_ALL);
// else
//    error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CLogger
//
// [DESCRIPTION]:  Log support, PHP version
//
// [FUNCTIONS]:    bool Info($msg = "");
//                 bool Warn($msg = "");
//                 bool Error($msg = "");
//
//                 void StoreMsg($msg = "");
//                 string GetLastMsg();
//
//                 void SetLogFile($fileName = "");
//                 string GetLogFile();
//
// [CREATED BY]:   Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CLogger
{
   var $lastLog;
   var $logFile;

   var $file;
   var $date;

   var $strERR;

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CLogger
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    $logFileName = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CLogger($logFileName = "")
{
   if(! empty($logFileName))
      $this->logFile = $logFileName;
   else
      $this->logFile = "phpLogger.log";

   $this->lastLog = "";

   $this->file = new CFile();
   $this->date = new CDate();

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Info
//
// [DESCRIPTION]:   Logs an info message into the log file
//
// [PARAMETERS]:    $msg = ""
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Info($msg = "")
{
   if(empty($msg))
      return true;

   $this->StoreMsg($msg);

   if(! $this->file->Open($this->logFile, "a+"))
   {
      $this->strERR = $this->file->GetError();
      return false;
   }

   $msgLog = split("\n", $msg);
   foreach($msgLog as $msg)
   {
      if($msg == "")
         continue;

      if($this->file->Write($this->date->GetDateTime(). " - INFO  - $msg\n") < 0)
      {
         $this->strERR = $this->file->GetError();
         $this->file->Close();
         return false;
      }
   }
   
   $this->file->Close();

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Warn
//
// [DESCRIPTION]:   Logs a warning message into the log file
//
// [PARAMETERS]:    $msg = ""
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Warn($msg = "")
{
   if(empty($msg))
      return true;

   $this->StoreMsg($msg);

   if(! $this->file->Open($this->logFile, "a+"))
   {
      $this->strERR = $this->file->GetError();
      return false;
   }

   $msgLog = split("\n", $msg);
   foreach($msgLog as $msg)
   {
      if($msg == "")
         continue;
         
      if($this->file->Write($this->date->GetDateTime(). " - WARN  - $msg\n") < 0)
      {
         $this->strERR = $this->file->GetError();
         $this->file->Close();
         return false;
      }
   }
   
   $this->file->Close();

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Error
//
// [DESCRIPTION]:   Logs an error message into the log file
//
// [PARAMETERS]:    $msg = ""
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Error($msg = "")
{
   if(empty($msg))
      return true;

   $this->StoreMsg($msg);

   if(! $this->file->Open($this->logFile, "a+"))
   {
      $this->strERR = $this->file->GetError();
      return false;
   }

   $msgLog = split("\n", $msg);
   foreach($msgLog as $msg)
   {
      if($msg == "")
         continue;

      if($this->file->Write($this->date->GetDateTime(). " - ERROR - $msg\n") < 0)
      {
         $this->strERR = $this->file->GetError();
         $this->file->Close();
         return false;
      }
   }
   
   $this->file->Close();

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StoreMsg
//
// [DESCRIPTION]:   Stores the message
//
// [PARAMETERS]:    $msg = ""
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StoreMsg($msg = "")
{
   $this->lastLog = $msg;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLastMsg
//
// [DESCRIPTION]:   Returns the last logged message
//
// [PARAMETERS]:    $msg = ""
//
// [RETURN VALUE]:  The last logged message as string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLastMsg()
{
   return $this->lastLog;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetLogFile
//
// [DESCRIPTION]:   Sets the log file name
//
// [PARAMETERS]:    $fileName = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetLogFile($fileName = "")
{
   if(empty($fileName))
      return;

   $this->logFile = $fileName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLogFile
//
// [DESCRIPTION]:   Returns the log file name
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  The log file name as string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLogFile()
{
   return $this->logFile;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
   return $this->strERR;
}

} // end of CLogger class

?>

<?php
/*****************************************************************************/
/*                                                                           */
/*  CScanner class interface                                                 */
/*                                                                           */
/*  (C) Eugen Savin (seugen@acrux.biz) 2005-10-19                            */
/*                                                                           */
/*****************************************************************************/
define("SCANNER_INCLUDED", "1");
include_once "File.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CMailScanner
//
// [DESCRIPTION]:  CMailScanner class interface
//
// [FUNCTIONS]:    void  SetDelayScanning($delayScanning = 0);
//                 int   GetDelayScanning();
//                 void  StartDaemon();
//                 void  StopDaemon();
//                 void  MarkAlive();
//                 bool  IsAlive();
//
// [CREATED BY]:   Eugen Savin (seugen@acrux.biz) 2005-10-19
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CMailScanner
{
    var $lockFileName;
    var $stopFileName;
    var $objFile;
    var $lastTimeRun;
    var $delayScanning;

    var $strErr;

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CScanner
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2005-10-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CMailScanner()
{
   $this->lockFileName         = "scanner/mail.lock";
   $this->stopFileName         = "scanner/mail.stop";

   $this->objFile              = new CFile();
   $this->lastTimeRun          = 0;
   $this->delayScanning        = 300; //seconds

   $this->strErr               = '';
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DelayScanning
//
// [DESCRIPTION]:   Set the delay in secconds between 2 scannings
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi Istvancsek (gabi@acrux.biz) 2005-10-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetDelayScanning($delayScanning = 0)
{
   $this->delayScanning = $delayScanning;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDelayScanning
//
// [DESCRIPTION]:   Get the delay in secconds between 2 scannings
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi Istvancsek (gabi@acrux.biz) 2005-10-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDelayScanning($delayScanning = 0)
{
   return $this->delayScanning;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MarkAlive
//
// [DESCRIPTION]:   Mark this daemon is running
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi Istvancsek (gabi@acrux.biz) 2005-10-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MarkAlive()
{
   if(! $this->objFile->Touch($this->lockFileName))
      print $this->objFile->GetError();
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StartDemon
//
// [DESCRIPTION]:   Mark this daemon is running
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi Istvancsek (gabi@acrux.biz) 2005-10-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StartDaemon($type = '')
{
   set_time_limit(0);

   $this->lastTimeRun = time();

   if($this->objFile->Exists($this->lockFileName))
      return;

   $this->objFile->Delete($this->stopFileName);

   while(true)
   {
      $this->MarkAlive();

      if($this->objFile->Exists($this->stopFileName))
      {
         if($this->objFile->Delete($this->stopFileName) && $this->objFile->Delete($this->lockFileName))
            exit(0);
      }

      $time = time();
      if(($time - $this->lastTimeRun) <= $this->delayScanning)
      {
         sleep(1);
         continue;
      }

      exec('/usr/bin/php -q scanner/SendTopQuotesMail.php '.$type);

      print "Delay : ".$this->delayScanning." Sending the emails : Date ".date('Y-m-d')." Hour ".date('H:i:s')."\n";

      $this->lastTimeRun = time();

      $this->MarkAlive();

   }// end while(true)

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StopDemon
//
// [DESCRIPTION]:   Stop this daemon
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi Istvancsek (gabi@acrux.biz) 2005-10-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StopDaemon()
{
   if(! $this->objFile->Touch($this->stopFileName))
      print $this->objFile->GetError();

}

} // end CScanner

?>

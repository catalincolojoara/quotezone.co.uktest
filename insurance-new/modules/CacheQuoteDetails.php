<?php

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CCacheQuoteDetails
//
// [DESCRIPTION]:  CCacheQuoteDetails class interface
//
// [FUNCTIONS]:
//                 true | false CacheQuoteDetails($quoteTypeID="", $quoteYear="", $quoteMonth="")
//                 string GetError()
//
// [CREATED BY]:   Moraru Valeriu (vali@acrux.biz) 2010-10-27
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
error_reporting(0);

include_once("globals.inc");
include_once("errors.inc");
include_once("sql.inc");
include_once("MySQL.php");

class CCacheQuoteDetails
{
   var $dbh;
   var $strERR;
   var $closeDB;

   function CCacheQuoteDetails($dbh=0)
   {
      if($dbh)
      {
         $this->dbh     = $dbh;
         $this->closeDB = false;
      }
      else
      {
         // default configuration
         $this->dbh = new CMySQL();

         if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
         {
            $this->strERR = $this->dbh->GetError();
            return;
         }

         $this->closeDB = true;
      }

      $this->strERR  = "";
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CacheQuoteDetails($quoteTypeID="", $quoteYear="", $quoteMonth="")
   //
   // [DESCRIPTION]:   Cache quote details
   //
   // [PARAMETERS]:    $quoteTypeID="", $quoteYear="", $quoteMonth=""
   //
   // [RETURN VALUE]:  true|false
   //
   // [CREATED BY]:    Moraru Valeriu (vali@acrux.biz) 2010-10-27
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CacheQuoteDetails($quoteTypeID="", $quoteYear="", $quoteMonth="")
   {
      if(! preg_match("/^\d+$/", $quoteTypeID))
      {
         $this-> strERR = GetErrorString("INVALID_QUOTE_TYPE_ID");
         return false;
      }

      if(! preg_match("/^\d+$/", $quoteYear))
      {
         $this-> strERR = GetErrorString("INVALID_QUOTE_TYPE_ID");
         return false;
      }

      if(! preg_match("/^\d+$/", $quoteMonth))
      {
         $this-> strERR = GetErrorString("INVALID_QUOTE_TYPE_ID");
         return false;
      }

      $sqlCmd .= "
         SELECT 
            l.id AS log_id,
            l.date AS quote_date,
            l.time AS quote_time,
            l.filename AS filename,
            l.host_ip AS host_ip,
            l.quote_user_id AS quote_user_id,
            LOWER(qu.first_name) AS first_name,
            LOWER(qu.last_name) AS last_name,
            qu.birth_date AS birth_date,
            LOWER(
               (CASE qzqt.name
                  WHEN 'car' THEN qud.post_code
                  WHEN 'van' THEN qud.post_code
                  WHEN 'home' THEN hqud.post_code
                  WHEN 'bike' THEN bqud.post_code
                  WHEN 'travel' THEN tqud.post_code
                  WHEN 'breakdown' THEN bdqud.post_code
                  WHEN 'loans' THEN lqud.post_code
                  WHEN 'pet' THEN CONCAT(pqud.postcode_prefix,' ',pqud.postcode_number)
                  WHEN 'caravan' THEN crqud.post_code
                  WHEN 'motorhome' THEN moqud.post_code
                  WHEN 'life' THEN liqud.post_code
                  WHEN 'health' THEN hequd.post_code
                  WHEN 'campervan' THEN cvqud.post_code
                  WHEN 'trailer-tent' THEN ttqud.post_code
                  WHEN 'boat' THEN boqud.post_code
                  WHEN 'energy' THEN enqud.post_code
                  WHEN 'taxi' THEN taqud.post_code
                  WHEN 'minibus' THEN miqud.post_code
                  WHEN 'limo' THEN limqud.post_code
                  WHEN 'coach' THEN coqud.post_code
                  WHEN 'horsebox' THEN hbqud.post_code
                  WHEN 'icecreamvan' THEN icvqud.post_code
                  WHEN 'cateringvan' THEN cavqud.post_code
                  WHEN 'drivingschool' THEN dsqud.post_code
                  WHEN 'motortrade' THEN mtqud.post_code
                  WHEN 'parkhome' THEN phqud.post_code
                  WHEN 'landlords' THEN ldqud.post_code
                  WHEN 'courier' THEN cuqud.post_code
                  WHEN 'gap' THEN gaqud.post_code
                  WHEN 'motor-fleet' THEN mfqud.post_code
                  WHEN 'taxi-fleet' THEN tfqud.post_code
                  WHEN 'truck' THEN trqud.post_code
                  WHEN 'office' THEN ofqud.post_code
                  WHEN 'shop' THEN shqud.post_code
                  WHEN 'pub' THEN puqud.post_code
                  WHEN 'surgery' THEN suqud.post_code
                  WHEN 'polish' THEN poqud.post_code
                  WHEN 'importcar' THEN icqud.post_code
                  WHEN 'professionalindemnity' THEN piqud.post_code
                  WHEN 'kitcar' THEN kcqud.post_code
                  WHEN 'tradesman' THEN tmqud.post_code
                  WHEN '4x4' THEN 44qud.post_code
                  WHEN 'classiccar' THEN ccqud.post_code
                  WHEN 'incomeprotection' THEN ipqud.post_code
                  WHEN 'publicliability' THEN plqud.post_code
                  WHEN 'convicteddriver' THEN cdqud.post_code
                  WHEN 'privatemedicalinsurance' THEN pmqud.post_code
                  WHEN 'mortgageprotection' THEN mpqud.post_code
                  WHEN 'carehome' THEN chqud.post_code
                  WHEN 'holidayhome' THEN hhqud.post_code
               END)
            )
            as post_code,

            LOWER(
               (CASE qzqt.name
                  WHEN 'car' THEN qud.email
                  WHEN 'van' THEN qud.email
                  WHEN 'home' THEN hqud.email
                  WHEN 'bike' THEN bqud.email
                  WHEN 'travel' THEN tqud.email_address
                  WHEN 'breakdown' THEN bdqud.email_address
                  WHEN 'loans' THEN lqud.email
                  WHEN 'pet' THEN pqud.email_address
                  WHEN 'caravan' THEN crqud.email_address
                  WHEN 'motorhome' THEN moqud.email_address
                  WHEN 'life' THEN liqud.email_address
                  WHEN 'health' THEN hequd.email_address
                  WHEN 'campervan' THEN cvqud.email_address
                  WHEN 'trailer-tent' THEN ttqud.email_address
                  WHEN 'boat' THEN boqud.email
                  WHEN 'energy' THEN enqud.email
                  WHEN 'taxi' THEN taqud.email_address
                  WHEN 'minibus' THEN miqud.email_address
                  WHEN 'limo' THEN limqud.email_address
                  WHEN 'coach' THEN coqud.email_address
                  WHEN 'horsebox' THEN hbqud.email_address
                  WHEN 'icecreamvan' THEN icvqud.email_address
                  WHEN 'cateringvan' THEN cavqud.email_address
                  WHEN 'drivingschool' THEN dsqud.email_address
                  WHEN 'motortrade' THEN mtqud.email_address
                  WHEN 'parkhome' THEN phqud.email_address
                  WHEN 'landlords' THEN ldqud.email_address
                  WHEN 'courier' THEN cuqud.email_address
                  WHEN 'gap' THEN gaqud.email_address
                  WHEN 'motor-fleet' THEN mfqud.email_address
                  WHEN 'taxi-fleet' THEN tfqud.email_address
                  WHEN 'truck' THEN trqud.email_address
                  WHEN 'office' THEN ofqud.email_address
                  WHEN 'shop' THEN shqud.email_address
                  WHEN 'pub' THEN puqud.email_address
                  WHEN 'surgery' THEN suqud.email_address
                  WHEN 'polish' THEN poqud.email_address
                  WHEN 'importcar' THEN icqud.email_address
                  WHEN 'professionalindemnity' THEN piqud.email_address
                  WHEN 'kitcar' THEN kcqud.email_address
                  WHEN 'tradesman' THEN tmqud.email_address
                  WHEN '4x4' THEN 44qud.email_address
                  WHEN 'classiccar' THEN ccqud.email_address
                  WHEN 'incomeprotection' THEN ipqud.email_address
                  WHEN 'publicliability' THEN plqud.email_address
                  WHEN 'convicteddriver' THEN cdqud.email_address
                  WHEN 'privatemedicalinsurance' THEN pmqud.email_address
                  WHEN 'mortgageprotection' THEN mpqud.email_address
                  WHEN 'carehome' THEN chqud.email_address
                  WHEN 'holidayhome' THEN hhqud.email_address
               END)
               ) as `email_address`,

               (CASE qzqt.name
                  WHEN 'car' THEN qud.start_insurance_date
                  WHEN 'van' THEN qud.start_insurance_date
                  WHEN 'home' THEN hqud.start_insurance_date
                  WHEN 'bike' THEN bqud.start_insurance_date
                  WHEN 'travel' THEN tqud.start_date
                  WHEN 'breakdown' THEN bdqud.date_of_insurance_start
                  WHEN 'loans' THEN lqud.start_date
                  WHEN 'pet' THEN pqud.pet_cover_start_date
                  WHEN 'caravan' THEN crqud.date_of_insurance_start
                  WHEN 'motorhome' THEN moqud.date_of_insurance_start
                  WHEN 'life' THEN liqud.date_of_insurance_start
                  WHEN 'health' THEN IF(lcase(hequd.renewal_timeframe) REGEXP 'today',lcase(hequd.renewal_timeframe),CONCAT(REPLACE(lcase(hequd.renewal_timeframe),'_',' '),' months'))
                  WHEN 'campervan' THEN cvqud.date_of_insurance_start
                  WHEN 'trailer-tent' THEN ttqud.date_of_insurance_start
                  WHEN 'boat' THEN boqud.start_insurance_date
                  WHEN 'energy' THEN '-'
                  WHEN 'taxi' THEN taqud.date_of_insurance_start
                  WHEN 'minibus' THEN miqud.date_of_insurance_start
                  WHEN 'limo' THEN limqud.date_of_insurance_start
                  WHEN 'coach' THEN coqud.date_of_insurance_start
                  WHEN 'horsebox' THEN hbqud.date_of_insurance_start
                  WHEN 'icecreamvan' THEN icvqud.date_of_insurance_start
                  WHEN 'cateringvan' THEN cavqud.date_of_insurance_start
                  WHEN 'drivingschool' THEN dsqud.date_of_insurance_start
                  WHEN 'motortrade' THEN mtqud.date_of_insurance_start
                  WHEN 'parkhome' THEN phqud.date_of_insurance_start
                  WHEN 'landlords' THEN ldqud.date_of_insurance_start
                  WHEN 'courier' THEN cuqud.date_of_insurance_start
                  WHEN 'gap' THEN gaqud.date_of_insurance_start
                  WHEN 'motor-fleet' THEN mfqud.date_of_insurance_start
                  WHEN 'taxi-fleet' THEN tfqud.date_of_insurance_start
                  WHEN 'truck' THEN trqud.date_of_insurance_start
                  WHEN 'office' THEN ofqud.date_of_insurance_start
                  WHEN 'shop' THEN shqud.date_of_insurance_start
                  WHEN 'pub' THEN puqud.date_of_insurance_start
                  WHEN 'surgery' THEN suqud.date_of_insurance_start
                  WHEN 'polish' THEN poqud.date_of_insurance_start
                  WHEN 'importcar' THEN icqud.date_of_insurance_start
                  WHEN 'professionalindemnity' THEN piqud.date_of_insurance_start
                  WHEN 'kitcar' THEN kcqud.date_of_insurance_start
                  WHEN 'tradesman' THEN tmqud.date_of_insurance_start
                  WHEN '4x4' THEN 44qud.date_of_insurance_start
                  WHEN 'classiccar' THEN ccqud.date_of_insurance_start
                  WHEN 'incomeprotection' THEN ipqud.date_of_insurance_start
                  WHEN 'publicliability' THEN plqud.date_of_insurance_start
                  WHEN 'convicted_driver' THEN cdqud.date_of_insurance_start
                  WHEN 'pmi' THEN IF(lcase(pmqud.renewal_timeframe) REGEXP 'today',lcase(pmqud.renewal_timeframe),CONCAT(REPLACE(lcase(pmqud.renewal_timeframe),'_',' '),' months'))
                  WHEN 'mortgageprotection' THEN mpqud.date_of_insurance_start
                  WHEN 'carehome' THEN chqud.date_of_insurance_start
                  WHEN 'holidayhome' THEN hhqud.date_of_insurance_start
               END)
               as `start_insurance_date`

           FROM ci_new.logs l 
            LEFT JOIN ci_new.qzquotes qzq ON (qzq.log_id=l.id) 
            LEFT JOIN ci_new.qzquote_types qzqt ON (qzqt.id=qzq.quote_type_id) 
            LEFT JOIN ci_new.quote_users qu ON (l.quote_user_id=qu.id) 
            LEFT JOIN ci_new.quote_user_details qud ON (qud.id=qzq.quote_user_details_id) 
            LEFT JOIN ci_new.home_quote_user_details hqud ON (hqud.id=qzq.quote_user_details_id) 
            LEFT JOIN ci_new.bike_quote_user_details bqud ON (bqud.id=qzq.quote_user_details_id) 
            LEFT JOIN ci_new.travel_quote_user_details tqud ON (tqud.id=qzq.quote_user_details_id) 
            LEFT JOIN ci_new.breakdown_quote_user_details bdqud ON (bdqud.id=qzq.quote_user_details_id) 
            LEFT JOIN ci_new.loans_quote_user_details lqud ON (lqud.id=qzq.quote_user_details_id) 
            LEFT JOIN ci_new.pet_quote_user_details pqud ON (pqud.id=qzq.quote_user_details_id)
            LEFT JOIN ci_new.campervan_quote_user_details cvqud ON (cvqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.caravan_quote_user_details crqud ON (crqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.health_quote_user_details hequd ON (hequd.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.life_quote_user_details liqud ON (liqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.motorhome_quote_user_details moqud ON (moqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.trailer_tent_quote_user_details ttqud ON (ttqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.boat_quote_user_details boqud ON (boqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.energy_quote_user_details enqud ON (enqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.taxi_quote_user_details taqud ON (taqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.minibus_quote_user_details miqud ON (miqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.limo_quote_user_details limqud ON (limqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.coach_quote_user_details coqud ON (coqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.horsebox_quote_user_details hbqud ON (hbqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.icecreamvan_quote_user_details icvqud ON (icvqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.cateringvan_quote_user_details cavqud ON (cavqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.drivingschool_quote_user_details dsqud ON (dsqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.motortrade_quote_user_details mtqud ON (mtqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.park_home_quote_user_details phqud ON (phqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.landlords_quote_user_details ldqud ON (ldqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.courier_quote_user_details cuqud ON (cuqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.gap_quote_user_details gaqud ON (gaqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.motor_fleet_quote_user_details mfqud ON (mfqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.taxi_fleet_quote_user_details tfqud ON (tfqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.truck_quote_user_details trqud ON (trqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.office_quote_user_details ofqud ON (ofqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.shop_quote_user_details shqud ON (shqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.pub_quote_user_details puqud ON (puqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.surgery_quote_user_details suqud ON (suqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.polish_quote_user_details poqud ON (poqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.importcar_quote_user_details icqud ON (icqud.id=qzq.quote_user_details_id)  
            LEFT JOIN ci_new.professionalindemnity_quote_user_details piqud ON (piqud.id=qzq.quote_user_details_id)
            LEFT JOIN ci_new.kitcar_quote_user_details kcqud ON (kcqud.id=qzq.quote_user_details_id)
            LEFT JOIN ci_new.tradesman_quote_user_details tmqud ON (tmqud.id=qzq.quote_user_details_id)
            LEFT JOIN ci_new.4x4_quote_user_details 44qud ON (44qud.id=qzq.quote_user_details_id)
            LEFT JOIN ci_new.classiccar_quote_user_details ccqud ON (ccqud.id=qzq.quote_user_details_id)
            LEFT JOIN ci_new.incomeprotection_quote_user_details ipqud ON (ipqud.id=qzq.quote_user_details_id)
            LEFT JOIN ci_new.publicliability_quote_user_details plqud ON (plqud.id=qzq.quote_user_details_id)
            LEFT JOIN ci_new.convicted_driver_quote_user_details cdqud ON (cdqud.id=qzq.quote_user_details_id)
            LEFT JOIN ci_new.pmi_quote_user_details pmqud ON (pmqud.id=qzq.quote_user_details_id)
            LEFT JOIN ci_new.mortgageprotection_quote_user_details mpqud ON (mpqud.id=qzq.quote_user_details_id)
            LEFT JOIN ci_new.carehome_quote_user_details chqud ON (chqud.id=qzq.quote_user_details_id)
            LEFT JOIN ci_new.holidayhome_quote_user_details hhqud ON (hhqud.id=qzq.quote_user_details_id)

            WHERE l.id=qzq.log_id 
               AND qzq.quote_type_id=$quoteTypeID ";

      $sqlCmd .= " AND l.date >= '".$quoteYear."-".$quoteMonth."-01' AND l.date <= '".$quoteYear."-".$quoteMonth."-31' ";

      $sqlCmd .= " ORDER BY l.date DESC, l.time DESC ";

      //print "\n<br>------------------------------------------------------<br>\n";
      //print($sqlCmd);
      //print "\n<br>------------------------------------------------------<br><br><br>\n";
      //die;

      print "begin SQL:".date('H:i:s')."\n";

      if(! $this->dbh->Exec($sqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      print "end SQL:".date('H:i:s')."\n";

      if($this->dbh->GetRows() == 0)
      {
         return false;
      }

      $k = 0;
      $results = array();

      while($this->dbh->MoveNext())
      {
         $results[$k]["quote_type_id"]        = $quoteTypeID;
         $results[$k]["quote_user_id"]        = $this->dbh->GetFieldValue("quote_user_id");
         $results[$k]["first_name"]           = $this->dbh->GetFieldValue("first_name");
         $results[$k]["last_name"]            = $this->dbh->GetFieldValue("last_name");
         $results[$k]["birth_date"]           = $this->dbh->GetFieldValue("birth_date");
         $results[$k]["email_address"]        = $this->dbh->GetFieldValue("email_address");
         $results[$k]["post_code"]            = $this->dbh->GetFieldValue("post_code");
         $results[$k]["quote_inception_date"] = $this->dbh->GetFieldValue("start_insurance_date");
         $results[$k]["quote_date"]           = $this->dbh->GetFieldValue("quote_date");
         $results[$k]["quote_time"]           = $this->dbh->GetFieldValue("quote_time");
         $results[$k]["log_id"]               = $this->dbh->GetFieldValue("log_id");
         $results[$k]["filename"]             = $this->dbh->GetFieldValue("filename");
         $results[$k]["host_ip"]              = $this->dbh->GetFieldValue("host_ip");

         $k++;
      }

      //print_r($results);
      //die;

      //if(! $this->dbh->Exec("use ci_new_cache;"))
      //{
      //   $this->strERR = $this->dbh->GetError();
      //   return false;
      //}


      $cacheTableName = "ci_new_cache.quote_user_details_quotes_".$quoteYear.$quoteMonth;

      // drop table if exists $tableName and re-create it only on fresh cache on all systems

      if($quoteTypeID == 1)
      {
         $sqlCmdDropTable =<<<EOT
            DROP TABLE IF EXISTS $cacheTableName;
EOT;
         if(! $this->dbh->Exec($sqlCmdDropTable))
         {
           $this->strERR = $this->dbh->GetError();
           return false;
         }

         $sqlCmdCreateTable = <<<EOT
            CREATE TABLE $cacheTableName (
               quote_type_id mediumint(8) unsigned NOT NULL default '0',
               quote_user_id bigint(20) unsigned NOT NULL default '0',
               first_name varchar(128) NOT NULL default '',
               last_name varchar(128) NOT NULL default '',
               birth_date date NOT NULL default '0000-00-00',
               email_address varchar(250) NOT NULL default '',
               post_code varchar(16) NOT NULL default '',
               quote_inception_date date default '0000-00-00',
               quote_date date default '0000-00-00',
               quote_time time default '00:00:00',
               log_id bigint(20) unsigned NOT NULL,
               filename varchar(64) NOT NULL default '',
               host_ip varchar(32) NOT NULL default '',
               KEY `quote_type_id` (`quote_type_id`),
               KEY `quote_user_id` (`quote_user_id`),
               KEY `first_name` (`first_name`),
               KEY `last_name` (`last_name`),
               KEY `birth_date` (`birth_date`),
               KEY `email_address` (`email_address`),
               KEY `post_code` (`post_code`),
               KEY `quote_inception_date` (`quote_inception_date`),
               KEY `quote_date` (`quote_date`)
            ) ENGINE=MyISAM DEFAULT CHARSET=latin1;

EOT;

         if(! $this->dbh->Exec($sqlCmdCreateTable))
         {
            $this->strERR = $this->dbh->GetError();
            return false;
         }
      }// if $quoteTypeID == 1 (this means started fresh cache results)

      foreach($results as $index => $detailsArray)
      {
         print "-$index";
         $sqlCmdInsert = <<<EOT
         INSERT INTO $cacheTableName 
            (
               quote_type_id,
               quote_user_id,
               first_name,
               last_name,
               birth_date,
               email_address,
               post_code,
               quote_inception_date,
               quote_date,
               quote_time,
               log_id,
               filename,
               host_ip
            ) 
            VALUES 
            (
            '$detailsArray[quote_type_id]',
            '$detailsArray[quote_user_id]',
            '$detailsArray[first_name]',
            '$detailsArray[last_name]',
            '$detailsArray[birth_date]',
            '$detailsArray[email_address]',
            '$detailsArray[post_code]',
            '$detailsArray[quote_inception_date]',
            '$detailsArray[quote_date]',
            '$detailsArray[quote_time]',
            '$detailsArray[log_id]',
            '$detailsArray[filename]',
            '$detailsArray[host_ip]'
            ) ;

EOT;

          //print $sqlCmdInsert;
          //die;

         if(! $this->dbh->Exec($sqlCmdInsert))
         {
            $this->strERR = $this->dbh->GetError();
            return false;
         }
      }

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetError
   //
   // [DESCRIPTION]:   Retrieve the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Adrian AXENTE (adi@acrux.biz) 2006-08-29
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetError()
   {
      return $this->strERR;
   }

}

?>

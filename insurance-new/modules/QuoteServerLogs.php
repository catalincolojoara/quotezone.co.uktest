<?php
/*****************************************************************************/
/*                                                                           */
/*  CQuoteServerLogs class interface                                         */
/*                                                                           */
/*  (C) 2008 Sturza Ciprian (cipi@acrux.biz)                                 */
/*                                                                           */
/*****************************************************************************/

define("Log_INCLUDED", "1");

include_once "errors.inc";
include_once "MySQL.php";

error_reporting(DEBUG_MODE ? 1 : 0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuoteServerLogs
//
// [DESCRIPTION]:  CQuoteServerLogs class interface
//
// [FUNCTIONS]:      true    | false  AssertQuoteServerLog($logID='', $serverIp='', $confirmed='')
//                   string  | false  StdToMysqlDate($date='')
//                   integer | false  AddQuoteServerLog($logID='', $serverIp='', $confirmed='N', $date='', $time='')
//                   true    | false  UpdateQuoteServerLog($quoteServerID ='', $logID='', $serverIp='', $confirmed='')
//                   true    | false  DeleteQuoteServerLog($quoteServerID='')
//                   array   | false  GetQuoteServerLog($quoteServerID='')
//                   array   | false  GetQuoteServerLogByLogID($logID='')
//                   Close()
//                   GetError()
//
// [CREATED BY]:     Sturza Ciprian (cipi@acrux.biz) 2008-05-05
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CQuoteServerLogs
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last Log error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuoteServerLogs
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Sturza Ciprian (cipi@acrux.biz) 2008-05-05
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQuoteServerLogs($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuoteServerLog
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    $logID, $quoteTypeID, $serverIp, $confirmed, $date, $time
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Sturza Ciprian (cipi@acrux.biz) 2008-05-05
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertQuoteServerLog($logID='', $quoteTypeID='', $serverIp='', $confirmed='')
{
   $this->strERR = '';

   if(! preg_match("/\d+/",$logID))
      $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");

   if(! preg_match("/\d+/",$quoteTypeID))
      $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");

   if(empty($serverIp))
      $this->strERR .= GetErrorString("INVALID_SERVER_IP_FIELD");

   if(($confirmed != 'N') && ($confirmed != 'Y'))
      $this->strERR .= GetErrorString("INVALID_CONFIRMED_FIELD");

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Sturza Ciprian (cipi@acrux.biz) 2008-05-05
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   if(! preg_match("/\d{1,2}\/\d{1,2}\/\d{4}/i",$date))
      $this->strERR = GetErrorString("INVALID_LOG_STD_TO_MYSQL_DATE");

   if(! empty($this->strERR))
      return false;

   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteServerLog
//
// [DESCRIPTION]:   Add new entry to the quote_webserver_logs table
//
// [PARAMETERS]:    $logID, $quoteTypeID, $serverIp, $confirmed, $date, $time
//
// [RETURN VALUE]:  quoteServerLogID or 0 in case of failure
//
// [CREATED BY]:    Sturza Ciprian (cipi@acrux.biz) 2008-05-05
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteServerLog($logID='', $quoteTypeID='', $serverIp='', $confirmed='N', $date='', $time='')
{
   if(! $this->AssertQuoteServerLog($logID,$quoteTypeID,$serverIp,$confirmed))
      return false;

   // check if quoteServerID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTE_SERVER_LOGS." WHERE log_id='$logID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if($this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("DUPLICATE_LOG_ID_FIELD");
      return false;
   }

   if(empty($date) && empty($time))
   {
      $date='now()';
      $time='now()';

      $this->lastSQLCMD = "INSERT INTO ".SQL_QUOTE_SERVER_LOGS." (log_id,quote_type_id,server_ip,confirmed,date,time) VALUES  ('$logID','$quoteTypeID','$serverIp','$confirmed',$date,$time)";

   }
   else
   {
      $this->lastSQLCMD = "INSERT INTO ".SQL_QUOTE_SERVER_LOGS." (log_id,quote_type_id,server_ip,confirmed,date,time) VALUES  ('$logID','$quoteTypeID','$serverIp','$confirmed','$date','$time')";

   }

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQuoteServerLog
//
// [DESCRIPTION]:   Update quote_webserver_logs table
//
// [PARAMETERS]:    $quoteServerID, $logID, $serverIp, $confirmed
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Sturza Ciprian (cipi@acrux.biz) 2008-05-05
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQuoteServerLog($quoteServerID ='', $quoteTypeID='', $logID='', $serverIp='', $confirmed='')
{
   if(! preg_match("/^\d+$/", $quoteServerID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_SERVER_ID_FIELD");
      return false;
   }

   if(! $this->AssertLog($logID='', $serverIp='', $confirmed=''))
      return false;

   // check if quoteServerID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTE_SERVER_LOGS." WHERE id='$quoteServerID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_SERVER_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_QUOTE_SERVER_LOGS." SET log_id='$logID',quote_type_id='$quoteTypeID',server_ip='$serverIp',confirmed='$confirmed' WHERE id='$quoteServerID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ConfirmQuoteLog
//
// [DESCRIPTION]:   Update quote_webserver_logs table
//
// [PARAMETERS]:    $logID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Sturza Ciprian (cipi@acrux.biz) 2008-05-05
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ConfirmQuoteLog($logID='')
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");
      return false;
   }

   // check if quoteServerID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTE_SERVER_LOGS." WHERE log_id='$logID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("LOG_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_QUOTE_SERVER_LOGS." SET confirmed='Y' WHERE log_id='$logID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQuoteServerLog
//
// [DESCRIPTION]:   Delete an entry from quote_webserver_logs table
//
// [PARAMETERS]:    $quoteServerID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Sturza Ciprian (cipi@acrux.biz) 2008-05-05
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteQuoteServerLog($quoteServerID='')
{
   if(! preg_match("/^\d+$/", $quoteServerID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_SERVER_ID_FIELD");
      return false;
   }

   // check if quoteServerID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTE_SERVER_LOGS." WHERE id='$quoteServerID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_SERVER_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_QUOTE_SERVER_LOGS." WHERE id='$quoteServerID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteServerLog
//
// [DESCRIPTION]:   Read data from quote_webserver_logs table and put it into an array variable
//
// [PARAMETERS]:    $quoteServerID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Sturza Ciprian (cipi@acrux.biz) 2008-05-05
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteServerLog($quoteServerID='')
{
   if(! preg_match("/^\d+$/", $quoteServerID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_SERVER_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_SERVER_LOGS." WHERE id='$quoteServerID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTE_SERVER_ID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]            = $this->dbh->GetFieldValue("id");
   $arrayResult["log_id"]        = $this->dbh->GetFieldValue("log_id");
   $arrayResult["quote_type_id"] = $this->dbh->GetFieldValue("quote_type_id");
   $arrayResult["server_ip"]     = $this->dbh->GetFieldValue("server_ip");
   $arrayResult["confirmed"]     = $this->dbh->GetFieldValue("confirmed");
   $arrayResult["date"]          = $this->dbh->GetFieldValue("date");
   $arrayResult["time"]          = $this->dbh->GetFieldValue("time");


   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteServerLogByLogID
//
// [DESCRIPTION]:   get quote server id from quote_webserver_logs table by log_id
//
// [PARAMETERS]:    $logID
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Sturza Ciprian (cipi@acrux.biz) 2008-05-05
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteServerLogByLogID($logID='')
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_SERVER_LOGS." WHERE log_id='$logID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("LOG_ID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]            = $this->dbh->GetFieldValue("id");
   $arrayResult["log_id"]        = $this->dbh->GetFieldValue("log_id");
   $arrayResult["quote_type_id"] = $this->dbh->GetFieldValue("quote_type_id");
   $arrayResult["server_ip"]     = $this->dbh->GetFieldValue("server_ip");
   $arrayResult["confirmed"]     = $this->dbh->GetFieldValue("confirmed");
   $arrayResult["date"]          = $this->dbh->GetFieldValue("date");
   $arrayResult["time"]          = $this->dbh->GetFieldValue("time");


   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Sturza Ciprian (cipi@acrux.biz) 2008-05-05
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Sturza Ciprian (cipi@acrux.biz) 2008-05-05
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Print the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Sturza Ciprian (cipi@acrux.biz) 2008-05-05
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}
} // end of CLog class
?>

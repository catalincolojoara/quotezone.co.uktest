<?php

include_once "errors.inc";
include_once "MySQL.php";

define("OUTBOUNDS_TABLE","outbounds");

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   COutbound
//
// [DESCRIPTION]:  COutbound was created in order to work with 'outbounds' table
//                 
//
// [FUNCTIONS]:
//    
//                integer|false    AddOutbound($logId,$outboundConfigid,$sentStatus,$receivedStatus,$serverIp,$extraParams,$date,$time)
//                array|false      GetOutboundDataById($outboundId = '')
//                boolean          DeleteOutboundDataById($outboundId = '')
//                array|false      GetQuoteOutbounds($logId,$siteId)
//                array|false      GetOutboundConfigTypes($siteId)
//                array|false      GetWaitingOutbounds($dateStart,$dateEnd,$timeStart,$timeEnd)
//                array|false      GetWaitingMailOutbounds($dateStart,$dateEnd,$timeStart,$timeEnd)
//                boolean          CheckOutboundExists($logId, $outboundConfigId )
//                boolean          UpdateOutboundField($outboundId, $field = '', $value = '')
//                boolean          AssertInteger($id)
//
//                Close();
//                string           GetError();
//
//
// [CREATED BY]:   Racu Daniel (dade@acrux.biz) 09-05-2008
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class COutbound
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: COutbound
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function COutbound($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertInteger
//
// [DESCRIPTION]:   Verify to see if the requested id is an integer.
//
// [PARAMETERS]:    $id
//
// [RETURN VALUE]:  bool
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 09-05-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertInteger($id)
{
   if(! preg_match("/^\d+$/",$id))
   {
      $this->strERR = GetErrorString("NOT_AN_INETGER");
      return false;
   }
   
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckOutboundExists($logId, $outboundConfigId )
//
// [DESCRIPTION]:   Verify to see if we already have an aoutbound for that log ai and outbound_type.
//
// [PARAMETERS]:    $logId, $outboundConfigId
//
// [RETURN VALUE]:  boolean
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 09-05-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckOutboundExists($logId, $outboundConfigId)
{
    
   if(! $this->AssertInteger($logId))  
       return false;
      
   if(! $this->AssertInteger($outboundConfigId))  
      return false;
  
   $sqlCmd  = "SELECT * FROM ".OUTBOUNDS_TABLE." WHERE log_id ='".$logId."' AND outbound_config_id = '".$outboundConfigId."'";

   if(! $this->dbh->Exec($sqlCmd, true))
     return false;


   if(! $this->dbh->GetRows())
      return false;

//   print " \n DUPLICATE ROWS ON LOG ID: ".$logId;

   return true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateOutboundTries($outboundId);
//
// [DESCRIPTION     Update the tries field for one outbound
//
// [PARAMETERS]:    $outboundId
//
// [RETURN VALUE]:  boolean
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 09-05-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateOutboundTries($outboundId)
{
   $sqlCmd = "UPDATE ".OUTBOUNDS_TABLE." SET tries = tries + 1 , received_status = 0 where id = '".$outboundId."'";

   if(! $this->dbh->Exec($sqlCmd))
     return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteOutboundDataById($outboundId = '');
//
// [DESCRIPTION     Update the tries field for one outbound
//
// [PARAMETERS]:    $outboundId
//
// [RETURN VALUE]:  boolean
//
// [CREATED BY]:    Calin Goia (calin@acrux.biz) 19-03-2009
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteOutboundDataById($outboundId = '')
{
   $sqlCmd = "DELETE FROM ".OUTBOUNDS_TABLE." WHERE id = '".$outboundId."'";

   if(! $this->dbh->Exec($sqlCmd))
     return false;

   return true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteOutbounds($logId,$siteId)
//
// [DESCRIPTION]:   Returns an array containg outbound config id and outbound id
//
// [PARAMETERS]:    $logId, $siteId
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 09-05-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteOutbounds($logId,$siteId)
{
   if(! $this->AssertInteger($logId))
       return false;

   if(! $this->AssertInteger($siteId))
       return false;

   $sqlCmd = "SELECT o.id,oc.type FROM outbounds o, outbound_config oc where o.outbound_config_id = oc.id and o.log_id = '".$logId."' AND oc.site_id = '".$siteId."'";

   //print $sqlCmd;   

   if(! $this->dbh->Exec($sqlCmd))
     return false;

   if(! $this->dbh->GetRows())
      return false;

   $results = array();

   $cnt = 0;
   while($this->dbh->MoveNext())
   {
        $results[$cnt]['type']            = $this->dbh->GetFieldValue('type');
        $results[$cnt]['outbound_id']     = $this->dbh->GetFieldValue('id');
        $cnt++;
   }

   return $results;

}



//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddOutbound
//
// [DESCRIPTION]:   Adds outbound to database
//
// [PARAMETERS]:    $logId,$outboundConfigid,$sentStatus,$receivedStatus,$serverIp,$extraParams,$date,$time
//
// [RETURN VALUE]:  integer|false
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 09-05-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddOutbound($logId, $outboundConfigId = '', $sentStatus = '', $receivedStatus = '', $serverIp = '', $extraParams = '', $date = '', $time = '')
{
   if(! $this->AssertInteger($logId))  
       return false;
      
   if(! $this->AssertInteger($outboundConfigId))  
      return false;
      
/*
   if( $this->CheckOutboundExists($logId, $outboundConfigId))
      return false;
*/
   $sqlCmd  = "INSERT INTO ".OUTBOUNDS_TABLE." (log_id, outbound_config_id, sent_status, received_status, server_ip, extra_params, date, time) ";
   $sqlCmd .= " VALUES ('".$logId."', '".$outboundConfigId."', '".$sentStatus."','".$receivedStatus."', '".$serverIp."','".$extraParams."','".$date."','".$time."')";   

   if(! $this->dbh->Exec($sqlCmd, true))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->Exec("SELECT LAST_INSERT_ID() AS id", true))
      $id = -1;
   elseif(! $this->dbh->FetchRows() )
      $id = -2;
   else
      $id = $this->dbh->GetFieldValue("id");
 
   if($id < 0)
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
   
   return $id;        
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetOutboundDataById($outboundId = "")
//
// [DESCRIPTION]:   Return all data for specified id
//
// [PARAMETERS]:    $outboundId
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 09-05-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetOutboundDataById($outboundId = "")
{
   if(! $this->AssertInteger($outboundId))
      return false;
      
   $sqlCmd = "SELECT * FROM ".OUTBOUNDS_TABLE." WHERE id = '".$outboundId."'";   
   
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
      
   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
   
   $results = array();
   
   $results['logId']             = $this->dbh->GetFieldValue('log_id');
   $results['outboundConfigId']  = $this->dbh->GetFieldValue('outbound_config_id');
   $results['sentStatus']        = $this->dbh->GetFieldValue('sent_status');
   $results['receivedStatus']    = $this->dbh->GetFieldValue('received_status');
   $results['serverIp']          = $this->dbh->GetFieldValue('server_ip');
   $results['extraParams']       = $this->dbh->GetFieldValue('extra_params');
   $results['date']              = $this->dbh->GetFieldValue('date');
   $results['time']              = $this->dbh->GetFieldValue('time');
   
   return $results;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetOutboundDataByLogId($logId = "")
//
// [DESCRIPTION]:   Return all data for specified logId
//
// [PARAMETERS]:    $logId
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 09-05-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetOutboundDataByLogId($logId = "")
{
   if(! $this->AssertInteger($outboundId))
      return false;
      
   $sqlCmd = "SELECT * FROM ".OUTBOUNDS_TABLE." WHERE log_id = '".$logId."'";   
   
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
      
   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
   
   $results = array();
   
   $results['logId']             = $this->dbh->GetFieldValue('log_id');
   $results['outboundConfigId']  = $this->dbh->GetFieldValue('outbound_config_id');
   $results['sentStatus']        = $this->dbh->GetFieldValue('sent_status');
   $results['receivedStatus']    = $this->dbh->GetFieldValue('received_status');
   $results['serverIp']          = $this->dbh->GetFieldValue('server_ip');
   $results['extraParams']       = $this->dbh->GetFieldValue('extra_params');
   $results['date']              = $this->dbh->GetFieldValue('date');
   $results['time']              = $this->dbh->GetFieldValue('time');
   
   return $results;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetWaitingOutbounds($dateStart,$dateEnd,$timeStart,$timeEnd)
//
// [DESCRIPTION]:   Returns outbounds that are still with no received status
//
// [PARAMETERS]:    $dateStart,$dateEnd,$timeStart,$timeEnd
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 09-05-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetWaitingOutbounds($dateStart = '', $dateEnd = '', $timeStart = '', $timeEnd = '')
{
   
   $sqlCmd = "SELECT * FROM ".OUTBOUNDS_TABLE;
   
   if($dateStart AND $dateEnd)
       $sqlCmd .= " WHERE date between ".$dateStart." AND ".$dateEnd;
    
   if($timeStart AND $timeEnd)
       $sqlCmd .= " WHERE time between ".$timeStart." AND ".$timeEnd;      
   
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
      
   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
   
   $results = array();
   
   $results['id']                = $this->dbh->GetFieldValue('id');
   $results['logId']             = $this->dbh->GetFieldValue('log_id');
   $results['outboundConfigId']  = $this->dbh->GetFieldValue('outbound_config_id');
   $results['sentStatus']        = $this->dbh->GetFieldValue('sent_status');
   $results['receivedStatus']    = $this->dbh->GetFieldValue('received_status');
   $results['serverIp']          = $this->dbh->GetFieldValue('server_ip');
   $results['extraParams']       = $this->dbh->GetFieldValue('extra_params');
   $results['date']              = $this->dbh->GetFieldValue('date');
   $results['time']              = $this->dbh->GetFieldValue('time');
   
   return $results;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetWaitingMailOutbounds($dateStart,$dateEnd,$timeStart,$timeEnd)
//
// [DESCRIPTION]:   Returns outbounds that are still with no received status
//
// [PARAMETERS]:    $dateStart,$dateEnd,$timeStart,$timeEnd
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 09-05-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetWaitingMailOutbounds($serverIp = array(), $daysBehind=7)
{
   
   $sqlIp         = " and ( ";
   $ipServerCount = count($serverIp); 
   $contor        = 1;
   foreach($serverIp as $ip)
   {
	$sqlIp .= " outb.server_ip = '".$ip."' ";

        if($contor < $ipServerCount)
           $sqlIp .= " OR ";

       $contor++    ;
   }

   $sqlIp         .= " )  ";

   $sqlCmd = "  SELECT outb.id,outb.extra_params as messageId , l.filename , outc.site_id as site_id, outb.date, outb.time FROM  outbounds outb, outbound_config outc , logs l WHERE l.id = outb.log_id AND outb.outbound_config_id = outc.id ";
   
   if($ipServerCount)
      $sqlCmd .= $sqlIp;
   
   // $sqlCmd .=" AND outb.received_status = 0 AND (outc.type='MAIL' OR outc.type='CSV' OR outc.type='TXT' OR outc.type='URL')";
   $sqlCmd .=" AND outb.sent_status = 1 AND outb.received_status = 0 AND outb.extra_params <> ''";

   $date = date("Y-m-d",mktime()- $daysBehind*86400);

   $sqlCmd .=" AND outb.date >= '$date'";

//   print $sqlCmd;
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
      
   $result = array();
   $contor = 0;

   $curDateTime = time();
   $dbDate      = '';
   $dbTime      = '';

   while($this->dbh->MoveNext())
   {
         $dbDate     = $this->dbh->GetFieldValue('date');          
         $dbTime     = $this->dbh->GetFieldValue('time');          
         $dbDateList = explode("-", $dbDate);
         $dbTimeList = explode(":", $dbTime);

         // mktime  ([ int $hour  [, int $minute  [, int $second  [, int $month  [, int $day  [, int $year  [, int $is_dst  ]]]]]]] )
         $dbDateTime = mktime($dbTimeList[0],$dbTimeList[1],$dbTimeList[2],$dbDateList[1],$dbDateList[2],$dbDateList[0]);
         if($curDateTime - $dbDateTime < 900)
         {
            //print "$dbDate $dbTime ".$this->dbh->GetFieldValue('filename')."\n";
            continue;
         }

	 $results[$contor]['id']                = $this->dbh->GetFieldValue('id');
	 $results[$contor]['messageId']         = $this->dbh->GetFieldValue('messageId');
	 $results[$contor]['fileName']          = $this->dbh->GetFieldValue('filename');
	 $results[$contor]['siteId']            = $this->dbh->GetFieldValue('site_id');         

         $contor++; 
   }
  
    return $results;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetWaitingMailOutboundsDaily($serverIp,$date)
//
// [DESCRIPTION]:   Returns outbounds that are still with no received status
//
// [PARAMETERS]:    $serverIp,$date
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Calin Goia (calin@acrux.biz) 2014-04-15
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetWaitingMailOutboundsDaily($serverIp = array(), $date='')
{
   
   if(trim($date) == '')
   {
      $this->strERR = 'No date received';
      return false;
   }

   $sqlIp         = " and ( ";
   $ipServerCount = count($serverIp); 
   $contor        = 1;
   foreach($serverIp as $ip)
   {
	$sqlIp .= " outb.server_ip = '".$ip."' ";

        if($contor < $ipServerCount)
           $sqlIp .= " OR ";

       $contor++    ;
   }

   $sqlIp         .= " )  ";

   $sqlCmd = "  SELECT outb.id,outb.extra_params as messageId , l.filename , outc.site_id as site_id, outb.date, outb.time, outc.type FROM  outbounds outb, outbound_config outc , logs l WHERE l.id = outb.log_id AND outb.outbound_config_id = outc.id ";
   
   if($ipServerCount)
      $sqlCmd .= $sqlIp;
   
   // $sqlCmd .=" AND outb.received_status = 0 AND (outc.type='MAIL' OR outc.type='CSV' OR outc.type='TXT' OR outc.type='URL')";
   $sqlCmd .=" AND outb.sent_status = 1 AND outb.received_status = 0 AND outb.extra_params <> ''";

   $sqlCmd .=" AND outb.date = '$date'";

//   print $sqlCmd;
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
      
   $result = array();
   $contor = 0;

   $curDateTime = time();
   $dbDate      = '';
   $dbTime      = '';

   while($this->dbh->MoveNext())
   {
         $dbDate     = $this->dbh->GetFieldValue('date');          
         $dbTime     = $this->dbh->GetFieldValue('time');          
         $dbDateList = explode("-", $dbDate);
         $dbTimeList = explode(":", $dbTime);

         // mktime  ([ int $hour  [, int $minute  [, int $second  [, int $month  [, int $day  [, int $year  [, int $is_dst  ]]]]]]] )
         $dbDateTime = mktime($dbTimeList[0],$dbTimeList[1],$dbTimeList[2],$dbDateList[1],$dbDateList[2],$dbDateList[0]);
         if($curDateTime - $dbDateTime < 900)
         {
            //print "$dbDate $dbTime ".$this->dbh->GetFieldValue('filename')."\n";
            continue;
         }

	 $results[$contor]['id']                = $this->dbh->GetFieldValue('id');
	 $results[$contor]['messageId']         = $this->dbh->GetFieldValue('messageId');
	 $results[$contor]['fileName']          = $this->dbh->GetFieldValue('filename');
	 $results[$contor]['siteId']            = $this->dbh->GetFieldValue('site_id');         
	 $results[$contor]['time']              = $dbTime;
	 $results[$contor]['outboundType']      = $this->dbh->GetFieldValue('type');

         $contor++; 
   }
  
    return $results;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateOutboundField($outboundId, $field, $value = '')
//
// [DESCRIPTION]:   Updates an outbound  field in outbounds table
//
// [PARAMETERS]:    $outboundId, $field, $value
//
// [RETURN VALUE]:  boolean
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 09-05-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateOutboundField($outboundId, $field = '', $value = '')
{
   if(! $this->AssertInteger($outboundId))
      return false;
      
   if(empty($field))
      return false;
      
   $sqlCmd = "UPDATE ".OUTBOUNDS_TABLE." SET ".$field." = '".$value."' WHERE id = '".$outboundId."'";  
   
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;    
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetOutboundConfigTypes($siteID)
//
// [DESCRIPTION]:   Get the outbound types for specified site id 
//
// [PARAMETERS]:    $siteID
//
// [RETURN VALUE]:  array | falseOUTBOUNDS_TABLE
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 09-05-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetOutboundConfigTypes($siteId)
{
   if(! $this->AssertInteger($siteId))
      return false;
   
   $sqlCmd = "SELECT * FROM  outbound_config WHERE site_id = '".$siteId."' ";   
   
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   } 
   
   $result = array();
   $contor = 0; 
   while($this->dbh->MoveNext())
   {
      $result[$contor]['id'] = $this->dbh->GetFieldValue('id');
      $result[$contor]['type'] = $this->dbh->GetFieldValue('type');      
      $result[$contor]['status'] = $this->dbh->GetFieldValue('status');
      $result[$contor]['top'] = $this->dbh->GetFieldValue('top');
      
      $contor++;
   }     
   
   return $result;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Racu Daniel (dade@acrux.biz) 09-05-2008
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
   return $this->strERR;
}


}// end COutbound class

?>

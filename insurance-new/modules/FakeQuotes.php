<?php
/*****************************************************************************/
/*                                                                           */
/*  CFakeQuotes class interface                                              */
/*                                                                           */
/*  (C) 2005  (florin@acrux.biz)                              */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CFakeQuotes
//
// [DESCRIPTION]:  CFakeQuotes class interface
//
// [FUNCTIONS]:
//
//                 Close();
//                 GetError();
//                 ShowError();
//
// [CREATED BY]:   Florin (florin@acrux.biz)
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CFakeQuotes
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CFakeQuotes
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:     (florin@acrux.biz)
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CFakeQuotes($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]:
//
// [DESCRIPTION]:
//
// [PARAMETERS]:
//
// [RETURN VALUE]:
//
// [CREATED BY]:    Florin (florin@acrux.biz)
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddFakeQuote($qzQuoteID, $logID, $quoteRef, $filename, $quoteTypeID,$source="",$sourceInfo="")
{

   if(! preg_match('/\d+/i',$qzQuoteID))
   {
      $this->strERR .= GetErrorString("INVALID_QZQUOTE_ID_FIELD");
      return false;
   }

   if(! preg_match('/\d+/i',$logID))
   {
      $this->strERR .= GetErrorString("INVALID_LOG_ID_FIELD");
      return false;
   }

   if (empty($quoteRef))
   {
      $this->strERR .= GetErrorString("INVALID_QUOTE_REF_FIELD");
      return false;
   }

   if (empty($filename))
   {
      $this->strERR .= GetErrorString("INVALID_FILENAME_FIELD");
      return false;
   }

   if (empty($quoteTypeID))
   {
      $this->strERR .= GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
      return false;
   }

//   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$date))
//   {
//      $this->strERR .= GetErrorString("INVALID_DATE_FIELD")."\n";
//      return false;
//   }

   $this->lastSQLCMD = "INSERT INTO fake_quotes (qz_quote_id,log_id,quote_ref,filename,quote_type_id,source,source_info,date,time) VALUES ('$qzQuoteID','$logID','$quoteRef','$filename','$quoteTypeID','$source','$sourceInfo',now(),now())";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFakeQuotesByDate
//
// [DESCRIPTION]:
//
// [PARAMETERS]:
//
// [RETURN VALUE]:
//
// [CREATED BY]:   Florin (florin@acrux.biz)
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFakeQuotesByDate($dateFrom, $dateTo)
{

   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$dateFrom))
   {
      $this->strERR .= GetErrorString("INVALID_DATEFROM_FIELD")."\n";
      return false;
   }

   if(! preg_match('/\d{4}\-\d{1,2}\-\d{1,2}/i',$dateTo))
   {
      $this->strERR .= GetErrorString("INVALID_DATETO_FIELD")."\n";
      return false;
   }

   $this->lastSQLCMD = "SELECT * from fake_quotes where date between '$dateFrom' and '$dateTo'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

	$arrayResult = array();

	while ($this->dbh->MoveNext)
	{
		$index = $this->dbh->GetFieldValue('id');
		$arrayResult[$index]['qz_quote_id']   = $this->dbh->GetFieldValue('qz_quote_id');
		$arrayResult[$index]['log_id']        = $this->dbh->GetFieldValue('log_id');
		$arrayResult[$index]['quote_ref']     = $this->dbh->GetFieldValue('quote_ref');
		$arrayResult[$index]['quote_type_id'] = $this->dbh->GetFieldValue('quote_type_id');
		$arrayResult[$index]['filename']      = $this->dbh->GetFieldValue('filename');
		$arrayResult[$index]['source']        = $this->dbh->GetFieldValue('source');
		$arrayResult[$index]['source_info']   = $this->dbh->GetFieldValue('source_info');
	}


   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFakeQuotesByID
//
// [DESCRIPTION]:
//
// [PARAMETERS]:
//
// [RETURN VALUE]:
//
// [CREATED BY]:   Florin (florin@acrux.biz)
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFakeQuotesByID($fakeQuoteID)
{

   if(! preg_match('/\d+/i',$fakeQuoteID))
   {
      $this->strERR .= GetErrorString("INVALID_FAKE_QUOTE_ID_FIELD")."\n";
      return false;
   }

   $this->lastSQLCMD = "SELECT * from fake_quotes where id='$fakeQuoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("<!-- RECORD_NOT_FOUND -->");
      return false;
   }



   $arrayResult = array();

	//$index = $this->dbh->GetFieldValue('id');

	$arrayResult['qz_quote_id']   = $this->dbh->GetFieldValue('qz_quote_id');
	$arrayResult['log_id']        = $this->dbh->GetFieldValue('log_id');
	$arrayResult['quote_ref']     = $this->dbh->GetFieldValue('quote_ref');
	$arrayResult['quote_type_id'] = $this->dbh->GetFieldValue('quote_type_id');
	$arrayResult['filename']      = $this->dbh->GetFieldValue('filename');
	$arrayResult['source']        = $this->dbh->GetFieldValue('source');
	$arrayResult['source_info']   = $this->dbh->GetFieldValue('source_info');

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:     ()
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:     ()
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Show the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:     ()
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

}//end class CFakeQuotes

?>

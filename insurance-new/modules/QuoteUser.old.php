<?php
/*****************************************************************************/
/*                                                                           */
/*  CQuoteUsers class interface                                              */
/*                                                                           */
/*  (C) 2005 Istvancsek Gabriel (gabi@acrux.biz)                              */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuoteUsers
//
// [DESCRIPTION]:  CQuoteUser class interface
//
// [FUNCTIONS]:    true   | false AssertQuoteUser(&$firstName, &$lastName, $birthDate='', $password='')
//                 string | false StdToMysqlDate($date='')
//                 int    | false AddQuoteUser($firstName='', $lastName='', $birthDate='', $password='')
//                 true   | false UpdateQuoteUser($quoteUserID='', $firstName='', $lastName='', $birthDate='', $password='')
//                 true   | false DeleteQuoteUser($quoteUserID='')
//                 array  | false GetQuoteUserByID($quoteUserID='')
//                 int    | false GetQuoteUserByEmail($userEmail='')
//                 array  | false CheckEmailUnicity($quoteUserID='')
//                 array  | false GetAllQuoteUsers()
//                 array  | false GetQuoteUserByEmailDateOfBirth($userEmail='', $dateOfBirth='')
//                 array  | false GetUserByFNameSNameDateOfBirth($fName='', $sName='', $dateOfBirth='')
//
//                 Close();
//                 GetError();
//                 ShowError();
//
// [CREATED BY]:   Istvancsek (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CQuoteUser
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuoteUsers
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQuoteUser($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuoteUser
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    &$firstName, &$lastName, $birthDate='', $password=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertQuoteUser(&$firstName, &$lastName, $birthDate='', $password='')
{
   $this->strERR = '';

   $firstName = str_replace("'","\'",$firstName);
   $lastName  = str_replace("'","\'",$lastName);
   $password  = str_replace("'","\'",$password);

   if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$firstName))
      $this->strERR = GetErrorString("INVALID_FIRST_NAME_FIELD")."\n";

   if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$lastName))
      $this->strERR .= GetErrorString("INVALID_LAST_NAME_FIELD")."\n";

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$birthDate))
      $this->strERR .= GetErrorString("INVALID_BIRTH_DATE_FIELD")."\n";

// TO DO -> DEBUG UPDATE TO NEW VERSION
//    if(empty($password))
//       $this->strERR .= GetErrorString("NVALID_PASSWORD_FIELD")."\n";

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Istvancsek Gabriel(gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   //if(! preg_match("\/", $date))
   //  return $date;
    
   if(! preg_match("/\d{1,2}\/\d{1,2}\/\d{4}/i",$date))
      $this->strERR = GetErrorString("INVALID_QUOTE_USERS_STD_TO_MYSQL_DATE")."\n";

   if(! empty($this->strERR))
      return false;
   
   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteUser
//
// [DESCRIPTION]:   Check if exist a user and add if not exist into quote_users table
//
// [PARAMETERS]:    $firstName, $lastName, $birthDate, $password
//
// [RETURN VALUE]:  $quoteUserID | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteUser($firstName='', $lastName='', $birthDate='', $password='')
{
   // we use in case of update quote users !!!!
   $birthDateStd = $birthDate;

   if(! $birthDate = $this->StdToMysqlDate($birthDate))
      return false;

   if(! $this->AssertQuoteUser($firstName, $lastName, $birthDate, $password))
      return false;

   // first check if we already have this user in our DB
   $lastSqlCmd = "SELECT id FROM ".SQL_QUOTE_USERS." WHERE first_name='$firstName' AND last_name='$lastName' AND birth_date='$birthDate' LIMIT 1";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   if($this->dbh->GetRows())
   {
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      $quoteUserID = $this->dbh->GetFieldValue("id");

      // we have to make update into db for old password  so we save the last password
      // UPDATE MODE !!!
      if(! empty($password))
      {
         if(! $this->UpdateQuoteUser($quoteUserID, $firstName, $lastName, $birthDateStd, $password))
            return false;
      }
      // end UPDATE MODE;

      return $quoteUserID;
   }

   $lastSqlCmd = "INSERT INTO ".SQL_QUOTE_USERS." (first_name, last_name, birth_date, password) VALUES ('$firstName', '$lastName', '$birthDate', '$password')";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $lastSqlCmd = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQuoteUser
//
// [DESCRIPTION]:   Update a user data from the quote_users table
//
// [PARAMETERS]:    $quoteUserID, $firstName, $lastName, $birthDate, $password
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQuoteUser($quoteUserID='', $firstName='', $lastName='', $birthDate='', $password='')
{

   if(! preg_match("/^\d+$/", $quoteUserID))
   {
         $this->strERR = GetErrorString("INVALID_USER_ID_FIELD")."\n";
      return false;
   }

   if(! $birthDate=$this->StdToMysqlDate($birthDate))
      return false;

   if(! $this->AssertQuoteUser($firstName, $lastName, $birthDate, $password))
      return false;

   // check if userID exists
   $lastSqlCmd = "SELECT id FROM ".SQL_QUOTE_USERS." WHERE id='$quoteUserID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_ID_NOT_FOUND");
      return false;
   }

   $lastSqlCmd = "UPDATE ".SQL_QUOTE_USERS." SET first_name='$firstName', last_name='$lastName', birth_date='$birthDate', password='$password' WHERE id='$quoteUserID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
/////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQuoteUser
//
// [DESCRIPTION]:   Delete an entry from the quote_users table
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvcansek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteQuoteUser($quoteUserID='')
{
   if(! preg_match("/^\d+$/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_USER_ID_FIELD");
      return false;
   }

   $lastSqlCmd = "SELECT id FROM ".SQL_QUOTE_USERS." WHERE id='$quoteUserID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERID_NOT_FOUND");
      return false;
   }

   $lastSqlCmd = "DELETE FROM ".SQL_QUOTE_USERS." WHERE id='$quoteUserID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteUserByID
//
// [DESCRIPTION]:   Read data of a user from quote_users table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Istvacsek Gabriel (dan@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteUserByID($quoteUserID='')
{
   if(! preg_match("/^\d+$/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_USER_ID_FIELD");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM ".SQL_QUOTE_USERS." WHERE id='$quoteUserID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]         = $this->dbh->GetFieldValue("id");
   $arrayResult["first_name"] = $this->dbh->GetFieldValue("first_name");
   $arrayResult["last_name"]  = $this->dbh->GetFieldValue("last_name");
   $arrayResult["birth_date"] = $this->dbh->GetFieldValue("birth_date");
   $arrayResult["password"]   = $this->dbh->GetFieldValue("password");

   return $arrayResult;
}

function GetQuoteUser($quoteUserID=0)
{
   return $this->GetQuoteUserByID($quoteUserID);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteUserIDByEmail
//
// [DESCRIPTION]:   Read data of a user from quote_users table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  int or false in case of failure
//
// [CREATED BY]:    Istvacsek Gabriel (dan@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteUserByEmail($userEmail='')
{
   if(! preg_match("/\.*\@.*\.[a-zA-Z]{2,3}/i", $userEmail))
   {
      $this->strERR = GetErrorString("INVALID_USER_EMAIL_FIELD");
      return false;
   }

   $lastSqlCmd = "SELECT ".SQL_QUOTE_USERS.".id FROM ".SQL_QUOTE_USERS.",".SQL_QUOTE_USER_DETAILS." WHERE ".SQL_QUOTE_USERS.".id = ".SQL_QUOTE_USER_DETAILS.".quote_user_id AND ".SQL_QUOTE_USER_DETAILS.".email='$userEmail'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckEmailUnicity
//
// [DESCRIPTION]:   Read data from qzquotes table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
// 
function CheckEmailUnicity($quoteUserID='')
{
   if(! preg_match("/\d+/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTE_USER_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT  count(distinct email) as entries FROM ".SQL_QUOTE_USERS.",".SQL_QUOTE_USER_DETAILS." WHERE ".SQL_QUOTE_USERS.".id='$quoteUserID' AND ".SQL_QUOTE_USER_DETAILS.".quote_user_id=".SQL_QUOTE_USERS.".id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
   
   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QZ_quote_user_details_id_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $entries = $this->dbh->GetFieldValue("entries");

   if($entries == 1)
      return true;

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuoteUsers
//
// [DESCRIPTION]:   Read data from quote_users table and put it into an array variable
//                  key = userID, value = Array(that contain all the record for that user)
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (dan@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuoteUsers()
{
   $lastSqlCmd = "SELECT * FROM ".SQL_QUOTE_USERS."";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERS_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;
      $arrayResult[$index]["id"]                  = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["first_name"]          = $this->dbh->GetFieldValue("first_name");
      $arrayResult[$index]["last_name"]           = $this->dbh->GetFieldValue("last_name");
      $arrayResult[$index]["birth_date"]          = $this->dbh->GetFieldValue("birth_date");
      $arrayResult[$index]["password"]            = $this->dbh->GetFieldValue("password");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserByFNameSNameDateOfBirth
//
// [DESCRIPTION]: get user id if exist 
//
// [PARAMETERS]:  $userEmail, $dateOfBirth
//
// [RETURN VALUE]: int | false
//
// [CREATED BY]:    Istvancsek Gabriel (dan@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserByEmailDateOfBirth($userEmail='', $dateOfBirth='')
{
   if(! preg_match("/\.*\@.*\.[a-zA-Z]{2,3}/i", $userEmail))
   {
      $this->strERR = GetErrorString("INVALID_USER_EMAIL_FIELD");
      return false;
   }

   if(! $dateOfBirth = $this->StdToMysqlDate($dateOfBirth))
      return false;

   $lastSqlCmd = "SELECT ".SQL_QUOTE_USERS.".id FROM ".SQL_QUOTE_USERS.",".SQL_QUOTE_USER_DETAILS." WHERE ".SQL_QUOTE_USER_DETAILS.".email='$userEmail' AND ".SQL_QUOTE_USERS.".birth_date='$dateOfBirth' AND ".SQL_QUOTE_USERS.".id=".SQL_QUOTE_USER_DETAILS.".quote_user_id";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("INVALID_USER_DATE_OF_BIRTH_FIELD");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
   
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserByFNameSNameDateOfBirth
//
// [DESCRIPTION]: get user id if exist 
//
// [PARAMETERS]:  $fName='', $sName='', $dateOfBirth=''
//
// [RETURN VALUE]: $arrayResult or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (dan@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserByFNameSNameDateOfBirth($fName='', $sName='', $dateOfBirth='')
{

   if(! $birthDate = $this->StdToMysqlDate($dateOfBirth))
      return false;

   if(! $this->AssertQuoteUser($fName, $sName, $birthDate))
      return false;

   $lastSqlCmd = "SELECT * FROM ".SQL_QUOTE_USERS." WHERE first_name='$fName' AND last_name='$sName' AND birth_date='$birthDate'";
   
   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult['id']       = $this->dbh->GetFieldValue("id");
   $arrayResult['password'] = $this->dbh->GetFieldValue("password");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserByEmailPassword
//
// [DESCRIPTION]: get user id if exist
//
// [PARAMETERS]:  $userEmail, $dateOfBirth
//
// [RETURN VALUE]: int | false
//
// [CREATED BY]:    Listeveanu Cezar (dan@acrux.biz) 2006-03-30
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserByEmailPassword($userEmail='', $password='')
{
   if(! preg_match("/\.*\@.*\.[a-zA-Z]{2,3}/i", $userEmail))
   {
       $this->strERR = GetErrorString("INVALID_USER_EMAIL_FIELD");
       return false;
   }

    //if(! $this->AssertQuoteUser($password))
    //      return false;

    $lastSqlCmd = "SELECT qu.id FROM ".SQL_QUOTE_USERS." qu, ".SQL_QUOTE_USER_DETAILS." qud WHERE qud.email='$userEmail' AND qu.password='$password' AND qu.id=qud.quote_user_id GROUP by qu.id";

    if(! $this->dbh->Exec($lastSqlCmd))
    {
        $this->strERR = $this->dbh->GetError();
        return false;
    }

    if(! $this->dbh->GetRows())
    {
        $this->strERR = GetErrorString("INVALID_USER_PASSWORD");
        return false;
    }

    $arrayResult = array();
    $index = 0;
    while($this->dbh->MoveNext())
    {
        $index++;
        $arrayResult[$index]["id"] = $this->dbh->GetFieldValue("id");
    }

    return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Show the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (gabi@acrux.biz) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

}//end class CQuoteUsers

?>

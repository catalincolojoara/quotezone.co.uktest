<?php

/*****************************************************************************/
/*                                                                           */
/* IMAP class implementation                                                 */
/*                                                                           */
/*  (C) 2004 Eugen Savin (eugen@claspsolutions.com)                          */
/*                                                                           */
/*****************************************************************************/

define("IMAP_INCLUDED", "1");

if(DEBUG_MODE)
   error_reporting(E_ALL);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CMsgInfo
//
// [DESCRIPTION]:  IMAP message info struct, PHP version
//
// [FUNCTIONS]:    Clear();
//
// [CREATED BY]:   Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CMsgInfo
{
   var $host;
   var $hosts;
   var $type;
   var $subtype;
   var $encoding;
   var $size;
   var $parts;
   var $attachments;
   var $header;
   var $body;
   var $from;
   var $to;
   var $subject;
   var $realname;
   var $date;

   function CMsgInfo()
   {
      $this->Clear();
   }

   function Clear()
   {
      $this->host        = "";
      $this->hosts       = array();
      $this->type        = 0;
      $this->subtype     = 0;
      $this->encoding    = 0;
      $this->size        = 0;
      $this->parts       = 0;
      $this->attachments = 0;
      $this->header      = "";
      $this->body        = "";
      $this->from        = "";
      $this->to          = "";
      $this->subject     = "";
      $this->realname    = "";
      $this->date        = "";
   }
} // end of class CMsgInfo

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CIMAP
//
// [DESCRIPTION]:  IMAP interface for the mail servers, PHP version
//
// [FUNCTIONS]:    Open($host="", $user="", $pass="", $type="pop3");
//                 bool IsAlive();
//
//                 int  CheckAttachments($messageNr = 0);
//                 array|false GetAttachments($messageNr = 0);
//
//                 bool MarkDelete($messageNr = 0);
//                 bool UnDelete($messageNr = 0);
//                 bool ExpungeMessages();
//
//                 object|false GetStatus($options = SA_ALL);
//                 int GetMessages();
//                 int GetRecentMessages();
//                 int GetUnseenMessages();
//
//                 object|false GetMsgInfo($messageNr = 0);
//                 string       GetHeader($messageNr = 0);
//                 array|false  GetHeaders();
//                 object|false GetHeaderInfo($messageNr = 0);
//                 string       GetBody($messageNr = 0);
//                 string       GetTextBody($messageNr = 0);
//                 int          GetMsgSize($messageNr = 0);
//
//                 void SetHost($host = "localhost");
//                 void SetUser($user = "");
//                 void SetPass($pass = "");
//                 void SetType($type = "");
//                 void SetDebug($debug = 1);
//
//                 string GetHost();
//                 string GetUser();
//                 string GetPass();
//                 string GetType();
//                 int    GetDebug();
//
//                 string MakeReplyText($msgText = "", $replyMarker=": ");
//                 array  ParseMessage($msgText = "");
//                 string GenerateMimeBoundary();
//
//                 Close($expungeMessages = 0);
//                 GetError(;
//
// [CREATED BY]:   Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CIMAP
{
    // default mailer configuration
    var $host;        // default IMAP host name
    var $user;        // default IMAP user name
    var $pass;        // default IMAP password
    var $type;        // default IMAP type

    var $imapConn;       // IMAP connection handle
    var $imapConnString; // IMAP connection string
    var $msgInfo;        // message info object

    var $debug;          // debug mode
    var $strERR;         // last error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CIMAP
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CIMAP()
{
   $this->host = "127.0.0.1";
   $this->user = "";
   $this->pass = "";
   $this->type = "pop3";

   $this->imapConn       = 0;
   $this->imapConnString = "";
   $this->msgInfo        = new CMsgInfo();

   $this->debug          = 0;

   $this->strERR = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Open
//
// [DESCRIPTION]:   Open a connection with the IMAP server
//
// [PARAMETERS]:    $host="", $user="", $pass="", $type="pop3"
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Open($host="", $user="", $pass="", $type="pop3")
{
   $this->SetHost($host);
   $this->SetUser($user);
   $this->SetPass($pass);
   $this->SetType($type);

   $this->strERR = "";

   if($this->imapConn)
   {
      $this->strERR = "already connected to server $this->host";
      return false;
   }

   $this->imapConnString = "";
   switch($this->type)
   {
      case "pop3":
         $this->imapConnString = "\{$this->host:110/pop3/notls}INBOX";
         break;

      case "pop3s":
         $this->imapConnString = "\{$this->host:995/pop3/ssl/novalidate-cert}INBOX";
         break;

      case "imap":
         $this->imapConnString = "\{$this->host:143/imap/notls}INBOX";
         break;

      case "imaps":
         $this->imapConnString = "\{$this->host:993/imap/ssl/novalidate-cert}INBOX";
         break;

      default:
         $this->strERR  = "unknown IMAP protocol, cannot detect port number.\n";
         $this->strERR .= "valid types are: pop3, pop3s, imap, imaps.";
         break;
   }

   if(! empty($this->strERR))
      return false;

   $this->imapConn = imap_open($this->imapConnString, $this->user, $this->pass);

   if(! $this->imapConn)
   {
      $this->strERR = "cannot open mailbox for $this->user@$this->host";
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: IsAlive
//
// [DESCRIPTION]:   Check if the connection is still active
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function IsAlive()
{
   if(! imap_ping($this->imapConn))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckAttachments
//
// [DESCRIPTION]:   Check if the message has attachments
//
// [PARAMETERS]:    $messageNr = 0
//
// [RETURN VALUE]:  Number of attachments if success, -1 otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckAttachments($messageNr = 0)
{
   $this->strERR = "";
   $attachments = 0;

   $msgStruct = imap_fetchstructure($this->imapConn, $messageNr);
   //print_r($msgStruct);

   if(! $msgStruct)
   {
      $this->strERR = "cannot get the structure for message $messageNr [$this->user@$this->host]";
      return -1;
   }

   // go through the message's parts
   $parts = count($msgStruct->parts);
   for($i=0; $i<$parts; $i++)
   {
      if(strtolower($msgStruct->parts[$i]->disposition) == "attachment" || strtolower($msgStruct->parts[$i]->disposition) == "inline")
         $attachments++;
   }

   return $attachments;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAttachments
//
// [DESCRIPTION]:   Look for all message attachments and put them into an associative
//                  array: key = fileName   value = part contents
//
// [PARAMETERS]:    $messageNr = 0
//
// [RETURN VALUE]:  Array if success (fileName => fileContents), false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAttachments($messageNr = 0)
{
   $this->strERR = "";
   $arrayAttch = array();

   $msgStruct = imap_fetchstructure($this->imapConn, $messageNr);
   //print_r($msgStruct);

   if(! $msgStruct)
   {
      $this->strERR = "cannot get the structure for message $messageNr [$this->user@$this->host]";
      return false;
   }

   // go through the message's parts
   $parts = count($msgStruct->parts);
   for($i=0; $i<$parts; $i++)
   {
      if(strtolower($msgStruct->parts[$i]->disposition) != "attachment")
         continue;

      $partName    = $msgStruct->parts[$i]->dparameters[0]->value;
      $partContent = imap_fetchbody($this->imapConn, $messageNr, $i+1);

      switch($msgStruct->parts[$i]->encoding)
      {
         // 7BIT
         case 0:
            //$arrayAttch[$msgStruct->parts[$i]->dparameters[0]->value] = imap_7bit($partContent);
            break;

         // 8BIT
         case 1:
            $partContent = imap_8bit($partContent);
            break;

         // BINARY
         case 2:
            $partContent = imap_binary($partContent);
            break;

         // BASE64
         case 3:
            $partContent = imap_base64($partContent);
            break;

         // QUOTED-PRINTABLE
         case 4:
            $partContent = imap_qprint($partContent);
            break;

         // OTHER
         case 5:
            break;

         default:
            $this->strERR = "unknown encoding type found [".$msgStruct->parts[$i]->encoding."] for message $messageNr [$this->user@$this->host]";
            break;
      }// end switch($msgStruct->parts[$i]->encoding)

      if(! empty($this->strERR))
         break;

      if(empty($partName))
         continue;

      $arrayAttch[$partName] = $partContent;
   } //end for($i=0; $i<$parts; $i++)

   if(! empty($this->strERR))
      return false;

   return $arrayAttch;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MarkDelete
//
// [DESCRIPTION]:   Set the delete flag for a mail message
//
// [PARAMETERS]:    $messageNr = 0
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MarkDelete($messageNr = 0)
{
   $this->strERR = "";

   if(! imap_delete($this->imapConn, $messageNr))
   {
      $this->strERR = "cannot mark message $messageNr for deletion [$this->user@$this->host]";
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UnDelete
//
// [DESCRIPTION]:   Unset the delete flag for a mail message
//
// [PARAMETERS]:    $messageNr = 0
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UnDelete($messageNr = 0)
{
   $this->strERR = "";

   if(! imap_undelete($this->imapConn, $messageNr))
   {
      $this->strERR = "cannot unmark message $messageNr for deletion [$this->user@$this->host]";
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ExpungeMessages
//
// [DESCRIPTION]:   Delete all messages marked for deletion
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  True if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ExpungeMessages()
{
   $this->strERR = "";

   if(! imap_expunge($this->imapConn))
   {
      $this->strERR = "cannot expunge messages [$this->user@$this->host]";
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetStatus
//
// [DESCRIPTION]:   Return the status information on a mailbox
//
// [PARAMETERS]:    $options = "SA_ALL"
//
// [RETURN VALUE]:  Object if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetStatus($options = SA_ALL)
{
   $this->strERR = "";

   $status = imap_status($this->imapConn, $this->imapConnString, $options);
   //print_r($status);

   if(! $status)
   {
      $this->strERR = "cannot get mailbox status [$this->user@$this->host]";
      return false;
   }

   return $status;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMessages
//
// [DESCRIPTION]:   Return all the messages found on a mailbox
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  The messages number if success, -1 otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMessages()
{
   $status = $this->GetStatus(SA_MESSAGES);

   if(! $status)
      return -1;

   return $status->messages;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRecentMessages
//
// [DESCRIPTION]:   Return all recent messages found on a mailbox
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  The messages number if success, -1 otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetRecentMessages()
{
   $status = $this->GetStatus(SA_RECENT);

   if(! $status)
      return -1;

   return $status->recent;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUnseenMessages
//
// [DESCRIPTION]:   Return all unseen messages found on a mailbox
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  The messages number if success, -1 otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUnseenMessages()
{
   $status = $this->GetStatus(SA_UNSEEN);

   if(! $status)
      return -1;

   return $status->unseen;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMsgInfo
//
// [DESCRIPTION]:   Retrieve info about the email message
//
// [PARAMETERS]:    $messageNr = 0
//
// [RETURN VALUE]:  CMsgInfo if success, -1 otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMsgInfo($messageNr = 0)
{
   $this->strERR = "";
   $this->msgInfo->Clear();

   $msgStruct = imap_fetchstructure($this->imapConn, $messageNr);

   if(! $msgStruct)
   {
      $this->strERR = "cannot get the structure for message $messageNr [$this->user@$this->host]";
      return false;
   }

   $header = $this->GetHeader($messageNr);
   if($header === false)
      return false;

   $headerInfo = $this->GetHeaderInfo($messageNr);
   if($headerInfo === false)
      return false;

   $body = $this->GetBody($messageNr);
   if($body === false)
      return false;

   $this->msgInfo->type        = $msgStruct->type;
   $this->msgInfo->subtype     = $msgStruct->subtype;
   $this->msgInfo->encoding    = $msgStruct->encoding;
   $this->msgInfo->size        = $msgStruct->bytes;
   $this->msgInfo->parts       = count($msgStruct->parts);
   $this->msgInfo->attachments = 0;
   $this->msgInfo->header      = $header;
   $this->msgInfo->body        = $body;

   $fromRealName = "";
   $fromAddress  = $headerInfo->fromaddress;
   $toAddress    = $headerInfo->toaddress;

   // remove some special chars using str_replace instead of preg_replace
   $fromAddress = str_replace("\"", "", $fromAddress);
   $fromAddress = str_replace("'",  "", $fromAddress);
   $fromAddress = str_replace("<",  "", $fromAddress);
   $fromAddress = str_replace(">",  "", $fromAddress);

   $toAddress = str_replace("\"", "", $toAddress);
   $toAddress = str_replace("'",  "", $toAddress);
   $toAddress = str_replace("<",  "", $toAddress);
   $toAddress = str_replace(">",  "", $toAddress);

   if(preg_match("/^(.*)\s([^\s]+)$/iU", $fromAddress, $matches))
   {
      $fromRealName = $matches[1];
      $fromAddress  = $matches[2];
   }

   if(preg_match("/^(.*)\s([^\s]+)$/iU", $toAddress, $matches))
      $toAddress  = $matches[2];

   $this->msgInfo->from        = $fromAddress;
   $this->msgInfo->to          = $toAddress;
   $this->msgInfo->subject     = $headerInfo->subject;
   $this->msgInfo->realname    = $fromRealName;
   $this->msgInfo->date        = $headerInfo->udate;

   // go through the message's parts
   for($i=0; $i<$this->msgInfo->parts; $i++)
   {
      if(strtolower($msgStruct->parts[$i]->disposition) == "attachment")
         $this->msgInfo->attachments++;

      //if($msgStruct->parts[$i]->type == "0")
      //   $this->msgInfo->body .= imap_fetchbody($this->imapConn, $messageNr, $i+1);
   }

   if($this->msgInfo->attachments)
       $this->msgInfo->body = $this->GetTextBody($messageNr);

   // build the hosts array
   if(preg_match_all("/^Received: .*\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\]/m", $this->msgInfo->header, $matches))
   {
      for($i=0; $i<count($matches[0]); $i++)
         array_push($this->msgInfo->hosts, $matches[1][$i]);

      // set the first 'from' IP
      for($i=0; $i<count($this->msgInfo->hosts); $i++)
      {
         if($this->msgInfo->hosts[$i] == "127.0.0.1")
            continue;

         if(preg_match("/^192\.168\./", $this->msgInfo->hosts[$i]))
            continue;

         $this->msgInfo->host = $this->msgInfo->hosts[$i];
         break;
      }

      if(! $this->msgInfo->host)
         $this->msgInfo->host = $this->msgInfo->hosts[$i-1];
   }

   return $this->msgInfo;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetHeader
//
// [DESCRIPTION]:   Return the header of the email
//
// [PARAMETERS]:    $messageNr = 0
//
// [RETURN VALUE]:  String if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetHeader($messageNr = 0)
{
   $this->strERR = "";

   $strHeader = imap_fetchheader($this->imapConn, $messageNr);

   if(empty($strHeader))
   {
      $this->strERR = "cannot get mail's header for message $messageNr [$this->user@$this->host]";
      return false;
   }

   return $strHeader;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetHeaders
//
// [DESCRIPTION]:   Return the headers of the email found on a mailbox
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Array if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetHeaders()
{
   $this->strERR = "";

   $headers = imap_headers($this->imapConn);

   if(count($headers) == 0)
   {
      $this->strERR = "cannot get mailbox headers [$this->user@$this->host]";
      return false;
   }

   return $headers;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetHeaderInfo
//
// [DESCRIPTION]:   Return the header details of the email
//
// [PARAMETERS]:    $messageNr = 0
//
// [RETURN VALUE]:  Object if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetHeaderInfo($messageNr = 0)
{
   $this->strERR = "";

   $headerInfo = imap_headerinfo($this->imapConn, $messageNr);

   if(! $headerInfo)
   {
      $this->strERR = "cannot get mail's header info for message $messageNr [$this->user@$this->host]";
      return false;
   }

   return $headerInfo;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetBody
//
// [DESCRIPTION]:   Return the body of the email
//
// [PARAMETERS]:    $messageNr = 0
//
// [RETURN VALUE]:  String if success, false otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetBody($messageNr = 0)
{
   $this->strERR = "";

   $strBody = imap_body($this->imapConn, $messageNr);

   if(empty($strBody))
   {
      $this->strERR = "cannot get mail's body for message $messageNr [$this->user@$this->host]";
      return false;
   }

   return $strBody;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTextBody
//
// [DESCRIPTION]:   Look for all text parts in the message
//
// [PARAMETERS]:    $messageNr = 0
//
// [RETURN VALUE]:  String if success, null string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTextBody($messageNr = 0)
{
   $this->strERR = "";
   $strText = "";

   $msgStruct = imap_fetchstructure($this->imapConn, $messageNr);
   //print_r($msgStruct);

   if(! $msgStruct)
   {
      $this->strERR = "cannot get the structure for message $messageNr [$this->user@$this->host]";
      return $strText;
   }

   // go through the message's parts
   $parts = count($msgStruct->parts);
   for($i=0; $i<$parts; $i++)
   {
      if($msgStruct->parts[$i]->type == "0" && strtolower($msgStruct->parts[$i]->subtype) == "plain")
         $strText .= imap_fetchbody($this->imapConn, $messageNr, $i+1);
   }

   return $strText;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMsgSize
//
// [DESCRIPTION]:   Return the size of the email message
//
// [PARAMETERS]:    $messageNr = 0
//
// [RETURN VALUE]:  Size number if success, -1 otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMsgSize($messageNr = 0)
{
   $this->strERR = "";
   $mailSize = -1;

   $msgStruct = imap_fetchstructure($this->imapConn, $messageNr);
   //print_r($msgStruct);

   if(! $msgStruct)
   {
      $this->strERR = "cannot get the structure for message $messageNr [$this->user@$this->host]";
      return $mailSize;
   }

   $mailSize = $msgStruct->bytes;

   return $mailSize;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetHost
//
// [DESCRIPTION]:   Set the host we connect to
//
// [PARAMETERS]:    $host = "localhost"
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetHost($host = "localhost")
{
   if(empty($host))
      return;

   $this->host = $host;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetUser
//
// [DESCRIPTION]:   Set the user name we use to connect to the IMAP server
//
// [PARAMETERS]:    $user = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetUser($user = "")
{
   if(empty($user))
      return;

   $this->user = $user;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetPass
//
// [DESCRIPTION]:   Set the user's password we use to connect to the IMAP server
//
// [PARAMETERS]:    $pass = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetPass($pass = "")
{
   if(empty($pass))
      return;

   $this->pass = $pass;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetType
//
// [DESCRIPTION]:   Set the IMAP server type: pop3, IMAP, pop3s, imaps
//
// [PARAMETERS]:    $type = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetType($type = "")
{
   if(empty($type))
      return;

   $this->type = strtolower($type);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetDebug
//
// [DESCRIPTION]:   Set the debug mode
//
// [PARAMETERS]:    $debug = 1
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetDebug($debug = 1)
{
   if(empty($debug))
      return;

    if($debug == 0 || $debug == 1)
      $this->debug = $debug;

   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetHost
//
// [DESCRIPTION]:   Return the host we connect to
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  host string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetHost()
{
   return $this->host;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUser
//
// [DESCRIPTION]:   Return the user name
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  IMAP user name string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUser()
{
   return $this->user;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetPass
//
// [DESCRIPTION]:  Return the user's password
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  IMAP password string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetPass()
{
   return $this->pass;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetType
//
// [DESCRIPTION]:   Return the IMAP server type: pop3, IMAP
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  IMAP type string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetType()
{
   return $this->type;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDebug
//
// [DESCRIPTION]:   Return the debug mode
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Debug mode number
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDebug()
{
   return $this->debug;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MakeReplyText
//
// [DESCRIPTION]:   Create a text as a reply text
//
// [PARAMETERS]:    $msgText = "", $replyMarker=": "
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MakeReplyText($msgText = "", $replyMarker=": ")
{
   if(empty($msgText))
      return;

   $msgText = preg_replace("/^/m", $replyMarker, $msgText);

   return $msgText;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ParseMessage
//
// [DESCRIPTION]:   Parses an email message and returns an array that contains
//                  the following keys: from, to, subjcet, body
//
// [PARAMETERS]:    $msgText = ""
//
// [RETURN VALUE]:  array
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ParseMessage($msgText = "")
{
   $arrayResult = array();

   if(empty($msgText))
      return $arrayResult;

   $msgText = str_replace("\r","",$msgText);
   list($header, $body) = split("\n\n", $msgText, 2);

   if(preg_match("/^From: ((.+)\s)?([^\s]+)$/mi", $header, $matches))
   {
      $fromRealName = $matches[1];
      $from = $matches[3];

      $from = str_replace("<", "",  $from);
      $from = str_replace(">", "",  $from);
   }

   if(preg_match("/^To: ((.+)\s)?([^\s]+)$/mi", $header, $matches))
   {
      $toRealName = $matches[1];
      $to = $matches[3];

      $to = str_replace("<", "",  $to);
      $to = str_replace(">", "",  $to);
   }

   if(preg_match("/^Subject: (.+)$/mi", $header, $matches))
      $subject = $matches[1];

   if(preg_match("/^Date: (.+)$/mi", $header, $matches))
      $date = $matches[1];

   $arrayResult["fromRealName"] = $fromRealName;
   $arrayResult["from"]         = $from;
   $arrayResult["toRealName"]   = $toRealName;
   $arrayResult["to"]           = $to;
   $arrayResult["subject"]      = $subject;
   $arrayResult["header"]       = $header;
   $arrayResult["body"]         = $body;
   $arrayResult["date"]         = $date;

   if(preg_match_all("/^Received: .*\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\]/m", $header, $matches))
   {
     for($i=0; $i<count($matches[0]); $i++)
       $arrayResult["hosts"][$i] = $matches[1][$i];

      // set the first 'from' IP
      for($i=0; $i<count($arrayResult["hosts"]); $i++)
      {
         if($arrayResult["hosts"][$i] == "127.0.0.1")
            continue;

         if(preg_match("/^192\.168\./", $arrayResult["hosts"][$i]))
            continue;

         $arrayResult["host"] = $arrayResult["hosts"][$i];
         break;
      }

      if(! $arrayResult["host"])
         $arrayResult["host"] = $arrayResult["hosts"][$i-1];
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
// [FUNCTION NAME]: GenerateMimeBoundary
//
// [DESCRIPTION]:   Generates a MIME boundary string, outlook express like
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  the MIME boundary string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GenerateMimeBoundary()
{
   $garbleText = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','R','S','T','U','V','X','Y','W','Z');

   $maxIndex = count($garbleText) - 1;
   $mimeBoundary = "----=_NextPart_";

   mt_srand($this->make_seed());
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= "_";

   mt_srand($this->make_seed());
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= "_";

   mt_srand($this->make_seed());
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= ".";

   mt_srand($this->make_seed());
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];
   $mimeBoundary .= $garbleText[mt_rand(0, $maxIndex)];

   return $mimeBoundary;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the connection with the IMAP server
//
// [PARAMETERS]:    $expungeMessages = 0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close($expungeMessages = 0)
{
   if($expungeMessages)
      imap_close($this->imapConn, CL_EXPUNGE);
   else
      imap_close($this->imapConn);

   $this->imapConn = 0;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
   $imapErrMsg = imap_last_error();
   imap_errors();

   if(! empty($imapErrMsg))
   {
      if(! empty($this->strERR))
         $this->strERR .= "\n";

      $this->strERR .= $imapErrMsg;
   }

   return $this->strERR;
}

// Make random number
function make_seed()
{
   list($usec, $sec) = explode(' ', microtime());
   return (float) $sec + ((float) $usec * 100000000);
}

} // end of class CIMAP

?>

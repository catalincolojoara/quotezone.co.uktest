<?php

redirect("https://taxi-insurance.quotezone.co.uk/taxi/index.php");

function redirect($url="")
{
   if(headers_sent())
   {
               print <<<  END_TAG

               <SCRIPT LANGUAGE="JavaScript">
               <!--
               location.href="$url";
               //-->
               </SCRIPT>

END_TAG;

   }
   else
   {
      $today = date("D M j G:i:s Y");
      header("HTTP/1.1 200 OK");
      header("Date: $today");
      header("Location: $url");
      header("Content-Type: text/html");
   }

   exit(0); //if it doesn't work die (Javascript disabled?)
}


?>

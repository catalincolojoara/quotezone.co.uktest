
HEADERS: MIME-Version: 1.0
Content-type: text/plain; charset=iso-8859-1
X-Mailer: PHP
X-Priority: 1

FROM:taxi@quotezone.co.uk
TO:qa@seopa.com
SUBJECT: Taxi query : Mr Qa Pen Fix Test Test Fake
BODY: QUOTEZONE 2017/09/13 01:02:27
TAXI INSURANCE QUERY DETAILS

No
 REGISTRATION NUMBER : 
 TAXI MODEL : A4
 YEAR OF MANUFACTURE : 2013
 ENGINE SIZE : 1968 D
 ESTIMATED TAXI VALUE (£) : 3500
 TAXI TYPE : Black Cab / Hackney Carriage
 MAIN TAXI USE : Private Hire
 ARE YOU AN UBER DRIVER? : No
 MAXIMUM NUMBER OF PASSENGERS : 5
 TAXI LICENSING AUTHORITY : LIVERPOOL
 TYPE OF COVER : Fully Comprehensive
 TAXI NO CLAIMS BONUS : 1 Year
 PRIVATE CAR NO CLAIMS BONUS : 1 Year
 FULL UK LICENCE : Under 6 months
 TAXI BADGE : No Badge
 TAXI DRIVER(S) : Insured +1 Named Driver
 CLAIMS LAST 5 YEARS : Yes, Claims Made
 MOTORING CONVICTIONS LAST 5 YEARS : Yes, Motoring Conviction(s) 

FIRST NAME/SURNAME : Mr Qa Pen Fix Test
PREFERRED TELEPHONE NUMBER : 01234567890
ADDITIONAL TELEPHONE NUMBER (OPTIONAL) : 
EMAIL ADDRESS : test@quotezone.co.uk 
ADDRESS LINE 1 : 3   Milton Close 
TOWN/CITY : St. Ives
COUNTY : Cambridgeshire
POSTCODE : PE27 6TX
DATE OF BIRTH : 30/09/1984
DATE OF INSURANCE START : 13/09/2017
QUOTE REFERENCE : ZA8-MJE

This user has provided their contact information for the sole purpose of providing a taxi quotation. No further contact can be made with the user by you or third parties for other sales or marketing purposes.

Quotezone is not responsible for verifying the accuracy and completeness of any information provided by a user as part of this quotation request. Please verify this information with the user.

Seopa Ltd T/A Quotezone.co.uk and CompareNI.com. Registered office: Seopa Ltd, Blackstaff Studios, Floor 2, 8-10 Amelia Street, Belfast, Co. Antrim, BT2 7GS. Registered in Northern Ireland NI46322. Seopa Ltd is authorised and regulated by the Financial Conduct Authority (FCA). Our register number is 313860. Our permitted business is insurance mediation.
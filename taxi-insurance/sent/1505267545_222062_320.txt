
HEADERS: MIME-Version: 1.0
Content-type: text/plain; charset=iso-8859-1
X-Mailer: PHP
X-Priority: 1

FROM:taxi@quotezone.co.uk
TO:qa@seopa.com
SUBJECT: Taxi query : Mr Qa Pen Fix Test Test Fake
BODY: COMPARENI.COM 2017/09/13 02:52:26
TAXI INSURANCE QUERY DETAILS

No
 REGISTRATION NUMBER : PE55HZV
 TAXI MODEL : MICRA
 YEAR OF MANUFACTURE : 2005
 ENGINE SIZE : 1386 P
 ESTIMATED TAXI VALUE (£) : 9001
 TAXI TYPE : Saloon
 MAIN TAXI USE : Public Hire
 MAXIMUM NUMBER OF PASSENGERS : 4
 TAXI LICENSING AUTHORITY : MANCHESTER
 TYPE OF COVER : Fully Comprehensive
 TAXI NO CLAIMS BONUS : 3 Years
 PRIVATE CAR NO CLAIMS BONUS : 3 Years
 FULL UK LICENCE : Over 10 Years
 TAXI BADGE : Under 6 Months
 TAXI DRIVER(S) : Insured Only
 CLAIMS LAST 5 YEARS : No Claims
 MOTORING CONVICTIONS LAST 5 YEARS : No Convictions 

FIRST NAME/SURNAME : Mr Qa Pen Fix Test
PREFERRED TELEPHONE NUMBER : 01234567890
ADDITIONAL TELEPHONE NUMBER (OPTIONAL) : 
EMAIL ADDRESS : qatest@seopa.com 
ADDRESS LINE 1 : 1   Bresagh Cottages 
TOWN/CITY : Lisburn
COUNTY : County Antrim
POSTCODE : BT27 6TX
DATE OF BIRTH : 13/09/1984
DATE OF INSURANCE START : 13/09/2017
QUOTE REFERENCE : Z4R-8NU

This user has provided their contact information for the sole purpose of providing a taxi quotation. No further contact can be made with the user by you or third parties for other sales or marketing purposes.

CompareNI.com is not responsible for verifying the accuracy and completeness of any information provided by a user as part of this quotation request. Please verify this information with the user.

Seopa Ltd T/A Quotezone.co.uk and CompareNI.com. Registered office: Seopa Ltd, Blackstaff Studios, Floor 2, 8-10 Amelia Street, Belfast, Co. Antrim, BT2 7GS. Registered in Northern Ireland NI46322. Seopa Ltd is authorised and regulated by the Financial Conduct Authority (FCA). Our register number is 313860. Our permitted business is insurance mediation.
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta name="robots" content="index, follow" />
        <meta name="keywords" content="taxi insurance" />
		<meta name="description" content="Compare cheap taxi insurance quotes at Quotezone.co.uk. Search a range of private hire insurance plans." />
		<title>Taxi Insurance Online from Quotezone - Compare Taxi Insurance Quotes</title>
		<link href="https://www.quotezone.co.uk/favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <link rel="stylesheet" href="https://www.quotezone.co.uk/css/template.css" type="text/css" />
        <link rel="stylesheet" href="https://www.quotezone.co.uk/css/qz_menu.css" type="text/css" />
        <link rel="stylesheet" href="https://www.quotezone.co.uk/css/qz_motoring.css" type="text/css" />
        <link rel="stylesheet" href="https://www.quotezone.co.uk/css/qz_overture.css" type="text/css" />
        <link rel="stylesheet" href="https://www.quotezone.co.uk/css/ieonly.css" type="text/css" /> 
        <meta http-equiv="Content-Language" content="en" />
		
    </head>
    <body id="page_bg">
    
<script type="text/javascript"> 
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."); 
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E")); 
</script> 
<script type="text/javascript"> 
try { 
var pageTracker = _gat._getTracker("UA-1359088-1"); 
pageTracker._setDomainName(".quotezone.co.uk"); 
pageTracker._trackPageview(); 
} catch(err) {}</script>
    
        <div class="center" align="center">
            <div id="footerSpider" style="top: 800px;">
                <div class="moduletable">
                    <ul>
                        <li>
                            <a href="http://www.quotezone.co.uk/">Home Page</a>
                        </li>
                        <li>
                            <a href="http://www.quotezone.co.uk/car-insurance.htm" >Car Insurance</a>
                        </li>
                        <li>
                            <a href="http://www.quotezone.co.uk/van-insurance.htm" >Van Insurance</a>
                        </li>
                        <li>
                            <a href="http://www.quotezone.co.uk/home-insurance.htm" >Home Insurance</a>
                        </li>
                        
                    </ul>
					<ul>
						<li>
                            <a href="https://loans.quotezone.co.uk/loans/">Loans</a>
                        </li>
                        
                                <li>
                                    <a href="http://www.quotezone.co.uk/life-insurance.htm">Life Insurance</a>
                                </li>
                                <li>
                                    <a href="http://www.quotezone.co.uk/bike-insurance.htm">Bike Insurance</a>
                                </li>
                                <li>
                                    <a href="http://www.quotezone.co.uk/travel-insurance.htm">Travel Insurance</a>
                                </li>           
                                
                            </ul>
							<ul>
								<li>
                                    <a href="http://www.quotezone.co.uk/pet-insurance.htm">Pet Insurance</a>
                                </li>
                                <li>
                                    <a href="http://www.quotezone.co.uk/health-insurance.htm">Health Insurance</a>
                                </li>
                                <li>
                                    <a href="http://www.quotezone.co.uk/caravan-insurance.htm">Caravan Insurance</a>
                                </li>
								<li>
                                    <a href="http://www.quotezone.co.uk/motorhome-insurance.htm">Motorhome Cover</a>
                                </li>
                                
							</ul>
							<ul>
								<li>
                                    <a href="http://www.quotezone.co.uk/campervan-insurance.htm">Campervan Cover</a>
                                </li>
                                <li>
                                    <a href="http://www.quotezone.co.uk/business-insurance.htm">Business Insurance</a>
                                </li>
                                <li>
                                    <a href="http://www.quotezone.co.uk/boat-insurance.htm">Boat Insurance</a>
                                </li>
                                <li>
                                    <a href="http://www.quotezone.co.uk/credit-cards.htm">Credit Cards</a>
                                </li>
                                
							</ul>
							<ul>
								<li>
                                    <a href="http://www.quotezone.co.uk/mortgages.htm">Mortgages</a>
                                </li>
                                <li>
                                    <a href="http://www.quotezone.co.uk/mortgage-protection.htm">Mortgage Protection</a>
                                </li>
                                <li>
                                    <a href="http://www.quotezone.co.uk/income-protection.htm">Income Protection</a>
                                </li>
                                <li>
                                    <a href="http://www.quotezone.co.uk/warranties.htm">Warranties</a>
                                </li>
								
							</ul>
							
                </div>
            </div>
            <div id="wrapper">
                <div id="wrapper_r">
                    <div id="header">
                        <div id="header_l">
                            <div id="header_r">
                                <div id="logo">
                                    <a href="http://www.quotezone.co.uk/" title="QuoteZone Home Page"><img src="https://www.quotezone.co.uk/images/qz_top_logo.gif" alt="QuoteZone Logo"/></a>
                                </div>
                                
				     <?php
      					include("includes/topmenu-v2.php"); 
      					
    				     ?>	

                                <div id="bannerbox">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="topTabs">
                        <ul class="menuprimary">
                            <li class="item1"><a href="http://www.quotezone.co.uk/"><span>Home</span></a></li>
                            <li id="current" class="parent active item27"><a href="http://www.quotezone.co.uk/insurance.htm"><span>Insurance</span></a></li>
                            <li class="parent item50"><a href="http://www.quotezone.co.uk/money.htm"><span>Money</span></a></li>
                            <li><a href="http://www.quotezone.co.uk/utilities.htm"><span>Utilities</span></a></li>
                            <li class="parent item41"><a href="http://www.quotezone.co.uk/travel.htm"><span>Travel</span></a></li>
                            <li><a href="http://www.quotezone.co.uk/motoring.htm"><span>Motoring</span></a></li>
                            <li class="parent item48"><a href="http://www.quotezone.co.uk/business.htm"><span>Business</span></a></li>
                            <li class="parent item49"><a href="http://www.quotezone.co.uk/shopping.htm"><span>Shopping</span></a></li>
                        </ul>
                    </div>
                    <div id="subTopTabs">
                       	<div class="centerTopBox">
    <?php
      include("includes/insurance-submenu.php"); 
      ShowSubMenu("Taxi");
    ?>							
						</div>
						<div class="subMenuFooter">
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="whitebox">
                        <div id="whitebox_t">
                            <div id="whitebox_tl">
                                <div id="whitebox_tr">
                                </div>
                            </div>
                        </div>
                        <div id="whitebox_m">
                            <div id="area">
                                <div id="leftcolumn">
                                </div>
                                <div id="maincolumn_full">
                                    <div id="maindivider">
                                    </div>
                                    <div class="main_col">
                                        <div class="main_col_left">
                                            <div id="page">
                                                <p class="buttonheading">
                                                </p>
                                                
                                                <div class="mainLeftBigBox_ins">
                                                    <div class="mainLeftBigBoxLeft_ins">
                                                    </div>
                                                    <div class="mainLeftBigBoxCenter_ins">
                                                        <div class="iconWrap">
                                                            <img src="https://www.quotezone.co.uk/images/med_taxi_img.gif" alt="Compare Taxi Insurance" />
                                                        </div>
                                                        <div class="titleWrap">
                                                           <h3>Compare Taxi Insurance Quotes</h3> 
                                                        </div>
                                                    </div>
                                                    <div class="mainLeftBigBoxRight_ins">
                                                    </div>
                                                </div>
                                            </div>
											<?php
											include("includes/overture.php"); 
											ShowOvertureAds("Taxi Insurance");
											?>
                                        </div>
                                        <div class="main_col_right">
                                            <div class="moduletable">
                                                <div id="mainRightBox">
                                                    <div class="mainRightBoxWeSearch" style="width: 220px;">
                                                        <br/>
                                                        <h3>We Search these Companies and more...</h3>
                                                        <iframe src="https://www.quotezone.co.uk/scrollerNew.htm" style="border: 1px solid rgb(204, 204, 204);" marginwidth="0" marginheight="0" scrolling="no" width="215" frameborder="0" height="35">
                                                        </iframe>
                                                    </div>
                                              	 	<?php include("includes/rightmenu.php"); ?>
												 </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                            </div>
                        </div>
                        <div id="whitebox_b">
                            <div id="whitebox_bl">
                                <div id="whitebox_br">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="footerspacer">
                    </div>
                </div>
                <div id="footerBox">
                    <div class="footerBoxLeft">
                    </div>
                    <div class="footerBoxCenter">
                        <div class="footerLinks">
                            <ul class="menu">
                                <li>
                                    <a href="http://www.quotezone.co.uk/legal.htm" rel="nofollow"><span>Terms and Conditions</span></a>
                                </li>
                                <li>
                                    <a href="http://www.quotezone.co.uk/privacy-policy.htm" rel="nofollow"><span>Privacy Policy</span></a>
                                </li>
                                <li>
                                    <a href="http://www.quotezone.co.uk/contact-quotezone.htm" rel="nofollow"><span>Contact Us</span></a>
                                </li>
                                <li>
                                    <a href="http://www.quotezone.co.uk/about-quotezone.htm" rel="nofollow"><span>About Our Services</span></a>
                                </li>
                                <li>
                                    <a href="http://www.quotezone.co.uk/sitemap.htm"><span>Sitemap</span></a>
                                </li>
                            </ul>
                        </div>
                        <div class="footerCopyright">
                            <img src="https://www.quotezone.co.uk/images/qz_small.gif" alt="Quotezone Footer Icon" />
                            <p>
                                Copyright &copy; <?php echo date("Y")?> Quotezone.co.uk
                            </p>
                        </div>
                    </div>
                    <div class="footerBoxRight">
                    </div>
                </div>
            </div>
        </div>
        <div class="timeDateRegion">
            <p>
            </p>
        </div>
        
    </body>
</html>
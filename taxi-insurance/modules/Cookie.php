<?php
/*****************************************************************************/
/*                                                                           */
/*  CCookie class interface                                            */
/*                                                                           */
/*  (C) 2004 Eugen Savin (eugen@claspsolutions.com)                          */
/*                                                                           */
/*****************************************************************************/
// include_once "globals.adm.inc";

error_reporting(0);

define("AFFILIATES_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";



if(DEBUG_MODE)
   error_reporting(E_ALL);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CCookie
//
// [DESCRIPTION]:  CCookie class interface
//
// [FUNCTIONS]:    int  AddCookie($cref="", $crDate=0)
//                 bool UpdateCookie($crefID=0, $cref="")
//                 bool DeleteCookie($crefID=0);
//                 array|false GetCookieByID($crefID=0);
//                 array|false GetCookieByName($cref="");
//                 array|false GetAllCookies();
//                 bool AssertCookie()
//
//                 void Close();
//                 string GetError();
//
// [CREATED BY]:   Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CCookie
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CCookie
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CCookie($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddCookie
//
// [DESCRIPTION]:   Add new entry to the cookies table
//
// [PARAMETERS]:    $cref="" , $crDate = 0
//
// [RETURN VALUE]:  quoteID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddCookie($cref="", $crDate=0)
{

   if(! $this->AssertCookie($cref, $crDate))
      return 0;

   // check if ID exists in DB
   $sqlCmd = "SELECT id FROM ".SQL_COOKIES." WHERE cref='$cref'";
       
   if(! $this->dbh->Exec($sqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   if($this->dbh->GetRows())
   {
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      return $this->dbh->GetFieldValue("id");
   }
   
   $sqlCmd = "INSERT INTO ".SQL_COOKIES." (cref,cr_date) VALUES ('$cref','$crDate')";
   
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $cookieID = $this->dbh->GetFieldValue("id");

   return $cookieID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateCookie
//
// [DESCRIPTION]:   Update cookies table
//
// [PARAMETERS]:    $crefID=0, $cref=""
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateCookie($crefID=0, $cref="")
{
//print "## $cookieID, $cookie";

   if(! preg_match("/^\d+$/", $crefID))
   {
      $this->strERR = GetErrorString("INVALID_COOKIEID_FIELD");
      return false;
   }

   if(! $this->AssertCookie($crefID, $cref))
      return false;

   // check if ID exists in DB
   $sqlCmd = "SELECT cref FROM ".SQL_COOKIES." WHERE id='$crefID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("COOKIEID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $dbCookie = $this->dbh->GetFieldValue("cref");

   if($cookie != $dbCookie)
   {
      // check if cookie exists in DB
      $sqlCmd = "SELECT id FROM ".SQL_COOKIES." WHERE cref='$cref'";

      if(! $this->dbh->Exec($sqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if($this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("COOKIE_ALREADY_EXISTS");
         return false;
      }
   }

   $sqlCmd = "UPDATE ".SQL_COOKIES." SET cref='$cref' WHERE id='$crefID'";

   //echo $sqlCmd ;
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteCookie
//
// [DESCRIPTION]:   Delete an entry from cookies table
//
// [PARAMETERS]:    $crefID = 0
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteCookie($crefID = 0)
{
   if(! preg_match("/^.{1,16}$/", $crefID))
   {
      $this->strERR = GetErrorString("INVALID_COOKIEID_FIELD");
      return false;
   }

   // check if ID exists in DB
   $sqlCmd = "SELECT id FROM ".SQL_COOKIES." WHERE id='$crefID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("COOKIEID_NOT_FOUND");
      return false;
   }

   $sqlCmd = "DELETE FROM ".SQL_COOKIES." WHERE id='$crefID'";

   //print "$sqlCmd";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCookieByID
//
// [DESCRIPTION]:   Read data from cookies table and put it into an array variable
//
// [PARAMETERS]:    $crefID = 0
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCookieByID($crefID = 0 )
{
   if(! preg_match("/^.{1,16}$/", $crefID))
   {
      print_r ($cookieID);
      $this->strERR = GetErrorString("INVALID_COOKIEID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT * FROM ".SQL_COOKIES." WHERE id='$crefID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("COOKIEID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]   = $this->dbh->GetFieldValue("id");
   $arrayResult["cref"] = $this->dbh->GetFieldValue("cref");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCookieByName
//
// [DESCRIPTION]:   Read data from cookies table and put it into an array variable
//
// [PARAMETERS]:    $cref = ""
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCookieByName($cref = "")
{
   if(! preg_match("/^.{1,16}$/", $cref))
   {
      $this->strERR = GetErrorString("INVALID_COOKIEID_FIELD");
      return false;
   }

   // check if we have this  cookie
   $sqlCmd = "SELECT id FROM ".SQL_COOKIES." WHERE cref='$cref'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("COOKIEID_NOT_FOUND");
      return false;
   }

   $sqlCmd = "SELECT * FROM ".SQL_COOKIES." WHERE cref='$cref' ORDER BY cref ASC";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]     = $this->dbh->GetFieldValue("id");
   $arrayResult["cref"] = $this->dbh->GetFieldValue("cref");

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllCookies
//
// [DESCRIPTION]:   Read data from cookies table and put it into an array variable
//
// [PARAMETERS]:    
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllCookies()
{
   $sqlCmd = "SELECT * FROM ".SQL_COOKIES." ORDER BY id ";


   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("COOKIEIDS_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("cref");

  //print_r ($arrayResult);

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertCookie
//
// [DESCRIPTION]:   Check if all fields are OK and set the error string if needed
//
// [PARAMETERS]     $cookie, $crDate
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertCookie($cref, $crDate)
{
   $this->strERR = "";

   if(! preg_match("/^.{1,32}$/", $cref))
      $this->strERR .= GetErrorString("INVALID_NAME_FIELD")."\n";
      
   if(! preg_match("/^.{1,32}$/", $crDate))
      $this->strERR .= GetErrorString("INVALID_CRDATE_FIELD")."\n";

   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

} // end of CCookie class
?>

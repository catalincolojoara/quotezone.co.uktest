<?php
/*****************************************************************************/
/*                                                                           */
/*  CArList class interface                                                 */
/*                                                                           */
/*  (C) 2004 ISTVNACSEK Gabriel (null@seopa.com)                              */
/*                                                                           */
/*****************************************************************************/
define("ARLIST_INCLUDED", "1");

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CArList
//
// [DESCRIPTION]:  CArList class interface
//
// [FUNCTIONS]:    void  Add($index = 0, $element=array())
//                 void  Insert($index=0, $subIndex=0, $element=array())
//                 void  Update($index=0, $subIndex=0, $element=array())
//                 void  Remove($index=0, $subIndex=0)
//
//                 int   GetCount($index = 0);
//                 array GetElement($index=0, $subIndex=0);
//
//                 array GetList();
//
//                 void Free();
//
// [CREATED BY]:   Eugen Savin (null@seopa.com) 2005-02-24
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CArList
{
    var $listSize;
    var $listArray;

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CArList
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CArList($listSize = 0)
{
   $this->listSize  = $listSize;
   $this->listArray = array();

   for($i=0;  $i<$listSize; $i++)
      $this->listArray[$i] = array();

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Add
//
// [DESCRIPTION]:   Adds an element to the list
//
// [PARAMETERS]:    $index=0, $element=array()
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Add($index = 0, $element=array())
{
   // '1' indexed
   $index--;

   if($index < 0 || $index > $this->listSize)
      return;

   $curSubArrayIndex = count($this->listArray[$index]);

   $this->listArray[$index][$curSubArrayIndex] = $element;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Insert
//
// [DESCRIPTION]:   Inserts an element in the list at a specific position
//
// [PARAMETERS]:    $index=0, $subIndex=0, $element=array()
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Insert($index=0, $subIndex=0, $element=array())
{
   // '1' indexed
   $index--;
   $subIndex--;

   if($index < 0 || $index > $this->listSize)
      return;

   $curSubArrayIndex = count($this->listArray[$index]);

   if($subIndex < 0 || $subIndex > $curSubArrayIndex)
      return;

   if($subIndex == $curSubArrayIndex)
   {
      $this->Add($index+1, $element);
      return;
   }

   // make the new array
   $tempArray = $this->listArray[$index];
   $this->listArray[$index] = array();

   $subIndexCounter = 0;
   foreach($tempArray as $key => $value)
   {
      if($subIndex == $key)
      {
         $this->listArray[$index][$subIndexCounter] = $element;
         $subIndexCounter++;
      }

      $this->listArray[$index][$subIndexCounter] = $value;
      $subIndexCounter++;
   }

   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Update
//
// [DESCRIPTION]:   Updates an element in the list at a specific position
//
// [PARAMETERS]:    $index=0, $subIndex=0, $element=array()
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Update($index=0, $subIndex=0, $element=array())
{
   // '1' indexed
   $index--;
   $subIndex--;

   if($index < 0 || $index > $this->listSize)
      return;

   $curSubArrayIndex = count($this->listArray[$index]);

   if($subIndex < 0 || $subIndex > $curSubArrayIndex)
      return;

   // make the new array
   $tempArray = $this->listArray[$index];
   $this->listArray[$index] = array();

   $subIndexCounter = 0;
   foreach($tempArray as $key => $value)
   {
      if($subIndex == $key)
         $this->listArray[$index][$subIndexCounter] = $element;
      else
         $this->listArray[$index][$subIndexCounter] = $value;

      $subIndexCounter++;
   }

   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Remove
//
// [DESCRIPTION]:   Removess an element from the list
//
// [PARAMETERS]:    $index=0, $subIndex=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Remove($index=0, $subIndex=0)
{
   // '1' indexed
   $index--;
   $subIndex--;

   if($index < 0 || $index > $this->listSize)
      return;

   $curSubArrayIndex = count($this->listArray[$index]);

   if($subIndex < 0 || $subIndex > $curSubArrayIndex)
      return;

   // make the new array
   $tempArray = $this->listArray[$index];
   $this->listArray[$index] = array();

   $subIndexCounter = 0;
   foreach($tempArray as $key => $value)
   {
      if($subIndex == $key)
         continue;

      $this->listArray[$index][$subIndexCounter] = $value;
      $subIndexCounter++;
   }

   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCount
//
// [DESCRIPTION]:   Returns the elements number of the given index
//
// [PARAMETERS]:    $index = 0
//
// [RETURN VALUE]:  the number of the elements found in index
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCount($index = 0)
{
   // '1' indexed
   $index--;

   if($index < 0 || $index > $this->listSize)
      return -1;

   return count($this->listArray[$index]);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetElement
//
// [DESCRIPTION]:   Returns the element array given index
//
// [PARAMETERS]:    $index = 0, $subIndex = 0
//
// [RETURN VALUE]:  the number of the elements found in index
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetElement($index = 0, $subIndex = 0)
{
   // '1' indexed
   $index--;
   $subIndex--;

   if($index < 0 || $index > $this->listSize)
      return;

   return $this->listArray[$index][$subIndex];
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetList
//
// [DESCRIPTION]:   Returns the formatted list
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array of elements one level deeper
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetList()
{
   $listArray = array();
   $index = 1;

   foreach($this->listArray as $key => $value)
   {
      foreach($value as $k => $v)
      {
         $listArray[$index] = $v;
         $index++;
      }
   }

   return $listArray;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Empty
//
// [DESCRIPTION]:   Empties the elements in the array list
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Free()
{
   foreach($this->listArray as $key => $value)
      $this->listArray[$key] = array();

   return;
}

} // end CArList

?>
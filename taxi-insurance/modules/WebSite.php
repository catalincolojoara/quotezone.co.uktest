<?php
/*****************************************************************************/
/*                                                                           */
/*  CWebSite class interface                                                 */
/*                                                                           */
/*  (C) 2004 Eugen Savin (seugen@abc.ro)                                     */
/*                                                                           */
/*****************************************************************************/

define("WEB_SITE_INCLUDED", "1");

include_once "Template.php";
include_once "MySQL.php";
include_once "User.php";
include_once "IpBan.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CWebSite
//
// [DESCRIPTION]:  CWebSite class interface
//
// [FUNCTIONS]:    void Initialise();
//                 void SetTitle($title = "");
//
//                 virtual bool Prepare();
//                 void SetWorkArea($workAreaContent = "");
//                 void Show();
//                 bool Run();
//
//                 dbh  GetDbHandle();
//                 int  GetUserID();
//                 user GetUserObj();
//                 string GetTmplsDir();
//
//                 void Error($errString = "");
//                 string GetError();
//                 void SetOfflineMessage($message);
//                 void Close();
//
// [CREATED BY]:   Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CWebSite
{
    // class-internal variables
    // database handler
    var $dbh;         // database server handle
    var $userObj;     // user object

    var $userID;      // user ID if logged on
    var $userName;    // user Name if logged on
    var $userEmail;   // user Email if logged on
    var $admin;       // admin name

    var $menuType;           // menu type related to user's actions
    var $siteContent;        // web site content
    var $siteTitle;          // the title of the shown web page
    var $templates;          // templates array("file name" => "file contents")
    var $templatesDirectory; // templates directory
    var $loadTemplates;      // keep the templates that will be pre-loaded in this array
    var $metaTags;           // key the meta tags entries array

    /****************************************************************************/
    /*  In the section listed below, sometimes ads refers to js tracking codes. */
    /****************************************************************************/

    var $googleAds;        // google ads flag
    var $msnAds;           // msn ads flag
    var $overtureAds;      // overture ads flag
    var $searchVisionAds;  // searchvision ads flag    
    var $amadesaTracker;   // amadesa js tracker
    var $quoteUserID;      // quote user id used by the google/indextools javascript
    var $svQuoteUserID;    // quote user id used by the searchVision javascript tracker
    var $trackerPage;      // used for tracking purposes
    var $vehiclePopunder;  // used to display some overture popunder in the vehicle page
    var $googleTracker;    // google js tracker
    var $showPopUpDiv;     // show the popup div
    var $setOnBeforeUnloadToLastPage; // set the function that will be called when the driver close the windon to the last page

    var $strERR;           // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CWebSite
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CWebSite($templatesDir="")
{
   $this->strERR = "";

   $this->userID    = 0;
   $this->userName  = "";
   $this->userEmail = "";
   $this->admin     = "";

   $this->menuType = COMMON_MENU;

   $this->siteContent = "";
   $this->siteTitle   = "";
   $this->templates   = array();
   $this->loadTemplates = array();
   $this->metaTags['robots']      = 'index,follow'; //default

   $this->imageLevel        = "0";
   $this->imageDir          = "";
   $this->googleAds         = false;
   $this->overtureAds       = false;
   $this->searchVisionAds   = false;
   $this->msnAds            = false;
   $this->amadesaTracker    = '';
   $this->googleTracker     = '';
   $this->quoteUserID       = 0;
   $this->svQuoteUserID     = 0;
   $this->trackerPage       = "";
   $this->vehiclePopunder   = "";

   $this->imagesPreloaded   = "";
   $this->showPopUpDiv      = "";
   $this->setOnBeforeUnloadToLastPage = "";

   // read template files
   $this->templatesDirectory = TEMPLATES_DIR;

   if(! empty($templatesDir))
      $this->templatesDirectory = $templatesDir;

   $this->Initialise();

   $objIpBan = new CIpBan();
   if($objIpBan->IsIpBan($_SERVER['REMOTE_ADDR']))
      $this->SetBannedIpWebsite();

// uncomment this if you want to put offline all websites car, van , home , bike ...
//  $this->SetOfflineWebsite();

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Initialise
//
// [DESCRIPTION]:   Initialises the web site class.
//                  - initialise php session
//                  - get userID in case someone's logged in
//                  - get menu type
//                  - read all files located  in the templates dir and put them
//                    into an array: key = file name, value = file content
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Initialise()
{
   session_start();

   $this->menuType  = COMMON;
   $this->userID    = 0;
   $this->userName  = "";
   $this->userEmail = "";
   $this->admin     = $_SERVER["REMOTE_USER"];

   $this->dbh = new CMySQL();

   if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
   {
      $this->strERR = $this->dbh->GetError();
      return;
   }

   $this->userObj = new CUser($this->dbh);

   if(! empty($_SESSION["session_key"]))
   {
      $this->userID    = $this->userObj->GetUserID($_SESSION["session_key"]);
      $this->userEmail = $_SESSION["userEmail"];

      if($this->userID)
         $this->menuType = SIMPLE_USER;
   }

   if(! empty($this->admin))
      $this->menuType = ADMIN_USER;

   if(! is_dir($this->templatesDirectory))
   {
      $this->strERR = GetErrorString("DIR_NOT_FOUND").": [".$this->templatesDirectory."]";
      return;
   }

   $this->loadTemplates = explode(" ", PRELOAD_TEMPLATES);
   foreach($this->loadTemplates as $tmplFile)
   {
      if(! ($fh = fopen($this->templatesDirectory."/$tmplFile", "r")))
      {
         $this->strERR = GetErrorString("CANNOT_OPEN_FILE").": [".$this->templatesDirectory."/$tmplFile]";
         return;
      }

      $this->templates[$tmplFile] = fread($fh, filesize($this->templatesDirectory."/$tmplFile"));
      fclose($fh);
   }

  return;

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetTitle
//
// [DESCRIPTION]:   Set the web site title
//
// [PARAMETERS]:    $title = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetTitle($title = "")
{
   $this->siteTitle = $title;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetMetaTag
//
// [DESCRIPTION]:   Set the meta tags of pages
//
// [PARAMETERS]:    $name="", $content=""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel Istvancsek (null@seopa.com) 2010-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetMetaTag($name="", $content)
{
   if(empty($name))
      return;
      
   if(empty($content))
      return;

   $this->metaTags[$name] = $content;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Prepare
//
// [DESCRIPTION]:   Prepare the web site. Replace template vars
//                  with the values based on a specific algorithm we define below
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Prepare()
{
   if(! empty($this->strERR))
      return false;

   $tmpl = new CTemplate();

   $tmpl->SetTemplateDirectory($this->templatesDirectory);

   if(! $tmpl->Load(WEB_SITE_FIRST_TMPL))
   {
      $this->strERR = $tmpl->GetError();
      return false;
   }

   $tmplVars = array();

   $tmplVars["site.title"]  = $this->siteTitle;
   $tmplVars["site.header"] = $this->templates['Header.tmpl'];
   $tmplVars["site.footer"] = $this->templates['Footer.tmpl'];

   foreach($this->metaTags as $name => $content)
   {
      $tmplVars["site.meta"]  .= "<meta name=\"".$name."\" content=\"".$content."\"> \n";
   }

   if($this->menuType == COMMON)
      $tmplVars["site.menu"] = $this->templates['CommonMenu.tmpl'];
   else if($this->menuType == SIMPLE_USER)
      $tmplVars["site.menu"] = $this->templates['UserMenu.tmpl'];
   else if($this->menuType == ADVANCED_USER)
      $tmplVars["site.menu"] = $this->templates['AdvancedMenu.tmpl'];
   else if($this->menuType == ADMIN_USER)
      $tmplVars["site.menu"] = $this->templates['AdminMenu.tmpl'];

   $tmplVars["site.work_area"] = $this->workAreaContent;

   $tmplVars["header.menu"] = $this->templates['HeaderMenu.tmpl'];

   $tmplVars["DIR"] = $this->imageDir;

   $tmplVars["LEVEL"] = $this->imageLevel;

   $tmplVars["IMAGES"] = $this->imagesPreloaded;

   $tmplVars["SETONBEFOREUNLOADTOLASTPAGE"] = $this->setOnBeforeUnloadToLastPage;
   $tmplVars["showpopupdiv"]                = $this->showPopUpDiv;

   if($this->googleAds)
   {
      $tmplGoogleAds = new CTemplate();

      if(! $tmplGoogleAds->Load("GoogleAds.tmpl"))
      {
         $this->strERR = $tmplGoogleAds->GetError();
         return false;
      }

      $googlePrepArray["orderid"] = $this->quoteUserID;
      $tmplGoogleAds->Prepare($googlePrepArray);

      $tmplVars["GOOGLE_ADS"] = $tmplGoogleAds->GetContent();
   }

   if($this->overtureAds)
   {
      $tmplOvertureAds = new CTemplate();

      if(! $tmplOvertureAds->Load("OvertureAds.tmpl"))
      {
         $this->strERR = $tmplOvertureAds->GetError();
         return false;
      }

      $tmplVars["OVERTURE_ADS"] = $tmplOvertureAds->GetContent();
   }

   if($this->searchVisionAds)
   {
      $tmplSearchVisionAds = new CTemplate();

      if(! $tmplSearchVisionAds->Load("SearchVisionAds.tmpl"))
      {
         $this->strERR = $tmplSearchVisionAds->GetError();
         return false;
      }

      $searchVisionPrepArray["orderid"] = $this->svQuoteUserID;
      $tmplSearchVisionAds->Prepare($searchVisionPrepArray);

      $tmplVars["SEARCHVISION_ADS"] = $tmplSearchVisionAds->GetContent();
   }

   if($this->amadesaTracker)
      $tmplVars["amadesa_tracker"] = $this->amadesaTracker;

   if($this->googleTracker)
      $tmplVars["google_tracker"] = $this->googleTracker;

   if($this->trackerPage)
      $tmplVars["tracker_page"] = $this->trackerPage;

   if($this->msnAds)
   {
      $tmplMsnAds = new CTemplate();
      if(! $tmplMsnAds->Load("MsnAds.tmpl"))
      {
         $this->strERR = $tmplMsnAds->GetError();
         return false;
      }

      $tmplVars["MSN_ADS"] = $tmplMsnAds->GetContent();
   }
 
   if($this->vehiclePopunder)
   {
      $tmplVehiclePopunder = new CTemplate();

      if(! $tmplVehiclePopunder->Load("VehiclePopunder.tmpl"))
      {
         $this->strERR = $tmplVehiclePopunder->GetError();
         return false;
      }

      $tmplVars["VEHICLE_POPUNDER"] = $tmplVehiclePopunder->GetContent();
   }

   // set current year
   $tmplVars["current_year.value"] = date("Y");

   // testing bike navigation menu
   $tmplVars["MENU"] = $this->menu;

   $tmpl->Prepare($tmplVars);

   $this->siteContent = $tmpl->GetContent();

   // clean up some session vars
   unset($_SESSION["loginErrorMessage"]);

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetMenu
//
// [DESCRIPTION]:   Set the menu 
//
// [PARAMETERS]:    $menuContent = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel ISTVANCSEK (null@seopa.com) 2007-03-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetMenu($menuContent = "")
{
   $this->menu = $menuContent;

   return;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetWorkArea
//
// [DESCRIPTION]:   Set the work area content
//
// [PARAMETERS]:    $workAreaContent = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetWorkArea($workAreaContent = "")
{
   $this->workAreaContent = $workAreaContent;

   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Show
//
// [DESCRIPTION]:   Show template content
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Show()
{
   print $this->siteContent;
   flush();
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetImageHeaderLevel
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetImageHeaderLevel($imageLevel)
{
   $this->imageLevel = $imageLevel;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetOnBeforeUnloadToLastPage($textToSet="")
//
// [DESCRIPTION]:   set the text that will be shown when a driver leaves quotezone
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function  SetOnBeforeUnloadToLastPage($textToSet="")
{
  $this->setOnBeforeUnloadToLastPage = $textToSet;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowPopupDivContent($textToSet="")
//
// [DESCRIPTION]:   show the div on the page
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function  ShowPopupDivContent($textToSet="")
{
  $this->showPopUpDiv = $textToSet;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetImageHeaderLevel
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetImagesPreloaded($images)
{
   foreach($images as $name)
   {
      $this->imagesPreloaded .= "'".$name."',";
   }

   $this->imagesPreloaded = substr($this->imagesPreloaded, 0, strlen($this->imagesPreloaded) - 1);

}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetImageHeaderLevel
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetImageDir($imageDir="")
{
   $this->imageDir = $imageDir;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetGoogleAds
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    $quoteUserID=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetGoogleAds($quoteUserID=0)
{
   $this->googleAds = true;
   $this->quoteUserID = $quoteUserID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetOvertureAds
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetOvertureAds()
{
   $this->overtureAds = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetSearchVisionAds
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetSearchVisionAds($quoteUserID)
{
   $this->searchVisionAds = true;
   $this->svQuoteUserID   = $quoteUserID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetAmadesaTracker
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetAmadesaTracker($amadesaCode='')
{
   $this->amadesaTracker = $amadesaCode;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetGoogleTracker
//
// [DESCRIPTION]:   set the js content of google tracker
//
// [PARAMETERS]:    js content
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2007-11-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetGoogleTracker($googleCode='')
{
   if(defined('TESTING_MODE'))
      if(TESTING_MODE)
         return;

   $this->googleTracker = $googleCode;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetTrackerPage
//
// [DESCRIPTION]:   set the page name for the tracker system
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetTrackerPage($pageName='')
{
   $this->trackerPage = $pageName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetMsnAds
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetMsnAds()
{
   $this->msnAds = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetVehiclePopunder
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetVehiclePopunder()
{
   $this->vehiclePopunder = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Run
//
// [DESCRIPTION]:   Run the website
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Run()
{
   if(! $this->Prepare())
      return false;

   $this->Show();

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDbHandle
//
// [DESCRIPTION]:   Return the database handle
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  database handle
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDbHandle()
{
   return $this->dbh;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserID
//
// [DESCRIPTION]:   Return the user ID
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  user ID
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserID()
{
   return $this->userID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserObj
//
// [DESCRIPTION]:   Return the user object
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  user object
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserObj()
{
   return $this->userObj;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTmplDir
//
// [DESCRIPTION]:   Return the templates directory
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  string
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTmplDir()
{
   return TEMPLATES_DIR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Error
//
// [DESCRIPTION]:   Show error and exit program!
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Error($errString = "")
{
   $this->SetWorkArea($errString);
   $this->Run();
   exit(0);
}

//////////////////////////////////////////////////////////////////////////////FB
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    $this->dbh->Close();
    $this->templates   = array();
    $this->siteContent = "";

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetOfflineWebsite
//
// [DESCRIPTION]:   Show offline website message
//
// [PARAMETERS]:    $message
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel Istvancsek (null@seopa.com) 2007-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetOfflineWebsite($message='This site is currently under maintenance. Please come back in 10 minutes.')
{
    $this->Error("<br><br><br><br><b>$message<br><br><br><br>");

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetBannedIpWebsite
//
// [DESCRIPTION]:   Show ip banned website message
//
// [PARAMETERS]:    $message
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel Istvancsek (null@seopa.com) 2007-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetBannedIpWebsite($message='This IP address has been automatically suspended from accessing our quote system due to repeated usage. Please contact info@quotezone.co.uk if you have any questions.')
{
    $this->Error("<br><br><br><br><b>$message<br><br><br><br>");

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckCookieSessionID
//
// [DESCRIPTION]:   Check cookie session id
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none | string
//
// [CREATED BY]:    Gabriel Istvancsek (null@seopa.com) 2007-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckCookieSessionID()
{
   if(! empty($_COOKIE[session_name()]))
      return;

   //get cookie via GET or POST
   if(! empty($_GET['cs']))
      $activeCookieSession = $_GET['cs'];

   if(! empty($_POST['cs']))
      $activeCookieSession = $_POST['cs'];

   if(empty($activeCookieSession))
   {
      if(empty($_SESSION))
         return session_id();
         
      return;
   }
   
   $currentCookieSession = session_id();

   if($currentCookieSession != $activeCookieSession)
   {
      session_destroy();
      
      session_id($activeCookieSession);
      session_start();
   }
   
   return $activeCookieSession;
}


}// end of CWebSite class

?>

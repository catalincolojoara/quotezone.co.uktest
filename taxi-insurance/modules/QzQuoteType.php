<?php
/*****************************************************************************/
/*                                                                           */
/*  CAffiliates class interface                                              */
/*                                                                           */
/*  (C) 2004 Eugen Savin (eugen@claspsolutions.com)                          */
/*                                                                           */
/*****************************************************************************/

define("AFFILIATES_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";



if(DEBUG_MODE)
   error_reporting(E_ALL);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQzQuoteType
//
// [DESCRIPTION]:  CQzQuoteType class interface
//
// [FUNCTIONS]:    int  AddQuoteType($quoteName="")
//                 bool UpdateQuoteType($quoteTypeID=0, $quoteName="")
//                 bool DeleteQuoteType($quoteTypeID=0);
//                 array|false GetQuoteTypeByID($quoteTypeID=0);
//                 array|false GetQuoteTypeByName($quoteName="");
//                 array|false GetAllQuoteTypes();
//                 bool AssertQuoteType()
//
//                 void Close();
//                 string GetError();
//
// [CREATED BY]:   Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CQzQuoteType
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQzQuoteType
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQzQuoteType($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteType
//
// [DESCRIPTION]:   Add new entry to the qzquote_type table
//
// [PARAMETERS]:    $quoteName=""
//
// [RETURN VALUE]:  quoteID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteType($quoteName="")
{

   if(! $this->AssertQuoteType($quoteName))
      return 0;

   // check if ID exists in DB
   $sqlCmd = "SELECT id FROM ".SQL_QZQUOTE_TYPES." WHERE name='$quoteName'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if($this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QZQUOTE_TYPE_ALREADY_EXISTS");
      return 0;
   }

   $sqlCmd = "INSERT INTO ".SQL_QZQUOTE_TYPES." (name) VALUES ('$quoteName')";
   // echo "$sqlCmd <br>";
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $ID = $this->dbh->GetFieldValue("id");

   return $ID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQuoteType
//
// [DESCRIPTION]:   Update qzquote_type table
//
// [PARAMETERS]:    $quoteTypeID=0, $quoteName=""
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQuoteType($quoteTypeID=0, $quoteName="")
{
//print "## $quoteTypeID, $quoteName";

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTETYPEID_FIELD");
      return false;
   }

   if(! $this->AssertQuoteType($quoteTypeID, $quoteName))
      return false;

   // check if ID exists in DB
   $sqlCmd = "SELECT name FROM ".SQL_QZQUOTE_TYPES." WHERE id=$quoteTypeID";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTETYPEID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $dbQuoteName = $this->dbh->GetFieldValue("name");

   if($quoteName != $dbQuoteName)
   {
      // check if name exists in DB
      $sqlCmd = "SELECT id FROM ".SQL_QZQUOTE_TYPES." WHERE name='$quoteName'";

      if(! $this->dbh->Exec($sqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if($this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("QUOTE_TYPE_ALREADY_EXISTS");
         return false;
      }
   }

   $sqlCmd = "UPDATE ".SQL_QZQUOTE_TYPES." SET id='$quoteTypeID', name='$quoteName' WHERE id='$quoteTypeID'";

   //echo $sqlCmd ;
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQuoteType
//
// [DESCRIPTION]:   Delete an entry from qzquote_type table
//
// [PARAMETERS]:    $quoteTypeID = 0
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteQuoteType($quoteTypeID = 0)
{
   if(! preg_match("/^.{1,16}$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTETYPEID_FIELD");
      return false;
   }

   // check if ID exists in DB
   $sqlCmd = "SELECT id FROM ".SQL_QZQUOTE_TYPES." WHERE id=$quoteTypeID";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTETYPEID_NOT_FOUND");
      return false;
   }

   $sqlCmd = "DELETE FROM ".SQL_QZQUOTE_TYPES." WHERE id=$quoteTypeID";

   //print "$sqlCmd";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteTypeByID
//
// [DESCRIPTION]:   Read data from qzquote_type table and put it into an array variable
//
// [PARAMETERS]:    $quoteTypeID = 0, &$arrayResult
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteTypeByID($quoteTypeID = 0 )
{
   if(! preg_match("/^.{1,16}$/", $quoteTypeID))
   {
      print_r ($quoteTypeID);
      $this->strERR = GetErrorString("INVALID_QUOTETYPEID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT * FROM ".SQL_QZQUOTE_TYPES." WHERE id='$quoteTypeID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTETYPEID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]   = $this->dbh->GetFieldValue("id");
   $arrayResult["name"] = $this->dbh->GetFieldValue("name");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteTypeByName
//
// [DESCRIPTION]:   Read data from qzquote_type table and put it into an array variable
//
// [PARAMETERS]:    $quoteName = "", &$arrayResult
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteTypeByName($quoteName = "")
{
   if(! preg_match("/^.{1,16}$/", $quoteName))
   {
      $this->strERR = GetErrorString("INVALID_QUOTETYPEID_FIELD");
      return false;
   }

   // check if we have this  name
   $sqlCmd = "SELECT id FROM ".SQL_QZQUOTE_TYPES." WHERE name=$quoteName";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERID_NOT_FOUND");
      return false;
   }

   $sqlCmd = "SELECT * FROM ".SQL_QZQUOTE_TYPES." WHERE name=$quoteName ORDER BY name ASC";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();
   while($this->dbh->MoveNext())
      $arrayResult[$id] = $this->dbh->GetFieldValue("name");

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuoteTypes
//
// [DESCRIPTION]:   Read data from qzquote_type table and put it into an array variable
//
// [PARAMETERS]:    $arrayResult
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuoteTypes()
{
   $sqlCmd = "SELECT * FROM ".SQL_QZQUOTE_TYPES." ORDER BY id ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTETYPEIDS_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("name");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuoteType
//
// [DESCRIPTION]:   Check if all fields are OK and set the error string if needed
//
// [PARAMETERS]     $name
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertQuoteType($quoteName)
{
   $this->strERR = "";

   if(! preg_match("/^.{1,16}$/", $quoteName))
      $this->strERR .= GetErrorString("INVALID_NAME_FIELD")."\n";

   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteTypeIDByPath()
{
   preg_match("/\w+$/", getcwd(), $matches);

   $typeName = $matches[0];

   $sqlCmd = "SELECT id FROM ".SQL_QZQUOTE_TYPES." WHERE name='$typeName'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTETYPEIDS_NOT_FOUND");
      return false;
   }

   return $this->dbh->GetFieldValue("id");

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

} // end of CQuoteType class
?>

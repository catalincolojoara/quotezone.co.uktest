<?php
/*****************************************************************************/
/*                                                                           */
/*  EmailOutboundsEngineTaxi class interface                                 */
/*                                                                           */
/*  (C) 2009 Alexandru FURTUNA (alexandru.furtuna@seopa.com)                              */
/*                                                                           */
/*****************************************************************************/

include_once "errors.inc";
include_once "MySQL.php";
include_once "EmailStore.php";
include_once "EmailReminder.php";
include_once "EmailStoreCodes.php";
include_once "QuoteDetails.php";
include_once "Session.php";
include_once "EmailOutboundsEngine.php";
include_once "EmailOutbounds.php";
include_once "/home/www/quotezone.co.uk/taxi-insurance/taxi/steps/YourDetailsElements.php";

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CEmailOutboundsEngineTaxi
//
// [DESCRIPTION]:  CEmailOutboundsEngineTaxi class interface, email outbounds support,
//                 PHP version
//
// [FUNCTIONS]:    FormatUserDetails($Session)
//                 CheckEmailOutboundResponse()
//                 SendUserDetails()
//                 ShowError()
//                 GetError()
//
//
//  (C) 2009 Alexandru FURTUNA (alexandru.furtuna@seopa.com) 2009-06-12
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CEmailOutboundsEngineTaxi Extends CEmailOutboundsEngine
{
    var $fileName;           // the fileName
    var $strERR;             // last error string
    var $systemType;         // system type(car,van,home,bike)
    var $quoteTypeID;        // quote type id (1-car , 2-van, 4-home, 5-bike)
    var $objEmailStore;      // email store id field
    var $objEmailStoreCodes; // EmailStoreCodes.php class object
    var $objEmailReminder;   // EmailReminder.php class object
    var $objQuoteDetails;    // QuoteDetails.php class object
    var $objOccupation;      // Occupation.php class ojject
    var $objBusiness;        // Business.php class object
    var $objSession;         // Session.php class object
    var $objEmailOutbounds;  //email outbound object
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CEmailOutboundsEngineTaxi
   //
   // [DESCRIPTION]:   Default class constructor. Initialization goes here.
   //
   // [PARAMETERS]:    $fileName, $dbh
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Alexandru FURTUNA (alexandru.furtuna@seopa.com) 2009-06-12
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CEmailOutboundsEngineTaxi($fileName, $dbh)
   {
      CEmailOutboundsEngine::CEmailOutboundsEngine('taxi','in');

      $this->fileName = $fileName;

      $this->systemType  = 'taxi';
      $this->quoteTypeID = '18';

      $this->host = "trc.emv2.com";
      $this->path = "/D2UTF8";

      $this->strERR  = "";

      if($dbh)
      {
         $this->dbh = $dbh;
         $this->closeDB = false;
      }
      else
      {
         // default configuration
         $this->dbh = new CMySQL();

         if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
         {
           $this->strERR = $this->dbh->GetError();
           return;
         }
      }

      $this->closeDB = true;

      $this->objQuoteDetails    = new CQuoteDetails($this->dbh);
      $this->objEmailStore      = new CEmailStore($this->dbh);
      $this->objEmailStoreCodes = new CEmailStoreCodes($this->dbh);
      $this->objEmailReminder   = new CEmailReminder($this->dbh);
      $this->objSession         = new CSession($this->dbh);
      $this->objEmailOutbounds  = new CEmailOutbounds($this->dbh);
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: FormatUserDetails
   //
   // [DESCRIPTION]:   Form user data details to be sent it
   //
   // [PARAMETERS]:    $Session
   //
   // [RETURN VALUE]:  formated $string or false if failure
   //
   // [CREATED BY]:    Alexandru FURTUNA (alexandru.furtuna@seopa.com) 2009-06-12
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function FormatUserDetails($Session)
   {
      global $_YourDetails;

      $fileName = $this->fileName;

      $logID          = $Session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];
      $emailAddress   = $Session['_YourDetails_']['email_address'];
      $quoteUserID    = $Session["_QZ_QUOTE_DETAILS_"]["quote_user_id"];

      $cheappestSiteId         = "0";
      $cheapeastQuotePrice     = "0";

      $PostArray = array();

      $date = date("Y-m-d");

      if(! $emStoreID = $this->objEmailStore->GetEmailStoreIDByEmailAndQuoteUserId($emailAddress,$quoteUserID))
      {
         //print "CANNOT_GET_THE_EMAIL_STORE_ID \n\n";
      }
      while (1)
      {
         $authInfo = $this->GenerateMimeBoundary();
         $authInfo = str_replace("----=_NextPart_","",$authInfo);
         $authInfo = substr($authInfo, 0, 16);

         if(! $this->objEmailReminder->GetEmRemByAuthInfo($authInfo, $array))
            break;
      }

      if(! $this->objEmailReminder->AddEmailRem($quoteUserID,$emStoreID,"1",$date,"","1", $authInfo,'1','1','0'))
      {
         //print "Cannot add email reminder\n";
      }

      //$PostArray["RENEW_LINK_FIELD"]          = $link;

      $PostArray["emv_tag"]                   = "DFB965A0200002DD";
      $PostArray["emv_ref"]                   = "EdX7CqkmjHhj8SA9MKJPUB6jXj5zGqiy-jrde6k2WrStK6o";

      $PostArray["TITLE_FIELD"]               = $Session['_YourDetails_']['title'];
      $PostArray["FIRSTNAME_FIELD"]           = $Session['_YourDetails_']['first_name'];
      $PostArray["LASTNAME_FIELD"]            = $Session['_YourDetails_']['surname'];
      $PostArray["DATEOFBIRTH_FIELD"]       = $Session['_YourDetails_']['date_of_birth_mm']."/".$Session['_YourDetails_']['date_of_birth_dd']."/".$Session['_YourDetails_']['date_of_birth_yyyy'];
      $PostArray["MOBILE_NUMBER_FIELD"]       = $Session['_YourDetails_']['mobile_telephone_prefix']."".$Session['_YourDetails_']['mobile_telephone_number'];
      //$PostArray["DAYTIME_NUMBER_FIELD"]      = $Session['_YourDetails_']['daytime_telephone_prefix']."".$Session['_YourDetails_']['daytime_telephone_number'];
      $PostArray["EMAIL_FIELD"]               = $emailAddress;
 
      $PostArray["VEHICLE_MAKE_FIELD"]        = $Session['_YourDetails_']['taxi_make'];
      $PostArray["VEHICLE_MODEL_FIELD"]       = $Session['_YourDetails_']['taxi_model'];
      $PostArray["ESTIMATED_VALUE_FIELD"]     = $Session['_YourDetails_']['estimated_value'];
      
      $taxiUsedFor = $Session['_YourDetails_']['taxi_used_for'];
      $PostArray["TYPE_OF_USE_FIELD"]         = $_YourDetails["taxi_used_for"]["$taxiUsedFor"];

      //$taxiCapacity = $Session['_YourDetails_']['taxi_capacity'];
      //$PostArray["TAXI_CAPACITY_FIELD"]        = $_YourDetails["taxi_capacity"]["$taxiCapacity"];


      $PostArray["YEAR_NCB_FIELD"]             = $Session['_YourDetails_']['taxi_ncb'];
      $PostArray["POSTCODE_FIELD"]             = $Session['_YourDetails_']['postcode'];
      $PostArray["HOUSE_NUMBER_FIELD"]         = $Session['_YourDetails_']['house_number_or_name'];
      $PostArray["STREET_FIELD"]               = $Session['_YourDetails_']['address_line2'];
      $PostArray["TOWN_CITY_FIELD"]            = $Session['_YourDetails_']['address_line3'];
      $PostArray["COUNTY_FIELD"]               = $Session['_YourDetails_']['address_line4'];

      $PostArray["INSURANCE_START_DATE_FIELD"] = $Session['_YourDetails_']['date_of_insurance_start_mm']."/".$Session['_YourDetails_']['date_of_insurance_start_dd']."/".$Session['_YourDetails_']['date_of_insurance_start_yyyy'];

      $PostArray["QUOTE_DATE_FIELD"]            = date('m')."/".date('d')."/".date('Y');

      $sid = str_replace("_","",$fileName);

      $taxiType = $Session['_YourDetails_']['taxi_type'];

      $taxyTypeField = "Taxi ".$_YourDetails['taxi_type']["$taxiType"];
      $taxyTypeField = strtolower($taxyTypeField);
      $PostArray["QUOTE_TYPE_FIELD"]               = $taxyTypeField;


      $PostArray["QUOTE_RESULTS_LINK_FIELD"] = "http://taxi-insurance.quotezone.co.uk/taxi/index.php?sid=$sid";
      $PostArray["EMVADMIN1_FIELD"]          = $sid;

      $unsubscribeLink = $this->CreateUnsubscribeLinkParams($emailAddress,'top3','taxi')."&sid=$sid&type=1&action=unsubscribe";
      $PostArray["UNSUBSCRIBE_LINK_FIELD"]          = "http://taxi-insurance.quotezone.co.uk/customer/unsubscribe.php?$unsubscribeLink";

      $PostArray["QUOTE_TIME_FIELD"]                = date("Gis");
      if(! $cheapeastQuotePrice)
         $cheapeastQuotePrice = "0";
      $PostArray["PREMIUM_FIELD"]                   = $cheapeastQuotePrice;
      $PostArray["QUOTE_COMPLETED_FIELD"]           = "Yes";
      $PostArray["QUOTE_REF_FIELD"]                 = $Session["_QZ_QUOTE_DETAILS_"]["quote_reference"];

      //print "<!-- <pre>";
      //print_r($PostArray);
      //print "</pre> -->";

      $string = "";

      foreach($PostArray as $key=>$val)
      {
         $string .= $key."=".urlencode($val)."&";
      }

      $string = rtrim($string, "&");

      //print "String : \n\n $string \n\n";

      return $string;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: SendUserDetails
   //
   // [DESCRIPTION]:   Form user data details to be sent it
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Alexandru FURTUNA (alexandru.furtuna@seopa.com) 2009-06-12
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SendUserDetails()
   {
      $fh = fopen("/home/www/quotezone.co.uk/taxi-insurance/taxi/TaxiEmailOutbounds.log","a+");
      fwrite($fh, "Load Session for file: ".$this->fileName."...\n");

      if(! $Session = $this->LoadSessionFile($this->fileName))
      {
         fwrite($fh, "FAILURE load session: $this->fileName ...\n");
         fclose($fh);
         $this->strERR = GetErrorString('CANT_GET_FILENAME_CONTENT');
         return false;
      }

      $logID       = $Session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];
      $wlUserID    = $Session["_QZ_QUOTE_DETAILS_"]["wlUserID"];
      $emailAddr   = $Session["_YourDetails_"]["email_address"];
      $quoteUserID = $Session["_QZ_QUOTE_DETAILS_"]["quote_user_id"];

      if($wlUserID == "136")
         return;
      
      if(! $this->LogWasSentOk($logID))
      {
         fwrite($fh, "POST ALLREADY SENT [$logID] file [$this->fileName]\n");
         fclose($fh);
         return false;
      }
         

      //Ticket id : XEV-793421
      /*
      $emailRule = $this->objEmailStore->GetEmailStoreRuleByEmailAndQuoteUserID($emailAddr,$quoteUserID);
      
      if($emailRule != '65535')
      {
         fwrite($fh, "Email address [$emailAddr] has rule [$emailRule] for file [$this->fileName] and log_id [$logID]\n");
         fwrite($fh, "Details wont be posted because the user has unsubscribed\n");
         fclose($fh);
         return false;
      }
      */

//      if(! $this->isNotWaitingQuotes($logID))
//      {
//         fwrite($fh, "QUOTE STILL WAITING: $this->fileName logID=$logID \n");
//         fclose($fh);
//         return false;
//      }

      if(! $this->objEmailOutbounds->AddEmailOutbounds($logID))
      {
         fwrite($fh, "CANNOT ADD  log_id [$logID] to database\n");
         fclose($fh);
         return false;
      }

      if(! $string = $this->FormatUserDetails($Session))
      {
         fwrite($fh, "FAILURE Formating user details for: $this->fileName logID=$logID \n");
         fclose($fh);
         return false;
      }

      fwrite($fh, "\n String : \n".$string."\n");
      fwrite($fh, "Strating send process for : $this->fileName logID=$logID \n");


      if(! $response = $this->EmailOutboundsEngineSend($this->path, $string))
      {
         fwrite($fh, "FAILURE Send process for : $this->fileName logID=$logID \n");
         fclose($fh);
         return false;
      }

      if(! $this->objEmailOutbounds->SetEmailOutboundsStatus($logID, "1"))
      {
         fwrite($fh, "FAILURE Cannot set the email status :".$this->fileName." logID=$logID \n");
         fclose($fh);
         return false;
      }

      if(! $this->CheckEmailOutboundResponse($response))
      {
         fwrite($fh, "The post response was an error - please check the details to repost\n");
         fclose($fh);
         return false;
      }

      fwrite($fh, $response);
      fwrite($fh, "Done \n");
      fclose($fh);

      return true;

   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CheckEmailOutboundResponse
   //
   // [DESCRIPTION]:   checks the response from the server
   //
   // [PARAMETERS]:    $response=""
   //
   // [RETURN VALUE]:  true or false in case of failure
   //
   // [CREATED BY]:    Alexandru FURTUNA (alexandru.furtuna@seopa.com) 2009-06-12
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CheckEmailOutboundResponse($response="")
   {

      if(empty($response))
      {
         $this->strERR = GetErrorString("INVALID_RESPONSE_FROM_EMAILVISION");
            return false;
      }

      //if(! preg_match("/http\:\/\/www\.quotezone\.co\.uk\/emailpostsuccesstaxi\.htm/isU",$response))
      if(! preg_match("/emailpostsuccesstaxi\.htm/isU",$response))
      {
         $this->strERR = GetErrorString("INVALID_RESPONSE_PREGMATCH");
            return false;
      }

      return true;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetError
   //
   // [DESCRIPTION]:   Retrieve the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Alexandru FURTUNA (alexandru.furtuna@seopa.com) 2009-06-12
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetError()
   {
       return $this->strERR;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ShowError
   //
   // [DESCRIPTION]:   Print the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Alexandru FURTUNA (alexandru.furtuna@seopa.com) 2009-06-12
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function ShowError()
   {
       return $this->strERR;
   }
}



?>

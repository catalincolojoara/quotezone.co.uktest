<?php

/*****************************************************************************/
/*                                                                           */
/*  CSkipQuoteMailInfo class interface                                       */
/*                                                                           */
/*  (C) 2006 Eugen Savin (null@seopa.com)                			           */
/*                                                                           */
/*****************************************************************************/

include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
error_reporting(E_ALL);
else
error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CSkipQuoteMailInfo
//
// [DESCRIPTION]:  CSkipQuoteMailInfo class interface
//
// [FUNCTIONS]:    int  AddSkipQuoteMailInfo($email_store_id=0)
//                 bool UpdateSkipQuoteMailInfo($userID=0, $email_store_id=0)
//                 bool DeleteSkipQuoteMailInfo($SkipQuoteMailInfoID = 0)
//                 array|false GetSkipQuoteMailIDByEmailStoreID($emailStoreID = 0)
//                 array|false GetSkipQuoteMailInfoByID($SkipQuoteMailInfoID = 0)
//                 array|false GetSkipQuoteMailInfoByEmailStoreID($SkipQuoteMailInfoID = 0)
//                 array GetAllSkipQuoteMailInfo();
//                 bool AssertSkipQuoteMailInfo($SkipQuoteMailInfoID, $email_store_id);
//
//                 void Close();
//                 string GetError();
//
// [CREATED BY]:   Eugen SAVIN (null@seopa.com) 2006-02-14
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CSkipQuoteMailInfo
{
	// database handler
	var $dbh;         // database server handle
	var $closeDB;     // close database flag

	// class-internal variables
	var $strERR;      // last USER error string

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: CSkipQuoteMailInfo
	//
	// [DESCRIPTION]:   Default class constructor. Initialization goes here.
	//
	// [PARAMETERS]:    none
	//
	// [RETURN VALUE]:  none
	//
	// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2006-02-14
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function CSkipQuoteMailInfo($dbh=0)
	{
		if($dbh)
		{
			$this->dbh = $dbh;
			$this->closeDB = false;
		}
		else
		{
			$this->dbh = new CMySQL();

			if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
			{
				$this->strERR = $this->dbh->GetError();
				return;
			}

			$this->closeDB = true;
		}

		$this->strERR  = "";
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: AddSkipQuoteMailInfo
	//
	// [DESCRIPTION]:   Add new entry to the SkipQuoteMailInfo table
	//
	// [PARAMETERS]:    $email_store_id=0
	//
	// [RETURN VALUE]:  SkipQuoteMailInfoID or 0 in case of failure
	//
	// [CREATED BY]:    Eugen SAVIN (null@seopa.com) 2006-02-14
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function AddSkipQuoteMailInfo($email_store_id=0)
	{
		if(! preg_match("/^\d+$/", $email_store_id))
		{
			$this->strERR = GetErrorString("INVALID_EMAIL_ID_FIELD");
			return 0;
		}

		$this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_EMAIL_INFO." where email_store_id = '$email_store_id'";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if( $this->dbh->GetRows() > 0)
		{
			$this->strERR = GetErrorString("EMAIL_ID_ALLREADY_ENTERED");
			return false;
		}


		$this->lastSQLCMD = "INSERT INTO ".SQL_QUOTE_EMAIL_INFO." (email_store_id) VALUES ('$email_store_id')";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return 0;
		}

		$this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

		if(! $this->dbh->Exec($this->lastSQLCMD))
		{
			$this->strERR = $this->dbh->GetError();
			return 0;
		}

		if(! $this->dbh->FetchRows())
		{
			$this->strERR = $this->dbh->GetError();
			return 0;
		}

		return $this->dbh->GetFieldValue("id");
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: UpdateSkipQuoteMailInfo
	//
	// [DESCRIPTION]:   Update SkipQuoteMailInfo table
	//
	// [PARAMETERS]:    $userID=0, $email_store_id=0
	//
	// [RETURN VALUE]:  true|false
	//
	// [CREATED BY]:    Eugen SAVIN (null@seopa.com) 2006-02-14
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function UpdateSkipQuoteMailInfo($userID=0, $email_store_id=0)
	{
		if(! preg_match("/^\d+$/", $SkipQuoteMailInfoID))
		{
			$this->strERR = GetErrorString("INVALID_SKIP_QUOTE_MAIL_INFO_ID_FIELD");
			return false;
		}

		if(! $this->AssertSkipQuoteMailInfo($userID, $email_store_id))
		return false;

		// check if SkipQuoteMailInfoID exists in DB
		$sqlCmd = "SELECT id, email_store_id FROM ".SQL_QUOTE_EMAIL_INFO." WHERE id=$SkipQuoteMailInfoID";

		if(! $this->dbh->Exec($sqlCmd))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if(! $this->dbh->GetRows())
		{
			$this->strERR = GetErrorString("SkipQuoteMailInfoID_NOT_FOUND");
			return false;
		}

		if(! $this->dbh->FetchRows())
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		$dbUserID = $this->dbh->GetFieldValue("id");

		// check if we have this new user ID
		if($userID != $dbUserID)
		{
			$sqlCmd = "SELECT id FROM ".SQL_QUOTE_EMAIL_INFO." WHERE id=$userID";

			if(! $this->dbh->Exec($sqlCmd))
			{
				$this->strERR = $this->dbh->GetError();
				return false;
			}

			if(! $this->dbh->GetRows())
			{
				$this->strERR = GetErrorString("USERID_NOT_FOUND");
				return false;
			}
		}

		$sqlCmd = "UPDATE ".SQL_QUOTE_EMAIL_INFO." SET email_store_id=$userID, WHERE id=$SkipQuoteMailInfoID";

		if(! $this->dbh->Exec($sqlCmd))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: DeleteSkipQuoteMailInfo
	//
	// [DESCRIPTION]:   Delete an entry from SkipQuoteMailInfo table
	//
	// [PARAMETERS]:    $SkipQuoteMailInfoID = 0
	//
	// [RETURN VALUE]:  true|false
	//
	// [CREATED BY]:    Eugen SAVIN (null@seopa.com) 2006-02-14
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function DeleteSkipQuoteMailInfo($SkipQuoteMailInfoID = 0)
	{
		if(! preg_match("/^\d+$/", $SkipQuoteMailInfoID))
		{
			$this->strERR = GetErrorString("INVALID_SKIP_QUOTE_MAIL_INFO_ID_FIELD");
			return false;
		}

		// check if SkipQuoteMailInfoID exists in DB
		$sqlCmd = "SELECT id FROM ".SQL_QUOTE_EMAIL_INFO." WHERE id=$SkipQuoteMailInfoID";

		if(! $this->dbh->Exec($sqlCmd))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if(! $this->dbh->GetRows())
		{
			$this->strERR = GetErrorString("SkipQuoteMailInfoID_NOT_FOUND");
			return false;
		}

		$sqlCmd = "DELETE FROM ".SQL_QUOTE_EMAIL_INFO." WHERE id=$SkipQuoteMailInfoID";

		if(! $this->dbh->Exec($sqlCmd))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: GetSkipQuoteMailIDByEmailStoreID
	//
	// [DESCRIPTION]:   Read data from skip_quote_mail_info table and put it into an array variable
	//
	// [PARAMETERS]:    $emailStoreID = 0
	//
	// [RETURN VALUE]:  array|false
	//
	// [CREATED BY]:    Eugen SAVIN (null@seopa.com) 2006-02-14
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function GetSkipQuoteMailIDByEmailStoreID($emailStoreID = 0)
	{
		if(! preg_match("/^\d+$/", $emailStoreID))
		{
			$this->strERR = GetErrorString("INVALID_SKIP_QUOTE_MAIL_INFO_ID_FIELD");
			return false;
		}

		$sqlCmd = "SELECT * FROM ".SQL_QUOTE_EMAIL_INFO." WHERE email_store_id=$emailStoreID";

		if(! $this->dbh->Exec($sqlCmd))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if(! $this->dbh->GetRows())
		{
			$this->strERR = GetErrorString("SKIP_MAIL_ID_NOT_FOUND");
			return false;
		}

		if(! $this->dbh->FetchRows())
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		$arrayResult = array();

		$arrayResult["id"] = $this->dbh->GetFieldValue("id");

		return $arrayResult["id"] ;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: GetSkipQuoteMailInfoByID
	//
	// [DESCRIPTION]:   Read data from skip_quote_mail_info table and put it into an array variable
	//
	// [PARAMETERS]:    $SkipQuoteMailInfoID = 0
	//
	// [RETURN VALUE]:  array|false
	//
	// [CREATED BY]:    Eugen SAVIN (null@seopa.com) 2006-02-14
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function GetSkipQuoteMailInfoByID($SkipQuoteMailInfoID = 0)
	{
		if(! preg_match("/^\d+$/", $SkipQuoteMailInfoID))
		{
			$this->strERR = GetErrorString("INVALID_SKIP_QUOTE_MAIL_INFO_ID_FIELD");
			return false;
		}

		$sqlCmd = "SELECT * FROM ".SQL_QUOTE_EMAIL_INFO." WHERE id=$SkipQuoteMailInfoID";

		if(! $this->dbh->Exec($sqlCmd))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if(! $this->dbh->GetRows())
		{
			$this->strERR = GetErrorString("SkipQuoteMailInfoID_NOT_FOUND");
			return false;
		}

		if(! $this->dbh->FetchRows())
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		$arrayResult = array();

		$arrayResult["id"]     			 = $this->dbh->GetFieldValue("id");
		$arrayResult["email_store_id"] = $this->dbh->GetFieldValue("email_store_id");

		return $arrayResult;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: GetSkipQuoteMailInfoByEmailStoreID
	//
	// [DESCRIPTION]:   Read data from skip_quote_mail_info table and put it into an array variable
	//
	// [PARAMETERS]:    $SkipQuoteMailInfoID = 0
	//
	// [RETURN VALUE]:  array|false
	//
	// [CREATED BY]:    Eugen SAVIN (null@seopa.com) 2006-02-14
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function GetSkipQuoteMailInfoByEmailStoreID($SkipQuoteMailInfoID = 0)
	{
		if(! preg_match("/^\d+$/", $SkipQuoteMailInfoID))
		{
			$this->strERR = GetErrorString("INVALID_SKIP_QUOTE_MAIL_INFO_ID_FIELD");
			return false;
		}

		$sqlCmd = "SELECT * FROM ".SQL_QUOTE_EMAIL_INFO." WHERE email_store_id=$SkipQuoteMailInfoID";

		if(! $this->dbh->Exec($sqlCmd))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if(! $this->dbh->GetRows())
		{
			$this->strERR = GetErrorString("SkipQuoteMailInfoID_NOT_FOUND");
			return false;
		}

		if(! $this->dbh->FetchRows())
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		$arrayResult = array();

		$arrayResult["id"]     			 = $this->dbh->GetFieldValue("id");
		$arrayResult["email_store_id"] = $this->dbh->GetFieldValue("email_store_id");

		return $arrayResult;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: GetAllSkipQuoteMailInfo()
	//
	// [DESCRIPTION]:   Read data from skip_quote_mail_info table and put it into an array variable
	//
	// [PARAMETERS]:
	//
	// [RETURN VALUE]:  array|false
	//
	// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2006-02-14
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function GetAllSkipQuoteMailInfo()
	{
		$sqlCmd = "SELECT * FROM ".SQL_QUOTE_EMAIL_INFO." ";

		$sqlCmd .= " ORDER BY id";

		if(! $this->dbh->Exec($sqlCmd))
		{
			$this->strERR = $this->dbh->GetError();
			return false;
		}

		if(! $this->dbh->GetRows())
		{
			$this->strERR = GetErrorString("EMAIL_STORE_ID_NOT_FOUND");
			return false;
		}

		$arrayResult = array();
		while($this->dbh->MoveNext())
		{
			$id = $this->dbh->GetFieldValue("email_store_id");

			$arrayResult[$id]["id"] 				= $this->dbh->GetFieldValue("id");
			$arrayResult[$id]["email_store_id"] = $this->dbh->GetFieldValue("email_store_id");
		}

		return $arrayResult;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: AssertSkipQuoteMailInfo
	//
	// [DESCRIPTION]:   Check if all fields are OK and set the error string if needed
	//
	// [PARAMETERS]     $SkipQuoteMailInfoID, $email_store_id
	//
	// [RETURN VALUE]:  true|false
	//
	// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2006-02-14
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function AssertSkipQuoteMailInfo($SkipQuoteMailInfoID, $email_store_id)
	{
		$this->strERR = "";

		if(! preg_match("/^\d+$/", $SkipQuoteMailInfoID))
		$this->strERR .= GetErrorString("INVALID_ID_FIELD")."\n";

		if(! preg_match("/^\d+$/", $email_store_id))
		$this->strERR .= GetErrorString("INVALID_EMAIL_STORE_ID_FIELD")."\n";

	   if(! empty($this->strERR))
		   return false;

		return true;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: Close
	//
	// [DESCRIPTION]:   Close the object and also close the connection with the
	//                  database server if necessary
	//
	// [PARAMETERS]:    none
	//
	// [RETURN VALUE]:  none
	//
	// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2006-02-14
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function Close()
	{
		if($this->closeDB)
		$this->dbh->Close();

		return;
	}

	//////////////////////////////////////////////////////////////////////////////FB
	//
	// [FUNCTION NAME]: GetError
	//
	// [DESCRIPTION]:   Retrieve the last error message
	//
	// [PARAMETERS]:    none
	//
	// [RETURN VALUE]:  Error string if there is any, empty string otherwise
	//
	// [CREATED BY]:    Eugen SAVIN (null@seopa.com) 2006-02-14
	//
	// [MODIFIED]:      - [programmer (email) date]
	//                    [short description]
	//////////////////////////////////////////////////////////////////////////////FE
	function GetError()
	{
		return $this->strERR;
	}

} // end of CSkipQuoteMailInfo class

?>
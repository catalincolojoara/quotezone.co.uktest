<?php
/*****************************************************************************/
/*                                                                           */
/*  COvertureTracker class interface                                                 */
/*                                                                           */
/*  (C) 2005 Ciciu Gheorghe (null@seopa.com)                                   */
/*                                                                           */
/*****************************************************************************/


include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);



//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   COvertureTracker
//
// [DESCRIPTION]:  COvertureTracker class interface
//
// [FUNCTIONS]:    bool AssertOvertureTraker($top=0,$additionalInfo='',$hostIP='',$crDate=0)
//                 int  AddOvertureTracker($top=0,$additionalInfo='',$hostIP='',$crDate=0)
//                 bool UpdateOvertureTracker($overtureTrackerID=0,$top=0,$additionalInfo='',$hostIP='',$crDate=0)
//                 bool DeleteOvertureTracker($overtureTrackerID=0)
//                 bool GetAllOvertureTracker()
//                 bool GetOvertureTop($overtureTrackerID=0)
//                 bool GetOvertureTrackerByTop($top="")
//                 bool GetOvertureTrackerByHostIP($hostIP="")
//                 bool GetOvertureTrackerByDate($crDate=0)
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Ciciu Gheorghe (null@seopa.com) 08-05-2006
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class COvertureTracker
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: COvertureTracker
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Ciciu Gheorghe (null@seopa.com) 08-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function COvertureTracker($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertOverureTraker
//
// [DESCRIPTION]:   Verify to see if the params are completed
//
// [PARAMETERS]:    $top=0,$additionalInfo='',$hostIP='',$crDate=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Ciciu Gheorghe (null@seopa.com) 08-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertOvertureTraker($logID, $quoteTypeID, $top,$qzTop, $additionalInfo,$hostIP,$crDate)
{
   $this->strERR = "";

   if(! preg_match("/^\d+$/", $logID))
      $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");

   if(! preg_match("/^\d+$/", $quoteTypeID))
      $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");

   if(! preg_match("/^\d+$/", $top))
      $this->strERR = GetErrorString("INVALID_TOP_FIELD");

   if(! preg_match("/^\d+$/", $qzTop))
      $this->strERR = GetErrorString("INVALID_QZ_TOP_FIELD");

   if(! preg_match("/^\d+$/", $crDate))
      $this->strERR = GetErrorString("INVALID_CR_DATE_FIELD");

//    if(empty($additionalInfo))
//       $this->strERR = GetErrorString("INVALID_ADDITIONAL_INFO_FIELD");

   if(! preg_match("/((1|2)?\d?\d)\.((1|2)?\d?\d)\.((1|2)?\d?\d)\.((1|2)?\d?\d)/", $hostIP))
      $this->strERR = GetErrorString("INVALID_HOST_IP_FIELD");

    if($this->strERR != "")
      return true;

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddOvertureTracker
//
// [DESCRIPTION]:   Add new entry to the overture_tracker table
//
// [PARAMETERS]:    $top=0, $additionalInfo='', $hostIP='', $crDate=''
//
// [RETURN VALUE]:  trakerID or 0 in case of failure
//
// [CREATED BY]:    Ciciu Gheorghe (null@seopa.com) 08-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddOvertureTracker($logID=0, $quoteTypeID=0, $top=0, $qzTop=0, $additionalInfo='',$hostIP='',$crDate=0, $popupPos='RIGHT', $impressionPos='-')
{
   if(! $crDate)
      $crDate = time();
	
   if($this->AssertOvertureTraker($logID, $quoteTypeID, $top, $qzTop, $additionalInfo,$hostIP,$crDate))
      return 0;

   $this->lastSQLCMD = "INSERT INTO ". SQL_OVERTURE_TRACKINGS."(log_id, quote_type_id, top, qztop, additional_info, host_ip, cr_date, popup_position,impression_position) VALUES('$logID','$quoteTypeID','$top', '$qzTop', '$additionalInfo', '$hostIP', '$crDate', '$popupPos','$impressionPos')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateOvertureTracker
//
// [DESCRIPTION]:   Modify an entry to the overture_tracker table
//
// [PARAMETERS]:    $overtureTrackerID=0,$top=0,$additionalInfo='',$hostIP='',$crDate=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Ciciu Gheorghe (null@seopa.com) 08-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateOvertureTracker($overtureTrackerID=0,$top=0,$additionalInfo='',$hostIP='',$crDate=0)
{
   if(! preg_match("/^\d+$/", $overtureTrackerID))
   {
      $this->strERR = GetErrorString("INVALID_TRACKER_ID_FIELD");
      return false;
   }

   if(! $crDate)
      $crDate = time();

   if($this->AssertOvertureTraker($top,$additionalInfo,$hostIP,$crDate))
      return false;

   $this->lastSQLCMD = "UPDATE ".SQL_OVERTURE_TRACKINGS." SET top='$top', additional_info='$additionalInfo', host_ip='$hostIP', cr_date='$crDate' WHERE id='$overtureTrackerID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteOvertureTracker
//
// [DESCRIPTION]:   Delete an entry from overture_tracker table by ID
//
// [PARAMETERS]:    $overtureTrackerID=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Ciciu Gheorghe (null@seopa.com) 08-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteOvertureTracker($overtureTrackerID=0)
{
   if(! preg_match("/^\d+$/", $overtureTrackerID))
   {
      $this->strERR = GetErrorString("INVALID_OVERTURE_TRACKER_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_OVERTURE_TRACKINGS." WHERE id='$overtureTrackerID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("OVERTURE_TRACKER_ID_FIELD_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_OVERTURE_TRACKINGS." WHERE id='$overtureTrackerID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteOvertureTracker
//
// [DESCRIPTION]:   Delete an entry from overture_tracker table by ID
//
// [PARAMETERS]:    $overtureTrackerID=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Ciciu Gheorghe (null@seopa.com) 08-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteOvertureTrackerByLogIDAndPopupPosition($logID=0, $popupPos='',$impressionPos='')
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_OVERTURE_TRACKER_LOG_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_OVERTURE_TRACKINGS." WHERE log_id='$logID' AND popup_position='$popupPos' and impression_position='$impressionPos' ";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllOvertureTracker
//
// [DESCRIPTION]:   Get all content of the table overture_tracker
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Ciciu Gheorghe (null@seopa.com) 08-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllOvertureTracker()
{
   $this->lastSQLCMD = "SELECT * FROM ".SQL_OVERTURE_TRACKINGS;

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("OVERTURE_TRACKER_NOT_FOUND");
      return false;
   }
   
   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $id = $this->dbh->GetFieldValue("id");

      $arrayResult[$id]["id"]               = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["top"]    	    = $this->dbh->GetFieldValue("top");
      $arrayResult[$id]["qztop"]    	    = $this->dbh->GetFieldValue("qztop");
      $arrayResult[$id]["additional_info"]  = $this->dbh->GetFieldValue("additional_info");
      $arrayResult[$id]["host_ip"]          = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$id]["cr_date"]          = $this->dbh->GetFieldValue("cr_date");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetOverureTop
//
// [DESCRIPTION]:   Get content of a record by id from the table overture_tracker
//
// [PARAMETERS]:    $overtureTrackerID=0
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Ciciu Gheorghe (null@seopa.com) 08-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetOvertureTop($overtureTrackerID=0)
{

   if(! preg_match("/^\d+$/", $overtureTrackerID))
   {
      $this->strERR = GetErrorString("INVALID_OVERTURE_TRACKER_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_OVERTURE_TRACKINGS." WHERE id='$overtureTrackerID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("OVERTURE_TRACKER_ID_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]               = $this->dbh->GetFieldValue("id");
   $arrayResult["top"]              = $this->dbh->GetFieldValue("top");
   $arrayResult["qztop"]    	    = $this->dbh->GetFieldValue("qztop");
   $arrayResult["additional_info"]  = $this->dbh->GetFieldValue("additional_info");
   $arrayResult["host_ip"]          = $this->dbh->GetFieldValue("host_ip");
   $arrayResult["cr_date"]          = $this->dbh->GetFieldValue("cr_date");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetOverureTrackerByTop
//
// [DESCRIPTION]:   Get content of a record by top from the table overture_tracker
//
// [PARAMETERS]:    $top=""
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Ciciu Gheorghe (null@seopa.com) 08-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTrackerByTop($top="")
{

   if(! preg_match("/^\d+$/", $top))
   {
      $this->strERR = GetErrorString("INVALID_TRACKER_URLS_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_OVERTURE_TRACKINGS." WHERE top='$top'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("TRACK_URLS_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {

      $id = $this->dbh->GetFieldValue("id");

      $arrayResult[$id]["id"]               = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["top"]    	    = $this->dbh->GetFieldValue("top");
      $arrayResult[$id]["qztop"] 	    = $this->dbh->GetFieldValue("qztop");
      $arrayResult[$id]["additional_info"]  = $this->dbh->GetFieldValue("additional_info");
      $arrayResult[$id]["host_ip"]          = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$id]["cr_date"]          = $this->dbh->GetFieldValue("cr_date");

   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetOvertureTrackerByHostIP
//
// [DESCRIPTION]:   Get content of a record by host_ip from the table overture_tracker
//
// [PARAMETERS]:    $hostIP=""
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Ciciu Gheorghe (null@seopa.com) 08-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetOvertureTrackerByHostIP($hostIP="")
{

   if(! preg_match("/((1|2)?\d?\d)\.((1|2)?\d?\d)\.((1|2)?\d?\d)\.((1|2)?\d?\d)/", $hostIP))
   {
      $this->strERR = GetErrorString("INVALID_HOST_IP_FIELD");
      return 0;
   }


   $this->lastSQLCMD = "SELECT * FROM ".SQL_OVERTURE_TRACKINGS." WHERE host_ip='$hostIP'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("HOST_IP_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $id = $this->dbh->GetFieldValue("id");
      
      $arrayResult[$id]["id"]               = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["top"]    	    = $this->dbh->GetFieldValue("top");
      $arrayResult[$id]["qztop"]    	    = $this->dbh->GetFieldValue("qztop");
      $arrayResult[$id]["additional_info"]  = $this->dbh->GetFieldValue("additional_info");
      $arrayResult[$id]["host_ip"]          = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$id]["cr_date"]          = $this->dbh->GetFieldValue("cr_date");

   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetOvertureTrackerByDate
//
// [DESCRIPTION]:   Get content of a record by cr_date from the table overture_tracker
//
// [PARAMETERS]:    $crDate=""
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Ciciu Gheorghe (null@seopa.com) 08-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetOvertureTrackerByDate($crDate=0)
{

   if(! preg_match("/^\d+$/", $crDate))
   {
      $this->strERR = GetErrorString("INVALID_CR_DATE_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_OVERTURE_TRACKINGS." WHERE cr_date='$crDate'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("CR_DATE_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $id = $this->dbh->GetFieldValue("id");

      $arrayResult[$id]["id"]               = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["top"]    	    = $this->dbh->GetFieldValue("top");
      $arrayResult[$id]["qztop"]    	    = $this->dbh->GetFieldValue("qztop");
      $arrayResult[$id]["additional_info"]  = $this->dbh->GetFieldValue("additional_info");
      $arrayResult[$id]["host_ip"]          = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$id]["cr_date"]          = $this->dbh->GetFieldValue("cr_date");
   }

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetOvertureTrackerByTopAndDate
//
// [DESCRIPTION]:   Get content of a record by cr_date from the table overture_tracker
//
// [PARAMETERS]:    $crDate=""
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Ciciu Gheorghe (null@seopa.com) 08-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetOvertureTrackerByTopAndDate($top=0, $beginDate=0, $endDate=0)
{

   if(! preg_match("/^\d+$/", $top))
   {
      $this->strERR = GetErrorString("INVALID_TOP_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $beginDate))
   {
      $this->strERR = GetErrorString("INVALID_CR_DATE_BEGIN_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $endDate))
   {
      $this->strERR = GetErrorString("INVALID_CR_DATE_END_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_OVERTURE_TRACKINGS." WHERE cr_date>='$beginDate' AND cr_date<='$endDate' AND top='$top'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("CR_DATE_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $id = $this->dbh->GetFieldValue("id");

      $arrayResult[$id]["id"]               = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["top"]    	    = $this->dbh->GetFieldValue("top");
      $arrayResult[$id]["qztop"]    	    = $this->dbh->GetFieldValue("qztop");
      $arrayResult[$id]["additional_info"]  = $this->dbh->GetFieldValue("additional_info");
      $arrayResult[$id]["host_ip"]          = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$id]["cr_date"]          = $this->dbh->GetFieldValue("cr_date");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Ciciu Gheorghe (null@seopa.com) 08-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Ciciu Gheorghe (null@seopa.com) 08-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}


}
?>

<?php

/*****************************************************************************/
/*                                                                           */
/*  CQuit class interface
/*                                                                           */
/*  (C) 2005 Istvancsek Gabriel (null@seopa.com)                                     */
/*                                                                           */
/*****************************************************************************/


define("EMAIL_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuit
//
// [DESCRIPTION]:  CQuit class interface
//
// [FUNCTIONS]:    int  AddQuit($name='', $email='')
//                 bool DeleteQuit($emailID=0)
//                 bool GetQuit($emailID=0, &$arrayResult)
//                 bool GetAllQuits(&$arrayResult)
//
//                   Close();
//                 GetError();
//
// [CREATED BY]:   Gabi Istvancsek (null@seopa.com) 2005-05-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CQuit
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuit
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function CQuit($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuit
//
// [DESCRIPTION]:   Add new entry to the news_letter table
//
// [PARAMETERS]:    $name='', $email=''
//
// [RETURN VALUE]:  emailID or 0 in case of failure
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function AddQuit($step='',$reason='',$date='now()', $time='now()')
{
   if(empty($step))
   {
      $this->strERR = GetErrorString("INVALID_STEP_FIELD");
      return 0;
   }

   if(empty($reason))
   {
      $this->strERR = GetErrorString("INVALID_REASON_FIELD");
   }

   $reason = preg_replace('/\s*$/','',$reason);

   $this->lastSQLCMD = "INSERT INTO ".SQL_QUIT." (step,reason,date,time) VALUES ('$step','$reason',$date,$time)";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");

}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQuit
//
// [DESCRIPTION]:   Delete an entry from news_letter table
//
// [PARAMETERS]:    $emailID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-05-13
//
// [MODIFIED]:      - [programmer (email) date]/*   if(! $email->GetAllQuits($resQuits))
//
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function DeleteQuit($quitID)
{
   if(! preg_match("/^\d+$/", $quitID))
   {
      $this->strERR = GetErrorString("INVALID_QUITID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUIT." WHERE id='$quitID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUITID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_QUIT." WHERE id='$quitID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuits
//
// [DESCRIPTION]:   Read data from news_letter table and put it into an array variable
//                  key = emailID, value = array ("name"  => name
//                                                  "email" => email)
//
// [PARAMETERS]:    $startDate="", $endDate="", &$arrayResult
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuits($startDate="" , $endDate="", &$arrayResult)
{
  if(! empty($startDate))
      {
      if(! preg_match("/^\d{4}\-\d{2}\-\d{2}$/", $startDate))
      {
         $this->strERR = GetErrorString("INVALID_START_DATE_FIELD");
         return false;
      }
   }

   if(! empty($endDate))
   {
      if(! preg_match("/^\d{4}\-\d{2}\-\d{2}$/", $endDate))
      {
         $this->strERR = GetErrorString("INVALID_END_DATE_FIELD");
         return false;
      }
   }
   
   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUIT." WHERE 1";

   if($startDate)
      $this->lastSQLCMD .= " AND date>='$startDate'";
   
   if($endDate)
      $this->lastSQLCMD .= " AND date<='$endDate'";
         
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUITS_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $quitResult["step"]   = $this->dbh->GetFieldValue("step");
      $quitResult["reason"] = $this->dbh->GetFieldValue("reason");
      $quitResult["date"]   = $this->dbh->GetFieldValue("date");
      $quitResult["time"]   = $this->dbh->GetFieldValue("time");

      $arrayResult[$this->dbh->GetFieldValue("id")] = $quitResult;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuit
//
// [DESCRIPTION]:   Read data from news_letter table and put it into an array variable
//
// [PARAMETERS]:    $emailID, &$arrayResult
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetQuit($quitID=0,&$arrayResult)
{
   if(! preg_match("/^\d+$/", $emailID))
   {
      $this->strERR = GetErrorString("INVALID_QUITID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUIT." WHERE id='$quitID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUITID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]     = $this->dbh->GetFieldValue("id");
   $arrayResult["step"]   = $this->dbh->GetFieldValue("step");
   $arrayResult["reason"] = $this->dbh->GetFieldValue("reason");
   $arrayResult["date"]   = $this->dbh->GetFieldValue("date");
   $arrayResult["time"]   = $this->dbh->GetFieldValue("time");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

}// end of CQuit class

?>

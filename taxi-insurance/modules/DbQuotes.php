<?php
/*****************************************************************************/
/*                                                                           */
/*  CDbQuotes class interface                                                 */
/*                                                                           */
/*  (C) 2007 Moraru Valeriu (null@seopa.com)                                   */
/*                                                                           */
/*****************************************************************************/


//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);



//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CDbQuotes
//
// [DESCRIPTION]:  CDbQuotes class interface
//
// [FUNCTIONS]:    false | array GetDbQuoteresults($fileName="")
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Moraru Valeriu (null@seopa.com)  19-07-2007
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CDbQuotes
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CDbQuotes
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CDbQuotes($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDbQuoteresults
//
// [DESCRIPTION]:   Get the quotes specific to in file
//
// [PARAMETERS]:    $fileName
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Moraru Valeriu (null@seopa.com) 19-07-2007
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDbQuoteresults( $fileName )
{
   if(empty($fileName))
   {
      $this->strERR = GetErrorString("INVALID_FILE_NAME");
      return false;
   }

   $sqlCmd = "select qzq.quote_ref as qz_quote_ref, qs.site_id, l.filename, q.quote_ref as comp_quote_ref, q.password, qd.* from qzquotes qzq, logs l, quote_status qs, quotes q, quote_details qd where qzq.log_id=l.id and qs.log_id=l.id and q.quote_status_id=qs.id and q.id=qd.quote_id and l.filename='".$fileName."' order by qd.annual_premium asc";
   //print "\n<br>\n".$sqlCmd."\n<br>\n";
   if(! $this->dbh->Exec($sqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   $arrayResult = array();
   
   $k = 0;
   while($this->dbh->MoveNext())
   {
      $arrayResult[$k]["insurer"]              = $this->dbh->GetFieldValue("insurer");
      $arrayResult[$k]["site_id"]              = $this->dbh->GetFieldValue("site_id");      
      $arrayResult[$k]["qz_quote_ref"]         = $this->dbh->GetFieldValue("qz_quote_ref");
      $arrayResult[$k]["comp_quote_ref"]       = $this->dbh->GetFieldValue("comp_quote_ref");
      $arrayResult[$k]["password"]             = $this->dbh->GetFieldValue("password");
      $arrayResult[$k]["quote_details_id"]     = $this->dbh->GetFieldValue("id");
      $arrayResult[$k]["quote_id"]             = $this->dbh->GetFieldValue("quote_id");
      $arrayResult[$k]["annual_premium"]       = $this->dbh->GetFieldValue("annual_premium");
      $arrayResult[$k]["monthly_premium"]      = $this->dbh->GetFieldValue("monthly_premium");
      $arrayResult[$k]["voluntary_excess"]     = $this->dbh->GetFieldValue("voluntary_excess");
      $arrayResult[$k]["voluntary_excess_sec"] = $this->dbh->GetFieldValue("voluntary_excess_sec");
      
      $k++;
   }
   //print "<pre>";print_r($arrayResult);
   // get the modules that did not return a quote
   $sqlCmd = "select distinct(qs.site_id),qs.status,qe.quote_message from quote_status qs,quote_errors qe ,logs l where l.id = qs.log_id and qs.id=qe.quote_status_id and qs.status in ('FAILURE','NO QUOTE','SKIPPED') and l.filename='".$fileName."' order by qs.id asc";
   //print "\n<br>\n".$sqlCmd."\n<br>\n";
   if(! $this->dbh->Exec($sqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   while($this->dbh->MoveNext())
   {
      $arrayResult[$k]["site_id"]              = $this->dbh->GetFieldValue("site_id");      
      $arrayResult[$k]["status"]               = $this->dbh->GetFieldValue("status");
      $arrayResult[$k]["quote_message"]        = $this->dbh->GetFieldValue("quote_message");
      
      $k++;
   }

   //print "<pre>";print_r($arrayResult);

   return $arrayResult;
}



}
?>

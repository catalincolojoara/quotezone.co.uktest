<?php
/*****************************************************************************/
/*                                                                           */
/*  CQuoteUserDetails class interface                                        */
/*                                                                           */
/*  (C) 2005 Istvancsek Gabi(null@seopa.com)                                 */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";
include_once "QzQuoteType.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuoteUserDetails
//
// [DESCRIPTION]:  CQuoteUser class interface
//
// [FUNCTIONS]:
//                 true | false DeleteQuoteUserDetails($userDetailsID='')
//                 array | false GetQuoteUserDetailsByID($userDetailsID='')
//                 array | false GetQuoteUserDetailsByEmail($userEmail='')
//                 array | false GetQuoteUserDetailsByEmailDateOfBirth($userEmail='', $dateOfBirth='')
//                 array | false GetAllQuoteUserDetails($quoteUserID='')
//                 array | false GetAllUsersDetailsByLogID($logID = '')
//
//                 Close();
//                 GetError();
//                 ShowError();
//
// [CREATED BY]:   Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CQuoteUserDetails
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string
    var $quoteType;

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuoteUserDetails
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQuoteUserDetails($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->qzQuoteType = new CQzQuoteType($this->dbh);

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuoteUser
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:  
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function _assertQuoteUserDetails($quoteUserID='', $qzQuoteTypeID='', $SESSION='', $type='add', &$sqlCMD)
{
   $this->strERR = '';

   if(empty($SESSION))
   {
      $this->strERR .= GetErrorString("INVALID_QUOTE_USER_DETAILS");
      return false;
   }

   if(! preg_match('/\d+/i',$quoteUserID))
   {
      $this->strERR .= GetErrorString("INVALID_USER_DETAILS_QUOTE_USER_ID");
      return false;
   }

   if(! preg_match("/^\d+$/", $qzQuoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   $resSession = array();

   $resSession['quote_user_id'] = $quoteUserID;
   $sqlParamCMD                 = '';

   $sqlCMD = "INSERT INTO ".SQL_QUOTE_USER_DETAILS."";

   switch($qzQuoteTypeID)
   {
      // taxi section
      case '18':
      
         ####################################################
         // get param values from the session
         ####################################################

         $SessionProposerDriver  = $SESSION['_YourDetails_'];

         // proposer data
         $resSession['taxi_used_for']               = $SessionProposerDriver["taxi_used_for"]; 
         $resSession['taxi_type']                   = $SessionProposerDriver["taxi_type"]; 
         $resSession['type_of_cover']               = $SessionProposerDriver["type_of_cover"]; 
         $resSession['taxi_make']                   = $SessionProposerDriver["taxi_make"]; 
         $resSession['taxi_model']                  = $SessionProposerDriver["taxi_model"]; 
         $resSession['year_of_manufacture']         = $SessionProposerDriver["year_of_manufacture"]; 
         $resSession['vehicle_mileage']             = $SessionProposerDriver["vehicle_mileage"]; 
         $resSession['estimated_value']             = $SessionProposerDriver["estimated_value"]; 
         $resSession['taxi_capacity']               = $SessionProposerDriver["taxi_capacity"]; 
         $resSession['taxi_ncb']                    = $SessionProposerDriver["taxi_ncb"]; 
         $resSession['private_car_ncb']             = $SessionProposerDriver["private_car_ncb"]; 
         $resSession['plating_authority']           = $SessionProposerDriver["plating_authority"];
         $resSession['period_of_licence']           = $SessionProposerDriver["period_of_licence"];
         $resSession['taxi_badge']            	    = $SessionProposerDriver["taxi_badge"];
         $resSession['taxi_driver']                 = $SessionProposerDriver["taxi_driver"];
         $resSession['claims_5_years']              = $SessionProposerDriver["claims_5_years"];
         $resSession['convictions_5_years']         = $SessionProposerDriver["convictions_5_years"];
         $resSession['vehicle_registration_number'] = $SessionProposerDriver["vehicle_registration_number"];
         $resSession['gap_insurance']               = $SessionProposerDriver["gap_insurance"];
         //$resSession['breakdown_cover']             = $SessionProposerDriver["breakdown_cover"];

         $resSession['title']                = $SessionProposerDriver["title"]; // Proposer title
         $resSession['daytime_phone_number'] = $SessionProposerDriver["daytime_telephone"]; // daytime_phone_number
         $resSession['mobile_phone_number']  = $SessionProposerDriver["mobile_telephone"]; //mobile_phone
         $resSession['best_day_call']        = $SessionProposerDriver["best_day_call"]; 
         $resSession['best_time_call']       = $SessionProposerDriver["best_time_call"]; 

         $resSession['email_address']    = $SessionProposerDriver["email_address"]; // Proposer email address
         $resSession['post_code']        = $SessionProposerDriver["postcode_prefix"]." ".$SessionProposerDriver["postcode_number"]; // Proposer postcode
        
         $resSession['house_number_or_name'] = $SessionProposerDriver["house_number_or_name"]; // Proposer house number and name
         $resSession['date_of_insurance_start'] = $SessionProposerDriver["date_of_insurance_start_yyyy"]."-".$SessionProposerDriver["date_of_insurance_start_mm"]."-".$SessionProposerDriver["date_of_insurance_start_dd"]; // Proposer house number and name


         ####################################################         
         // check the params values
         ####################################################

         if(! preg_match('/[(Mr)|(Mrs)|(Ms)|(Miss)]/i',$resSession['title']))
            $this->strERR = GetErrorString("INVALID_TITLE_FIELD");
            
         $resSession['house_number_or_name'] = str_replace("'","\'",$resSession['house_number_or_name']);

         if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,128}/i',$resSession['house_number_or_name']))
            $this->strERR .= GetErrorString("INVALID_ADDRESS_NAME_FIELD");

         //if(! preg_match('/[0-9\-\s\_]{1,32}/i',$resSession['daytime_phone_number']))
         //   $this->strERR .= GetErrorString("INVALID_DAY_PHONE_FIELD");

         //if(! preg_match('/[0-9\-\s\_]{1,32}/i',$resSession['mobile_phone_number']))
         //   $this->strERR .= GetErrorString("INVALID_MOBILE_PHONE_FIELD");

         if(! preg_match('/[a-zA-Z0-9\-\s\_]{1,16}/i',$resSession['post_code']))
            $this->strERR .= GetErrorString("INVALID_POST_CODE_FIELD");

         if(! preg_match('/\.*\@.*\.[a-zA-Z]{2,3}/i',$resSession['email_address']))
            $this->strERR .= GetErrorString("INVALID_EMAIL_FIELD");

         break;

      default:
         $this->strERR  .= 'INVALID_QUOTE_USER_DETAILS_QUOTE_TYPE_ID';
         break;
   }

   if(! empty($this->strERR))
      return false;

   switch($type)
   {
      case 'add':

         $sqlParamCMDKeys   = ' (';
         $sqlParamCMDValues = ' VALUES (';

         foreach($resSession as $sqlParamName => $sqlParamValue)
         {
            $sqlParamCMDKeys   .= $sqlParamName.",";
            $sqlParamCMDValues .= "'".$sqlParamValue."',";
         }

         $sqlParamCMDKeys   = preg_replace("/,$/s","",$sqlParamCMDKeys);
         $sqlParamCMDValues = preg_replace("/,$/s","",$sqlParamCMDValues);

         $sqlParamCMDKeys   .= ')';
         $sqlParamCMDValues .= ')';

         break;

      default :
         $this->strERR = 'INVALID_QUOTE_USER_DETAILS_ACTION_TYPE';
         return false;

   }

   $sqlCMD .= $sqlParamCMDKeys.$sqlParamCMDValues;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Istvancsek Gabriel(null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   if(! preg_match("/\d{2}\/\d{2}\/\d{4}/i",$date))
      $this->strERR = GetErrorString("INVALID_QUOTE_USER_DETAILS_STD_TO_MYSQL_DATE");

   if(! empty($this->strERR))
      return false;
   
   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteUserDetails
//
// [DESCRIPTION]:   Add new entry to the quote_user_details table
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  userID | false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteUserDetails($quoteUserID='', $qzQuoteTypeID='', $SESSION='')
{
   if(! $this->_assertQuoteUserDetails($quoteUserID, $qzQuoteTypeID, $SESSION, 'add',$lastSqlCmd))
      return false;

   if(! $this->dbh->Exec($lastSqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $lastSqlCmd = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteUserDetailsByID
//
// [DESCRIPTION]:   Read data of a user from quote_user_details table and put it into an array variable
//
// [PARAMETERS]:    $userDetailsID
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
// Send TopQuotesEmail
function GetQuoteUserDetailsByID($userDetailsID='')
{
   if(! preg_match("/^\d+$/", $userDetailsID))
   {
      $this->strERR = GetErrorString("INVALID_USER_DETAILS_ID_FIELD");
      return false;
   }

   $quoteUserDetailsFields = $this->dbh->GetAllFieldsName(SQL_QUOTE_USER_DETAILS);

   $lastSqlCmd = "SELECT * FROM ".SQL_QUOTE_USER_DETAILS." WHERE id='$userDetailsID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USER_DETAILS_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   foreach($quoteUserDetailsFields as $index => $fieldName)
      $arrayResult[$fieldName] = $this->dbh->GetFieldValue($fieldName);
   
   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllUsersDetailsByLogID
//
// [DESCRIPTION]:   Read data from quote_user_details table and put it into an array variable
//                  key = userID, value = Array(that contain all the record for that user)
//
// [PARAMETERS]:     $logID
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllUsersDetailsByLogID($logID = '')
{
   if(! preg_match("/\d+/",$logID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_USER_DETAIK_LOG_ID_FIELD");
      return false;
   }

   $quoteUserDetailsFields = $this->dbh->GetAllFieldsName(SQL_QUOTE_USER_DETAILS);
   $quoteUsersFields       = $this->dbh->GetAllFieldsName(SQL_QUOTE_USERS);


   $lastSqlCmd = "SELECT ".SQL_QUOTE_USERS.".*,".SQL_QUOTE_USER_DETAILS.".*  FROM ".SQL_QUOTE_USERS.",".SQL_QUOTE_USER_DETAILS.",".SQL_QZQUOTES.",".SQL_LOGS." WHERE ".SQL_LOGS.".id='$logID' AND ".SQL_QZQUOTES.".log_id=".SQL_LOGS.".id AND ".SQL_QZQUOTES.".quote_user_details_id=".SQL_QUOTE_USER_DETAILS.".id AND ".SQL_QUOTE_USER_DETAILS.".quote_user_id=".SQL_QUOTE_USERS.".id";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("USER_DETAILS_LOG_ID_NOT_FOUND");
      return false;
   }

   $arrayResult=array();

   foreach($quoteUserDetailsFields as $index => $fieldName)
      $arrayResult[$fieldName] = $this->dbh->GetFieldValue($fieldName);

   foreach($quoteUsersFields as $index => $fieldName)
      $arrayResult[$fieldName] = $this->dbh->GetFieldValue($fieldName);

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}
}//end class CQuoteUserDetails

?>

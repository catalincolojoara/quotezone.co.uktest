<?php
/*****************************************************************************/
/*                                                                           */
/*  CLog class interface
/*                                                                           */
/*  (C) 2005 Istvancsek Gabriel (null@seopa.com)                                     */
/*                                                                           */
/*****************************************************************************/

define("Log_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CLog
//
// [DESCRIPTION]:  CLog class interface
//
// [FUNCTIONS]:      true    | false  AssertLog($quoteUserID='', $hostIp='', $filename='')
//                   string  | false  StdToMysqlDate($date='')
//                   integer | false  AddLog($quoteUserID='', $hostIp='', $filename='', $date='now()', $time='now()')
//                   true    | false  UpdateLog($logID='', $quoteUserID='', $hostIp='', $filename)
//                   true    | false  DeleteLog($logID='')
//                   array   | false  GetLog($logID='')
//                   integer | false  GetLogIdByFilename($fileName='')
//                   array   | false  GetAllLogsByQuoteUserID($quoteUserID = '')
//                   array   | false  GetAllLogsByHostIp($quoteUserID = '')
//                   array   | false  GetAllLogsByInterval($quoteUserID='', $hostIp='', $startDate='', $endDate='', $startTime='',
//                                   $endTime='')
//  		     array   | false  GetLogsByQuoteType($quoteUserID='', $hostIp='', $startDate='',$endDate='',$startTime='',$endTime='' ,$quoteTypeID='')
//                   array   | false GetLastQuoteDate($quoteTypeID)
//                   array   | false GetLastLog($quoteTypeID='')
// 
//                   Close()
//                   GetError()
//                   GetError()
//
// [CREATED BY]:   Istvancsek Gabriel (null@seopa.com) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CLog
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last Log error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CLog
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CLog($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertLog
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    $quoteUserID, $hostIp, $filename
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertLog($quoteUserID='', $hostIp='', $filename='')
{
   $this->strERR = '';

   if(! preg_match("/\d+/",$quoteUserID))
      $this->strERR = GetErrorString("INVALID_LOG_QUOTE_USER_ID_FIELD");


   if(empty($hostIp))
      $this->strERR .= GetErrorString("INVALID_LOG_HOST_IP_FIELD");

   if(empty($filename))
      $this->strERR .= GetErrorString("INVALID_LOG_DATA_USER_FILENAME_FIELD");

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Istvancsek Gabriel(null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   if(! preg_match("/\d{2}\/\d{2}\/\d{4}/i",$date))
      $this->strERR = GetErrorString("INVALID_LOG_STD_TO_MYSQL_DATE");

   if(! empty($this->strERR))
      return false;
   
   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddLog
//
// [DESCRIPTION]:   Add new entry to the Logs table
//
// [PARAMETERS]:    $quoteUserID, $hostIp, $filename, $date, $time
//
// [RETURN VALUE]:  logID or 0 in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-13
//
// [MODIFIED]:      - Velnic Daniel (dan@acux.biz) 2005-08-15
//                    added quote_user_id
//////////////////////////////////////////////////////////////////////////////FE
function AddLog($quoteUserID='', $hostIp='', $filename='', $date='', $time='')
{
   if(! $this->AssertLog($quoteUserID,$hostIp,$filename))
      return false;

   if(empty($date) && empty($time))
   {
      $date='now()';
      $time='now()';

      $this->lastSQLCMD = "INSERT INTO ".SQL_LOGS." (quote_user_id,host_ip,filename,date,time) VALUES  ('$quoteUserID','$hostIp','$filename',$date,$time)";

   }
   else
   {
      $this->lastSQLCMD = "INSERT INTO ".SQL_LOGS." (quote_user_id,host_ip,filename,date,time) VALUES  ('$quoteUserID','$hostIp','$filename','$date','$time')";
      
   }
      

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateLog
//
// [DESCRIPTION]:   Update Log table
//
// [PARAMETERS]:    $logID, $quoteUserID, $hostIp, $filename
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateLog($logID='', $quoteUserID='', $hostIp='', $filename)
{

   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_LOGSID_FIELD");
      return false;
   }

   if(! $this->AssertLog($quoteUserID='', $hostIp='', $filename=''))
      return false;

   // check if logID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_LOGS." WHERE id='$logID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("LOG_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_LOGS." SET quote_user_id='$quoteUserID',host_ip='$hostIp',filename='$filename' WHERE id='$logID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteLog
//
// [DESCRIPTION]:   Delete an entry from Log table
//
// [PARAMETERS]:    $logID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteLog($logID='')
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");
      return false;
   }

   // check if logID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_LOGS." WHERE id='$logID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("LOG_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_LOGS." WHERE id='$logID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLog
//
// [DESCRIPTION]:   Read data from Logs table and put it into an array variable
//
// [PARAMETERS]:    $LogID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLog($logID='')
{
   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_LOGID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_LOGS." WHERE id='$logID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("LOGID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]             = $this->dbh->GetFieldValue("id");
   $arrayResult["quote_user_id"]  = $this->dbh->GetFieldValue("quote_user_id");
   $arrayResult["host_ip"]        = $this->dbh->GetFieldValue("host_ip");
   $arrayResult["filename"]       = $this->dbh->GetFieldValue("filename");
   $arrayResult["date"]           = $this->dbh->GetFieldValue("date");
   $arrayResult["time"]           = $this->dbh->GetFieldValue("time");


   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLogIdByFilename
//
// [DESCRIPTION]:   get log id from logs table by filename
//
// [PARAMETERS]:    $fileName
//
// [RETURN VALUE]:  $logID|false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-26
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLogIdByFilename($fileName='')
{
   if(empty($fileName))
   {
      $this->strERR = GetErrorString("INVALID_LOG_FILENAME_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_LOGS." WHERE filename='$fileName'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
      return false;

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllLogsByQuoteUserID
//
// [DESCRIPTION]:   Read data from Logs table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllLogsByQuoteUserID($quoteUserID = '')
{
   if(! preg_match("/^\d+$/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_LOG_QUOTE_USER_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_LOGS." WHERE quote_user_id='$quoteUserID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("LOG_ID_NOT_FOUND");
      return false;
   }

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;

      $arrayResult[$index]["id"]            = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["quote_user_id"] = $this->dbh->GetFieldValue("quote_user_id");
      $arrayResult[$index]["host_ip"]       = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$index]["filename"]      = $this->dbh->GetFieldValue("filename");
      $arrayResult[$index]["date"]          = $this->dbh->GetFieldValue("date");
      $arrayResult[$index]["time"]          = $this->dbh->GetFieldValue("time");
   }

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllLogsByHostIp
//
// [DESCRIPTION]:   Read data from Logs table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllLogsByHostIp($quoteUserID = '')
{
   if(! preg_match("/^\d+$/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_LOG_QUOTE_USER_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_LOGS." WHERE host_ip='$hostIp'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("LOG_ID_NOT_FOUND");
      return false;
   }

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;

      $arrayResult[$index]["id"]            = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["quote_user_id"] = $this->dbh->GetFieldValue("quote_user_id");
      $arrayResult[$index]["host_ip"]       = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$index]["filename"]      = $this->dbh->GetFieldValue("filename");
      $arrayResult[$index]["date"]          = $this->dbh->GetFieldValue("date");
      $arrayResult[$index]["time"]          = $this->dbh->GetFieldValue("time");
   }

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllLogsByHostIp
//
// [DESCRIPTION]:   Read data from Logs table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCountLogsByHostIpQuoteUserToday($hostIp='', $quoteUserID='', $quoteTypeID='')
{
   if(! empty($hostIp))
   {
      if(! preg_match("/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/",$hostIp))
      {
         $this->strERR = GetErrorString("INVALID_LOG_HOST_IP_FIELD");
         return false;
      }
   }

   if(! empty($quoteUserID))
   {
      if(! preg_match("/^\d+$/", $quoteUserID))
      {
         $this->strERR = GetErrorString("INVALID_LOG_QUOTE_USER_ID_FIELD");
         return false;
      }
   }

   if(! empty($quoteTypeID))
   {
      if(! preg_match("/^\d+$/", $quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_LOG_QUOTE_TYPE_ID_FIELD");
         return false;
      }
   }

   $this->lastSQLCMD = "SELECT count(*) as total FROM ".SQL_LOGS." as l,".SQL_QZQUOTES." as qz WHERE l.id=qz.log_id AND l.date='".date("Y-m-d")."' ";

   if(! empty($hostIp))
      $this->lastSQLCMD .= " AND l.host_ip='$hostIp' ";

   if(! empty($quoteTypeID))
      $this->lastSQLCMD .= " AND qz.quote_type_id='$quoteTypeID' ";

   if(! empty($quoteUserID))
      $this->lastSQLCMD .= " AND l.quote_user_id='$quoteTypeID' ";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("LOG_ID_NOT_FOUND");
      return false;
   }

   return $this->dbh->GetFieldValue("total");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllLogsByInterval
//
// [DESCRIPTION]:   Read data from Logs table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserID, $hostIp, $startDate,$endDate,$startTime,$endTime
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllLogsByInterval($quoteUserID='', $hostIp='', $startDate='',$endDate='',$startTime='',$endTime='')
{
   if(! empty($quoteUserID))
   {
      if(! preg_match("/^\d+$/", $quoteUserID))
      {
         $this->strERR = GetErrorString("INVALID_LOG_QUOTE_USER_ID_FIELD");
         return false;
      }
   }

   if(! empty($hostIp))
   {
      if(! preg_match("/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/",$hostIp))
      {
         $this->strERR = GetErrorString("INVALID_LOG_HOST_IP_FIELD");
         return false;
      }
   }

   if(! empty($startDate))
      if(! $startDate = $this->StdToMysqlDate($startDate))
         return false;

   if(! empty($endDate))
      if(! $endDate = $this->StdToMysqlDate($endDate))
         return false;
   
   if(! empty($startTime))
   {
      if(! preg_match("/^\d{2}\:\d{2}\:\d{2}$/", $startTime))
      {
         $this->strERR = GetErrorString("INVALID_START_TIME_FIELD");
         return false;
      }
   }

   if(! empty($endTime))
   {
      if(! preg_match("/^\d{2}\:\d{2}\:\d{2}$/", $endTime))
      {
         $this->strERR = GetErrorString("INVALID_END_TIME_FIELD");
         return false;
      }
   }

   // we have to check if the startDate and endDate are not the same date and remorting the select mysql 
   //check dates

   list($startYear, $startMonth, $startDay) = split("-", $startDate);
   list($endYear, $endMonth, $endDay)       = split("-", $endDate);

   $diffTimeStamp  = mktime(0,0,0,$endMonth, $endDay, $endYear) - mktime(0,0,0,$startMonth, $startDay, $startYear);

   $case = 0;
   // the same date
   if($diffTimeStamp  == 0 )
      $case = 1;
   else if($diffTimeStamp == 24*60*60 ) // the diff is less or equal than 1 day
      $case = 2;
   else // the diff is more than 1 day
      $case = 3;

   $this->lastSQLCMD = "SELECT * FROM ".SQL_LOGS."";

   switch($case)
   {
      case '1':

         if($startDate || $endDate || $startTime || $endTime)
            $this->lastSQLCMD .=" WHERE 1";

         if($startDate)
            $this->lastSQLCMD .= " AND ".SQL_LOGS.".date='$startDate' ";
         else if($endDate)
            $this->lastSQLCMD .= " AND ".SQL_LOGS.".date='$endDate' ";
         
         if($startTime)
            $this->lastSQLCMD .= " AND ".SQL_LOGS.".time>='$startTime' ";
         
         if($endTime)
            $this->lastSQLCMD .= " AND ".SQL_LOGS.".time<'$endTime' ";
         break;

      case '2':
         if($startDate || $endDate || $startTime || $endTime)
            $this->lastSQLCMD .=" WHERE 1";
         
         if($startDate)
            $this->lastSQLCMD .= " AND ((".SQL_LOGS.".date='$startDate' ";
         
         if($startTime)
            $this->lastSQLCMD .= " AND ".SQL_LOGS.".time>='$startTime') ";
         else
            $this->lastSQLCMD .= " ) ";

         if($endDate)
            $this->lastSQLCMD .= " OR (".SQL_LOGS.".date='$endDate' ";

         if($endTime)
            $this->lastSQLCMD .= " AND ".SQL_LOGS.".time<'$endTime')) ";
         else
            $this->lastSQLCMD .= " )) ";

         break;

      case '3':
         if($startDate || $endDate || $startTime || $endTime)
            $this->lastSQLCMD .=" WHERE 1";
         
         if($startDate)
            $this->lastSQLCMD .= " AND ((".SQL_LOGS.".date='$startDate' ";

         if($startTime)
            $this->lastSQLCMD .= " AND ".SQL_LOGS.".time>='$startTime') ";
         else
            $this->lastSQLCMD .= " ) ";

         if($endDate)
            $this->lastSQLCMD .= " OR (".SQL_LOGS.".date='$endDate' ";

         if($endTime)
            $this->lastSQLCMD .= " AND ".SQL_LOGS.".time<'$endTime') ";
         else
            $this->lastSQLCMD .= " ) ";

         $endDateInter   = date("Y-m-d", mktime(0,0,0,$endMonth, $endDay - 1, $endYear));
         $startDateInter = date("Y-m-d", mktime(0,0,0,$startMonth, $startDay + 1, $startYear));

         if($startDate)
            $this->lastSQLCMD .= " OR (".SQL_LOGS.".date>='$startDateInter' ";
         else
            $this->lastSQLCMD .= " ) ";

         if($endDate)
            $this->lastSQLCMD .= " AND ".SQL_LOGS.".date<='$endDateInter')) ";
         else
            $this->lastSQLCMD .= " )) ";
         
         break;
   }// end switch($case)

   if(! empty($quoteUserID))
      $this->lastSQLCMD .=" AND ".SQL_LOGS.".quote_user_id='".$quoteUserID."' "; 
      
   if(! empty($hostIp))
      $this->lastSQLCMD .=" AND ".SQL_LOGS.".host_ip='".$hostIp."' ";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("LOG_ID_NOT_FOUND");
      return false;
   }

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;

      $arrayResult[$index]["id"]            = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["quote_user_id"] = $this->dbh->GetFieldValue("quote_user_id");
      $arrayResult[$index]["host_ip"]       = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$index]["filename"]      = $this->dbh->GetFieldValue("filename");
      $arrayResult[$index]["date"]          = $this->dbh->GetFieldValue("date");
      $arrayResult[$index]["time"]          = $this->dbh->GetFieldValue("time");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLogsByQuoteType
//
// [DESCRIPTION]:   Read data from Logs table and put it into an array variable
//
// [PARAMETERS]:    $quoteUserID, $hostIp, $startDate,$endDate,$startTime,$endTime,$quoteTypeID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLogsByQuoteType($quoteUserID='', $hostIp='', $startDate='',$endDate='',$startTime='',$endTime='' ,$quoteTypeID='')
{
   if(! empty($quoteUserID))
   {
      if(! preg_match("/^\d+$/", $quoteUserID))
      {
         $this->strERR = GetErrorString("INVALID_LOG_QUOTE_USER_ID_FIELD");
         return false;
      }
   }

   if(! empty($hostIp))
   {
      if(! preg_match("/\d{3}\.\d{3}\.\d{3}\.\d{3}/",$hostIp))
      {
         $this->strERR = GetErrorString("INVALID_LOG_HOST_IP_FIELD");
         return false;
      }
   }

   if(! empty($startDate))
      if(! $startDate = $this->StdToMysqlDate($startDate))
         return false;

   if(! empty($endDate))
      if(! $endDate = $this->StdToMysqlDate($endDate))
         return false;

   if(! empty($startTime))
   {
      if(! preg_match("/^\d{2}\:\d{2}\:\d{2}$/", $startTime))
      {
         $this->strERR = GetErrorString("INVALID_START_TIME_FIELD");
         return false;
      }
   }

   if(! empty($endTime))
   {
      if(! preg_match("/^\d{2}\:\d{2}\:\d{2}$/", $endTime))
      {
         $this->strERR = GetErrorString("INVALID_END_TIME_FIELD");
         return false;
      }
   }

	if(! empty($quoteTypeID))
   {
      if(! preg_match("/^\d+$/", $quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
         return false;
      }
   }

   // we have to check if the startDate and endDate are not the same date and remorting the select mysql
   //check dates

   list($startYear, $startMonth, $startDay) = split("-", $startDate);
   list($endYear, $endMonth, $endDay)       = split("-", $endDate);

   $diffTimeStamp  = mktime(0,0,0,$endMonth, $endDay, $endYear) - mktime(0,0,0,$startMonth, $startDay, $startYear);

   $case = 0;
   // the same date
   if($diffTimeStamp  == 0 )
      $case = 1;
   else if($diffTimeStamp == 24*60*60 ) // the diff is less or equal than 1 day
      $case = 2;
   else // the diff is more than 1 day
      $case = 3;

   $sqlCMD = "SELECT l.*,qzq.quote_type_id from ".SQL_LOGS." l, ".SQL_QZQUOTES." qzq";

   switch($case)
   {
      case '1':

         if($startDate || $endDate || $startTime || $endTime)
            $sqlCMD .=" WHERE 1";

         if($startDate)
            $sqlCMD .= " AND l.date='$startDate' ";
         else if($endDate)
            $sqlCMD .= " AND l.date='$endDate' ";

         if($startTime)
            $sqlCMD .= " AND l.time>='$startTime' ";

         if($endTime)
            $sqlCMD .= " AND l.time<'$endTime' ";
         break;

      case '2':
         if($startDate || $endDate || $startTime || $endTime)
            $sqlCMD .=" WHERE 1";

         if($startDate)
            $sqlCMD .= " AND ((l.date='$startDate' ";

         if($startTime)
            $sqlCMD .= " AND l.time>='$startTime') ";
         else
            $sqlCMD .= " ) ";

         if($endDate)
            $sqlCMD .= " OR (l.date='$endDate' ";

         if($endTime)
            $sqlCMD .= " AND l.time<'$endTime')) ";
         else
            $sqlCMD .= " )) ";

         break;

      case '3':
         if($startDate || $endDate || $startTime || $endTime)
            $sqlCMD .=" WHERE 1";

         if($startDate)
            $sqlCMD .= " AND ((l.date='$startDate' ";

         if($startTime)
            $sqlCMD .= " AND l.time>='$startTime') ";
         else
            $sqlCMD .= " ) ";

         if($endDate)
            $sqlCMD .= " OR (l.date='$endDate' ";

         if($endTime)
            $sqlCMD .= " AND l.time<'$endTime') ";
         else
            $sqlCMD .= " ) ";

         $endDateInter   = date("Y-m-d", mktime(0,0,0,$endMonth, $endDay - 1, $endYear));
         $startDateInter = date("Y-m-d", mktime(0,0,0,$startMonth, $startDay + 1, $startYear));

         if($startDate)
            $sqlCMD .= " OR (l.date>='$startDateInter' ";
         else
            $sqlCMD .= " ) ";

         if($endDate)
            $sqlCMD .= " AND l.date<='$endDateInter')) ";
         else
            $sqlCMD .= " )) ";

         break;
   }// end switch($case)

   if(! empty($quoteUserID))
      $sqlCMD .=" AND l.quote_user_id='".$quoteUserID."' ";

   if(! empty($hostIp))
      $sqlCMD .=" AND l.host_ip='".$hostIp."' ";

	if(! empty($quoteTypeID))
		$sqlCMD .=" AND l.id=qzq.log_id AND qzq.quote_type_id='".$quoteTypeID."' ";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("LOG_ID_NOT_FOUND");
      return false;
   }

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;

      $arrayResult[$index]["id"]            = $this->dbh->GetFieldValue("id");
      $arrayResult[$index]["quote_user_id"] = $this->dbh->GetFieldValue("quote_user_id");
      $arrayResult[$index]["host_ip"]       = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$index]["filename"]      = $this->dbh->GetFieldValue("filename");
      $arrayResult[$index]["date"]          = $this->dbh->GetFieldValue("date");
      $arrayResult[$index]["time"]          = $this->dbh->GetFieldValue("time");
      $arrayResult[$index]["quote_type_id"] = $this->dbh->GetFieldValue("quote_type_id");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLastQuoteDate($quoteTypeID)
//
// [DESCRIPTION]:   Get last quote time and date
//
// [PARAMETERS]:    $quoteTypeId
//
// [RETURN VALUE]:  $dateAndTime
//
// [CREATED BY]:    Listeveanu Cezar (null@seopa.com) 2006-11-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLastQuoteDate($quoteTypeID)
{
   $this->lastSQLCMD = "SELECT l.date, l.time FROM ".SQL_LOGS." l, ".SQL_QZQUOTES." qz WHERE qz.log_id=l.id";

   if($quoteTypeID)
        $this->lastSQLCMD .= " AND qz.quote_type_id='$quoteTypeID' ORDER BY l.id DESC LIMIT 1";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $date = $this->dbh->GetFieldValue("date");
   $time = $this->dbh->GetFieldValue("time");

   $dateAndTime = $date." ".$time;

   return $dateAndTime;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLastLog
//
// [DESCRIPTION]:   Read data from Logs table and put it into an array variable
//
// [PARAMETERS]:    $quoteTypeID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Sturza Ciprian (ciprian.sturza@seopa.com) 2010-06-15
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLastLog($quoteTypeID='')
{
   if(! empty($quoteTypeID))
   {
      if(! preg_match("/^\d+$/", $quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
         return false;
      }
   }

   $sqlCMD = "SELECT l.* from ".SQL_LOGS." l, ".SQL_QZQUOTES." qzq WHERE l.id=qzq.log_id AND qzq.quote_type_id=$quoteTypeID AND l.host_ip <> '86.125.114.56' AND l.host_ip <> '86.125.114.56' AND l.host_ip <> '37.153.72.90' ORDER BY l.id DESC LIMIT 1";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult["id"]            = $this->dbh->GetFieldValue("id");
   $arrayResult["quote_user_id"] = $this->dbh->GetFieldValue("quote_user_id");
   $arrayResult["host_ip"]       = $this->dbh->GetFieldValue("host_ip");
   $arrayResult["filename"]      = $this->dbh->GetFieldValue("filename");
   $arrayResult["date"]          = $this->dbh->GetFieldValue("date");
   $arrayResult["time"]          = $this->dbh->GetFieldValue("time");

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Print the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}
} // end of CLog class
?>

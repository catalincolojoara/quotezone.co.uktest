<?php
/*****************************************************************************/
/*                                                                           */
/*  CMailInfo class interface                                              */
/*                                                                           */
/*  (C) 2005 Velnic Daniel (null@seopa.com)                                   */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

//if(DEBUG_MODE)
//   error_reporting(1);
//else
//   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CMailInfo
//
// [DESCRIPTION]:  CMailInfo class interface
//
// [FUNCTIONS]:    int  AddMailInfo($qzQuotesID=0)
//                 bool DeleteMailInfo($mailInfoID=0)
//                 bool UpdateMailInfo($mailInfoID=0, $qzQuotesID='')
//                 bool GetAllMailInfo()
//                 bool GetMailInfoByID($mailInfoID=0)
//                 bool GetMailInfoByQzQuotes($qzQuotesID='')
//                 bool GetLastMailInfoID()
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Velnic Daniel (null@seopa.com) 2005-08-31
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CMailInfo
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CMailInfo
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 31-08-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CMailInfo($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddMailInfo
//
// [DESCRIPTION]:   Add new entry to the mail_info table
//
// [PARAMETERS]:    $qzQuotesID=''
//
// [RETURN VALUE]:  $mailInfoID or 0 in case of failure
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 25-08-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddMailInfo($qzQuotesID=0, $quoteTypeID=0)
{
   if(! preg_match("/^\d+$/", $qzQuotesID))
   {
      $this->strERR = GetErrorString("INVALID_MAILINFO_QUOTES_ID_FIELD");
      return 0;
   }

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_MAILINFO_QUOTE_TYPE_ID_FIELD");
      return 0;
   }

   $this->lastSQLCMD = "INSERT INTO ".SQL_MAIL_INFO." (qzquote_id,quote_type_id,date,time) VALUES ('$qzQuotesID','$quoteTypeID',now(),now())";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddMailInfoCarMailTopOff
//
// [DESCRIPTION]:   Add new entry to the mail_info table
//
// [PARAMETERS]:    $qzQuotesID=''
//
// [RETURN VALUE]:  $mailInfoID or 0 in case of failure
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 25-08-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddMailInfoCarMailTopOff($qzQuotesID=0,$quoteTypeId='')
{
   if(! preg_match("/^\d+$/", $qzQuotesID))
   {
      $this->strERR = GetErrorString("INVALID_MAILINFO_QUOTES_ID_FIELD");
      return 0;
   }   

   if(! preg_match("/^\d+$/", $quoteTypeId))
   {
      $this->strERR = GetErrorString("INVALID_MAILINFO_QUOTE_TYPE_ID_FIELD");
      return 0;
   }   

   $this->lastSQLCMD = "SELECT * FROM ".SQL_MAIL_INFO." WHERE qzquote_id='$qzQuotesID' AND quote_type_id='$quoteTypeId'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   if ($this->dbh->GetRows() > 0)
   {
   		if (!$this->dbh->FetchRows())
   		{
	    	   $this->strERR = $this->dbh->GetError();
		   return 0;
   		}
   		
   		return $this->dbh->GetFieldValue('id');
   }
      
   $this->lastSQLCMD = "INSERT INTO ".SQL_MAIL_INFO." (qzquote_id,top_one,top_three,quote_type_id,date,time) VALUES ('$qzQuotesID','OFF','OFF','$quoteTypeId',now(),now())";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteMailInfo
//
// [DESCRIPTION]:   Delete an entry from mail_info table by ID
//
// [PARAMETERS]:    $mailInfoID=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 25-08-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteMailInfo($mailInfoID=0)
{
   if(! preg_match("/^\d+$/", $mailInfoID))
   {
      $this->strERR = GetErrorString("INVALID_MAIL_INFO_ID_FIELD");
      return 0;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_MAIL_INFO." WHERE id='$mailInfoID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("CONTENT_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_MAIL_INFO." WHERE id='$mailInfoID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateMailInfo
//
// [DESCRIPTION]:   Update a record
//
// [PARAMETERS]:    $mailInfoID=0, $qzQuotesID=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 25-08-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateMailInfo($mailInfoID=0, $qzQuotesID='')
{
   if(! preg_match("/^\d+$/", $mailInfoID))
   {
      $this->strERR = GetErrorString("INVALID_MAIL_INFO_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $qzQuotesID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTES_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM mail_info WHERE id='$mailInfoID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("MAILINFOID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_MAIL_INFO." SET qzquote_id='$qzQuotesID',date=now(),time=now() WHERE id='$mailInfoID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateMailInfo
//
// [DESCRIPTION]:   Update a record
//
// [PARAMETERS]:    $mailInfoID=0, $qzQuotesID=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 25-08-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetSendMailInfo($qzQuoteID='', $topOne='', $topThree='')
{

   if(! preg_match("/^\d+$/", $qzQuoteID))
   {
      $this->strERR = GetErrorString("INVALID_MAIL_INFO_QZ_QUOTES_ID_FIELD");
      return false;
   }

   if(! preg_match("/^(ON)|(OFF)$/", $topOne))
   {
      $this->strERR = GetErrorString("INVALID_MAIL_INFO_TOP_ONE_FIELD");
      return false;
   }

   if(! preg_match("/^(ON)|(OFF)$/", $topThree))
   {
      $this->strERR = GetErrorString("INVALID_MAIL_INFO_TOP_THREE_FIELD");
      return false;
   }

   if($topOne == 'OFF' && $topThree == 'OFF')
   {
      $this->strERR = GetErrorString("MUST_SET_ONE_OF_THE_TOPS");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_MAIL_INFO." WHERE qzquote_id='$qzQuoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("MAIL_INFO_QZ_QUOTE_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_MAIL_INFO." SET ";

   if($topOne == 'ON')
      $this->lastSQLCMD .= "top_one='$topOne'";

   if($topOne == 'ON' && $topThree == 'ON')
      $this->lastSQLCMD .= ",";

   if($topThree == 'ON')
      $this->lastSQLCMD .= "top_three='$topThree'";

   $this->lastSQLCMD .= ",date=now(),time=now() WHERE qzquote_id='$qzQuoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateMailInfo
//
// [DESCRIPTION]:   Update a record
//
// [PARAMETERS]:    $mailInfoID=0, $qzQuotesID=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 25-08-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UnsetSendMailInfo($qzQuoteID='', $top='')
{

   if(! preg_match("/^\d+$/", $qzQuoteID))
   {
      $this->strERR = GetErrorString("INVALID_MAIL_INFO_QZ_QUOTE_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $top))
   {
      $this->strERR = GetErrorString("INVALID_MAIL_INFO_TOP_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_MAIL_INFO." WHERE qzquote_id='$qzQuoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("MAIL_INFO_QZ_QUOTE_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_MAIL_INFO." SET ";

   switch($top)
   {
      case '1':
         $this->lastSQLCMD .= "top_one='OFF'";
         break;

      case '3':
         $this->lastSQLCMD .= "top_three='OFF'";
         break;
   }

   $this->lastSQLCMD .= ",date=now(),time=now() WHERE qzquote_id='$qzQuoteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllMailInfo
//
// [DESCRIPTION]:   Get all content of the table mail_info
//
// [PARAMETERS]:    &$arrayResult
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 25-08-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllMailInfo()
{
   $this->lastSQLCMD = "SELECT * FROM ".SQL_MAIL_INFO."";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("MAIL_INFO_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $contentResult["id"]         = $this->dbh->GetFieldValue("id");
      $contentResult["qzquote_id"] = $this->dbh->GetFieldValue("qzquote_id");
      $contentResult["date"]       = $this->dbh->GetFieldValue("date");
      $contentResult["time"]       = $this->dbh->GetFieldValue("time");

      $arrayResult[$this->dbh->GetFieldValue("id")] = $contentResult;
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMailInfoByID
//
// [DESCRIPTION]:   Get a record by ID
//
// [PARAMETERS]:    $mailInfoID=0,&$arrayResult
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 25-08-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMailInfoByID($mailInfoID=0)
{
   if(! preg_match("/^\d+$/", $mailInfoID))
   {
      $this->strERR = GetErrorString("INVALID_ID_MAILINFO_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_MAIL_INFO." WHERE id='$mailInfoID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("MAILINFO_ID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]         = $this->dbh->GetFieldValue("id");
   $arrayResult["qzquote_id"] = $this->dbh->GetFieldValue("qzquote_id");
   $arrayResult["date"]       = $this->dbh->GetFieldValue("date");
   $arrayResult["time"]       = $this->dbh->GetFieldValue("time");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMailInfoByQzQuotes
//
// [DESCRIPTION]:   Get all record by qzquotes_id
//
// [PARAMETERS]:    $qzQuotesID='',&$arrayResult
//
// [RETURN VALUE]:  false | true
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 25-08-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMailInfoByQzQuoteID($qzQuotesID='')
{
   if(! preg_match("/^\d+$/", $qzQuotesID))
   {
      $this->strERR = GetErrorString("INVALID_QZ_QUOTES_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_MAIL_INFO." WHERE qzquote_id='$qzQuotesID'";

    if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QZQUOTES_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("MAILINFO_ID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]         = $this->dbh->GetFieldValue("id");
   $arrayResult["qzquote_id"] = $this->dbh->GetFieldValue("qzquote_id");
   $arrayResult["date"]       = $this->dbh->GetFieldValue("date");
   $arrayResult["time"]       = $this->dbh->GetFieldValue("time");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLastMailInfoID()
//
// [DESCRIPTION]:   Get last id
//
// [PARAMETERS]:    
//
// [RETURN VALUE]:  false | true
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 25-08-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetLastMailInfoID($quoteTypeID=0)
{

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_MAILINFO_QUOTE_TYPE_ID_FIELD");
      return 0;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_MAIL_INFO." WHERE quote_type_id='$quoteTypeID' ORDER BY id DESC LIMIT 1";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

    if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("MAILINFO_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult["id"]         = $this->dbh->GetFieldValue("id");
   $arrayResult["qzquote_id"] = $this->dbh->GetFieldValue("qzquote_id");
   $arrayResult["date"]       = $this->dbh->GetFieldValue("date");
   $arrayResult["time"]       = $this->dbh->GetFieldValue("time");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetLastScanMailInfoID()
//
// [DESCRIPTION]:   Get last id
//
// [PARAMETERS]:    
//
// [RETURN VALUE]:  false | true
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 25-08-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTopOneScanMailInfoID($quoteTypeID=0)
{

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_MAILINFO_QUOTE_TYPE_ID_FIELD");
      return 0;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_MAIL_INFO." WHERE top_one='ON' AND quote_type_id='$quoteTypeID' ORDER BY id DESC LIMIT 1";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

    if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("MAILINFO_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $index = 0;

   $arrayResult["id"]         = $this->dbh->GetFieldValue("id");
   $arrayResult["qzquote_id"] = $this->dbh->GetFieldValue("qzquote_id");
   $arrayResult["date"]       = $this->dbh->GetFieldValue("date");
   $arrayResult["time"]       = $this->dbh->GetFieldValue("time");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserInformationForUserMailInfoFile
//
// [DESCRIPTION]:   Get information about the user
//
// [PARAMETERS]:    $filename
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 25-08-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteDetailsByLogId($logID='')
{

   if(! preg_match("/^\d+$/", $logID))
   {
      $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD_IN_MAILINFO");
      return false;
   }

   $this->lastSQLCMD = " select qd.insurer,qd.id as quoteDetId,qd.annual_premium,qd.quote_id,qs.site_id from ".SQL_LOGS." l,".SQL_QUOTE_STATUS." qs,".SQL_QUOTE." q, ".SQL_QUOTE_DETAILS." qd where l.id='$logID' and l.id=qs.log_id and qs.id=q.quote_status_id and q.id=qd.quote_id and qs.status='SUCCESS' order by qs.site_id desc;";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("NO_RECORD_FOUND_FOR_LOG_ID_IN_MAILINFO");
      return false;
   }
   
   $i=0;
   $content = array();
   while($this->dbh->MoveNext())
   {
      $i = $this->dbh->GetFieldValue("quoteDetId");
      $arrayResult["id"]             = $this->dbh->GetFieldValue("id");
      $arrayResult["quote_id"]       = $this->dbh->GetFieldValue("quote_id");
      $arrayResult["insurer"]        = $this->dbh->GetFieldValue("insurer");
      $arrayResult["annual_premium"] = $this->dbh->GetFieldValue("annual_premium");
      $arrayResult["site_id"]        = $this->dbh->GetFieldValue("site_id");

      if($arrayResult["site_id"] == $content[$arrayResult["site_id"]])
         $content[$arrayResult["site_id"]][$i] = $arrayResult;
      else
         $content[$arrayResult["site_id"]][$i] = $arrayResult;
   }
   
   return $content;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 25-08-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 25-08-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

}
?>

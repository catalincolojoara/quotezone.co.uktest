<?php
/*****************************************************************************/
/*                                                                           */
/*  CTrackUrls class interface                                               */
/*                                                                           */
/*  (C) 2005 Velnic Daniel (null@seopa.com)                                   */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CTrackUrls
//
// [DESCRIPTION]:  CTrackUrls class interface
//
// [FUNCTIONS]:    bool AssertTrakerUrls($trackUrl='',$name='')
//                 int  AddTrackUrls($trackUrl='', $name='')
//                 bool UpdateTrackUrls($trackUrlID=0,$trackUrl='',$name='')
//                 bool DeleteTrackUrls($trackUrlID=0)
//                 bool GetAllTrackUrls()
//                 bool GetTrackUrlsByID($trackUrlID=0)
//                 bool GetTrackUrlsByUrl($trackUrl="")
//                 bool GetTrackUrlByName($name="")
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CTrackUrls
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CTrackUrls
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CTrackUrls($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertTrakerUrls
//
// [DESCRIPTION]:   Verify to see if the params are completed
//
// [PARAMETERS]:    $trackUrl='',$name=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertTrakerUrls($trackUrl='',$name='')
{
   $this->strERR = "";

   if(empty($trackUrl))
      $this->strERR = GetErrorString("INVALID_TRACK_URL_FIELD");

   if(empty($name))
      $this->strERR = GetErrorString("INVALID_TRACK_URL_NAME_FIELD");

    if($this->strERR != "")
      return true;

   return false;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddTrackUrls
//
// [DESCRIPTION]:   Add new entry to the track_urls table
//
// [PARAMETERS]:    $trackUrl='',$name=''
//
// [RETURN VALUE]:  trUrlID or 0 in case of failure
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddTrackUrls($trackUrl='', $name='')
{

   if($this->AssertTrakerUrls($trackUrl,$name))
      return 0;
/*
   $this->lastSQLCMD = "SELECT * FROM track_urls WHERE (url='$trackUrl' AND name='$name')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {

       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   if($this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("TRACK_URL_ALREADY_EXISTS");
      return 0;
   }
*/
   $this->lastSQLCMD = "INSERT INTO track_urls(url, name) VALUES ('$trackUrl','$name')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateTrackUrls
//
// [DESCRIPTION]:   Update a entry to the track_urls table
//
// [PARAMETERS]:    $trackUrlID=0, $trackUrl='', $name=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateTrackUrls($trackUrlID=0, $trackUrl='', $name='')
{
   if(! preg_match("/^\d+$/", $trackUrlID))
   {
      $this->strERR = GetErrorString("INVALID_TRACK_URLS_ID_FIELD");
      return false;
   }

   if($this->AssertTrakerUrls($trackUrl,$name))
      return false;
/*
   $this->lastSQLCMD = "SELECT url,name FROM  track_urls WHERE id='$trackUrlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("TRACK_URLS_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $dbTrackUrl = $this->dbh->GetFieldValue("url");
   $dbName     = $this->dbh->GetFieldValue("name");

   if($trackUrl != $dbTrackUrl)
   {
      $this->lastSQLCMD = "SELECT * FROM track_urls WHERE url='$trackUrl' AND id != $trackUrlID";

      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if($this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("TRACK_URL_ADDRESS_ALREADY_EXISTS");
         return false;
      }
   }

   if($name != $dbName)
   {
      $this->lastSQLCMD = "SELECT * FROM track_urls WHERE name='$name' AND id != $trackUrlID";

      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if($this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("TRACK_URL_NAME_ALREADY_EXISTS");
         return false;
      }
   }
*/
   $this->lastSQLCMD = "UPDATE track_urls SET url='$trackUrl', name='$name' WHERE id=$trackUrlID";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteTrackUrls
//
// [DESCRIPTION]:   Delete an entry from track_urls table by ID
//
// [PARAMETERS]:    $trackUrlID=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteTrackUrls($trackUrlID=0)
{
   if(! preg_match("/^\d+$/", $trackUrlID))
   {
      $this->strERR = GetErrorString("INVALID_TRACK_URLS_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM track_urls WHERE id='$trackUrlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("TRACK_URLS_ID_FIELD_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM tracker WHERE track_urls_id='$trackUrlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }


   if($this->dbh->GetRows())
   {
      $this->lastSQLCMD = "DELETE FROM tracker WHERE track_urls_id='$trackUrlID'";

      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }
   }

   $this->lastSQLCMD = "DELETE FROM track_urls WHERE id='$trackUrlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllTrackUrls
//
// [DESCRIPTION]:   Get all content of the table track_urls
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllTrackUrls()
{
   $this->lastSQLCMD = "SELECT * FROM track_urls ORDER BY id DESC";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("TRACK_URLS_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $id = $this->dbh->GetFieldValue("id");

      $arrayResult[$id]["id"]   = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["url"]  = $this->dbh->GetFieldValue("url");
      $arrayResult[$id]["name"] = $this->dbh->GetFieldValue("name");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTrackUrlsByID
//
// [DESCRIPTION]:   Get content of a record by id from the table track_urls
//
// [PARAMETERS]:    $trackUrlID=0
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTrackUrlsByID($trackUrlID=0)
{

   if(! preg_match("/^\d+$/", $trackUrlID))
   {
      $this->strERR = GetErrorString("INVALID_TRACK_URLS_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM track_urls WHERE id='$trackUrlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("TRACK_URLS_ID_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]        = $this->dbh->GetFieldValue("id");
   $arrayResult["url"]       = $this->dbh->GetFieldValue("url");
   $arrayResult["name"]      = $this->dbh->GetFieldValue("name");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTrackUrlsByUrl
//
// [DESCRIPTION]:   Get content of a record by url from the table track_urls
//
// [PARAMETERS]:    $trackUrl=""
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTrackUrlsByUrl($trackUrl="")
{
// todo no array ref, return array
   if(empty($trackUrl))
   {
      $this->strERR = GetErrorString("INVALID_TRACK_URL_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM track_urls WHERE url='$trackUrl'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("TRACK_URL_FIELD_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]        = $this->dbh->GetFieldValue("id");
   $arrayResult["url"]       = $this->dbh->GetFieldValue("url");
   $arrayResult["name"]      = $this->dbh->GetFieldValue("name");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTrackUrlByName
//
// [DESCRIPTION]:   Get all content of the table track_urls
//
// [PARAMETERS]:    $name=""
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTrackUrlByName($name="")
{

   if(empty($name))
   {
      $this->strERR = GetErrorString("INVALID_TRACK_URL_NAME_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM track_urls WHERE name='$name'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("TRACK_URLS_NAME_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
      return false;

   $arrayResult = array();

   $arrayResult["id"]   = $this->dbh->GetFieldValue("id");
   $arrayResult["url"]  = $this->dbh->GetFieldValue("url");
   $arrayResult["name"] = $this->dbh->GetFieldValue("name");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

}

?>

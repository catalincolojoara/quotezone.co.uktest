<?php

include_once "errors.inc";
include_once "MySQL.php";
// 
if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CStepTrackings
//
// [DESCRIPTION]:  CStepTrackings class interface
//
// [FUNCTIONS]:    true   | false AssertStepTrackings($step='',$quoteTypeID=0)
//                 int    | false AddStepTrackings($step='',$quoteTypeID=0,$hostIP='',$date='now()',$time='now()')
//                 true   | false UpdateStepTrackings($id,$step='',$date='now()',$time='now()')
//                 true   | false GetStepTrackings($quitID=0,&$arrayResult)
//                 true   | false GetAllStepTrackings($quoteTypeID,$step,$startDate,$endDate,&$arrayResult)
//
//                                SaveSessionFile()
//                                GetError()
//
// [CREATED BY]:   Gigi Ciciu (null@seopa.com) 2006-05-19
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
   
class CStepTrackings
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

function CStepTrackings($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();
      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertStepTrackings
//
// [DESCRIPTION]:   Verify to see if the requested fields are completed
//
// [PARAMETERS]:    $step, $quoteTypeID, $hostIP
//
// [RETURN VALUE]:  true or false in case of failure
//
// [CREATED BY]:    Gigi Ciciu (null@seopa.com) 2006-05-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertStepTrackings($step='',$quoteTypeID=0)
{
    $this->strERR = "";

    if(! preg_match("/^\d+$/", $quoteTypeID))
    {
       $this->strERR .= GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
       return false;
    }
    
    if(empty($step))
    {
      $this->strERR .= GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
      return false;
    } 
   
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddStepTrackings
//
// [DESCRIPTION]:   Add new entry into the step_trackings table
//
// [PARAMETERS]:    $step='', $quoteTypeID=0, $hostIP='', $date='now()', $time='now()';
//
// [RETURN VALUE]:   or 0 in case of failure
//
// [CREATED BY]:    Gigi Ciciu (null@seopa.com) 2006-05-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function AddStepTrackings($step='',$quoteTypeID=0,$hostIP='',$date='now()',$time='now()')
{

	if(! $this-> AssertStepTrackings($step,$quoteTypeID,$hostIP))
		return false;
		
	$this->lastSQLCMD = "INSERT INTO ".SQL_STEP_TRACKINGS." (quote_type_id,step,host_ip,date,time) VALUES ('$quoteTypeID','$step','$hostIP',$date,$time)";
	
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }
   
   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateStepTrackings
//
// [DESCRIPTION]:   Update data from step_trackings table 
//
// [PARAMETERS]:    $StepTrackingID, &$arrayResult
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gigi Ciciu (null@seopa.com) 2006-05-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function UpdateStepTrackings($id,$step='',$date='now()',$time='now()')
{
	
	if(! preg_match("/^\d+$/", $id))
   {
      $this->strERR = GetErrorString("INVALID_STEP_TRACKING_ID_FIELD");
      return false;
   }

	$this->lastSQLCMD = "UPDATE ".SQL_STEP_TRACKINGS." SET step='$step',date=$date,time=$time WHERE id='$id'";
	
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }
   
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetStepTrackings
//
// [DESCRIPTION]:   Read data from step_trackings table and put them into an array variable
//
// [PARAMETERS]:    $StepTrackingID, &$arrayResult
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gigi Cicicu (null@seopa.com) 2006-05-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetStepTrackings($quitID=0,&$arrayResult)
{
	if(! preg_match("/^\d+$/", $quitID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_STEP_TRACKINGS." WHERE id='$quitID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("SQL_QUOTE_ID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]     			= $this->dbh->GetFieldValue("id");
   $arrayResult["step"]   			= $this->dbh->GetFieldValue("step");
   $arrayResult["quoteTypeID"] 			= $this->dbh->GetFieldValue("quote_type_id");
   $arrayResult["date"]   			= $this->dbh->GetFieldValue("date");
   $arrayResult["time"]   			= $this->dbh->GetFieldValue("time");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllStepTrackings
//
// [DESCRIPTION]:   Read data from step_trackings table and put them into an array variable
//
// [PARAMETERS]:    $startDate="",$step="", $endDate="", &$arrayResult
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gigi Ciciu (null@seopa.com) 2006-05-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetAllStepTrackings($quoteTypeID,$step="",$startDate,$endDate,&$arrayResult)
{

   if(! preg_match("/^\d+$/", $quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_QUITE_TYPE_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_STEP_TRACKINGS."  WHERE 1 ";

   $this->lastSQLCMD .= " AND quote_type_id='$quoteTypeID'";

   if($startDate)
      $this->lastSQLCMD .= " AND date>='$startDate'";

   if($endDate)
      $this->lastSQLCMD .= " AND date<='$endDate'";

   if(! empty($step))
      $this->lastSQLCMD .= " AND step='$step'";


   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("STEP_TRACKING_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $arrayResult[$this->dbh->GetFieldValue("id")]["step"]             = $this->dbh->GetFieldValue("step");
      $arrayResult[$this->dbh->GetFieldValue("id")]["quoteTypeID"]      = $this->dbh->GetFieldValue("quote_type_id");
      $arrayResult[$this->dbh->GetFieldValue("id")]["hostIP"]           = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$this->dbh->GetFieldValue("id")]["date"]             = $this->dbh->GetFieldValue("date");
      $arrayResult[$this->dbh->GetFieldValue("id")]["time"]             = $this->dbh->GetFieldValue("time");

   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SaveSessionFile
//
// [DESCRIPTION]:   Put the data from session into a file
//
// [PARAMETERS]:    void
//
// [RETURN VALUE]:  void
//
// [CREATED BY]:    Gigi Ciciu (null@seopa.com) 2006-05-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SaveSessionFile($id)
{
   if(! preg_match("/^\d+$/", $id))
   {
      $this->strERR = GetErrorString("INVALID_STEP_TRACKING_ID_FIELD");
      return false;
   }

   $data = var_export($_SESSION, true);
   
   if(! $compressedData = $this->CompressData($data,'bzip2','9'))
      return false;

   $compressedData = mysql_escape_string($compressedData);

   $this->lastSQLCMD = "UPDATE ".SQL_STEP_TRACKINGS." SET filecontent='$compressedData',compression_type='bzip2',compression_level='9' WHERE id='$id' ";
   
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: RemoveSessionFile
//
// [DESCRIPTION]:   remove a file
//
// [PARAMETERS]:    $filename
//
// [RETURN VALUE]:   or false in case of failure otherwise true
//
// [CREATED BY]:    Gigi Ciciu (null@seopa.com) 2006-05-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function RemoveSessionFile($id)
{
   if(! preg_match("/^\d+$/", $id))
   {
      $this->strERR = GetErrorString("INVALID_STEP_TRACKING_ID_FIELD");
      return false;
   }
   
   $this->lastSQLCMD = "DELETE FROM ".SQL_STEP_TRACKINGS." WHERE id='$id' ";
   
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }
   
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Gigi Ciciu (null@seopa.com) 2006-05-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetError()
{
	return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DecompressData($data="")
//
// [DESCRIPTION]:   Decompreses data
//
// [PARAMETERS]:    $data="",$compressType="9"
//
// [RETURN VALUE]:  Error if decompressfails, decompressed data otherwise.
//
// [CREATED BY]:    Gabriel Istvancsek (null@seopa.com) 2007-06-04
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DecompressData($data="", $compressType="bzip2")
{

   switch($compressType)
   {
      case "bzip2":
         $decompressedData = bzdecompress($data);
         break;

      case "gzip":
         $decompressedData = gzuncompress($data);
         break;

      default:
         $this->strERR = "INVALID_COMRESSION_TYPE";
         return false;
         break;
   }

   return $decompressedData;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CompressData($data="",$compressType="9")
//
// [DESCRIPTION]:   Compress data
//
// [PARAMETERS]:    $data="",$compressType="9"
//
// [RETURN VALUE]:  Error if compress fails, compressed data otherwise. CompressType is
//                  the compression rate (9 is the best compression but not the fastest)
//
// [CREATED BY]:    Gabriel Istvancsek (null@seopa.com) 2007-06-04
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CompressData($data="", $compressType="bzip2", $compressLevel=9)
{

   if(! preg_match("/^[0-9]$/", $compressLevel))
   {
      $this->strERR = "INVALID_COMRESS_LEVEL";
      return false;
   }

   switch($compressType)
   {
      case "bzip2":
         $compressedData = bzcompress($data, $compressLevel);
         break;

      case "gzip":
         $compressedData = gzcompress($data, $compressLevel);
         break;

      default:
         $this->strERR = "INVALID_COMRESSION_TYPE";
         return false;
         break;
   }

   return $compressedData;
}

}
?>

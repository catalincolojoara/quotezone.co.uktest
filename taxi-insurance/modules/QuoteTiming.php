<?php

//Documentation for quote_timing
/*

We have to create a quote_timing table in which we will store all the details about the quoting process
We will need status (WAITING,SUCCESS,FAILURE,NO QUOTE) and mode (NORMAL,SCANNING,DEBUG) and the time and 
date of their ocurance. We will store the quote_status_id there too so we can get a better report 
of all the times needed for a quote to arrive and other details that we might need.

table definition:
CREATE TABLE `quote_timing` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `quote_status_id` bigint(20) unsigned NOT NULL default '0',
  `status` varchar(32) NOT NULL default '',
  `mode` varchar(64) NOT NULL default '',
  `date` date default '0000-00-00',
  `time` time default '00:00:00',
  PRIMARY KEY  (`id`),
  KEY `quote_status_id` (`quote_status_id`),
  KEY `qt_date_key` (`date`),
  KEY `qt_time_key` (`time`)
) ENGINE=InnoDB


implementation : 
1. copy the QuoteTiming.php module in the main modules of the sitem
2. we need to add SQL_QUOTE_TIMING in the sql.adm.inc and sql.inc files 
   we have to associate this to the quote_timing table like so:
      define("SQL_QUOTE_TIMING", "quote_timing"); // Quote timing table

3. you have to add this in the VANEngine.php , HOMEEngine.php and other engines ot the sistems like this:

for example will take VANENgine.php


include_once "QuoteTiming.php";

//need to declare the object in the class:
class CVANEngine extends CRSEngine
{
   var $objQuoteTiming;             // Quote timing  class object

   //need to instantiate it in the constructor
   function CVANEngine($siteName = "", $fileName = "", $scanningFlag = 0, $debugMode = 0)
   {
      $this->objQuoteTiming             = new CQuoteTiming($this->dbh);
   }

   //when all sites start we have to add them in the db as waiting:
   function FetchProcessInit()
   {
      $this->objQuoteTiming->AddQuoteTiming($this->quoteStatusID, "WAITING", "NORMAL");
   }

   //we need to update it when the process has finished (SUCCESS,FAILURE)
   function UpdateQuoteStatusToDB($status)
   {
      $this->objQuoteTiming->AddQuoteTiming($this->quoteStatusID, $status, $type);
   }

}

have fun!!!

*/
//

/*****************************************************************************/
/*                                                                           */
/*  CQuoteTiming class interface                                             */
/*                                                                           */
/*  (C) 2008 Furtuna C. Alexandru (alexandru.furtuna@seopa.com)                           */
/*                                                                           */
/*****************************************************************************/

define("Status_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuoteTiming
//
// [DESCRIPTION]:  CQuoteTiming class interface
//
// [FUNCTIONS]:      true   | false AssertQuoteTiming($quoteStatusID='', $status='', $mode='', $date='',$time='')
//                   string | false StdToMysqlDate($date='')
//                   int    | false AddQuoteTiming($quoteStatusID='', $status='SUCCESS', $mode='NORMAL', $date='',$time='')
//                   true   | false ExistQuoteTiming($quoteStatusID='', $status='', $mode='')
//                   array  | false GetAllQuoteTiming()
// 
//                   Close()
//                   GetError()
//                   ShowError()
//
// [CREATED BY]:   Furtuna C. Alexandru (alexandru.furtuna@seopa.com) 2008-03-03
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CQuoteTiming
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last Status error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuoteTiming
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna C. Alexandru (alexandru.furtuna@seopa.com) 2008-03-03
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQuoteTiming($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuoteTiming
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    $quoteStatusID='', $status='', $mode='', $date='',$time=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Furtuna C. Alexandru (alexandru.furtuna@seopa.com) 2008-03-03
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertQuoteTiming($quoteStatusID='', $status='', $mode='', $date='',$time='')
{
   $this->strERR = '';

   if(! preg_match("/\d+/",$quoteStatusID))
      $this->strERR  = GetErrorString("INVALID_QUOTE_TIMING_QUOTE_STATUS_ID_FIELD");

   if(empty($status))
      $this->strERR .= GetErrorString("INVALID_QUOTE_TIMING_STATUS_FIELD");

   if(empty($mode))
      $this->strERR .= GetErrorString("INVALID_QUOTE_TIMING_MODE_FIELD");

   if(! empty($date))
   {
      if(! preg_match("/\d{2}\/\d{2}\/\d{4}/i",$date))
         $this->strERR .= GetErrorString("INVALID_QUOTE_TIMING_DATE_FIELD");
   }

   if(! empty($time))
   {
      if(! preg_match("/\d{2}\:\d{2}\:\d{2}/i",$time))
         $this->strERR .= GetErrorString("INVALID_QUOTE_TIMING_TIME_FIELD");
   }

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StdToMysqlDate
//
// [DESCRIPTION]:   Transform the date into MySql date format
//
// [PARAMETERS]:    $date : dd/mm/yyyy
//
// [RETURN VALUE]:  $date : yyyy-mm-dd
//
// [CREATED BY]:    Istvancsek Gabriel(null@seopa.com) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StdToMysqlDate($date='')
{
   $this->strERR = '';

   if(! preg_match("/\d{2}\/\d{2}\/\d{4}/i",$date))
      $this->strERR = GetErrorString("INVALID_QUOTE_STATUS_STD_TO_MYSQL_DATE")."\n";

   if(! empty($this->strERR))
      return false;
   
   list($day,$month,$year) = split("/",$date);
   $date = "$year-$month-$day";

   return $date;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteTiming
//
// [DESCRIPTION]:   Add new entry to the quote_timing table
//
// [PARAMETERS]:    $quoteStatusID='', $status='', $mode='', $date='',$time=''
//
// [RETURN VALUE]:  int or false
//
// [CREATED BY]:    Furtuna C. Alexandru (alexandru.furtuna@seopa.com) 2008-03-03
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteTiming($quoteStatusID='', $status='SUCCESS', $mode='NORMAL', $date='',$time='')
{
   if(! $this->AssertQuoteTiming($quoteStatusID, $status, $mode, $date, $time))
      return false;

   if( $this-> ExistQuoteTiming($quoteStatusID, $status, $mode))
      return false;

   if(empty($date) && empty($time))
   {
      $date= date("Y-m-d");
      $time= date("G:i:s");
   }

   $this->lastSQLCMD = "INSERT INTO ".SQL_QUOTE_TIMING." (`quote_status_id`,`status`,`mode`,`date`,`time`) VALUES ('$quoteStatusID', '$status', '$mode', '$date', '$time')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ExistQuoteTimingSite
//
// [DESCRIPTION]:   check if exist the quote_timing  exists in the table
//
// [PARAMETERS]:    $quoteStatusID='', $status='', $mode=''
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Furtuna C. Alexandru (alexandru.furtuna@seopa.com) 2008-03-03
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ExistQuoteTiming($quoteStatusID='', $status='', $mode='')
{

   if(! $this->AssertQuoteTiming($quoteStatusID, $status, $mode))
      return false;

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_TIMING." WHERE `quote_status_id`='$quoteStatusID' AND status='$status' AND mode='$mode'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna C. Alexandru (alexandru.furtuna@seopa.com) 2008-03-03
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel acrux.biz) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}
} // end of CQuoteTiming class









?>
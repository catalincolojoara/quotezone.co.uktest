<?php

/*****************************************************************************/
/*                                                                           */
/*  CScanning class interface
/*                                                                           */
/*  (C) 2005 Istvancsek Gabriel (null@seopa.com)                                     */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);
//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuoteScanning
//
// [DESCRIPTION]:  CQuoteScanning class interface
//
// [FUNCTIONS]:    int  AddQuoteScanning($quoteStatusID='', $status='ON' ,$date='now()', $time='now()')
//                 bool UpdateQuoteScanning($quoteStatusID='',$status='')
//                 bool DeleteQuoteScanning($scanningID='')
//                 bool GetAllQuoteScannings()
//                 bool GetAllSitesQuoteScanning()
//                 bool GetQuoteScanning($quoteStatusID='')
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Gabi Istvancsek (null@seopa.com) 2005-05-13
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CQuoteScanning
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuoteScanning
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function CQuoteScanning($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteScanning
//
// [DESCRIPTION]:   Add new entry to the quote_scannings table table
//
// [PARAMETERS]:    $quoteStatusID, $status ,$date, $time
//
// [RETURN VALUE]:  integer | false
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-09-27
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function AddQuoteScanning($quoteStatusID='', $status='ON' ,$date='now()', $time='now()')
{

   if(! preg_match("/^\d+$/", $quoteStatusID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_SCANNIG_QUOTE_STATUS_ID_FIELD");
      return false;
   }

   // check if filname and site id  exists
   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_SCANNINGS." WHERE quote_status_id='$quoteStatusID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if($this->dbh->FetchRows())
      return true;

   $this->lastSQLCMD = "INSERT INTO ".SQL_QUOTE_SCANNINGS." (quote_status_id,status,date,time,tries) VALUES ('$quoteStatusID','$status',$date,$time,'0')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQuoteScanning
//
// [DESCRIPTION]:   Update an entry from scanning table
//
// [PARAMETERS]:    $quoteStatusID,$status
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-05-13
//
// [MODIFIED]:      - [programmer (email) date]/*   if(! $email->GetAllScannings($resScannings))
//
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQuoteScanning($quoteStatusID='',$status='')
{

   if(! preg_match("/^\d+$/", $quoteStatusID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_SCANNIG_QUOTE_STATUS_ID_FIELD");
      return false;
   }

   if(empty($status))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_SCANNIG_STATUS_FIELD");
      return false;
   }

   // check if filname and site id  exists
   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_SCANNINGS." WHERE quote_status_id='$quoteStatusID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_SCANNING_QUOTE_STATUS_ID");
      return false;
   }

   $numberOfTry = $this->dbh->GetFieldValue("tries");

   $numberOfTry++;

   // limit of quote scannings
   if($numberOfTry >= 8)
      $status = 'OFF';

   $this->lastSQLCMD = "UPDATE ".SQL_QUOTE_SCANNINGS." SET status='$status',tries='$numberOfTry',date=now(),time=now() WHERE quote_status_id='$quoteStatusID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQuoteScanning
//
// [DESCRIPTION]:   Delete an entry from news_letter table
//
// [PARAMETERS]:    $scanningID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-05-13
//
// [MODIFIED]:      - [programmer (email) date]/*   if(! $email->GetAllScannings($resScannings))
//
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function DeleteQuoteScanning($scanningID='')
{
   if(! preg_match("/^\d+$/", $scanningID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_SCANNING_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTE_SCANNINGS." WHERE id='$scanningID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_SCANNING_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_QUOTE_SCANNINGS." WHERE id='$scanningID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQuoteScannings
//
// [DESCRIPTION]:   Read data from news_letter table and put it into an array variable
//                  key = emailID, value = array ("name"  => name
//                                                  "email" => email)
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQuoteScannings()
{
   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_SCANNINGS."";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SCANNINGS_NOT_FOUND");
      return false;
   }

   $index = 0;
   while($this->dbh->MoveNext())
   {
      $index++;

      $arrayResult[$index]["quote_status_id"] = $this->dbh->GetFieldValue("quote_status_id");
      $arrayResult[$index]["status"]          = $this->dbh->GetFieldValue("status");
      $arrayResult[$index]["tries"]           = $this->dbh->GetFieldValue("tries");
      $arrayResult[$index]["date"]            = $this->dbh->GetFieldValue("date");
      $arrayResult[$index]["time"]            = $this->dbh->GetFieldValue("time");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllSitesQuoteScanning
//
// [DESCRIPTION]:   Read data from news_letter table and put it into an array variable
//                  key = emailID, value = array ("name"  => name
//                                                  "email" => email)
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllSitesQuoteScanning()
{
   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_SCANNINGS." where status='ON' ORDER BY tries limit 1";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTE_SCANNING_ID_NOT_FOUND");
      return false;
   }

   $quoteStatusID = $this->dbh->GetFieldValue("quote_status_id");

   $this->lastSQLCMD = "SELECT l.filename,qs.site_id FROM ".SQL_LOGS." l,".SQL_QUOTE_STATUS." qs WHERE qs.id='$quoteStatusID' AND qs.log_id=l.id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTE_SCANNING_ID_NOT_FOUND");
      return false;
   }

   $arrayResult["site_id"]    = $this->dbh->GetFieldValue("site_id");
   $arrayResult["filename"]   = $this->dbh->GetFieldValue("filename");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSiteQuoteScanning
//
// [DESCRIPTION]:
//
// [PARAMETERS]:    $siteID
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSiteQuoteScanning($siteID = '')
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_SCANNIG_SITE_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT l.filename,qs.site_id,qsc.quote_status_id,qsc.date,qsc.time,qsc.tries FROM ".SQL_LOGS." l,".SQL_QUOTE_STATUS." qs,".SQL_QUOTE_SCANNINGS." qsc WHERE qsc.status='ON' AND qs.id=qsc.quote_status_id AND (qs.status='FAILURE' OR qs.status='NO QUOTE') AND qs.site_id='$siteID' AND qs.log_id=l.id ORDER by qsc.tries ASC, qsc.date DESC,qsc.time DESC";


   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_SCANNING_ID_NOT_FOUND");
      return false;
   }

   $index = 0;

   while($this->dbh->MoveNext())
   {
      $index++;

      $arrayResult[$index]["quote_status_id"] = $this->dbh->GetFieldValue("quote_status_id");
      $arrayResult[$index]["site_id"]         = $this->dbh->GetFieldValue("site_id");
      $arrayResult[$index]["filename"]        = $this->dbh->GetFieldValue("filename");
      $arrayResult[$index]["tries"]           = $this->dbh->GetFieldValue("tries");
      $arrayResult[$index]["date"]            = $this->dbh->GetFieldValue("date");
      $arrayResult[$index]["time"]            = $this->dbh->GetFieldValue("time");
   }

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteScanning
//
// [DESCRIPTION]:   Read data from news_letter table and put it into an array variable
//
// [PARAMETERS]:    $quoteStatusID=''
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteScanning($quoteStatusID='')
{
   if(! preg_match("/^\d+$/", $quoteStatusID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_SCANNING_QUOTE_STATUS_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_SCANNINGS." WHERE quote_status_id='$quoteStatusID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTE_SCANNING_ID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]              = $this->dbh->GetFieldValue("id");
   $arrayResult["quote_status_id"] = $this->dbh->GetFieldValue("quote_status_id");
   $arrayResult["status"]          = $this->dbh->GetFieldValue("status");
   $arrayResult["date"]            = $this->dbh->GetFieldValue("date");
   $arrayResult["time"]            = $this->dbh->GetFieldValue("time");
   $arrayResult["tries"]           = $this->dbh->GetFieldValue("tries");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteScanning
//
// [DESCRIPTION]:   Read data from news_letter table and put it into an array variable
//
// [PARAMETERS]:    $quoteStatusID=''
//
// [RETURN VALUE]:  array | false
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckQuoteScanning()
{
   $timeStamp = mktime() - 60 * 60;

   $date = date("Y-m-d", $timeStamp);
   $time = date("H:i:s", $timeStamp);

   // check some lost quote scannings from table and if exist put awway setting on falg OFF
   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_SCANNINGS." LEFT JOIN ".SQL_QUOTE_STATUS." ON ".SQL_QUOTE_STATUS.".id=".SQL_QUOTE_SCANNINGS.".quote_status_id LEFT JOIN ".SQL_LOGS." ON ".SQL_LOGS.".id=".SQL_QUOTE_STATUS.".log_id WHERE ((".SQL_QUOTE_SCANNINGS.".date='$date' AND ".SQL_QUOTE_SCANNINGS.".time<='$time') OR ".SQL_QUOTE_SCANNINGS.".date < '$date')  AND ".SQL_QUOTE_SCANNINGS.".tries='0' AND ".SQL_QUOTE_SCANNINGS.".STATUS='ON'";
 
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();

     return false;
   }

   if($this->dbh->GetRows())
   {
      $i = 0;
      while($this->dbh->MoveNext())
      {
         $i++;
         $arrayResult[$i]['log_id']             = $this->dbh->GetFieldValue("log_id");
         $arrayResult[$i]['quote_status_id']    = $this->dbh->GetFieldValue("quote_status_id");
         $arrayResult[$i]['filename']           = $this->dbh->GetFieldValue("filename");
         $arrayResult[$i]['site_id']            = $this->dbh->GetFieldValue("site_id");
         $arrayResult[$i]['host_ip']            = $this->dbh->GetFieldValue("host_ip");
      }

      // update extra entries set on OFF flag to not be scanned infinetly
       $this->lastSQLCMD = "UPDATE ".SQL_QUOTE_SCANNINGS." SET status='OFF' WHERE (date='$date' AND time<='$time') OR date < '$date'  AND tries='0' AND status='ON'";

      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      return $arrayResult;
   }

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

}// end of CScanning class

?>

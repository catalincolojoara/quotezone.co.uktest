<?php

/*****************************************************************************/
/*                                                                           */
/*  CSite class interface
/*                                                                           */
/*  (C) 2004 Eugen Savin (seugen@abc.ro)                                     */
/*                                                                           */
/*****************************************************************************/

define("SITE_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

//if(DEBUG_MODE)
//   error_reporting(1);
//else
//   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CSite
//
// [DESCRIPTION]:  CSite class interface
//
// [FUNCTIONS]:    int  AddSite($name='', $proto='HTTP', $quoteTypeID='');
//                 bool UpdateSite($siteID=0, $name='', $proto='');
//                 bool DeleteSite($siteID=0);
//                 bool GetSite($siteID=0);
//                 bool GetAllSites();
//
//                 Close();
//                 GetError();
//                 ShowError();
//
// [CREATED BY]:   Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CSite
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CSite
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CSite($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddSite
//
// [DESCRIPTION]:   Add new entry to the sites table
//
// [PARAMETERS]:    $name='', $proto=''
//
// [RETURN VALUE]:  siteID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddSite($name='', $proto='HTTP', $quoteTypeID=0, $description="", $broker="", $results="", $returns="", $masterSiteID="", $status="")
{
   if(empty($name))
   {
      $this->strERR = GetErrorString("INVALID_SITE_NAME_FIELD");
      return 0;
   }

   if(empty($proto))
   {
      $this->strERR = GetErrorString("INVALID_SITE_PROTO_FIELD");
      return 0;
   }

   if(! preg_match("/\d+/",$quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_SITE_QUOTE_TYPE_ID_FIELD");
      return 0;
   }

   $description = trim($description);
   $broker      = trim($broker);

   if(! preg_match("/\d+/",$results))
   {
      $this->strERR = GetErrorString("INVALID_SITE_RESULTS_FIELD");
      return 0;
   }

   if(! preg_match("/\d+/",$masterSiteID))
   {
      $this->strERR = GetErrorString("INVALID_SITE_MASTER_SITE_ID_FIELD");
      return 0;
   }

   if(! preg_match("/ON|OFF/",$status))
   {
      $this->strERR = GetErrorString("INVALID_SITE_STATUS_FIELD");
      return 0;
   }

   if(! preg_match("/YES|NO/",$returns))
   {
      $this->strERR = GetErrorString("INVALID_SITE_RETURN_FIELD");
      return 0;
   }

   $this->lastSQLCMD = "INSERT INTO ".SQL_SITES." (name,proto,quote_type_id,description,broker,results,returns,master_site_id,status) VALUES ('$name','$proto','$quoteTypeID','$descirption','$broker','$results','$returns','$masterSiteID','$status')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateSite
//
// [DESCRIPTION]:   Update sites table
//
// [PARAMETERS]:    $siteID=0, $name='', $proto=''
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateSite($siteID=0, $name='', $proto='', $quoteTypeID=0, $description="", $broker="", $results="", $returns="", $masterSiteID="", $status="")
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITEID_FIELD");
      return false;
   }

   if(empty($name))
   {
      $this->strERR = GetErrorString("INVALID_SITE_NAME_FIELD");
      return false;
   }

   if(empty($proto))
   {
      $this->strERR = GetErrorString("INVALID_SITE_PROTO_FIELD");
      return false;
   }

   if(! preg_match("/\d+/",$quoteTypeID))
   {
      $this->strERR = GetErrorString("INVALID_SITE_QUOTE_TYPE_ID_FIELD");
      return 0;
   }
   
   $description = trim($description);
   $broker      = trim($broker);

   if(! preg_match("/\d+/",$results))
   {
      $this->strERR = GetErrorString("INVALID_SITE_RESULTS_FIELD");
      return 0;
   }

   if(! preg_match("/\d+/",$masterSiteID))
   {
      $this->strERR = GetErrorString("INVALID_SITE_MASTER_SITE_ID_FIELD");
      return 0;
   }

   if(! preg_match("/ON|OFF/",$status))
   {
      $this->strERR = GetErrorString("INVALID_SITE_STATUS_FIELD");
      return 0;
   }

   if(! preg_match("/YES|NO/",$returns))
   {
      $this->strERR = GetErrorString("INVALID_SITE_RETURN_FIELD");
      return 0;
   }

   // check if siteID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_SITES." WHERE id='$siteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITEID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_SITES." SET name='$name',proto='$proto',quote_type_id='$quoteTypeID',description='$description',broker='$broker',results='$results',returns='$returns',master_site_id='$masterSiteID',status='$status' WHERE id='$siteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteSite
//
// [DESCRIPTION]:   Delete an entry from sites table
//
// [PARAMETERS]:    $siteID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteSite($siteID=0)
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITEID_FIELD");
      return false;
   }

   // check if siteID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_SITES." WHERE id='$siteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITEID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_SITES." WHERE id='$siteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSite
//
// [DESCRIPTION]:   Read data from sites table and put it into an array variable
//
// [PARAMETERS]:    $siteID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSite($siteID=0)
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITEID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_SITES." WHERE id='$siteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("SITEID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]             = $this->dbh->GetFieldValue("id");
   $arrayResult["name"]           = $this->dbh->GetFieldValue("name");
   $arrayResult["proto"]          = $this->dbh->GetFieldValue("proto");
   $arrayResult["quote_type_id"]  = $this->dbh->GetFieldValue("quote_type_id");
   $arrayResult["description"]    = $this->dbh->GetFieldValue("description");
   $arrayResult["results"]        = $this->dbh->GetFieldValue("results");
   $arrayResult["master_site_id"] = $this->dbh->GetFieldValue("master_site_id");
   $arrayResult["status"]         = $this->dbh->GetFieldValue("status");
   $arrayResult["returns"]        = $this->dbh->GetFieldValue("returns");
   $arrayResult["broker"]         = $this->dbh->GetFieldValue("broker");
   $arrayResult["lead_price"]     = $this->dbh->GetFieldValue("lead_price");
   $arrayResult["esv_price"]      = $this->dbh->GetFieldValue("esv_price");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSiteByName
//
// [DESCRIPTION]:   Read data from sites table and put it into an array variable
//
// [PARAMETERS]:    $siteName
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Ciprian Sturza (ciprian.sturza@seopa.com) 2007-02-15
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSiteByName($siteName="")
{
   $this->lastSQLCMD = "SELECT * FROM ".SQL_SITES." WHERE name='$siteName'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITENAME_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $id                                 = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["id"]             = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["name"]           = $this->dbh->GetFieldValue("name");
      $arrayResult[$id]["proto"]          = $this->dbh->GetFieldValue("proto");
      $arrayResult[$id]["quote_type_id"]  = $this->dbh->GetFieldValue("quote_type_id");
      $arrayResult[$id]["description"]    = $this->dbh->GetFieldValue("description");
      $arrayResult[$id]["results"]        = $this->dbh->GetFieldValue("results");
      $arrayResult[$id]["master_site_id"] = $this->dbh->GetFieldValue("master_site_id");
      $arrayResult[$id]["status"]         = $this->dbh->GetFieldValue("status");
      $arrayResult[$id]["returns"]        = $this->dbh->GetFieldValue("returns");
      $arrayResult[$id]["broker"]         = $this->dbh->GetFieldValue("broker");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SearchSiteByName
//
// [DESCRIPTION]:   Read data from sites table and put it into an array variable
//
// [PARAMETERS]:    $siteName="", $limit=0, $offset=0
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Ciprian Sturza (ciprian.sturza@seopa.com) 2007-02-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SearchSiteByName($siteName="", $limit=0, $offset=0)
{
   $this->lastSQLCMD = "SELECT s.*, qz.name AS type FROM ". SQL_SITES." s, ".SQL_QZQUOTE_TYPES." qz WHERE s.quote_type_id=qz.id AND s.name LIKE '%$siteName%' ORDER BY s.name";

   if($limit)
   {
      if($offset)
         $sqlCmd .= " LIMIT $offset,$limit";
      else
         $sqlCmd .= " LIMIT $limit";
   }

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITENAME_NOT_FOUND");
      return false;
   }

    while($this->dbh->MoveNext())
   {
      $id                                  = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["id"]              = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["name"]            = $this->dbh->GetFieldValue("name");
      $arrayResult[$id]["proto"]           = $this->dbh->GetFieldValue("proto");
      $arrayResult[$id]["description"]     = $this->dbh->GetFieldValue("description");
      $arrayResult[$id]["results"]         = $this->dbh->GetFieldValue("results");
      $arrayResult[$id]["master_site_id"]  = $this->dbh->GetFieldValue("master_site_id");
      $arrayResult[$id]["status"]          = $this->dbh->GetFieldValue("status");
      $arrayResult[$id]["type"]            = $this->dbh->GetFieldValue("type");
      $arrayResult[$id]["returns"]         = $this->dbh->GetFieldValue("returns");
      $arrayResult[$id]["broker"]          = $this->dbh->GetFieldValue("broker");
   }

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllSites
//
// [DESCRIPTION]:   Read data from sites table and put it into an array variable
//                  key = siteID, value = siteName
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllSites($quoteTypeID='')
{
   if(! empty($quoteTypeID))
      if(! preg_match("/^\d+$/",$quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_SITEID_FIELD");
         return false;
      }

   $this->lastSQLCMD = "SELECT id,name FROM ".SQL_SITES." WHERE 1 ";

   if(! empty($quoteTypeID))
      $this->lastSQLCMD.= " AND quote_type_id='$quoteTypeID'";
   
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITES_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("name");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllSites
//
// [DESCRIPTION]:   Read data from sites table and put it into an array variable
//                  key = siteID, value = siteName
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllActiveSlaveSites($masterSiteID='')
{
   if(! preg_match("/^\d+$/",$masterSiteID))
   {
      $this->strERR = GetErrorString("INVALID_MASTER_SITE_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id,broker,results FROM ".SQL_SITES." WHERE status='ON' AND master_site_id='$masterSiteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITES_NOT_FOUND");
      return false;
   }

   $counter = 0;
   while($this->dbh->MoveNext())
   {
      $counter++;
      $arrayResult[$counter]['id']      = $this->dbh->GetFieldValue("id");
      $arrayResult[$counter]['broker']  = $this->dbh->GetFieldValue("broker");
      $arrayResult[$counter]['results'] = $this->dbh->GetFieldValue("results");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetBrokerSites
//
// [DESCRIPTION]:   Read data from sites table and put it into an array variable
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Ciprian STURZA (ciprian.sturza@seopa.com) 2010-04-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetBrokerSites($brokerID='', $quoteTypeID='')
{
   if(! empty($quoteTypeID))
      if(! preg_match("/^\d+$/",$quoteTypeID))
      {
         $this->strERR = GetErrorString("INVALID_SITEID_FIELD");
         return false;
      }

   if(! empty($brokerID))
      if(! preg_match("/^\d+$/",$brokerID))
      {
         $this->strERR = GetErrorString("INVALID_BROKERID_FIELD");
         return false;
      }

   $this->lastSQLCMD = "SELECT id,name FROM ".SQL_SITES." WHERE 1 ";

   if(! empty($brokerID))
      $this->lastSQLCMD.= " AND broker_id='$brokerID'";

   if(! empty($quoteTypeID))
      $this->lastSQLCMD.= " AND quote_type_id='$quoteTypeID'";

   $this->lastSQLCMD.= "ORDER BY id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITES_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("name");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllSlaveSites
//
// [DESCRIPTION]:   Read data from sites table and put it into an array variable
//                  key = siteID, value = siteName
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllSlaveSites($masterSiteID='')
{
   if(! preg_match("/^\d+$/",$masterSiteID))
   {
      $this->strERR = GetErrorString("INVALID_MASTER_SITE_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id,broker,results FROM ".SQL_SITES." WHERE master_site_id='$masterSiteID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITES_NOT_FOUND");
      return false;
   }

   $counter = 0;
   while($this->dbh->MoveNext())
   {
      $counter++;
      $arrayResult[$counter]['id']      = $this->dbh->GetFieldValue("id");
      $arrayResult[$counter]['broker']  = $this->dbh->GetFieldValue("broker");
      $arrayResult[$counter]['results'] = $this->dbh->GetFieldValue("results");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Print the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

} // end of CSite class
?>

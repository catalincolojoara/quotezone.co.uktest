<?php
/*****************************************************************************/
/*                                                                           */
/*  CCustomer class interface                                              */
/*                                                                           */
/*  (C) 2005 Istvancsek Gabriel (null@seopa.com)                              */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CCustomer
//
// [DESCRIPTION]:  CCustomer class interface
//
// [FUNCTIONS]:    
//
//                 Close();
//                 GetError();
//                 ShowError();
//
// [CREATED BY]:   Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CCustomer
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CCustomer
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CCustomer($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuoteUser
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    &$firstName, &$lastName, $birthDate='', $password=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertRenewal($emStoreID, $type, $date)
{
   $this->strERR = '';

   if(! preg_match('/\d+/i',$emStoreID))
      $this->strERR .= GetErrorString("INVALID_USER_ID_FIELD");

   if(! preg_match('/\d+/i',$type))
      $this->strERR .= GetErrorString("INVALID_TYPE_FIELD");

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$date))
      $this->strERR .= GetErrorString("INVALID_DATE_FIELD")."\n";

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddRenewal
//
// [DESCRIPTION]:   Check if exist a user and add if not exist into renewal table
//
// [PARAMETERS]:    $emStoreID, $crDate
//
// [RETURN VALUE]:  $renewalID | false
//
// [CREATED BY]:    Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddCustomer($emStoreID, $type, $date, $time)
{
//print "$emStoreID $type $date $time";
   if(! preg_match('/\d+/i',$emStoreID))
   {
      $this->strERR .= GetErrorString("INVALID_EM_STORE_ID_FIELD");
      return false;
   }

   if(! preg_match('/\d+/i',$type))
   {
      $this->strERR .= GetErrorString("INVALID_TYPE_FIELD");
      return false;
   }

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$date))
   {
      $this->strERR .= GetErrorString("INVALID_DATE_FIELD")."\n";
      return false;
   }
   
   $this->lastSQLCMD = "SELECT id FROM customer WHERE em_store_id='$emStoreID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
      return false;
   }

   if($this->dbh->GetRows())
   {
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      return $this->dbh->GetFieldValue("id");
   }

   $this->lastSQLCMD = "INSERT INTO customer (em_store_id,type,date,time) VALUES ('$emStoreID','$type','$date','$time')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmailStoreIDByE
//
// [DESCRIPTION]:   Get a id record by email
//
// [PARAMETERS]:    $email=''
//
// [RETURN VALUE]:  $arrayResult["id"]
//
// [CREATED BY]:    Florin (null@seopa.com) 15-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCustomerIDByEmStoreID($emStoreID='', $type='')
{
	if(!preg_match('/\d+/i',$emStoreID))
	{
		$this->strERR = GetErrorString("INVALID_EM_STORE_ID_FIELD");
		return false;
	}

	$this->lastSQLCMD = "SELECT * FROM customer WHERE em_store_id='$emStoreID'";

        if ($type)
                $this->lastSQLCMD .= " and type='$type'";


	if(! $this->dbh->Exec($this->lastSQLCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	if(! $this->dbh->FetchRows())
	{
		$this->strERR = GetErrorString("USER_NOT_FOUND");
		return false;
	}

	$id = $this->dbh->GetFieldValue("id");

	return $id;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmailStoreIDByE
//
// [DESCRIPTION]:   Get a id record by email
//
// [PARAMETERS]:    $email=''
//
// [RETURN VALUE]:  $arrayResult["id"]
//
// [CREATED BY]:    Florin (null@seopa.com) 15-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCustomerByEmStoreID($emStoreID='', $type='')
{
	if(!preg_match('/\d+/i',$emStoreID))
	{
		$this->strERR = GetErrorString("INVALID_EM_STORE_ID_FIELD");
		return false;
	}

	$this->lastSQLCMD = "SELECT * FROM customer WHERE em_store_id='$emStoreID'";

	if ($type)
		$this->lastSQLCMD .= " and type='$type'";

	if(! $this->dbh->Exec($this->lastSQLCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	if(! $this->dbh->FetchRows())
	{
		$this->strERR = GetErrorString("USER_NOT_FOUND");
		return false;
	}

	$arrayResult["id"]          = $this->dbh->GetFieldValue("id");
	$arrayResult["em_store_id"] = $this->dbh->GetFieldValue("em_store_id");
	$arrayResult["type"]        = $this->dbh->GetFieldValue("type");
	$arrayResult["date"]        = $this->dbh->GetFieldValue("date");
	$arrayResult["time"]        = $this->dbh->GetFieldValue("time");
		
	return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmailStoreIDByE
//
// [DESCRIPTION]:   Get a id record by email
//
// [PARAMETERS]:    $email=''
//
// [RETURN VALUE]:  $arrayResult["id"]
//
// [CREATED BY]:    Florin (null@seopa.com) 15-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTotalCustomers($dateFrom,$dateTo)
{
	$this->lastSQLCMD = "SELECT count(*) as cnt FROM customer WHERE date>='$dateFrom' AND date<='$dateTo'";

	if(! $this->dbh->Exec($this->lastSQLCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	if(! $this->dbh->GetRows())
	{
		$this->strERR = GetErrorString("USER_NOT_FOUND");
		return false;
	}
	if(! $this->dbh->FetchRows())
	{
		$this->strERR = GetErrorString("USER_NOT_FOUND");
		return false;
	}

	$cnt = $this->dbh->GetFieldValue("cnt");		
	
	return $cnt;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmailStoreIDByE
//
// [DESCRIPTION]:   Get a id record by email
//
// [PARAMETERS]:    $email=''
//
// [RETURN VALUE]:  $arrayResult["id"]
//
// [CREATED BY]:    Florin (null@seopa.com) 15-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTotalCustomersByType($dateFrom,$dateTo,$type)
{
	$this->lastSQLCMD = "SELECT count(*) as cnt FROM customer WHERE date>='$dateFrom' AND date<='$dateTo' and type='$type'";
print $this->lastSQLCMD;
	if(! $this->dbh->Exec($this->lastSQLCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	if(! $this->dbh->GetRows())
	{
		$this->strERR = GetErrorString("USER_NOT_FOUND");
		return false;
	}
	if(! $this->dbh->FetchRows())
	{
		$this->strERR = GetErrorString("USER_NOT_FOUND");
		return false;
	}

	$cnt = $this->dbh->GetFieldValue("cnt");		
	
	return $cnt;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Show the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

}//end class CCustomer

?>

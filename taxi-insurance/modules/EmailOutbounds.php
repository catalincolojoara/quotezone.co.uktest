<?php
/*****************************************************************************/
/*                                                                           */
/*  EmailOutbounds class interface                                           */
/*                                                                           */
/*  (C) 2008 Gabriel ISTVANCSEK (null@seopa.com)                             */
/*                                                                           */
/*****************************************************************************/

include_once "MySQL.php";
include_once "sql.adm.inc";
//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CEmailOutbounds
//
// [DESCRIPTION]:  CEmailOutbounds class interface, email outbounds support,
//                 PHP version
//
// [FUNCTIONS]:    CEmailOutbounds($dbh=0)
//                 AssertEmailOutbounds($logID)
//                 AddEmailOutbounds($logID)
//                 SetEmailOutboundsStatus($logID, $status=0)
//                 SetEmailOutboundsResponse($logID, $response=0)
//                 CheckEmailOutboundsStatus($logID)
//                 CheckEmailOutboundsResponse($logID)
//                 GetError()
//                 ShowError()
//
//
//  (C) 2008 Gabriel ISTVANCSEK (null@seopa.com) 2008-02-01
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CEmailOutbounds
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last error string

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CEmailOutbounds
   //
   // [DESCRIPTION]:   Default class constructor. Initialization goes here.
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabriel ISTVANCSEK (null@seopa.com) 2000-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CEmailOutbounds($dbh=0)
   {
      if($dbh)
      {
         $this->dbh = $dbh;
         $this->closeDB = false;
      }
      else
      {
         // default configuration
         $this->dbh = new CMySQL();

         if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
         {
           $this->strERR = $this->dbh->GetError();
           return;
         }

         $this->closeDB = true;
      }

       $this->strERR  = "";
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AssertEmailOutbounds
   //
   // [DESCRIPTION]:   Verify to see if the requested fields are completed
   //
   // [PARAMETERS]:    $logID
   //
   // [RETURN VALUE]:  true or false in case of failure
   //
   // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AssertEmailOutbounds($logID)
   {
      $this->strERR = "";

      if(! preg_match("/^\d+$/", $logID))
      {
         $this->strERR .= "\n".GetErrorString("INVALID_EMAIL_OUTBOUNDS_LOG_ID");
      }

      if(! empty($this->strERR))
         return false;

      return true;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: AssertEmailOutbounds
   //
   // [DESCRIPTION]:   Verify to see if the requested fields are completed
   //
   // [PARAMETERS]:    $logID
   //
   // [RETURN VALUE]:  true or false in case of failure
   //
   // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function AddEmailOutbounds($logID)
   {
      if(! $this->AssertEmailOutbounds($logID))
         return false;

      $this->lastSQLCMD = "SELECT id FROM ".SQL_EMAIL_OUTBOUNDS." WHERE log_id='$logID'";

      if(! $this->dbh->Exec($this->lastSQLCMD,true))
      {
          $this->strERR = $this->dbh->GetError();
          return false;
      }

      // if exist return quote_details_id
      if($this->dbh->GetRows())
      {
         if(! $this->dbh->FetchRows())
         {
            $this->strERR = $this->dbh->GetError();
            return false;
         }

         return $this->dbh->GetFieldValue("id");
      }

      $this->lastSQLCMD = "INSERT INTO ".SQL_EMAIL_OUTBOUNDS." (log_id,date,time,response,status) VALUES ('$logID', now(), now(),'0','0')";

      if(! $this->dbh->Exec($this->lastSQLCMD,true))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

      if(! $this->dbh->Exec($this->lastSQLCMD,true))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      return $this->dbh->GetFieldValue("id");

   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: SetEmailOutboundsStatus
   //
   // [DESCRIPTION]:   Update email_outbounds table
   //
   // [PARAMETERS]:    $logID, $status
   //
   // [RETURN VALUE]:  true or false in case o failure
   //
   // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-14
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SetEmailOutboundsStatus($logID, $status=0)
   {
      if(! $this->AssertEmailOutbounds($logID))
         return false;

      $prepareStatus = 0;
      if($status)
         $prepareStatus = 1;

      $this->lastSQLCMD = "SELECT id FROM ".SQL_EMAIL_OUTBOUNDS." WHERE log_id='$logID' ";

      if(! $this->dbh->Exec($this->lastSQLCMD,true))
      {
        $this->strERR = $this->dbh->GetError();
        return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("EMAIL_OUTBOUNDS_ID_NOT_FOUND");
         return false;
      }

      $this->lastSQLCMD = "UPDATE ".SQL_EMAIL_OUTBOUNDS." SET status='$prepareStatus',date=now(),time=now() WHERE log_id='$logID' ";

      if(! $this->dbh->Exec($this->lastSQLCMD,true))
      {
        $this->strERR = $this->dbh->GetError();
        return false;
      }
      
      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: SetEmailOutboundsResponse
   //
   // [DESCRIPTION]:   Update email_outbounds table
   //
   // [PARAMETERS]:    $logID, $response
   //
   // [RETURN VALUE]:  true or false in case o failure
   //
   // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-14
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SetEmailOutboundsResponse($logID, $response=0)
   {
      if(! $this->AssertEmailOutbounds($logID))
         return false;

      $prepareResponse = 0;
      if($response)
         $prepareResponse = 1;

      $this->lastSQLCMD = "SELECT id FROM ".SQL_EMAIL_OUTBOUNDS." WHERE log_id='$logID' ";

      if(! $this->dbh->Exec($this->lastSQLCMD,true))
      {
        $this->strERR = $this->dbh->GetError();
        return false;
      }

      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("EMAIL_OUTBOUNDS_ID_NOT_FOUND");
         return false;
      }

      $this->lastSQLCMD = "UPDATE ".SQL_EMAIL_OUTBOUNDS." SET response='$prepareResponse',date=now(),time=now() WHERE log_id='$logID' ";

      if(! $this->dbh->Exec($this->lastSQLCMD,true))
      {
        $this->strERR = $this->dbh->GetError();
        return false;
      }

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CheckEmailOutboundsStatus
   //
   // [DESCRIPTION]:   Update email_outbounds table
   //
   // [PARAMETERS]:    $logID,  $response
   //
   // [RETURN VALUE]:  true or false in case o failure
   //
   // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-14
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CheckEmailOutboundsStatus($logID)
   {
      if(! $this->AssertEmailOutbounds($logID))
         return false;

      $this->lastSQLCMD = "SELECT status FROM ".SQL_EMAIL_OUTBOUNDS." WHERE log_id='$logID' ";

      if(! $this->dbh->Exec($this->lastSQLCMD,true))
      {
        $this->strERR = $this->dbh->GetError();
        return false;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = GetErrorString("EMAIL_OUTBOUNDS_ID_NOT_FOUND");
         return false;
      }

      return $this->dbh->GetFieldValue('status');
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CheckEmailOutboundsResponse
   //
   // [DESCRIPTION]:   Update email_outbounds table
   //
   // [PARAMETERS]:    $logID, $response
   //
   // [RETURN VALUE]:  true or false in case o failure
   //
   // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-14
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CheckEmailOutboundsResponse($logID)
   {
      if(! $this->AssertEmailOutbounds($logID))
         return false;

      $this->lastSQLCMD = "SELECT response FROM ".SQL_EMAIL_OUTBOUNDS." WHERE log_id='$logID' ";

      if(! $this->dbh->Exec($this->lastSQLCMD,true))
      {
        $this->strERR = $this->dbh->GetError();
        return false;
      }

      if(! $this->dbh->FetchRows())
      {
         $this->strERR = GetErrorString("EMAIL_OUTBOUNDS_ID_NOT_FOUND");
         return false;
      }

      return $this->dbh->GetFieldValue('response');
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetError
   //
   // [DESCRIPTION]:   Retrieve the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetError()
   {
       return $this->strERR;
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ShowError
   //
   // [DESCRIPTION]:   Print the last error message
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  Error string if there is any, empty string otherwise
   //
   // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2008-02-01
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function ShowError()
   {
       return $this->strERR;
   }
}


?>

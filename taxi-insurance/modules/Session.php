<?php
/*****************************************************************************/
/*                                                                           */
/*  CSession class interface                                                 */
/*                                                                           */
/*  (C) 2004 ISTVNACSEK Gabriel (null@seopa.com)                              */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";
define("SESSION_INCLUDED", "1");

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CSession
//
// [DESCRIPTION]:  CSession class interface
//
// [FUNCTIONS]:
//                 int  function AddSessionParam($paramName='')
//                 bool function UpdateSessionParam($paramSessionID=0, $paramName='')
//                 bool function DeleteSessionParam($paramSessionID=0)
//                 bool function GetSessionParam($paramSessionID=0, &$arrayResult)
//                 bool GetAllSessionParams(&$arrayResult)
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Gabriel ISTVNACSEK (null@seopa.com) 2004-07-07
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CSession
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last url error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CUrl
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CSession($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddSessionParam
//
// [DESCRIPTION]:   Add new entry to the session_params table
//
// [PARAMETERS]:    $paramName=''
//
// [RETURN VALUE]:  ParamID or 0 in case of failure
//
// [CREATED BY]:    Gabi ISTVNACSEK (null@seopa.com) 2004-09-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddSessionParam($paramName='')
{

   if(empty($paramName))
   {
      $this->strERR = GetErrorString("INVALID_PARAM_SESSION_NAME_FIELD");
      return 0;
   }

   // check if param session name already exists
   $this->lastSQLCMD = "SELECT name FROM ".SQL_SESSION_PARAMS." WHERE name='$paramName'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return 0;
   }

   if($this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("PARAM_SESSION_NAME_ALREADY_EXISTS");
      return 0;
   }
   // insert param session name into session_params table
   $this->lastSQLCMD = "INSERT INTO ".SQL_SESSION_PARAMS." (name) VALUES ('$paramName')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }
   // extract last insert param session id
   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateSessionParam
//
// [DESCRIPTION]:   Update session_params table
//
// [PARAMETERS]:    $paramSessionID=0, $paramName=''
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi ISTVNACSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateSessionParam($paramSessionID=0, $paramName='')
{
   if(! preg_match("/^\d+$/", $paramSessionID))
   {
      $this->strERR = GetErrorString("INVALID_PARAM_SESSION_ID_FIELD");
      return false;
   }

   if(empty($paramName))
   {
      $this->strERR = GetErrorString("INVALID_PARAM_SESSION_NAME_FIELD");
      return false;
   }

   // check if paramSessionID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_SESSION_PARAMS." WHERE id='$paramSessionID'";
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URL_PARAM_SESSION_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "SELECT name FROM ".SQL_SESSION_PARAMS." WHERE name='$paramName'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return 0;
   }

   if($this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("PARAM_SESSION_NAME_ALREADY_EXISTS");
      return 0;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_SESSION_PARAMS." SET name='$paramName' WHERE id='$paramSessionID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteSessionParam
//
// [DESCRIPTION]:   Delete an entry from session_params table
//
// [PARAMETERS]:    $paramSessionID=0
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi Istvnacsek (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteSessionParam($paramSessionID=0)
{
   if(! preg_match("/^\d+$/", $paramSessionID))
   {
      $this->strERR = GetErrorString("INVALID_PARAM_SESSION_ID_FIELD");
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("PARAM_SESSION_ID_NOT_FOUND");
      return false;
   }

   // check if paramSessionID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_SESSION_PARAMS." WHERE id='$paramSessionID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("PARAM_SESSION_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_SESSION_PARAMS." WHERE id='$paramSessionID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionParam
//
// [DESCRIPTION]:   Read data from session_params table and put it into an array variable
//
// [PARAMETERS]:    $paramSessionID=0, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi ISTVNACSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionParam($paramSessionID=0, &$arrayResult)
{
   if(! preg_match("/^\d+$/", $paramSessionID))
   {
      $this->strERR = GetErrorString("INVALID_PARAM_SESSION_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_SESSION_PARAMS." WHERE id='$paramSessionID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("PARAM_SESSION_ID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]     = $this->dbh->GetFieldValue("id");
   $arrayResult["name"]   = $this->dbh->GetFieldValue("name");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllSessionParams
//
// [DESCRIPTION]:   Read data from session_params table and put it into an array variable
//                  key = paramSessionID, value = paramSessionName
//
// [PARAMETERS]:    &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi ISTVNACSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllSessionParams(&$arrayResult)
{
   $this->lastSQLCMD = "SELECT id,name FROM ".SQL_SESSION_PARAMS."";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("PARAM_SESSION_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("name");

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionAllDriverNumber
//
// [DESCRIPTION]:   Get number of all drivers
//
// [PARAMETERS]:    $_SESSION
//
// [RETURN VALUE]:  $numberOfAllDrivers | false
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionAllDriverNumber($SESSION)
{
   $categoryName  = "_DRIVERS_";

   if(empty($SESSION[$categoryName]))
      return false;

   $numberOfAllDrivers = count($SESSION[$categoryName]);

   return $numberOfAllDrivers;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionAdditionalDriverNumber
//
// [DESCRIPTION]:   Get number of additional drivers
//
// [PARAMETERS]:    $_SESSION
//
// [RETURN VALUE]:  $numberOfAdditionalDrivers | false
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionAdditionalDriverNumber($SESSION)
{
   $categoryName  = "_DRIVERS_";
   $categoryLevel = 1;

   if(empty($SESSION[$categoryName][$categoryLevel]))
      return false;

   $numberOfAdditionalDriver = count($SESSION[$categoryName]) - 1;

   return $numberOfAdditionalDriver;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionDriver
//
// [DESCRIPTION]:   Get additional data entry by $idDriver Additional
//
// [PARAMETERS]:    $_SESSION, $idDriver
//
// [RETURN VALUE]:  an array $AdditionalDriver | false
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionDriver($SESSION, $idDriver)
{
   $categoryName  = "_DRIVERS_";
   $categoryLevel = $idDriver;

   if(empty($SESSION[$categoryName][$categoryLevel]))
      return false;

   $driver = $SESSION[$categoryName][$idDriver];

   return $driver;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionProposerDriver
//
// [DESCRIPTION]:   Get porposer data entry
//
// [PARAMETERS]:    $_SESSION
//
// [RETURN VALUE]:  an array $ProposerDriver | false
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionProposerDriver($SESSION)
{
   $categoryName  = "_DRIVERS_";
   $categoryLevel = 0;

   if(empty($SESSION[$categoryName][$categoryLevel]))
      return false;

   $ProposerDriver = $SESSION[$categoryName][$categoryLevel];

   return $ProposerDriver;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionAdditionalDriver
//
// [DESCRIPTION]:   Get additional data entry by $idDriver Additional
//
// [PARAMETERS]:    $_SESSION, $idDriver
//
// [RETURN VALUE]:  an array $AdditionalDriver | false
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionAdditionalDriver($SESSION, $idDriver)
{
   $categoryName  = "_DRIVERS_";
   $categoryLevel = $idDriver;

   if(empty($SESSION[$categoryName][$categoryLevel]))
      return false;

   $AdditionalDriver = $SESSION[$categoryName][$idDriver];

   return $AdditionalDriver;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionVehicle
//
// [DESCRIPTION]:   Get vehicle data entry
//
// [PARAMETERS]:    $_SESSION
//
// [RETURN VALUE]:  an array $vehicle | false
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionVehicle($SESSION)
{
   $categoryName  = "_VEHICLE_";

   if(empty($SESSION[$categoryName]))
      return false;

   $vehicle = $SESSION[$categoryName];

   return $vehicle;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionVehicleCode
//
// [DESCRIPTION]:   Get vehicle data entry
//
// [PARAMETERS]:    $_SESSION
//
// [RETURN VALUE]:  number | false
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionVehicleCode($SESSION)
{
   $categoryName  = "_VEHICLE_";

   if(empty($SESSION[$categoryName]))
      return false;

   $vehicleCode = $SESSION[$categoryName]['vehicle_confirm'];

   return $vehicleCode;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionCover
//
// [DESCRIPTION]:   Get cover data entry
//
// [PARAMETERS]:    $_SESSION
//
// [RETURN VALUE]:  an array $cover | false
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionCover($SESSION)
{
   $categoryName  = "_COVER_";

   if(empty($SESSION[$categoryName]))
      return false;

   $cover = $SESSION[$categoryName];

   return $cover;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionClaimsDriverNumber
//
// [DESCRIPTION]:   Get number of claims by idDriver
//
// [PARAMETERS]:    $_SESSION, $idDriver
//
// [RETURN VALUE]:  $numberOfClaimsDriver | false
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionClaimsDriverNumber($SESSION, $idDriver)
{
   $categoryName  = "_CLAIMS_";
   $categoryLevel = $idDriver;

   if(empty($SESSION[$categoryName][$categoryLevel]))
      return false;

   $numberOfClaimsDriver = count($SESSION[$categoryName][$categoryLevel]);

   return $numberOfClaimsDriver;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionAllClaimsNumber
//
// [DESCRIPTION]:   Get total number of claims
//
// [PARAMETERS]:    $_SESSION
//
// [RETURN VALUE]:  $numberOfClaimsDriver | false
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionAllClaimsNumber($SESSION)
{
   $categoryName = "_CLAIMS_";

   if(empty($SESSION[$categoryName]))
      return false;

   $totalClaims = 0;
   foreach($SESSION[$categoryName] as $idDriver => $resClaims)
   {
      $totalClaims +=count($resClaims);
   }

   return $totalClaims;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionAllConvictionsNumber
//
// [DESCRIPTION]:   Get total number of convictions
//
// [PARAMETERS]:    $_SESSION
//
// [RETURN VALUE]:  $numberOfConvictionsDriver | false
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionAllConvictionsNumber($SESSION)
{
   $categoryName = "_CONVICTIONS_";

   if(empty($SESSION[$categoryName]))
      return false;

   $totalConvictions = 0;
   foreach($SESSION[$categoryName] as $idDriver => $resConvictions)
   {
      $totalConvictions +=count($resConvictions);
   }

   return $totalConvictions;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionConvictionsDriverNumber
//
// [DESCRIPTION]:   Get number of convictions by idDriver
//
// [PARAMETERS]:    $_SESSION, $idDriver
//
// [RETURN VALUE]:  $numberOfClaimsDriver | false
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionConvictionsDriverNumber($SESSION, $idDriver)
{
   $categoryName  = "_CONVICTIONS_";
   $categoryLevel = $idDriver;

   if(empty($SESSION[$categoryName][$categoryLevel]))
      return false;

   $numberOfConvictionsDriver = count($SESSION[$categoryName][$categoryLevel]);

   return $numberOfConvictionsDriver;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionClaimsDriver
//
// [DESCRIPTION]:   Get data entry of claims by idDriver
//
// [PARAMETERS]:    $_SESSION, $idDriver
//
// [RETURN VALUE]:  an array $claimsDriver | false
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionClaimsDriver($SESSION, $idDriver)
{
   $categoryName  = "_CLAIMS_";
   $categoryLevel = $idDriver;

   if(empty($SESSION[$categoryName][$categoryLevel]))
      return false;

   $claimsDriver = $SESSION[$categoryName][$categoryLevel];

   return $claimsDriver;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionConvictionsDriver
//
// [DESCRIPTION]:   Get data entry of claims by idDriver
//
// [PARAMETERS]:    $_SESSION, $idDriver
//
// [RETURN VALUE]:  an array $convictionsDriver | false
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionConvictionsDriver($SESSION, $idDriver)
{
   $categoryName  = "_CONVICTIONS_";
   $categoryLevel = $idDriver;

   if(empty($SESSION[$categoryName][$categoryLevel]))
      return false;

   $convictionsDriver = $SESSION[$categoryName][$categoryLevel];

   return $convictionsDriver;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSessionQZQuoteDetails
//
// [DESCRIPTION]:   Get data entry of _QZ_QUOTE_DETAILS_
//
// [PARAMETERS]:    $_SESSION
//
// [RETURN VALUE]:  an array $qzQuoteDetails | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 2004-08-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSessionQZQuoteDetails($SESSION)
{
   $categoryName  = "_QZ_QUOTE_DETAILS_";

   if(empty($SESSION[$categoryName]))
      return false;

   $qzQuoteDetails = array();
   $qzQuoteDetails = $SESSION[$categoryName];

   return $qzQuoteDetails;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: HaveWomanDriver
//
// [DESCRIPTION]:   Check if we have any woman driver
//
// [PARAMETERS]:    $_SESSION
//
// [RETURN VALUE]:  women driver counter
//
// [CREATED BY]:    Eugen Savin (null@seopa.com) 2005-09-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function HaveWomanDriver($SESSION)
{
   $drivers = $SESSION['_DRIVERS_'];
   $womenCounter = 0;

   foreach($drivers as $index => $drvArray)
      if($drvArray['sex'] == 'F' || $drvArray['additional_sex'] == 'F')
         $womenCounter++;

   return $womenCounter;
}

} // end CSession
?>
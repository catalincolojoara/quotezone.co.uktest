<?php
/*****************************************************************************/
/*                                                                           */
/*  CScanner class interface                                                 */
/*                                                                           */
/*  (C) Eugen Savin (null@seopa.com) 2005-10-19                            */
/*                                                                           */
/*****************************************************************************/
define("SCANNER_INCLUDED", "1");
include_once "File.php";
include_once "QuoteScanning.php";
include_once "QuoteStatus.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CScanner
//
// [DESCRIPTION]:  CScanner class interface
//
// [FUNCTIONS]:    void  SetOfflineHours($startHour, $endHour);
//                 bool  IsOfflineHours();
//                 void  SetDelayScanning($delayScanning = 0);
//                 int   GetDelayScanning();
//                 void  StartDaemon();
//                 void  StopDaemon();
//                 void  MarkAlive();
//                 bool  IsAlive();
//
// [CREATED BY]:   Eugen Savin (null@seopa.com) 2005-10-19
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CScanner
{
    var $siteID;
    var $lockFileName;
    var $stopFileName;
    var $dbhRead;
    var $dbhWrite;
    var $dirIn;
    var $objFile;
    var $lastTimeRun;
    var $delayScanning;
    var $startOfflineHour;
    var $endOfflineOfHour;

    var $strErr;
    var $engineSite;

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CScanner
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2005-10-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CScanner($siteID)
{
   $this->siteID               = $siteID;
   $this->lockFileName         = "scanner/scanner_$siteID.lock";
   $this->stopFileName         = "scanner/scanner_$siteID.stop";

   // read database qz2
   $this->dbhRead              = '';
   // write database qz1
   $this->dbhWrite             = '';

   // dir in
   $this->dirIn                = '';

   $this->objFile              = new CFile();
   $this->lastTimeRun          = 0;
   $this->delayScanning        = 10; //seconds

   $this->startOfflineHour     = '00:00:00';
   $this->numberOfOfflineHours = '0';

   $this->strErr               = '';
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetOfflineHours
//
// [DESCRIPTION]:   Specify the interval hours when the site is offline
//
// [PARAMETERS]:    startHour, numberOfHours
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2005-10-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetOfflineHours($startOfflineOfHour='', $endOfflineOfHour='')
{
   if(! preg_match("/\d{2}:\d{2}:\d{2}/", $startOfflineOfHour))
   {
      $this->strErr = 'INVALID_START_OFFLINE_HOUR';
      return false;
   }

   if(! preg_match("/\d{2}:\d{2}:\d{2}/", $endOfflineOfHour))
   {
      $this->strErr = 'INVALID_END_OFFLINE_HOUR';
      return false;
   }

   $this->startOfflineHour   = $startOfflineHour;
   $this->endOfflineHour     = $endOfflineHour;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: IsOfflineHours
//
// [DESCRIPTION]:   Check if the current time is in the offline interval
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-10-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function IsOfflineHours()
{
   list($startHour, $startMinute, $startSecond) = split(":", $this->startOfflineHour);
   list($endHour, $endMinute, $endSecond)       = split(":", $this->endOfflineHour);

   $day   = date('d');
   $month = date('m');
   $year  = date('Y');

   $currentTimeStamp      = time();
   $startOfflineTimeStamp = mktime($startHour, $startMinute, $startSecond, $month, $day, $year);
   $endOfflineTimeStamp   = mktime($endHour, $endMinute, $endSecond, $month, $day, $year);

   if(($currentTimeStamp >= $startOfflineTimeStamp) && ($currentTimeStamp <= $endOfflineTimeStamp))
   {
      return true;
   }

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetDelayScanning
//
// [DESCRIPTION]:   Set the delay in secconds between 2 scannings
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-10-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetDelayScanning($delayScanning = 0)
{
   $this->delayScanning = $delayScanning;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDelayScanning
//
// [DESCRIPTION]:   Get the delay in secconds between 2 scannings
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-10-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDelayScanning($delayScanning = 0)
{
   return $this->delayScanning;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MarkAlive
//
// [DESCRIPTION]:   Mark this daemon is running
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-10-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MarkAlive()
{
   if(! $this->objFile->Touch($this->lockFileName))
      print $this->objFile->GetError();
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StartDemon
//
// [DESCRIPTION]:   Mark this daemon is running
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-10-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StartDaemon()
{
   set_time_limit(0);
   $objQuoteScanning = new CQuoteScanning();
   $objQuoteStatus   = new CQuoteStatus();

   if($this->objFile->Exists($this->lockFileName))
      return;

   $this->objFile->Delete($this->stopFileName);

   while(true)
   {
      $this->MarkAlive();

      if($this->objFile->Exists($this->stopFileName))
      {
         if($this->objFile->Delete($this->stopFileName) && $this->objFile->Delete($this->lockFileName))
            exit(0);
      }
      
      if($this->IsOfflineHours())
      {
         sleep(60);
         print "This site is offline\n";
         continue;
      }
      
      $time = time();
      if(($time - $this->lastTimeRun) < $this->delayScanning)
      {
         sleep($time - $this->lastTimeRun);
         continue;
      }
      
      $this->lastTimeRun = time();

      // check if we have dead quotes - status waiting
      // we have to punt on failure status and added to the scanner table
      if($resQuotesStatus = $objQuoteStatus->GetAllQuoteStatusBySiteIDAndStatus($this->siteID, "WAITING", true))
      {
         foreach($resQuotesStatus as $index => $resQuoteStatus)
         {
            if(! $objQuoteStatus->UpdateQuoteStatus($resQuoteStatus['id'], $resQuoteStatus['log_id'], $resQuoteStatus['site_id'], 'FAILURE'))
               continue;

            if(! $objQuoteScanning->AddQuoteScanning($resQuoteStatus['id']))
               continue;
         }
      }

      $resScanningSites = array();

      if(! $resScanningSites = $objQuoteScanning->GetSiteQuoteScanning($this->siteID))
         continue;

      foreach($resScanningSites as $index => $resScanningSites)
      {
         if($this->objFile->Exists($this->stopFileName))
            break;

         //print_r($resScanningSites);

         $tries = $resScanningSites['tries'];

         list($year, $month, $day)     = split("-",$resScanningSites['date']);
         list($hour, $minute, $second) = split(":",$resScanningSites['time']);

         $processTime = 0;
         $currentTime = time();
         $timeStamp   = mktime($hour, $minute, $second, $month, $day, $year);

         if($tries)
            $processTime = $timeStamp + pow(3, $tries) * 60;

         if($processTime > $currentTime)
            continue;
 
          echo getcwd(), " ", ('/usr/bin/php -q steps/GetQuote.php '.$this->siteID.' '.$resScanningSites['filename'].' 1 0 '), "\n";
          exec('/usr/bin/php -q steps/GetQuote.php '.$this->siteID.' '.$resScanningSites['filename'].' 1 0 ');
         
          $this->MarkAlive();

      }

   }// end while(true)

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: StopDemon
//
// [DESCRIPTION]:   Stop this daemon
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi Istvancsek (null@seopa.com) 2005-10-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function StopDaemon()
{
   if(! $this->objFile->Touch($this->stopFileName))
      print $this->objFile->GetError();

}

} // end CScanner

?>

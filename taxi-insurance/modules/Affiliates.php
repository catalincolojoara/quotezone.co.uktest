<?php
/*****************************************************************************/
/*                                                                           */
/*  CAffiliates class interface                                            */
/*                                                                           */
/*  (C) 2004 Eugen Savin (eugen@claspsolutions.com)                          */
/*                                                                           */
/*****************************************************************************/

error_reporting(0);

define("AFFILIATES_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";


if(DEBUG_MODE)
   error_reporting(E_ALL);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CAffiliates
//
// [DESCRIPTION]:  CAffiliates class interface
//
// [FUNCTIONS]:    int  AddAffiliates($userID=0, $quoteTypeID=0, $prefix="", $suffix="", $crDate="")
//                 bool UpdateAffiliates($affiliateID=0, $quoteTypeID=0, $prefix="", $suffix="")
//                 bool DeleteAffiliates($affiliatesID=0);
//                 array|false GetAffiliatesByID($affiliatesID=0);
//                 array|false GetAffiliatesByUID($userID=0);
//                 array|false GetAllAffUID($userID=0);
//                 array|false GetAllAffiliates();
//                 bool AssertAffiliates($userID, $prefix, $suffix, $crDate)
//
//                 void Close();
//                 string GetError();
//
// [CREATED BY]:   Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CAffiliates
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CAffiliates
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CAffiliates($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddAffiliates
//
// [DESCRIPTION]:   Add new entry to the affiliates table
//
// [PARAMETERS]:    $userID=0, $quoteTypeID=0, $prefix="", $suffix="", $crDate=""
//
// [RETURN VALUE]:  addressbookID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddAffiliates($userID=0, $quoteTypeID=0, $prefix="", $suffix="", $crDate="")
{

   if(! $this->AssertAffiliates($userID, $quoteTypeID, $prefix, $suffix, $crDate))
      return 0;

   // check if userID exists in DB
   $sqlCmd = "SELECT id FROM ".SQL_USERS." WHERE id=$userID";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERID_NOT_FOUND");
      return 0;
   }

   // check if userID exists in DB
   $sqlCmd = "SELECT id FROM ".SQL_AFFILIATES." WHERE prefix='$prefix' AND suffix='$suffix'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if($this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATE_ALREADY_EXISTS");
      return 0;
   }

   // check if userID exists in DB
   //$sqlCmd = "SELECT id FROM affiliates WHERE suffix='$suffix'";

   //if(! $this->dbh->Exec($sqlCmd))
   //{
   //   $this->strERR = $this->dbh->GetError();
   //   return 0;
   //}

   //if($this->dbh->GetRows())
   //{
   //   $this->strERR = GetErrorString("SUFFIX_ALREADY_EXISTS");
   //   return 0;
   //}

   $sqlCmd = "INSERT INTO ".SQL_AFFILIATES." (user_id, quote_type_id, prefix,  suffix, cr_date) VALUES ('$userID', '$quoteTypeID', '$prefix', '$suffix', '$crDate')";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

  $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $affiliatesID = $this->dbh->GetFieldValue("id");

   return $affiliatesID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateAffiliates
//
// [DESCRIPTION]:   Update affiliates table
//
// [PARAMETERS]:    $userID=0, $quoteTypeID=0, $prefix="", $suffix=""
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateAffiliates($affiliatesID=0, $userID=0, $quoteTypeID=0, $prefix="", $suffix="")
{
// print "$affiliatesID, $quoteTypeID, $userID, $prefix, $suffix, $clientRef";

   if(! preg_match("/^\d+$/", $affiliatesID))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATESID_FIELD");
      return false;
   }

   if(! $this->AssertAffiliates($userID, $quoteTypeID, $prefix, $suffix))
      return false;

   // check if affiliateID exists in DB
   $sqlCmd = "SELECT * FROM ".SQL_AFFILIATES." WHERE id=$affiliatesID";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATESID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $dbPrefix = $this->dbh->GetFieldValue("prefix");
   $dbSuffix = $this->dbh->GetFieldValue("suffix");

   if($prefix != $dbPrefix)
   {
      // check if userID exists in DB
      $sqlCmd = "SELECT id FROM ".SQL_AFFILIATES." WHERE prefix='$prefix'";

      if(! $this->dbh->Exec($sqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if($this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("PREFIX_ALREADY_EXISTS");
         return false;
      }
   }

   if($suffix != $dbSuffix)
   {
      // check if userID exists in DB
      $sqlCmd = "SELECT id FROM ".SQL_AFFILIATES." WHERE suffix='$suffix'";

      if(! $this->dbh->Exec($sqlCmd))
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      if($this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("SUFFIX_ALREADY_EXISTS");
         return false;
      }
   }

   $sqlCmd = "UPDATE ".SQL_AFFILIATES." SET user_id='$userID', quote_type_id='$quoteTypeID', prefix='$prefix', suffix='$suffix' WHERE  id='$affiliatesID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteAffiliates
//
// [DESCRIPTION]:   Delete an entry from affiliates table
//
// [PARAMETERS]:    $affiliatesID = 0
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteAffiliates($affiliatesID = 0)
{
   if(! preg_match("/^\d+$/", $affiliatesID))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATESID_FIELD");
      return false;
   }

   // check if addressbookID exists in DB
   $sqlCmd = "SELECT id FROM ".SQL_AFFILIATES." WHERE id=$affiliatesID";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATESID_NOT_FOUND");
      return false;
   }

   $sqlCmd = "DELETE FROM ".SQL_AFFILIATES." WHERE id=$affiliatesID";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffiliatesByID
//
// [DESCRIPTION]:   Read data from affiliates table and put it into an array variable
//
// [PARAMETERS]:    $affiliatesID = 0
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAffiliatesByID($affiliatesID = 0)
{
   if(! preg_match("/^\d+$/", $affiliatesID))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATESID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT * FROM ".SQL_AFFILIATES." WHERE id=$affiliatesID";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATESID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]               = $this->dbh->GetFieldValue("id");
   $arrayResult["user_id"]          = $this->dbh->GetFieldValue("user_id");
   $arrayResult["quote_type_id"]    = $this->dbh->GetFieldValue("quote_type_id");
   $arrayResult["prefix"]           = $this->dbh->GetFieldValue("prefix");
   $arrayResult["suffix"]           = $this->dbh->GetFieldValue("suffix");
   $arrayResult["cr_date"]          = $this->dbh->GetFieldValue("cr_date");

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffiliatesByUID
//
// [DESCRIPTION]:   Read data from affiliates table and put it into an array variable
//
// [PARAMETERS]:    $userID = 0
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAffiliatesByUID($userID = 0)
{
   if(! preg_match("/^\d+$/", $userID))
   {
      $this->strERR = GetErrorString("INVALID_USERID_FIELD");
      return false;
   }

   // check if we have this  user ID
   $sqlCmd = "SELECT id FROM ".SQL_AFFILIATES." WHERE user_id=$userID";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERID_NOT_FOUND");
      return false;
   }

   $sqlCmd = "SELECT * FROM ".SQL_AFFILIATES." WHERE user_id=$userID ORDER BY id";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();
   while($this->dbh->MoveNext())
   {
      $id = $this->dbh->GetFieldValue("id");

      $arrayResult[$id]["user_id"]        = $this->dbh->GetFieldValue("user_id");
      $arrayResult[$id]["quote_type_id"]  = $this->dbh->GetFieldValue("quote_type_id");
      $arrayResult[$id]["prefix"]         = $this->dbh->GetFieldValue("prefix");
      $arrayResult[$id]["suffix"]         = $this->dbh->GetFieldValue("suffix");
      $arrayResult[$id]["cr_date"]        = $this->dbh->GetFieldValue("cr_date");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllAffUId
//
// [DESCRIPTION]:   Read data from affiliates table and put it into an array variable
//
// [PARAMETERS]:    $userID=0
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllAffUID()
{
   // check if we have this  user ID
   $sqlCmd = "SELECT a.user_id,u.name FROM ".SQL_AFFILIATES." a, ".SQL_USERS." u WHERE a.user_id=u.id GROUP BY a.user_id";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();
   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("user_id")] = $this->dbh->GetFieldValue("name");

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllAffiliates
//
// [DESCRIPTION]:   Read data from affiliates table and put it into an array variable
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllAffiliates()
{
   $sqlCmd = "SELECT * FROM ".SQL_AFFILIATES." ORDER BY id ";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATESIDS_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   while($this->dbh->MoveNext())
   {
      $id = $this->dbh->GetFieldValue("id");

   $arrayResult[$id]["user_id"]          = $this->dbh->GetFieldValue("user_id");
   $arrayResult[$id]["quote_type_id"]    = $this->dbh->GetFieldValue("quote_type_id");
   $arrayResult[$id]["prefix"]           = $this->dbh->GetFieldValue("prefix");
   $arrayResult[$id]["suffix"]           = $this->dbh->GetFieldValue("suffix");
   $arrayResult[$id]["cr_date"]          = $this->dbh->GetFieldValue("cr_date");

   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAffiliatesIDBySufixAndPrefix
//
// [DESCRIPTION]:   Read data from affiliates table and put it into an array variable
//
// [PARAMETERS]:    $affiliatesID = 0
//
// [RETURN VALUE]:  id | false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAffiliatesIDBySufixAndPrefix($prefix, $suffix)
{
   if(empty($prefix))
   {
      $this->strERR = GetErrorString("INVALID_PREFIX_FIELD");
      return false;
   }

   if(empty($suffix))
   {
      $this->strERR = GetErrorString("INVALID_SUFIX_FIELD");
      return false;
   }

   $sqlCmd = "SELECT id FROM ".SQL_AFFILIATES." WHERE prefix='$prefix' and suffix='$suffix'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATES_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }


   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertAffiliates
//
// [DESCRIPTION]:   Check if all fields are OK and set the error string if needed
//
// [PARAMETERS]     $userID, $quoteTypeID, $prefix, $suffix, $crDate
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertAffiliates($userID, $quoteTypeID, $prefix, $suffix, $crDate)
{
   $this->strERR = "";

   if(! preg_match("/^\d+$/", $userID))
      $this->strERR .= GetErrorString("INVALID_USERID_FIELD")."\n";

   if(! preg_match("/^\d+$/", $quoteTypeID))
      $this->strERR .= GetErrorString("INVALID_QUOTETYPEID_FIELD")."\n";

   if(! preg_match("/^[a-zA-Z]+$/", $prefix))
      $this->strERR .= GetErrorString("INVALID_PREFIX_FIELD")."\n";

   if( !preg_match("/^\d+$/", $suffix))
      $this->strERR .= GetErrorString("INVALID_SUFFIX_FIELD")."\n";

   if( !preg_match("/^.{0,240}$/", $crDate))
      $this->strERR .= GetErrorString("INVALID_CRDATE_FIELD")."\n";

   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

} // end of CAffiliates class
?>

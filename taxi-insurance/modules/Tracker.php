<?php
/*****************************************************************************/
/*                                                                           */
/*  CTracker class interface                                                 */
/*                                                                           */
/*  (C) 2005 Velnic Daniel (null@seopa.com)                                   */
/*                                                                           */
/*****************************************************************************/


//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);



//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CTracker
//
// [DESCRIPTION]:  CTracker class interface
//
// [FUNCTIONS]:    bool AssertTraker($siteID=0,$logID=0,$trackUrlsID=0,$additionalInfo='',$hostIP='',$crDate=0)
//                 int  AddTracker($siteID=0,$logID=0,$trackUrlsID=0,$additionalInfo='',$hostIP='',$crDate=0)
//                 bool UpdateTracker($trackerID=0,$siteID=0,$logID=0,$trackUrlsID=0,$additionalInfo='',$hostIP='',$crDate=0)
//                 bool DeleteTracker($trackerID=0)
//                 bool GetAllTracker()
//                 bool GetTrackerByID($trackerID=0)
//                 bool GetTrackerByTrackUrlID($trackUrlsID="")
//                 bool GetTrackerByHostIP($hostIP="")
//                 bool GetTrackerByDate($crDate=0)
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CTracker
{
   // database handler
   var $dbh;         // database server handle
   var $closeDB;     // close database flag

   // class-internal variables
   var $lastSQLCMD;  // keep here the last SQL command
   var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CTracker
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CTracker($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertTraker
//
// [DESCRIPTION]:   Verify to see if the params are completed
//
// [PARAMETERS]:    $siteID=0, $logID=0, $trackUrlsID=0,$additionalInfo='',$hostIP='',$crDate=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertTraker($siteID=0,$logID=0,$trackUrlsID=0,$additionalInfo='',$hostIP='',$crDate=0)
{
   $this->strERR = "";

   if(! preg_match("/^\d+$/", $siteID))
      $this->strERR = GetErrorString("INVALID_SITE_ID_FIELD");

   if(! preg_match("/^\d+$/", $logID))
      $this->strERR = GetErrorString("INVALID_LOG_ID_FIELD");

   if(! preg_match("/^\d+$/", $trackUrlsID))
      $this->strERR = GetErrorString("INVALID_TRACK_URLS_ID_FIELD");

   if(! preg_match("/^\d+$/", $crDate))
      $this->strERR = GetErrorString("INVALID_CR_DATE_FIELD");

//    if(empty($additionalInfo))
//       $this->strERR = GetErrorString("INVALID_ADDITIONAL_INFO_FIELD");

   if(! preg_match("/((1|2)?\d?\d)\.((1|2)?\d?\d)\.((1|2)?\d?\d)\.((1|2)?\d?\d)/", $hostIP))
      $this->strERR = GetErrorString("INVALID_HOST_IP_FIELD");

    if($this->strERR != "")
      return true;

   return false;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddTracker
//
// [DESCRIPTION]:   Add new entry to the tracker table
//
// [PARAMETERS]:    $siteID=0, $logID=0, $trackUrlsID=0, $additionalInfo='', $hostIP='', $crDate=''
//
// [RETURN VALUE]:  trakerID or 0 in case of failure
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddTracker($siteID=0,$logID=0,$trackUrlsID=0,$additionalInfo='',$hostIP='',$crDate=0)
{
   if(! $crDate)
      $crDate = time();

   if($this->AssertTraker($siteID,$logID,$trackUrlsID,$additionalInfo,$hostIP,$crDate))
      return 0;
  
   if($_SESSION["RETRIEVE_QUOTES"]["SID"] == '1')
      $sid = 1;
   else
      $sid = 0;

   $this->lastSQLCMD = "INSERT INTO ".SQL_TRACKER." (site_id,log_id,sid,track_urls_id, additional_info, host_ip, cr_date) VALUES('$siteID','$logID','$sid','$trackUrlsID', '$additionalInfo', '$hostIP', '$crDate')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateTracker
//
// [DESCRIPTION]:   Modify an entry to the tracker table
//
// [PARAMETERS]:    $trackerID=0,$siteID=0,$logID=0,$trackUrlsID=0,$additionalInfo='',$hostIP='',$crDate=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateTracker($trackerID=0,$siteID=0,$logID=0,$trackUrlsID=0,$additionalInfo='',$hostIP='',$crDate=0)
{
   if(! preg_match("/^\d+$/", $trackerID))
   {
      $this->strERR = GetErrorString("INVALID_TRACKER_ID_FIELD");
      return false;
   }

   if(! $crDate)
      $crDate = time();

   if($this->AssertTraker($siteID,$logID,$trackUrlsID,$additionalInfo,$hostIP,$crDate))
      return false;

   $this->lastSQLCMD = "SELECT id FROM  ".SQL_TRACK_URLS." WHERE id='$trackUrlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("TRACK_URLS_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_TRACKER." SET site_id='$siteID',log_id='$logID',track_urls_id='$trackUrlsID', additional_info='$additionalInfo', host_ip='$hostIP', cr_date='$crDate' WHERE id='$trackerID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteTracker
//
// [DESCRIPTION]:   Delete an entry from tracker table by ID
//
// [PARAMETERS]:    $trackerID=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteTracker($trackerID=0)
{
   if(! preg_match("/^\d+$/", $trackerID))
   {
      $this->strERR = GetErrorString("INVALID_TRACKER_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_TRACKER." WHERE id='$trackerID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("TRACKER_ID_FIELD_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_TRACKER." WHERE id='$trackerID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllTracker
//
// [DESCRIPTION]:   Get all content of the table tracker
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllTracker()
{
   $this->lastSQLCMD = "SELECT * FROM ".SQL_TRACKER."";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("TRACKER_NOT_FOUND");
      return false;
   }
   
   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $id = $this->dbh->GetFieldValue("id");

      $arrayResult[$id]["id"]               = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["track_urls_id"]    = $this->dbh->GetFieldValue("track_urls_id");
      $arrayResult[$id]["additional_info"]  = $this->dbh->GetFieldValue("additional_info");
      $arrayResult[$id]["host_ip"]          = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$id]["cr_date"]          = $this->dbh->GetFieldValue("cr_date");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTrackerByID
//
// [DESCRIPTION]:   Get content of a record by id from the table tracker
//
// [PARAMETERS]:    $trackerID=0
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTrackerByID($trackerID=0)
{

   if(! preg_match("/^\d+$/", $trackerID))
   {
      $this->strERR = GetErrorString("INVALID_TRACKER_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_TRACKER." WHERE id='$trackerID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("TRACKER_ID_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]               = $this->dbh->GetFieldValue("id");
   $arrayResult["track_urls_id"]    = $this->dbh->GetFieldValue("track_urls_id");
   $arrayResult["additional_info"]  = $this->dbh->GetFieldValue("additional_info");
   $arrayResult["host_ip"]          = $this->dbh->GetFieldValue("host_ip");
   $arrayResult["cr_date"]          = $this->dbh->GetFieldValue("cr_date");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTrackerByTrackUrlID
//
// [DESCRIPTION]:   Get content of a record by trackUrlsID from the table tracker
//
// [PARAMETERS]:    $trackUrlsID=""
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTrackerByTrackUrlID($trackUrlsID="")
{

   if(! preg_match("/^\d+$/", $trackUrlsID))
   {
      $this->strERR = GetErrorString("INVALID_TRACKER_URLS_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_TRACKER." WHERE track_urls_id='$trackUrlsID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("TRACK_URLS_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {

      $id = $this->dbh->GetFieldValue("id");

      $arrayResult[$id]["id"]               = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["track_urls_id"]    = $this->dbh->GetFieldValue("track_urls_id");
      $arrayResult[$id]["additional_info"]  = $this->dbh->GetFieldValue("additional_info");
      $arrayResult[$id]["host_ip"]          = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$id]["cr_date"]          = $this->dbh->GetFieldValue("cr_date");

   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTrackerByHostIP
//
// [DESCRIPTION]:   Get content of a record by host_ip from the table tracker
//
// [PARAMETERS]:    $hostIP=""
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTrackerByHostIP($hostIP="")
{

   if(! preg_match("/((1|2)?\d?\d)\.((1|2)?\d?\d)\.((1|2)?\d?\d)\.((1|2)?\d?\d)/", $hostIP))
   {
      $this->strERR = GetErrorString("INVALID_HOST_IP_FIELD");
      return 0;
   }


   $this->lastSQLCMD = "SELECT * FROM ".SQL_TRACKER." WHERE host_ip='$hostIP'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("HOST_IP_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $id = $this->dbh->GetFieldValue("id");
      
      $arrayResult[$id]["id"]               = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["track_urls_id"]    = $this->dbh->GetFieldValue("track_urls_id");
      $arrayResult[$id]["additional_info"]  = $this->dbh->GetFieldValue("additional_info");
      $arrayResult[$id]["host_ip"]          = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$id]["cr_date"]          = $this->dbh->GetFieldValue("cr_date");

   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTrackerByDate
//
// [DESCRIPTION]:   Get content of a record by cr_date from the table tracker
//
// [PARAMETERS]:    $crDate=""
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTrackerByDate($crDate=0)
{

   if(! preg_match("/^\d+$/", $crDate))
   {
      $this->strERR = GetErrorString("INVALID_CR_DATE_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_TRACKER." WHERE cr_date='$crDate'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("CR_DATE_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $id = $this->dbh->GetFieldValue("id");

      $arrayResult[$id]["id"]               = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["track_urls_id"]    = $this->dbh->GetFieldValue("track_urls_id");
      $arrayResult[$id]["additional_info"]  = $this->dbh->GetFieldValue("additional_info");
      $arrayResult[$id]["host_ip"]          = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$id]["cr_date"]          = $this->dbh->GetFieldValue("cr_date");
   }

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTrackerByTrackUrlIdAndDate
//
// [DESCRIPTION]:   Get content of a record by cr_date from the table tracker
//
// [PARAMETERS]:    $crDate=""
//
// [RETURN VALUE]:  $arrayResult | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTrackerByTrackUrlIdAndDate($trackUrlsID=0, $beginDate=0, $endDate=0, $additionalInfo='')
{
   if(! preg_match("/^\d+$/", $trackUrlsID))
   {
      $this->strERR = GetErrorString("INVALID_TRACKER_URLS_ID_FIELD");
      return false;
   }

   if(! empty($beginDate))
      if(! preg_match("/^\d+$/", $beginDate))
      {
         $this->strERR = GetErrorString("INVALID_CR_DATE_BEGIN_FIELD");
         return false;
      }

   if(! empty($endDate))
      if(! preg_match("/^\d+$/", $endDate))
      {
         $this->strERR = GetErrorString("INVALID_CR_DATE_END_FIELD");
         return false;
      }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_TRACKER." WHERE track_urls_id='$trackUrlsID'";

   if(! empty($beginDate))
      $this->lastSQLCMD .= " AND cr_date>='$beginDate' ";

   if(! empty($endDate))
      $this->lastSQLCMD .= " AND cr_date<='$endDate' ";

   if(! empty($additionalInfo))
      $this->lastSQLCMD .= " AND additional_info like '%$additionalInfo%' " ;

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("CR_DATE_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $id = $this->dbh->GetFieldValue("id");

      $arrayResult[$id]["id"]               = $this->dbh->GetFieldValue("id");
      $arrayResult[$id]["track_urls_id"]    = $this->dbh->GetFieldValue("track_urls_id");
      $arrayResult[$id]["additional_info"]  = $this->dbh->GetFieldValue("additional_info");
      $arrayResult[$id]["host_ip"]          = $this->dbh->GetFieldValue("host_ip");
      $arrayResult[$id]["cr_date"]          = $this->dbh->GetFieldValue("cr_date");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 19-09-2005
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}


}
?>

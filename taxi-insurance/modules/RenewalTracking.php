<?php
/*****************************************************************************/
/*                                                                           */
/*  CRenewalTracking class interface                                              */
/*                                                                           */
/*  (C) 2005 Istvancsek Gabriel (null@seopa.com)                              */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CRenewalTrackingTracking
//
// [DESCRIPTION]:  CRenewalTracking class interface
//
// [FUNCTIONS]:    
//
//                 Close();
//                 GetError();
//                 ShowError();
//
// [CREATED BY]:   Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CRenewalTracking
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CRenewalTracking
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CRenewalTracking($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuoteUser
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    &$firstName, &$lastName, $birthDate='', $password=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertRenewalTracking($quoteUserID, $emStoreID, $clicked, $date)
{
   $this->strERR = '';

   if(! preg_match('/\d+/i',$quoteUserID))
      $this->strERR .= GetErrorString("INVALID_USER_ID_FIELD");

   if(! preg_match('/\d+/i',$qzQuoteID))
      $this->strERR .= GetErrorString("INVALID_QZ_QUOTE_ID_FIELD");

   if(! preg_match('/\d+/i',$emStoreID))
      $this->strERR .= GetErrorString("INVALID_EM_STORE_ID_FIELD");

   if(! preg_match('/\d+/i',$clicked))
      $this->strERR .= GetErrorString("INVALID_CLICKED_FIELD")."\n";

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$date))
      $this->strERR .= GetErrorString("INVALID_DATE_FIELD")."\n";

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddRenewal
//
// [DESCRIPTION]:   Check if exist a user and add if not exist into renewal table
//
// [PARAMETERS]:    $emStoreID, $crDate
//
// [RETURN VALUE]:  $renewalID | false
//
// [CREATED BY]:    Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddRenewalTracking($quoteUserID, $emStoreID, $clicked, $date, $time,$type,$year='1')
{	
   if(! preg_match('/\d+/i',$quoteUserID))
   {
      $this->strERR .= GetErrorString("INVALID_QUOTE_USER_ID_FIELD");
      return false;
   }

   if(! preg_match('/\d+/i',$emStoreID))
   {
      $this->strERR .= GetErrorString("INVALID_EM_STORE_ID_FIELD");
      return false;
   }

   if(! preg_match('/\d+/i',$clicked))
   {
      $this->strERR .= GetErrorString("INVALID_CLICKED_FIELD");
      return false;
   }

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$date))
   {
      $this->strERR .= GetErrorString("INVALID_DATE_FIELD")."\n";
      return false;
   }
   
   $this->lastSQLCMD = "INSERT INTO renewal_tracking (quote_user_id,em_store_id,clicked,date,time,quote_type_id,year) VALUES ('$quoteUserID','$emStoreID','$clicked','$date','$time','$type','$year')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddRenewalIn
//
// [DESCRIPTION]:   Check if exist a user and add if not exist into renewal table
//
// [PARAMETERS]:    $emStoreID, $crDate
//
// [RETURN VALUE]:  $renewalID | false
//
// [CREATED BY]:    Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddRenewalIn($renewalID, $button, $quoteTypeId,$date,$time,$year)
{	
   if(! preg_match('/\d+/i',$renewalID))
   {
      $this->strERR .= GetErrorString("INVALID_RENEWAL_ID_FIELD");
      return false;
   }

   if(! preg_match('/.*/i',$button))
   {
      $this->strERR .= GetErrorString("INVALID_BUTTON_FIELD");
      return false;
   }

   if(! preg_match('/\d+/i',$quoteTypeId))
   {
      $this->strERR .= GetErrorString("INVALID_QUOTE_TYPE_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "INSERT INTO renewal_in (renewal_id,button,quote_type_id,date,time,year) VALUES ('$renewalID','$button','$quoteTypeId','$date','$time','$year')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTrackingsByDate
//
// [DESCRIPTION]:   Get number of renewals by date
//
// [PARAMETERS]:    $renewalID, $quoteUserID, $qzQuoteID, $upDate
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:   Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTrackingsByDate($dateFrom, $dateTo, $clickType,$year='1')
{

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$dateFrom))
   {
      $this->strERR .= GetErrorString("INVALID_DATEFROM_FIELD")."\n";
      return false;
   }
   
   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$dateTo))
   {
      $this->strERR .= GetErrorString("INVALID_DATETO_FIELD")."\n";
      return false;
   }
   
   $this->lastSQLCMD = "SELECT count(*) as cnt FROM renewal_tracking WHERE date>='$dateFrom' AND date<='$dateTo' and year='$year'";

   if ($clickType != '')
	$this->lastSQLCMD .= " AND clicked='$clickType'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");   
   
   return $cnt;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTrackingsByDate
//
// [DESCRIPTION]:   Get number of renewals by date
//
// [PARAMETERS]:    $renewalID, $quoteUserID, $qzQuoteID, $upDate
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:   Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTrackingsByDateAndType($dateFrom, $dateTo, $type, $clickType,$year='1')
{

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$dateFrom))
   {
      $this->strERR .= GetErrorString("INVALID_DATEFROM_FIELD")."\n";
      return false;
   }
   
   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$dateTo))
   {
      $this->strERR .= GetErrorString("INVALID_DATETO_FIELD")."\n";
      return false;
   }
   
   $this->lastSQLCMD = "SELECT count(*) as cnt FROM renewal_tracking WHERE date>='$dateFrom' AND date<='$dateTo' and quote_type_id='$type' and year='$year'";

   if ($clickType != '')
	$this->lastSQLCMD .= "AND clicked='$clickType'";
//print $this->lastSQLCMD.'<br>';
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");   
   
   return $cnt;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRenewalsByDate
//
// [DESCRIPTION]:   Get number of renewals by date
//
// [PARAMETERS]:    $renewalID, $quoteUserID, $qzQuoteID, $upDate
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:   Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAverageClicksPerDay($year='1')
{

   $this->lastSQLCMD = "SELECT min(date) as date1,max(date) as date2 from renewal_tracking where year='$year'";// AND quote_user_id!='0'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   $date1 = $this->dbh->GetFieldValue("date1");
   $date2 = $this->dbh->GetFieldValue("date2");
   
   $this->lastSQLCMD = "SELECT count(*) as cnt FROM renewal_tracking where year='$year'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");   

   if ($date2 == $date1)
	$daysInterval = 1;
   else
	$daysInterval = intval((strtotime($date2) - strtotime($date1)) / 86400);
      
   $avgQuotesPerDay = number_format(($cnt/$daysInterval), 2, ".","");
   
   
   return $avgQuotesPerDay;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Show the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

}//end class CRenewalTracking

?>

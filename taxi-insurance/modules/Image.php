<?php
/*****************************************************************************/
/*                                                                           */
/*  CImage class interface                                                   */
/*                                                                           */
/*  (C) 2005 Istvancsek Gabriel (null@seopa.com)                              */
/*                                                                           */
/*****************************************************************************/


error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CImage
//
// [DESCRIPTION]:  Generate png image random
//
// [FUNCTIONS]: 
// 
//
// [CREATED BY]:   Istvancsek Gabriel (null@seopa.com) 2005-11-24
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CImage
{

   var $imageWidth;                    // width of image
   var $imageHeight;                   // height of image
   var $imageContent;                  // image content

   var $imageBackgroundColor;          // image background color
   var $imageBackgroundColorSelection; // image background color
   var $imageTextColor;                // image text color
   var $imageTextColorSelection;       // image text color selection : fixed | random
   var $imageNoiseColor;               // image noise color
   var $imageNoiseColorSelection;      // image noise color selection : fixed | random

   var $imageNoiseNumber;              // how many noise elemnts to add tt image
   var $imageNoiseType;                // type of noise dot, line, rectangle, circle

   var $fontPath;                      // the path where the fonts are
   var $fontTitle;                     // font title
   var $fontTitleSelection;            // font title selection : fixed | random
   var $fontSize;                      // font size
   var $fontSizeSelection;             // font size selection : fixed | random
   var $fontSizeExtraLimit;            // in case of random generate random +- font size of fixed font size
   
   var $imageText;                     // text image content
   var $imageTextAngle;                // maximum angle of text in the image
   var $imageTextAngleSelection;       // fixed | random
   var $imageTextAngleExtraLimit;      // in case of random generate random +- angle

   var $strERR;                        //errors message

      function CImage()
      {
         //default values
         $this->imageWidth                    = '100';
         $this->imageHeight                   = '50';
         $this->imageContent                  = '';
   
         $this->imageBackgroundColor          = '2E5D95';
         $this->imageBackgroundColorSelection = 'fixed';
   
         $this->imageTextColor                = 'FFFFFF';
         $this->imageTextColorSelection       = 'fixed';
   
         $this->imageNoiseColor               = 'FFCCFF';
         $this->imageNoiseColorSelection      = 'fixed';
   
         $this->fontPath                      = '../../fonts/';
         $this->fontTitle                     = array('wintermu.ttf','zorque.ttf','larabieb.ttf');
         $this->fontTitleSelection            = 'fixed';
         $this->fontTitleDefault              = 'larabieb.ttf';
   
         $this->fontSize                      = '26';
         $this->fontSizeSelection             = 'fixed';
         $this->fontSizeExtraLimit            = '1';
   
         $this->imageText                     = 'no set!';
         $this->imageTextAngle                = '0';
         $this->imageTextAngleSelection       = 'fixed';
         $this->imageTextAngleExtraLimit      = '5';
   
         $this->imageNoiseNumber              = '800';
         $this->imageNoiseType                = 'dot';
   
         $this->strERR                        = '';
      }

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetImageWidth
      //
      // [DESCRIPTION]:   Set image width
      //
      // [PARAMETERS]:    $imageWidth
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetImageWidth($imageWidth='')
      {
         if(! preg_match("/\d+/", $imageWidth))
         {
            $this->strERR = 'INVALID_IMAGE_WIDTH';
            return false;
         }

         $this->imageWidth = $imageWidth;

         return true;
      }// end function SetImageWidth($imageWidth='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetImageHeight
      //
      // [DESCRIPTION]:   Set image height
      //
      // [PARAMETERS]:    $imageHeight
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetImageHeight($imageHeight='')
      {
         if(! preg_match("/\d+/", $imageHeight))
         {
            $this->strERR = 'INVALID_IMAGE_HEIGHT';
            return false;
         }

         $this->imageHeight = $imageHeight;

         return true;
      }// end function SetImageHeight($imageHeight='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetImageBackgroundColor
      //
      // [DESCRIPTION]:   Set image background color
      //
      // [PARAMETERS]:    $imageHeight
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetImageBackgroundColor($imageBackgroundColor='')
      {
         if(! preg_match("/^[A-Za-z0-9]{6}$/", $imageHeight))
         {
            $this->strERR = 'INVALID_IMAGE_BACKGROUND_COLOR';
            return false;
         }

         $this->imageBackgroundColor = $imageBackgroundColor;

         return true;
      }// end function SetImageBackgroundColor($imageBackgroundColor='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetImageTextColor
      //
      // [DESCRIPTION]:   Set image text xolor
      //
      // [PARAMETERS]:    $imageTextColor
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetImageTextColor($imageTextColor='')
      {
         if(! preg_match("/^[A-Za-z0-9]{6}$/", $imageTextColor))
         {
            $this->strERR = 'INVALID_IMAGE_TEXT_COLOR';
            return false;
         }

         $this->imageTextColor = $imageTextColor;

         return true;
      }// end function SetImageBackgroundColor($imageBackgroundColor='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetImageNoiseColor
      //
      // [DESCRIPTION]:   Set image noise color
      //
      // [PARAMETERS]:    $imageNoiseColor
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetImageNoiseColor($imageNoiseColor='')
      {
         if(! preg_match("/^[A-Za-z0-9]{6}$/", $imageNoiseColor))
         {
            $this->strERR = 'INVALID_IMAGE_NOISE_COLOR';
            return false;
         }

         $this->imageNoiseColor = $imageNoiseColor;

         return true;
      }// end function SetImageBackgroundColor($imageBackgroundColor='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetImageBackgroundColorSelection
      //
      // [DESCRIPTION]:   Set image selection of background image like : fixed | random
      //
      // [PARAMETERS]:    $imageNoiseColor
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetImageBackgroundColorSelection($selection='')
      {
         if(! preg_match("/(fixed)|(random)/", $selection))
         {
            $this->strERR = 'INVALID_IMAGE_BACKGROUND_COLOR_SELECTION';
            return false;
         }

         $this->imageBackgroundColorSelection = $selection;

         return true;
      }// end function SetImageBackgroundColor($imageBackgroundColor='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetImageTextColorSelection
      //
      // [DESCRIPTION]:   Set image selection of text like : fixed | random
      //
      // [PARAMETERS]:    $imageNoiseColor
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetImageTextColorSelection($selection='')
      {
         if(! preg_match("/(fixed)|(random)/", $selection))
         {
            $this->strERR = 'INVALID_IMAGE_TEXT_COLOR_SELECTION';
            return false;
         }

         $this->imageTextColorSelection = $selection;

         return true;
      }// end function SetImageBackgroundColor($imageBackgroundColor='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetImageBackgroundColorSelection
      //
      // [DESCRIPTION]:   Set image selection of background image like : fixed | random
      //
      // [PARAMETERS]:    $imageNoiseColor
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetImageNoiseColorSelection($selection='')
      {
         if(! preg_match("/(fixed)|(random)/", $selection))
         {
            $this->strERR = 'INVALID_IMAGE_NOISE_COLOR_SELECTION';
            return false;
         }

         $this->imageNoiseColorSelection = $selection;

         return true;
      }// end function SetImageBackgroundColor($imageBackgroundColor='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetFontPath
      //
      // [DESCRIPTION]:   set the oath where the fonts are
      //
      // [PARAMETERS]:    $fontPath
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetFontPath($fontPath='')
      {
         if(empty($fontPath))
         {
            $this->strERR = 'INVALID_FONT_PATH';
            return false;
         }

         $this->fontPath = $fontPath;

         return true;

      }// end function _prepareImageBackgroundColor($imageBackgroundColorHex='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetFontTitle
      //
      // [DESCRIPTION]:   set the titles for the fonts 
      //
      // [PARAMETERS]:    $fontTitle = array()
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetFontTitle($fontTitle = array())
      {
         if(! is_array($fontTitle))
         {
            $this->strERR = 'INVALID_FONT_TITLE';
            return false;
         }

         $this->fontTitle = $fontPath;

         return true;

      }// end function _prepareImageBackgroundColor($imageBackgroundColorHex='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetFontTitleSelection
      //
      // [DESCRIPTION]:   Set font title  : fixed | random
      //
      // [PARAMETERS]:    $selection
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetFontTitleSelection($selection='')
      {
         if(! preg_match("/(fixed)|(random)/", $selection))
         {
            $this->strERR = 'INVALID_FONT_TITLE_SELECTION';
            return false;
         }

         $this->fontTitleSelection = $selection;

         return true;

      }// end function SetImageBackgroundColor($imageBackgroundColor='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetFontSize
      //
      // [DESCRIPTION]:   Set font size
      //
      // [PARAMETERS]:    $fontSize
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetFontSize($fontSize='')
      {
         if(! preg_match("/\d+/", $fontSize))
         {
            $this->strERR = 'INVALID_FONT_SIZE';
            return false;
         }

         $this->fontSize = $fontSize;

         return true;
      }// end function SetImageHeight($imageHeight='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetFontSizeSelection
      //
      // [DESCRIPTION]:   set the font size
      //
      // [PARAMETERS]:    $fontSize
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetFontSizeSelection($selection='')
      {
         if(! preg_match("/(fixed)|(random)/", $selection))
         {
            $this->strERR = 'INVALID_FONT_SIZE_SELECTION';
            return false;
         }

         $this->fontSizeSelection = $selection;

         return true;
      }// end SetFontSizeSelection($selection='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetFontSizeExtraLimit
      //
      // [DESCRIPTION]:   Set font size
      //
      // [PARAMETERS]:    $fontSize
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetFontSizeExtraLimit($fontSizeExtraLimit='')
      {
         if(! preg_match("/\d+/", $fontSizeExtraLimit))
         {
            $this->strERR = 'INVALID_FONT_SIZE';
            return false;
         }

         $this->fontSizeExtraLimit = $fontSizeExtraLimit;

         return true;
      }// end SetFontSizeExtraLimit($fontSizeExtraLimit='')


      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetImageText
      //
      // [DESCRIPTION]:   Set image text
      //
      // [PARAMETERS]:    $imageText
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetImageText($imageText='')
      {
         if(empty($imageText))
         {
            $this->strERR = 'INVALID_IMAGE_TEXT';
            return false;
         }

         $this->imageText = $imageText;

         return true;
      }// end function SetImageText($imageText='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetImageText
      //
      // [DESCRIPTION]:   Set image text
      //
      // [PARAMETERS]:    $imageText
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function GetImageText()
      {
         return $this->imageText;

      }// end function SetImageText($imageText='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetImageText
      //
      // [DESCRIPTION]:   Set image text
      //
      // [PARAMETERS]:    $imageText
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetImageTextAngle($imageTextAngle='')
      {
         if(! preg_match("/\d+/",$imageTextAngle))
         {
            $this->strERR = 'INVALID_IMAGE_TEXT_ANGLE';
            return false;
         }

         $this->imageTextAngle = $imageTextAngle;

         return true;
      }// end SetImageTextAngle($imageTextAngle='')
      
      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetImageTextAngleSelection
      //
      // [DESCRIPTION]:   set image text angle selection : fixed | random
      //
      // [PARAMETERS]:    $imageTextAngleSelection
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetImageTextAngleSelection($selection='')
      {
         if(! preg_match("/(fixed)|(random)/", $selection))
         {
            $this->strERR = 'INVALID_IMAGE_TEXT_ANGLE_SELECTION';
            return false;
         }

         $this->imageTextAngleSelection = $selection;

         return true;
      }// end function SetImageTextAngleSelection($selection='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetImageTextAngleExtraLimit
      //
      // [DESCRIPTION]:   Set imate text angle limit +- from the default value random
      //
      // [PARAMETERS]:    $imageTextAngleExtraLimit
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetImageTextAngleExtraLimit($imageTextAngleExtraLimit='')
      {
         if(! preg_match("/\d+/",$imageTextAngleExtraLimit))
         {
            $this->strERR = 'INVALID_IMAGE_TEXT_ANGLE';
            return false;
         }

         $this->imageTextAngleExtraLimit = $imageTextAngleExtraLimit;

         return true;
      }// end function SetImageTextAngleExtraLimit($imageTextAngleExtraLimit='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetImageTextAngleExtraLimit
      //
      // [DESCRIPTION]:   Set imate text angle limit +- from the default value random
      //
      // [PARAMETERS]:    $imageTextAngleExtraLimit
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetImageImageNoiseNumber($imageNoiseNumber='')
      {
         if(! preg_match("/\d+/",$imageNoiseNumber))
         {
            $this->strERR = 'INVALID_IMAGE_NOISE_NUMBER';
            return false;
         }

         $this->imageNoiseNumber = $imageNoiseNumber;

         return true;
      }// end function SetImageTextAngleExtraLimit($imageTextAngleExtraLimit='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: SetImageTextAngleSelection
      //
      // [DESCRIPTION]:   set image noise type selection : dot | line | rectangle | (circle ?) ...
      //
      // [PARAMETERS]:    $imageNoiseType
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function SetImageNoiseType($selection='')
      {
         if(! preg_match("/(dot)|(line)|(rectangle)|(circle)/", $selection))
         {
            $this->strERR = 'INVALID_IMAGE_TEXT_ANGLE_SELECTION';
            return false;
         }

         $this->imageNoiseType = $selection;

         return true;
      }// end SetImageNoiseType($selection='')

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: _convertImageColor
      //
      // [DESCRIPTION]:   convert hexa to decimal by chanel R G B 
      //
      // [PARAMETERS]:    $imageColorHex
      //
      // [RETURN VALUE]:  false | array
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function _convertImageColor($imageColorHex='')
      {
         if(! preg_match("/^[A-Za-z0-9]{6}$/", $imageColorHex))
         {
            $this->strERR = 'INVALID_IMAGE_COLOR';
            return false;
         }

         $resImageColor['R'] = hexdec($imageColorHex[0].$imageColorHex[1]);
         $resImageColor['G'] = hexdec($imageColorHex[2].$imageColorHex[3]);
         $resImageColor['B'] = hexdec($imageColorHex[4].$imageColorHex[5]);

         return $resImageColor;

      }// end function _prepareImageBackgroundColor($imageBackgroundColorHex='')
    
      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: _generateRandomColor
      //
      // [DESCRIPTION]:   generate random color R G B
      //
      // [PARAMETERS]:    $imageElement
      //
      // [RETURN VALUE]:  false | array
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function _generateRandomColor($imageElement='')
      {
         switch($imageElement)
         {
            case 'background':
               
               $imageRGB = $this->_convertImageColor($this->imageBackgroundColor);

               if($this->imageBackgroundColorSelection == 'random')
               {
                  $R = rand(0,132);
                  $G = rand(0,132);
                  $B = rand(0,132);

                  $imageRGB['R'] = $R;
                  $imageRGB['G'] = $G;
                  $imageRGB['B'] = $B;

                  $this->imageBackgroundColor = dechex($R).dechex($G).dechex($B);
               }

               break;

            case 'text':

               $imageRGB = $this->_convertImageColor($this->imageTextColor);

               if($this->imageTextColorSelection == 'random')
               {
                  $R = rand(191,255);
                  $G = rand(191,255);
                  $B = rand(191,255);

                  $imageRGB['R'] = $R;
                  $imageRGB['G'] = $G;
                  $imageRGB['B'] = $B;

                  $this->imageTextColor = dechex($R).dechex($G).dechex($B);
               }

               break;

            case 'noise':

               $imageRGB = $this->_convertImageColor($this->imageNoiseColor);

               if($this->imageNoiseColorSelection == 'random')
               {
                  $R = rand(133,190);
                  $G = rand(133,190);
                  $B = rand(133,190);

                  $imageRGB['R'] = $R;
                  $imageRGB['G'] = $G;
                  $imageRGB['B'] = $B;

                  $this->imageTextColor = dechex($R).dechex($G).dechex($B);
               }

               break;
         }

         return $imageRGB;
      }

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: _generateRandomFontSize
      //
      // [DESCRIPTION]:   generate random color R G B
      //
      // [PARAMETERS]:    $imageElement
      //
      // [RETURN VALUE]:  false | array
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function _generateRandomFontSize()
      {
         $fontSize = $this->fontSize;

         if($this->fontSizeSelection == 'random')
         {
            $fontSize = rand($this->fontSize - $this->fontSizeExtraLimit,$this->fontSize + $this->fontSizeExtraLimit);
         }

         return $fontSize;
      }

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: _generateRandomFontSize
      //
      // [DESCRIPTION]:   generate random color R G B
      //
      // [PARAMETERS]:    $imageElement
      //
      // [RETURN VALUE]:  false | array
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function _generateRandomTextAngle()
      {
         $imageTextAngle = $this->imageTextAngle;

         if($this->imageTextAngleSelection == 'random')
         {
            $imageTextAngle = rand(-$this->imageTextAngleExtraLimit,$this->imageTextAngleExtraLimit);
         }

         return $imageTextAngle;
      }

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: _generateRandomFontTitle
      //
      // [DESCRIPTION]:   generate random color R G B
      //
      // [PARAMETERS]:    $imageElement
      //
      // [RETURN VALUE]:  false | array
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function _generateRandomFontTitle()
      {
         $fontTitle = $this->fontTitleDefault;

         if($this->fontTitleSelection == 'random')
         {
            $fontTitleKey = rand(0, count($this->fontTitle) - 1);

            $fontTitle    = $this->fontTitle[$fontTitleKey];
         }

         return $fontTitle;
      }

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: _generateRandomFontTitle
      //
      // [DESCRIPTION]:   generate random color R G B
      //
      // [PARAMETERS]:    $imageElement
      //
      // [RETURN VALUE]:  false | array
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function GenerateRandomImageText($type='car')
      {
         $garbleTextLetters = array('A','C','D','E','F','G','H','I','J','K','L','M','N','P','R','T','U','V','X','Y','W');
         $garbleTextNumbers = array('1','3','4','6','7','9');
      
         $maxLettersIndex = count($garbleTextLetters) - 1;
         $maxNumbersIndex = count($garbleTextNumbers) - 1;
      
         switch($type)
         {
            case 'car':
               $randomText = "C";
               break;

            case 'van':
               $randomText = "V";
               break;

            case 'home':
               $randomText = "H";
               break;

            case 'bike':
               $randomText = "B";
               break;
         }

         $time = time();
      
         for($i=0; $i<4; $i++)
         {
            mt_srand($this->_makeSeed());
            $guess = mt_rand(0, 1);
      
            mt_srand($this->_makeSeed());
      
            if($guess == 0)
               $randomText .= $garbleTextLetters[mt_rand(0, $maxLettersIndex)];
            else
               $randomText .= $garbleTextNumbers[mt_rand(0, $maxNumbersIndex)];
         }

         $this->imageText = $randomText;

         return true;
      }

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: MakeSeed()
      //
      // [DESCRIPTION]:   Make random number
      //
      // [PARAMETERS]:    none
      //
      // [RETURN VALUE]:  random number
      //
      // [CREATED BY]:    Eugen SAVIN (null@seopa.com) 2005-09-13
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function _makeSeed()
      {
         list($usec, $sec) = explode(' ', microtime());
      
         return (float) $sec + ((float) $usec * 100000000);
      }

      //////////////////////////////////////////////////////////////////////////////FB
      //
      // [FUNCTION NAME]: GenerateImage()
      //
      // [DESCRIPTION]:   generate the image
      //
      // [PARAMETERS]:    none
      //
      // [RETURN VALUE]:  false | true
      //
      // [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-11-24
      //
      // [MODIFIED]:      - [programmer (email) date]
      //                    [short description]
      //////////////////////////////////////////////////////////////////////////////FE
      function GenerateImage()
      {
         // create image
         if(! $this->imageContent = ImageCreate($this->imageWidth,$this->imageHeight))
         {
            $this->strERR = 'CANNOT_CREATE_IMAGE';

            return false;
         }

         if(! $imageBackgroundRGB = $this->_generateRandomColor('background'))
            return false;

         // set image background color, text color and nois color
         $imageBackgroundColor = ImageColorAllocate($this->imageContent,$imageBackgroundRGB['R'], $imageBackgroundRGB['G'], $imageBackgroundRGB['B']);

         if(! $imageTextRGB = $this->_generateRandomColor('text'))
            return false;

         $imageTextColor       = ImageColorAllocate($this->imageContent,$imageTextRGB['R'], $imageTextRGB['G'], $imageTextRGB['B']);

         if(! $imageNoiseRGB = $this->_generateRandomColor('noise'))
            return false;

         $imageNoiseColor      = ImageColorAllocate($this->imageContent,$imageNoiseRGB['R'], $imageNoiseRGB['G'], $imageNoiseRGB['B']);

         $fontSize        = $this->_generateRandomFontSize();
         $fontTitle       = $this->_generateRandomFontTitle();
         $imageTextAngle  = $this->_generateRandomTextAngle();

         // Fill the image with background color
         ImageFill($this->imageContent, $this->imageWidth, $this->imageHeight, $imageBackgroundColor);

         // Add centered text to image
         $resImage = ImageTtfbBox($fontSize, $imageTextAngle, $this->fontPath.$fontTitle, $this->imageText);

         $imageText_X = round(($this->imageWidth - (abs($resImage[2]-$resImage[0]))) / 2, 0);
         $imageText_Y = round(($this->imageHeight - (abs($resImage[5]-$resImage[3]))) / 2, 0);

         ImageTTFText($this->imageContent, $fontSize, $imageTextAngle, $imageText_X, $imageText_Y - $resImage[5], $imageTextColor, $this->fontPath.$fontTitle, $this->imageText);

         $imageNoiseColor = $imageBackgroundColor;

         $i = 0;

         while ($i < $this->imageNoiseNumber)
         {
            $dotX = rand(0, $this->imageWidth);
            $dotY = rand(0, $this->imageHeight);

            switch ($this->imageNoiseType)
            {
               case 'line':
                     $line_width = rand(4,20);

                     if (rand(0,10)>=5)
                     {
                        // Draw horizontal line
                        ImageLine($this->imageContent, $dotX, $dotY, $dotX + $line_width, $dotY, $imageNoiseColor);
                     }
                     else
                     {
                        // Draw vertical line
                        ImageLine($this->imageContent, $dotX, $dotY, $dotX, $dotY + $line_width, $imageNoiseColor);
                     }
                     break;

               case 'dot':
                     ImageSetPixel($this->imageContent, $dotX, $dotY, $imageNoiseColor);
                     break;

               case 'rectangle':
                     ImageRectangle($this->imageContent, $dotX-1, $dotY-1, $dotX+1, $dotY+1, $imageNoiseColor);
                     break;
            }

            $i++;
         }

         header('Content-type:image/png');
         header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
         header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
         header('Cache-Control: no-store, no-cache, must-revalidate');
         header('Cache-Control: post-check=0, pre-check=0', false);

         ImagePNG($this->imageContent);
         ImageDestroy($this->imageContent);

      }// end function GenerateImage()
   
}// end class CImage

?>

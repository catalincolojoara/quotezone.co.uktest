<?php
/*****************************************************************************/
/*                                                                           */
/*  CAffiliates class interface                                            */
/*                                                                           */
/*  (C) 2004 Eugen Savin (eugen@claspsolutions.com)                          */
/*                                                                           */
/*****************************************************************************/
// include_once "globals.adm.inc";
define("AFFILIATES_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";



if(DEBUG_MODE)
   error_reporting(E_ALL);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQzCookieAffiliate
//
// [DESCRIPTION]:  CQzCookieAffiliate class interface
//
// [FUNCTIONS]:    int  AddQzCookieAffiliate($qzQuoteID=0, $affiliateID=0, $cookieID=0)
//                 bool UpdateQzCookieAffiliate($qzQuoteAffiliatesID=0, $qzQuoteID=0, $affiliateID=0, $cookieID=0)
//                 bool DeleteQzCookieAffiliate($qzQuoteAffiliatesID=0);
//                 array|false GetQzCookieAffiliateByID($qzQuoteAffiliatesID=0);
//                 array|false GetQzCookieAffiliateByQID($qzQuoteID=0);
//                 array|false GetQzCookieAffiliateByAID($affiliateID=0);
//                 int GetQzCookieAffiliatesCount($startDate='', $endDate='', $affiliateId=0, $quoteTypeID=0, $uniqueCookie=0);
//                 array|false GetQzCookieAffiliateByCID($cookieID=0);
//                 array|false GetAllQzCookieAffiliates();
//                 bool AssertQzCookieAffiliate()
//
//                 void Close();
//                 string GetError();
//
// [CREATED BY]:   Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CQzCookieAffiliate
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQzCookieAffiliate
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQzCookieAffiliate($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

   $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQzCookieAffiliate
//
// [DESCRIPTION]:   Add new entry to the qzquote_affiliates table
//
// [PARAMETERS]:    $qzQuoteID=0, $affiliateID=0, $cookieID=0
//
// [RETURN VALUE]:  $qzQuoteAffiliatesID=0 or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQzCookieAffiliate($qzQuoteID=0, $affiliateID=0, $cookieID=0)
{

   if(! $this->AssertQzCookieAffiliate($qzQuoteID, $affiliateID, $cookieID))
      return 0;

   // check if ID exists in DB
   $sqlCmd = "SELECT id FROM ".SQL_QZQUOTE_AFFILIATES." WHERE qz_quote_id ='$qzQuoteID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if($this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QZQUOTE_AFFILIATE_ID_ALREADY_EXISTS");
      return 0;
   }

   $sqlCmd = "INSERT INTO ".SQL_QZQUOTE_AFFILIATES." (qz_quote_id,affiliate_id,cookie_id) VALUES ('$qzQuoteID','$affiliateID','$cookieID')";
   // echo "$sqlCmd <br>";
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   $qzQuoteAffiliatesID = $this->dbh->GetFieldValue("id");

   return $qzQuoteAffiliatesID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQzCookieAffiliate
//
// [DESCRIPTION]:   Update qzquote_affiliates table
//
// [PARAMETERS]:    $qzQuoteAffiliatesID=0, $qzQuoteID=0, $affiliateID=0, $cookieID=0
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQzCookieAffiliate($qzQuoteAffiliatesID=0, $qzQuoteID=0, $affiliateID=0, $cookieID=0)
{
//print "## $qzQuoteIDID, $qzQuoteID";

   if(! preg_match("/^\d+$/", $qzQuoteAffiliatesID))
   {
      $this->strERR = GetErrorString("INVALID_QZQUOTEAFFILIATEID_FIELD");
      return false;
   }

   if(! $this->AssertQzCookieAffiliate($qzQuoteID, $affiliateID, $cookieID))
      return false;

   // check if ID exists in DB
   $sqlCmd = "SELECT id FROM ".SQL_QZQUOTE_AFFILIATES." WHERE id='$qzQuoteAffiliatesID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QZQUOTEAFFILIATEID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $sqlCmd = "UPDATE ".SQL_QZQUOTE_AFFILIATES." SET id='$qzQuoteAffiliatesID', qz_quote_id='$qzQuoteID',affiliate_id='$affiliateID',cookie_id='$cookieID' WHERE id='$qzQuoteAffiliatesID'";

   //echo $sqlCmd ;
   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQzCookieAffiliate
//
// [DESCRIPTION]:   Delete an entry from qzquote_affiliates table
//
// [PARAMETERS]:    $qzQuoteAffiliatesID = 0
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteQzCookieAffiliate($qzQuoteAffiliatesID = 0)
{
   if(! preg_match("/^\d{1,20}$/", $qzQuoteAffiliatesID))
   {
      $this->strERR = GetErrorString("INVALID_QZQUOTEAFFILIATEID_FIELD");
      return false;
   }

   // check if ID exists in DB
   $sqlCmd = "SELECT id FROM ".SQL_QZQUOTE_AFFILIATES." WHERE id='$qzQuoteAffiliatesID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QZQUOTEAFFILIATEID_NOT_FOUND");
      return false;
   }

   $sqlCmd = "DELETE FROM ".SQL_QZQUOTE_AFFILIATES." WHERE id='$qzQuoteAffiliatesID'";

   //print "$sqlCmd";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQzCookieAffiliateByID
//
// [DESCRIPTION]:   Read data from qzquote_affiliates table and put it into an array variable
//
// [PARAMETERS]:    $qzQuoteAffiliatesID = 0
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQzCookieAffiliateByID($qzQuoteAffiliatesID = 0 )
{
   if(! preg_match("/^\d{1,20}$/", $qzQuoteAffiliatesID))
   {
      print_r ($qzQuoteIDID);
      $this->strERR = GetErrorString("INVALID_QZQUOTEAFFILIATEID_FIELD");
      return false;
   }

   $sqlCmd = "SELECT * FROM ".SQL_QZQUOTE_AFFILIATES." WHERE id='$qzQuoteAffiliatesID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QZQUOTEAFFILIATEID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]           = $this->dbh->GetFieldValue("id");
   $arrayResult["qz_quote_id"]  = $this->dbh->GetFieldValue("qz_quote_id");
   $arrayResult["affiliate_id"] = $this->dbh->GetFieldValue("affiliate_id");
   $arrayResult["cookie_id"]    = $this->dbh->GetFieldValue("cookie_id");

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQzCookieAffiliateByAID
//
// [DESCRIPTION]:   Read data from qzquote_affiliates table and put it into an array variable
//
// [PARAMETERS]:    $affiliateID=0
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQzCookieAffiliateByQID($affiliateID=0)
{
   if(! preg_match("/^\d{1,20}$/", $affiliateID))
   {
      $this->strERR = GetErrorString("INVALID_AFFILIATEID_FIELD");
      return false;
   }

   // check if we have this  cookie
   $sqlCmd = "SELECT * FROM ".SQL_QZQUOTE_AFFILIATES." WHERE affiliate_id='$affiliateID'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("AFFILIATEID_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   while($this->dbh->MoveNext())
   {
      $id = $this->dbh->GetFieldValue("id");

      $arrayResult[$id]["quote_type_id"]  = $this->dbh->GetFieldValue("quote_type_id");
      $arrayResult[$id]["affiliate_id"]   = $this->dbh->GetFieldValue("affiliate_id");
      $arrayResult[$id]["cookie_id"]      = $this->dbh->GetFieldValue("cookie_id");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQzCookieAffiliatesCount
//
// [DESCRIPTION]:   Read data from cookie_stats table and put it into an array variable
//
// [PARAMETERS]:    $startDate='', $endDate='', $affiliateId=0, $quoteTypeID=0, $uniqueCookie=0
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2005-11-30
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQzCookieAffiliatesCount($startDate='', $endDate='', $affiliateId=0, $quoteTypeID=0, $uniqueCookie=0)
{
   $this->lastSQLCMD = "SELECT count(";

   if(! $uniqueCookie)
      $this->lastSQLCMD .= "*)";
   else
      $this->lastSQLCMD .= " distinct qza.cookie_id)";

   $this->lastSQLCMD .= " as cnt FROM ".SQL_QZQUOTE_AFFILIATES." qza, ".SQL_QZQUOTES." qzq, ".SQL_LOGS." l WHERE 1";

   if($startDate)
      $this->lastSQLCMD .= " AND l.date>='$startDate'";

   if($endDate)
      $this->lastSQLCMD .= " AND l.date<='$endDate'";

   if($affiliateId)
      $this->lastSQLCMD .= " AND qza.affiliate_id='$affiliateId'";

   if($quoteTypeID)
      $this->lastSQLCMD .= " AND qzq.quote_type_id='$quoteTypeID'";
      
   $this->lastSQLCMD .= " AND qza.qz_quote_id=qzq.id AND qzq.log_id=l.id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return -1;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return -2;
   }

   return $this->dbh->GetFieldValue("cnt");;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllQzCookieAffiliates
//
// [DESCRIPTION]:   Read data from qzquote_affiliates table and put it into an array variable
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllQzCookieAffiliates()
{
   $sqlCmd = "SELECT * FROM ".SQL_QZQUOTE_AFFILIATES." ORDER BY id ";


   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QZCOOKIEAFFILIATESID_NOT_FOUND");
      return false;
   }

    $arrayResult = array();
   while($this->dbh->MoveNext())
   {
      $id = $this->dbh->GetFieldValue("id");

      $arrayResult[$id]["quote_type_id"]  = $this->dbh->GetFieldValue("quote_type_id");
      $arrayResult[$id]["affiliate_id"]   = $this->dbh->GetFieldValue("affiliate_id");
      $arrayResult[$id]["cookie_id"]      = $this->dbh->GetFieldValue("cookie_id");
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQzCookieAffiliate
//
// [DESCRIPTION]:   Check if all fields are OK and set the error string if needed
//
// [PARAMETERS]     $qzQuoteID, $affiliateID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertQzCookieAffiliate($qzQuoteID="", $affiliateID="", $cookieID="")
{
   $this->strERR = "";

   if(! preg_match("/^\d+$/", $qzQuoteID))
      $this->strERR .= GetErrorString("INVALID_QUOTEID_FIELD")."\n";

   if(! preg_match("/^\d+$/", $affiliateID))
      $this->strERR .= GetErrorString("INVALID_AFFILIATEID_FIELD")."\n";

    if(! preg_match("/^\d+$/", $cookieID))
      $this->strERR .= GetErrorString("INVALID_COOKIEID_FIELD")."\n";

   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-11-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

}// end of CQzCookieAffiliate class
?>
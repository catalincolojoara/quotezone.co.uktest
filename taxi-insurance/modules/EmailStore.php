<?php
/*****************************************************************************/
/*                                                                           */
/*  CEmailStore class interface                                       		  */
/*                                                                           */
/*  (C) 2006 Velnic Daniel (null@seopa.com)                                   */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";


//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CEmailStore
//
// [DESCRIPTION]:  CEmailStore class interface
//
// [FUNCTIONS]:    int   AddEmailStore($homeOwner=0, $realName='', $email='',$rule='65535')
//                 bool  UpdateHomeOwner($EmailStoreID='', $homeOwner='')
//                 bool  UpdateEmail($EmailStoreID='', $emailStore='')
//                 bool  UpdateRule($EmailStoreID='', $rule='')
//                 bool  UpdateQuoteUserID($EmailStoreID='', $quoteUserID='')
//                 bool  DeleteEmailStore($EmailStoreID=0)
//                 array GetAllEmailStore(&$arrayResult, $limit, $offset)
//                 array GetEmailStoreByRN($realName='')
//                 array GetEmailStoreByE($email='')
//                 array SearchEmailStoreByEmail($email='', $limit=0, $offset=0)
//                 array GetEmailStoreByID($EmailStoreID = 0)
//                 array GetAllHomeOwner($homeOwner='')
//                 str   GetRuleByEmail($email='')
//                 array SendEmailByRule($rule='')
//                 array SendQuoteByRule($rule='')
//                 int   GetSearchEmailsCount($search='')
//                 int   GetEmailsCount()
//
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CEmailStore
{
// database handler
var $dbh;         // database server handle
var $closeDB;     // close database flag

// class-internal variables
var $strERR;      // last SITE error string

var $emailTypes;
var $quoteTypes;
var $arraydecacat;

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CEmailStore
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CEmailStore($dbh=0)
{
	if($dbh)
	{
		$this->dbh = $dbh;
		$this->closeDB = false;
	}
	else
	{
		// default configuration
		$this->dbh = new CMySQL();

		if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
		{
			$this->strERR = $this->dbh->GetError();
			return;
		}

		$this->closeDB = true;
	}

	$this->strERR  = "";

	$this->emailTypes = array("top1" => 1, "top3" => 2, "renew" => 4, "campaign" => 8);
	$this->quoteTypes = array("car" => 1, "van" => 2, "home" => 4, "bike" => 8);

	$this->arraydecacat = array("1","2","3");
}


function GetArrayDeCacat()
{
	return $this->arraydecacat;
}

function GetAllEmailTypes()
{
   //$emailArray = array_flip($this->emailTypes);
   $emailArray = $this->emailTypes;

   return $emailArray;
}


function GetAllQuoteTypes()
{
   //$quoteArray = array_flip($this->quoteTypes);
   $quoteArray = $this->quoteTypes;

   return $quoteArray;
}


function GetEmailTypes($emaiType='')
{
   $email = $this->emailTypes;

   return $email[$emaiType];
}


function GetQuoteTypes($quoteType='')
{
   $quote = $this->quoteTypes;

   return $quote[$quoteType];
}


function GetEmailNameTypes($emaiType='')
{
   $email = array_flip($this->emailTypes);

   return $email[$emaiType];
}


function GetQuoteNameTypes($quoteType='')
{
   $quote = array_flip($this->quoteTypes);

   return $quote[$quoteType];
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddEmailStore
//
// [DESCRIPTION]:   Add new entry to the email_store table
//
// [PARAMETERS]:    $homeOwner=0, $realName='', $email=''
//
// [RETURN VALUE]:  ID or 0 in case of failure
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddEmailStore($homeOwner=0, $realName='', $email='', $quoteUserId="0",$rule='65535')
{
	if(! preg_match("/^\d+$/", $homeOwner))
	{
		$this->strERR = GetErrorString("INVALID_HOMEOWNER_FIELD");
		return 0;
	}

	if(! preg_match("/^\d+$/", $quoteUserId))
	{
		$this->strERR = GetErrorString("INVALID_QUOTE_USER_ID_FIELD");
		return 0;
	}

	if(empty($realName))
	{
		$this->strERR = GetErrorString("INVALID_REALNAME_FIELD");
		return 0;
	}

	if(!preg_match('/\.*\@.*\.[a-zA-Z]{2,3}/i',$email))
	{
		$this->strERR = GetErrorString("INVALID_EMAIL_FIELD");
		return 0;
	}

	$sqlCMD = "SELECT id FROM ".SQL_EMAIL_STORE." WHERE email = '$email' and quote_user_id='$quoteUserId'";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	if( $this->dbh->GetRows() > 0)
	{
		$this->strERR = GetErrorString("EMAIL_STORE_RECORD_ALLREADY_EXISTS");

		if(! $this->dbh->FetchRows())
		{
			$this->strERR = $this->dbh->GetError();
			return 0;
		}

		return $this->dbh->GetFieldValue('id');
	}

	$sqlCMD = "INSERT INTO ".SQL_EMAIL_STORE." (homeowner, real_name, email, rule, quote_user_id) VALUES ('$homeOwner','$realName','$email', '$rule','$quoteUserId')";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	$sqlCMD = "SELECT LAST_INSERT_ID() AS id";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	if(! $this->dbh->FetchRows())
	{
		$this->strERR = $this->dbh->GetError();
		return 0;
	}

	return $this->dbh->GetFieldValue("id");

}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateHomeOwner
//
// [DESCRIPTION]:   Update email_store set homeowner='$homeOwner'
//
// [PARAMETERS]:    $EmailStoreID='', $homeOwner=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateHomeOwner($EmailStoreID='', $homeOwner='')
{
	if(! preg_match("/^\d+$/", $EmailStoreID))
	{
		$this->strERR = GetErrorString("INVALID_EMAIL_STORE_ID_FIELD");
		return false;
	}

	if(! preg_match("/^\d+$/", $homeOwner))
	{
		$this->strERR = GetErrorString("INVALID_HOMEOWNER_FIELD");
		return false;
	}

	$sqlCMD = "SELECT * FROM ".SQL_EMAIL_STORE." where id='$EmailStoreID'";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	if(! $this->dbh->GetRows())
	{
		$this->strERR = GetErrorString("EMAIL_STORE_ID_NOT_FOUND");
		return false;
	}

	$sqlCMD = "UPDATE ".SQL_EMAIL_STORE." SET homeowner='$homeOwner' where id='$EmailStoreID'";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	return true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateEmail
//
// [DESCRIPTION]:   Update email_store
//
// [PARAMETERS]:    $EmailStoreID='', $emailStore=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateEmail($EmailStoreID='', $userName='', $emailStore='')
{
	if(! preg_match("/^\d+$/", $EmailStoreID))
	{
		$this->strERR = GetErrorString("INVALID_EMAIL_STORE_ID_FIELD");
		return false;
	}

	$sqlCMD = "SELECT * FROM ".SQL_EMAIL_STORE." where id='$EmailStoreID'";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	if(! $this->dbh->GetRows())
	{
		$this->strERR = GetErrorString("EMAIL_STORE_ID_NOT_FOUND");
		return false;
	}

	$sqlCMD = "UPDATE ".SQL_EMAIL_STORE." SET email='$emailStore',real_name='$userName' WHERE id='$EmailStoreID'";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	return true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateRule
//
// [DESCRIPTION]:   Update email_store set rule='$rule'
//
// [PARAMETERS]:    $EmailStoreID='', $rule=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Cezar LISTEVEANU (null@seopa.com) 2007-02-15
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateRule($EmailStoreID='', $rule='')
{
	if(! preg_match("/^\d+$/", $EmailStoreID))
	{
		$this->strERR = GetErrorString("INVALID_EMAIL_STORE_ID_FIELD");
		return false;
	}

	if(! preg_match("/^\d+$/", $rule))
	{
		$this->strERR = GetErrorString("INVALID_RULE_FIELD");
		return false;
	}

	$sqlCMD = "SELECT * FROM ".SQL_EMAIL_STORE." where id='$EmailStoreID'";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	if(! $this->dbh->GetRows())
	{
		$this->strERR = GetErrorString("EMAIL_STORE_ID_NOT_FOUND");
		return false;
	}

	$sqlCMD = "UPDATE ".SQL_EMAIL_STORE." SET rule='$rule' where id='$EmailStoreID'";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQuoteUserID
//
// [DESCRIPTION]:   Update email_store set quote_user_id=$quoteUserID
//
// [PARAMETERS]:    $EmailStoreID='', $rule=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Cezar LISTEVEANU (null@seopa.com) 2007-02-15
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQuoteUserID($EmailStoreID='', $quoteUserID=0)
{
   if(! preg_match("/^\d+$/", $EmailStoreID))
   {
      $this->strERR = GetErrorString("INVALID_EMAIL_STORE_ID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_USER_ID");
      return false;
   }

   $sqlCMD = "SELECT * FROM ".SQL_EMAIL_STORE." where id='$EmailStoreID'";

   if(! $this->dbh->Exec($sqlCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("EMAIL_STORE_ID_NOT_FOUND");
      return false;
   }

   $sqlCMD = "UPDATE ".SQL_EMAIL_STORE." SET quote_user_id='$quoteUserID' where id='$EmailStoreID'";

   if(! $this->dbh->Exec($sqlCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CalculateRule
//
// [DESCRIPTION]:
//
// [PARAMETERS]:
//
// [RETURN VALUE]:
//
// [CREATED BY]:    Cezar LISTEVEANU (null@seopa.com) 2007-02-15
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CalculateRule($emailTypes,$quoteTypes)
{

//	$this->emailTypes = array("top1" => 1, "top3" => 2, "renew" => 4, "campain" => 8);
//	$this->quoteTypes = array("car" => 1, "van" => 2, "home" => 4, "bike" => 8);


	if (!$emailTypes)
		$emailTypes = $this->GetAllEmailTypes();
	if (!$quoteTypes)
		$quoteTypes = $this->GetAllQuoteTypes();

	$sumEmails = 0;
	foreach ($emailTypes as $emailName=>$emailValue)
	{
		$sumEmails = $sumEmails | $emailValue;
	}

	$emailTypesCnt = count($emailTypes);

	for($i=$emailTypesCnt;$i<8;$i++)
	{
		$sumEmails = $sumEmails | pow(2,$i);
	}

//	$sumEmails = $sumEmails | 16 | 32 | 64 | 128;

	$sumQuotes = 0;
	foreach ($quoteTypes as $quoteName=>$quoteValue)
	{
		$sumQuotes = $sumQuotes | $quoteValue;
	}

	$quoteTypesCnt = count($quoteTypes);

	for ($i=$quoteTypesCnt;$i<8;$i++)
	{
		$sumQuotes = $sumQuotes | pow(2,$i);
	}

//	$sumQuotes = $sumQuotes | 16 | 32 | 64 | 128;

	$sendMails = bindec(decbin($sumQuotes).decbin($sumEmails));

/*
//	$sumQuotes = $car | $van | $home | $bike | 16 | 32 | 64 | 128;
//	$sumEmails = $top1 | $top3 | $renew | $campain  | 16 | 32 | 64 | 128;
	$sumQuotes = 1 | 2 | 4 | 8 | 16 | 32 | 64 | 128;
	$sumEmails = 1 | 2 | 4 | 8 | 16 | 32 | 64 | 128;
	$sendMails = bindec(decbin($sumQuotes).decbin($sumEmails));
*/
//	print "sumquotes: $sumQuotes <br>";
//	print "sumemails: $sumEmails <br>";

	return $sendMails;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteEmailStore
//
// [DESCRIPTION]:   Delete an entry from email_store table by ID
//
// [PARAMETERS]:    $EmailStoreID=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteEmailStore($EmailStoreID=0)
{
	if(! preg_match("/^\d+$/", $EmailStoreID))
	{
		$this->strERR = GetErrorString("INVALID_EMAIL_STORE_ID_FIELD");
		return 0;
	}

	$sqlCMD = "SELECT id FROM ".SQL_EMAIL_STORE." WHERE id='$EmailStoreID'";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	if(! $this->dbh->GetRows())
	{
		$this->strERR = GetErrorString("EMAIL_STORE_ID_NOT_FOUND");
		return false;
	}

	$sqlCMD = "DELETE FROM ".SQL_EMAIL_STORE." WHERE id='$EmailStoreID'";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllEmailStore
//
// [DESCRIPTION]:   Get all content of the table email_store
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllEmailStore(&$arrayResult,$limit, $offset)
{
	$sqlCMD = "SELECT * FROM ".SQL_EMAIL_STORE;

	if($limit)
   {
      if($offset)
         $sqlCMD .= " LIMIT $offset,$limit";
      else
         $sqlCMD .= " LIMIT $limit";
   }

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	if(! $this->dbh->GetRows())
	{
		$this->strERR = GetErrorString("EMAIL_STORE_NOT_FOUND");
		return false;
	}

	while($this->dbh->MoveNext())
	{
		$contentResult["id"]            = $this->dbh->GetFieldValue("id");
		$contentResult["real_name"]     = $this->dbh->GetFieldValue("real_name");
		$contentResult["email"]         = $this->dbh->GetFieldValue("email");
		$contentResult["homeowner"]     = $this->dbh->GetFieldValue("homeowner");
		$contentResult["rule"]          = $this->dbh->GetFieldValue("rule");
		$contentResult["quote_user_id"] = $this->dbh->GetFieldValue("quote_user_id");

		$arrayResult[$this->dbh->GetFieldValue("id")] = $contentResult;
	}

	//return $arrayResult;
	return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmailStoreIDByEmail
//
// [DESCRIPTION]:   Get a id record by email
//
// [PARAMETERS]:    $email=''
//
// [RETURN VALUE]:  $arrayResult["id"]
//
// [CREATED BY]:    Cezar Listeveanu (null@seopa.com) 15-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmailStoreIDByEmail($email='')
{
    if(!preg_match('/\.*\@.*\.[a-zA-Z]{2,3}/i',$email))
    {
            $this->strERR = GetErrorString("INVALID_EMAIL_FIELD");
            return false;
    }

    $this->lastSQLCMD = "SELECT * FROM ".SQL_EMAIL_STORE." WHERE email='$email'";

    if(! $this->dbh->Exec($this->lastSQLCMD))
    {
            $this->strERR = $this->dbh->GetError();
            return false;
    }

    if(! $this->dbh->FetchRows())
    {
            $this->strERR = GetErrorString("EMAIL_NOT_FOUND");
            return false;
    }

    $arrayResult["id"] = $this->dbh->GetFieldValue("id");

    return $arrayResult["id"];
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmailStoreIDByEmailAndQuoteUserId
//
// [DESCRIPTION]:   Get a id record by email
//
// [PARAMETERS]:    $email='',$quoteUserID=''
//
// [RETURN VALUE]:  $arrayResult["id"]
//
// [CREATED BY]:    Cezar Listeveanu (null@seopa.com) 15-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmailStoreIDByEmailAndQuoteUserId($email='',$quoteUserID='')
{
    if(!preg_match('/\.*\@.*\.[a-zA-Z]{2,3}/i',$email))
    {
            $this->strERR = GetErrorString("INVALID_EMAIL_FIELD");
            return false;
    }

    $this->lastSQLCMD = "SELECT * FROM ".SQL_EMAIL_STORE." WHERE email='$email' AND quote_user_id='$quoteUserID'";

    if(! $this->dbh->Exec($this->lastSQLCMD))
    {
            $this->strERR = $this->dbh->GetError();
            return false;
    }

    if(! $this->dbh->FetchRows())
    {
            $this->strERR = GetErrorString("EMAIL_NOT_FOUND");
            return false;
    }

    $arrayResult["id"] = $this->dbh->GetFieldValue("id");

    return $arrayResult["id"];
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmailStoreRuleByEmailAndQuoteUserID
//
// [DESCRIPTION]:   Returns the email rule by email
//
// [PARAMETERS]:    $email=''
//
// [RETURN VALUE]:  $emailRule
//
// [CREATED BY]:    Alexandru Furtuna (alexandru.furtuna@seopa.com) 27-05-2010
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmailStoreRuleByEmailAndQuoteUserID($email='',$quoteUserID='')
{
   
   if(!preg_match('/\.*\@.*\.[a-zA-Z]{2,3}/i',$email))
   {
      $this->strERR = GetErrorString("INVALID_EMAIL_FIELD");
      return false;
   }
   
   $this->lastSQLCMD = "SELECT * FROM ".SQL_EMAIL_STORE." WHERE email='$email' AND quote_user_id='$quoteUserID'";
   
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
   
   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("EMAIL_NOT_FOUND");
      return false;
   }
   
   $emailRule = $this->dbh->GetFieldValue("rule");
 
   return $emailRule;
   
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmailStoreIDByE
//
// [DESCRIPTION]:   Get a id record by email
//
// [PARAMETERS]:    $email=''
//
// [RETURN VALUE]:  $arrayResult["id"]
//
// [CREATED BY]:    Cezar Listeveanu (null@seopa.com) 15-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllEmailStoreIDByEmail($email='',&$arrayResult)
{
    if(!preg_match('/\.*\@.*\.[a-zA-Z]{2,3}/i',$email))
    {
            $this->strERR = GetErrorString("INVALID_EMAIL_FIELD");
            return false;
    }

    $this->lastSQLCMD = "SELECT * FROM ".SQL_EMAIL_STORE." WHERE email='$email'";

    if(! $this->dbh->Exec($this->lastSQLCMD))
    {
            $this->strERR = $this->dbh->GetError();
            return false;
    }

	while($this->dbh->MoveNext())
	{
		$contentResult["id"]            = $this->dbh->GetFieldValue("id");
		$contentResult["real_name"]     = $this->dbh->GetFieldValue("real_name");
		$contentResult["email"]         = $this->dbh->GetFieldValue("email");
		$contentResult["homeowner"]     = $this->dbh->GetFieldValue("homeowner");
		$contentResult["rule"]          = $this->dbh->GetFieldValue("rule");
		$contentResult["quote_user_id"] = $this->dbh->GetFieldValue("quote_user_id");

		$arrayResult[$this->dbh->GetFieldValue("id")] = $contentResult;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmailStoreByRN
//
// [DESCRIPTION]:   Get a record by real_name
//
// [PARAMETERS]:    $realName='',
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmailStoreByRN($realName='') // to be removed when new system gets online
{
	if(empty($realName))
	{
		$this->strERR = GetErrorString("INVALID_REALNAME_FIELD");
		return false;
	}

	$sqlCMD = "SELECT * FROM ".SQL_EMAIL_STORE." WHERE real_name='$realName'";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	if(! $this->dbh->FetchRows())
	{
		$this->strERR = GetErrorString("REALNAME_NOT_FOUND");
		return false;
	}

	$arrayResult["id"]        = $this->dbh->GetFieldValue("id");
	$arrayResult["real_name"] = $this->dbh->GetFieldValue("real_name");
	$arrayResult["email"]     = $this->dbh->GetFieldValue("email");
	$arrayResult["homeowner"] = $this->dbh->GetFieldValue("homeowner");
	$arrayResult["rule"]      = $this->dbh->GetFieldValue("rule");

	return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmailStoreByRN
//
// [DESCRIPTION]:   Get a record by real_name
//
// [PARAMETERS]:    $realName='',
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllEmailStoreByRN($realName='')
{
	if(empty($realName))
	{
		$this->strERR = GetErrorString("INVALID_REALNAME_FIELD");
		return false;
	}

	$sqlCMD = "SELECT * FROM ".SQL_EMAIL_STORE." WHERE real_name='$realName'";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	while($this->dbh->MoveNext())
	{
		$contentResult["id"]            = $this->dbh->GetFieldValue("id");
		$contentResult["real_name"]     = $this->dbh->GetFieldValue("real_name");
		$contentResult["email"]         = $this->dbh->GetFieldValue("email");
		$contentResult["homeowner"]     = $this->dbh->GetFieldValue("homeowner");
		$contentResult["rule"]          = $this->dbh->GetFieldValue("rule");
		$contentResult["quote_user_id"] = $this->dbh->GetFieldValue("quote_user_id");

		$arrayResult[$this->dbh->GetFieldValue("id")] = $contentResult;
	}
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SearchEmailStoreByEmail
//
// [DESCRIPTION]:   Read data from users table and put it into an array variable
//
// [PARAMETERS]:    IN:  $email='', $limit=0, $offset=0
//                  OUT: $arrayResult
//
// [RETURN VALUE]:  array | false in case search failed
//
// [CREATED BY]:   Cezar LISTEVEANU (null@seopa.com) 2007-02-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SearchEmailStoreByEmail($email='', $limit=0, $offset=0)
{
   if(empty($email))
	{
		$this->strERR = GetErrorString("INVALID_EMAIL_FIELD");
		return false;
	}

   $sqlCmd = "SELECT * FROM ".SQL_EMAIL_STORE." WHERE email LIKE '%$email%'";

   if($limit)
   {
      if($offset)
         $sqlCmd .= " LIMIT $offset,$limit";
      else
         $sqlCmd .= " LIMIT $limit";
   }

   if(! $this->dbh->Exec($sqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }
//print $sqlCmd;
   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("EMAIL_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
   {
      $emailID                                = $this->dbh->GetFieldValue("id");
      $arrayResult[$emailID]["id"]            = $this->dbh->GetFieldValue("id");
      $arrayResult[$emailID]["homeowner"]     = $this->dbh->GetFieldValue("homeowner");
      $arrayResult[$emailID]["real_name"]     = $this->dbh->GetFieldValue("real_name");
      $arrayResult[$emailID]["email"]         = $this->dbh->GetFieldValue("email");
      $arrayResult[$emailID]["rule"]          = $this->dbh->GetFieldValue("rule");
      $arrayResult[$emailID]["quote_user_id"] = $this->dbh->GetFieldValue("quote_user_id");
   }

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmailStoreByID
//
// [DESCRIPTION]:   Get a record by id
//
// [PARAMETERS]:    $EmailStoreID = 0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmailStoreByID($EmailStoreID = 0)
{
	if(! preg_match("/^\d+$/", $EmailStoreID))
	{
		$this->strERR = GetErrorString("INVALID_ID_FIELD");
		return false;
	}

	$sqlCMD = "SELECT * FROM ".SQL_EMAIL_STORE." where id='$EmailStoreID'";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	if(! $this->dbh->FetchRows())
	{
		$this->strERR = GetErrorString("ID_NOT_FOUND");
		return false;
	}

	$arrayResult["id"]            = $this->dbh->GetFieldValue("id");
	$arrayResult["real_name"]     = $this->dbh->GetFieldValue("real_name");
	$arrayResult["email"]         = $this->dbh->GetFieldValue("email");
	$arrayResult["homeowner"]     = $this->dbh->GetFieldValue("homeowner");
	$arrayResult["rule"]          = $this->dbh->GetFieldValue("rule");
	$arrayResult["quote_user_id"] = $this->dbh->GetFieldValue("quote_user_id");

	return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmailStoreByID
//
// [DESCRIPTION]:   Get a record by id
//
// [PARAMETERS]:    $EmailStoreID = 0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmailStoreByIDOld($EmailStoreID = 0)
{
	if(! preg_match("/^\d+$/", $EmailStoreID))
	{
		$this->strERR = GetErrorString("INVALID_ID_FIELD");
		return false;
	}

	$sqlCMD = "SELECT * FROM ".SQL_EMAIL_STORE_OLD." where id='$EmailStoreID'";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	if(! $this->dbh->FetchRows())
	{
		$this->strERR = GetErrorString("ID_OLD_NOT_FOUND");
		return false;
	}

	$arrayResult["id"]            = $this->dbh->GetFieldValue("id");
	$arrayResult["real_name"]     = $this->dbh->GetFieldValue("real_name");
	$arrayResult["email"]         = $this->dbh->GetFieldValue("email");
	$arrayResult["homeowner"]     = $this->dbh->GetFieldValue("homeowner");
	$arrayResult["rule"]          = $this->dbh->GetFieldValue("rule");
	$arrayResult["quote_user_id"] = $this->dbh->GetFieldValue("quote_user_id");

	return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmailStoreByID
//
// [DESCRIPTION]:   Get a record by id
//
// [PARAMETERS]:    $EmailStoreID = 0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmailStoreByEmailAndQuoteUserId($email='',$quote_user_id = 0)
{
	if(! preg_match("/^\d+$/", $quote_user_id))
	{
		$this->strERR = GetErrorString("INVALID_QUOTE_USER_ID_FIELD");
		return false;
	}

	$sqlCMD = "SELECT * FROM ".SQL_EMAIL_STORE." where email='$email' AND quote_user_id='$quote_user_id'";

	if(! $this->dbh->Exec($sqlCMD,true))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	if(! $this->dbh->FetchRows())
	{
		$this->strERR = GetErrorString("ID_NOT_FOUND");
		return false;
	}

	$arrayResult["id"]            = $this->dbh->GetFieldValue("id");
	$arrayResult["real_name"]     = $this->dbh->GetFieldValue("real_name");
	$arrayResult["email"]         = $this->dbh->GetFieldValue("email");
	$arrayResult["homeowner"]     = $this->dbh->GetFieldValue("homeowner");
	$arrayResult["rule"]          = $this->dbh->GetFieldValue("rule");
	$arrayResult["quote_user_id"] = $this->dbh->GetFieldValue("quote_user_id");

	return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmailStoreByID
//
// [DESCRIPTION]:   Get a record by id
//
// [PARAMETERS]:    $EmailStoreID = 0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmailStoreByQuoteUserId($quote_user_id = 0)
{
	if(! preg_match("/^\d+$/", $quote_user_id))
	{
		$this->strERR = GetErrorString("INVALID_QUOTE_USER_ID_FIELD");
		return false;
	}

	$sqlCMD = "SELECT * FROM ".SQL_EMAIL_STORE." where quote_user_id='$quote_user_id'";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	if(! $this->dbh->FetchRows())
	{
		$this->strERR = GetErrorString("ID_NOT_FOUND");
		return false;
	}

	$arrayResult["id"]            = $this->dbh->GetFieldValue("id");
	$arrayResult["real_name"]     = $this->dbh->GetFieldValue("real_name");
	$arrayResult["email"]         = $this->dbh->GetFieldValue("email");
	$arrayResult["homeowner"]     = $this->dbh->GetFieldValue("homeowner");
	$arrayResult["rule"]          = $this->dbh->GetFieldValue("rule");
	$arrayResult["quote_user_id"] = $this->dbh->GetFieldValue("quote_user_id");

	return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllHomeOwner
//
// [DESCRIPTION]:   Get all homeowner
//
// [PARAMETERS]:    $homeOwner=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllHomeOwner($homeOwner='')
{
	if(! preg_match("/^\d+$/", $homeOwner))
	{
		$this->strERR = GetErrorString("INVALID_HOMEOWNER_FIELD");
		return 0;
	}

	$sqlCMD = "SELECT * FROM ".SQL_EMAIL_STORE." where homeowner='$homeOwner'";

	if(! $this->dbh->Exec($sqlCMD))
	{
		$this->strERR = $this->dbh->GetError();
		return false;
	}

	if(! $this->dbh->GetRows())
	{
		$this->strERR = GetErrorString("HOMEOWNER_NOT_FOUND");
		return false;
	}

	while($this->dbh->MoveNext())
	{
		$contentResult["id"]            = $this->dbh->GetFieldValue("id");
		$contentResult["real_name"]     = $this->dbh->GetFieldValue("real_name");
		$contentResult["email"]         = $this->dbh->GetFieldValue("email");
		$contentResult["homeowner"]     = $this->dbh->GetFieldValue("homeowner");
		$contentResult["rule"]          = $this->dbh->GetFieldValue("rule");
		$contentResult["quote_user_id"] = $this->dbh->GetFieldValue("quote_user_id");

		$arrayResult[$this->dbh->GetFieldValue("id")] = $contentResult;
	}

	return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRuleByEmail
//
// [DESCRIPTION]:   Read data from email_store table
//
// [PARAMETERS]:    $email
//
// [RETURN VALUE]:  $rule
//
// [CREATED BY]:   Cezar LISTEVEANU (null@seopa.com) 2007-02-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetRuleByEmail($email='') // to be removed when new system gets online
{
   if(empty($email))
   {
      $this->strERR = GetErrorString("INVALID_EMAIL_FIELD");
      return false;
   }

   $sqlCMD = "SELECT rule FROM ".SQL_EMAIL_STORE." WHERE email='$email' LIMIT 1";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("EMAIL_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $rule = $this->dbh->GetFieldValue("rule");

   return $rule;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRuleByEmail
//
// [DESCRIPTION]:   Read data from email_store table
//
// [PARAMETERS]:    $email
//
// [RETURN VALUE]:  $rule
//
// [CREATED BY]:   Cezar LISTEVEANU (null@seopa.com) 2007-02-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllRuleByEmail($email='',&$arrayResult) // to be removed when new system gets online
{
   if(empty($email))
   {
      $this->strERR = GetErrorString("INVALID_EMAIL_FIELD");
      return false;
   }

   $sqlCMD = "SELECT rule FROM ".SQL_EMAIL_STORE." WHERE email='$email' LIMIT 1";

   if(! $this->dbh->Exec($sqlCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("EMAIL_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

	while($this->dbh->MoveNext())
	{
		$contentResult["rule"]          = $this->dbh->GetFieldValue("rule");

		$arrayResult[$this->dbh->GetFieldValue("id")] = $contentResult;
	}

	return true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SendEmailByRule
//
// [DESCRIPTION]:
//
// [PARAMETERS]:    $rule
//
// [RETURN VALUE]:  Array (with the parameters that belong to the email) or false in case of failure
//
// [CREATED BY]:   Cezar LISTEVEANU (null@seopa.com) 2007-02-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SendEmailByRule($rule='')
{
   $ruleBin = decbin($rule);

   $emailTypeDec = bindec(substr($ruleBin,strlen($ruleBin)/2));

   /*print "Email Type: $emailTypeDec<br>";*/

   $emailTypesArray = $this->emailTypes;

   $emailResult = array();

   foreach($emailTypesArray as $key=>$value)
   {
      if(($emailTypeDec & $value) == $value)
      {
         array_push($emailResult, $value);
      }
   }

   $arrayResult1 = array_diff($emailTypesArray,$emailResult);
   $arrayResult  = array_diff($emailTypesArray,$arrayResult1);

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SendQuoteByRule
//
// [DESCRIPTION]:
//
// [PARAMETERS]:    $rule
//
// [RETURN VALUE]:  Array (with the parameters that belong to the email) or false in case of failure
//
// [CREATED BY]:   Cezar LISTEVEANU (null@seopa.com) 2007-02-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SendQuoteByRule($rule='')
{
   $ruleBin = decbin($rule);

   $quoteTypeDec = bindec(substr($ruleBin,0,strlen($ruleBin)/2));

   /*print "Quote Type: $quoteTypeDec<br>";*/

   $quoteTypesArray = $this->quoteTypes;

   $quoteResult = array();

   foreach($quoteTypesArray as $key=>$value)
   {
      if(($quoteTypeDec & $value) == $value)
      {
         array_push($quoteResult, $value);
      }
   }

   $arrayResult1 = array_diff($quoteTypesArray, $quoteResult);
   $arrayResult  = array_diff($quoteTypesArray, $arrayResult1);

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSearchEmailsCount
//
// [DESCRIPTION]:   Get number of searched users
//
// [PARAMETERS]:    IN:  $search=''
//                  OUT: $cnt
//
// [RETURN VALUE]:  int | false in case search failed
//
// [CREATED BY]:   Cezar LISTEVEANU (null@seopa.com) 2007-02-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSearchEmailsCount($search='')
{
   $sqlCmd= "SELECT COUNT(*) AS cnt FROM ".SQL_EMAIL_STORE." WHERE email LIKE '%$search%'";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
      return 0;

   $cnt = $this->dbh->GetFieldValue("cnt");

   if(! $cnt)
      $cnt = 0;

   return $cnt;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetEmailsCount
//
// [DESCRIPTION]:   Get number of users
//
// [PARAMETERS]:    OUT: $cnt
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:   Cezar LISTEVEANU (null@seopa.com) 2007-02-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetEmailsCount()
{
   $sqlCmd= "SELECT COUNT(id) AS cnt FROM ".SQL_EMAIL_STORE."";

   if(! $this->dbh->Exec($sqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
      return 0;

   $cnt = $this->dbh->GetFieldValue("cnt");

   if(! $cnt)
      $cnt = 0;


   return $cnt;

}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
	return $this->strERR;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Velnic Daniel (null@seopa.com) 14-02-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
	if($this->closeDB)
	$this->dbh->Close();

	return;
}

function GenerateAuthenticityCode($email)
{
	$asciiCodes = array(
		"42" => "-",
		"46" => ".",
		"48" => "0",
		"49" => "1",
		"50" => "2",
		"51" => "3",
		"52" => "4",
		"53" => "5",
		"54" => "6",
		"55" => "7",
		"56" => "8",
		"57" => "9",
		"64" => "@",
		"65" => "A",
		"66" => "B",
		"67" => "C",
		"68" => "D",
		"69" => "E",
		"70" => "F",
		"71" => "G",
		"72" => "H",
		"73" => "I",
		"74" => "J",
		"75" => "K",
		"76" => "L",
		"77" => "M",
		"78" => "N",
		"79" => "O",
		"80" => "P",
		"81" => "Q",
		"82" => "R",
		"83" => "S",
		"84" => "T",
		"85" => "U",
		"86" => "V",
		"87" => "W",
		"88" => "X",
		"89" => "Y",
		"90" => "Z",
		"95" => "_",
		"97" => "a",
		"98" => "b",
		"99" => "c",
		"100" => "d",
		"101" => "e",
		"102" => "f",
		"103" => "g",
		"104" => "h",
		"105" => "i",
		"106" => "j",
		"107" => "k",
		"108" => "l",
		"109" => "m",
		"110" => "n",
		"111" => "o",
		"112" => "p",
		"113" => "q",
		"114" => "r",
		"115" => "s",
		"116" => "t",
		"117" => "u",
		"118" => "v",
		"119" => "w",
		"120" => "x",
		"121" => "y",
		"122" => "z",
		);

	$ourCodes = array(
		"01" => "A",
		"02" => "B",
		"03" => "C",
		"04" => "D",
		"05" => "E",
		"06" => "F",
		"07" => "G",
		"08" => "H",
		"09" => "I",
		"10" => "J",
		"11" => "K",
		"12" => "L",
		"13" => "M",
		"14" => "N",
		"15" => "O",
		"16" => "P",
		"17" => "Q",
		"18" => "R",
		"19" => "S",
		"20" => "T",
		"21" => "U",
		"22" => "V",
		"23" => "W",
		"24" => "X",
		"25" => "Y",
		"26" => "Z",
		"27" => "a",
		"28" => "b",
		"29" => "c",
		"30" => "d",
		"31" => "e",
		"32" => "f",
		"33" => "g",
		"34" => "h",
		"35" => "i",
		"36" => "j",
		"37" => "k",
		"38" => "l",
		"39" => "m",
		"40" => "n",
		"41" => "o",
		"42" => "p",
		"43" => "q",
		"44" => "r",
		"45" => "s",
		"46" => "t",
		"47" => "u",
		"48" => "v",
		"49" => "w",
		"50" => "x",
		"51" => "y",
		"52" => "z",
		"53" => "0",
		"54" => "1",
		"55" => "2",
		"56" => "3",
		"57" => "4",
		"58" => "5",
		"59" => "6",
		"60" => "7",
		"61" => "8",
		"62" => "9",
		"63" => "A",
		"64" => "B",
		"65" => "C",
		"66" => "D",
		"67" => "E",
		"68" => "F",
		"69" => "G",
		"70" => "H",
		"71" => "I",
		"72" => "J",
		"73" => "K",
		"74" => "L",
		"75" => "M",
		"76" => "N",
		"77" => "O",
		"78" => "P",
		"79" => "Q",
		"80" => "R",
		"81" => "S",
		"82" => "T",
		"83" => "U",
		"84" => "V",
		"85" => "W",
		"86" => "X",
		"87" => "Y",
		"88" => "Z",
		"89" => "a",
		"90" => "b",
		"91" => "c",
		"92" => "d",
		"93" => "e",
		"94" => "f",
		"95" => "g",
		"96" => "h",
		"97" => "i",
		"98" => "j",
		"99" => "k",
		);

	$invAsciiCodes = array_flip($asciiCodes);

	// email conversion

	if (!$email)
		$email = "test@test.com";

	$splitEmail = preg_split('//', $email, -1, PREG_SPLIT_NO_EMPTY);

//	print "<pre>";
//	print_r($splitEmail);

	$encodedMail = '';

	foreach ($splitEmail as $key=>$char)
	{
		$encodedMail .= $invAsciiCodes[$char];
	}

//	print "<hr>";
//	print $encodedMail;

	// END email conversion

	// password conversion

	$password = "quotezone";

	$splitPassw = preg_split('//', $password, -1, PREG_SPLIT_NO_EMPTY);

	$encodedPassw = "";

	foreach ($splitPassw as $key=>$char)
	{
		$encodedPassw .= $invAsciiCodes[$char];
	}

//	print "<hr>";
//	print $encodedPassw;

	// END password conversion

	// email & password intercalation

	$splitEncEmail = preg_split('//', $encodedMail, -1, PREG_SPLIT_NO_EMPTY);
	$splitEncPassw = preg_split('//', $encodedPassw, -1, PREG_SPLIT_NO_EMPTY);

	$intercalation1st = "";

	for ($i=0; $i<count($splitEncEmail); $i++)
	{
		if (array_key_exists($i, $splitEncEmail))
			$intercalation1st .= $splitEncEmail[$i];

		if (array_key_exists($i, $splitEncPassw))
			$intercalation1st .= $splitEncPassw[$i];
	}

//	print "<hr>";
//	print $intercalation1st;

	$splitInterc1st = preg_split('//', $intercalation1st, -1, PREG_SPLIT_NO_EMPTY);

	$emailParts = explode("@",$email);
	$nrToMultiply = strlen($emailParts[0]);

	$interc1final = "";

	$counter = 1;

	foreach ($splitInterc1st as $key=>$value)
	{
		$interc1final .= $value * $counter;

		$counter++;
		if ($counter>$nrToMultiply)
			$counter = 1;
	}

//	print "<hr>";
//	print $interc1final;

	// END email & password intercalation

	// first intercalation & password intercalation

	$splitInterc1 = preg_split('//', $interc1final, -1, PREG_SPLIT_NO_EMPTY);

	$intercalation2nd = '';

	for ($i=0; $i<count($splitInterc1); $i++)
	{
		if (array_key_exists($i, $splitInterc1))
			$intercalation2nd .= $splitInterc1[$i];

		if (array_key_exists($i, $splitEncPassw))
			$intercalation2nd .= $splitEncPassw[$i];
	}

//	print "<hr>";
//	print $intercalation2nd;

	$emailParts1 = explode(".", $emailParts[1]);

	$nrToMultiply2 = '';

	for ($i=0; $i<count($emailParts1)-1;$i++)
	{
		$nrToMultiply2 += strlen($emailParts1[$i]);
	}

	$interc2final = "";

	$splitInterc2nd = preg_split('//', $intercalation2nd, -1, PREG_SPLIT_NO_EMPTY);

	$counter = 1;

	foreach ($splitInterc2nd as $key=>$value)
	{
		$interc2final .= $value * $counter;

		$counter++;
		if ($counter>$nrToMultiply2)
			$counter = 1;
	}

//	print "<hr>";
//	print $interc2final;

	// END first intercalation & password intercalation

	// resulting string

	$finalArray = preg_split('//', $interc2final, -1, PREG_SPLIT_NO_EMPTY);

	$resultString = "";

	$counter = count($finalArray)-1;

	for ($i=0;$i<(count($finalArray)/2)-1; $i++)
	{
		$index = $finalArray[$counter].$finalArray[$counter-1];
		$resultString .= $ourCodes[$index];
		$counter-=2;
	}

//	print "<hr>";
//	print $resultString;

	// END resulting string

	return $resultString;
}

function CreateUnsubscribeLinkParams($email,$emailParams,$quoteParams)
{
	$authcode   = $this->GenerateAuthenticityCode(str_replace(";",".",$emailParams)."@".str_replace(";",".",$quoteParams));
	$emauthcode = $this->GenerateAuthenticityCode($email);

	$resultString = "emailaddress=$email&emauthcode=$emauthcode&email=$emailParams&quote=$quoteParams&authcode=$authcode";

	return $resultString;
}

}

?>

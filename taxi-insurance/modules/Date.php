<?php

/*****************************************************************************/
/*                                                                           */
/* Date class implementation                                                 */
/*                                                                           */
/*  (C) 2004 Eugen Savin (eugen@claspsolutions.com)                          */
/*                                                                           */
/*****************************************************************************/

define("DATE_INCLUDED", "1");

if(DEBUG_MODE)
   error_reporting(E_ALL);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CDate
//
// [DESCRIPTION]:  Support for operations with date types, PHP version
//
// [FUNCTIONS]:    int    MakeTime($year=0, $month=0, $day=0, $hour=0, $min=0, $sec=0);
//                 int    GetTime();
//                 string GetDateTime($time = 0);
//                 string GetDateTimeUK($time = 0);
//                 string GetDate($time = 0);
//                 string GetDateUK($time = 0);
//                 string GetDateString($time = 0);
//                 string GetDateShortString($time = 0);
//
//                 int    GetYear($time = 0);
//                 int    GetMonth($time = 0);
//                 string GetMonthString($time = 0);
//                 int    GetMonths();
//                 int    GetShortMonths();
//
//                 int    GetDay($time = 0);
//                 int    GetWeekDay($time = 0);
//                 int    GetMonthDay($time = 0);
//                 string GetDayString($time = 0);
//                 int    GetDaysInMonth($month = 0);
//                 int    GetDaysInYear($year = 0);
//
//                 int GetHour($time = 0)
//                 int GetMinutes($time = 0)
//                 int GetSeconds($time = 0)
//
//                 bool   IsLeapYear($year = 0);
//                 string GetHMS($time = 0, $separator="", $fields=3);
//                 int    Days360($startDate="", $endDate="");
//
//                 TODO
//                 GetDateMinusDays();
//                 GetDatePlusDays();
//                 DifferenceDays();
//
//                 CompareDates($timeFirst=0, $timeSecond = 0);
//                 ParseISODate($isoDate = "YYYY-MM-DD");
//
//                 GetError(;
//
// [CREATED BY]:   Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CDate
{
    var $days;        // days of week names array
    var $shortDays;   // days of week names array, short names
    var $months;      // month names array
    var $shortMonths; // month names array, short names

    var $strERR;      // last error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CDate
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CDate()
{
   $this->days = array( '0' => 'Sunday',
                        '1' => 'Monday',
                        '2' => 'Tuesday',
                        '3' => 'Wednesday',
                        '4' => 'Thursday',
                        '5' => 'Friday',
                        '6' => 'Saturday'
                      );

   $this->shortDays = array( '0' => 'Sun',
                             '1' => 'Mon',
                             '2' => 'Tue',
                             '3' => 'Wed',
                             '4' => 'Thu',
                             '5' => 'Fri',
                             '6' => 'Sat'
                      );

   $this->months = array( '01' => 'January',
                          '02' => 'February',
                          '03' => 'March',
                          '04' => 'April',
                          '05' => 'May',
                          '06' => 'June',
                          '07' => 'July',
                          '08' => 'August',
                          '09' => 'September',
                          '10' => 'October',
                          '11' => 'November',
                          '12' => 'December'
                        );

   $this->shortMonths = array( '01' => 'Jan',
                               '02' => 'Feb',
                               '03' => 'Mar',
                               '04' => 'Apr',
                               '05' => 'May',
                               '06' => 'Jun',
                               '07' => 'Jul',
                               '08' => 'Aug',
                               '09' => 'Sep',
                               '10' => 'Oct',
                               '11' => 'Nov',
                               '12' => 'Dec'
                        );

   $this->strERR = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: MakeTime
//
// [DESCRIPTION]:   Return the given args time measured in the number of seconds
//                  since the Unix Epoch (January 1 1970 00:00:00 GMT).
//
// [PARAMETERS]:    $year=0, $month=0, $day=0, $hour=0, $min=0, $sec=0
//
// [RETURN VALUE]:  The number of seconds as integer
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function MakeTime($year=0, $month=0, $day=0, $hour=0, $min=0, $sec=0)
{
   return mktime($hour, $min, $sec, $month, $day, $year);
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTime
//
// [DESCRIPTION]:   Return the current time measured in the number of seconds
//                  since the Unix Epoch (January 1 1970 00:00:00 GMT).
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  The number of seconds as integer
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTime()
{
   return time();
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDateTime
//
// [DESCRIPTION]:   Return the date formatted as ISO datetime: YYYY-MM-DD HH:MM:SS
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The datetime as string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDateTime($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   return date("Y-m-d H:i:s", $time);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDateTimeUK
//
// [DESCRIPTION]:   Return the date formatted as UK datetime: YYYY-MM-DD HH:MM:SS
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The datetime as string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDateTimeUK($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   return date("d-m-Y H:i:s", $time);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDate
//
// [DESCRIPTION]:   Return the date formatted as ISO date: YYYY-MM-DD
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The ISO date as string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDate($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   return date("Y-m-d", $time);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDateUK
//
// [DESCRIPTION]:   Return the date formatted as UK date: YYYY-DD-MM
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The UK date as string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDateUK($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   return date("d-m-Y", $time);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDateString
//
// [DESCRIPTION]:   Return the date formatted as a literal date: Sat Mar 10 15:16:08
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The date as string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDateString($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   return date("D M j Y H:i:s", $time);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDateShortString
//
// [DESCRIPTION]:   Return the date formatted as a literal date: Sat Mar 10
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The date as string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDateShortString($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   return date("D M j Y", $time);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetYear
//
// [DESCRIPTION]:   Return the year
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The year as integer, 4 digits
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetYear($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   return date("Y", $time);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMonth
//
// [DESCRIPTION]:   Return the month
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The month as integer, 2 digits
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMonth($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   return date("m", $time);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMonthString
//
// [DESCRIPTION]:   Return the month
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The month as string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMonthString($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   return date("F", $time);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMonths
//
// [DESCRIPTION]:   Return the months of the year
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Array ( monthNr => monthName )
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMonths()
{
   return $this->months;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetShortMonths
//
// [DESCRIPTION]:   Return the short months of the year
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Array ( monthNr => monthName )
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetShortMonths()
{
   return $this->shortMonths;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDay
//
// [DESCRIPTION]:   Return the day
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The day as integer, 2 digits
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDay($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   return date("d", $time);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetWeekDay
//
// [DESCRIPTION]:   Return the week day
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The week day as integer, 1 digit
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetWeekDay($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   return date("w", $time);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMonthDay
//
// [DESCRIPTION]:   Return the day of the month
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The month day as integer, 2 digits
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMonthDay($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   $monthDay = date("j", $time);

   if($monthDay < 10)
      $monthDay = "0".$monthDay;

   return $monthDay;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDayString
//
// [DESCRIPTION]:   Return the day
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The day as string, 3 letters
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDayString($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   return date("D", $time);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDaysInMonth
//
// [DESCRIPTION]:   Return the number of days for a specified year and month
//
// [PARAMETERS]:    $month = 0, $year = 0
//
// [RETURN VALUE]:  The number of month's days, < 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDaysInMonth($month = 0, $year = 0)
{
   $month = intval($month);

   if(! ereg("[0-9]+", $month))
      return -1;

   if(! ereg("[0-9]+", $year))
      return -2;

   if($month > 12 || $month < 1)
      return -3;

   $daysInMonth = array(0,31,28,31,30,31,30,31,31,30,31,30,31);

   $days = $daysInMonth[$month];

   if($month == 2)
      $days += $this->IsLeapYear($year);

   return $days;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDaysInYear
//
// [DESCRIPTION]:   Return the number of days for a specified year
//
// [PARAMETERS]:    $year = 0
//
// [RETURN VALUE]:  The number of year's days, < 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDaysInYear($year = 0)
{
   if(! ereg("[0-9]+", $year))
      return -1;

   $daysInYear = 365 + $this->IsLeapYear($year);

   return $daysInYear;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetHour
//
// [DESCRIPTION]:   Return the hour
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The hour as integer, 2 digits
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetHour($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   return date("H", $time);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetMinutes
//
// [DESCRIPTION]:   Return the minutes
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The minutes as integer, 2 digits
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetMinutes($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   return date("i", $time);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSeconds
//
// [DESCRIPTION]:   Return the seconds
//
// [PARAMETERS]:    $time = 0
//
// [RETURN VALUE]:  The seconds as integer, 2 digits
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSeconds($time = 0)
{
   if(! $time)
      $time = $this->GetTime();

   return date("s", $time);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: IsLeapYear
//
// [DESCRIPTION]:   Detects if the year is leap or not
//
// [PARAMETERS]:    $year = 0
//
// [RETURN VALUE]:  1 if the year is leap, 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function IsLeapYear($year = 0)
{
   if(! ereg("[0-9]+", $year))
      return 0;

   if($year %4 == 0)
   {
      if($year % 100 == 0)
      {
         if($year % 400 == 0)
            return 1;
         else
            return 0;
      }

      return 1;
   }

   return 0;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetHMS
//
// [DESCRIPTION]:   Builds a string like 20h 20m 32s
//
// [PARAMETERS]:    $seconds=0, $separator="", $fields=3
//
// [RETURN VALUE]:  string
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetHMS($seconds=0, $separator="", $fields=3)
{
   if($seconds < 0)
      return "0s";

   if($fields < 0 || $fields > 5)
      $fields = 3;

   $secs   = $seconds;
   $mins   = "0";
   $hrs    = "0";
   $days   = "0";
   $months = "0";
   $years  = "0";

   if($secs > 59)
   {
      $mins = floor($secs/60);
      $secs = $secs % 60;
   }

   if($mins > 59)
   {
      $hrs  = floor($mins/60);
      $mins = $mins % 60;
   }

   if($hrs > 23)
   {
      $days = floor($hrs/24);
      $hrs  = $hrs % 24;
   }

   if($days > 364)
   {
      $years = floor($days/365);
      $days   = $days % 365;
   }

   //if($hrs < 10)
   //   $hrs = "0".$hrs;

   //if($mins < 10)
   //   $mins = "0".$mins;

   //if($secs < 10)
   //   $secs = "0".$secs;

   $hmsString = "";

   $counter = 0;

   if($years > 0)
   {
      $hmsString .= $years."y";
      $counter++;
   }

   if($counter == $fields)
      return $hmsString;

   if($counter == $fields)
      return $hmsString;

   if($days > 0 || $counter)
   {
      if($hmsString)
         $hmsString .= $separator;

      $hmsString .= $days."d";
      $counter++;
   }

   if($counter == $fields)
      return $hmsString;

   if($hrs > 0 || $counter)
   {
      if($hmsString)
         $hmsString .= $separator;

      $hmsString .= $hrs."h";
      $counter++;
   }

   if($counter == $fields)
      return $hmsString;

   if($mins || $counter)
   {
      if($hmsString)
         $hmsString .= $separator;

      $hmsString .= $mins."m";
      $counter++;
   }

   if($counter == $fields)
      return $hmsString;

   if(! empty($hmsString))
      $hmsString .= $separator.$secs."s";
   else
      $hmsString = $secs."s";

   return $hmsString;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Days360
//
// [DESCRIPTION]:   returns the number of days between the two dates considering
//                  the year 360 days, 30 days each month
//
// [PARAMETERS]:    $startDate="", $endDate=""
//
// [RETURN VALUE]:  number of days as integer
//
// [CREATED BY]:    Eugen SAVIN (eugen@claspsolutions.com) 2004-10-18
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Days360($startDate="", $endDate="")
{
   list($startYear, $startMonth, $startDay) = split("-", $startDate);
   list($endYear,   $endMonth,   $endDay)   = split("-", $endDate);

   if($startDay >= $this->GetDaysInMonth($startMonth, $startYear))
      $startDay = 30;

   $totalDays = 30 - $startDay;
   $month     = $startMonth+1;

   if($month > 12)
   {
      $startYear++;
      $month = 1;
   }

   for($year=$startYear; $year<=$endYear; $year++)
   {
      while($month <= 12)
      {
         if($month == $endMonth && $year == $endYear)
         {
            if($endDay >= $this->GetDaysInMonth($endMonth, $endYear))
               $endDay = 30;

            $totalDays += $endDay;

            break;
         }

         $totalDays += 30;

         $month++;

         if($month > 12)
         {
            $month = 1;
            break;
         }
      }
   }

   return $totalDays;
}

} // end of class CDate

?>

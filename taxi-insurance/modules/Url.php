<?php

/*****************************************************************************/
/*                                                                           */
/*  CUrl class interface                                                     */
/*                                                                           */
/*  (C) 2004 Eugen Savin (seugen@abc.ro)                                     */
/*                                                                           */
/*****************************************************************************/

define("URL_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CUrl
//
// [DESCRIPTION]:  CUrl class interface
//
// [FUNCTIONS]:    int  AddUrl($siteID=0, $step=0, $url='');
//                 bool UpdateUrl($urlID=0, $siteID=0, $step=0, $url='');
//                 bool DeleteUrl($urlID=0);
//                 bool GetUrl($urlID=0, &$arrayResult);
//                 bool GetAllUrls($siteID=0, &$arrayResult);
//                 int  GetFirstAvailStep($siteID=0);
//
//                 int  AddParam($urlID=0, $sesID=0, $type='', $paramName='', $paramValue='');
//                 bool UpdateParam($paramID=0, $urlID=0, $sesID=0, $status='', $type='', $paramName='', $paramValue='');
//                 bool DeleteParam($paramID=0);
//                 bool GetParams($paramID=0, &$arrayResult);
//                 bool GetUrlParams($urlID=0, &$arrayResult);
//                 bool GetUrlResponses($urlID=0, &$arrayResult);
//                 bool GetUrlAssumptions($urlID=0, &$arrayResult);
//
//                 int  AddResponse($urlID=0, $type='', $response='');
//                 bool UpdateResponse($responseID=0, $urlID=0, $type='', $response='');
//                 bool DeleteResponse($responseID=0);
//                 bool GetResponse($responseID=0, &$arrayResult);
//
//                 int  AddAssumption($urlID=0, $type='', $paramName='', $regEX='');
//                 bool UpdateAssumption($paramID=0, $urlID=0, $type='', $paramName='', $regEX='');
//                 bool DeleteAssumption($paramID=0);
//                 bool GetAssumption($paramID=0, &$arrayResult);

//                 Close();
//                 GetError();
//
// [CREATED BY]:   Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CUrl
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last url error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CUrl
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CUrl($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddUrl
//
// [DESCRIPTION]:   Add new entry to the urls table
//
// [PARAMETERS]:    $siteID, $step, $url
//
// [RETURN VALUE]:  UrlID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddUrl($siteID=0, $step=0, $url='')
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITEID_FIELD");
      return 0;
   }

   if(! preg_match("/^\d+$/", $step))
   {
      $this->strERR = GetErrorString("INVALID_STEP_FIELD");
      return 0;
   }

   if(empty($url))
   {
      $this->strERR = GetErrorString("INVALID_URL_FIELD");
      return 0;
   }

   $this->lastSQLCMD = "INSERT INTO ".SQL_URLS." (site_id,step,url) VALUES ('$siteID','$step','$url')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateUrl
//
// [DESCRIPTION]:   Update urls table
//
// [PARAMETERS]:    $urlID, $siteID, $step, $url
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateUrl($urlID=0, $siteID=0, $step=0, $url='')
{
   if(! preg_match("/^\d+$/", $urlID))
   {
      $this->strERR = GetErrorString("INVALID_URLID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITEID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $step))
   {
      $this->strERR = GetErrorString("INVALID_STEP_FIELD");
      return false;
   }

   if(empty($url))
   {
      $this->strERR = GetErrorString("INVALID_URL_FIELD");
      return false;
   }

   // check if UrlID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_URLS." WHERE id='$urlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URLID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_URLS." SET site_id='$siteID',step='$step',url='$url' WHERE id='$urlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   // TODO ?
   //if(! $this->dbh->GetAffectedRows())
   //{
   //   $this->strERR = GetErrorString("URL_FAILED_UPDATE");
   //   return false;
   //}

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteUrl
//
// [DESCRIPTION]:   Delete an entry from urls table
//
// [PARAMETERS]:    $urlID,
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteUrl($urlID=0)
{
   if(! preg_match("/^\d+$/", $urlID))
   {
      $this->strERR = GetErrorString("INVALID_URLID_FIELD");
      return false;
   }

   // check if UrlID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_URLS." WHERE id='$urlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URLID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_URLS." WHERE id='$urlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   // TODO ?
   //if(! $this->dbh->GetAffectedRows())
   //{
   //   $this->strERR = GetErrorString("URL_FAILED_DELETE");
   //   return false;
   //}

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUrl
//
// [DESCRIPTION]:   Read data from urls table and put it into an array variable
//
// [PARAMETERS]:    $urlID=0, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUrl($urlID=0, &$arrayResult)
{
   if(! preg_match("/^\d+$/", $urlID))
   {
      $this->strERR = GetErrorString("INVALID_URLID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_URLS." WHERE id='$urlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("URLID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]      = $this->dbh->GetFieldValue("id");
   $arrayResult["site_id"] = $this->dbh->GetFieldValue("site_id");
   $arrayResult["step"]    = $this->dbh->GetFieldValue("step");
   $arrayResult["url"]     = $this->dbh->GetFieldValue("url");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllUrls
//
// [DESCRIPTION]:   Read data from urls table and put it into an array variable
//                  key = siteID, value = urlAddress
//
// [PARAMETERS]:    $siteID=0, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllUrls($siteID=0, &$arrayResult)
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITEID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id,url FROM ".SQL_URLS." WHERE site_id='$siteID' ORDER BY step";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URLS_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("url");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetFirstAvailStep
//
// [DESCRIPTION]:   Read the first available step from urls table
//
//
// [PARAMETERS]:    $siteID=0
//
// [RETURN VALUE]:  step number if success, 0 otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetFirstAvailStep($siteID=0)
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITEID_FIELD");
      return false;
   }

   $stepNr = 0;

   $this->lastSQLCMD = "SELECT step FROM ".SQL_URLS." WHERE site_id='$siteID' ORDER BY step ASC";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return 0;
   }

   if(! $this->dbh->Getrows())
      return 1;

   while($this->dbh->MoveNext())
   {
      if(($this->dbh->GetFieldValue("step") - $stepNr) >= 2)
         break;

      $stepNr = $this->dbh->GetFieldValue("step");
   }

   $stepNr++;

   return $stepNr;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUrlParams
//
// [DESCRIPTION]:   Read data from url_params table and put it into an array variable
//                  key = paramID, value = paramName
//
// [PARAMETERS]:    $urlID=0, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUrlParams($urlID=0, &$arrayResult)
{
   if(! preg_match("/^\d+$/", $urlID))
   {
      $this->strERR = GetErrorString("INVALID_URLID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id,name FROM ".SQL_URL_PARAMS." WHERE url_id='$urlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URL_PARAMS_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("name");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUrlReponses
//
// [DESCRIPTION]:   Read data from url_response table and put it into an array variable
//                  key = responseID, value = respnonseType
//
// [PARAMETERS]:    $urlID=0, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUrlResponses($urlID=0, &$arrayResult)
{
   if(! preg_match("/^\d+$/", $urlID))
   {
      $this->strERR = GetErrorString("INVALID_URLID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id,type FROM url_response WHERE url_id='$urlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URL_RESPONSES_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("type");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUrlAssumptions
//
// [DESCRIPTION]:   Read data from url_response table and put it into an array variable
//                  key = assumptionID, value = assumptionParam
//
// [PARAMETERS]:    $urlID=0, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUrlAssumptions($urlID=0, &$arrayResult)
{
   if(! preg_match("/^\d+$/", $urlID))
   {
      $this->strERR = GetErrorString("INVALID_URLID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id,as_param FROM url_response WHERE url_id='$urlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URL_ASSUMPTIONS_NOT_FOUND");
      return false;
   }

   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("as_param");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddParam
//
// [DESCRIPTION]:   Add new entry to the url_params table
//
// [PARAMETERS]:    $urlID=0, $sesID=0, $type='', $paramName='', $paramValue=''
//
// [RETURN VALUE]:  ParamID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      [Gabi ISTVANCSEK  (null@seopa.com) 2004-10-09]
//                    [implemented session params id and status params]
//////////////////////////////////////////////////////////////////////////////FE

function AddParam($urlID=0, $sesID=0, $type='', $paramName='', $paramValue='')
{
   if(! preg_match("/^\d+$/", $urlID))
   {
      $this->strERR = GetErrorString("INVALID_URLID_FIELD");
      return 0;
   }

   if(! preg_match("/^\d+$/", $sesID))
   {
      $this->strERR = GetErrorString("INVALID_SESSION_PARAM_ID_FIELD");
      return 0;
   }

   if(($type != 'POST') && ($type != 'GET'))
   {
      $this->strERR = GetErrorString("INVALID_PARAM_TYPE_FIELD");
      return 0;
   }

   if(empty($paramName))
   {
      $this->strERR = GetErrorString("INVALID_PARAM_NAME_FIELD");
      return 0;
   }

   // check if urlID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_URLS." WHERE id='$urlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return 0;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URLID_NOT_FOUND");
      return 0;
   }

   // check if url param name already exists
   $this->lastSQLCMD = "SELECT name FROM ".SQL_URL_PARAMS." WHERE url_id='$urlID' AND name='$paramName'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return 0;
   }

   if($this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URL_PARAM_NAME_ALREADY_EXISTS");
      return 0;
   }

   $this->lastSQLCMD = "INSERT INTO ".SQL_URL_PARAMS." (url_id,ses_id,status,type,name,value) VALUES ('$urlID','$sesID','ENABLED','$type','$paramName','$paramValue')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateParam
//
// [DESCRIPTION]:   Update url_params table
//
// [PARAMETERS]:    $paramID=0, $urlID=0, $sesID=0, $status='', $type='', $paramName='', $paramValue=''
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      [Gabi ISTVANCSEK  (null@seopa.com) 2004-10-09]
//                    [implemented session params id and status params]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateParam($paramID=0, $urlID=0, $sesID=0, $status='', $type='POST', $paramName='', $paramValue='')
{
   if(! preg_match("/^\d+$/", $paramID))
   {
      $this->strERR = GetErrorString("INVALID_URL_PARAMID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $urlID))
   {
      $this->strERR = GetErrorString("INVALID_URLID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $sesID))
   {
      $this->strERR = GetErrorString("INVALID_SESSION_PARAM_ID_FIELD");
      return 0;
   }

   if(($status != 'ENABLED') && ($status != 'DISABLED'))
   {
      $this->strERR = GetErrorString("INVALID_PARAM_STATUS_FIELD");
      return false;
   }

   if(($type != 'POST') && ($type != 'GET'))
   {
      $this->strERR = GetErrorString("INVALID_PARAM_TYPE_FIELD");
      return false;
   }

   if(empty($paramName))
   {
      $this->strERR = GetErrorString("INVALID_PARAM_NAME_FIELD");
      return false;
   }

   // check if paramID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_URL_PARAMS." WHERE id='$paramID'";
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URL_PARAMID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_URL_PARAMS." SET url_id='$urlID',ses_id='$sesID',status='$status',type='$type',name='$paramName',value='$paramValue' WHERE id='$paramID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   // TODO ?
   //if(! $this->dbh->GetAffectedRows())
   //{   $arrayResult["ses_id"] = $this->dbh->GetFieldValue("ses_id");
   //$arrayResult["status"] = $this->dbh->GetFieldValue("status");

   //   $this->strERR = GetErrorString("URL_PARAM_FAILED_UPDATE");
   //   return false;
   //}

   return true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteParam
//
// [DESCRIPTION]:   Delete an entry from url_params table
//
// [PARAMETERS]:    $paramID=0
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      [Gabi ISTVANCSEK  (null@seopa.com) 2004-10-09]
//                    [implemented session params id and status params]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteParam($paramID=0)
{
   if(! preg_match("/^\d+$/", $paramID))
   {
      $this->strERR = GetErrorString("INVALID_URL_PARAMID_FIELD");
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URL_PARAMID_NOT_FOUND");
      return false;
   }

   // check if paramID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_URL_PARAMS." WHERE id='$paramID'";
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URL_PARAMID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_URL_PARAMS." WHERE id='$paramID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   // TODO ?
   //if(! $this->dbh->GetAffectedRows())
   //{
   //   $this->strERR = GetErrorString("URL_PARAM_FAILED_DELETE");
   //   return false;
   //}

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetParam
//
// [DESCRIPTION]:   Read data from url_params table and put it into an array variable
//
// [PARAMETERS]:    $paramID=0, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      [Gabi ISTVANCSEK  (null@seopa.com) 2004-10-09]
//                    [implemented session params id and status params]
//////////////////////////////////////////////////////////////////////////////FE, $regEX=''
function GetParam($paramID=0, &$arrayResult)
{
   if(! preg_match("/^\d+$/", $paramID))
   {
      $this->strERR = GetErrorString("INVALID_URL_PARAMID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_URL_PARAMS." WHERE id='$paramID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("URL_PARAMID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]     = $this->dbh->GetFieldValue("id");
   $arrayResult["url_id"] = $this->dbh->GetFieldValue("url_id");
   $arrayResult["ses_id"] = $this->dbh->GetFieldValue("ses_id");
   $arrayResult["status"] = $this->dbh->GetFieldValue("status");
   $arrayResult["type"]   = $this->dbh->GetFieldValue("type");
   $arrayResult["name"]   = $this->dbh->GetFieldValue("name");
   $arrayResult["value"]  = $this->dbh->GetFieldValue("value");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddResponse
//
// [DESCRIPTION]:   Add new entry to the url_response table
//
// [PARAMETERS]:    $urlID=0, $type='', $response=''
//
// [RETURN VALUE]:  ReponseID or 0 in case of failure
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddResponse($urlID=0, $type='', $response='')
{
   if(! preg_match("/^\d+$/", $urlID))
   {
      $this->strERR = GetErrorString("INVALID_URLID_FIELD");
      return 0;
   }

   if(($type != 'SUCCESS') && ($type != 'FAILURE'))
   {
      $this->strERR = GetErrorString("INVALID_RESPONSE_TYPE_FIELD");
      return 0;
   }

   if(empty($response))
   {
      $this->strERR = GetErrorString("INVALID_RSPONSE_FIELD");
      return 0;
   }

   // check if urlID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_URLS." WHERE id='$urlID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return 0;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URLID_NOT_FOUND");
      return 0;
   }

   // check if url response already exists
   $this->lastSQLCMD = "SELECT id FROM url_response WHERE url_id='$urlID' AND type='$type'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return 0;
   }

   if($this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URL_RESPONSE_ALREADY_EXISTS");
      return 0;
   }

   $this->lastSQLCMD = "INSERT INTO url_response (url_id,type,response) VALUES ('$urlID','$type','$response')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
       return 0;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return 0;
   }

   return $this->dbh->GetFieldValue("id");
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateResponse
//
// [DESCRIPTION]:   Update url_response table
//
// [PARAMETERS]:    $responseID=0, $urlID=0, $type='', $response=''
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateResponse($responseID=0, $urlID=0, $type='', $response='')
{
   if(! preg_match("/^\d+$/", $responseID))
   {
      $this->strERR = GetErrorString("INVALID_URL_RESPONSEID_FIELD");
      return false;
   }

   if(! preg_match("/^\d+$/", $urlID))
   {
      $this->strERR = GetErrorString("INVALID_URLID_FIELD");
      return false;
   }

   if(($type != 'SUCCESS') && ($type != 'FAILURE'))
   {
      $this->strERR = GetErrorString("INVALID_RESPONSE_TYPE_FIELD");
      return 0;
   }

   if(empty($response))
   {
      $this->strERR = GetErrorString("INVALID_RSPONSE_FIELD");
      return 0;
   }

   // check if responseID exists
   $this->lastSQLCMD = "SELECT id FROM url_response WHERE id='$responseID'";
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URL_RESPONSEID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE url_response SET url_id='$urlID',type='$type',response='$response' WHERE id='$responseID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   // TODO ?
   //if(! $this->dbh->GetAffectedRows())
   //{
   //   $this->strERR = GetErrorString("URL_RESPONSE_FAILED_UPDATE");
   //   return false;
   //}

   return true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteResponse
//
// [DESCRIPTION]:   Delete an entry from url_response table
//
// [PARAMETERS]:    $paramID=0
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteResponse($responseID=0)
{
   if(! preg_match("/^\d+$/", $responseID))
   {
      $this->strERR = GetErrorString("INVALID_URL_RESPONSEID_FIELD");
      return false;
   }

   // check if responseID exists
   $this->lastSQLCMD = "SELECT id FROM url_response WHERE id='$responseID'";
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("URL_RESPONSEID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM url_response WHERE id='$responseID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   // TODO ?
   //if(! $this->dbh->GetAffectedRows())
   //{
   //   $this->strERR = GetErrorString("URL_RESPONSE_FAILED_DELETE");
   //   return false;
   //}

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetResponse
//
// [DESCRIPTION]:   Read data from url_response table and put it into an array variable
//
// [PARAMETERS]:    $responseID=0, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetResponse($responseID=0, &$arrayResult)
{
   if(! preg_match("/^\d+$/", $responseID))
   {
      $this->strERR = GetErrorString("INVALID_URL_RESPONSEID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM url_response WHERE id='$responseID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("URL_RESPONSEID_NOT_FOUND");
      return false;
   }

   $arrayResult["id"]       = $this->dbh->GetFieldValue("id");
   $arrayResult["url_id"]   = $this->dbh->GetFieldValue("url_id");
   $arrayResult["type"]     = $this->dbh->GetFieldValue("type");
   $arrayResult["response"] = $this->dbh->GetFieldValue("response");

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

} // end of CUrl class
?>

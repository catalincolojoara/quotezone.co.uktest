<?php

/*****************************************************************************/
/*                                                                           */
/*  CQuoteError class interface                                                   */
/*                                                                           */
/*  (C) 2005 Istvancsek Gabriel (null@seopa.com)                             */
/*                                                                           */
/*****************************************************************************/

define("ERRORS_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQuoteError
//
// [DESCRIPTION]:  CQuoteError class interface
//
// [FUNCTIONS]:      true  | false AssertQuoteError($quoteStatusID='', $urlStep='', $url='', $message='')
//                   int   | false AddQuoteError($quoteStatusID='', $urlStep='', $url='', $message='')
//                   true  | false UpdateQuoteError($quoteErrorID='', $quoteStatusID='', $urlStep='', $url='', $message='')
//                   true  | false DeleteQuoteError($quoteErrorID='')
//                   array | false GetQuoteErrorDetails($quoteErrorID='')
//                   array | false GetErrorDetailsByLogID($quoteStatusID='')
//
//                   Close()
//                   GetError()
//                   ShowError()
//
// [CREATED BY]:   Istvancsek Gabriel (null@seopa.com) 2005-09-14
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CQuoteError
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last Error error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CQuoteError
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CQuoteError($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertQuoteError
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    $quoteStatusID='', $urlStep='', $url='', $message=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertQuoteError($quoteStatusID='', $urlStep='', $url='', &$message, &$quoteMessage)
{
   $this->strERR = '';

   if(! preg_match("/^\d+$/", $quoteStatusID))
      $this->strERR  = GetErrorString("INVALID_QUOTE_ERROR_QUOTE_STATUS_ID_FIELD");

   if(! preg_match("/^\d+$/", $urlStep))
      $this->strERR .= GetErrorString("INVALID_QUOTE_ERROR_URL_STEP_ID_FIELD");

   if(empty($url))
      $this->strERR .= GetErrorString("INVALID_QUOTE_ERROR_URL_FIELD");

   if(empty($message))
      $this->strERR .= GetErrorString("INVALID_QUOTE_ERROR_MESSAGE_FIELD");

   $message = str_replace("\\","", $message);
   $message = str_replace("'","\'", $message);

   if(empty($quoteMessage))
      $this->strERR .= GetErrorString("INVALID_QUOTE_ERROR_QUOTE_MESSAGE_FIELD");

   $quoteMessage = str_replace("\\","", $quoteMessage);
   $quoteMessage = str_replace("'","\'", $quoteMessage);

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteError
//
// [DESCRIPTION]:   Add new entry to the Errors table
//
// [PARAMETERS]:    $quoteStatusID='', $urlSTEP='', $url='', $message=''
//
// [RETURN VALUE]:  errorID or 0 in case of failure
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteError($quoteStatusID='', $urlStep='', $url='', $message='', $quoteMessage='')
{
   if(! $this->AssertQuoteError($quoteStatusID, $urlStep, $url, $message, $quoteMessage))
      return false;

   $this->lastSQLCMD = "INSERT INTO ".SQL_QUOTE_ERRORS." (quote_status_id,url_step,url,message,quote_message) VALUES ('$quoteStatusID','$urlStep','$url','$message','$quoteMessage')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQuoteError
//
// [DESCRIPTION]:   Update Error table
//
// [PARAMETERS]:    $quoteErrorID='', $quoteStatusID='', $urlStep='', $url='', $message='', $quoteMessage=''
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQuoteError($quoteErrorID='', $quoteStatusID='', $urlStep='', $url='', $message='', $quoteMessage='')
{
   if(! $this->AssertQuoteError($quoteStatusID, $urlStep, $url, $message, $quoteMessage))
      return false;

   if(! preg_match("/^\d+$/", $quoteErrorID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ERROR_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTE_ERRORS." WHERE id='$errorID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("ERRORID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_QUOTE_ERRORS." SET quote_status_id='$quoteStatusID',url_step='$urlSTEP',url='$url',message='$message',quote_message='$quoteMessage' WHERE id='$quoteErrorID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteQuoteError
//
// [DESCRIPTION]:   Delete an entry from Error table
//
// [PARAMETERS]:    $quoteErrorID
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteQuoteError($quoteErrorID='')
{
   if(! preg_match("/^\d+$/", $quoteErrorID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ERROR_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT id FROM ".SQL_QUOTE_ERRORS." WHERE id='$$quoteErrorID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("QUOTE_ERROR_ID_NOT_FOUND");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_QUOTE_ERRORS." WHERE id='$quoteErrorID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetQuoteErrorDetails
//
// [DESCRIPTION]:   Read data from Errors table and put it into an array variable
//
// [PARAMETERS]:    $quoteErrorID
//
// [RETURN VALUE]:  array|false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuoteErrorDetails($quoteErrorID='')
{
   if(! preg_match("/^\d+$/", $quoteErrorID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ERROR_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_ERRORS." WHERE id='$quoteErrorID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ERROR_ID_FIELD");
      return false;
   }

   $arrayResult = array();
   
   $arrayResult["id"]                = $this->dbh->GetFieldValue("id");
   $arrayResult["quote_status_id"]   = $this->dbh->GetFieldValue("quote_status_id");
   $arrayResult["url_step"]          = $this->dbh->GetFieldValue("url_step");
   $arrayResult["url"]               = $this->dbh->GetFieldValue("url");
   $arrayResult["message"]           = $this->dbh->GetFieldValue("message");
   $arrayResult["quote_message"]     = $this->dbh->GetFieldValue("quote_message");

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetErrorDetailsByLogID
//
// [DESCRIPTION]:   Read data from Errors table and put it into an array variable
//
// [PARAMETERS]:    $quoteStatusID
//
// [RETURN VALUE]:  $arrayResult |  false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetErrorDetailsByLogID($quoteStatusID='')
{
   if(! preg_match("/^\d+$/", $quoteStatusID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_ERROR_STATUS_ID_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_QUOTE_ERRORS." WHERE quote_status_id='$quoteStatusID' ORDER BY id DESC limit 1";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("QUOTE_ERROR_STATUS_ID_NOT_FOUND");
      return false;
   }

   $arrayResult = array();

   $arrayResult["id"]              = $this->dbh->GetFieldValue("id");
   $arrayResult["quote_status_id"] = $this->dbh->GetFieldValue("quote_status_id");
   $arrayResult["url_step"]        = $this->dbh->GetFieldValue("url_step");
   $arrayResult["url"]             = $this->dbh->GetFieldValue("url");
   $arrayResult["message"]         = $this->dbh->GetFieldValue("message");
   $arrayResult["quote_message"]   = $this->dbh->GetFieldValue("quote_message");

   return $arrayResult;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Print the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-14
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}
} // end of CQuoteError class
?>

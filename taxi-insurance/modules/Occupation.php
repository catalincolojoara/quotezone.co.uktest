<?php
/*****************************************************************************/
/*                                                                           */
/*  COccupation class interface                                                 */
/*                                                                           */
/*  (C) 2004 ISTVNACSEK Gabriel (null@seopa.com)                              */
/*                                                                           */
/*****************************************************************************/
//                MySQL table definitions

define("SESSION_INCLUDED", "1");

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";
define("SESSION_INCLUDED", "1");

//if(DEBUG_MODE)
//   error_reporting(1);
//else
//   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   COccupation
//
// [DESCRIPTION]:  COccupation class interface
//
// [FUNCTIONS]:    
//                 int  function AddOccupation($paramName='')
//                 bool function UpdateOccupation($paramOccupationID=0, $paramName='')
//                 bool function DeleteOccupation($paramOccupationID=0)
//                 bool function GetOccupation($paramOccupationID=0, &$arrayResult)
//                 bool GetAllOccupations(&$arrayResult)
// 
//                 Close();
//                 GetError();
//
// [CREATED BY]:   Gabriel ISTVNACSEK (null@seopa.com) 2004-07-07
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class COccupation
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $lastSQLCMD;  // keep here the last SQL command
    var $strERR;      // last url error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CUrl
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function COccupation($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddOccupation
//
// [DESCRIPTION]:   Add new entry to the OCC_CODES table
//
// [PARAMETERS]:    $paramName=''
//
// [RETURN VALUE]:  ParamID or 0 in case of failure
//
// [CREATED BY]:    Gabi ISTVNACSEK (null@seopa.com) 2004-09-10
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddOccupation($id="", $name="")
{

   if(empty($id))
   {
      $this->strERR = GetErrorString("INVALID_ID_OCCUPATION_FIELD");
      return false;
   }

   if(empty($name))
   {
      $this->strERR = GetErrorString("INVALID_NAME_OCCUPATION_FIELD");
      return false;
   }
   
   // check if param session name already exists
   $this->lastSQLCMD = "SELECT id,name FROM ".SQL_OCCUPATION_CODES." WHERE id='$id' OR name='$name'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
      return false;
   }

   if($this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("PARAM_ID_OR_NAME_OCCUPATION_ALREADY_EXISTS");
      return false;
   }
   // insert param session name into OCC_CODES table
   $this->lastSQLCMD = "INSERT INTO ".SQL_OCCUPATION_CODES." (id,name) VALUES ('$id','$name')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
      return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateOccupation
//
// [DESCRIPTION]:   Update OCC_CODES table
//
// [PARAMETERS]:    $paramOccupationID=0, $paramName=''
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi ISTVNACSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateOccupation($id='', $name='')
{

   if(empty($id))
   {
      $this->strERR = GetErrorString("INVALID_ID_OCCUPATION_FIELD");
      return false;
   }

   if(empty($name))
   {
      $this->strERR = GetErrorString("INVALID_NAME_OCCUPATION_FIELD");
      return false;
   }

   // check if paramOccupationID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_OCCUPATION_CODES." WHERE id='$paramOccupationID'";
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   // check if param session name already exists
   $this->lastSQLCMD = "SELECT id,name FROM ".SQL_OCCUPATION_CODES." WHERE id='$id' OR name='$name'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("PARAM_ID_OR_NAME_OCCUPATION_NOT_EXISTS");
      return false;
   }

   $this->lastSQLCMD = "UPDATE ".SQL_OCCUPATION_CODES." SET name='$name' WHERE id='$id'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteOccupation
//
// [DESCRIPTION]:   Delete an entry from OCC_CODES table
//
// [PARAMETERS]:    $paramOccupationID=0
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi Istvnacsek (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteOccupation($id='')
{
   if(empty($id))
   {
      $this->strERR = GetErrorString("INVALID_ID_OCCUPATION_FIELD");
      return false;
   }

   // check if paramOccupationID exists
   $this->lastSQLCMD = "SELECT id FROM ".SQL_OCCUPATION_CODES." WHERE id='$id'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("ID_OCCUPATION_NOT_EXIST");
      return false;
   }

   $this->lastSQLCMD = "DELETE FROM ".SQL_OCCUPATION_CODES." WHERE id='$id'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetOccupation
//
// [DESCRIPTION]:   Read data from OCC_CODES table and put it into an array variable
//
// [PARAMETERS]:    $paramOccupationID=0, &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi ISTVNACSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetOccupation($id='', &$arrayResult)
{
   if(empty($id))
   {
      $this->strERR = GetErrorString("INVALID_ID_OCCUPATION_FIELD");
      return false;
   }

   $this->lastSQLCMD = "SELECT * FROM ".SQL_OCCUPATION_CODES." WHERE id='$id'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("ID_OCCUPATION_NOT_EXIST");
      return false;
   }

   $arrayResult["id"]     = $this->dbh->GetFieldValue("id");
   $arrayResult["name"]   = $this->dbh->GetFieldValue("name");

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllOccupations
//
// [DESCRIPTION]:   Read data from OCC_CODES table and put it into an array variable
//                  key = paramOccupationID, value = paramOccupationName
//
// [PARAMETERS]:    &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi ISTVNACSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllOccupations(&$arrayResult)
{
   $this->lastSQLCMD = "SELECT id,name FROM ".SQL_OCCUPATION_CODES."";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("ID_OCCUPATION_NOT_EXIST");
      return false;
   }

   while($this->dbh->MoveNext())
      $arrayResult[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("name");

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllOccupationsByName
//
// [DESCRIPTION]:   Read data from OCC_CODES table and put it into an array variable
//                  key = occupationID, value = occupationName
//
// [PARAMETERS]:    &$resultArray
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Gabi ISTVNACSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllOccupationsByName($name='', &$arrayResult)
{
   if(empty($name))
   {
      $this->strERR = GetErrorString("INVALID_NAME_OCCUPATION_FIELD");
      return false;
   }

   $nameOccExplode = explode(" ",$name);

   foreach($nameOccExplode as $nameKey => $nameValue)
   {   
      if(strlen($nameValue) < 3)
      {
         $this->strERR = GetErrorString("INVALID_KEY_OCCUPATION_LENGTH_sFIELD");
         //return false;
         continue;
      }
         
      if(strlen($nameValue) > 3)
         $this->lastSQLCMD = "SELECT id,name FROM ".SQL_OCCUPATION_CODES." WHERE name LIKE '%".substr($nameValue,0,strlen($nameValue)-1)."%'";
      else
         $this->lastSQLCMD = "SELECT id,name FROM ".SQL_OCCUPATION_CODES." WHERE name LIKE '%".$nameValue."%'";
   
      if(! $this->dbh->Exec($this->lastSQLCMD))
      {
      $this->strERR = $this->dbh->GetError();
      return false;
      }
   
      if(! $this->dbh->GetRows())
      {
         $this->strERR = GetErrorString("ID_OCCUPATION_NOT_EXIST");
         continue;
      }
   
      
      while($this->dbh->MoveNext())
         $arrayResultTemp[$this->dbh->GetFieldValue("id")] = $this->dbh->GetFieldValue("name");
   
      foreach($arrayResultTemp as $key => $value)
      {
         $valueExplode = explode(" ", $value);
         foreach($valueExplode as $k => $v)
         {
            if(preg_match("/^".substr($nameValue,0,strlen($nameValue)-1).".*$/i",$v))
            {
               $arrayResult[$key] = $value;
               break;
            }
         }
      }
   }
   
   if(! count($arrayResult))
      return false;
      
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-10-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

} // end COccupation

?>

<?php
/*****************************************************************************/
/*                                                                           */
/*  CSitesUsersLogin class interface                                            */
/*                                                                           */
/*  (C) 2005 Ciciu Gigi (null@seopa.com)                                   */
/*                                                                           */
/*****************************************************************************/

include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CSitesUsersLogin
//
// [DESCRIPTION]:  CSitesUsersLogin class interface
//
// [FUNCTIONS]:   true   | false AssertSitesUsersLogin($qzQuoteID=0, $quotePassword='')
//                int    | false AddSitesUsersLogin($qzQuoteID=0, $quotePassword='')
//                true   | false UpdateSitesUsersLogin($sitesUsersLoginID=0,$qzQuoteID=0, $quotePassword='')
//                true   | false DeleteSitesUsersLoginByID($sitesUsersLoginID=0)
//                string | false GetUserPasswordByEmail($email="", $siteID="")
//                array  | false GetSitesUsersLoginByID($sitesUsersLoginID=0)
//                array  | false GetSitesUsersLoginBySiteID($siteID=0)
//                int    | false GetNumberOfSitesUsersLoginBySiteID($siteID=0)
//                int    | false GetSitesUsersLoginByPass($quotePassword='', $siteID=0)
//                int    | false GetSitesUsersLoginByQzQuoteID($qzQuoteID=0, $siteID=0)
//                array  | false GetAllSitesUsersLogin()
//
//                Close();
//                GetError();
//                ShowError();
//
// [CREATED BY]:  Ciciu Gigi (null@seopa.com)
//
// [MODIFIED]:    - [programmer (email) date]
//                  [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CSitesUsersLogin
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CSitesUsersLogin
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Ciciu Gigi (null@seopa.com) 2006-13-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CSitesUsersLogin($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertSitesUsersLogin
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    $qzQuoteID=0,$quoteUserDetailsID=0,$siteId=0,$quotePassword=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Ciciu Gigi (null@seopa.com) 2006-13-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertSitesUsersLogin($qzQuoteID,$siteId,$quotePassword)
{
   $this->strERR = '';

   if(! preg_match('/([\d]+)/i',$qzQuoteID))
      $this->strERR .= GetErrorString("INVALID_QZ_QUOTE_ID")."\n";     
      
   if(! preg_match('/([\d]+)/i',$siteId))
      $this->strERR .= GetErrorString("INVALID_SITES_USER_ID")."\n";
      
   if(empty($quotePassword))
      $this->strERR .= GetErrorString("INVALID_PASSWORD")."\n";      

   if(! empty($this->strERR))
      return false;

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddSitesUsersLogin
//
// [DESCRIPTION]:   Check if exist a car and add if not exist into QUINN_VEHICLE table
//
// [PARAMETERS]:    $qzQuoteID=0,$quoteUserDetailsID=0,$siteId=0,$quotePassword=''
//
// [RETURN VALUE]:  $sitesUsersLoginID | false
//
// [CREATED BY]:    Ciciu Gigi (null@seopa.com) 2006-13-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddSitesUsersLogin($qzQuoteID=0,$siteId=0,$quotePassword='')
{
   if(! $this->AssertSitesUsersLogin($qzQuoteID,$siteId,$quotePassword))
      return false;

   
   $lastSqlCmd = "SELECT id FROM ".SQL_SITES_USERS_LOGIN." WHERE qz_quote_id='$qzQuoteID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   if($this->dbh->GetRows())
   {
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      $sitesUsersLoginID = $this->dbh->GetFieldValue("id");
      
      return $sitesUsersLoginID;
   }

   $lastSqlCmd = "INSERT INTO ".SQL_SITES_USERS_LOGIN." (qz_quote_id, password,site_id) VALUES ('$qzQuoteID', '".addslashes($quotePassword)."', '".$siteId."')";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
       $this->strERR = $this->dbh->GetError();
       return false;
   }

   $lastSqlCmd = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateSitesUsersLogin
//
// [DESCRIPTION]:   Update a user data from the sites_users_login table
//
// [PARAMETERS]:    $sitesUsersLoginID='',$qzQuoteID=0,$quoteUserDetailsID=0,$siteId=0,$quotePassword=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Ciciu Gigi (null@seopa.com) 2006-13-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateSitesUsersLogin($sitesUsersLoginID=0,$qzQuoteID=0,$siteId=0,$quotePassword='')
{
   if(! preg_match("/^\d+$/", $sitesUsersLoginID))
   {
         $this->strERR = GetErrorString("INVALID_QUINN_USER_LOGIN_ID")."\n";
      return false;
   }

   if(! $this->AssertSitesUsersLogin($qzQuoteID,$siteId,$quotePassword))
      return false;
      
   $lastSqlCmd = "SELECT id FROM ".SQL_SITES_USERS_LOGIN." WHERE id='$sitesUsersLoginID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITES_USER_LOGIN_ID_NOT_FOUND");
      return false;
   }
 
   $lastSqlCmd = "UPDATE ".SQL_SITES_USERS_LOGIN." SET qz_quote_id='$qzQuoteID', password='".addslashes($quotePassword)."', site_id='".$siteId."' WHERE id='$sitesUsersLoginID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
/////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DeleteSitesUsersLogByID
//
// [DESCRIPTION]:   Delete an entry from the sites_users_login table
//
// [PARAMETERS]:    $id=0
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Ciciu Gigi (null@seopa.com) 2006-13-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DeleteSitesUsersLoginByID($sitesUsersLoginID=0)
{
   if(! preg_match("/^\d+$/", $sitesUsersLoginID))
   {
      $this->strERR = GetErrorString("INVALID_SITES_USER_LOGIN_ID");
      return false;
   }

   $lastSqlCmd = "SELECT id FROM ".SQL_SITES_USERS_LOGIN." WHERE id='$sitesUsersLoginID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITES_USER_LOGIN_ID_NOT_FOUND");
      return false;
   }

   $lastSqlCmd = "DELETE FROM ".SQL_SITES_USERS_LOGIN." WHERE id='$sitesUsersLoginID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSitesUsersLoginByID
//
// [DESCRIPTION]:   Read data from sites_users_login table and put it into an array variable
//
// [PARAMETERS]:    $sitesUsersLoginID=0
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Ciciu Gigi  (null@seopa.com) 2006-13-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSitesUsersLoginByID($sitesUsersLoginID)
{
   if(! preg_match("/^\d+$/", $sitesUsersLoginID))
   {
      $this->strERR = GetErrorString("INVALID_SITES_USER_LOGIN_ID");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM ".SQL_SITES_USERS_LOGIN." WHERE id='$sitesUsersLoginID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITES_USER_LOGIN_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
   
   $arrayResult = array();

   $arrayResult["id"]                    = trim($this->dbh->GetFieldValue("id"));
   $arrayResult["qz_quote_id"]           = trim($this->dbh->GetFieldValue("qz_quote_id"));
   $arrayResult["site_id"]               = trim($this->dbh->GetFieldValue("site_id"));
   $arrayResult["password"]              = trim(stripslashes($this->dbh->GetFieldValue("password")));

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserPasswordByEmail
//
// [DESCRIPTION]:   Read password from sites_users_login table which is usefull at login section
//
// [PARAMETERS]:    $email = ""
//
// [RETURN VALUE]:  string or false in case of failure
//
// [CREATED BY]:    ISTVANCSEK Gabriel  (null@seopa.com) 2006-19-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserPasswordByEmail($email="", $siteID="")
{
   if(empty($email))
   {
      $this->strERR = GetErrorString("INVALID_SITES_USERS_EMAIL_ADDRESS_LOGIN");
      return false;
   }

   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITES_USER_LOGIN_ID");
      return false;
   }

   $lastSqlCmd = "SELECT password FROM ".SQL_SITES_USERS_LOGIN." sul, ".SQL_QZQUOTES." qzq, ".SQL_QUOTE_USER_DETAILS." qud WHERE sul.qz_quote_id=qzq.id and qzq.quote_user_details_id=qud.id and qud.email='$email' AND sul.site_id='$siteID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITES_USER_LOGIN_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }
   
   return $this->dbh->GetFieldValue("password");

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSitesUsersLoginBySiteID
//
// [DESCRIPTION]:   Read data from sites_users_login table and put it into an array variable
//
// [PARAMETERS]:    $siteID=0
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Ciciu Gigi  (null@seopa.com) 2006-13-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSitesUsersLoginBySiteID($siteID=0)
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITES_USER_LOGIN_ID");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM ".SQL_SITES_USERS_LOGIN." WHERE site_id='$siteID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITES_USER_LOGIN_ID_NOT_FOUND");
      return false;
   }
   
   $arrayResult = array();   
   $index       = 0;
   while($this->dbh->MoveNext())
   {   
   	$index++;
      $arrayResult[$index]["id"]                    = trim($this->dbh->GetFieldValue("id"));
      $arrayResult[$index]["qz_quote_id"]           = trim($this->dbh->GetFieldValue("qz_quote_id"));
      $arrayResult[$index]["site_id"]               = trim($this->dbh->GetFieldValue("site_id"));
      $arrayResult[$index]["password"]              = trim(stripslashes($this->dbh->GetFieldValue("password")));
   }   

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetNumberOfSitesUsersLoginBySiteID
//
// [DESCRIPTION]:   get number of email accounts by site id
//
// [PARAMETERS]:    $siteID
//
// [RETURN VALUE]:  int | false;
//
// [CREATED BY]:    Ciciu Gigi  (null@seopa.com) 2006-13-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetNumberOfSitesUsersLoginBySiteID($siteID=0)
{
   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITES_USER_LOGIN_ID");
      return false;
   }

   $lastSqlCmd = "SELECT count(*) as total FROM ".SQL_SITES_USERS_LOGIN." WHERE site_id='$siteID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITES_USER_LOGIN_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("total");  
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSitesUsersLoginByPass
//
// [DESCRIPTION]:   Read data  from sites_users_login table and put it into an array variable
//
// [PARAMETERS]:    $quotePassword=''
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Ciciu Gigi  (null@seopa.com) 2006-13-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSitesUsersLoginByPass($quotePassword='')
{
   if(empty($quotePassword))
   {
      $this->strERR = GetErrorString("INVALID_SITES_USER_PASSWORD");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM ".SQL_SITES_USERS_LOGIN." WHERE password='".stripslashes($quotePassword)."'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITES_USER_PASSWORD_NOT_FOUND");
      return false;
   }

   $arrayResult = array();   
   $index       = 0;
   while($this->dbh->MoveNext())
   {   
   	$index++;
	   $arrayResult[$index]["id"]                    = trim($this->dbh->GetFieldValue("id"));
	   $arrayResult[$index]["qz_quote_id"]           = trim($this->dbh->GetFieldValue("qz_quote_id"));
	   $arrayResult[$index]["site_id"]               = trim($this->dbh->GetFieldValue("site_id"));
      $arrayResult[$index]["password"]              = trim(stripslashes($this->dbh->GetFieldValue("password"))   );
   }   

   return $arrayResult;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetSitesUsersLoginByQzQuoteID
//
// [DESCRIPTION]:   Read data  from sites_users_login table and put it into an array variable
//
// [PARAMETERS]:    $qzQuoteID=0, $siteID=0
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Ciciu Gigi  (null@seopa.com) 2006-13-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetSitesUsersLoginByQzQuoteID($qzQuoteID=0, $siteID=0)
{
   if(! preg_match("/^\d+$/", $qzQuoteID))
   {
      $this->strERR = GetErrorString("INVALID_SITES_QZ_QUOTE_ID");
      return false;
   }

   if(! preg_match("/^\d+$/", $siteID))
   {
      $this->strERR = GetErrorString("INVALID_SITES_SITE_ID");
      return false;
   }

   $lastSqlCmd = "SELECT * FROM ".SQL_SITES_USERS_LOGIN." WHERE qz_quote_id='$qzQuoteID' AND site_id='$siteID'";

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("SITES_QZ_QUOTE_ID_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   $arrayResult = array();
   
   $arrayResult["id"]                    = trim($this->dbh->GetFieldValue("id"));
   $arrayResult["qz_quote_id"]           = trim($this->dbh->GetFieldValue("qz_quote_id"));
   $arrayResult["site_id"]               = trim($this->dbh->GetFieldValue("site_id"));
   $arrayResult["password"]              = trim(stripslashes($this->dbh->GetFieldValue("password")));

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetAllSitesUsersLogin
//
// [DESCRIPTION]:   Read data from sites_users_login table and put it into an array variable
//
// [PARAMETERS]:
//
// [RETURN VALUE]:  $arrayResult or false in case of failure
//
// [CREATED BY]:    Ciciu Gigi  (null@seopa.com) 2006-13-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAllSitesUsersLogin()
{
   $lastSqlCmd = "SELECT * FROM ".SQL_SITES_USERS_LOGIN;

   if(! $this->dbh->Exec($lastSqlCmd))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("USERS_NOT_FOUND");
      return false;
   }

   $arrayResult = array();
   $index       = 0;
   while($this->dbh->MoveNext())
   {
		$index++;
		$arrayResult[$index]["id"]             = trim($this->dbh->GetFieldValue("id"));
		$arrayResult[$index]["qz_quote_id"]    = trim($this->dbh->GetFieldValue("qz_quote_id"));
		$arrayResult[$index]["site_id"]        = trim($this->dbh->GetFieldValue("site_id"));
		$arrayResult[$index]["password"]       = trim(stripslashes($this->dbh->GetFieldValue("password")));
   }

   return $arrayResult;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Ciciu Gigi (null@seopa.com) 2006-13-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Ciciu Gigi (null@seopa.com) 2006-13-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Show the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Ciciu Gigi (null@seopa.com) 2006-13-09
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

}//end class CSitesUsersLogin

?>

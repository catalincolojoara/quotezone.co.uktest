<?php
/*****************************************************************************/
/*                                                                           */
/*  CRenewal class interface                                              */
/*                                                                           */
/*  (C) 2005 Istvancsek Gabriel (null@seopa.com)                              */
/*                                                                           */
/*****************************************************************************/

//include_once "globals.inc";
include_once "errors.inc";
include_once "MySQL.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CRenewal
//
// [DESCRIPTION]:  CRenewal class interface
//
// [FUNCTIONS]:    
//
//                 Close();
//                 GetError();
//                 ShowError();
//
// [CREATED BY]:   Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CRenewal
{
    // database handler
    var $dbh;         // database server handle
    var $closeDB;     // close database flag

    // class-internal variables
    var $strERR;      // last USER error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CRenewal
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CRenewal($dbh=0)
{
   if($dbh)
   {
      $this->dbh = $dbh;
      $this->closeDB = false;
   }
   else
   {
      // default configuration
      $this->dbh = new CMySQL();

      if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
      {
        $this->strERR = $this->dbh->GetError();
        return;
      }

      $this->closeDB = true;
   }

    $this->strERR  = "";
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AssertRenewal
//
// [DESCRIPTION]:   Validate all parameters value
//
// [PARAMETERS]:    &$firstName, &$lastName, $birthDate='', $password=''
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AssertRenewal($quoteUserID, $qzQuoteID, $emStoreID, $crDate, $upDate)
{
   $this->strERR = '';

   if(! preg_match('/\d+/i',$quoteUserID))
      $this->strERR .= GetErrorString("INVALID_USER_ID_FIELD");

   if(! preg_match('/\d+/i',$qzQuoteID))
      $this->strERR .= GetErrorString("INVALID_QZ_QUOTE_ID_FIELD");

   if(! preg_match('/\d+/i',$emStoreID))
      $this->strERR .= GetErrorString("INVALID_EM_STORE_ID_FIELD");

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$crDate))
      $this->strERR .= GetErrorString("INVALID_CR_DATE_FIELD")."\n";

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$upDate))
      $this->strERR .= GetErrorString("INVALID_UPDATE_FIELD")."\n";

   if(! empty($this->strERR))
      return false;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddRenewal
//
// [DESCRIPTION]:   Check if exist a user and add if not exist into renewal table
//
// [PARAMETERS]:    $emStoreID, $crDate
//
// [RETURN VALUE]:  $renewalID | false
//
// [CREATED BY]:    Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddRenewal($emStoreID, $crDate, $crTime,$type,$year=1)
{

   if(! preg_match('/\d+/i',$emStoreID))
   {
      $this->strERR .= GetErrorString("INVALID_EM_STORE_ID_FIELD");
      return false;
   }

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$crDate))
   {
      $this->strERR .= GetErrorString("INVALID_CR_DATE_FIELD")."\n";
      return false;
   }
   /*
   $this->lastSQLCMD = "SELECT id FROM renewal WHERE em_store_id='$emStoreID' and year='$year'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
      return false;
   }

   if($this->dbh->GetRows())
   {
      if(! $this->dbh->FetchRows())
      {
         $this->strERR = $this->dbh->GetError();
         return false;
      }

      return $this->dbh->GetFieldValue("id");
   }
	*/
   $this->lastSQLCMD = "INSERT INTO renewal (em_store_id,cr_date,cr_time,quote_type_id,year) VALUES ('$emStoreID','$crDate','$crTime','$type','$year')";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
       $this->strERR = $this->dbh->GetError();
      return false;
   }

   $this->lastSQLCMD = "SELECT LAST_INSERT_ID() AS id";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = $this->dbh->GetError();
      return false;
   }

   return $this->dbh->GetFieldValue("id");
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateRenewal
//
// [DESCRIPTION]:   Update a user data from the renewal table
//
// [PARAMETERS]:    $renewalID, $quoteUserID, $qzQuoteID, $upDate
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:   Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateRenewal($renewalID, $quoteUserID, $qzQuoteID, $upDate, $upTime)
{

   if(! preg_match("/^\d+$/", $quoteUserID))
   {
      $this->strERR = GetErrorString("INVALID_QUOTE_USER_ID_FIELD");
      return false;
   }

   if(! preg_match('/\d+/i',$qzQuoteID))
   {
      $this->strERR .= GetErrorString("INVALID_QZ_QUOTE_ID_FIELD");
      return false;
   }

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$upDate))
   {
      $this->strERR .= GetErrorString("INVALID_UPDATE_FIELD")."\n";
      return false;
   }
   
//   if(! $this->AssertQuoteStatus($quoteUserID, '0', '0', '0000-00-00',$upDate))
//      return false;

   // check if quoteUserID exists
//   $this->lastSQLCMD = "SELECT id FROM renewal WHERE id='$renewalID'";
//
//   if(! $this->dbh->Exec($this->lastSQLCMD))
//   {
//     $this->strERR = $this->dbh->GetError();
//     return false;
//   }
//
//
//   if(! $this->dbh->GetRows())
//   {
//      $this->strERR = GetErrorString("RENEWAL_ID_NOT_FOUND");
//      return false;
//   }

   $this->lastSQLCMD = "UPDATE renewal SET quote_user_id='$quoteUserID',qz_quote_id='$qzQuoteID',update_date='$upDate',update_time='$upTime' where id='$renewalID'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

//   if($_SERVER['REMOTE_ADDR']== '86.125.114.56')
//   {
		$this->lastSQLCMD = "SELECT log_id FROM qzquotes WHERE id='$qzQuoteID'";
		
	    if(! $this->dbh->Exec($this->lastSQLCMD))
	    {
	      $this->strERR = $this->dbh->GetError();
	      return false;
	    }
	
	   if(! $this->dbh->FetchRows())
	   {
	      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
	      return false;
	   }
	   
	   $logId = $this->dbh->GetFieldValue("log_id");
   
   	   $this->lastSQLCMD = "INSERT INTO renewal_quotes (log_id,renewal_id,date,time) VALUES ('$logId','$renewalID','$upDate','$upTime')";
		
	   if(! $this->dbh->Exec($this->lastSQLCMD))
	   {
	     $this->strERR = $this->dbh->GetError();
	     return false;
	   }
//   }
   
   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRenewalsByDate
//
// [DESCRIPTION]:   Get number of renewals by date
//
// [PARAMETERS]:    $renewalID, $quoteUserID, $qzQuoteID, $upDate
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:   Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetRenewalsByDate($dateFrom, $dateTo, $year='1')
{

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$dateFrom))
   {
      $this->strERR .= GetErrorString("INVALID_DATEFROM_FIELD")."\n";
      return false;
   }
   
   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$dateTo))
   {
      $this->strERR .= GetErrorString("INVALID_DATETO_FIELD")."\n";
      return false;
   }
   
   $this->lastSQLCMD = "SELECT count(distinct(em_store_id)) as cnt FROM renewal WHERE cr_date>='$dateFrom' AND cr_date<='$dateTo' AND year='$year'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");   
   
   return $cnt;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRenewalsByDate
//
// [DESCRIPTION]:   Get number of renewals by date
//
// [PARAMETERS]:    $renewalID, $quoteUserID, $qzQuoteID, $upDate
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:   Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetRenewalsByDateAndType($dateFrom, $dateTo, $type, $year='1')
{

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$dateFrom))
   {
      $this->strERR .= GetErrorString("INVALID_DATEFROM_FIELD")."\n";
      return false;
   }
   
   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$dateTo))
   {
      $this->strERR .= GetErrorString("INVALID_DATETO_FIELD")."\n";
      return false;
   }
   
   $this->lastSQLCMD = "SELECT count(distinct(em_store_id)) as cnt FROM renewal WHERE cr_date>='$dateFrom' AND cr_date<='$dateTo' and quote_type_id='$type' and year='$year'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");   
   
   return $cnt;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRenewalsByDate
//
// [DESCRIPTION]:   Get number of renewals by date
//
// [PARAMETERS]:    $renewalID, $quoteUserID, $qzQuoteID, $upDate
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:   Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuotesByDate($dateFrom, $dateTo, $type='1',$year='1')
{

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$dateFrom))
   {
      $this->strERR .= GetErrorString("INVALID_DATEFROM_FIELD")."\n";
      return false;
   }
   
   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$dateTo))
   {
      $this->strERR .= GetErrorString("INVALID_DATETO_FIELD")."\n";
      return false;
   }
   
   $this->lastSQLCMD = "SELECT count(*) as cnt FROM renewal WHERE update_date>='$dateFrom' AND update_date<='$dateTo' and quote_type_id='$type' and year='$year'";// AND quote_user_id!='0'";
//   $this->lastSQLCMD = "SELECT count(*) as cnt FROM renewal r,qzquotes qzq WHERE r.update_date>='$dateFrom' AND r.update_date<='$dateTo' and r.qz_quote_id=qzq.id and qzq.quote_type_id='$type'";
//print $this->lastSQLCMD;
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");   
   
   return $cnt;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRenewalsByDate
//
// [DESCRIPTION]:   Get number of renewals by date
//
// [PARAMETERS]:    $renewalID, $quoteUserID, $qzQuoteID, $upDate
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:   Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetQuotesByDateAndType($dateFrom, $dateTo, $type,$year='1',$renType)
{

   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$dateFrom))
   {
      $this->strERR .= GetErrorString("INVALID_DATEFROM_FIELD")."\n";
      return false;
   }
   
   if(! preg_match('/\d{4}\-\d{2}\-\d{2}/i',$dateTo))
   {
      $this->strERR .= GetErrorString("INVALID_DATETO_FIELD")."\n";
      return false;
   }
   
//   $this->lastSQLCMD = "SELECT count(*) as cnt FROM renewal WHERE update_date>='$dateFrom' AND update_date<='$dateTo'";// AND quote_user_id!='0'";
   $this->lastSQLCMD = "SELECT count(*) as cnt FROM renewal r,qzquotes qzq WHERE r.update_date>='$dateFrom' AND r.update_date<='$dateTo' and r.qz_quote_id=qzq.id and qzq.quote_type_id='$type' and r.quote_type_id='$renType' and r.year='$year'";
//print $this->lastSQLCMD;
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");   
   
   return $cnt;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRenewalsByDate
//
// [DESCRIPTION]:   Get number of renewals by date
//
// [PARAMETERS]:    $renewalID, $quoteUserID, $qzQuoteID, $upDate
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:   Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAverageQuotesPerDay($year='1')
{

   $this->lastSQLCMD = "SELECT min(cr_date) as date1,max(cr_date) as date2 from renewal where year='$year'";// AND quote_user_id!='0'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   $date1 = $this->dbh->GetFieldValue("date1");
   $date2 = $this->dbh->GetFieldValue("date2");
   
   $this->lastSQLCMD = "SELECT count(*) as cnt FROM renewal WHERE quote_user_id!='0'  and year='$year'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");   

   //$daysInterval = intval((strtotime($date2) - strtotime($date1)) / 86400);
   if ($date2 == $date1)
        $daysInterval = 1;
   else
        $daysInterval = intval((strtotime($date2) - strtotime($date1)) / 86400);

   $avgQuotesPerDay = number_format(($cnt/$daysInterval), 2, ".","");
   
   
   return $avgQuotesPerDay;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetRenewalsByDate
//
// [DESCRIPTION]:   Get number of renewals by date
//
// [PARAMETERS]:    $renewalID, $quoteUserID, $qzQuoteID, $upDate
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:   Florin (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetAverageVisitsPerDay($year='1')
{

   $this->lastSQLCMD = "SELECT min(cr_date) as date1,max(cr_date) as date2 from renewal where year='$year'";// AND quote_user_id!='0'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   $date1 = $this->dbh->GetFieldValue("date1");
   $date2 = $this->dbh->GetFieldValue("date2");
   
   $this->lastSQLCMD = "SELECT count(*) as cnt FROM renewal where year='$year'";

   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");   

   //$daysInterval = intval((strtotime($date2) - strtotime($date1)) / 86400);
   if ($date2 == $date1)
        $daysInterval = 1;
   else
        $daysInterval = intval((strtotime($date2) - strtotime($date1)) / 86400);
   
   $avgQuotesPerDay = number_format(($cnt/$daysInterval), 2, ".","");
   
   
   return $avgQuotesPerDay;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckIfUserMadeQuote($userId, $noOfDays)
//
// [DESCRIPTION]:   Get all homeowner
//
// [PARAMETERS]:    $update="", $arrayResult
//
// [RETURN VALUE]:  true | false
//
// [CREATED BY]:    Florin MENGHEA (null@seopa.com) 23-05-2006
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckIfUserMadeQuote($emStoreID, $date, $year='1')
{
   if(! preg_match("/^\d+$/", $emStoreID))
   {
      $this->strERR = GetErrorString("INVALID_USERID_FIELD");
      return 0;
   }

   $this->lastSQLCMD = "SELECT count(*) as cnt FROM renewal where em_store_id='$emStoreID' AND update_date>='$date' and year='$year'";

//   print $this->lastSQLCMD;
   
   if(! $this->dbh->Exec($this->lastSQLCMD))
   {
     $this->strERR = $this->dbh->GetError();
     return false;
   }

   if(! $this->dbh->GetRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   if(! $this->dbh->FetchRows())
   {
      $this->strERR = GetErrorString("RECORD_NOT_FOUND");
      return false;
   }

   $cnt = $this->dbh->GetFieldValue("cnt");

   if ($cnt > 0)
      return true;

   return false;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowError
//
// [DESCRIPTION]:   Show the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ShowError()
{
    print $this->strERR;
}

}//end class CRenewal

?>

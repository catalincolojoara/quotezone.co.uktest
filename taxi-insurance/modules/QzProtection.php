<?php
/*****************************************************************************/
/*                                                                           */
/*  CQzProtection class interface                                            */
/*                                                                           */
/*  (C) 2005 Gabriel Istvancsek (null@seopa.com)                          */
/*                                                                           */
/*****************************************************************************/

// include_once "modules/globals.inc";
include_once "errors.inc";
include_once "Log.php";
include_once "QzQuoteType.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CQzProtection
//
// [DESCRIPTION]:  CQzProtection class interface
//
// [FUNCTIONS]:
// 
//                 void Close();
//                 string GetError();
//
// [CREATED BY]:   Gabriel Istvancsek (null@seopa.com) 2005-11-21
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
class CQzProtection
{
   var $cookie;
   var $session;
   var $quotesLimit;
   var $hostLimit;
   var $cookiePath;
   var $quoteTypeID;

   var $objLog;
   
   // class-internal variables
   var $strERR;      // last USER error string

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: CQzProtection
   //
   // [DESCRIPTION]:   Default class constructor. Initialization goes here.
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:   Gabriel Istvancsek (null@seopa.com) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function CQzProtection($quoteTypeID, $cookie, $session)
   {
      $this->cookie       = $cookie;  // COOKIE content
      $this->session      = $session; // SESSION content
      $this->quotesLimit  = 5;        // Request Limit by cookies, session and quoteUserID => max
      $this->hostLimit    = 10;       // Request Limit content - different users from same host
      
      $this->strERR       = ''; //last error string

      $this->objLog       = new CLog();
      $objQuoteType       = new CQzQuoteType();

      if(! $resQuoteType  = $objQuoteType->GetQuoteTypeByID($quoteTypeID))
         $this->strERR    = $objQuoteType->GetError();

      $this->cookiePath   = "/".strtolower($resQuoteType['name']);
      $this->quoteTypeID  = $quoteTypeID;

   }// end function CQzProtection()
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ValidateCookieRequest
   //
   // [DESCRIPTION]:   Validate Quote User by cookie request
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  false | number of quotes which have make today
   //
   // [CREATED BY]:   Gabriel Istvancsek (null@seopa.com) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function _validateCookieRequest()
   {
      // we have to check the active counter from session
      $quotesCounter =  base64_decode($this->cookie["key"]);
      $quotesDate    =  base64_decode($this->cookie["value"]);

      if($quotesDate != date("Y-m-d"))
      {
         $quotesCounter = 1;
         $this->cookie["key"] = base64_encode($quotesCounter);
      }

      if($this->quotesLimit < $quotesCounter)
         return false;
      
      return true;
      
   }// end function ValidateRequest()
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ValidateCookieRequest
   //
   // [DESCRIPTION]:   Validate Quote User by cookie request
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  false | number of quotes which have make today
   //
   // [CREATED BY]:   Gabriel Istvancsek (null@seopa.com) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function _setCookieRequest()
   {
      // we have to check the active counter from session
      $quotesCounter =  base64_decode($this->cookie["key"]);
      $quotesDate    =  base64_decode($this->cookie["value"]);

      // if we dont have cookie  set up already
      if($quotesDate != date("Y-m-d"))
         $quotesCounter = 1;
      else
         $quotesCounter++;

      $cookieExpire = mktime(0,0,0,date("m"),date("d")+1,date("Y"));

      if(! setcookie("value",base64_encode(date("Y-m-d")),$cookieExpire,$this->cookiePath,".quotezone.co.uk"))
      {
         $this->strERR = 'QZ_PROTECTION_CANT_SET_COOKIE';
         return false;
      }

      if(! setcookie("key",base64_encode($quotesCounter),$cookieExpire,$this->cookiePath,".quotezone.co.uk"))
      {
         $this->strERR = 'QZ_PROTECTION_CANT_SET_COOKIE';
         return false;
      }

      return true;
      
   }// end function ValidateRequest()

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ValidateSessionRequest
   //
   // [DESCRIPTION]:   Validate Quote User by cookie request
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  false | number of quotes which have make today
   //
   // [CREATED BY]:   Gabriel Istvancsek (null@seopa.com) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function _validateSessionRequest()
   {
      // we have to check the active counter from session
      $quotesCounter =  $this->session["QUOTE_NUMBER"];

      // if we dont have cookie  set up already
      if(! isset($quotesCounter))
         $quotesCounter = 1;
      else
         $quotesCounter++;

      if($this->quotesLimit < $this->session["QUOTE_NUMBER"])
         return false;

      return true;

   }// end function ValidateRequest()

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ValidateSessionRequest
   //
   // [DESCRIPTION]:   Validate Quote User by cookie request
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  false | number of quotes which have make today
   //
   // [CREATED BY]:   Gabriel Istvancsek (null@seopa.com) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function _setSessionRequest()
   {
      // we have to check the active counter from session
      $quotesCounter =  $this->session["QUOTE_NUMBER"];

      // if we dont have cookie  set up already
      if(! isset($quotesCounter))
         $quotesCounter = 1;
      else
         $quotesCounter++;

      $this->session["QUOTE_NUMBER"] = $quotesCounter;

      return true;

   }// end function ValidateRequest()
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ValidateUserRequest
   //
   // [DESCRIPTION]:   Validate Quote User by cookie request
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  false | number of quotes which have make today
   //
   // [CREATED BY]:   Gabriel Istvancsek (null@seopa.com) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function _validateUserRequest()
   {
      $quoteUserID = $this->session['_QZ_QUOTE_DETAILS_']['quote_user_id'];

      // check if user is a new one ...
      if(empty($quoteUserID))
         return true;

      $numberOfQuotesToday = $this->objLog->GetCountLogsByHostIpQuoteUserToday('',$quoteUserID,$this->quoteTypeID);

      if($this->quotesLimit < $numberOfQuotesToday)
         return false;

      return true;

   }// end function ValidateUserRequest()

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ValidateHostRequest
   //
   // [DESCRIPTION]:   Validate Quote User by cookie request
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  false | number of quotes which have make today
   //
   // [CREATED BY]:   Gabriel Istvancsek (null@seopa.com) 2005-11-21
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function _validateHostRequest()
   {
      $remoteAddress = $_SERVER['REMOTE_ADDR'];

      $numberOfQuotesToday = $this->objLog->GetCountLogsByHostIpQuoteUserToday($remoteAddress,'',$this->quoteTypeID);

      if($this->hostLimit < $numberOfQuotesToday)
         return false;

      return true;

   }// end function ValidateHostRequest()
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ValidateRequest
   //
   // [DESCRIPTION]:   Validate Request
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  false | true
   //
   // [CREATED BY]:   Gabriel Istvancsek (null@seopa.com) 2005-12-12
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function ValidateRequest()
   {

      if(! $this->_validateCookieRequest())
         return false;

      if(! $this->_validateSessionRequest())
         return false;

      if(! $this->_validateUserRequest())
         return false;
    
      if(! $this->_validateHostRequest())
         return false;

      return true;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: ValidateRequest
   //
   // [DESCRIPTION]:   Validate Request
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  false | true
   //
   // [CREATED BY]:   Gabriel Istvancsek (null@seopa.com) 2005-12-12
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function SetRequest()
   {

      // set cookie
      $this->_setCookieRequest();

      // set session
      $this->_setSessionRequest();

      return $this->session;
   }

}// end class CQzProtection
?>

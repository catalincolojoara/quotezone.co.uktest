<?php
error_reporting(0);
session_start();

include_once "../modules/globals.inc";
include_once "TrackUrls.php";
include_once "Tracker.php";

$siteID  = $_GET['siteID'];
$hostIP  = $_SERVER['REMOTE_ADDR'];
$urlName = $_GET['urlName'];
$urlAddr = $_GET['urlAddr'];
//$info    = $_GET['urlInfo'];

$info    = "QuoteRef: ".$_SESSION['_QZ_QUOTE_DETAILS_']['quote_reference'];
$logID   = $_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id'];

$trackUrls = new CTrackUrls();
$tracker   = new CTracker();

if(! $trackUrlInfo = $trackUrls->GetTrackUrlByName($urlName))
{
   echo $trackUrls->GetError();
   exit(0);
}

LogMsgDB($siteID,$logID,$trackUrlInfo["id"], $info, $hostIP);

if($urlAddr)
   Redirect($urlAddr);
else
   Redirect($trackUrlInfo["url"]);

function Redirect($url = "")
{
   if (headers_sent())
   {
      print <<<  END_TAG

      <SCRIPT LANGUAGE="JavaScript">
      //<!--
         location.href="$url";
      //-->
      </SCRIPT>

END_TAG;
   }
   else
   {
      $today = date("D M j G:i:s Y");
      header("HTTP/1.1 200 OK");
      header("Date: $today");
      header("Location: $url");
      header("Content-Type: text/html");
   }

   exit(0);
}

 function LogMsg($msg="")
 {
   $fd = fopen("tracker.log", "a+");

   if(! $fd)
      return;

   $today = date("D M j G:i:s Y");

   fwrite($fd, "$today $msg\n");

   fclose($fd);
 }

 function LogMsgDB($siteID=0,$logID=0,$trackUrlID=0, $additionalInfo="", $hostIP="")
 {
   global $tracker;

   $time = time();

   $tracker->AddTracker($siteID,$logID,$trackUrlID, $additionalInfo, $hostIP, $time);
 }

?>

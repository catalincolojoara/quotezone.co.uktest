<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11140',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '929',
    'companyName' => 'J Willey ',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Taxi (Kirklees) Query:',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Paula@jwilley.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$taxiNcb=$_SESSION["_YourDetails_"]["taxi_ncb"]; $taxiBadge=$_SESSION["_YourDetails_"]["taxi_badge"]; if($taxiNcb == "0" && $taxiBadge < "5") $_companyDetails["filter_details"][]["BSPK_SKP_NCB_BADGE"]["ACC;VAL;EQL;SKIP if Taxi no claims bonus = 0 NCB AND Taxi badge = No Badge, Under 6 months, 6 months to 1 year, 1-2 years."]  = "BSPK_SKP";',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'KIRKLEES',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 65' => '25-65',
      ),
    ),
    2 => 
    array (
      'taxi_used_for' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
    3 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BD[0-99]*',
        ),
      ),
    ),
    5 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 2000 and 100000' => '2000 - 100000',
      ),
    ),
  ),
)


 ?>
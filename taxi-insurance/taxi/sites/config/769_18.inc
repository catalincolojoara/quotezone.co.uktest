<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '769',
    'siteOrigID' => '',
    'brokerID' => '36',
    'logoID' => '34',
    'companyName' => 'Think (OOH)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '10',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxiandmbquotes@think-ins.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|17:00-23:59',
          2 => '00:00-09:00|17:00-23:59',
          3 => '00:00-09:00|17:00-23:59',
          4 => '00:00-09:00|17:00-23:59',
          5 => '00:00-09:00|17:00-23:59',
          6 => '00:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
        ),
      ),
    ),
    3 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0 - 14',
      ),
    ),
    4 => 
    array (
      'taxi_used_for' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
        ),
      ),
    ),
    5 => 
    array (
      'convictions_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
          6 => '15',
          7 => '16',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    8 => 
    array (
      'type_of_cover' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
        ),
      ),
    ),
    9 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 69' => '30 - 69',
      ),
    ),
    10 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BARNSLEY',
          1 => 'BASSETLAW',
          2 => 'BIRMINGHAM',
          3 => 'BLACKBURN WITH DARWEN',
          4 => 'BLYTH VALLEY',
          5 => 'BOLSOVER',
          6 => 'BOLTON',
          7 => 'BRADFORD',
          8 => 'BURNLEY',
          9 => 'BURY',
          10 => 'CALDERDALE',
          11 => 'CARDIFF',
          12 => 'CHESTERFIELD',
          13 => 'CHORLEY',
          14 => 'COVENTRY',
          15 => 'CRAVEN',
          16 => 'CREWE & NANTWICH',
          17 => 'DERBY',
          18 => 'DONCASTER',
          19 => 'DUDLEY',
          20 => 'ELLESEMERE PORT & NESTON',
          21 => 'GATESHEAD',
          22 => 'GLASGOW CITY',
          23 => 'HALTON',
          24 => 'HYNDBURN',
          25 => 'KINGSTON-UPON-HULL',
          26 => 'KIRKLEES',
          27 => 'KNOWSLEY',
          28 => 'LEEDS',
          29 => 'LEICESTER',
          30 => 'LIVERPOOL',
          31 => 'LUTON',
          32 => 'MANCHESTER',
          33 => 'MEDWAY',
          34 => 'MILTON KEYNES',
          35 => 'NEWCASTLE UPON TYNE',
          36 => 'NEWPORT',
          37 => 'NORTH EAST DERBYSHIRE',
          38 => 'NORTH TYNESIDE',
          39 => 'NOTTINGHAM',
          40 => 'OLDHAM',
          41 => 'ORKNEY ISLANDS (KIRKWALL)',
          42 => 'OSWESTRY',
          43 => 'PENDLE',
          44 => 'RESTORMEL',
          45 => 'RIBBLE VALLEY',
          46 => 'ROCHDALE',
          47 => 'ROSSENDALE',
          48 => 'ROTHERHAM',
          49 => 'SALFORD',
          50 => 'SANDWELL',
          51 => 'SEFTON',
          52 => 'SHEFFIELD',
          53 => 'SHETLAND ISLANDS (LERWICK)',
          54 => 'SLOUGH',
          55 => 'SOUTH RIBBLE',
          56 => 'SOUTH STAFFORDSHIRE',
          57 => 'SOUTH TYNESIDE',
          58 => 'STOCKPORT',
          59 => 'SUNDERLAND',
          60 => 'SWANSEA',
          61 => 'TAMESIDE',
          62 => 'TEESDALE',
          63 => 'TRAFFORD',
          64 => 'VALE ROYAL',
          65 => 'WAKEFIELD',
          66 => 'WALSALL',
          67 => 'WEAR VALLEY',
          68 => 'WESTERN ISLES (STORNOWAY)',
          69 => 'WIGAN',
          70 => 'WINDSOR & MAIDENHEAD',
          71 => 'WIRRALL',
          72 => 'WOLVERHAMPTON',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10344',
    'siteOrigID' => '',
    'brokerID' => '90',
    'logoID' => '500',
    'companyName' => 'Insurance 4u Services (Uber)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Taxi (Uber) Query:',
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'chris@carinsurance4u.co.uk',
        1 => 'dan@carinsurance4u.co.uk',
        2 => 'chrisjones@carinsurance4u.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 70' => '25 - 70',
      ),
    ),
    3 => 
    array (
      'uber_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'Y',
        ),
      ),
    ),
    4 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 28' => '0 - 28',
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BIRMINGHAM',
          1 => 'BROMSGROVE',
          2 => 'CANNOCK CHASE',
          3 => 'DUDLEY',
          4 => 'EAST STAFFORDSHIRE',
          5 => 'LICHFIELD',
          6 => 'NORTH WARWICKSHIRE',
          7 => 'REDDITCH',
          8 => 'SANDWELL',
          9 => 'SHROPSHIRE COUNTY',
          10 => 'SOLIHULL',
          11 => 'SOUTH STAFFORDSHIRE',
          12 => 'STAFFORD',
          13 => 'TAMWORTH',
          14 => 'TELFORD & WREKIN',
          15 => 'WALSALL',
          16 => 'WOLVERHAMPTON',
          17 => 'WYRE FORREST',
        ),
      ),
    ),
    6 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'DY[1-12]',
          2 => 'ST12',
          3 => 'ST[14-21]',
          4 => 'SY[1-14]',
          5 => 'TF[1-13]',
          6 => 'WS[1-15]',
          7 => 'WV[1-16]',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    8 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
  ),
)


 ?>
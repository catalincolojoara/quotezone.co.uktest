<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10082',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '91',
    'companyName' => 'One Answer - QZ027 Taxi 2 (OOH) (0 NCB)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'One Answer - QZ027 Taxi 2 (OOH) (0 NCB)',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'QZ027_lead@1answer.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => 'include_once "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/taxi/steps/functions.php"; $privateCarNcb = $_SESSION["_YourDetails_"]["private_car_ncb"]; $ageNCBMsg     = "";     $DISdd    = $_SESSION["_YourDetails_"]["date_of_insurance_start_dd"]; $DISmm    = $_SESSION["_YourDetails_"]["date_of_insurance_start_mm"]; $DISyyyy  = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"]; $dobDD    = $_SESSION["_YourDetails_"]["date_of_birth_dd"]; $dobMM    = $_SESSION["_YourDetails_"]["date_of_birth_mm"]; $dobYYYY  = $_SESSION["_YourDetails_"]["date_of_birth_yyyy"]; $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd; $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD; $propAge = GetAge($proposerBirthDate,$insuranceStartDate); if($propAge < 25) {$_companyDetails["filter_details"][]["private_car_ncb"]["SKP;LIST;; If Age < 25 SKIP if Private car no claims bonus = No NCB "] = array(0 => "0",); } ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|19:00-23:59',
          2 => '00:00-09:00|19:00-23:59',
          3 => '00:00-09:00|19:00-23:59',
          4 => '00:00-09:00|19:00-23:59',
          5 => '00:00-09:00|19:00-23:59',
          6 => '00:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '15',
          1 => '16',
        ),
      ),
    ),
    2 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0-14',
      ),
    ),
    3 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 22 and 100' => '22-100',
      ),
    ),
    4 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'ADUR',
          1 => 'ARUN',
          2 => 'ASHFORD',
          3 => 'BRADFORD',
          4 => 'BRIGHTON & HOVE',
          5 => 'CRAWLEY',
          6 => 'LEWES',
          7 => 'LONDON PCO',
          8 => 'MID SUSSEX',
          9 => 'REIGATE AND BANSTEAD',
          10 => 'ROTHER',
          11 => 'SEVENOAKS',
          12 => 'TUNBRIDGE WELLS',
          13 => 'WEALDEN',
          14 => 'WORTHING',
        ),
      ),
    ),
    6 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 1500 and 100000' => '1500 - 100000',
      ),
    ),
    7 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'CA[0-99]*',
          1 => 'CB[0-99]*',
          2 => 'DD[0-99]*',
          3 => 'HG[0-99]*',
          4 => 'HP[0-99]*',
          5 => 'HR[0-99]*',
          6 => 'HX[0-99]*',
          7 => 'IP[0-99]*',
          8 => 'KT[0-99]*',
          9 => 'LL[0-99]*',
          10 => 'LN[0-99]*',
          11 => 'MK[0-99]*',
          12 => 'PA[0-99]*',
          13 => 'YO[0-99]*',
        ),
      ),
    ),
    8 => 
    array (
      'type_of_cover' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
    9 => 
    array (
      'private_car_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    10 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
        ),
      ),
    ),
    11 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '5',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '2959',
    'siteOrigID' => '',
    'brokerID' => '57',
    'logoID' => '724',
    'companyName' => 'Milestone - Taxi',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => '',
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Rob.g@milestonehouse.com',
        1 => 'Nathan.t@milestonehouse.com',
        2 => 'Ryan.c@milestonehouse.com',
        3 => 'Devon.c@milestonehouse.com',
        4 => 'Brenda.b@milestonehouse.com',
        5 => 'Caroline.w@milestonehouse.com',
        6 => 'Shauna.s@milestonehouse.com',
        7 => 'Natalie.g@milestonehouse.com',
        8 => 'Jack.w@milestonehouse.com',
        9 => 'James.h@milestonehouse.com',
        10 => 'Matthew.o@milestonehouse.com',
        11 => 'red@rcdata.co.uk',
        12 => 'Darren.c@milestonehouse.com',
        13 => 'Nicholas.l@milestonehouse.com',
        14 => 'Chris.h@milestonehouse.com',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$platingAuthority = $_SESSION["_YourDetails_"]["plating_authority"]; $taxiNcb = $_SESSION["_YourDetails_"]["taxi_ncb"]; if($platingAuthority == "LONDON PCO" && $taxiNcb == "0") $_companyDetails["filter_details"][]["BSK_SKP_LIC_NCB"]["ACC;VAL;EQL;If Taxi Licensing Authority = London PCO SKIP if Taxi NCB = No NCB ."]  = "BSK_SKP"; ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-16:45',
          2 => '09:00-16:45',
          3 => '09:00-16:45',
          4 => '09:00-16:45',
          5 => '09:00-16:45',
          6 => '09:00-16:45',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
          3 => '10',
          4 => '11',
          5 => '12',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '5',
          1 => '6',
          2 => '7',
          3 => '8',
          4 => '9',
          5 => '10',
          6 => '11',
          7 => '12',
          8 => '13',
        ),
      ),
    ),
    3 => 
    array (
      'claims_5_years' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'No',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
        ),
      ),
    ),
    6 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BIRMINGHAM',
          1 => 'BLACKBURN WITH DARWEN',
          2 => 'BOLTON',
          3 => 'BRADFORD',
          4 => 'BURNLEY',
          5 => 'BURY',
          6 => 'LIVERPOOL',
          7 => 'LONDON PCO',
          8 => 'MANCHESTER',
          9 => 'NEWCASTLE UPON TYNE',
          10 => 'OLDHAM',
          11 => 'ROCHDALE',
          12 => 'SANDWELL',
          13 => 'SEFTON',
          14 => 'ST HELENS',
          15 => 'THREE RIVERS',
          16 => 'WATFORD',
        ),
      ),
    ),
    7 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 70' => '30 - 70',
      ),
    ),
    8 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 21' => '0 - 21',
      ),
    ),
    9 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BL[0-99]*',
          4 => 'BR[0-99]*',
          5 => 'BT[0-99]*',
          6 => 'CR[0-99]*',
          7 => 'CV[0-99]*',
          8 => 'DA[0-99]*',
          9 => 'DY[0-99]*',
          10 => 'E[0-99]*',
          11 => 'EC[0-99]*',
          12 => 'EN[0-99]*',
          13 => 'HA[0-99]*',
          14 => 'IG[0-99]*',
          15 => 'KT[0-99]*',
          16 => 'L[0-99]*',
          17 => 'LU[0-99]*',
          18 => 'M[0-99]*',
          19 => 'ME[0-99]*',
          20 => 'N[0-99]*',
          21 => 'NE[0-99]*',
          22 => 'NW[0-99]*',
          23 => 'OL[0-99]*',
          24 => 'PR[0-99]*',
          25 => 'RM[0-99]*',
          26 => 'SE[0-99]*',
          27 => 'SK[0-99]*',
          28 => 'SL[0-99]*',
          29 => 'SM[0-99]*',
          30 => 'SR[0-99]*',
          31 => 'SS[0-99]*',
          32 => 'SW[0-99]*',
          33 => 'TS[0-99]*',
          34 => 'TW[0-99]*',
          35 => 'UB[0-99]*',
          36 => 'W[0-99]*',
          37 => 'WC[0-99]*',
          38 => 'WD[0-99]*',
          39 => 'WS[0-99]*',
          40 => 'WV[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '2374',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '91',
    'companyName' => 'One Answer - QZ036 QZ OR Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'One Answer - QZ036 QZ OR Taxi',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'qz036_lead@1answer.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'url' => '1',
  ),
  'bespoke_filters' => 
  array (
    0 => '$birthDate =""; $birthDate = $_SESSION["_YourDetails_"]["date_of_birth_yyyy"]."-".$_SESSION["_YourDetails_"]["date_of_birth_mm"]."-".$_SESSION["_YourDetails_"]["date_of_birth_dd"]; $DISDate   = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"]."-".$_SESSION["_YourDetails_"]["date_of_insurance_start_mm"]."-".$_SESSION["_YourDetails_"]["date_of_insurance_start_dd"];  list($birthYear,$birthMonth,$birthDay) = explode("-", $birthDate);    list($curYear,$curMonth,$curDay)       = explode("-", $DISDate);    $yearDiff  = $curYear  - $birthYear;    $monthDiff = $curMonth - $birthMonth;    $dayDiff   = $curDay   - $birthDay;    if(($monthDiff < 0) || ($monthDiff == 0 && $dayDiff < 0)){      $yearDiff--;} if($yearDiff<25 && $_SESSION["_YourDetails_"]["taxi_ncb"] == 0){$_companyDetails["filter_details"][]["REJECT"]["ACC;VAL;MAX;Proposer Age < 25 and Taxi no claims bonus = No NCB SKIP"] = "";}',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-19:00',
          2 => '09:00-19:00',
          3 => '09:00-19:00',
          4 => '09:00-19:00',
          5 => '09:00-19:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
          3 => '4',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'SKP;VAL;MIN;Skip if minimum value is 10' => '10',
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BL[0-99]*',
          4 => 'BT[0-99]*',
          5 => 'E[0-99]*',
          6 => 'FY[0-99]*',
          7 => 'HD[0-99]*',
          8 => 'L[0-99]*',
          9 => 'LU[0-99]*',
          10 => 'M[0-99]*',
          11 => 'NW[0-99]*',
          12 => 'OL[0-99]*',
          13 => 'S[0-99]*',
          14 => 'SW[0-99]*',
          15 => 'UB[0-99]*',
          16 => 'W[0-99]*',
          17 => 'WF[0-99]*',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '5',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '15',
          1 => '16',
        ),
      ),
    ),
    8 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    9 => 
    array (
      'private_car_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
        ),
      ),
    ),
    10 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 60' => '30 - 60',
      ),
    ),
    11 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
        ),
      ),
    ),
    12 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    13 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BARKING',
          1 => 'BARNET',
          2 => 'BEXLEY',
          3 => 'BOLTON',
          4 => 'BRENT',
          5 => 'BROMLEY',
          6 => 'CAMDEN',
          7 => 'CROYDEN',
          8 => 'EALING',
          9 => 'ENFIELD',
          10 => 'GREENWICH',
          11 => 'HACKNEY',
          12 => 'HAMMERSMITH',
          13 => 'HARINGEY',
          14 => 'HAVERING',
          15 => 'HILLINGDON',
          16 => 'HOUNSLOW',
          17 => 'ISLINGTON',
          18 => 'KENSINGTON',
          19 => 'KINGSTON UPON THAMES ROYAL',
          20 => 'KNOWSLEY',
          21 => 'LAMBETH',
          22 => 'LEEDS',
          23 => 'LEWISHAM',
          24 => 'LONDON PCO',
          25 => 'MERTON',
          26 => 'MIDDLESBOROUGH',
          27 => 'MILTON KEYNES',
          28 => 'NEWHAM',
          29 => 'REDBRIDGE',
          30 => 'RICHMOND',
          31 => 'ROSSENDALE',
          32 => 'SLOUGH',
          33 => 'SOLIHULL',
          34 => 'SOUTHWARK',
          35 => 'SUTTON',
          36 => 'TOWER HAMLETS',
          37 => 'WALTHAM FOREST',
          38 => 'WANDSWORTH',
          39 => 'WARRINGTON',
          40 => 'WESTMINSTER',
        ),
      ),
    ),
  ),
)


 ?>
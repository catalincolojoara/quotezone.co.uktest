<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11755',
    'siteOrigID' => '',
    'brokerID' => '36',
    'logoID' => '34',
    'companyName' => 'Think Insurance 2 (OOH)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Taxi (2) Query:',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
        1 => 'michael.kirk@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxiandmbquotes@think-ins.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|21:00-23:59',
          2 => '00:00-09:00|21:00-23:59',
          3 => '00:00-09:00|21:00-23:59',
          4 => '00:00-09:00|21:00-23:59',
          5 => '00:00-09:00|21:00-23:59',
          6 => '00:00-09:00',
          7 => '21:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 15' => '15',
      ),
    ),
    2 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
        ),
      ),
    ),
    3 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0-14',
      ),
    ),
    4 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    5 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 65' => '30-65',
      ),
    ),
    6 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    8 => 
    array (
      'private_car_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '5',
        ),
      ),
    ),
    9 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    10 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BB[1-6',
          1 => 'BB[10-12]',
          2 => 'BD[1-10]',
          3 => 'BD14',
          4 => 'BD18',
          5 => 'BL[0-99]*',
          6 => 'BT[0-99]*',
          7 => 'E[0-99]*',
          8 => 'EC[0-99]*',
          9 => 'G[0-99]*',
          10 => 'L[1-18]',
          11 => 'L20',
          12 => 'LS[1-13]',
          13 => 'LS28',
          14 => 'M[1-8]',
          15 => 'M[11-18]',
          16 => 'M21',
          17 => 'M30',
          18 => 'M32',
          19 => 'M40',
          20 => 'M41',
          21 => 'N[0-99]*',
          22 => 'NE[1-12]',
          23 => 'NE15',
          24 => 'NE16',
          25 => 'NE21',
          26 => 'NE28',
          27 => 'NE31',
          28 => 'NW[0-99]*',
          29 => 'SE[0-99]*',
          30 => 'SW[0-99]*',
          31 => 'TS[1-5]',
          32 => 'W[0-99]*',
          33 => 'WC[0-99]*',
        ),
      ),
    ),
    11 => 
    array (
      'type_of_cover' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
          2 => '4',
        ),
      ),
    ),
    12 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 2000 and 35000' => '2000 - 35000',
      ),
    ),
    13 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '10',
          1 => '11',
          2 => '12',
          3 => '13',
          4 => '14',
          5 => '15',
          6 => '16',
        ),
      ),
    ),
  ),
)


 ?>
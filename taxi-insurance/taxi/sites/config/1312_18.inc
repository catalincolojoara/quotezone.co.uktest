<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '1312',
    'siteOrigID' => '',
    'brokerID' => '60',
    'logoID' => '51',
    'companyName' => 'XYZ - Taxi (2)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'XYZ - Taxi (2)',
    'limit' => 
    array (
      'day' => '5',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotes@xyzinsurance.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 35 and 60' => '35 - 60',
      ),
    ),
    1 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'year_of_manufacture' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2013',
          1 => '2014',
          2 => '2015',
          3 => '2016',
          4 => '2017',
          5 => '2018',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
          5 => '9',
          6 => '10',
          7 => '11',
          8 => '12',
          9 => '13',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    5 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '10',
          1 => '11',
          2 => '12',
        ),
      ),
    ),
    6 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'LONDON PCO',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    8 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
        ),
      ),
    ),
    9 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 21' => '0-21',
      ),
    ),
    10 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'CM[0-99]*',
          1 => 'DA2',
          2 => 'DA3',
          3 => 'DA4',
          4 => 'DA9',
          5 => 'DA10',
          6 => 'DA11',
          7 => 'DA12',
          8 => 'DA13',
          9 => 'EN6',
          10 => 'EN7',
          11 => 'EN8',
          12 => 'EN9',
          13 => 'GU[0-99]*',
          14 => 'HP[0-99]*',
          15 => 'IG7',
          16 => 'IG9',
          17 => 'IG10',
          18 => 'KT7',
          19 => 'KT10',
          20 => 'KT12',
          21 => 'KT13',
          22 => 'KT15',
          23 => 'KT16',
          24 => 'KT17',
          25 => 'KT19',
          26 => 'ME[0-99]*',
          27 => 'RH[0-99]*',
          28 => 'SL2',
          29 => 'SL4',
          30 => 'SL5',
          31 => 'SS[0-99]*',
          32 => 'TN[0-99]*',
          33 => 'WD[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
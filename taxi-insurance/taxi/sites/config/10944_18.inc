<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10944',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '19',
    'companyName' => 'Broadsure Direct - Taxi 1',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'wed' => '5',
      'fri' => '5',
      'mon' => '5',
      'thu' => '5',
      'tue' => '5',
    ),
    'emailSubject' => 'Broadsure Direct - Taxi 1',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'sales@broadsuredirect.com',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$taxiNcb=$_SESSION["_YourDetails_"]["taxi_ncb"]; $privateCarNcb=$_SESSION["_YourDetails_"]["private_car_ncb"]; if($taxiNcb == "0" && $privateCarNcb == "0") $_companyDetails["filter_details"][]["BSPK_SKP_NCB_PCNCB"]["ACC;VAL;EQL;Skip if Taxi no claims bonus and Private car no claims bonus both = No NCB ."]  = "SKP"; ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-18:00',
          2 => '09:00-18:00',
          3 => '09:00-18:00',
          4 => '09:00-18:00',
          5 => '09:00-18:00',
          6 => '09:00-13:00',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
        ),
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'private_car_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    6 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 69' => '25-69',
      ),
    ),
    7 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'ADUR',
          1 => 'ARUN',
          2 => 'ASHFORD',
          3 => 'BELFAST',
          4 => 'BIRMINGHAM',
          5 => 'BLACKBURN WITH DARWEN',
          6 => 'BRADFORD',
          7 => 'BRIGHTON & HOVE',
          8 => 'BURNLEY',
          9 => 'CANTERBURY',
          10 => 'CHICHESTER',
          11 => 'CRAWLEY',
          12 => 'DARTFORD',
          13 => 'DOVER',
          14 => 'DVA (NI)',
          15 => 'EASTBOURNE',
          16 => 'GILLINGHAM',
          17 => 'GRAVESHAM',
          18 => 'GUILDFORD',
          19 => 'HASTINGS',
          20 => 'HORSHAM',
          21 => 'ISLE OF MAN',
          22 => 'JERSEY',
          23 => 'LEICESTER',
          24 => 'LEWES',
          25 => 'LIVERPOOL',
          26 => 'LONDON PCO',
          27 => 'LUTON',
          28 => 'MAIDSTONE',
          29 => 'MANCHESTER',
          30 => 'MID SUSSEX',
          31 => 'NORTHERN IRELAND',
          32 => 'PSV JERSEY',
          33 => 'REDDITCH',
          34 => 'REIGATE AND BANSTEAD',
          35 => 'ROCHESTER',
          36 => 'ROTHER',
          37 => 'SALFORD',
          38 => 'SEFTON',
          39 => 'SEVENOAKS',
          40 => 'SHEPWAY',
          41 => 'SOLIHULL',
          42 => 'ST HELIER - JERSEY',
          43 => 'SWALE',
          44 => 'TAMESIDE',
          45 => 'TANDRIDGE',
          46 => 'THANET',
          47 => 'TONBRIDGE & MALLING',
          48 => 'TRAFFORD',
          49 => 'TUNBRIDGE WELLS',
          50 => 'WEALDEN',
          51 => 'WESTMINSTER',
          52 => 'WORTHING',
        ),
      ),
    ),
  ),
)


 ?>
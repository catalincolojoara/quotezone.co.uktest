<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10631',
    'siteOrigID' => '',
    'brokerID' => '33',
    'logoID' => '91',
    'companyName' => 'One Answer - QZ031',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'One Answer - QZ031',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'QZ031_LEAD@1ANSWER.CO.UK',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-18:00',
          6 => '00:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
          5 => '9',
          6 => '10',
          7 => '11',
          8 => '12',
        ),
      ),
    ),
    2 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    3 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
          3 => '4',
          4 => '5',
          5 => '6',
          6 => '7',
          7 => '8',
          8 => '9',
          9 => '10',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
          5 => '9',
          6 => '10',
          7 => '11',
          8 => '12',
          9 => '13',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
        ),
      ),
    ),
    7 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 65' => '25-65',
      ),
    ),
    8 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 2250 and 100000' => ' 2250 - 100000',
      ),
    ),
    9 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    10 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BARKING',
          1 => 'BARNET',
          2 => 'BARNSLEY',
          3 => 'BELFAST',
          4 => 'BEXLEY',
          5 => 'BIRMINGHAM',
          6 => 'BLACKBURN WITH DARWEN',
          7 => 'BLACKPOOL',
          8 => 'BOLTON',
          9 => 'BRADFORD',
          10 => 'BRENT',
          11 => 'BROMLEY',
          12 => 'BURY',
          13 => 'CALDERDALE',
          14 => 'CAMDEN',
          15 => 'CHESTERFIELD',
          16 => 'COVENTRY',
          17 => 'CROYDEN',
          18 => 'DARTFORD',
          19 => 'DERBY',
          20 => 'DUDLEY',
          21 => 'EALING',
          22 => 'ENFIELD',
          23 => 'GEDLING',
          24 => 'GREENWICH',
          25 => 'HACKNEY',
          26 => 'HALTON',
          27 => 'HAMMERSMITH',
          28 => 'HARINGEY',
          29 => 'HAVERING',
          30 => 'HILLINGDON',
          31 => 'HOUNSLOW',
          32 => 'ISLINGTON',
          33 => 'KENSINGTON',
          34 => 'KINGSTON UPON THAMES ROYAL',
          35 => 'KIRKLEES',
          36 => 'KNOWSLEY',
          37 => 'LAMBETH',
          38 => 'LEEDS',
          39 => 'LEICESTER',
          40 => 'LEWISHAM',
          41 => 'LIVERPOOL',
          42 => 'LONDON PCO',
          43 => 'LUTON',
          44 => 'MANCHESTER',
          45 => 'MERTON',
          46 => 'MIDDLESBOROUGH',
          47 => 'MILTON KEYNES',
          48 => 'NEWCASTLE UPON TYNE',
          49 => 'NEWHAM',
          50 => 'NORTHERN IRELAND',
          51 => 'NOTTINGHAM',
          52 => 'OLDHAM',
          53 => 'PRESTON',
          54 => 'PSV EASTERN',
          55 => 'PSV JERSEY',
          56 => 'PSV NORTH EAST',
          57 => 'PSV NORTH WEST',
          58 => 'REDBRIDGE',
          59 => 'RICHMOND',
          60 => 'ROCHDALE',
          61 => 'ROSSENDALE',
          62 => 'SALFORD',
          63 => 'SANDWELL',
          64 => 'SEFTON',
          65 => 'SHEFFIELD',
          66 => 'SLOUGH',
          67 => 'SOLIHULL',
          68 => 'SOUTH RIBBLE',
          69 => 'SOUTH TYNESIDE',
          70 => 'SOUTHWARK',
          71 => 'STOCKPORT',
          72 => 'SUNDERLAND',
          73 => 'SUTTON',
          74 => 'TAMWORTH',
          75 => 'THURROCK',
          76 => 'TOWER HAMLETS',
          77 => 'TRAFFORD',
          78 => 'WAKEFIELD',
          79 => 'WALSALL',
          80 => 'WALTHAM FOREST',
          81 => 'WANDSWORTH',
          82 => 'WARRINGTON',
          83 => 'WATFORD',
          84 => 'WESTMINSTER',
          85 => 'WIGAN',
          86 => 'WIRRALL',
          87 => 'WOLVERHAMPTON',
        ),
      ),
    ),
  ),
)


 ?>
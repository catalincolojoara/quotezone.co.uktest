<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '1717',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '633',
    'companyName' => 'SEIB Insurance Brokers - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'gwhite@seib.co.uk',
        1 => 'jdemosthenous@seib.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$_companyDetails["filter_details"][]["postcode_sk"]["ACC;LIST;;Accept the below list of values."]  = array ("AB10", "AB11", "AB12", "AB13", "AB15", "AB16", "AB21", "AB22", "AB23", "AB24", "AB25", "AB30", "AB31", "AB32", "AB33", "AB34", "AB35", "AB36", "AB37", "AB38", "AB39", "AB41", "AB42", "AB43", "AB44", "AB45", "AB51", "AB52", "AB53", "AB54", "AB55", "AB56", "AB99", "B14", "BA22", "BD24", "BH11", "BH16", "BH17", "BH18", "BH19", "BH20", "BH21", "BH22", "BH23", "BH24", "BH25", "BH31", "BS20", "BS21", "BS22", "BS23", "BS24", "BS25", "BS26", "BS27", "BS28", "BS29", "CA10", "CA12", "CA13", "CA16", "CA17", "CA18", "CA19", "CA20", "CA21", "CA23", "CA3", "CA4", "CA5", "CA6", "CA7", "CA8", "CA9", "DD10", "DD11", "DD2", "DD5", "DD6", "DD7", "DD8", "DD9", "DG1", "DG10", "DG11", "DG12", "DG13", "DG14", "DG16", "DG2", "DG3", "DG4", "DG5", "DG6", "DG7", "DG8", "DG9", "DL10", "DL11", "DL12", "DL13", "DL2", "DL6", "DL7", "DL8", "DL9", "DT1", "DT10", "DT11", "DT2", "DT3", "DT5", "DT6", "DT7", "DT8", "DT9", "EH12", "EH15", "EH18", "EH19", "EH20", "EH22", "EH24", "EH25", "EH27", "EH28", "EH30", "EH32", "EH33", "EH34", "EH35", "EH36", "EH37", "EH38", "EH39", "EH4", "EH40", "EH41", "EH43", "EH44", "EH45", "EH46", "EH5", "EH51", "EH54", "EH55", "EH9", "EH99", "EX10", "EX12", "EX13", "EX14", "EX15", "EX16", "EX17", "EX18", "EX19", "EX20", "EX21", "EX22", "EX23", "EX24", "EX31", "EX32", "EX33", "EX34", "EX35", "EX36", "EX37", "EX38", "EX39", "EX5", "EX6", "FK10", "FK11", "FK12", "FK13", "FK14", "FK15", "FK16", "FK17", "FK18", "FK19", "FK20", "FK21", "FK3", "FK7", "FK8", "FK9", "G76", "G84", "GL13", "GL18", "GL19", "GL52", "GL53", "GL55", "GL56", "GL7", "GL9", "GY1", "GY2", "GY3", "GY4", "GY5", "GY6", "GY7", "GY8", "GY9", "HG3", "HR1", "HR2", "HR3", "HR4", "HR5", "HR6", "HR7", "HR8", "HR9", "HS1", "HS2", "HS3", "HS4", "HS5", "HS6", "HS7", "HS8", "HS9", "HU20", "IM1", "IM2", "IM3", "IM4", "IM5", "IM6", "IM7", "IM8", "IM9", "IM99", "IV1", "IV10", "IV11", "IV12", "IV13", "IV14", "IV15", "IV16", "IV17", "IV18", "IV19", "IV2", "IV20", "IV23", "IV27", "IV28", "IV3", "IV30", "IV31", "IV32", "IV36", "IV4", "IV40", "IV41", "IV42", "IV43", "IV44", "IV45", "IV46", "IV47", "IV48", "IV49", "IV5", "IV51", "IV52", "IV53", "IV54", "IV55", "IV56", "IV63", "IV8", "IV9", "JE1", "JE2", "JE3", "JE4", "KA1", "KA10", "KA11", "KA12", "KA13", "KA14", "KA15", "KA16", "KA2", "KA20", "KA21", "KA22", "KA23", "KA24", "KA25", "KA26", "KA27", "KA28", "KA29", "KA4", "KA5", "KA6", "KA7", "KA8", "KA9", "KW1", "KW10", "KW12", "KW13", "KW14", "KW15", "KW16", "KW17", "KW2", "KW3", "KW5", "KW6", "KW7", "KW8", "KW9", "KY10", "KY11", "KY12", "KY14", "KY15", "KY16", "KY2", "KY4", "KY5", "KY6", "KY7", "KY8", "KY9", "KY99", "LA10", "LA17", "LA19", "LA20", "LA21", "LA22", "LD1", "LD2", "LD3", "LD4", "LD5", "LD6", "LD7", "LD8", "LL11", "LL12", "LL14", "LL15", "LL16", "LL17", "LL20", "LL21", "LL22", "LL23", "LL24", "LL25", "LL26", "LL27", "LL28", "LL29", "LL30", "LL31", "LL32", "LL33", "LL34", "LL35", "LL36", "LL37", "LL38", "LL39", "LL40", "LL41", "LL42", "LL43", "LL44", "LL45", "LL46", "LL47", "LL48", "LL49", "LL51", "LL52", "LL53", "LL54", "LL55", "LL56", "LL57", "LL58", "LL59", "LL60", "LL61", "LL62", "LL63", "LL64", "LL65", "LL66", "LL67", "LL68", "LL69", "LL70", "LL71", "LL72", "LL73", "LL74", "LL75", "LL76", "LL77", "LL78", "LN10", "LN11", "LN12", "LN13", "LN2", "LN3", "LN4", "LN5", "LN6", "LN7", "LN8", "LN9", "NE19", "NE47", "NE48", "NE49", "NE61", "NE65", "NE66", "NE67", "NE68", "NE69", "NE70", "NE71", "NP15", "NP16", "NP18", "NP25", "NP26", "NP7", "NR22", "NR32", "NR33", "NR34", "NR35", "PA12", "PA13", "PA16", "PA17", "PA18", "PA19", "PA20", "PA21", "PA22", "PA23", "PA24", "PA25", "PA26", "PA27", "PA29", "PA30", "PA31", "PA32", "PA33", "PA34", "PA35", "PA36", "PA38", "PA41", "PA42", "PA43", "PA44", "PA45", "PA46", "PA47", "PA48", "PA49", "PA60", "PA61", "PA62", "PA63", "PA64", "PA65", "PA66", "PA67", "PA68", "PA69", "PA70", "PA71", "PA72", "PA73", "PA74", "PA75", "PA76", "PA77", "PA78", "PE22", "PE24", "PE35", "PH1", "PH10", "PH12", "PH13", "PH14", "PH15", "PH16", "PH17", "PH18", "PH19", "PH2", "PH20", "PH21", "PH22", "PH23", "PH24", "PH25", "PH26", "PH3", "PH30", "PH31", "PH32", "PH33", "PH34", "PH35", "PH36", "PH37", "PH38", "PH39", "PH4", "PH40", "PH41", "PH42", "PH43", "PH44", "PH49", "PH5", "PH50", "PH6", "PH7", "PH8", "PH9", "PL10", "PL11", "PL12", "PL13", "PL14", "PL15", "PL16", "PL17", "PL18", "PL20", "PL21", "PL22", "PL23", "PL24", "PL25", "PL26", "PL27", "PL28", "PL29", "PL30", "PL31", "PL32", "PL33", "PL34", "PL35", "PL7", "PL8", "PL9", "PO10", "PO11", "PO18", "PO19", "PO20", "PO22", "PO30", "PO31", "PO32", "PO33", "PO34", "PO35", "PO36", "PO37", "PO38", "PO39", "PO40", "PO41", "SA17", "SA19", "SA20", "SA31", "SA32", "SA33", "SA34", "SA35", "SA36", "SA37", "SA38", "SA39", "SA41", "SA42", "SA43", "SA44", "SA45", "SA46", "SA47", "SA48", "SA61", "SA62", "SA63", "SA64", "SA65", "SA66", "SA67", "SA68", "SA69", "SN12", "SN13", "SN14", "SN15", "SN16", "SN25", "SP3", "SP8", "SY10", "SY12", "SY13", "SY15", "SY16", "SY17", "SY18", "SY19", "SY20", "SY21", "SY22", "SY23", "SY24", "SY25", "SY5", "SY6", "SY7", "SY8", "SY9", "SY99", "TA1", "TA10", "TA11", "TA13", "TA16", "TA17", "TA18", "TA19", "TA2", "TA20", "TA21", "TA22", "TA23", "TA24", "TA3", "TA4", "TA5", "TA6", "TA7", "TA8", "TA9", "TD1", "TD10", "TD11", "TD12", "TD13", "TD14", "TD15", "TD2", "TD3", "TD4", "TD5", "TD6", "TD7", "TD8", "TD9", "TQ10", "TQ6", "TQ7", "TQ8", "TR1", "TR10", "TR11", "TR12", "TR13", "TR15", "TR16", "TR18", "TR2", "TR20", "TR21", "TR23", "TR24", "TR25", "TR26", "TR27", "TR4", "TR5", "TR6", "TR8", "TR9", "WR10", "WR11", "WR13", "WR14", "WR15", "WR2", "WR3", "WR4", "WR6", "WR8", "YO1", "YO10", "YO11", "YO12", "YO13", "YO14", "YO15", "YO16", "YO17", "YO18", "YO19", "YO21", "YO22", "YO23", "YO24", "YO25", "YO26", "YO30", "YO31", "YO32", "YO41", "YO42", "YO43", "YO51", "YO60", "YO61", "YO62", "YO7", "ZE1", "ZE2", "ZE3",);',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'claims_5_years' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'No',
        ),
      ),
    ),
    1 => 
    array (
      'convictions_5_years' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'No',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
        ),
      ),
    ),
    3 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
          5 => '9',
          6 => '10',
        ),
      ),
    ),
    5 => 
    array (
      'year_of_manufacture' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1901',
          1 => '1902',
          2 => '1903',
          3 => '1904',
          4 => '1905',
          5 => '1906',
          6 => '1907',
          7 => '1908',
          8 => '1909',
          9 => '1910',
          10 => '1911',
          11 => '1912',
          12 => '1913',
          13 => '1914',
          14 => '1915',
          15 => '1916',
          16 => '1917',
          17 => '1918',
          18 => '1919',
          19 => '1920',
          20 => '1921',
          21 => '1922',
          22 => '1923',
          23 => '1924',
          24 => '1925',
          25 => '1926',
          26 => '1927',
          27 => '1928',
          28 => '1929',
          29 => '1930',
          30 => '1931',
          31 => '1932',
          32 => '1933',
          33 => '1934',
          34 => '1935',
          35 => '1936',
          36 => '1937',
          37 => '1938',
          38 => '1939',
          39 => '1940',
          40 => '1941',
          41 => '1942',
          42 => '1943',
          43 => '1944',
          44 => '1945',
          45 => '1946',
          46 => '1947',
          47 => '1948',
          48 => '1949',
          49 => '1950',
          50 => '1951',
          51 => '1952',
          52 => '1953',
          53 => '1954',
          54 => '1955',
          55 => '1956',
          56 => '1957',
          57 => '1958',
          58 => '1959',
          59 => '1960',
          60 => '1961',
          61 => '1962',
          62 => '1963',
          63 => '1964',
          64 => '1965',
          65 => '1966',
          66 => '1967',
          67 => '1968',
          68 => '1969',
          69 => '1970',
          70 => '1971',
          71 => '1972',
          72 => '1973',
          73 => '1974',
          74 => '1975',
          75 => '1976',
          76 => '1977',
          77 => '1978',
          78 => '1979',
          79 => '1980',
          80 => '1981',
          81 => '1982',
          82 => '1983',
          83 => '1984',
          84 => '1985',
          85 => '1986',
          86 => '1987',
          87 => '1988',
          88 => '1989',
          89 => '1990',
          90 => '1991',
          91 => '1992',
          92 => '1993',
          93 => '1994',
          94 => '1995',
          95 => '1996',
          96 => '1997',
          97 => '1998',
          98 => '1999',
          99 => '2000',
          100 => '2001',
          101 => '2002',
          102 => '2003',
          103 => '2004',
          104 => '2005',
          105 => '2006',
        ),
      ),
    ),
    6 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 70' => '25-70',
      ),
    ),
    7 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
          3 => '10',
          4 => '11',
          5 => '12',
          6 => '13',
          7 => '14',
          8 => '15',
          9 => '16',
        ),
      ),
    ),
    8 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
        ),
      ),
    ),
    9 => 
    array (
      'taxi_type' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
    10 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
          3 => '10',
          4 => '11',
          5 => '12',
        ),
      ),
    ),
    11 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '8',
          1 => '9',
          2 => '10',
          3 => '11',
          4 => '12',
          5 => '13',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12382',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '902',
    'companyName' => 'JMBInsurance',
    'offline_from_plugin' => false,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '1',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'joe@jmbinsurance.co.uk',
        1 => 'john@jmbinsurance.co.uk',
      ),
    ),
    'TXT' => 
    array (
      'COMPANY' => 
      array (
        0 => 'inboundlead@slgm.co.uk',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'bespoke_filters' => 
  array (
    0 => '$platingAuthority=strtoupper($_SESSION["_YourDetails_"]["plating_authority"]); if($platingAuthority =="LONDON PCO") $_companyDetails["filter_details"][]["postcode_sk"]["ACC;LIST;;Accept IF Plating Authority = London PCO and postcode = TN, RH, BN."]  = array ("TN[0-99]*", "RH[0-99]*","BN[0-99]*"); ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 69' => ' 25 - 69',
      ),
    ),
    1 => 
    array (
      'taxi_used_for' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
        ),
      ),
    ),
    2 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ADUR',
          1 => 'BRIGHTON & HOVE',
          2 => 'CRAWLEY',
          3 => 'EASTBOURNE',
          4 => 'HASTINGS',
          5 => 'HORSHAM',
          6 => 'LEWES',
          7 => 'MAIDSTONE',
          8 => 'MID SUSSEX',
          9 => 'ROTHER',
          10 => 'SEVENOAKS',
          11 => 'TANDRIDGE',
          12 => 'TONBRIDGE & MALLING',
          13 => 'TUNBRIDGE WELLS',
          14 => 'WEALDEN',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10080',
    'siteOrigID' => '',
    'brokerID' => '33',
    'logoID' => '91',
    'companyName' => 'One Answer - QZ034 14',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'One Answer - QZ034 14',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'QZ034_lead@1answer.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-18:30',
          2 => '09:00-18:30',
          3 => '09:00-18:30',
          4 => '09:00-18:30',
          5 => '09:00-18:30',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 2100 and 100000' => '2100 - 100000',
      ),
    ),
    4 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'LONDON PCO',
        ),
      ),
    ),
    6 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    8 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 65' => ' 25 - 65',
      ),
    ),
    9 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BL[0-99]*',
          4 => 'BT[0-99]*',
          5 => 'CW[0-99]*',
          6 => 'DY[0-99]*',
          7 => 'E[0-99]*',
          8 => 'EC[0-99]*',
          9 => 'G[0-99]*',
          10 => 'HD[0-99]*',
          11 => 'HG[0-99]*',
          12 => 'HX[0-99]*',
          13 => 'L[0-99]*',
          14 => 'LE[0-99]*',
          15 => 'LS[0-99]*',
          16 => 'M[0-99]*',
          17 => 'N[0-99]*',
          18 => 'NE[0-99]*',
          19 => 'NW[0-99]*',
          20 => 'OL[0-99]*',
          21 => 'S[0-99]*',
          22 => 'SE[0-99]*',
          23 => 'SW[0-99]*',
          24 => 'TS[0-99]*',
          25 => 'W[0-99]*',
          26 => 'WF[0-99]*',
          27 => 'WS[0-99]*',
          28 => 'WV[0-99]*',
          29 => 'IM[0-99]*',
        ),
      ),
    ),
    10 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
  ),
)


 ?>
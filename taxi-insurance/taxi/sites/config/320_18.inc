<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '320',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '1041',
    'companyName' => 'Test Fake',
    'offline_from_plugin' => false,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => true,
    'limit' => 
    array (
      '3hours' => '2',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
        1 => 'qa@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
    ),
    'TXT' => 
    array (
      'TECHNICAL' => 
      array (
        0 => 'qa@seopa.com',
        1 => 'eb2-technical@seopa.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'ABERDEEN CENTRAL (INVERURIE)',
        ),
      ),
    ),
    1 => 
    array (
      'uber_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Y',
        ),
      ),
    ),
    2 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'AB22',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10143',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '642',
    'companyName' => 'Jenkinson Insurance - Taxi (Black Cab)',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Craig@jenkinsoninsurance.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    1 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BIRMINGHAM',
          1 => 'BRIGHTON & HOVE',
          2 => 'BRISTOL CITY OF UA',
          3 => 'CARDIFF',
          4 => 'COVENTRY',
          5 => 'DERBY',
          6 => 'KINGSTON-UPON-HULL',
          7 => 'LANCASTER',
          8 => 'LEICESTER',
          9 => 'NEWCASTLE UPON TYNE',
          10 => 'NEWPORT',
          11 => 'NORWICH',
          12 => 'NOTTINGHAM',
          13 => 'OXFORD',
          14 => 'PETERBOROUGH',
          15 => 'PLYMOUTH',
          16 => 'PRESTON',
          17 => 'SHEFFIELD',
          18 => 'SOUTHAMPTON',
          19 => 'ST ALBANS',
          20 => 'STOKE ON TRENT',
          21 => 'SUNDERLAND',
          22 => 'SWANSEA',
          23 => 'WAKEFIELD',
          24 => 'YORK',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_type' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 8' => '8',
      ),
    ),
  ),
)


 ?>
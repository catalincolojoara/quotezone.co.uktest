<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '2696',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '899',
    'companyName' => 'Alternative Insurance Brokers',
    'offline_from_plugin' => true,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'leads@alternativeinsurancebrokers.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-06:00|09:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-23:59',
          6 => 'SKP_ALL_DAY',
          7 => '20:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 21' => '0-21',
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 22 and 75' => '22 - 75',
      ),
    ),
    5 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 10' => '10',
      ),
    ),
    6 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 2000 and 100000' => '2000 - 100000',
      ),
    ),
    7 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
        ),
      ),
    ),
    8 => 
    array (
      'claims_5_years' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    9 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    10 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
        ),
      ),
    ),
    11 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[20-27[',
          1 => 'BT[0-99]*',
          2 => 'E[0-99]*',
          3 => 'EC[0-99]*',
          4 => 'N[0-99]*',
          5 => 'NW[0-99]*',
          6 => 'SE[0-99]*',
          7 => 'SW[0-99]*',
          8 => 'UB[1-6]',
          9 => 'W[0-99]*',
          10 => 'WC[0-99]*',
        ),
      ),
    ),
    12 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'LONDON PCO',
          1 => 'ROSSENDALE',
          2 => 'WOLVERHAMPTON',
        ),
      ),
    ),
  ),
)


 ?>
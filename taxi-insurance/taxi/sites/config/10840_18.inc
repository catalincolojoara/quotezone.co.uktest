<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10840',
    'siteOrigID' => '',
    'brokerID' => '33',
    'logoID' => '1030',
    'companyName' => 'Riviera Insurance',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'leads@rivierainsurance.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$postcodeFlag = ""; $platingFlag  = ""; $postCode     = $_SESSION["_YourDetails_"]["postcode"]; preg_match("/^(.*)\\d/isU",$postCode,$postCodeArr); $postcodeToCheck = $postCodeArr[1]; $acceptedPcodesArray = array("SA", "CF", "NP", "GL", "OX", "TR", "PL", "EX", "TQ", "TA", "BS", "BA", "DT", "SN", "SP", "BH", "SO", "PO", ); if(!in_array($postcodeToCheck,$acceptedPcodesArray)) $postcodeFlag = "skip"; $platingAuthority = strtoupper($_SESSION["_YourDetails_"]["plating_authority"]); $acceptedPlatingAuthiritiesArray = array("BATH & NORTH EAST SOMERSET", "BLAENAU GWENT", "BOURNEMOUTH", "BRISTOL", "BRISTOL CITY OF UA", "CAERPHILLY", "CARADON", "CARDIFF", "CARMARTHENSHIRE", "CARRICK", "CEREDIGION", "CHELTENHAM", "CHERWELL", "CHRISTCHURCH", "CORNWALL COUNCIL", "COTSWOLD", "EAST DEVON", "EAST DORSET", "EXETER", "FOREST OF DEAN", "GLOUCESTER", "KERRIER", "MENDIP", "MERTHYR TYDFIL", "MID DEVON", "MONMOUTHSHIRE", "NEATH & PORT TALBOT", "NEWPORT", "NORTH CORNWALL", "NORTH DEVON", "NORTH DORSET", "NORTH SOMERSET", "NORTH WILTSHIRE", "OXFORD", "PEMBROKESHIRE", "PENWITH", "PLYMOUTH", "POOLE", "PURBECK", "RHONDDA CYNON TAFF", "SEDGEMOOR", "SOUTH GLOUCESTER", "SOUTH HAMS", "SOUTH OXFORDSHIRE", "SOUTH SOMERSET", "STROUD", "SWANSEA", "SWINDON", "TAUNTON DEANE", "TEIGNBRIDGE", "TEST VALLEY", "TEWKESBURY", "TORBAY", "TORFAEN", "TORRIDGE", "VALE OF GLAMORGAN", "VALE OF WHITE HORSE", "WEST DEVON", "WEST DORSET", "WEST SOMERSET", "WEYMOUTH & PORTLAND", "WILTSHIRE COUNTY",); if(!in_array($platingAuthority,$acceptedPlatingAuthiritiesArray)) $platingFlag = "skip"; if($postcodeFlag == "skip" && $platingFlag == "skip") $_companyDetails["filter_details"][]["BSPK_ACC_PCODE_AUTH"]["ACC;VAL;EQL;Accept one of these postcodes OR Plating Authority ."]  = "ACC"; ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '04:00-23:59',
          2 => '04:00-23:59',
          3 => '04:00-23:59',
          4 => '04:00-23:59',
          5 => '04:00-23:59',
          6 => '04:00-23:59',
          7 => '04:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '513',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '653',
    'companyName' => 'DNA - Taxi (Public Hire)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'DNA - Taxi (Public Hire)',
    'limit' => 
    array (
      'hour' => '1',
      'day' => '5',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Taxileads@dna-insurance.com',
      ),
    ),
    'TXT' => 
    array (
      'COMPANY' => 
      array (
        0 => 'leads@inbound.msrvr.net',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
        1 => 'leads@seopa.com',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$_companyDetails["filter_details"][]["WS_ID"]["SKP;LIST;;SKIP if webservice source = Danny Imray. "] = array(           0 => "5fdbf8c1a05cd4cdf82ed72104896c7e",       1 => "785e5997d6ca27649d56af14c5d31141",       );',
    1 => '$taxiNcb=$_SESSION["_YourDetails_"]["taxi_ncb"]; $privateCarNcb=$_SESSION["_YourDetails_"]["private_car_ncb"]; if($taxiNcb == "0" && $privateCarNcb < "3") $_companyDetails["filter_details"][]["BSPK_SKP_NCB_PNCB"]["ACC;VAL;EQL;If Taxi no claims bonus = No NCB skip if Private car no claims bonus < 3 Years. "] = "BSPK_SKP"; ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-18:00',
          2 => '09:00-18:00',
          3 => '09:00-18:00',
          4 => '09:00-18:00',
          5 => '09:00-18:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BELFAST',
          1 => 'BRADFORD',
        ),
      ),
    ),
    3 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    4 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
    5 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 65' => '30-65',
      ),
    ),
    6 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
        ),
      ),
    ),
    7 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BD[0-99]*',
          1 => 'BT[0-99]*',
        ),
      ),
    ),
    8 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 21' => '0-21',
      ),
    ),
    9 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
  ),
)


 ?>
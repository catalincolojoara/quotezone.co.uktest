<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '1888',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '57',
    'companyName' => 'Insurance Choice 2 - Taxi ',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '7',
      'month' => '193',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxiquotes@insurancechoice.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
    'url' => '1',
    'csv' => '1',
  ),
  'bespoke_filters' => 
  array (
    0 => '     $taxiNCB       = $_SESSION["_YourDetails_"]["taxi_ncb"];     if($taxiNCB == 0)     {       $_companyDetails["filter_details"][]["private_car_ncb"]["SKP;LIST;;If Taxi no claims bonus = No NCB SKIP if Private car no claims bonus = No NCB,  1 Year, 2 Years"] = array(         0=>"0",         1=>"1",         2=>"2",       );     }',
    1 => '$_companyDetails["filter_details"][]["WS_ID"]["SKP;LIST;;SKIP if webservice source = Danny Imray. "] = array(           0 => "5fdbf8c1a05cd4cdf82ed72104896c7e", 			 1 => "785e5997d6ca27649d56af14c5d31141",       );',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
        ),
      ),
    ),
    1 => 
    array (
      'DIS' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 14' => '14',
      ),
    ),
    2 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'SKP;VAL;MIN;Skip if minimum value is 15' => '15',
      ),
    ),
    3 => 
    array (
      'taxi_used_for' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BH[0-99]*',
          1 => 'BS[0-99]*',
          2 => 'CF[0-99]*',
          3 => 'CV[0-99]*',
          4 => 'DY[0-99]*',
          5 => 'GL[0-99]*',
          6 => 'GU[0-99]*',
          7 => 'HP[0-99]*',
          8 => 'LE[0-99]*',
          9 => 'MK[0-99]*',
          10 => 'NG[0-99]*',
          11 => 'NN[0-99]*',
          12 => 'NP[0-99]*',
          13 => 'OX[0-99]*',
          14 => 'RG[0-99]*',
          15 => 'SG[0-99]*',
          16 => 'SL[0-99]*',
          17 => 'SN[0-99]*',
          18 => 'ST[0-99]*',
          19 => 'TF[0-99]*',
          20 => 'WD[0-99]*',
          21 => 'WR[0-99]*',
          22 => 'WS[0-99]*',
          23 => 'WV[0-99]*',
        ),
      ),
    ),
    5 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 100' => '30 - 100',
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '9626',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '589',
    'companyName' => 'GoSkippy',
    'offline_from_plugin' => false,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'commercialleads@goskippy.com',
        1 => 'paul.casson@rock-services.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-23:59',
          6 => '00:00-12:30',
          7 => '16:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
          3 => '10',
          4 => '11',
          5 => '12',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BIRMINGHAM',
          1 => 'BLACKBURN WITH DARWEN',
          2 => 'BLACKPOOL',
          3 => 'BRADFORD',
          4 => 'BURNLEY',
          5 => 'ELLESEMERE PORT & NESTON',
          6 => 'GATESHEAD',
          7 => 'HYNDBURN',
          8 => 'KNOWSLEY',
          9 => 'LEEDS',
          10 => 'LIVERPOOL',
          11 => 'LONDON PCO',
          12 => 'LUTON',
          13 => 'MANCHESTER',
          14 => 'MERTON',
          15 => 'MIDDLESBOROUGH',
          16 => 'NEWCASTLE UPON TYNE',
          17 => 'NORTH TYNESIDE',
          18 => 'NOTTINGHAM',
          19 => 'PRESTON',
          20 => 'REDCAR & CLEVELAND',
          21 => 'ROCHDALE',
          22 => 'RUSHCLIFFE',
          23 => 'SALFORD',
          24 => 'SANDWELL',
          25 => 'SEFTON',
          26 => 'SHEFFIELD',
          27 => 'SOUTH EAST & METROPOLITAN',
          28 => 'SOUTH RIBBLE',
          29 => 'SOUTH TYNESIDE',
          30 => 'SUNDERLAND',
          31 => 'TAMESIDE',
          32 => 'TRAFFORD',
          33 => 'WAKEFIELD',
          34 => 'WALSALL',
          35 => 'WATFORD',
          36 => 'WIRRALL',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_type' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '10',
          1 => '11',
          2 => '12',
          3 => '13',
          4 => '14',
          5 => '15',
          6 => '16',
        ),
      ),
    ),
    8 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 21' => '0 - 21',
      ),
    ),
    9 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 35 and 100' => '35-100',
      ),
    ),
  ),
)


 ?>
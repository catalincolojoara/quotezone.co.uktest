<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '361',
    'siteOrigID' => '',
    'brokerID' => '96',
    'logoID' => '497',
    'companyName' => 'Insurance Factory (Marker Study) - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'mon' => '14',
      'tue' => '14',
      'wed' => '14',
      'thu' => '14',
      'fri' => '14',
      'sat' => '8',
      'sun' => '8',
    ),
    'emailSubject' => 'Insurance Factory (Marker Study) - Taxi',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'shopquotes@insurancefactory.co.uk',
        1 => 'callcampaign@insurancefactory.co.uk',
        2 => 'LeadGen@insurancefactory.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '4',
        ),
      ),
    ),
    1 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    2 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 3000 and 40000' => '3000 - 40000',
      ),
    ),
    3 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 27 and 67' => '27-67',
      ),
    ),
    4 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
          5 => '9',
          6 => '10',
          7 => '11',
          8 => '12',
        ),
      ),
    ),
    5 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'AB[0-99]*',
          1 => 'AL[0-99]*',
          2 => 'B[28-99]',
          3 => 'BS[0-99]*',
          4 => 'CB[0-99]*',
          5 => 'CV[0-99]*',
          6 => 'DY[0-99]*',
          7 => 'EH[0-99]*',
          8 => 'G[0-99]*',
          9 => 'GL[0-99]*',
          10 => 'GU[0-99]*',
          11 => 'LE[0-99]*',
          12 => 'MK[0-99]*',
          13 => 'NG[0-99]*',
          14 => 'NN[0-99]*',
          15 => 'OX[0-99]*',
          16 => 'RG[0-99]*',
          17 => 'SN[0-99]*',
          18 => 'SS[0-99]*',
          19 => 'WR[0-99]*',
          20 => 'WS[0-99]*',
          21 => 'WV[0-99]*',
        ),
      ),
    ),
    6 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 2 and 21' => '2 - 21',
      ),
    ),
  ),
)


 ?>
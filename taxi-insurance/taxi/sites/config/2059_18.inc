<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '2059',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '19',
    'companyName' => 'Broadsure Direct - Taxi 2',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
    ),
    'emailSubject' => 'Broadsure Direct - Taxi 2',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'sales@broadsuredirect.com',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-18:00',
          2 => '09:00-18:00',
          3 => '09:00-18:00',
          4 => '09:00-18:00',
          5 => '09:00-18:00',
          6 => '09:00-13:00',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
        ),
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 69' => '25-69',
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ADUR',
          1 => 'ARUN',
          2 => 'ASHFORD',
          3 => 'BRIGHTON & HOVE',
          4 => 'CANTERBURY',
          5 => 'CHICHESTER',
          6 => 'CRAWLEY',
          7 => 'DARTFORD',
          8 => 'DOVER',
          9 => 'EASTBOURNE',
          10 => 'GILLINGHAM',
          11 => 'GRAVESHAM',
          12 => 'GUILDFORD',
          13 => 'HASTINGS',
          14 => 'HORSHAM',
          15 => 'LEWES',
          16 => 'MAIDSTONE',
          17 => 'MID SUSSEX',
          18 => 'REIGATE AND BANSTEAD',
          19 => 'ROCHESTER',
          20 => 'ROTHER',
          21 => 'SEVENOAKS',
          22 => 'SHEPWAY',
          23 => 'SWALE',
          24 => 'TANDRIDGE',
          25 => 'THANET',
          26 => 'TONBRIDGE & MALLING',
          27 => 'TUNBRIDGE WELLS',
          28 => 'WEALDEN',
          29 => 'WORTHING',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '847',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '942',
    'companyName' => 'Laurie Ross - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'shettleston@laurieross.com',
        1 => 'Gregg@laurieross.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'DG7',
          1 => 'EH46',
          2 => 'EH55',
          3 => 'FK[6-9]*',
          4 => 'FK[15-20]*',
          5 => 'G[0-99]*',
          6 => 'KA[1-6]*',
          7 => 'KA[16-19]*',
          8 => 'ML[1-12]*',
          9 => 'PA[0-99]*',
          10 => 'PA1[3-14]*',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '547',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '20',
    'companyName' => 'Coversure Huntingdon (Chessington)',
    'offline_from_plugin' => true,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'worcesterpark@coversure.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$taxiNcb=$_SESSION["_YourDetails_"]["taxi_ncb"]; if($taxiNcb == "0") $_companyDetails["filter_details"][]["postcode_sk"]["SKP;LIST;;IF Postcode = SL[0-99] then SKIP if Taxi No Claims Bonus = No NCB."]  = array ("SL[0-99]*",);',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-23:59',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 28 and 100' => '28-100',
      ),
    ),
    3 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
          5 => '8',
          6 => '9',
        ),
      ),
    ),
    5 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'GU[25]',
          1 => 'GU25',
          2 => 'HP[9-12]',
          3 => 'HP[9-12]',
          4 => 'HP[13]',
          5 => 'HP13',
          6 => 'HP[15]',
          7 => 'HP15',
          8 => 'KT[1-21]',
          9 => 'KT[1-21]',
          10 => 'RG[5]',
          11 => 'RG5',
          12 => 'RG[12]',
          13 => 'RG12',
          14 => 'RG[42]',
          15 => 'RG42',
          16 => 'SE[1]',
          17 => 'SE1',
          18 => 'SE[11]',
          19 => 'SE11',
          20 => 'SE[17]',
          21 => 'SE17',
          22 => 'SE[5]',
          23 => 'SE5',
          24 => 'SL[0-9]',
          25 => 'SL[0-9]',
          26 => 'SM[1-7]',
          27 => 'SM[1-7]',
          28 => 'SW[1]',
          29 => 'SW1',
          30 => 'SW[3]',
          31 => 'SW3',
          32 => 'SW[5]',
          33 => 'SW5',
          34 => 'SW[7]',
          35 => 'SW7',
          36 => 'SW[8]',
          37 => 'SW8',
          38 => 'SW[10]',
          39 => 'SW10',
          40 => 'SW[13]',
          41 => 'SW13',
          42 => 'SW[14]',
          43 => 'SW14',
          44 => 'SW[15]',
          45 => 'SW15',
          46 => 'SW[20]',
          47 => 'SW20',
          48 => 'SW[6]',
          49 => 'SW6',
          50 => 'TW[1-20]',
          51 => 'TW[1-20]',
          52 => 'W[2-14]',
          53 => 'W[2-14]',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_used_for' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
    7 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'SPELTHORNE',
          1 => 'WINDSOR & MAIDENHEAD',
          2 => 'WOKINGHAM',
        ),
      ),
    ),
    8 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
        ),
      ),
    ),
    9 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
  ),
)


 ?>
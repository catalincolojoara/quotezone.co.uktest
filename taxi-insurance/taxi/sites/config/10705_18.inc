<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10705',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => 758,
    'companyName' => 'Rowett Insurance',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Glyn@rowett-insurance.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 100' => '25-100',
      ),
    ),
    1 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'PL[0-99]*',
          1 => 'TR[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
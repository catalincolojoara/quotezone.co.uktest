<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10908',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '212',
    'companyName' => 'First Insurance Services - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '5',
    ),
    'emailSubject' => 'First Insurance Services - Taxi',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Anthony.phillips@first.co.uk',
        1 => 'Craig.hancock@first.co.uk',
        2 => 'martin.frangleton@first.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
          3 => '10',
          4 => '11',
          5 => '12',
          6 => '13',
          7 => '14',
          8 => '15',
          9 => '16',
        ),
      ),
    ),
    3 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 69' => '25-69',
      ),
    ),
    5 => 
    array (
      'registration_number_question' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'N',
        ),
      ),
    ),
    6 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'LONDON PCO',
        ),
      ),
    ),
    7 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 7' => '0 - 7',
      ),
    ),
    8 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    9 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BN[0-99]*',
          1 => 'GU[0-99]*',
          2 => 'HP[0-99]*',
          3 => 'PO[0-99]*',
          4 => 'RG[0-99]*',
          5 => 'RH[0-99]*',
          6 => 'SL[0-99]*',
          7 => 'SP[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
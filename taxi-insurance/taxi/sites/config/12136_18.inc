<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12136',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => 948,
    'companyName' => 'Collingwood Insurance',
    'offline_from_plugin' => false,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '10',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
        1 => 'michael.kirk@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'TopxTaxi@Collingwood.co.uk',
        1 => 'helen.shields@collingwood.co.uk',
        2 => 'paul.vickers@collingwood.co.uk',
        3 => 'Gillian.Tompkins@Collingwood.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$_companyDetails["filter_details"][]["engine_size"]["SKP;LIST;;Skip electric vehicles:  0, 0 , 0 cc, 0 CC, 0 E, 0 e."]  = array (0 => "0", 1 => "0 ", 2 => "0 cc", 3 => "0 CC", 4 => "0 E", 5 => "0 e", ); ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-18:30',
          2 => '09:00-18:30',
          3 => '09:00-18:30',
          4 => '09:00-18:30',
          5 => '09:00-18:30',
          6 => '09:00-13:00',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 24 and 28' => '24 - 28',
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
          5 => '9',
          6 => '10',
          7 => '11',
          8 => '12',
          9 => '13',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
          5 => '8',
          6 => '9',
          7 => '10',
        ),
      ),
    ),
  ),
)


 ?>
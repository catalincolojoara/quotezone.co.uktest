<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '1510',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '19',
    'companyName' => 'Broadsure Direct 1',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '5',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'sales@broadsuredirect.com',
        1 => 'info@broadsuredirect.com',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-18:00',
          2 => '09:00-18:00',
          3 => '09:00-18:00',
          4 => '09:00-18:00',
          5 => '09:00-18:00',
          6 => '09:00-12:30',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 39' => '25 - 39',
      ),
    ),
    2 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ASHFORD',
          1 => 'CANTERBURY',
          2 => 'DOVER',
          3 => 'HASTINGS',
          4 => 'MAIDSTONE',
          5 => 'ROTHER',
          6 => 'SEVENOAKS',
          7 => 'SHEPWAY',
          8 => 'SWALE',
          9 => 'THANET',
          10 => 'THANET BROADSTAIRS',
          11 => 'THANET DISTRICT',
          12 => 'THANET MARGATE',
          13 => 'THANET RAMSGATE',
          14 => 'TONBRIDGE & MALLING',
          15 => 'TUNBRIDGE WELLS',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12695',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '258',
    'companyName' => 'Onesure - Taxi (New Badge)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'mon' => '3',
      'tue' => '3',
      'wed' => '4',
      'thu' => '4',
      'fri' => '4',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxiquotes@onesureinsurance.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 69' => '30 - 69',
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'private_car_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
        ),
      ),
    ),
    4 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    5 => 
    array (
      'convictions_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    6 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0-14',
      ),
    ),
    7 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'WV[0-99]*',
        ),
      ),
    ),
    8 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BELFAST',
          1 => 'BIRMINGHAM',
          2 => 'BLACKBURN WITH DARWEN',
          3 => 'BOLTON',
          4 => 'BRADFORD',
          5 => 'BURNLEY',
          6 => 'BURY',
          7 => 'CALDERDALE',
          8 => 'DUDLEY',
          9 => 'DVA (NI)',
          10 => 'HYNDBURN',
          11 => 'KIRKLEES',
          12 => 'KNOWSLEY',
          13 => 'LEEDS',
          14 => 'LIVERPOOL',
          15 => 'LONDON PCO',
          16 => 'MANCHESTER',
          17 => 'NORTHERN IRELAND',
          18 => 'OLDHAM',
          19 => 'ROCHDALE',
          20 => 'ROSSENDALE',
          21 => 'SALFORD',
          22 => 'SANDWELL',
          23 => 'SEFTON',
          24 => 'SOLIHULL',
          25 => 'STOCKPORT',
          26 => 'TAMESIDE',
          27 => 'TRAFFORD',
          28 => 'WAKEFIELD',
          29 => 'WALSALL',
          30 => 'WIGAN',
          31 => 'WOLVERHAMPTON',
        ),
      ),
    ),
  ),
)


 ?>
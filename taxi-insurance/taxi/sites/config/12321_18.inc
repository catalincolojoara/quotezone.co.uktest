<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12321',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '960',
    'companyName' => 'Insurance Protector Group',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'hour' => '1',
      'day' => '5',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
        1 => 'michael.kirk@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'business@insuranceprotectorgroup.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '08:30-17:00',
          2 => '08:30-17:00',
          3 => '08:30-17:00',
          4 => '08:30-17:00',
          5 => '08:30-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
          6 => '15',
          7 => '16',
        ),
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '5',
        ),
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 74' => '25-74',
      ),
    ),
    5 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    6 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BLACKBURN WITH DARWEN',
          1 => 'BOLTON',
          2 => 'BRADFORD',
          3 => 'BURNLEY',
          4 => 'BURY',
          5 => 'CALDERDALE',
          6 => 'HARLOW',
          7 => 'HYNDBURN',
          8 => 'KIRKLEES',
          9 => 'KNOWSLEY',
          10 => 'LEEDS',
          11 => 'LIVERPOOL',
          12 => 'MANCHESTER',
          13 => 'OLDHAM',
          14 => 'ROCHDALE',
          15 => 'ROSSENDALE',
          16 => 'SALFORD',
          17 => 'SEFTON',
          18 => 'ST HELENS',
          19 => 'STOCKPORT',
          20 => 'TAMESIDE',
          21 => 'TRAFFORD',
          22 => 'WIGAN',
        ),
      ),
    ),
  ),
)


 ?>
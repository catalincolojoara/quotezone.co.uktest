<?php

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '2511',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '902',
    'companyName' => 'JMB Insurance Brokers',
    'offline_from_plugin' => true,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'week' => '10',
      'day' => '4',
    ),
    'emailSubject' => 'JMB Insurance ',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'joemanning@jmbinsurance.co.uk',
        1 => 'john@jmbinsurance.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$vehMake = $_SESSION["_YourDetails_"]["vehicle_make"];$vehModel = $_SESSION["_YourDetails_"]["vehicle_model"];$filterVehMakeModel = true;if($vehMake == "TY" && $vehModel == "PRIUS")$filterVehMakeModel = false;if($vehMake == "FO" && $vehModel == "GALAXY")$filterVehMakeModel = false;if($vehMake == "MC" && ($vehModel == "C" || $vehModel == "C200" || $vehModel == "C220" || $vehModel == "C250" || $vehModel == "C300" || $vehModel == "C350" ||  $vehModel == "C63"))$filterVehMakeModel = false;if($filterVehMakeModel)$_companyDetails["filter_details"][]["ACC_TY_PRIUS"]["ACC;VAL;EQL;ACCEPT Vehicle Make - Toyota AND Vehicle Model - Prius or Vehicle Make - Ford, Vehicle Model - Galaxy or Vehicle Make - Mercedes, Vehicle Model - C Class"] = "ACC_TY";',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 40 and 100' => ' 40 - 100',
      ),
    ),
    1 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'LONDON PCO',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '4',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
  ),
)


?>
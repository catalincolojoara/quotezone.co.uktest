<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '9576',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '295',
    'companyName' => 'Top Marques',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'info@topmarques.uk.com',
        1 => 'kurtis.mcdonald@topmarques.uk.com',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$_companyDetails["filter_details"][]["taxi_plating"]["ACC;LIST;;ACCEPT only attached Taxi Plating Authorities"] = array("EAST STAFFORDSHIRE","SOUTH STAFFORDSHIRE","TAMWORTH","STAFFORD","LICHFIELD",);',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-16:00',
          2 => '09:00-16:00',
          3 => '09:00-16:00',
          4 => '09:00-16:00',
          5 => '09:00-16:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
  ),
)


 ?>
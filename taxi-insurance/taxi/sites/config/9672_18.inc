<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '9672',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '721',
    'companyName' => 'Patons Scotland OOH',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '20',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxileads@patonsinsurance.co.uk',
        1 => 'taxi@patonsquotes.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|17:00-23:59',
          2 => '00:00-09:00|17:00-23:59',
          3 => '00:00-09:00|17:00-23:59',
          4 => '00:00-09:00|17:00-23:59',
          5 => '00:00-09:00|17:00-23:59',
          6 => '00:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_plating' => 
      array (
        'ACC;LIST;;Skip the below list of values.' => 
        array (
          0 => 'ABERDEEN',
          1 => 'ABERDEEN',
          2 => 'ABERDEENSHIRE',
          3 => 'ANGUS',
          4 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          5 => 'CLACKMANANSHIRE (ALLOA)',
          6 => 'DUMFRIES & GALLOWAY',
          7 => 'DUNDEE',
          8 => 'EAST AYRSHIRE KILMARNOCK',
          9 => 'EAST DUNBARTONSHIRE KIRKINTILLOCH',
          10 => 'EAST LOTHIAN HADDINGTON',
          11 => 'EAST RENFREWSHIRE GIFFNOCH',
          12 => 'EDINBURGH',
          13 => 'GLASGOW CITY',
          14 => 'INVERCLYDE GREENOCK',
          15 => 'FALKIRK',
          17 => 'FIFE COUNCIL (KIRKCALDY)',
          18 => 'INVERNESS HIGHLANDS',
          19 => 'MIDLOTHIAN COUNCIL',
          20 => 'MORAY (ELGIN)',
          21 => 'NORTH AYRSHIRE (IRVINE)',
          22 => 'NORTH LANARKSHIRE NORTH',
          23 => 'ORKNEY ISLANDS KIRKWALL',
          24 => 'PERTH & KINROSS',
          25 => 'RENFREWSHIRE PAISLEY',
          26 => 'SCOTTISH BORDERS',
          27 => 'SHETLAND ISLANDS LERWICK',
          28 => 'SOUTH AYRSHIRE AYR',
          29 => 'STIRLING',
          30 => 'WEST DUNBARTONSHIRE CLYDEBANK',
          31 => 'WEST LOTHIAN LIVINGSTON',
          32 => 'WESTERN ISLES STORNOWAY',
          33 => 'SOUTH LANARKSHIRE',
        ),
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
          5 => '8',
          6 => '9',
          7 => '10',
          8 => '11',
          9 => '12',
        ),
      ),
    ),
    3 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 23' => '0 - 23',
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 21 and 75' => '21 - 75',
      ),
    ),
  ),
)


 ?>
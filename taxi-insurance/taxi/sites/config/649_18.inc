<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '649',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '54',
    'companyName' => 'Quoteline Direct',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '75',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
    ),
    'TXT' => 
    array (
      'COMPANY' => 
      array (
        0 => 'email.leads@quotelinedirect.co.uk',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 100' => '25-100',
      ),
    ),
    2 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    3 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BD[0-99]*',
          2 => 'BR[0-99]*',
          3 => 'BT[0-99]*',
          4 => 'CR[0-99]*',
          5 => 'E[0-99]*',
          6 => 'EC[0-99]*',
          7 => 'EN[0-99]*',
          8 => 'HA[0-99]*',
          9 => 'IG[0-99]*',
          10 => 'KT[0-99]*',
          11 => 'LS[0-99]*',
          12 => 'N[0-99]*',
          13 => 'NE[0-99]*',
          14 => 'NW[0-99]*',
          15 => 'RM[0-99]*',
          16 => 'S[0-99]*',
          17 => 'SE[0-99]*',
          18 => 'SM[0-99]*',
          19 => 'SR[0-99]*',
          20 => 'SW[0-99]*',
          21 => 'TW[0-99]*',
          22 => 'UB[0-99]*',
          23 => 'W[0-99]*',
          24 => 'WC[0-99]*',
          25 => 'WD[0-99]*',
        ),
      ),
    ),
    4 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'ABERDEEN',
          1 => 'ABERDEEN CENTRAL (INVERURIE)',
          2 => 'ABERDEEN CITY (ABERDEEN)',
          3 => 'ABERDEENSHIRE',
          4 => 'ABERDEENSHIRE NORTH (BANFF)',
          5 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          6 => 'ANGUS',
          7 => 'ANGUS COUNCIL (FORFAR)',
          8 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          9 => 'BADENOCH & STRATHSPEY (HIGHLAND - KINGUSSIE)',
          10 => 'BELFAST',
          11 => 'BERWICK ON TWEED',
          12 => 'BIRMINGHAM',
          13 => 'CAITHNESS (HIGHLANDS - WICK)',
          14 => 'CLACKMANANSHIRE (ALLOA)',
          15 => 'DERBY',
          16 => 'DUMFRIES & GALLOWAY',
          17 => 'DUNDEE',
          18 => 'DVA (NI)',
          19 => 'EAST AYRSHIRE (KILMARNOCK)',
          20 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          21 => 'EAST KILBRIDE',
          22 => 'EAST LOTHIAN (HADDINGTON)',
          23 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          24 => 'EDINBURGH',
          25 => 'FALKIRK',
          26 => 'FIFE COUNCIL (KIRKCALDY)',
          27 => 'HIGHLAND COUNCIL (INVERNESS)',
          28 => 'INVERCLYDE (GREENOCK)',
          29 => 'KIRKLEES',
          30 => 'LOCHABER (HIGHLAND - FORT WILLIAM)',
          31 => 'LONDON PCO',
          32 => 'LUTON',
          33 => 'MANCHESTER',
          34 => 'MIDLOTHIAN COUNCIL',
          35 => 'MORAY (ELGIN)',
          36 => 'NAIRN (HIGHLANDS)',
          37 => 'NORTH AYRSHIRE (IRVINE)',
          38 => 'NORTH LANARKSHIRE (MOTHERWELL)',
          39 => 'NORTH LANARKSHIRE(SOUTH/CENTRAL)',
          40 => 'ORKNEY ISLANDS (KIRKWALL)',
          41 => 'PERTH & KINROSS',
          42 => 'RENFREWSHIRE (PAISLEY)',
          43 => 'ROSS & CROMARTY (HIGHLAND - DINGWALL)',
          44 => 'SCOTTISH BORDERS',
          45 => 'SHETLAND ISLANDS (LERWICK)',
          46 => 'SKYE & LOCHALSH (HIGHLAND - ISLE OF SKYE)',
          47 => 'SOUTH AYRSHIRE (AYR)',
          48 => 'SOUTH LANARKSHIRE',
          49 => 'SOUTH LANARKSHIRE (HAMILTON)',
          50 => 'SOUTH LANARKSHIRE (LANARK)',
          51 => 'SOUTH LANARKSHIRE (RUTHERGLEN)',
          52 => 'STIRLING',
          53 => 'SUTHERLAND (HIGHLAND - GOLSPIE)',
          54 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          55 => 'WEST LOTHIAN (LIVINGSTON)',
          56 => 'WESTERN ISLES (STORNOWAY)',
        ),
      ),
    ),
  ),
)


 ?>
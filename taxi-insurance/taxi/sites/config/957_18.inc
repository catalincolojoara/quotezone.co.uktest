<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '957',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '102',
    'companyName' => 'Cabshield',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '8',
      'hour' => '2',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotezone@cabshield.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-23:45',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]',
          1 => 'E[0-99]',
          2 => 'N[0-99]',
          3 => 'NW[0-99]',
          4 => 'SE[0-99]',
          5 => 'SW[0-99]',
          6 => 'W[0-99]',
        ),
      ),
    ),
    3 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 100' => '25-100',
      ),
    ),
    4 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_plating' => 
      array (
        'SKP;LIST;;SKIP the attached Taxi Plating Authorities. ' => 
        array (
          0 => 'BARKING',
          1 => 'BARNET',
          2 => 'BEXLEY',
          3 => 'BRENT',
          4 => 'BROMLEY',
          5 => 'CAMDEN',
          6 => 'CROYDEN',
          7 => 'EALING',
          8 => 'ENFIELD',
          9 => 'GREENWICH',
          10 => 'HACKNEY',
          11 => 'HAMMERSMITH',
          12 => 'HARINGEY',
          13 => 'HAVERING',
          14 => 'HILLINGDON',
          15 => 'HOUNSLOW',
          16 => 'ISLINGTON',
          17 => 'KENSINGTON',
          18 => 'KINGSTON UPON THAMES ROYAL',
          19 => 'LAMBETH',
          20 => 'LEWISHAM',
          21 => 'LONDON PCO',
          22 => 'MERTON',
          23 => 'NEWHAM',
          24 => 'REDBRIDGE',
          25 => 'RICHMOND',
          26 => 'SOUTHWARK',
          27 => 'SUTTON',
          28 => 'TOWER HAMLETS',
          29 => 'WALTHAM FOREST',
          30 => 'WANDSWORTH',
        ),
      ),
    ),
    7 => 
    array (
      'vehicle_registration_number' => 
      array (
        'SKP;VAL;MAX;SKIP if Taxi registration is blank. ' => '00',
      ),
    ),
    8 => 
    array (
      'DIS' => 
      array (
        'SKP;VAL;MIN;SKIP if "Insurance Start Date" < "Quote Date" + 7 Days' => '6',
      ),
    ),
  ),
  'pausedEmails' => 
  array (
    'MAIL' => 
    array (
      'COMPANY' => 
      array (
        0 => 'gemma.cabshield@gmail.com',
      ),
    ),
  ),
)


 ?>
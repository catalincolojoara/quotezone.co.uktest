<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '3228',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '493',
    'companyName' => 'Osborne and Sons - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '20',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'tq@os-ins.co.uk',
        1 => 'info@os-ins.co.uk',
        2 => 'donna@os-ins.co.uk',
        3 => 'donnawye@hotmail.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$_companyDetails["filter_details"][]["WS_ID"]["SKP;LIST;;SKIP if webservice source = Danny Imray. "] = array(           0 => "5fdbf8c1a05cd4cdf82ed72104896c7e", 			 1 => "785e5997d6ca27649d56af14c5d31141",       );',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '10',
          1 => '11',
          2 => '12',
          3 => '13',
          4 => '14',
          5 => '15',
          6 => '16',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    2 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0-14',
      ),
    ),
    3 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    4 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'CRAWLEY',
          1 => 'ELMBRIDGE',
          2 => 'EPSOM & EWELL',
          3 => 'HORSHAM',
          4 => 'LONDON PCO',
          5 => 'MID SUSSEX',
          6 => 'MOLE VALLEY',
          7 => 'REIGATE AND BANSTEAD',
          8 => 'RUNNYMEDE',
          9 => 'TANDRIDGE',
        ),
      ),
    ),
    5 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 66' => '25 - 66',
      ),
    ),
    6 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11602',
    'siteOrigID' => '',
    'brokerID' => '109',
    'logoID' => '103',
    'companyName' => 'County Insurance (Worthing) - Taxi (IOM)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'County Insurance (Worthing) - Taxi (IOM)',
    'limit' => 
    array (
      'month' => '114',
      'day' => '8',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
      ),
    ),
  ),
  'request' => 
  array (
    'url' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-16:00',
          6 => 'SKP_ALL_DAY',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 23 and 75' => '23 - 75',
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 2 and 23' => '2-23',
      ),
    ),
    4 => 
    array (
      'registration_number_question' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'Y',
        ),
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BRADFORD',
          1 => 'BRIDGEND',
          2 => 'CAERPHILLY',
          3 => 'CARDIFF',
          4 => 'CARMARTHENSHIRE',
          5 => 'CEREDIGION',
          6 => 'CONWY',
          7 => 'DENBIGSHIRE',
          8 => 'GWYNEDD',
          9 => 'LIVERPOOL',
          10 => 'LONDON PCO',
          11 => 'LUTON',
          12 => 'MANCHESTER',
          13 => 'MERTHYR TYDFIL',
          14 => 'MONMOUTHSHIRE',
          15 => 'SWANSEA',
          16 => 'WINDSOR & MAIDENHEAD',
        ),
      ),
    ),
    6 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BA[0-99]*',
          1 => 'BH[0-99]*',
          2 => 'BN[0-99]*',
          3 => 'BS[0-99]*',
          4 => 'CM[0-99]*',
          5 => 'CO[0-99]*',
          6 => 'CT[0-99]*',
          7 => 'DT[0-99]*',
          8 => 'EX[0-99]*',
          9 => 'GL[0-99]*',
          10 => 'GU[0-99]*',
          11 => 'IP[0-99]*',
          12 => 'LN12',
          13 => 'ME[0-99]*',
          14 => 'NN[0-99]*',
          15 => 'NR[0-99]*',
          16 => 'OX[0-99]*',
          17 => 'PL[0-99]*',
          18 => 'PO[0-99]*',
          19 => 'RG[0-9]',
          20 => 'RG11',
          21 => 'RG[13-99]',
          22 => 'RH[0-99]*',
          23 => 'SL1',
          24 => 'SL2',
          25 => 'SL[4-99]',
          26 => 'SN[0-99]*',
          27 => 'SO[0-99]*',
          28 => 'SP[0-99]*',
          29 => 'TA[0-99]*',
          30 => 'TN[0-99]*',
          31 => 'TQ[0-99]*',
          32 => 'TR[0-99]*',
          33 => 'WS[10-14]',
          34 => 'YO[0-99]*',
          35 => 'IM[0-99]*',
        ),
      ),
    ),
  ),
  'pausedEmails' => 
  array (
    'MAIL' => 
    array (
      'COMPANY' => 
      array (
        0 => 'worthing@insuretaxi.com',
      ),
    ),
  ),
)


 ?>
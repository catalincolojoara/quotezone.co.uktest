<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11776',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '165',
    'companyName' => 'HR Insurance - Taxi (No NCB)',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'j.elphinstone@hrinsure.co.uk',
        1 => 'QuotezoneTaxiReferrals@hrinsure.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    3 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 74' => '25-74',
      ),
    ),
    4 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
          1 => '5',
        ),
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEEN',
          1 => 'ABERDEEN CENTRAL (INVERURIE)',
          2 => 'ABERDEEN CITY (ABERDEEN)',
          3 => 'ABERDEENSHIRE',
          4 => 'ABERDEENSHIRE NORTH (BANFF)',
          5 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          6 => 'ANGUS',
          7 => 'ANGUS COUNCIL (FORFAR)',
          8 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          9 => 'BADENOCH & STRATHSPEY (HIGHLAND - KINGUSSIE)',
          10 => 'BERWICK ON TWEED',
          11 => 'CAITHNESS (HIGHLANDS - WICK)',
          12 => 'CLACKMANANSHIRE (ALLOA)',
          13 => 'DUMFRIES & GALLOWAY',
          14 => 'DUNDEE',
          15 => 'EAST AYRSHIRE (KILMARNOCK)',
          16 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          17 => 'EAST KILBRIDE',
          18 => 'EAST LOTHIAN (HADDINGTON)',
          19 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          20 => 'EDINBURGH',
          21 => 'FALKIRK',
          22 => 'FIFE COUNCIL (KIRKCALDY)',
          23 => 'HIGHLAND COUNCIL (INVERNESS)',
          24 => 'INVERCLYDE (GREENOCK)',
          25 => 'LOCHABER (HIGHLAND - FORT WILLIAM)',
          26 => 'MIDLOTHIAN COUNCIL',
          27 => 'MORAY (ELGIN)',
          28 => 'NAIRN (HIGHLANDS)',
          29 => 'NORTH AYRSHIRE (IRVINE)',
          30 => 'NORTH LANARKSHIRE (MOTHERWELL)',
          31 => 'NORTH LANARKSHIRE(SOUTH/CENTRAL)',
          32 => 'ORKNEY ISLANDS (KIRKWALL)',
          33 => 'PERTH & KINROSS',
          34 => 'RENFREWSHIRE (PAISLEY)',
          35 => 'ROSS & CROMARTY (HIGHLAND - DINGWALL)',
          36 => 'SCOTTISH BORDERS',
          37 => 'SHETLAND ISLANDS (LERWICK)',
          38 => 'SKYE & LOCHALSH (HIGHLAND - ISLE OF SKYE)',
          39 => 'SOUTH AYRSHIRE (AYR)',
          40 => 'SOUTH LANARKSHIRE',
          41 => 'SOUTH LANARKSHIRE (HAMILTON)',
          42 => 'SOUTH LANARKSHIRE (LANARK)',
          43 => 'SOUTH LANARKSHIRE (RUTHERGLEN)',
          44 => 'STIRLING',
          45 => 'SUTHERLAND (HIGHLAND - GOLSPIE)',
          46 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          47 => 'WEST LOTHIAN (LIVINGSTON)',
          48 => 'WESTERN ISLES (STORNOWAY)',
        ),
      ),
    ),
    6 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'AB[0-99]*',
          1 => 'DD[0-99]*',
          2 => 'DG[0-99]*',
          3 => 'EH[0-99]*',
          4 => 'FK[0-99]*',
          5 => 'G[0-99]*',
          6 => 'HS[0-99]*',
          7 => 'IV[0-99]*',
          8 => 'KA[0-99]*',
          9 => 'KW[0-99]*',
          10 => 'KY[0-99]*',
          11 => 'ML[0-99]*',
          12 => 'PA[0-99]*',
          13 => 'PH[0-99]*',
          14 => 'TD[0-99]*',
          15 => 'ZE[0-99]*',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
          6 => '15',
          7 => '16',
        ),
      ),
    ),
    8 => 
    array (
      'private_car_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
        ),
      ),
    ),
    9 => 
    array (
      'claims_5_years' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'No',
        ),
      ),
    ),
  ),
)


 ?>
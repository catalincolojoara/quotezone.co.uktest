<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11308',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '702',
    'companyName' => 'Plan Insurance - Taxi NB Limo Quotezone Weekend',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxi.quotes@planinsurance.co.uk',
        1 => 'marketingleads@planinsurance.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => 'SKP_ALL_DAY',
          2 => 'SKP_ALL_DAY',
          3 => 'SKP_ALL_DAY',
          4 => 'SKP_ALL_DAY',
          5 => 'SKP_ALL_DAY',
          6 => 'SKP_ALL_DAY',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'estimated_value_sk' => 
      array (
        'SKP;VAL;;Skip if value is between 0 and 12,499' => ' 0 - 12,499',
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 28 and 70' => '28-70',
      ),
    ),
    3 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BB[0-99]',
          1 => 'BD[0-99]',
          2 => 'BL[0-99]',
          3 => 'BT[0-99]',
          4 => 'LU[0-99]',
          5 => 'OL[0-99]',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
        ),
      ),
    ),
  ),
)


 ?>
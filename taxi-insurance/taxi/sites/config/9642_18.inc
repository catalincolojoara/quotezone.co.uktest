<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '9642',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '562',
    'companyName' => 'Policywise',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Taxi@policywise.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '     $numberOfCars=$_SESSION["_YourDetails_"]["number_of_cars"];     if($numberOfCars ==1)     $_companyDetails["filter_details"][]["vehicle_registration"]["SKP;VAL;MAX;SKIP if Vehicle registration (if known) is BLANK ."]  = "1";     ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
          3 => '10',
          4 => '11',
          5 => '12',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '4',
        ),
      ),
    ),
    2 => 
    array (
      'ABI_CODE' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '90200041',
          1 => '90200042',
          2 => '90200029',
          3 => '90200030',
          4 => '90200037',
          5 => '90200038',
          6 => '90200039',
          7 => '90200040',
          8 => '35553801',
          9 => '35554101',
          10 => '35554201',
          11 => '90200106',
          12 => '90200107',
          13 => '90200108',
          14 => '90200109',
          15 => '90200110',
          16 => '90200111',
          17 => '32045201',
          18 => '32045202',
          19 => '32045301',
          20 => '32045302',
          21 => '32045401',
          22 => '32045402',
          23 => '90200103',
          24 => '90200104',
          25 => '90200112',
          26 => '90200113',
          27 => '90200114',
          28 => '90200115',
          29 => '90200116',
          30 => '90200117',
          31 => '90200118',
          32 => '90200119',
          33 => '90200120',
          34 => '90200121',
          35 => '90200122',
          36 => '90200123',
          37 => '90200124',
          38 => '90200125',
          39 => '90200126',
          40 => '90200127',
          41 => '90200128',
          42 => '90200129',
          43 => '90200130',
          44 => '90200131',
          45 => '90200132',
          46 => '90200133',
          47 => '90200134',
          48 => '90200135',
          49 => '90200136',
          50 => '90200137',
          51 => '90200138',
          52 => '90200139',
          53 => '90200140',
          54 => '90200141',
          55 => '90200142',
          56 => '90200143',
          57 => '90200144',
          58 => '90200145',
          59 => '90200146',
          60 => '90200147',
          61 => '39199075',
          62 => '90200190',
          63 => '90200191',
          64 => '90200192',
          65 => '90200193',
          66 => '90200194',
          67 => '90200195',
          68 => '12097501',
          69 => '12097601',
          70 => '12097701',
          71 => '12097801',
          72 => '12097901',
          73 => '12098001',
          74 => '12098101',
          75 => '12098201',
          76 => '10300988',
          77 => '10300989',
          78 => '18503295',
          79 => '18503296',
          80 => '18503297',
          81 => '18503298',
          82 => '18503299',
          83 => '18503300',
          84 => '18503301',
          85 => '18503302',
          86 => '18503303',
          87 => '18503304',
          88 => '18503306',
          89 => '18503307',
          90 => '45063401',
          91 => '45063402',
          92 => '53627401',
          93 => '53627501',
          94 => '53627927',
          95 => '53627928',
          96 => '53627932',
          97 => '53627959',
          98 => '53627961',
          99 => '53627994',
          100 => '53627995',
          101 => '53627996',
          102 => '53627997',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    4 => 
    array (
      'year_of_manufacture' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2005',
          1 => '2006',
          2 => '2007',
          3 => '2008',
          4 => '2009',
          5 => '2010',
          6 => '2011',
          7 => '2012',
          8 => '2013',
          9 => '2014',
          10 => '2015',
        ),
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEENSHIRE',
          1 => 'ABERDEENSHIRE NORTH (BANFF)',
          2 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          3 => 'BIRMINGHAM',
          4 => 'BRIGHTON & HOVE',
          5 => 'BRISTOL',
          6 => 'BRISTOL CITY OF UA',
          7 => 'CARDIFF',
          8 => 'COVENTRY',
          9 => 'DERBY',
          10 => 'DUNDEE',
          11 => 'EDINBURGH',
          12 => 'KINGSTON-UPON-HULL',
          13 => 'LANCASTER',
          14 => 'LEEDS',
          15 => 'LEICESTER',
          16 => 'NEWCASTLE UPON TYNE',
          17 => 'NEWPORT',
          18 => 'NORWICH',
          19 => 'NOTTINGHAM',
          20 => 'OXFORD',
          21 => 'PETERBOROUGH',
          22 => 'PLYMOUTH',
          23 => 'PRESTON',
          24 => 'SHEFFIELD',
          25 => 'SOUTHAMPTON',
          26 => 'ST ALBANS',
          27 => 'STOKE ON TRENT',
          28 => 'SUNDERLAND',
          29 => 'SWANSEA',
          30 => 'WAKEFIELD',
          31 => 'YORK',
        ),
      ),
    ),
  ),
)


 ?>
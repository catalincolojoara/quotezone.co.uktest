<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '900',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '44',
    'companyName' => 'Milestone 3',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'COMPANY' => 
      array (
        0 => 'rob.g@milestonehouse.com',
      ),
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => 'SKP_ALL_DAY',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-23:59',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 19 and 100' => '19-100',
      ),
    ),
    2 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]',
        ),
      ),
    ),
    3 => 
    array (
      'type_of_driving_licence' => 
      array (
        'SKP;LIST;MIN;SKIP_RULE_DRIVING_LICENCE--DRV_LIC_SKP_PROVISIONAL_UK' => 
        array (
          0 => '4',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '3431',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '702',
    'companyName' => 'Plan Insurance - Taxi NB Quotezone C3 Out Of Hours',
    'offline_from_plugin' => false,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'TAXI.QUOTES@planinsurance.co.uk',
        1 => 'marketingleads@planinsurance.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'bespoke_filters' => 
  array (
    0 => '$taxiNcb=$_SESSION["_YourDetails_"]["taxi_ncb"]; $privateCarNcb=$_SESSION["_YourDetails_"]["private_car_ncb"]; if($taxiNcb == "0" && $privateCarNcb == "0") $_companyDetails["filter_details"][]["BSPK_SKP_NCB_PNCB"]["ACC;VAL;EQL;Skip if Taxi no claims bonus and Private car no claims bonus both = No NCB. "] = "BSPK_SKP"; ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|17:30-23:59',
          2 => '00:00-09:00|17:30-23:59',
          3 => '00:00-09:00|17:30-23:59',
          4 => '00:00-09:00|17:30-23:59',
          5 => '00:00-09:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
        ),
      ),
    ),
    5 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    6 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BL[0-99]*',
          4 => 'BT[0-99]*',
          5 => 'CF[0-99]*',
          6 => 'CH[0-99]*',
          7 => 'CM[0-99]*',
          8 => 'CR[0-99]*',
          9 => 'CV[0-99]*',
          10 => 'DA[0-99]*',
          11 => 'DE[0-99]*',
          12 => 'E[0-99]*',
          13 => 'G[0-99]*',
          14 => 'HA[0-99]*',
          15 => 'HP[0-99]*',
          16 => 'HX[0-99]*',
          17 => 'L[0-99]*',
          18 => 'LE[0-99]*',
          19 => 'LS[0-99]*',
          20 => 'LU[0-99]*',
          21 => 'M[0-99]*',
          22 => 'ML[0-99]*',
          23 => 'N[0-99]*',
          24 => 'NG[0-99]*',
          25 => 'OL[0-99]*',
          26 => 'OX[0-99]*',
          27 => 'PA[0-99]*',
          28 => 'PE[0-99]*',
          29 => 'PO[0-99]*',
          30 => 'PR[0-99]*',
          31 => 'RG[0-99]*',
          32 => 'RH[0-99]*',
          33 => 'RM[0-99]*',
          34 => 'SE[0-99]*',
          35 => 'SM[0-99]*',
          36 => 'SS[0-99]*',
          37 => 'UB[0-99]*',
          38 => 'WA[0-99]*',
          39 => 'WD[0-99]*',
          40 => 'WF[0-99]*',
          41 => 'WN[0-99]*',
        ),
      ),
    ),
    7 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
        ),
      ),
    ),
    8 => 
    array (
      'year_of_manufacture' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1901',
          1 => '1902',
          2 => '1903',
          3 => '1904',
          4 => '1905',
          5 => '1906',
          6 => '1907',
          7 => '1908',
          8 => '1909',
          9 => '1910',
          10 => '1911',
          11 => '1912',
          12 => '1913',
          13 => '1914',
          14 => '1915',
          15 => '1916',
          16 => '1917',
          17 => '1918',
          18 => '1919',
          19 => '1920',
          20 => '1921',
          21 => '1922',
          22 => '1923',
          23 => '1924',
          24 => '1925',
          25 => '1926',
          26 => '1927',
          27 => '1928',
          28 => '1929',
          29 => '1930',
          30 => '1931',
          31 => '1932',
          32 => '1933',
          33 => '1934',
          34 => '1935',
          35 => '1936',
          36 => '1937',
          37 => '1938',
          38 => '1939',
          39 => '1940',
          40 => '1941',
          41 => '1942',
          42 => '1943',
          43 => '1944',
          44 => '1945',
          45 => '1946',
          46 => '1947',
          47 => '1948',
          48 => '1949',
          49 => '1950',
          50 => '1951',
          51 => '1952',
          52 => '1953',
          53 => '1954',
          54 => '1955',
          55 => '1956',
          56 => '1957',
          57 => '1958',
          58 => '1959',
          59 => '1960',
          60 => '1961',
          61 => '1962',
          62 => '1963',
          63 => '1964',
          64 => '1965',
          65 => '1966',
          66 => '1967',
          67 => '1968',
          68 => '1969',
          69 => '1970',
          70 => '1971',
          71 => '1972',
          72 => '1973',
          73 => '1974',
          74 => '1975',
          75 => '1976',
          76 => '1977',
          77 => '1978',
          78 => '1979',
          79 => '1980',
          80 => '1981',
          81 => '1982',
          82 => '1983',
          83 => '1984',
          84 => '1985',
          85 => '1986',
          86 => '1987',
          87 => '1988',
          88 => '1989',
          89 => '1990',
          90 => '1991',
          91 => '1992',
          92 => '1993',
          93 => '1994',
          94 => '1995',
          95 => '1996',
          96 => '1997',
          97 => '1998',
          98 => '1999',
          99 => '2000',
          100 => '2001',
          101 => '2002',
          102 => '2003',
          103 => '2004',
          104 => '2005',
          105 => '2006',
          106 => '2007',
        ),
      ),
    ),
    9 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 7000 and 100000' => '7000 - 100000',
      ),
    ),
    10 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 69' => '30 - 69',
      ),
    ),
    11 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
  ),
)


 ?>
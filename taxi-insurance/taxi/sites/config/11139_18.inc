<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11139',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '929',
    'companyName' => 'J Willey ',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Taxi (York) Query:',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'paula@jwilley.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    1 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'YORK',
        ),
      ),
    ),
  ),
)


 ?>
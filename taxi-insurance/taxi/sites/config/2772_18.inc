<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '2772',
    'siteOrigID' => '',
    'brokerID' => '96',
    'logoID' => '497',
    'companyName' => 'Insurance Factory (Marker Study) - Taxi 3',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Insurance Factory (Marker Study) - Taxi 3',
    'limit' => 
    array (
      'mon' => '25',
      'tue' => '25',
      'wed' => '25',
      'thu' => '25',
      'fri' => '25',
      'sat' => '31',
      'sun' => '31',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'shopquotes@insurancefactory.co.uk',
        1 => 'callcampaign@insurancefactory.co.uk',
        2 => 'LeadGen@insurancefactory.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 100' => '25-100',
      ),
    ),
    1 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
          5 => '9',
          6 => '10',
          7 => '11',
          8 => '12',
          9 => '13',
        ),
      ),
    ),
    2 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    3 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BELFAST',
          1 => 'COVENTRY',
          2 => 'NORTHERN IRELAND',
          3 => 'SEFTON',
          4 => 'SOLIHULL',
        ),
      ),
    ),
  ),
)


 ?>
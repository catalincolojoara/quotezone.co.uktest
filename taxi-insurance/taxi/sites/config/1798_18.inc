<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '1798',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '57',
    'companyName' => 'Insurance Choice - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'month' => '355',
    ),
    'emailSubject' => 'Insurance Choice - Taxi',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxiquotes@insurancechoice.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'url' => '1',
    'csv' => '1',
  ),
  'bespoke_filters' => 
  array (
    0 => '$_companyDetails["filter_details"][]["WS_ID"]["SKP;LIST;;SKIP if webservice source = Danny Imray. "] = array(           0 => "5fdbf8c1a05cd4cdf82ed72104896c7e", 			 1 => "785e5997d6ca27649d56af14c5d31141",       );',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
          1 => '5',
        ),
      ),
    ),
    3 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 100' => '25 - 100',
      ),
    ),
    4 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 21' => '0 - 21',
      ),
    ),
    5 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BH[0-99]*',
          1 => 'BS[0-99]*',
          2 => 'CF[0-99]*',
          3 => 'CV[0-99]*',
          4 => 'DE[23-99]',
          5 => 'DL[0-99]*',
          6 => 'DN[0-99]*',
          7 => 'DY[0-99]*',
          8 => 'EX[0-99]*',
          9 => 'GL[0-99]*',
          10 => 'GU[0-99]*',
          11 => 'HP[0-99]*',
          12 => 'LE[0-99]*',
          13 => 'MK[0-99]*',
          14 => 'NE[0-99]*',
          15 => 'NG[0-99]*',
          16 => 'NN[0-99]*',
          17 => 'NP[0-99]*',
          18 => 'OX[0-99]*',
          19 => 'RG[0-99]*',
          20 => 'SG[0-99]*',
          21 => 'SL[0-99]*',
          22 => 'SN[0-99]*',
          23 => 'ST[0-99]*',
          24 => 'TF[0-99]*',
          25 => 'WD[0-99]*',
          26 => 'WR[0-99]*',
          27 => 'WS[0-99]*',
          28 => 'WV[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
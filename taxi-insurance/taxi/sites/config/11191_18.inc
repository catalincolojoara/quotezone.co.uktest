<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11191',
    'siteOrigID' => '',
    'brokerID' => '109',
    'logoID' => '103',
    'companyName' => 'County Insurance (Taxi) - Worthing',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'County Insurance (Taxi) - Worthing',
    'limit' => 
    array (
      'day' => '15',
      'month' => '70',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'worthing@insuretaxi.com',
      ),
    ),
  ),
  'request' => 
  array (
    'url' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-15:00',
          6 => 'SKP_ALL_DAY',
          7 => '01:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BRADFORD',
          1 => 'BRIDGEND',
          2 => 'CAERPHILLY',
          3 => 'CARDIFF',
          4 => 'CARMARTHENSHIRE',
          5 => 'CEREDIGION',
          6 => 'CONWY',
          7 => 'DENBIGSHIRE',
          8 => 'GWYNEDD',
          9 => 'LIVERPOOL',
          10 => 'LONDON PCO',
          11 => 'LUTON',
          12 => 'MANCHESTER',
          13 => 'MERTHYR TYDFIL',
          14 => 'MONMOUTHSHIRE',
          15 => 'SWANSEA',
        ),
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 23 and 75' => '23 - 75',
      ),
    ),
    3 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BA[0-99]*',
          1 => 'BH[0-99]*',
          2 => 'BN[0-10]',
          3 => 'BN[16-99]',
          4 => 'CT[0-99]*',
          5 => 'DT[0-99]*',
          6 => 'GU[0-99]*',
          7 => 'ME[0-99]*',
          8 => 'PO[0-99]*',
          9 => 'RG[2-99]*',
          10 => 'RH[0-99]*',
          11 => 'SN[0-99]*',
          12 => 'SO[0-99]*',
          13 => 'SP[0-99]*',
          14 => 'TN[0-99]*',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    5 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 2 and 23' => '2 - 23',
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '2161',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => 818,
    'companyName' => 'County Insurance (Taxi) - Brady and Jones',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
    ),
    'emailSubject' => 'County Insurance (Taxi) - Brady and Jones',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
      ),
    ),
  ),
  'request' => 
  array (
    'url' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-23:59',
          6 => 'SKP_ALL_DAY',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'CONWY',
          1 => 'DENBIGSHIRE',
          2 => 'GWYNEDD',
          3 => 'ISLE OF ANGLESEY',
          4 => 'WREXHAM',
        ),
      ),
    ),
    2 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'CH7',
          1 => 'LL[0-99]*',
          2 => 'SY13',
          3 => 'SY14',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '5',
        ),
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 100' => '25-100',
      ),
    ),
  ),
  'pausedEmails' => 
  array (
    'MAIL' => 
    array (
      'COMPANY' => 
      array (
        0 => 'enquiries@bradyandjonesinsurance.com',
      ),
    ),
  ),
)


 ?>
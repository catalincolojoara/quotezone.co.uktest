<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11728',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '103',
    'companyName' => 'County Insurance (Taxi) - Heckmondwike 2',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'County Insurance (Taxi) - Heckmondwike 2',
    'limit' => 
    array (
      'month' => '6',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
      ),
    ),
  ),
  'request' => 
  array (
    'url' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-23:59',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 8' => '8',
      ),
    ),
    3 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 21 and 100' => '21-100',
      ),
    ),
    4 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
          2 => '4',
        ),
      ),
    ),
    5 => 
    array (
      'registration_number_question' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'N',
        ),
      ),
    ),
    6 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'CA[0-99]*',
          1 => 'DN[1-41]*',
          2 => 'HU[1-20]*',
          3 => 'YO[1-31]*',
        ),
      ),
    ),
    7 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0-14',
      ),
    ),
    8 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
  ),
  'pausedEmails' => 
  array (
    'MAIL' => 
    array (
      'COMPANY' => 
      array (
        0 => 'info@taxichoiceinsurance.co.uk',
      ),
    ),
  ),
)


 ?>
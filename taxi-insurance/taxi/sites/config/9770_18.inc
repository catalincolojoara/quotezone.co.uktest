<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '9770',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '558',
    'companyName' => 'CovermyCab LDN',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'CovermyCab LDN',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotezone@protector-policies.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '     $vehMake = $_SESSION["_YourDetails_"]["vehicle_make"];  $vehModel = $_SESSION["_YourDetails_"]["vehicle_model"];   if($vehMake != "LD" && $vehMake != "MT")     {             $_companyDetails["filter_details"][]["vehicle_model"]["ACC;LIST;;ACCEPT only if Taxi model, Taxi MODEL = $vehModel--ACC_LIST"] = array( 0=>"VITO",1=>"E7",2=>"EURO7TAXI",3=>"EUROTAXI",);      $_companyDetails["filter_details"][]["vehicle_make"]["ACC;LIST;;ACCEPT only if Taxi model, Taxi make = $vehMake--ACC_LIST"] = array( 0=>"MC",1=>"PU",);     }     ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 15' => '15',
      ),
    ),
    1 => 
    array (
      'taxi_used_for' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '10',
          1 => '11',
          2 => '12',
          3 => '13',
          4 => '14',
        ),
      ),
    ),
    3 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 40000' => '0 - 40000',
      ),
    ),
    4 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
          2 => '4',
          3 => '5',
        ),
      ),
    ),
    5 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
    6 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 21' => '0-21',
      ),
    ),
    7 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BL[0-99]*',
          4 => 'BT[0-99]*',
          5 => 'CA[0-99]*',
          6 => 'CF[0-99]*',
          7 => 'CH[0-99]*',
          8 => 'CR[0-99]*',
          9 => 'CV[0-99]*',
          10 => 'CW[0-99]*',
          11 => 'DH[0-99]*',
          12 => 'DL[0-99]*',
          13 => 'DN[0-99]*',
          14 => 'DY[0-99]*',
          15 => 'E[1-20]*',
          16 => 'EC[0-99]*',
          17 => 'FY[0-99]*',
          18 => 'HA[0-99]*',
          19 => 'HD[0-99]*',
          20 => 'HG[0-99]*',
          21 => 'HR[0-99]*',
          22 => 'HU[0-99]*',
          23 => 'HX[0-99]*',
          24 => 'L[0-99]*',
          25 => 'LA[0-99]*',
          26 => 'LD[0-99]*',
          27 => 'LL[0-99]*',
          28 => 'LS[0-99]*',
          29 => 'M[0-99]*',
          30 => 'N[0-99]*',
          31 => 'NE[0-99]*',
          32 => 'NP[0-99]*',
          33 => 'NW[0-99]*',
          34 => 'OL[0-99]*',
          35 => 'PR[0-99]*',
          36 => 'S[0-99]*',
          37 => 'SA[0-99]*',
          38 => 'SE[0-99]*',
          39 => 'SK[0-99]*',
          40 => 'SL[0-99]*',
          41 => 'SM[0-99]*',
          42 => 'SR[0-99]*',
          43 => 'ST[0-99]*',
          44 => 'SW[0-99]*',
          45 => 'SY[0-99]*',
          46 => 'TF[0-99]*',
          47 => 'TS[0-99]*',
          48 => 'TW[0-99]*',
          49 => 'UB[0-99]*',
          50 => 'W[0-99]*',
          51 => 'WA[0-99]*',
          52 => 'WC[0-99]*',
          53 => 'WF[0-99]*',
          54 => 'WN[0-99]*',
          55 => 'WR[0-99]*',
          56 => 'WS[0-99]*',
          57 => 'WV[0-99]*',
          58 => 'YO[0-99]*',
          59 => 'GY[0-99]*',
          60 => 'JE[0-99]*',
          61 => 'IM[0-99]*',
        ),
      ),
    ),
    8 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 28 and 50' => '28 - 50',
      ),
    ),
  ),
)


 ?>
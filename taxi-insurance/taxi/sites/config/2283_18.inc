<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '2283',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '448',
    'companyName' => 'Brightside (OIS TQ) Nationwide',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'hour' => '2',
      'day' => '12',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxi.info@taxi-direct.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'bespoke_filters' => 
  array (
    0 => '$_companyDetails["filter_details"][]["engine_size"]["SKP;LIST;;Skip electric vehicles:  0, 0 , 0 cc, 0 CC, 0 E, 0 e ."]  = array (0 => "0", 1 => "0 ", 2 => "0 cc", 3 => "0 CC", 4 => "0 E", 5 => "0 e", ); ',
    1 => '$platingAuthority=$_SESSION["_YourDetails_"]["plating_authority"]; $taxiBadge=$_SESSION["_YourDetails_"]["taxi_badge"]; $valuesToSkipArr = array("STOCKPORT","ST HELENS","2","3",); if(in_array($platingAuthority, $valuesToSkipArr) && in_array($taxiBadge, $valuesToSkipArr)) $_companyDetails["filter_details"][]["BSPK_SKP_AUTH_BDG"]["ACC;VAL;EQL;If Taxi Plating Authority = STOCKPORT, ST HELENS SKIP if Taxi Badge = Under 6 Months 6 Months to 1 Year."]  = "SKP"; ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BD[0-99]*',
          1 => 'BT[0-99]*',
        ),
      ),
    ),
    1 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 21' => '0-21',
      ),
    ),
    2 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
          5 => '8',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    4 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    5 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 23 and 75' => '23 - 75',
      ),
    ),
    6 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    7 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEENSHIRE NORTH (BANFF)',
          1 => 'ADUR',
          2 => 'ANGUS',
          3 => 'ANGUS COUNCIL (FORFAR)',
          4 => 'ARUN',
          5 => 'ASHFIELD',
          6 => 'BASILDON',
          7 => 'BEDFORD',
          8 => 'BLABY',
          9 => 'BLAENAU GWENT',
          10 => 'BLYTH VALLEY',
          11 => 'BOLSOVER',
          12 => 'BOSTON',
          13 => 'BRACKNELL FOREST',
          14 => 'BRECKLAND',
          15 => 'BRISTOL',
          16 => 'BRISTOL CITY OF UA',
          17 => 'BROMSGROVE',
          18 => 'BROXBOURNE',
          19 => 'CAITHNESS (HIGHLANDS - WICK)',
          20 => 'CANTERBURY',
          21 => 'CARADON',
          22 => 'CARLISLE',
          23 => 'CARRICK',
          24 => 'CASTLE MORPETH',
          25 => 'CASTLE POINT',
          26 => 'CEREDIGION',
          27 => 'CHARNWOOD',
          28 => 'CHELMSFORD',
          29 => 'CHELTENHAM',
          30 => 'CHERWELL',
          31 => 'CHESTER',
          32 => 'CHESTERFIELD',
          33 => 'CHILTERN',
          34 => 'CHORLEY',
          35 => 'CHRISTCHURCH',
          36 => 'CLACKMANANSHIRE (ALLOA)',
          37 => 'COLCHESTER',
          38 => 'CONWY',
          39 => 'COPELAND',
          40 => 'CORBY',
          41 => 'CORNWALL COUNCIL',
          42 => 'COTSWOLD',
          43 => 'CRAVEN',
          44 => 'CRAWLEY',
          45 => 'DARTFORD',
          46 => 'DAVENTRY',
          47 => 'DENBIGSHIRE',
          48 => 'DEVON COUNTY',
          49 => 'DOVER',
          50 => 'DUNDEE',
          51 => 'EAST AYRSHIRE (KILMARNOCK)',
          52 => 'EAST CAMBRIDGESHIRE',
          53 => 'EAST DEVON',
          54 => 'EAST HERTFORDSHIRE',
          55 => 'EAST KILBRIDE',
          56 => 'EAST LINDSEY',
          57 => 'EAST LOTHIAN (HADDINGTON)',
          58 => 'EAST NORTHANTS',
          59 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          60 => 'EAST RIDING OF YORKSHIRE',
          61 => 'EAST STAFFORDSHIRE',
          62 => 'EASTBOURNE',
          63 => 'EASTLEIGH',
          64 => 'EDEN',
          65 => 'ELLESEMERE PORT & NESTON',
          66 => 'EPPING FOREST',
          67 => 'EXETER',
          68 => 'FALKIRK',
          69 => 'FAREHAM',
          70 => 'FENLAND',
          71 => 'GEDLING',
          72 => 'GLASGOW CITY',
          73 => 'GOSPORT',
          74 => 'GREAT YARMOUTH',
          75 => 'GUERNSEY',
          76 => 'GUILDFORD',
          77 => 'GWYNEDD',
          78 => 'HALTON',
          79 => 'HAMBLETON',
          80 => 'HARBOROUGH',
          81 => 'HARLOW',
          82 => 'HARROGATE',
          83 => 'HART',
          84 => 'HAVANT',
          85 => 'HEREFORDSHIRE',
          86 => 'HERTSMERE',
          87 => 'HIGH PEAK',
          88 => 'HIGHLAND COUNCIL (INVERNESS)',
          89 => 'HINCKLEY & BOSWORTH',
          90 => 'HORSHAM',
          91 => 'INVERCLYDE (GREENOCK)',
          92 => 'ISLE OF ANGLESEY',
          93 => 'ISLE OF MAN',
          94 => 'ISLE OF SCILLY',
          95 => 'ISLE OF WIGHT',
          96 => 'JERSEY',
          97 => 'KENNET',
          98 => 'KERRIER',
          99 => 'KETTERING',
          100 => 'KINGS LYNN & WEST NORFOLK',
          101 => 'LANCASTER',
          102 => 'LEICESTER',
          103 => 'LEWES',
          104 => 'LICHFIELD',
          105 => 'LINCOLN',
          106 => 'LOCHABER (HIGHLAND - FORT WILLIAM)',
          107 => 'MACCLESFIELD / CHESHIRE EAST',
          108 => 'MALDON',
          109 => 'MALVERN HILLS',
          110 => 'MANSFIELD',
          111 => 'MELTON',
          112 => 'MENDIP',
          113 => 'MID DEVON',
          114 => 'MID SUFFOLK',
          115 => 'MIDDLESBOROUGH',
          116 => 'MIDLOTHIAN COUNCIL',
          117 => 'MILTON KEYNES',
          118 => 'MOLE VALLEY',
          119 => 'MORAY (ELGIN)',
          120 => 'NAIRN (HIGHLANDS)',
          121 => 'NEATH & PORT TALBOT',
          122 => 'NEWARK & SHERWOOD',
          123 => 'NEWPORT',
          124 => 'NORTH CORNWALL',
          125 => 'NORTH DEVON',
          126 => 'NORTH EAST DERBYSHIRE',
          127 => 'NORTH EAST LINCOLNSHIRE',
          128 => 'NORTH KESTEVEN',
          129 => 'NORTH LANARKSHIRE (MOTHERWELL)',
          130 => 'NORTH LANARKSHIRE(SOUTH/CENTRAL)',
          131 => 'NORTH LICOLNSHIRE',
          132 => 'NORTH NORFOLK',
          133 => 'NORTH SHROPSHIRE',
          134 => 'NORTH SOMERSET',
          135 => 'NORTH WARWICKSHIRE',
          136 => 'NORWICH',
          137 => 'ORKNEY ISLANDS (KIRKWALL)',
          138 => 'OSWESTRY',
          139 => 'PEMBROKESHIRE',
          140 => 'PENDLE',
          141 => 'PENWITH',
          142 => 'PERTH & KINROSS',
          143 => 'PLYMOUTH',
          144 => 'POOLE',
          145 => 'PORTSMOUTH',
          146 => 'POWYS',
          147 => 'PSV JERSEY',
          148 => 'PSV NORTH EAST',
          149 => 'PSV SCOTLAND',
          150 => 'PSV WALES',
          151 => 'PSV WESTERN',
          152 => 'PURBECK',
          153 => 'REDDITCH',
          154 => 'REIGATE AND BANSTEAD',
          155 => 'RENFREWSHIRE (PAISLEY)',
          156 => 'RESTORMEL',
          157 => 'RIBBLE VALLEY',
          158 => 'RICHMONDSHIRE',
          159 => 'ROSS & CROMARTY (HIGHLAND - DINGWALL)',
          160 => 'RUGBY',
          161 => 'RUSHMOOR',
          162 => 'RUTLAND',
          163 => 'RYEDALE',
          164 => 'SCARBOROUGH',
          165 => 'SCOTTISH BORDERS',
          166 => 'SEDGEFIELD',
          167 => 'SEDGEMOOR',
          168 => 'SELBY',
          169 => 'SHETLAND ISLANDS (LERWICK)',
          170 => 'SHROPSHIRE COUNTY',
          171 => 'SKYE & LOCHALSH (HIGHLAND - ISLE OF SKYE)',
          172 => 'SOUTH AYRSHIRE (AYR)',
          173 => 'SOUTH BUCKINGHAM',
          174 => 'SOUTH DERBYSHIRE',
          175 => 'SOUTH EAST & METROPOLITAN',
          176 => 'SOUTH HAMS',
          177 => 'SOUTH HOLLAND',
          178 => 'SOUTH KESTEVEN',
          179 => 'SOUTH LAKELAND',
          180 => 'SOUTH NORFOLK',
          181 => 'SOUTH RIBBLE',
          182 => 'SOUTH SHROPSHIRE',
          183 => 'SOUTH SOMERSET',
          184 => 'SOUTH STAFFORDSHIRE',
          185 => 'SOUTH TYNESIDE',
          186 => 'SOUTHAMPTON',
          187 => 'SOUTHEND-ON-SEA',
          188 => 'SPELTHORNE',
          189 => 'ST EDMUNDSBURY',
          190 => 'ST HELENS',
          191 => 'STAFFORD',
          192 => 'STAFFORDSHIRE MOORLANDS',
          193 => 'STEVENAGE',
          194 => 'STIRLING',
          195 => 'STOCKPORT',
          196 => 'STOCKTON ON TEES',
          197 => 'STOKE ON TRENT',
          198 => 'STRATFORD ON AVON',
          199 => 'STROUD',
          200 => 'SUFFOLK COASTAL',
          201 => 'SUNDERLAND',
          202 => 'SURREY HEATH',
          203 => 'SUTHERLAND (HIGHLAND - GOLSPIE)',
          204 => 'SWALE',
          205 => 'TANDRIDGE',
          206 => 'TAUNTON DEANE',
          207 => 'TEESDALE',
          208 => 'TEIGNBRIDGE',
          209 => 'TELFORD & WREKIN',
          210 => 'TEWKESBURY',
          211 => 'THANET',
          212 => 'THURROCK',
          213 => 'TONBRIDGE & MALLING',
          214 => 'TORBAY',
          215 => 'TORFAEN',
          216 => 'TORRIDGE',
          217 => 'TUNBRIDGE WELLS',
          218 => 'UTTLESFORD',
          219 => 'VALE OF GLAMORGAN',
          220 => 'VALE OF WHITE HORSE',
          221 => 'VALE ROYAL',
          222 => 'WANSBECK',
          223 => 'WARRINGTON',
          224 => 'WARWICK',
          225 => 'WAVENEY',
          226 => 'WAVERLEY',
          227 => 'WEALDEN',
          228 => 'WEAR VALLEY',
          229 => 'WELLINGBOROUGH',
          230 => 'WEST BERKSHIRE',
          231 => 'WEST DEVON',
          232 => 'WEST DORSET',
          233 => 'WEST LANCASHIRE',
          234 => 'WEST LINDSEY',
          235 => 'WEST LOTHIAN (LIVINGSTON)',
          236 => 'WEST OXFORDSHIRE',
          237 => 'WEST SOMERSET',
          238 => 'WESTERN ISLES (STORNOWAY)',
          239 => 'WIGAN',
          240 => 'WINCHESTER',
          241 => 'WOKING',
          242 => 'WORTHING',
          243 => 'WYCHAVON',
          244 => 'WYCOMBE',
          245 => 'WYRE',
          246 => 'WYRE FORREST',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10150',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '448',
    'companyName' => 'Brightside (One Insurance Solution) (Orbit)',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Taxi Query (Orbit):',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxi.info@taxi-direct.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_type' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    3 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BIRMINGHAM',
          1 => 'BRIGHTON & HOVE',
          2 => 'BRISTOL CITY OF UA',
          3 => 'CARDIFF',
          4 => 'COVENTRY',
          5 => 'DERBY',
          6 => 'GLASGOW',
          7 => 'KINGSTON-UPON-HULL',
          8 => 'LANCASTER',
          9 => 'LEEDS',
          10 => 'LEICESTER',
          11 => 'NEWCASTLE UPON TYNE',
          12 => 'NEWPORT',
          13 => 'NORWICH',
          14 => 'NOTTINGHAM',
          15 => 'OXFORD',
          16 => 'PETERBOROUGH',
          17 => 'PRESTON',
          18 => 'SHEFFIELD',
          19 => 'SOUTHAMPTON',
          20 => 'ST ALBANS',
          21 => 'STOKE ON TRENT',
          22 => 'SUNDERLAND',
          23 => 'SWANSEA',
          24 => 'WAKEFIELD',
          25 => 'YORK',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'AB[0-99]*',
          1 => 'DD[0-99]*',
          2 => 'DG[0-99]*',
          3 => 'EH[0-99]*',
          4 => 'FK[0-99]*',
          5 => 'G[0-99]*',
          6 => 'HS[0-99]*',
          7 => 'IV[0-99]*',
          8 => 'KA[0-99]*',
          9 => 'KW[0-99]*',
          10 => 'KY[0-99]*',
          11 => 'ML[0-99]*',
          12 => 'PA[0-99]*',
          13 => 'PH[0-99]*',
          14 => 'TD[0-99]*',
          15 => 'ZE[0-99]*',
        ),
      ),
    ),
    5 => 
    array (
      'engine_size' => 
      array (
        'SKP;LIST;;Skip electric vehicles.' => 
        array (
          0 => '0',
          1 => '0 ',
          2 => '0 cc',
          3 => '0 CC',
          4 => '0 E',
          5 => '0 e',
        ),
      ),
    ),
  ),
)


 ?>
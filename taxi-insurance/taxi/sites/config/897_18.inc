<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '897',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '95',
    'companyName' => 'Mayfair Online',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      '30min' => '1',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'telesales@mayfaironline.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '08:00-19:30',
          2 => '08:00-19:30',
          3 => '08:00-19:30',
          4 => '08:00-19:30',
          5 => '08:00-19:30',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'DY[0-99]*',
          2 => 'HA[0-99]*',
          3 => 'L[0-99]*',
          4 => 'LU[0-99]*',
          5 => 'N[0-99]*',
          6 => 'NW[0-99]*',
          7 => 'UB[0-99]*',
          8 => 'WS[0-99]*',
          9 => 'WV[0-99]*',
        ),
      ),
    ),
    2 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 6000 and 100000' => ' 6000 - 100000',
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
          5 => '9',
          6 => '10',
          7 => '11',
          8 => '12',
        ),
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'LONDON PCO',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10304',
    'siteOrigID' => '',
    'brokerID' => '77',
    'logoID' => '448',
    'companyName' => 'Brightside OIS TQ (2)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => '',
    'limit' => 
    array (
      'hour' => '2',
      'day' => '20',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxi.info@taxi-direct.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 23 and 75' => '23 - 75',
      ),
    ),
    3 => 
    array (
      'engine_size' => 
      array (
        'SKP;LIST;;Skip electric vehicles.' => 
        array (
          0 => '0',
          1 => '0 ',
          2 => '0 cc',
          3 => '0 CC',
          4 => '0 E',
          5 => '0 e',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    6 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          1 => 'ALLERDALE',
          2 => 'ASHFIELD',
          3 => 'ASHFORD',
          4 => 'AYLESBURY VALE',
          5 => 'BARROW IN FURNESS',
          6 => 'BASINGSTOKE & DEANE',
          7 => 'BATH & NORTH EAST SOMERSET',
          8 => 'BLABY',
          9 => 'BLAENAU GWENT',
          10 => 'BOLSOVER',
          11 => 'BOURNEMOUTH',
          12 => 'BRAINTREE',
          13 => 'BRIDGEND',
          14 => 'BRIGHTON & HOVE',
          15 => 'BROADLAND',
          16 => 'BROXBOURNE',
          17 => 'CAMBRIDGE',
          18 => 'CANTERBURY',
          19 => 'CARADON',
          20 => 'CARLISLE',
          21 => 'CARMARTHENSHIRE',
          22 => 'CASTLE POINT',
          23 => 'CENTRAL BEDFORDSHIRE',
          24 => 'CHARNWOOD',
          25 => 'CHELTENHAM',
          26 => 'CHERWELL',
          27 => 'CHESHIRE EAST COUNCIL',
          28 => 'CHESHIRE WEST & CHESTER (BLUE) CHESTER',
          29 => 'CHESHIRE WEST & CHESTER (GREEN) VALE ROYAL',
          30 => 'CHESHIRE WEST & CHESTER (RED) ELLESMERE PORT',
          31 => 'CHESTER',
          32 => 'CHESTER-LE-STREET',
          33 => 'CHICHESTER',
          34 => 'CHILTERN',
          35 => 'COPELAND',
          36 => 'COTSWOLD',
          37 => 'DARLINGTON',
          38 => 'DURHAM',
          39 => 'EAST DEVON',
          40 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          41 => 'EAST HAMPSHIRE',
          42 => 'EAST HERTFORDSHIRE',
          43 => 'EASTBOURNE',
          44 => 'EASTLEIGH',
          45 => 'ELMBRIDGE',
          46 => 'EREWASH',
          47 => 'EXETER',
          48 => 'FALKIRK',
          49 => 'FAREHAM',
          50 => 'GEDLING',
          51 => 'GOSPORT',
          52 => 'GUILDFORD',
          53 => 'GWYNEDD',
          54 => 'HALTON',
          55 => 'HART',
          56 => 'HAVANT',
          57 => 'HEREFORDSHIRE',
          58 => 'HERTSMERE',
          59 => 'HIGH PEAK',
          60 => 'HORSHAM',
          61 => 'INVERCLYDE (GREENOCK)',
          62 => 'ISLE OF WIGHT',
          63 => 'KERRIER',
          64 => 'KINGS LYNN & WEST NORFOLK',
          65 => 'LANCASTER',
          66 => 'LINCOLN',
          67 => 'MACCLESFIELD / CHESHIRE EAST',
          68 => 'MALDON',
          69 => 'MENDIP',
          70 => 'MERTHYR TYDFIL',
          71 => 'MID DEVON',
          72 => 'MIDLOTHIAN COUNCIL',
          73 => 'MONMOUTHSHIRE',
          74 => 'NEATH & PORT TALBOT',
          75 => 'NEW FOREST',
          76 => 'NEWARK & SHERWOOD',
          77 => 'NEWCASTLE UNDER LYME',
          78 => 'NEWPORT',
          79 => 'NORTH CORNWALL',
          80 => 'NORTH DEVON',
          81 => 'NORTH DORSET',
          82 => 'NORTH EAST DERBYSHIRE',
          83 => 'NORTH HERTFORDSHIRE',
          84 => 'NORTH KESTEVEN',
          85 => 'NORTH LANARKSHIRE (MOTHERWELL)',
          86 => 'NORTH LANARKSHIRE(SOUTH/CENTRAL)',
          87 => 'NORTH LICOLNSHIRE',
          88 => 'NORTH NORFOLK',
          89 => 'NORTH SHROPSHIRE',
          90 => 'NORTH SOMERSET',
          91 => 'NORTH WEST LEICESTERSHIRE',
          92 => 'NUNEATON & BEDWORTH',
          93 => 'OXFORD',
          94 => 'PENDLE',
          95 => 'PENWITH',
          96 => 'PLYMOUTH',
          97 => 'POWYS',
          98 => 'PSV EASTERN',
          99 => 'PSV NORTH EAST',
          100 => 'PSV NORTH WEST',
          101 => 'PSV SCOTLAND',
          102 => 'PSV WEST MIDLANDS',
          103 => 'RENFREWSHIRE (PAISLEY)',
          104 => 'RESTORMEL',
          105 => 'RHONDDA CYNON TAFF',
          106 => 'RIBBLE VALLEY',
          107 => 'RUGBY',
          108 => 'RUSHCLIFFE',
          109 => 'RUSHMOOR',
          110 => 'SCARBOROUGH',
          111 => 'SCOTTISH BORDERS',
          112 => 'SEDGEMOOR',
          113 => 'SELBY',
          114 => 'SEVENOAKS',
          115 => 'SHETLAND ISLANDS (LERWICK)',
          116 => 'SHROPSHIRE COUNTY',
          117 => 'SOUTH AYRSHIRE (AYR)',
          118 => 'SOUTH CAMBRIDGESHIRE',
          119 => 'SOUTH GLOUCESTER',
          120 => 'SOUTH HAMS',
          121 => 'SOUTH KESTEVEN',
          122 => 'SOUTH LAKELAND',
          123 => 'SOUTH NORTHAMPTONSHIRE',
          124 => 'SOUTH SOMERSET',
          125 => 'ST EDMUNDSBURY',
          126 => 'STAFFORD',
          127 => 'STEVENAGE',
          128 => 'STIRLING',
          129 => 'STRATFORD ON AVON',
          130 => 'STROUD',
          131 => 'SUNDERLAND',
          132 => 'SWINDON',
          133 => 'TAMWORTH',
          134 => 'TANDRIDGE',
          135 => 'TAUNTON DEANE',
          136 => 'TEIGNBRIDGE',
          137 => 'TELFORD & WREKIN',
          138 => 'TEWKESBURY',
          139 => 'THURROCK',
          140 => 'TONBRIDGE & MALLING',
          141 => 'TORBAY',
          142 => 'TORFAEN',
          143 => 'TORRIDGE',
          144 => 'TUNBRIDGE WELLS',
          145 => 'VALE OF GLAMORGAN',
          146 => 'VALE OF WHITE HORSE',
          147 => 'WARWICK',
          148 => 'WATFORD',
          149 => 'WEST BERKSHIRE',
          150 => 'WEST LANCASHIRE',
          151 => 'WEST LOTHIAN (LIVINGSTON)',
          152 => 'WEST OXFORDSHIRE',
          153 => 'WEYMOUTH & PORTLAND',
          154 => 'WILTSHIRE COUNTY',
          155 => 'WINDSOR & MAIDENHEAD',
          156 => 'WOKING',
          157 => 'WOKINGHAM',
          158 => 'WORTHING',
          159 => 'WREXHAM',
          160 => 'WYCHAVON',
          161 => 'WYRE',
          162 => 'WYRE FORREST',
          163 => 'YORK',
        ),
      ),
    ),
  ),
)


 ?>
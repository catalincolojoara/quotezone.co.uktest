<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '1744',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '653',
    'companyName' => 'DNA - Taxi (Standard)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '20',
    ),
    'emailSubject' => 'DNA - Taxi (Standard)',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Taxileads@dna-insurance.com',
      ),
    ),
    'TXT' => 
    array (
      'COMPANY' => 
      array (
        0 => 'leads@inbound.msrvr.net',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$taxiNCB = $_SESSION["_YourDetails_"]["taxi_ncb"];if($taxiNCB == 0){ $_companyDetails["filter_details"][]["private_car_ncb"]["ACC;LIST;;Accept if Taxi no claims bonus = No NCB and private car no claims bonus 1 year, 2 years, 3 years, 4 years, 5+ years"] = array( 0=>"1", 1=>"2", 2=>"3", 3=>"4", 4=>"5");}else $_companyDetails["filter_details"][]["taxi_ncb"]["ACC;LIST;;Accept if value selected is 1 year, 2 years or 3 years."] = array( 0=>"1", 1=>"2", 2=>"3");',
    1 => '$_companyDetails["filter_details"][]["EMV"]["SKP;LIST;;Skip the below list of values."]  = array ( 0 => "EMV",); ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '08:30-19:00',
          2 => '08:30-19:00',
          3 => '08:30-19:00',
          4 => '08:30-19:00',
          5 => '08:30-19:00',
          6 => '08:30-13:00',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'WS_ID' => 
      array (
        'SKP;LIST;;SKIP if webservice ID = 5fdbf8c1a05cd4cdf82ed72104896c7e.' => 
        array (
          0 => '5fdbf8c1a05cd4cdf82ed72104896c7e',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 21 and 65' => '21 - 65',
      ),
    ),
    5 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'AB[0-99]*',
          1 => 'BD[0-99]*',
          2 => 'BT[0-99]*',
          3 => 'DD[0-99]*',
          4 => 'DG[0-99]*',
          5 => 'EH[0-99]*',
          6 => 'FK[0-99]*',
          7 => 'G[0-99]*',
          8 => 'HS[0-99]*',
          9 => 'IV[0-99]*',
          10 => 'KA[0-99]*',
          11 => 'KW[0-99]*',
          12 => 'KY[0-99]*',
          13 => 'ML[0-99]*',
          14 => 'PA[0-99]*',
          15 => 'PH[0-99]*',
          16 => 'TD[0-99]*',
          17 => 'ZE[0-99]*',
        ),
      ),
    ),
    6 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'LONDON PCO',
        ),
      ),
    ),
    7 => 
    array (
      'year_of_manufacture' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2008',
          1 => '2009',
          2 => '2010',
          3 => '2011',
          4 => '2012',
          5 => '2013',
          6 => '2014',
          7 => '2015',
          8 => '2016',
        ),
      ),
    ),
  ),
)


 ?>
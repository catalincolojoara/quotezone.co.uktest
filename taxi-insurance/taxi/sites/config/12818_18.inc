<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12818',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '507',
    'companyName' => 'AIB Insurance Bureau - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '5',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'help@aib.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '08:30-17:30',
          2 => '08:30-17:30',
          3 => '08:30-17:30',
          4 => '08:30-17:30',
          5 => '08:30-17:30',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
          5 => '9',
          6 => '10',
          7 => '11',
          8 => '12',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
          5 => '9',
          6 => '10',
          7 => '11',
          8 => '12',
          9 => '13',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
          3 => '4',
          4 => '5',
          5 => '6',
          6 => '7',
          7 => '8',
          8 => '9',
        ),
      ),
    ),
    4 => 
    array (
      'private_car_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
          3 => '4',
          4 => '5',
        ),
      ),
    ),
    5 => 
    array (
      'claims_5_years' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'No',
        ),
      ),
    ),
    6 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BT[0-99]*',
          2 => 'E[0-99]*',
          3 => 'EC[0-99]*',
          4 => 'L[0-99]*',
          5 => 'M[0-99]*',
          6 => 'N[0-99]*',
          7 => 'NW[0-99]*',
          8 => 'SE[0-99]*',
          9 => 'SW[0-99]*',
          10 => 'W[0-99]*',
          11 => 'WC[0-99]*',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
        ),
      ),
    ),
    8 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 69' => '25 - 69',
      ),
    ),
    9 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
        ),
      ),
    ),
    10 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEEN CITY (ABERDEEN)',
          1 => 'ABERDEENSHIRE',
          2 => 'ADUR',
          3 => 'ALLERDALE',
          4 => 'ANGUS',
          5 => 'ANGUS COUNCIL (FORFAR)',
          6 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          7 => 'ARUN',
          8 => 'ASHFIELD',
          9 => 'ASHFORD',
          10 => 'AYLESBURY VALE',
          11 => 'BABERGH',
          12 => 'BADENOCH & STRATHSPEY (HIGHLAND - KINGUSSIE)',
          13 => 'BASILDON',
          14 => 'BATH & NORTH EAST SOMERSET',
          15 => 'BLAENAU GWENT',
          16 => 'BLYTH VALLEY',
          17 => 'BOLSOVER',
          18 => 'BOSTON',
          19 => 'BOURNEMOUTH',
          20 => 'BRACKNELL FOREST',
          21 => 'BROADLAND',
          22 => 'BROMSGROVE',
          23 => 'BROXBOURNE',
          24 => 'BROXTOWE',
          25 => 'CAITHNESS (HIGHLANDS - WICK)',
          26 => 'CAMBRIDGE',
          27 => 'CANNOCK CHASE',
          28 => 'CANTERBURY',
          29 => 'CARADON',
          30 => 'CARLISLE',
          31 => 'CARMARTHENSHIRE',
          32 => 'CARRICK',
          33 => 'CHELTENHAM',
          34 => 'CHERWELL',
          35 => 'CHESTER',
          36 => 'CHESTER-LE-STREET',
          37 => 'CHESTERFIELD',
          38 => 'CHICHESTER',
          39 => 'CHRISTCHURCH',
          40 => 'CLACKMANANSHIRE (ALLOA)',
          41 => 'COLCHESTER',
          42 => 'CONGLETON',
          43 => 'CONGLETON / CHESHIRE EAST',
          44 => 'CONWY',
          45 => 'CORBY',
          46 => 'COTSWOLD',
          47 => 'CRAVEN',
          48 => 'DAVENTRY',
          49 => 'DENBIGSHIRE',
          50 => 'DERBY',
          51 => 'DERBYSHIRE DALES',
          52 => 'DONCASTER',
          53 => 'DOVER',
          54 => 'DUMFRIES & GALLOWAY',
          55 => 'DURHAM',
          56 => 'EAST AYRSHIRE (KILMARNOCK)',
          57 => 'EAST CAMBRIDGESHIRE',
          58 => 'EAST DEVON',
          59 => 'EAST DORSET',
          60 => 'EAST HAMPSHIRE',
          61 => 'EAST LINDSEY',
          62 => 'EAST LOTHIAN (HADDINGTON)',
          63 => 'EAST RIDING OF YORKSHIRE',
          64 => 'EAST STAFFORDSHIRE',
          65 => 'EASTBOURNE',
          66 => 'EASTLEIGH',
          67 => 'EDINBURGH',
          68 => 'EXETER',
          69 => 'FAREHAM',
          70 => 'FENLAND',
          71 => 'FLINTSHIRE',
          72 => 'FOREST HEATH',
          73 => 'FOREST OF DEAN',
          74 => 'GEDLING',
          75 => 'GLOUCESTER',
          76 => 'GWYNEDD',
          77 => 'HAMBLETON',
          78 => 'HARLOW',
          79 => 'HART',
          80 => 'HARTLEPOOL',
          81 => 'HASTINGS',
          82 => 'HAVANT',
          83 => 'HEREFORDSHIRE',
          84 => 'HERTSMERE',
          85 => 'HIGH PEAK',
          86 => 'HIGHLAND COUNCIL (INVERNESS)',
          87 => 'HORSHAM',
          88 => 'HUNTINGDON',
          89 => 'INVERCLYDE (GREENOCK)',
          90 => 'IPSWICH',
          91 => 'ISLE OF WIGHT',
          92 => 'KENNET',
          93 => 'KERRIER',
          94 => 'KETTERING',
          95 => 'KINGS LYNN & WEST NORFOLK',
          96 => 'LEWES',
          97 => 'LICHFIELD',
          98 => 'LINCOLN',
          99 => 'LOCHABER (HIGHLAND - FORT WILLIAM)',
          100 => 'MALDON',
          101 => 'MALVERN HILLS',
          102 => 'MANCHESTER',
          103 => 'MEDWAY',
          104 => 'MENDIP',
          105 => 'MID DEVON',
          106 => 'MID SUFFOLK',
          107 => 'MID SUSSEX',
          108 => 'MIDLOTHIAN COUNCIL',
          109 => 'MONMOUTHSHIRE',
          110 => 'MORAY (ELGIN)',
          111 => 'NAIRN (HIGHLANDS)',
          112 => 'NEW FOREST',
          113 => 'NEWARK & SHERWOOD',
          114 => 'NEWCASTLE UNDER LYME',
          115 => 'NEWPORT',
          116 => 'NORTH AYRSHIRE (IRVINE)',
          117 => 'NORTH CORNWALL',
          118 => 'NORTH DEVON',
          119 => 'NORTH DORSET',
          120 => 'NORTH EAST DERBYSHIRE',
          121 => 'NORTH EAST LINCOLNSHIRE',
          122 => 'NORTH HERTFORDSHIRE',
          123 => 'NORTH LANARKSHIRE (MOTHERWELL)',
          124 => 'NORTH LICOLNSHIRE',
          125 => 'NORTH NORFOLK',
          126 => 'NORTH SHROPSHIRE',
          127 => 'NORTH SOMERSET',
          128 => 'NORTH WARWICKSHIRE',
          129 => 'NORTHAMPTON',
          130 => 'NORWICH',
          131 => 'NUNEATON & BEDWORTH',
          132 => 'OADBY & WIGSTON',
          133 => 'ORKNEY ISLANDS (KIRKWALL)',
          134 => 'OSWESTRY',
          135 => 'OXFORD',
          136 => 'PEMBROKESHIRE',
          137 => 'PENDLE',
          138 => 'PENWITH',
          139 => 'PERTH & KINROSS',
          140 => 'PETERBOROUGH',
          141 => 'PLYMOUTH',
          142 => 'POOLE',
          143 => 'PORTSMOUTH',
          144 => 'PURBECK',
          145 => 'REDDITCH',
          146 => 'RESTORMEL',
          147 => 'RHONDDA CYNON TAFF',
          148 => 'RICHMONDSHIRE',
          149 => 'ROTHERHAM',
          150 => 'RUGBY',
          151 => 'RUTLAND',
          152 => 'RYEDALE',
          153 => 'SALISBURY',
          154 => 'SCARBOROUGH',
          155 => 'SCOTTISH BORDERS',
          156 => 'SELBY',
          157 => 'SEVENOAKS',
          158 => 'SOUTH DERBYSHIRE',
          159 => 'SOUTH GLOUCESTER',
          160 => 'SOUTH HAMS',
          161 => 'SOUTH HOLLAND',
          162 => 'SOUTH LAKELAND',
          163 => 'SOUTH NORFOLK',
          164 => 'SOUTH OXFORDSHIRE',
          165 => 'SOUTH SOMERSET',
          166 => 'SOUTH TYNESIDE',
          167 => 'SOUTHAMPTON',
          168 => 'SPELTHORNE',
          169 => 'ST EDMUNDSBURY',
          170 => 'STAFFORD',
          171 => 'STEVENAGE',
          172 => 'STIRLING',
          173 => 'STRATFORD ON AVON',
          174 => 'STROUD',
          175 => 'SUFFOLK COASTAL',
          176 => 'SURREY HEATH',
          177 => 'SUTHERLAND (HIGHLAND - GOLSPIE)',
          178 => 'SWINDON',
          179 => 'TAMWORTH',
          180 => 'TANDRIDGE',
          181 => 'TAUNTON DEANE',
          182 => 'TEIGNBRIDGE',
          183 => 'TENDRING',
          184 => 'TEST VALLEY',
          185 => 'THURROCK',
          186 => 'TONBRIDGE & MALLING',
          187 => 'TORBAY',
          188 => 'TORFAEN',
          189 => 'TORRIDGE',
          190 => 'TUNBRIDGE WELLS',
          191 => 'VALE OF WHITE HORSE',
          192 => 'VALE ROYAL',
          193 => 'WARWICK',
          194 => 'WAVERLEY',
          195 => 'WEALDEN',
          196 => 'WELLINGBOROUGH',
          197 => 'WELWYN HATFIELD',
          198 => 'WEST BERKSHIRE',
          199 => 'WEST DEVON',
          200 => 'WEST DORSET',
          201 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          202 => 'WEST LINDSEY',
          203 => 'WEST LOTHIAN (LIVINGSTON)',
          204 => 'WEST OXFORDSHIRE',
          205 => 'WEST SOMERSET',
          206 => 'WESTERN ISLES (STORNOWAY)',
          207 => 'WEYMOUTH & PORTLAND',
          208 => 'WINCHESTER',
          209 => 'WOKING',
          210 => 'WOKINGHAM',
          211 => 'WORCESTER',
          212 => 'WORTHING',
          213 => 'WREXHAM',
          214 => 'WYCHAVON',
          215 => 'WYCOMBE',
          216 => 'WYRE FORREST',
          217 => 'YORK',
        ),
      ),
    ),
    11 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 30' => '0 - 30',
      ),
    ),
  ),
)


 ?>
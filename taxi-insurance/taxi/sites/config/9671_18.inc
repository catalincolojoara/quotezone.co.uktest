<?php

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '9671',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '156',
    'companyName' => 'Patons Manchester OOH',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxileads@patonsinsurance.co.uk',
        1 => 'taxi@patonsquotes.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|18:00-23:59',
          2 => '00:00-09:00|18:00-23:59',
          3 => '00:00-09:00|18:00-23:59',
          4 => '00:00-09:00|18:00-23:59',
          5 => '00:00-09:00|18:00-23:59',
          6 => '00:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 29 and 100' => '29-100',
      ),
    ),
    2 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_plating' => 
      array (
        'ACC;LIST;;Skip the below list of values.' => 
        array (
          0 => 'MANCHESTER',
          1 => 'SALFORD',
          2 => 'TRAFFORD',
          3 => 'TAMESIDE',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
  ),
)


?>
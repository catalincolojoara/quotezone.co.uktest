<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '9673',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '570',
    'companyName' => 'A19 Insurance',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'hour' => '1',
      'day' => '5',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxis@a19insurance.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$_companyDetails["filter_details"][]["taxi_plating"]["ACC;LIST;;ACCEPT only attached Taxi Plating Authorities"] = array("ABERDEEN", "ABERDEENSHIRE", "ADUR", "ALLERDALE", "ANGUS", "ARGYLE & BUTE", "ARUN", "ASHFORD", "AYLESBURY VALE", "BABERGH", "BADENOCH & STRATHSPEY", "BARROW IN FURNESS", "BATH & N.E SOMERSET", "BEDFORD", "BLYTH VALLEY", "BOSTON", "BOURNEMOUTH", "BRAINTREE", "BRECKLAND", "BRENTWOOD", "BRIDGEND", "BRIGHTON & HOVE", "BROADLAND", "CAERPHILLY", "CAITHNESS (HIGHLANDS)", "CALDERDALE", "CAMBRIDGE", "CANTERBURY", "CARADON", "CARDIFF", "CARLISLE", "CARRICK", "CHELMSFORD", "CHELTENHAM", "CHERWELL", "CHESTER", "CHESTER-LE-STREET", "CHICHESTER", "CHILTERN", "CHRISTCHURCH", "CLACKMANNANSHIRE", "COLCHESTER", "CONWY", "COPELAND", "CORBY", "COTSWOLD", "CRAWLEY", "DACORUM", "DARLINGTON", "DARTFORD", "DAVENTRY", "DENBIGSHIRE", "DERWENTSIDE", "DONCASTER", "DOVER", "DUMFRIES & GALLOWAY", "DUNDEE", "DURHAM", "EASINGTON", "EAST AYRSHIRE (KILMARNOCK)", "EAST CAMBRIDGESHIRE", "EAST DEVON", "EAST DORSET", "EAST DUNBARTONSHIRE (KIRKINTILLOCH)", "EAST HAMPSHIRE", "EAST HERTS", "EAST KILBRIDE", "EAST LINDSEY", "EAST LOTHIAN (HADDINGTON)", "EAST NORTHANTS", "EAST RENFREWSHIRE (GIFFNOCH)", "EAST RIDING", "EASTBOURNE", "EDEN", "EDINBURGH", "ELMBRIDGE", "EPPING FOREST", "EPSOM & EWELL", "EXETER", "FALKIRK", "FAREHAM", "FENLAND", "FIFE", "FOREST HEATH", "FOREST OF DEAN", "GATESHEAD", "GILLINGHAM", "GLASGOW", "GLOUCESTER", "GOSPORT", "GRAVESHAM", "GREAT YARMOUTH", "GUILDFORD", "GWYNEDD", "HALTON", "HAMBLETON", "HARLOW", "HARROGATE", "HART", "HARTLEPOOL", "HASTINGS", "HAVANT", "HEREFORD", "HORSHAM", "HUNTINGDONSHIRE", "INVERCLYDE (GREENOCK)", "IPSWICH", "ISLE OF ANGLESEY", "ISLE OF MAN", "ISLE OF SCILLY", "ISLE OF WIGHT", "JERSEY", "KENNET", "KERRIER", "KETTERING", "KINGS LYNN & W NORFOLK", "KINGSTON-UPON-HULL", "LANCASTER", "LEOMINSTER", "LEWES", "LINCOLN", "LOCHABER (HIGHLAND - FORT WILLIAM)", "MAIDSTONE", "MALDON", "MALVERN HILLS", "MEDWAY", "MENDIP", "MERTHYR TYDFIL", "MID BEDFORDSHIRE", "MID DEVON", "MID SUFFOLK", "MID SUSSEX", "MIDDLESBOROUGH", "MIDLOTHIAN", "MILTON KEYNES", "MOLE VALLEY", "MORAY", "NAIRN (HIGHLANDS)", "NEWCASTLE UNDER LYME", "NEWCASTLE UPON TYNE", "NORTH AYRSHIRE", "NORTH CORNWALL", "NORTH DEVON", "NORTH DORSET", "NORTH EAST LINCOLNSHIRE", "NORTH HERTS", "NORTH LANARKSHIRE (NORTH)", "NORTH LANARKSHIRE(SOUTH/CENTRAL)", "NORTH LICOLNSHIRE", "NORTH NORFOLK", "NORTH SHROPSHIRE", "NORTH TYNESIDE", "NORTH WARWICKS", "NORTH WILTSHIRE", "NORTHAMPTON", "NORTHUMBERLAND", "NORWICH", "NUNEATON & BEDWORTH", "ORKNEY ISLANDS (KIRKWALL)", "OSWESTRY", "OXFORD", "PENWITH", "PERTH & KINROSS", "PETERBOROUGH", "PLYMOUTH", "POOLE", "PORTSMOUTH", "POWYS", "PSV EASTERN", "PSV JERSEY", "PSV NORTH WEST", "PSV SCOTLAND", "PSV WALES", "PURBECK", "REDCAR & CLEVELAND", "REIGATE AND BANSTEAD", "RESTORMEL", "RHONDDA CYNON TAFF", "RICHMONDSHIRE", "ROSS & CROMARTY (HIGHLAND - DINGWALL)", "ROTHER", "RUNNYMEDE", "RUSHMOOR", "RYEDALE", "SALISBURY", "SCARBOROUGH", "SCOTTISH BORDERS", "SEDGEFIELD", "SEDGEMOOR", "SELBY", "SEVENOAKS", "SHEPWAY", "SHETLAND ISLANDS (LERWICK)", "SHREWSBURY", "SHROPSHIRE COUNTY", "SKYE & LOCHAISH (HIGHLANDS)", "SOUTH AYRSHIRE (AYR)", "SOUTH BUCKINGHAM", "SOUTH CAMBRIDGE", "SOUTH EAST & METROPOLITAN", "SOUTH HAMS", "SOUTH HEREFORDSHIRE", "SOUTH HOLLAND", "SOUTH LAKELAND", "SOUTH LANARKSHIRE", "SOUTH NORFOLK", "SOUTH NORTHANTS", "SOUTH OXFORDSHIRE", "SOUTH SHROPSHIRE", "SOUTH SOMERSET", "SOUTH TYNESIDE", "SPELTHORNE", "ST ALBANS", "ST EDMUNDSBURY", "STEVENAGE", "STIRLING", "STOCKPORT", "STOCKTON ON TEES", "STRATFORD ON AVON", "STROUD", "SUFFOLK COASTAL", "SUNDERLAND", "SURREY HEATH", "SUTHERLAND (HIGHLANDS)", "SWALE", "SWINDON", "TANDRIDGE", "TAUNTON DEANE", "TEESDALE", "TEIGNBRIDGE", "TENDRING", "TEWKESBURY", "THANET DISTRICT", "THURROCK", "TONBRIDGE & MALLING", "TORBAY", "TORRIDGE", "TUNBRIDGE WELLS", "UTTLESFORD", "VALE OF GLAMORGAN", "VALE OF WHITE HORSE", "WARWICK", "WAVENEY", "WAVERLEY", "WEALDEN", "WEAR VALLEY", "WELLINGBOROUGH", "WELWYN HATFIELD", "WEST DEVON", "WEST DORSET", "WEST DUNBARTONSHIRE (CLYDEBANK)", "WEST LOTHIAN (LIVINGSTON)", "WEST OXFORD", "WEST SOMERSET", "WEST WILTSHIRE", "WESTERN ISLES (STORNOWAY)", "WEYMOUTH & PORTLAND", "WOKING", "WORCESTER", "WORTHING", "WREXHAM", "WYCHAVON", "WYCOMBE", "YORK",);',
    1 => '$_companyDetails["filter_details"][]["taxi_vehicle_age"]["SKP;VAL;MIN;SKIP if Vehicle Age > 9 Years Old. "]  = "9";',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => 'SKP_ALL_DAY',
          2 => '00:00-17:30|20:00-23:59',
          3 => '00:00-17:30|20:00-23:59',
          4 => '00:00-17:30|20:00-23:59',
          5 => '00:00-17:30|20:00-23:59',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 70' => '25-70',
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
        ),
      ),
    ),
    5 => 
    array (
      'best_time_call' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
        ),
      ),
    ),
    6 => 
    array (
      'DIS_RANGE' => 
      array (
        'SKP;VAL;;SKIP if Insurance Start Date is < 6 days after Quote Date,  or > 14 days  after Quote Date' => '6-14',
      ),
    ),
  ),
)


 ?>
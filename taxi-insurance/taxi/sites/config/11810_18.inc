<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11810',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '1030',
    'companyName' => 'Riviera Insurance',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'leads@rivierainsurance.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '04:00-23:59',
          2 => '04:00-23:59',
          3 => '04:00-23:59',
          4 => '04:00-23:59',
          5 => '04:00-23:59',
          6 => '04:00-23:59',
          7 => '04:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BA[0-99]*',
          1 => 'BH[0-99]*',
          2 => 'BS[0-99]*',
          3 => 'CF[0-99]*',
          4 => 'DT[0-99]*',
          5 => 'EX[0-99]*',
          6 => 'GL[0-99]*',
          7 => 'NP[0-99]*',
          8 => 'OX[0-99]*',
          9 => 'PL[0-99]*',
          10 => 'PO[0-99]*',
          11 => 'SA[0-99]*',
          12 => 'SN[0-99]*',
          13 => 'SO[0-99]*',
          14 => 'SP[0-99]*',
          15 => 'TA[0-99]*',
          16 => 'TQ[0-99]*',
          17 => 'TR[0-99]*',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
  ),
)


 ?>
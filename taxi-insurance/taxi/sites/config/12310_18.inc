<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12310',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '970',
    'companyName' => 'We Insure Extra - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'month' => '50',
      'day' => '3',
      'hour' => '1',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'm.kasim@weinsureextra.co.uk',
        1 => 'sheana@weinsureextra.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '10:00-16:00',
          2 => '10:00-16:00',
          3 => '10:00-16:00',
          4 => '10:00-16:00',
          5 => '10:00-16:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 100' => '25-100',
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    5 => 
    array (
      'uber_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Y',
        ),
      ),
    ),
    6 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BRADFORD',
          1 => 'LONDON PCO',
          2 => 'MANCHESTER',
          3 => 'WOLVERHAMPTON',
        ),
      ),
    ),
    7 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 7' => '0-7',
      ),
    ),
    8 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
    9 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    10 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B5',
          1 => 'B6',
          2 => 'B8',
          3 => 'B9',
          4 => 'B10',
          5 => 'B11',
          6 => 'B12',
          7 => 'B18',
          8 => 'B19',
          9 => 'B20',
          10 => 'B21',
          11 => 'B66',
          12 => 'BD[0-99]*',
          13 => 'BT[0-99]*',
          14 => 'E12',
          15 => 'L4',
          16 => 'L6',
          17 => 'L11',
          18 => 'L33',
          19 => 'LS[0-99]*',
          20 => 'M[0-99]*',
          21 => 'N22',
          22 => 'S4',
        ),
      ),
    ),
    11 => 
    array (
      'convictions_5_years' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    12 => 
    array (
      'claims_5_years' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
  ),
)


 ?>
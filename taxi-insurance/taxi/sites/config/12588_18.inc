<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12588',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '961',
    'companyName' => 'PMW Commercial Solutions',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      '2hours' => '1',
      'day' => '2',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'info@pmwcommercialsolutions.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    2 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BLACKBURN WITH DARWEN',
          1 => 'BOLTON',
          2 => 'BRADFORD',
          3 => 'LIVERPOOL',
          4 => 'LONDON PCO',
          5 => 'MANCHESTER',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
          3 => '4',
          4 => '5',
          5 => '6',
          6 => '7',
        ),
      ),
    ),
    4 => 
    array (
      'uber_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Y',
        ),
      ),
    ),
    5 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BD[0-99]*',
          2 => 'BL[0-99]*',
          3 => 'BT[0-99]*',
          4 => 'LL[0-99]*',
          5 => 'LU[0-99]*',
          6 => 'M[0-99]*',
        ),
      ),
    ),
    6 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 100' => '25-100',
      ),
    ),
    7 => 
    array (
      'convictions_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    8 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    9 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
    10 => 
    array (
      'private_car_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
    11 => 
    array (
      'taxi_used_for' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
  ),
)


 ?>
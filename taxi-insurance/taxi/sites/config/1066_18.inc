<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '1066',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '816',
    'companyName' => 'Motorcade (Birmingham)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'christianc@acorninsure.com',
        1 => 'lucyp@acorninsure.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 25000' => ' 0 - 25000',
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 69' => '25-69',
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
          3 => '4',
        ),
      ),
    ),
    3 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'CV[0-99]*',
          1 => 'DE[0-99]*',
          2 => 'DY[0-99]*',
          3 => 'LE[0-99]*',
          4 => 'NG[0-99]*',
          5 => 'NN[0-99]*',
          6 => 'ST[0-99]*',
        ),
      ),
    ),
    4 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
  ),
)


 ?>
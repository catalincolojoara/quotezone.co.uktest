<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '1594',
    'siteOrigID' => '',
    'brokerID' => '57',
    'logoID' => '615',
    'companyName' => 'Milestone - Taxi (New Badge)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Milestone - Taxi (New Badge)',
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'rob.g@milestonehouse.com',
        1 => 'nathan.t@milestonehouse.com',
        2 => 'brenda.b@milestonehouse.com',
        3 => 'caroline.w@milestonehouse.com',
        4 => 'shauna.s@milestonehouse.com',
        5 => 'jack.w@milestonehouse.com',
        6 => 'james.h@milestonehouse.com',
        7 => 'matthew.o@milestonehouse.com',
        8 => 'red@rcdata.co.uk',
        9 => 'zoe.h@milestonehouse.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    2 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BRADFORD',
          1 => 'KNOWSLEY',
          2 => 'LEICESTER',
          3 => 'LIVERPOOL',
          4 => 'LONDON PCO',
          5 => 'SEFTON',
          6 => 'WOLVERHAMPTON',
        ),
      ),
    ),
    3 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 23 and 73' => ' 23 - 73',
      ),
    ),
    4 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 21' => ' 0 - 21',
      ),
    ),
    5 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    6 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BD[0-99]*',
          1 => 'BR[0-99]*',
          2 => 'BT[0-99]*',
          3 => 'CR[0-99]*',
          4 => 'DA[0-99]*',
          5 => 'E[0-99]*',
          6 => 'EC[0-99]*',
          7 => 'EN[0-99]*',
          8 => 'HA[0-99]*',
          9 => 'IG[0-99]*',
          10 => 'L[0-99]*',
          11 => 'LS[0-99]*',
          12 => 'N[0-99]*',
          13 => 'NW[0-99]*',
          14 => 'OL[0-99]*',
          15 => 'SE[0-99]*',
          16 => 'SW[0-99]*',
          17 => 'TW[0-99]*',
          18 => 'UB[0-99]*',
          19 => 'W[0-99]*',
          20 => 'WC[0-99]*',
          21 => 'WD[0-99]*',
          22 => 'WV[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
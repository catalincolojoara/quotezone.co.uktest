<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10043',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '558',
    'companyName' => 'Cover My Cab - Taxi (Black Cab OOH)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Cover My Cab - Taxi (Black Cab OOH)',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotezone@protector-policies.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '     $vehMake = $_SESSION["_YourDetails_"]["vehicle_make"];  $vehModel = $_SESSION["_YourDetails_"]["vehicle_model"];   if($vehMake != "LD" && $vehMake != "MT")     {             $_companyDetails["filter_details"][]["vehicle_model"]["ACC;LIST;;ACCEPT only if Taxi model, Taxi MODEL = $vehModel--ACC_LIST"] = array( 0=>"VITO",1=>"E7",2=>"EURO7TAXI",3=>"EUROTAXI",);      $_companyDetails["filter_details"][]["vehicle_make"]["ACC;LIST;;ACCEPT only if Taxi model, Taxi make = $vehMake--ACC_LIST"] = array( 0=>"MC",1=>"PU",);     }     ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|17:00-23:59',
          2 => '00:00-09:00|17:00-23:59',
          3 => '00:00-09:00|17:00-23:59',
          4 => '00:00-09:00|17:00-23:59',
          5 => '00:00-09:00|17:00-23:59',
          6 => '00:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '5',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '10',
          1 => '11',
          2 => '12',
          3 => '13',
          4 => '14',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'AB[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BL[0-99]*',
          4 => 'BT[0-99]*',
          5 => 'CW[0-99]*',
          6 => 'DG[0-99]*',
          7 => 'DH[0-99]*',
          8 => 'DY[0-99]*',
          9 => 'EH[0-99]*',
          10 => 'FK[0-99]*',
          11 => 'FY[0-99]*',
          12 => 'G[0-99]*',
          13 => 'HG[0-99]*',
          14 => 'HS[0-99]*',
          15 => 'HX[0-99]*',
          16 => 'IV[0-99]*',
          17 => 'KA[0-99]*',
          18 => 'KW[0-99]*',
          19 => 'KY[0-99]*',
          20 => 'L[0-99]*',
          21 => 'LS[0-99]*',
          22 => 'M[1-18]*',
          23 => 'ML[0-99]*',
          24 => 'PA[0-99]*',
          25 => 'PH[0-99]*',
          26 => 'SK[0-99]*',
          27 => 'SR[0-99]*',
          28 => 'TD[0-99]*',
          29 => 'TS[0-99]*',
          30 => 'WA[0-99]*',
          31 => 'WF[0-99]*',
          32 => 'ZE[0-99]*',
          33 => 'GY[0-99]*',
          34 => 'JE[0-99]*',
          35 => 'IM[0-99]*',
        ),
      ),
    ),
    5 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 27 and 71' => '27 - 71',
      ),
    ),
    6 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 30' => '0 - 30',
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12643',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '702',
    'companyName' => 'Plan Insurance',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'marketingleads@planinsurance.co.uk',
        1 => 'taxisales@planinsurance.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'bespoke_filters' => 
  array (
    0 => '$taxiType=$_SESSION["_YourDetails_"]["taxi_type"]; $platingAuthority=trim(strtoupper($_SESSION["_YourDetails_"]["plating_authority"])); if($taxiType =="1"&&$platingAuthority=="LONDON PCO") $_companyDetails["filter_details"][]["BSPK_SKP EB2-61655"]["ACC;VAL;EQL;If Taxi Plating Authority = London PCO SKIP if Taxi Type = Black Cab / Hackney Carriage ."]  = "SKP"; ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:30',
          2 => '09:00-17:30',
          3 => '09:00-17:30',
          4 => '09:00-17:30',
          5 => '09:00-17:30',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 7000 and 100000' => '7000 - 100000',
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 35 and 70' => '35-70',
      ),
    ),
    3 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 11' => '11',
      ),
    ),
    4 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
        ),
      ),
    ),
    5 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BL[0-99]*',
          4 => 'CH[0-99]*',
          5 => 'E[0-99]*',
          6 => 'HX[0-99]*',
          7 => 'L[0-99]*',
          8 => 'LS[0-99]*',
          9 => 'LU[0-99]*',
          10 => 'M[0-99]*',
          11 => 'OL[0-99]*',
          12 => 'PR[0-99]*',
          13 => 'WD[0-99]*',
          14 => 'WN[0-99]*',
        ),
      ),
    ),
    6 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
    8 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    9 => 
    array (
      'type_of_cover' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
          2 => '4',
        ),
      ),
    ),
    10 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
          5 => '8',
        ),
      ),
    ),
    11 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
  ),
)


 ?>
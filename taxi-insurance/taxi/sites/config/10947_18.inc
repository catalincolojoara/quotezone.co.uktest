<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10947',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '212',
    'companyName' => 'First Insurance Services - Taxi 3',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'First Insurance Services - Taxi 3',
    'limit' => 
    array (
      'day' => '5',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Anthony.phillips@first.co.uk',
        1 => 'Craig.hancock@first.co.uk',
        2 => 'martin.frangleton@first.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'registration_number_question' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'N',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
          3 => '10',
          4 => '11',
          5 => '12',
          6 => '13',
          7 => '14',
          8 => '15',
          9 => '16',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '4',
        ),
      ),
    ),
    5 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    6 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BELFAST',
          1 => 'DVA (NI)',
          2 => 'LONDON PCO',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    8 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 7' => '0 - 7',
      ),
    ),
    9 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    10 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 69' => '25 - 69',
      ),
    ),
    11 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'AB[0-99]*',
          1 => 'BA[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BN[0-99]*',
          4 => 'BS[0-99]*',
          5 => 'BT[0-99]*',
          6 => 'CB[0-99]*',
          7 => 'CF[0-99]*',
          8 => 'CO[0-99]*',
          9 => 'DD[0-99]*',
          10 => 'DH[0-99]*',
          11 => 'EH[0-99]*',
          12 => 'EN[0-99]*',
          13 => 'EX[0-99]*',
          14 => 'FK[0-99]*',
          15 => 'G[0-99]*',
          16 => 'GL[0-99]*',
          17 => 'GU[0-99]*',
          18 => 'HP[0-99]*',
          19 => 'IP[0-99]*',
          20 => 'IV[0-99]*',
          21 => 'LN[0-99]*',
          22 => 'MK[0-99]*',
          23 => 'ML[0-99]*',
          24 => 'NN[0-99]*',
          25 => 'NR[0-99]*',
          26 => 'OX[0-99]*',
          27 => 'PE[0-99]*',
          28 => 'PH[0-99]*',
          29 => 'PL[0-99]*',
          30 => 'PO[0-99]*',
          31 => 'RG[0-99]*',
          32 => 'RH[0-99]*',
          33 => 'SA[0-99]*',
          34 => 'SG[0-99]*',
          35 => 'SL[0-99]*',
          36 => 'SN[0-99]*',
          37 => 'SP[0-99]*',
          38 => 'SY[0-99]*',
          39 => 'TA[0-99]*',
          40 => 'YO[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
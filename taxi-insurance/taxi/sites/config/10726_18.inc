<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10726',
    'siteOrigID' => '',
    'brokerID' => '75',
    'logoID' => '368',
    'companyName' => 'Connect Insurance 11',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '7',
    ),
    'emailSubject' => 'Taxi 11 Query:',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'leadTX@connect-insurance.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$platingAuthority=strtoupper($_SESSION["_YourDetails_"]["plating_authority"]); $platingAuthorityArr = array("WOLVERHAMPTON",); if(in_array($platingAuthority, $platingAuthorityArr)) $_companyDetails["filter_details"][]["postcode_sk"]["ACC;LIST;;If Taxi Plating Authority = Wolverhampton Only Accept if Postcode = WV[0-99]*."]  = array ("WV[0-99]*",); ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 74' => '25 - 74',
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
          6 => '15',
          7 => '16',
        ),
      ),
    ),
    4 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 28' => '0-28',
      ),
    ),
    5 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    6 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BD8',
          1 => 'BT[0-99]*',
          2 => 'E[0-99]*',
          3 => 'EC[0-99]*',
          4 => 'N[0-99]*',
          5 => 'NW[0-99]*',
          6 => 'SE[0-99]*',
          7 => 'SW[0-99]*',
          8 => 'W[0-99]*',
          9 => 'WC[0-99]*',
        ),
      ),
    ),
    7 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ALLERDALE',
          1 => 'ASHFORD',
          2 => 'BARROW IN FURNESS',
          3 => 'BASINGSTOKE & DEANE',
          4 => 'BATH & NORTH EAST SOMERSET',
          5 => 'BLAENAU GWENT',
          6 => 'BRACKNELL FOREST',
          7 => 'BRAINTREE',
          8 => 'BRENTWOOD',
          9 => 'BRIDGENORTH',
          10 => 'CAERPHILLY',
          11 => 'CARLISLE',
          12 => 'CARMARTHENSHIRE',
          13 => 'CEREDIGION',
          14 => 'CHELMSFORD',
          15 => 'CHELTENHAM',
          16 => 'CHESTER-LE-STREET',
          17 => 'COPELAND',
          18 => 'COTSWOLD',
          19 => 'CRAVEN',
          20 => 'DARLINGTON',
          21 => 'DONCASTER',
          22 => 'DOVER',
          23 => 'DURHAM',
          24 => 'EASINGTON',
          25 => 'EAST HAMPSHIRE',
          26 => 'EAST HERTFORDSHIRE',
          27 => 'EAST LINDSEY',
          28 => 'EAST RIDING OF YORKSHIRE',
          29 => 'EDEN',
          30 => 'EPPING FOREST',
          31 => 'FOREST OF DEAN',
          32 => 'GLOUCESTER',
          33 => 'GRAVESHAM',
          34 => 'GUILDFORD',
          35 => 'HAMBLETON',
          36 => 'HARLOW',
          37 => 'HARROGATE',
          38 => 'HART',
          39 => 'HASTINGS',
          40 => 'HEREFORDSHIRE',
          41 => 'HYNDBURN',
          42 => 'KENNET',
          43 => 'KINGSTON-UPON-HULL',
          44 => 'LANCASTER',
          45 => 'LINCOLN',
          46 => 'MALDON',
          47 => 'MID BEDFORDSHIRE',
          48 => 'MILTON KEYNES',
          49 => 'MONMOUTHSHIRE',
          50 => 'NEATH & PORT TALBOT',
          51 => 'NEWPORT',
          52 => 'NORTH EAST LINCOLNSHIRE',
          53 => 'NORTH HERTFORDSHIRE',
          54 => 'NORTH LICOLNSHIRE',
          55 => 'NORTH SOMERSET',
          56 => 'NORTH WILTSHIRE',
          57 => 'PEMBROKESHIRE',
          58 => 'PENDLE',
          59 => 'POWYS',
          60 => 'READING',
          61 => 'RIBBLE VALLEY',
          62 => 'RICHMONDSHIRE',
          63 => 'ROTHER',
          64 => 'RUSHMOOR',
          65 => 'RYEDALE',
          66 => 'SCARBOROUGH',
          67 => 'SEDGEFIELD',
          68 => 'SELBY',
          69 => 'SEVENOAKS',
          70 => 'SHEPWAY',
          71 => 'SOUTH GLOUCESTER',
          72 => 'SOUTH LAKELAND',
          73 => 'SOUTH STAFFORDSHIRE',
          74 => 'STEVENAGE',
          75 => 'STROUD',
          76 => 'SURREY HEATH',
          77 => 'SWANSEA',
          78 => 'SWINDON',
          79 => 'TAMESIDE',
          80 => 'TEESDALE',
          81 => 'TELFORD & WREKIN',
          82 => 'TEWKESBURY',
          83 => 'THANET',
          84 => 'THANET BROADSTAIRS',
          85 => 'THANET DISTRICT',
          86 => 'THANET MARGATE',
          87 => 'THANET RAMSGATE',
          88 => 'TORFAEN',
          89 => 'WEAR VALLEY',
          90 => 'WEST BERKSHIRE',
          91 => 'WEST LINDSEY',
          92 => 'WEST WILTSHIRE',
          93 => 'WOKING',
          94 => 'WOKINGHAM',
          95 => 'WOLVERHAMPTON',
          96 => 'YORK',
        ),
      ),
    ),
    8 => 
    array (
      'registration_number_question' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'N',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12833',
    'siteOrigID' => '',
    'brokerID' => '90',
    'logoID' => '500',
    'companyName' => 'Insurance 4u Services (Uber) (OOH)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Taxi (Uber) Query:',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'chris@carinsurance4u.co.uk',
        1 => 'dan@carinsurance4u.co.uk',
        2 => 'chrisjones@carinsurance4u.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|17:00-23:59',
          2 => '00:00-09:00|17:00-23:59',
          3 => '00:00-09:00|17:00-23:59',
          4 => '00:00-09:00|17:00-23:59',
          5 => '00:00-09:00|17:00-23:59',
          6 => '00:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 28' => '0-28',
      ),
    ),
    2 => 
    array (
      'uber_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'Y',
        ),
      ),
    ),
    3 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BIRMINGHAM',
          1 => 'BROMSGROVE',
          2 => 'CANNOCK CHASE',
          3 => 'DUDLEY',
          4 => 'EAST STAFFORDSHIRE',
          5 => 'LICHFIELD',
          6 => 'NORTH WARWICKSHIRE',
          7 => 'REDDITCH',
          8 => 'SANDWELL',
          9 => 'SHROPSHIRE COUNTY',
          10 => 'SOLIHULL',
          11 => 'SOUTH STAFFORDSHIRE',
          12 => 'STAFFORD',
          13 => 'TAMWORTH',
          14 => 'TELFORD & WREKIN',
          15 => 'WALSALL',
          16 => 'WOLVERHAMPTON',
          17 => 'WYRE FORREST',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    6 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    7 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 70' => '25-70',
      ),
    ),
    8 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'DY[1-12]',
          2 => 'ST12',
          3 => 'ST[14-21]',
          4 => 'SY[1-14]',
          5 => 'TF[1-13]',
          6 => 'WS[1-15]',
          7 => 'WV[1-16]',
        ),
      ),
    ),
  ),
)


 ?>
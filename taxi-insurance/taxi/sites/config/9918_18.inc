<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '9918',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '721',
    'companyName' => 'Patons Insurance',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Taxi query: London Black Cab OH',
    'limit' => 
    array (
      'day' => '5',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxi@patonsquotes.co.uk',
        1 => 'taxileads@patonsinsurance.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '     $vehMake = $_SESSION["_YourDetails_"]["vehicle_make"];     if($vehMake != "LD")     {         $_companyDetails["filter_details"][]["vehicle_model"]["ACC;LIST;;ACCEPT only if Taxi model, Taxi MODEL = see attached--ACC_LIST"] = array( 0=>"VITO",);     $_companyDetails["filter_details"][]["vehicle_make"]["ACC;LIST;;ACCEPT only if Taxi model, Taxi make = see attached--ACC_LIST"] = array( 0=>"MC",);     }     ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 65' => '25-65',
      ),
    ),
    2 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'LONDON PCO',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_type' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '4',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11981',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '792',
    'companyName' => 'CJS Risk Management - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotezone@cjs-rm.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => 'SKP_ALL_DAY',
          2 => 'SKP_ALL_DAY',
          3 => '09:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-23:59',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 23 and 100' => '23-100',
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
          5 => '9',
          6 => '10',
          7 => '11',
          8 => '12',
          9 => '13',
        ),
      ),
    ),
    3 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BR[0-99]*',
          1 => 'CR[0-99]*',
          2 => 'DA[0-99]*',
          3 => 'E[0-99]*',
          4 => 'EC[0-99]*',
          5 => 'EN[0-99]*',
          6 => 'HA[0-99]*',
          7 => 'IG[0-99]*',
          8 => 'KT[0-99]*',
          9 => 'N[0-99]*',
          10 => 'NW[0-99]*',
          11 => 'RM[0-99]*',
          12 => 'SE[0-99]*',
          13 => 'SM[0-99]*',
          14 => 'SW[0-99]*',
          15 => 'TW[0-99]*',
          16 => 'UB[0-99]*',
          17 => 'W[0-99]*',
          18 => 'WC[0-99]*',
          19 => 'WD[0-99]*',
        ),
      ),
    ),
    4 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'LONDON PCO',
        ),
      ),
    ),
  ),
)


 ?>
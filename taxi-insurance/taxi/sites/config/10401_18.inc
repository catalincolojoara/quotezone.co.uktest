<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10401',
    'siteOrigID' => '',
    'brokerID' => '73',
    'logoID' => '558',
    'companyName' => 'Cover My Cab - Taxi (NSC OOH)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Cover My Cab - Taxi NSC (OOH)',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotezone@covermycab.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|17:00-23:59',
          2 => '00:00-09:00|17:00-23:59',
          3 => '00:00-09:00|17:00-23:59',
          4 => '00:00-09:00|17:00-23:59',
          5 => '00:00-09:00|17:00-23:59',
          6 => '00:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 28 and 69' => '28 - 69',
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 15' => '15',
      ),
    ),
    4 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '4',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '10',
          1 => '11',
          2 => '12',
          3 => '13',
          4 => '14',
        ),
      ),
    ),
    6 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'AB[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BL[0-99]*',
          4 => 'BT[0-99]*',
          5 => 'CW[0-99]*',
          6 => 'DD[0-99]*',
          7 => 'DG[0-99]*',
          8 => 'DH[0-99]*',
          9 => 'DY[0-99]*',
          10 => 'EH[0-99]*',
          11 => 'FK[0-99]*',
          12 => 'FY[0-99]*',
          13 => 'G[0-99]*',
          14 => 'HG[0-99]*',
          15 => 'HS[0-99]*',
          16 => 'HX[0-99]*',
          17 => 'IV[0-99]*',
          18 => 'KA[0-99]*',
          19 => 'KW[0-99]*',
          20 => 'KY[0-99]*',
          21 => 'L[0-99]*',
          22 => 'LS[0-99]*',
          23 => 'M[1-18]*',
          24 => 'ML[0-99]*',
          25 => 'NW[0-99]*',
          26 => 'PA[0-99]*',
          27 => 'PH[0-99]*',
          28 => 'SE[0-99]*',
          29 => 'SK[0-99]*',
          30 => 'SR[0-99]*',
          31 => 'SW[0-99]*',
          32 => 'TD[0-99]*',
          33 => 'TR[0-99]*',
          34 => 'W[0-99]*',
          35 => 'WA[0-99]*',
          36 => 'WC[0-99]*',
          37 => 'WF[0-99]*',
          38 => 'WN[0-99]*',
          39 => 'ZE[0-99]*',
          40 => 'GY[0-99]*',
          41 => 'JE[0-99]*',
          42 => 'IM[0-99]*',
        ),
      ),
    ),
    7 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 30' => '0 - 30',
      ),
    ),
    8 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'ABERDEEN CITY (ABERDEEN)',
          1 => 'ABERDEENSHIRE',
          2 => 'ABERDEENSHIRE NORTH (BANFF)',
          3 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          4 => 'ALNWICK',
          5 => 'ANGUS',
          6 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          7 => 'BADENOCH & STRATHSPEY (HIGHLAND - KINGUSSIE)',
          8 => 'BELFAST',
          9 => 'BERWICK ON TWEED',
          10 => 'BIRMINGHAM',
          11 => 'BLACKBURN WITH DARWEN',
          12 => 'BLYTH VALLEY',
          13 => 'BOLTON',
          14 => 'BRADFORD',
          15 => 'BURNLEY',
          16 => 'BURY',
          17 => 'CAERPHILLY',
          18 => 'CALDERDALE',
          19 => 'CARADON',
          20 => 'CARRICK',
          21 => 'CHESTER-LE-STREET',
          22 => 'CLACKMANANSHIRE (ALLOA)',
          23 => 'CREWE & NANTWICH',
          24 => 'DUMFRIES & GALLOWAY',
          25 => 'DUNDEE',
          26 => 'DURHAM',
          27 => 'DVA (NI)',
          28 => 'EASINGTON',
          29 => 'EAST AYRSHIRE (KILMARNOCK)',
          30 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          31 => 'EAST KILBRIDE',
          32 => 'EAST LOTHIAN (HADDINGTON)',
          33 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          34 => 'EDINBURGH',
          35 => 'ELLESEMERE PORT & NESTON',
          36 => 'FALKIRK',
          37 => 'FIFE COUNCIL (KIRKCALDY)',
          38 => 'FLINTSHIRE',
          39 => 'GATESHEAD',
          40 => 'GLASGOW CITY',
          41 => 'GUERNSEY',
          42 => 'HIGHLAND COUNCIL (INVERNESS)',
          43 => 'HYNDBURN',
          44 => 'ISLE OF MAN',
          45 => 'ISLE OF SCILLY',
          46 => 'JERSEY',
          47 => 'KENNET',
          48 => 'KERRIER',
          49 => 'LEEDS',
          50 => 'LIVERPOOL',
          51 => 'LOCHABER (HIGHLAND - FORT WILLIAM)',
          52 => 'LONDON PCO',
          53 => 'MACCLESFIELD / CHESHIRE EAST',
          54 => 'MANCHESTER',
          55 => 'MERTHYR TYDFIL',
          56 => 'MID BEDFORDSHIRE',
          57 => 'MIDLOTHIAN COUNCIL',
          58 => 'MORAY (ELGIN)',
          59 => 'NAIRN (HIGHLANDS)',
          60 => 'NEATH & PORT TALBOT',
          61 => 'NEWCASTLE UPON TYNE',
          62 => 'NEWPORT',
          63 => 'NORTH AYRSHIRE (IRVINE)',
          64 => 'NORTH LANARKSHIRE (MOTHERWELL)',
          65 => 'NORTH LANARKSHIRE(SOUTH/CENTRAL)',
          66 => 'NORTH TYNESIDE',
          67 => 'NORTH WILTSHIRE',
          68 => 'NORTHERN IRELAND',
          69 => 'NORTHUMBERLAND',
          70 => 'OLDHAM',
          71 => 'ORKNEY ISLANDS (KIRKWALL)',
          72 => 'PENWITH',
          73 => 'PERTH & KINROSS',
          74 => 'RENFREWSHIRE (PAISLEY)',
          75 => 'RESTORMEL',
          76 => 'ROCHDALE',
          77 => 'ROSS & CROMARTY (HIGHLAND - DINGWALL)',
          78 => 'ROSSENDALE',
          79 => 'SALFORD',
          80 => 'SALISBURY',
          81 => 'SCOTTISH BORDERS',
          82 => 'SEDGEFIELD',
          83 => 'SKYE & LOCHALSH (HIGHLAND - ISLE OF SKYE)',
          84 => 'SOUTH AYRSHIRE (AYR)',
          85 => 'SOUTH BEDFORDSHIRE',
          86 => 'SOUTH EAST & METROPOLITAN',
          87 => 'SOUTH LANARKSHIRE',
          88 => 'SOUTH LANARKSHIRE (HAMILTON)',
          89 => 'SOUTH LANARKSHIRE (LANARK)',
          90 => 'SOUTH LANARKSHIRE (RUTHERGLEN)',
          91 => 'SOUTH TYNESIDE',
          92 => 'STIRLING',
          93 => 'SUNDERLAND',
          94 => 'SUTHERLAND (HIGHLAND - GOLSPIE)',
          95 => 'SWANSEA',
          96 => 'TAMESIDE',
          97 => 'VALE ROYAL',
          98 => 'WAKEFIELD',
          99 => 'WALSALL',
          100 => 'WANSBECK',
          101 => 'WEAR VALLEY',
          102 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          103 => 'WEST LOTHIAN (LIVINGSTON)',
          104 => 'WEST WILTSHIRE',
          105 => 'WESTERN ISLES (STORNOWAY)',
          106 => 'WIGAN',
          107 => 'WREXHAM',
        ),
      ),
    ),
  ),
)


 ?>
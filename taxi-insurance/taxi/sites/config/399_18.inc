<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '399',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '2',
    'companyName' => 'Staveley Head - Taxi ',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'specialistvehicleteam@staveleyhead.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => 1,
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-18:30',
          2 => '09:00-18:30',
          3 => '09:00-18:30',
          4 => '09:00-18:30',
          5 => '09:00-18:30',
          6 => '09:00-12:30',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 74' => ' 25 - 74',
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 35000' => '0 - 35000',
      ),
    ),
    5 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 15' => '15',
      ),
    ),
    6 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
          6 => '15',
          7 => '16',
        ),
      ),
    ),
    7 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    8 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B66',
          1 => 'BB1',
          2 => 'BB9',
          3 => 'BD5',
          4 => 'BD[7-9]',
          5 => 'BD[97-99]',
          6 => 'BL3',
          7 => 'BL9',
          8 => 'BT[0-99]*',
          9 => 'L[3-4]',
          10 => 'L[6-8]',
          11 => 'L[11-12]',
          12 => 'L15',
          13 => 'L25',
          14 => 'L33',
          15 => 'LS7',
          16 => 'M[8-9]',
          17 => 'M[11-12]',
          18 => 'M16',
          19 => 'M19',
          20 => 'OL1',
          21 => 'OL8',
        ),
      ),
    ),
    9 => 
    array (
      'taxi_used_for' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10711',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '103',
    'companyName' => 'County Insurance (Taxi) - Quotezone Mail box',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'County Insurance (Taxi) - Quotezone Mail box',
    'limit' => 
    array (
      'day' => '35',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotezone@countyins.com',
      ),
    ),
  ),
  'request' => 
  array (
    'url' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BRADFORD',
          1 => 'BRIDGEND',
          2 => 'CAERPHILLY',
          3 => 'CARDIFF',
          4 => 'CARMARTHENSHIRE',
          5 => 'CEREDIGION',
          6 => 'CONWY',
          7 => 'DENBIGSHIRE',
          8 => 'GWYNEDD',
          9 => 'LIVERPOOL',
          10 => 'LONDON PCO',
          11 => 'LUTON',
          12 => 'MANCHESTER',
          13 => 'MERTHYR TYDFIL',
          14 => 'MONMOUTHSHIRE',
          15 => 'SWANSEA',
        ),
      ),
    ),
    1 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0 - 14',
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 24 and 100' => '24 - 100',
      ),
    ),
    3 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BA[0-99]*',
          2 => 'BB[0-99]*',
          3 => 'BD[0-99]*',
          4 => 'BH[0-99]*',
          5 => 'BL[0-99]*',
          6 => 'BN[0-99]*',
          7 => 'BT[0-99]*',
          8 => 'CA[1-28]',
          9 => 'CA95',
          10 => 'CT[0-99]*',
          11 => 'DN[0-99]*',
          12 => 'DT[0-99]*',
          13 => 'E[0-99]*',
          14 => 'EC[0-99]*',
          15 => 'GU[0-99]*',
          16 => 'HD[0-99]*',
          17 => 'HG[0-99]*',
          18 => 'HX[0-99]*',
          19 => 'IG[0-99]*',
          20 => 'L[0-99]*',
          21 => 'LL[0-99]*',
          22 => 'LS[0-99]*',
          23 => 'LU[0-99]*',
          24 => 'M[0-99]*',
          25 => 'ME[0-99]*',
          26 => 'N[0-99]*',
          27 => 'NW[0-99]*',
          28 => 'OL[0-99]*',
          29 => 'PO[0-99]*',
          30 => 'PR[0-99]*',
          31 => 'RG[0-99]*',
          32 => 'RH[0-99]*',
          33 => 'SE[0-99]*',
          34 => 'SK[0-99]*',
          35 => 'SL[0-99]*',
          36 => 'SN[0-99]*',
          37 => 'SO[0-99]*',
          38 => 'SP[0-99]*',
          39 => 'SW[0-99]*',
          40 => 'TN[0-99]*',
          41 => 'UB[0-99]*',
          42 => 'W[0-99]*',
          43 => 'WC[0-99]*',
          44 => 'WF[0-99]*',
          45 => 'YO[0-99]*',
          46 => 'GY[0-99]*',
          47 => 'JE[0-99]*',
          48 => 'IM[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
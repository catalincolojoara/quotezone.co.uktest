<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11990',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '761',
    'companyName' => 'Freeway Insurance 3',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotes@freewayinsurance.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'bespoke_filters' => 
  array (
    0 => '  $filter = "";   $typeOfCover=$_SESSION["_YourDetails_"]["type_of_cover"]; $estimatedValue=$_SESSION["_YourDetails_"]["estimated_value"]; if($typeOfCover == "1")     $_companyDetails["filter_details"][]["estimated_value"]["SKP;VAL;MIN;if Type of Cover is  Fully Comprehensive SKIP if Vehicle Value is Over  50000 ."]  = "50000"; ',
    1 => '   $filter = "";  $typeOfCover=$_SESSION["_YourDetails_"]["type_of_cover"]; $estimatedValue=$_SESSION["_YourDetails_"]["estimated_value"]; if($typeOfCover == "2")     $_companyDetails["filter_details"][]["estimated_value"]["SKP;VAL;MIN;if Type of Cover is  Third Party Fire & Theft SKIP if Vehicle Value Over   6000 ."]  = "6000"; ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 20 and 75' => '20-75',
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    2 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    3 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'LONDON PCO',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 12' => '12',
      ),
    ),
    5 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '8',
          1 => '9',
          2 => '10',
          3 => '11',
          4 => '12',
          5 => '13',
          6 => '14',
          7 => '15',
          8 => '16',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    7 => 
    array (
      'type_of_cover' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
        ),
      ),
    ),
    8 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    9 => 
    array (
      'convictions_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    10 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 3500 and 100000' => '3500 - 100000',
      ),
    ),
  ),
)


 ?>
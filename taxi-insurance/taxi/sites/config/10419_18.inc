<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10419',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '103',
    'companyName' => 'County Insurance (Taxi) - Cardiff',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'County Insurance (Taxi) - Cardiff',
    'limit' => 
    array (
      'month' => '47',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
      ),
    ),
  ),
  'request' => 
  array (
    'url' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'age_range' => 
      array (
        'SKP;VAL;;Skip if value is between 10 and 23' => '10-23',
      ),
    ),
    1 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BLAENAU GWENT',
          1 => 'BRIDGEND',
          2 => 'CAERPHILLY',
          3 => 'CARDIFF',
          4 => 'CARMARTHENSHIRE',
          5 => 'CEREDIGION',
          6 => 'MERTHYR TYDFIL',
          7 => 'MONMOUTHSHIRE',
          8 => 'NEATH & PORT TALBOT',
          9 => 'NEWPORT',
          10 => 'PEMBROKESHIRE',
          11 => 'POWYS',
          12 => 'RHONDDA CYNON TAFF',
          13 => 'SWANSEA',
          14 => 'TORFAEN',
          15 => 'VALE OF GLAMORGAN',
        ),
      ),
    ),
    2 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0 - 14',
      ),
    ),
    3 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BA[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BH[0-99]*',
          4 => 'BN[0-99]*',
          5 => 'BT[0-99]*',
          6 => 'CA[0-99]*',
          7 => 'CT[0-99]*',
          8 => 'DN[0-99]*',
          9 => 'DT[0-99]*',
          10 => 'GU[0-99]*',
          11 => 'IG[0-99]*',
          12 => 'LL[0-99]*',
          13 => 'ME[0-99]*',
          14 => 'PO[0-99]*',
          15 => 'RG[0-99]*',
          16 => 'RH[0-99]*',
          17 => 'SN[0-99]*',
          18 => 'SO[0-99]*',
          19 => 'SP[0-99]*',
          20 => 'TN[0-99]*',
          21 => 'UB[0-99]*',
          22 => 'YO[0-99]*',
          23 => 'IM[0-99]*',
        ),
      ),
    ),
  ),
  'pausedEmails' => 
  array (
    'MAIL' => 
    array (
      'COMPANY' => 
      array (
        0 => 'taxicardiff@insuretaxi.com',
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12302',
    'siteOrigID' => '',
    'brokerID' => '19',
    'logoID' => '20',
    'companyName' => 'Coversure Kennington',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Croydon@coversure.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '08:00-17:00',
          2 => '08:00-17:00',
          3 => '08:00-17:00',
          4 => '08:00-17:00',
          5 => '08:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_used_for' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_type' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
          2 => '4',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
          5 => '8',
        ),
      ),
    ),
    4 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'LONDON PCO',
        ),
      ),
    ),
    5 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    6 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
          3 => '10',
          4 => '11',
          5 => '12',
        ),
      ),
    ),
    7 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 100' => '30-100',
      ),
    ),
    8 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BR3',
          1 => 'BR4',
          2 => 'CR[0-99]*',
          3 => 'CR2',
          4 => 'CR3',
          5 => 'CR4',
          6 => 'SE1',
          7 => 'SE5',
          8 => 'SE11',
          9 => 'SE17',
          10 => 'SE19',
          11 => 'SE20',
          12 => 'SE25',
          13 => 'SE27',
          14 => 'SW3',
          15 => 'SW5',
          16 => 'SW7',
          17 => 'SW8',
          18 => 'SW10',
          19 => 'W2',
          20 => 'W3',
          21 => 'W5',
          22 => 'W6',
          23 => 'W7',
          24 => 'W8',
          25 => 'W9',
          26 => 'W10',
          27 => 'W11',
          28 => 'W12',
          29 => 'W13',
          30 => 'W14',
        ),
      ),
    ),
    9 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
  ),
)


 ?>
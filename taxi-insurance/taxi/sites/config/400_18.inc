<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '400',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '448',
    'companyName' => 'Brightside (One Insurance Solution)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Taxi (MPV) Query:',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxi.info@taxi-direct.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
          5 => '8',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 23 and 75' => '23 - 75',
      ),
    ),
    3 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
          1 => 'DE[0-99]*',
          2 => 'DT[0-99]*',
          3 => 'EX[0-99]*',
          4 => 'PL[0-99]*',
          5 => 'TA[0-99]*',
          6 => 'TQ[0-99]*',
          7 => 'TR[0-99]*',
        ),
      ),
    ),
    5 => 
    array (
      'engine_size' => 
      array (
        'SKP;LIST;;Skip electric vehicles.' => 
        array (
          0 => '0',
          1 => '0 ',
          2 => '0 cc',
          3 => '0 CC',
          4 => '0 E',
          5 => '0 e',
        ),
      ),
    ),
    6 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BARNSLEY',
          1 => 'BIRMINGHAM',
          2 => 'BLACKBURN WITH DARWEN',
          3 => 'BOLTON',
          4 => 'BRADFORD',
          5 => 'BRENTWOOD',
          6 => 'BURNLEY',
          7 => 'BURY',
          8 => 'CALDERDALE',
          9 => 'DERBY',
          10 => 'DUDLEY',
          11 => 'GATESHEAD',
          12 => 'HYNDBURN',
          13 => 'KIRKLEES',
          14 => 'KNOWSLEY',
          15 => 'LEEDS',
          16 => 'LIVERPOOL',
          17 => 'LUTON',
          18 => 'MANCHESTER',
          19 => 'NEWCASTLE UPON TYNE',
          20 => 'OLDHAM',
          21 => 'PRESTON',
          22 => 'ROSSENDALE',
          23 => 'SALFORD',
          24 => 'SANDWELL',
          25 => 'SEFTON',
          26 => 'SHEFFIELD',
          27 => 'SLOUGH',
          28 => 'SOLIHULL',
          29 => 'ST ALBANS',
          30 => 'ST HELENS',
          31 => 'STOCKPORT',
          32 => 'TRAFFORD',
          33 => 'WAKEFIELD',
          34 => 'WALSALL',
          35 => 'WIRRALL',
          36 => 'WOLVERHAMPTON',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
  ),
)


 ?>
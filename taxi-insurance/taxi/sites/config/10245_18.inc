<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10245',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '590',
    'companyName' => 'Go Skippy - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '30',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'commercialleads@goskippy.com',
        1 => 'paul.casson@rock-services.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => 'SKP_ALL_DAY',
          2 => 'SKP_ALL_DAY',
          3 => '09:00-17:30',
          4 => '09:00-17:30',
          5 => '09:00-17:30',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
          1 => 'LU[0-99]*',
          2 => 'RH[0-99]*',
          3 => 'SL[0-99]*',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_used_for' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
        ),
      ),
    ),
    5 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 69' => '30 - 69',
      ),
    ),
    6 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BIRMINGHAM',
          1 => 'BLACKBURN WITH DARWEN',
          2 => 'BLACKPOOL',
          3 => 'BRADFORD',
          4 => 'BURNLEY',
          5 => 'ELLESEMERE PORT & NESTON',
          6 => 'GATESHEAD',
          7 => 'HYNDBURN',
          8 => 'KIRKLEES',
          9 => 'KNOWSLEY',
          10 => 'LEEDS',
          11 => 'LIVERPOOL',
          12 => 'LONDON PCO',
          13 => 'LUTON',
          14 => 'MANCHESTER',
          15 => 'MERTON',
          16 => 'NORTHERN IRELAND',
          17 => 'PRESTON',
          18 => 'REDCAR & CLEVELAND',
          19 => 'ROCHDALE',
          20 => 'ROSSENDALE',
          21 => 'RUSHCLIFFE',
          22 => 'SALFORD',
          23 => 'SANDWELL',
          24 => 'SEFTON',
          25 => 'SHEFFIELD',
          26 => 'SOLIHULL',
          27 => 'SOUTH EAST & METROPOLITAN',
          28 => 'SOUTH RIBBLE',
          29 => 'TAMESIDE',
          30 => 'TRAFFORD',
          31 => 'WAKEFIELD',
          32 => 'WATFORD',
          33 => 'WIRRALL',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '4',
        ),
      ),
    ),
    8 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    9 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
          3 => '10',
          4 => '11',
          5 => '12',
        ),
      ),
    ),
  ),
)


 ?>
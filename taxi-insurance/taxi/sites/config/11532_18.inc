<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11532',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '754',
    'companyName' => 'Quotax Insurance',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'daniel.fosker@quotax.net',
        1 => 'leads@quotax.net',
        2 => 'leadsn1@taxi-insurance.london',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    3 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BABERGH',
          1 => 'BRECKLAND',
          2 => 'BROADLAND',
          3 => 'FOREST HEATH',
          4 => 'GREAT YARMOUTH',
          5 => 'IPSWICH',
          6 => 'KINGS LYNN & WEST NORFOLK',
          7 => 'MID SUFFOLK',
          8 => 'NORTH NORFOLK',
          9 => 'NORWICH',
          10 => 'SOUTH NORFOLK',
          11 => 'ST EDMUNDSBURY',
          12 => 'SUFFOLK COASTAL',
          13 => 'WAVENEY',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10567',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '728',
    'companyName' => 'Abbey Insurance - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Darlain.pollock@abbey-insurance.co.uk',
        1 => 'kyla.burch@abbey-insurance.co.uk',
        2 => 'Gillian.spence@abbey-insurance.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 23 and 65' => '23 - 65',
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11100',
    'siteOrigID' => '',
    'brokerID' => '19',
    'logoID' => '20',
    'companyName' => 'Coversure Leyton',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'hour' => '2',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'watford@coversure.co.uk',
        1 => 'thurrock@coversure.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'url' => '1',
  ),
  'bespoke_filters' => 
  array (
    0 => ' $platingAuth = $_SESSION["_YourDetails_"]["plating_authority"]; if($platingAuth == "LONDON PCO") { $_companyDetails["filter_details"][]["postcode_sk"]["ACC;LIST;;If Taxi licensing authority is London 0-99]."]  = array (0 => "WD[0-99]*",); } else { $_companyDetails["filter_details"][]["plating_authority"]["ACC;LIST;;Accept the below list of values."]  = array ( 0 => "DACORUM", 1 => "HERTSMERE", 2 => "ST ALBANS", 3 => "THREE RIVERS", 4 => "THURROCK", 5 => "WATFORD", ); } ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|17:00-23:59',
          2 => '00:00-09:00|17:00-23:59',
          3 => '00:00-09:00|17:00-23:59',
          4 => '00:00-09:00|17:00-23:59',
          5 => '00:00-09:00|17:00-23:59',
          6 => '00:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'type_of_cover' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'registration_number_question' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'Y',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    5 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 65' => '25 - 65',
      ),
    ),
    6 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 39999' => ' 0 - 39999',
      ),
    ),
    7 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '5',
        ),
      ),
    ),
    8 => 
    array (
      'taxi_used_for' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
        ),
      ),
    ),
    9 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
        ),
      ),
    ),
    10 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    11 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'LU[0-99]*',
        ),
      ),
    ),
    12 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 15' => '0 - 15',
      ),
    ),
  ),
)


 ?>
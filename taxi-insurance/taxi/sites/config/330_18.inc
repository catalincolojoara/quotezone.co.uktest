<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '330',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '4',
    'companyName' => 'A Plan - Parkstone - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '30',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxi.sales@aplan.co.uk',
        1 => 'mat.howard@aplan.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    1 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0-14',
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 19 and 75' => '19 - 75',
      ),
    ),
    3 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'ABERDEEN',
          1 => 'ABERDEEN CITY (ABERDEEN)',
          2 => 'AMBER VALLEY',
          3 => 'BARNSLEY',
          4 => 'BASILDON',
          5 => 'BASSETLAW',
          6 => 'BELFAST',
          7 => 'BIRMINGHAM',
          8 => 'BLACKPOOL',
          9 => 'BOLSOVER',
          10 => 'BRADFORD',
          11 => 'BRENTWOOD',
          12 => 'BRIDGENORTH',
          13 => 'BROXBOURNE',
          14 => 'BROXTOWE',
          15 => 'CANNOCK CHASE',
          16 => 'CARLISLE',
          17 => 'CASTLE POINT',
          18 => 'CHESTERFIELD',
          19 => 'CHORLEY',
          20 => 'DARLINGTON',
          21 => 'DARTFORD',
          22 => 'DERBY',
          23 => 'DONCASTER',
          24 => 'DUDLEY',
          25 => 'DUMFRIES & GALLOWAY',
          26 => 'DUNDEE',
          27 => 'DVA (NI)',
          28 => 'EAST KILBRIDE',
          29 => 'EAST RIDING OF YORKSHIRE',
          30 => 'EAST STAFFORDSHIRE',
          31 => 'EDINBURGH',
          32 => 'ELLESEMERE PORT & NESTON',
          33 => 'EREWASH',
          34 => 'FYLDE',
          35 => 'GATESHEAD',
          36 => 'GEDLING',
          37 => 'GLASGOW CITY',
          38 => 'GRAVESHAM',
          39 => 'HALTON',
          40 => 'HAMBLETON',
          41 => 'HARTLEPOOL',
          42 => 'HIGH PEAK',
          43 => 'HINCKLEY & BOSWORTH',
          44 => 'HUNTINGDON',
          45 => 'HYNDBURN',
          46 => 'ISLE OF MAN',
          47 => 'ISLINGTON',
          48 => 'KETTERING',
          49 => 'KINGSTON-UPON-HULL',
          50 => 'KIRKLEES',
          51 => 'KNOWSLEY',
          52 => 'LEEDS',
          53 => 'LICHFIELD',
          54 => 'LIVERPOOL',
          55 => 'LONDON PCO',
          56 => 'LUTON',
          57 => 'MACCLESFIELD / CHESHIRE EAST',
          58 => 'MANCHESTER',
          59 => 'MANSFIELD',
          60 => 'MELTON',
          61 => 'MIDDLESBOROUGH',
          62 => 'NEWCASTLE UPON TYNE',
          63 => 'NORTH EAST DERBYSHIRE',
          64 => 'NORTH KESTEVEN',
          65 => 'NORTH TYNESIDE',
          66 => 'NORTH WARWICKSHIRE',
          67 => 'NORTHERN IRELAND',
          68 => 'NUNEATON & BEDWORTH',
          69 => 'OADBY & WIGSTON',
          70 => 'OLDHAM',
          71 => 'PENDLE',
          72 => 'PRESTON',
          73 => 'REDCAR & CLEVELAND',
          74 => 'REDDITCH',
          75 => 'RIBBLE VALLEY',
          76 => 'RICHMONDSHIRE',
          77 => 'ROCHDALE',
          78 => 'ROSSENDALE',
          79 => 'ROTHERHAM',
          80 => 'RUGBY',
          81 => 'RUNNYMEDE',
          82 => 'RUSHCLIFFE',
          83 => 'RYEDALE',
          84 => 'SALFORD',
          85 => 'SEDGEFIELD',
          86 => 'SEFTON',
          87 => 'SELBY',
          88 => 'SHEFFIELD',
          89 => 'SLOUGH',
          90 => 'SOLIHULL',
          91 => 'SOUTH DERBYSHIRE',
          92 => 'SOUTH KESTEVEN',
          93 => 'SOUTH RIBBLE',
          94 => 'SOUTH STAFFORDSHIRE',
          95 => 'SOUTH TYNESIDE',
          96 => 'SPELTHORNE',
          97 => 'ST ALBANS',
          98 => 'ST HELENS',
          99 => 'STAFFORD',
          100 => 'STEVENAGE',
          101 => 'STOCKPORT',
          102 => 'STOCKTON ON TEES',
          103 => 'STOKE ON TRENT',
          104 => 'STRATFORD ON AVON',
          105 => 'SUNDERLAND',
          106 => 'TAMESIDE',
          107 => 'TAMWORTH',
          108 => 'TEESDALE',
          109 => 'THREE RIVERS',
          110 => 'THURROCK',
          111 => 'TRAFFORD',
          112 => 'VALE ROYAL',
          113 => 'WAKEFIELD',
          114 => 'WALSALL',
          115 => 'WANSBECK',
          116 => 'WARRINGTON',
          117 => 'WARWICK',
          118 => 'WATFORD',
          119 => 'WEAR VALLEY',
          120 => 'WELWYN HATFIELD',
          121 => 'WEST LINDSEY',
          122 => 'WEST LOTHIAN (LIVINGSTON)',
          123 => 'WIGAN',
          124 => 'WIRRALL',
          125 => 'WOLVERHAMPTON',
          126 => 'WREXHAM',
          127 => 'WYRE',
          128 => 'WYRE FORREST',
          129 => 'YORK',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BL[0-99]*',
          4 => 'BT[0-99]*',
          5 => 'E[0-99]*',
          6 => 'EC[0-99]*',
          7 => 'FY[0-99]*',
          8 => 'L[0-99]*',
          9 => 'LS[0-99]*',
          10 => 'M[0-99]*',
          11 => 'N[0-99]*',
          12 => 'NW[0-99]*',
          13 => 'OL[0-99]*',
          14 => 'PR[0-99]*',
          15 => 'SE[0-99]*',
          16 => 'SW[0-99]*',
          17 => 'W[0-99]*',
          18 => 'WC[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
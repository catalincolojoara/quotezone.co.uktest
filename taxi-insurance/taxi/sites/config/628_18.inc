<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '628',
    'siteOrigID' => '',
    'brokerID' => '36',
    'logoID' => '34',
    'companyName' => 'Think Insurance',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '7',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Martin@think-ins.co.uk',
        1 => 'David@think-ins.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '08:30-17:00',
          2 => '08:30-17:00',
          3 => '08:30-17:00',
          4 => '08:30-17:00',
          5 => '08:30-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'convictions_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 69' => '30 - 69',
      ),
    ),
    3 => 
    array (
      'type_of_cover' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
        ),
      ),
    ),
    4 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    6 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_used_for' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
        ),
      ),
    ),
    8 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
        ),
      ),
    ),
    9 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'ALLERDALE',
          1 => 'BARNSLEY',
          2 => 'BASSETLAW',
          3 => 'BIRMINGHAM',
          4 => 'BLACKBURN WITH DARWEN',
          5 => 'BLYTH VALLEY',
          6 => 'BOLSOVER',
          7 => 'BOLTON',
          8 => 'BRADFORD',
          9 => 'BURNLEY',
          10 => 'BURY',
          11 => 'CALDERDALE',
          12 => 'CARDIFF',
          13 => 'CHESTERFIELD',
          14 => 'CHORLEY',
          15 => 'COVENTRY',
          16 => 'CRAVEN',
          17 => 'CREWE & NANTWICH',
          18 => 'DERBY',
          19 => 'DONCASTER',
          20 => 'DUDLEY',
          21 => 'ELLESEMERE PORT & NESTON',
          22 => 'GATESHEAD',
          23 => 'GLASGOW CITY',
          24 => 'HALTON',
          25 => 'HYNDBURN',
          26 => 'ISLE OF MAN',
          27 => 'ISLE OF SCILLY',
          28 => 'KENNET',
          29 => 'KERRIER',
          30 => 'KINGSTON-UPON-HULL',
          31 => 'KIRKLEES',
          32 => 'KNOWSLEY',
          33 => 'LEEDS',
          34 => 'LEICESTER',
          35 => 'LIVERPOOL',
          36 => 'LONDON PCO',
          37 => 'LUTON',
          38 => 'MANCHESTER',
          39 => 'MEDWAY',
          40 => 'MILTON KEYNES',
          41 => 'NEWCASTLE UPON TYNE',
          42 => 'NEWPORT',
          43 => 'NORTH EAST DERBYSHIRE',
          44 => 'NORTH TYNESIDE',
          45 => 'NOTTINGHAM',
          46 => 'OLDHAM',
          47 => 'ORKNEY ISLANDS (KIRKWALL)',
          48 => 'OSWESTRY',
          49 => 'PENDLE',
          50 => 'RESTORMEL',
          51 => 'RIBBLE VALLEY',
          52 => 'ROCHDALE',
          53 => 'ROSSENDALE',
          54 => 'ROTHERHAM',
          55 => 'SALFORD',
          56 => 'SALISBURY',
          57 => 'SANDWELL',
          58 => 'SEFTON',
          59 => 'SHEFFIELD',
          60 => 'SHETLAND ISLANDS (LERWICK)',
          61 => 'SLOUGH',
          62 => 'SOUTH RIBBLE',
          63 => 'SOUTH STAFFORDSHIRE',
          64 => 'SOUTH TYNESIDE',
          65 => 'STOCKPORT',
          66 => 'SUNDERLAND',
          67 => 'SWANSEA',
          68 => 'TAMESIDE',
          69 => 'TEESDALE',
          70 => 'TRAFFORD',
          71 => 'VALE ROYAL',
          72 => 'WAKEFIELD',
          73 => 'WALSALL',
          74 => 'WEAR VALLEY',
          75 => 'WEST LANCASHIRE',
          76 => 'WESTERN ISLES (STORNOWAY)',
          77 => 'WIGAN',
          78 => 'WINDSOR & MAIDENHEAD',
          79 => 'WIRRALL',
          80 => 'WOLVERHAMPTON',
        ),
      ),
    ),
    10 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    11 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
          6 => '15',
          7 => '16',
        ),
      ),
    ),
  ),
)


 ?>
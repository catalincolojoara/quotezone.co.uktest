<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11202',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '212',
    'companyName' => 'First Insurance Services - Taxi 2',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'First Insurance Services - Taxi 2',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Anthony.phillips@first.co.uk',
        1 => 'Craig.hancock@first.co.uk',
        2 => 'martin.frangleton@first.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'AB[0-99]*',
          1 => 'BA[0-99]*',
          2 => 'BS[0-99]*',
          3 => 'CB[0-99]*',
          4 => 'CF[0-99]*',
          5 => 'CO[0-99]*',
          6 => 'DD[0-99]*',
          7 => 'DH[0-99]*',
          8 => 'EH[0-99]*',
          9 => 'EN[0-99]*',
          10 => 'EX[0-99]*',
          11 => 'FK[0-99]*',
          12 => 'G[0-99]*',
          13 => 'GL[0-99]*',
          14 => 'IP[0-99]*',
          15 => 'IV[0-99]*',
          16 => 'LN[0-99]*',
          17 => 'MK[0-99]*',
          18 => 'ML[0-99]*',
          19 => 'NN[0-99]*',
          20 => 'NR[0-99]*',
          21 => 'OX[0-99]*',
          22 => 'PE[0-99]*',
          23 => 'PH[0-99]*',
          24 => 'PL[0-99]*',
          25 => 'SA[0-99]*',
          26 => 'SG[0-99]*',
          27 => 'SN[0-99]*',
          28 => 'SY[0-99]*',
          29 => 'TA[0-99]*',
          30 => 'YO[0-99]*',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
          3 => '4',
          4 => '5',
          5 => '6',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    4 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 21' => '0-21',
      ),
    ),
    5 => 
    array (
      'registration_number_question' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'N',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
          3 => '10',
          4 => '11',
          5 => '12',
          6 => '13',
          7 => '14',
          8 => '15',
          9 => '16',
        ),
      ),
    ),
    8 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    9 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 40 and 69' => '40-69',
      ),
    ),
    10 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
  ),
)


 ?>
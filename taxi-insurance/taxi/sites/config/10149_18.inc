<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10149',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '448',
    'companyName' => 'Brightside (One Insurance Solution) (Walsingham)',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Taxi Query (Walsingham): ',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxi.info@taxi-direct.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-23:59',
          6 => '00:00-12:00',
          7 => '12:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 65' => '30-65',
      ),
    ),
    2 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    3 => 
    array (
      'year_of_manufacture' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1901',
          1 => '1902',
          2 => '1903',
          3 => '1904',
          4 => '1905',
          5 => '1906',
          6 => '1907',
          7 => '1908',
          8 => '1909',
          9 => '1910',
          10 => '1911',
          11 => '1912',
          12 => '1913',
          13 => '1914',
          14 => '1915',
          15 => '1916',
          16 => '1917',
          17 => '1918',
          18 => '1919',
          19 => '1920',
          20 => '1921',
          21 => '1922',
          22 => '1923',
          23 => '1924',
          24 => '1925',
          25 => '1926',
          26 => '1927',
          27 => '1928',
          28 => '1929',
          29 => '1930',
          30 => '1931',
          31 => '1932',
          32 => '1933',
          33 => '1934',
          34 => '1935',
          35 => '1936',
          36 => '1937',
          37 => '1938',
          38 => '1939',
          39 => '1940',
          40 => '1941',
          41 => '1942',
          42 => '1943',
          43 => '1944',
          44 => '1945',
          45 => '1946',
          46 => '1947',
          47 => '1948',
          48 => '1949',
          49 => '1950',
          50 => '1951',
          51 => '1952',
          52 => '1953',
          53 => '1954',
          54 => '1955',
          55 => '1956',
          56 => '1957',
          57 => '1958',
          58 => '1959',
          59 => '1960',
          60 => '1961',
          61 => '1962',
          62 => '1963',
          63 => '1964',
          64 => '1965',
          65 => '1966',
          66 => '1967',
          67 => '1968',
          68 => '1969',
          69 => '1970',
          70 => '1971',
          71 => '1972',
          72 => '1973',
          73 => '1974',
          74 => '1975',
          75 => '1976',
          76 => '1977',
          77 => '1978',
          78 => '1979',
          79 => '1980',
          80 => '1981',
          81 => '1982',
          82 => '1983',
          83 => '1984',
          84 => '1985',
          85 => '1986',
          86 => '1987',
          87 => '1988',
          88 => '1989',
          89 => '1990',
          90 => '1991',
          91 => '1992',
          92 => '1993',
          93 => '1994',
          94 => '1995',
          95 => '1996',
          96 => '1997',
          97 => '1998',
          98 => '1999',
          99 => '2000',
          100 => '2001',
          101 => '2002',
          102 => '2003',
          103 => '2004',
          104 => '2005',
        ),
      ),
    ),
    4 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BELFAST',
          1 => 'BERWICK ON TWEED',
          2 => 'BIRMINGHAM',
          3 => 'BLACKBURN WITH DARWEN',
          4 => 'BOLTON',
          5 => 'BRADFORD',
          6 => 'BURNLEY',
          7 => 'CRAVEN',
          8 => 'DVA (NI)',
          9 => 'GUERNSEY',
          10 => 'HALTON',
          11 => 'HYNDBURN',
          12 => 'ISLE OF MAN',
          13 => 'KEIGHLEY',
          14 => 'KIRKLEES',
          15 => 'KNOWSLEY',
          16 => 'LEEDS',
          17 => 'LIVERPOOL',
          18 => 'LUTON',
          19 => 'MANCHESTER',
          20 => 'NORTHERN IRELAND',
          21 => 'OLDHAM',
          22 => 'PENDLE',
          23 => 'PRESTON',
          24 => 'REDDITCH',
          25 => 'RIBBLE VALLEY',
          26 => 'ROCHDALE',
          27 => 'ROSSENDALE',
          28 => 'RUSHMOOR',
          29 => 'SALFORD',
          30 => 'SANDWELL',
          31 => 'SEFTON',
          32 => 'SLOUGH',
          33 => 'SOUTH BEDFORDSHIRE',
          34 => 'SOUTH BUCKINGHAM',
          35 => 'ST HELENS',
          36 => 'STOCKPORT',
          37 => 'TAMESIDE',
          38 => 'TAMWORTH',
          39 => 'TRAFFORD',
          40 => 'WARRINGTON',
          41 => 'WIRRALL',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
          2 => '4',
          3 => '5',
        ),
      ),
    ),
    6 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B20',
          1 => 'B21',
          2 => 'BB[0-99]*',
          3 => 'BD[0-99]*',
          4 => 'BL[0-99]*',
          5 => 'BT[0-99]*',
          6 => 'E[0-99]*',
          7 => 'L[0-99]*',
          8 => 'LU[0-99]*',
          9 => 'OL[0-99]*',
          10 => 'TD9',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
        ),
      ),
    ),
    8 => 
    array (
      'engine_size' => 
      array (
        'SKP;LIST;;Skip electric vehicles.' => 
        array (
          0 => '0',
          1 => '0 ',
          2 => '0 cc',
          3 => '0 CC',
          4 => '0 E',
          5 => '0 e',
        ),
      ),
    ),
  ),
)


 ?>
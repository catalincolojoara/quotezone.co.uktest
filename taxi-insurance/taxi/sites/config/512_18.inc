<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '512',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '558',
    'companyName' => 'Cover My Cab - Taxi (CMC Central OH)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Cover My Cab - Taxi CMC Central (Quotezone Prime)',
    'limit' => 
    array (
      'day' => '8',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotezone-central@covermycab.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 28 and 69' => '28-69',
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 15' => '15',
      ),
    ),
    4 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '4',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '10',
          1 => '11',
          2 => '12',
          3 => '13',
          4 => '14',
        ),
      ),
    ),
    6 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'LONDON PCO',
        ),
      ),
    ),
    7 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'AB[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BL[0-99]*',
          4 => 'BT[0-99]*',
          5 => 'CW[0-99]*',
          6 => 'DD[0-99]*',
          7 => 'DG[0-99]*',
          8 => 'DH[0-99]*',
          9 => 'DY[0-99]*',
          10 => 'EH[0-99]*',
          11 => 'FK[0-99]*',
          12 => 'FY[0-99]*',
          13 => 'G[0-99]*',
          14 => 'HG[0-99]*',
          15 => 'HS[0-99]*',
          16 => 'HX[0-99]*',
          17 => 'IV[0-99]*',
          18 => 'KA[0-99]*',
          19 => 'KW[0-99]*',
          20 => 'KY[0-99]*',
          21 => 'L[0-99]*',
          22 => 'LS[0-99]*',
          23 => 'M[0-99]*',
          24 => 'ML[0-99]*',
          25 => 'PA[0-99]*',
          26 => 'PH[0-99]*',
          27 => 'SK[0-99]*',
          28 => 'SR[0-99]*',
          29 => 'TD[0-99]*',
          30 => 'TR[0-99]*',
          31 => 'WA[0-99]*',
          32 => 'WF[0-99]*',
          33 => 'ZE[0-99]*',
          34 => 'GY[0-99]*',
          35 => 'JE[0-99]*',
          36 => 'IM[0-99]*',
        ),
      ),
    ),
    8 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 30' => '0 - 30',
      ),
    ),
  ),
)


 ?>
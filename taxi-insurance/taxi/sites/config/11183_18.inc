<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11183',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '89',
    'companyName' => 'Velos - Taxi',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'motor@velosgroup.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '08:30-17:30',
          2 => '08:30-17:30',
          3 => '08:30-17:30',
          4 => '08:30-17:30',
          5 => '08:30-17:30',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 8' => '8',
      ),
    ),
    3 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'AL[0-99]*',
          1 => 'BA[0-99]*',
          2 => 'BH[0-99]*',
          3 => 'BN[0-99]*',
          4 => 'BS[0-99]*',
          5 => 'CB[0-99]*',
          6 => 'CM[0-99]*',
          7 => 'CO[0-99]*',
          8 => 'CT[0-99]*',
          9 => 'DT[0-99]*',
          10 => 'EX[0-99]*',
          11 => 'GL[0-99]*',
          12 => 'GU[0-99]*',
          13 => 'HP[0-99]*',
          14 => 'IP[0-99]*',
          15 => 'LE[0-99]*',
          16 => 'LN[0-99]*',
          17 => 'ME[0-99]*',
          18 => 'MK[0-99]*',
          19 => 'NR[0-99]*',
          20 => 'OX[0-99]*',
          21 => 'PE[0-99]*',
          22 => 'PL[0-99]*',
          23 => 'PO[0-99]*',
          24 => 'RG[0-99]*',
          25 => 'RH[0-99]*',
          26 => 'SG[0-99]*',
          27 => 'SL[0-99]*',
          28 => 'SN[0-99]*',
          29 => 'SO[0-99]*',
          30 => 'SP[0-99]*',
          31 => 'SS[0-99]*',
          32 => 'TA[0-99]*',
          33 => 'TN[0-99]*',
          34 => 'TQ[0-99]*',
          35 => 'TR[0-99]*',
        ),
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'LONDON PCO',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '5',
          1 => '6',
          2 => '7',
          3 => '8',
          4 => '9',
          5 => '10',
          6 => '11',
          7 => '12',
          8 => '13',
        ),
      ),
    ),
  ),
)


 ?>
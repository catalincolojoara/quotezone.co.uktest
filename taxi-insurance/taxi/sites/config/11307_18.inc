<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11307',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '702',
    'companyName' => 'Plan Insurance - Taxi NB Quotezone BC Weekend',
    'offline_from_plugin' => false,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxi.quotes@planinsurance.co.uk',
        1 => 'marketingleads@planinsurance.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => 'SKP_ALL_DAY',
          2 => 'SKP_ALL_DAY',
          3 => 'SKP_ALL_DAY',
          4 => 'SKP_ALL_DAY',
          5 => 'SKP_ALL_DAY',
          6 => '12:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_type' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
    3 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'LONDON PCO',
          1 => 'WESTMINSTER',
        ),
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 100' => '25 - 100',
      ),
    ),
  ),
)


 ?>
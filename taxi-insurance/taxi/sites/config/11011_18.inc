<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11011',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '685',
    'companyName' => 'Lifestyle Insurance - Taxi 3',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '1',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BELFAST',
          1 => 'BLACKBURN WITH DARWEN',
          2 => 'BOLTON',
          3 => 'BRADFORD',
          4 => 'BURNLEY',
          5 => 'BURY',
          6 => 'CALDERDALE',
          7 => 'CHORLEY',
          8 => 'CRAVEN',
          9 => 'DUDLEY',
          10 => 'DVA (NI)',
          11 => 'HYNDBURN',
          12 => 'KIRKLEES',
          13 => 'KNOWSLEY',
          14 => 'LEEDS',
          15 => 'LIVERPOOL',
          16 => 'LONDON PCO',
          17 => 'LUTON',
          18 => 'NEWCASTLE UPON TYNE',
          19 => 'NORTHERN IRELAND',
          20 => 'OLDHAM',
          21 => 'PRESTON',
          22 => 'ROCHESTER',
          23 => 'ROSSENDALE',
          24 => 'SALFORD',
          25 => 'SEFTON',
          26 => 'SHEFFIELD',
          27 => 'SLOUGH',
          28 => 'ST HELENS',
          29 => 'STOCKPORT',
          30 => 'STOCKTON ON TEES',
          31 => 'STOKE ON TRENT',
          32 => 'SUNDERLAND',
          33 => 'TAMESIDE',
          34 => 'THREE RIVERS',
          35 => 'TRAFFORD',
          36 => 'WAKEFIELD',
          37 => 'WALSALL',
          38 => 'WARRINGTON',
          39 => 'WATFORD',
          40 => 'WIGAN',
          41 => 'WIRRALL',
          42 => 'WOLVERHAMPTON',
        ),
      ),
    ),
    1 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BR1-7',
          1 => 'CR0-8',
          2 => 'DA1',
          3 => 'DA5',
          4 => 'DA6',
          5 => 'DA8',
          6 => 'DA14-17',
          7 => 'E1-18',
          8 => 'EC1-4',
          9 => 'EN1-5',
          10 => 'HA0-9',
          11 => 'IG1',
          12 => 'IG2',
          13 => 'IG3',
          14 => 'IG6',
          15 => 'IG8',
          16 => 'IG11',
          17 => 'KT1',
          18 => 'KT2',
          19 => 'KT3',
          20 => 'KT4',
          21 => 'KT6',
          22 => 'KT9',
          23 => 'N1-22',
          24 => 'NW1-11',
          25 => 'RM1-14',
          26 => 'SE1-28',
          27 => 'SM2-7',
          28 => 'SW1-19',
          29 => 'TN14',
          30 => 'TN16',
          31 => 'TW1',
          32 => 'TW3-13',
          33 => 'UB1-10',
          34 => 'W1-14',
          35 => 'WC1-2',
        ),
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 21 and 29' => '21-29',
      ),
    ),
    3 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'SKP;VAL;MIN;Skip if minimum value is 9' => '9',
      ),
    ),
    5 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    6 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0-14',
      ),
    ),
  ),
)


 ?>
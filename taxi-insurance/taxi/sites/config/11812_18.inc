<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11812',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '1030',
    'companyName' => 'Riviera Insurance',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'leads@rivierainsurance.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '04:00-23:59',
          2 => '04:00-23:59',
          3 => '04:00-23:59',
          4 => '04:00-23:59',
          5 => '04:00-23:59',
          6 => '04:00-23:59',
          7 => '04:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
          6 => '15',
          7 => '16',
        ),
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 69' => '25-69',
      ),
    ),
    3 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BD[0-99]*',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
  ),
)


 ?>
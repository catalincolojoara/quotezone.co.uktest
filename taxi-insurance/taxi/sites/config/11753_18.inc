<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11753',
    'siteOrigID' => '',
    'brokerID' => '36',
    'logoID' => '34',
    'companyName' => 'Think Insurance 2',
    'offline_from_plugin' => false,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Taxi (2) Query:',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxiandmbquotes@think-ins.co.uk',
        1 => 'Martin@think-ins.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => 1,
  ),
  'bespoke_filters' => 
  array (
    0 => '$daytimeTelephone  = substr($_SESSION["_YourDetails_"]["daytime_telephone"],0,2);$mobileTelephone   = substr($_SESSION["_YourDetails_"]["mobile_telephone"],0,2);     if($daytimeTelephone != "07" && $mobileTelephone != "07")     $_companyDetails["filter_details"][]["daytime_telephone"]["SKP;VAL;MIN;Accept if one of Telephone number begins with 07 ."] = 1;  ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-19:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    3 => 
    array (
      'type_of_cover' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
          2 => '4',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '8',
          1 => '9',
          2 => '10',
          3 => '11',
          4 => '12',
          5 => '13',
          6 => '14',
          7 => '15',
          8 => '16',
        ),
      ),
    ),
    6 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 64' => '30 - 64',
      ),
    ),
    7 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
          3 => '4',
        ),
      ),
    ),
    8 => 
    array (
      'convictions_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    9 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B66',
          1 => 'BB1',
          2 => 'BB9',
          3 => 'BD5',
          4 => 'BD7',
          5 => 'BD8',
          6 => 'BD9',
          7 => 'BD97',
          8 => 'BD98',
          9 => 'BD99',
          10 => 'BL3',
          11 => 'BL9',
          12 => 'BT[0-99]*',
          13 => 'HS[0-99]*',
          14 => 'L3',
          15 => 'L4',
          16 => 'L6',
          17 => 'L7',
          18 => 'L8',
          19 => 'L11',
          20 => 'L12',
          21 => 'L15',
          22 => 'L25',
          23 => 'L33',
          24 => 'LS7',
          25 => 'M8',
          26 => 'M9',
          27 => 'M11',
          28 => 'M12',
          29 => 'M16',
          30 => 'M19',
          31 => 'OL1',
          32 => 'OL8',
          33 => 'GY[0-99]*',
          34 => 'JE[0-99]*',
          35 => 'IM[0-99]*',
        ),
      ),
    ),
    10 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 2000 and 30000' => '2000 - 30000',
      ),
    ),
    11 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0-14',
      ),
    ),
    12 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 15' => '15',
      ),
    ),
    13 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BARKING',
          1 => 'BARNET',
          2 => 'BELFAST',
          3 => 'BEXLEY',
          4 => 'BIRMINGHAM',
          5 => 'BRENT',
          6 => 'BROMLEY',
          7 => 'CAMDEN',
          8 => 'CROYDEN',
          9 => 'DVA (NI)',
          10 => 'EALING',
          11 => 'ENFIELD',
          12 => 'GREENWICH',
          13 => 'HACKNEY',
          14 => 'HAMMERSMITH',
          15 => 'HARINGEY',
          16 => 'HAVERING',
          17 => 'HILLINGDON',
          18 => 'ISLINGTON',
          19 => 'KENSINGTON',
          20 => 'KINGSTON UPON THAMES ROYAL',
          21 => 'KNOWSLEY',
          22 => 'LEWISHAM',
          23 => 'LIVERPOOL',
          24 => 'LONDON PCO',
          25 => 'MANCHESTER',
          26 => 'MERTON',
          27 => 'NORTHERN IRELAND',
          28 => 'REDBRIDGE',
          29 => 'RICHMOND',
          30 => 'SALFORD',
          31 => 'SOUTHWARK',
          32 => 'SUTTON',
          33 => 'WALTHAM FOREST',
          34 => 'WANDSWORTH',
          35 => 'WESTMINSTER',
          36 => 'WIRRALL',
        ),
      ),
    ),
    14 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    15 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    16 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0-14',
      ),
    ),
  ),
)


 ?>
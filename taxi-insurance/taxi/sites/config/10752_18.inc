<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10752',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '91',
    'companyName' => 'One Answer - QZ047 Taxi (Trident 1)',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'One Answer - QZ047 Taxi (Trident 1)',
    'limit' => 
    array (
      'month' => '666',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'QZ047_lead@1answer.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '10',
          3 => '11',
          4 => '12',
          5 => '13',
          6 => '14',
          7 => '15',
          8 => '16',
        ),
      ),
    ),
    4 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 25000' => '0 - 25000',
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ADUR',
          1 => 'ARUN',
          2 => 'ASHFORD',
          3 => 'BRIGHTON & HOVE',
          4 => 'CRAWLEY',
          5 => 'LEWES',
          6 => 'MID SUSSEX',
          7 => 'REIGATE AND BANSTEAD',
          8 => 'ROTHER',
          9 => 'SEVENOAKS',
          10 => 'TUNBRIDGE WELLS',
          11 => 'WEALDEN',
          12 => 'WORTHING',
        ),
      ),
    ),
    6 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 20 and 69' => ' 20 - 69',
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12366',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => 975,
    'companyName' => 'JB Brokers Ltd - Taxi',
    'offline_from_plugin' => true,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '4',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotes@taxiins.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'AB[0-99]*',
          1 => 'CA[0-99]*',
          2 => 'CW[0-99]*',
          3 => 'DE[0-99]*',
          4 => 'DG[0-99]*',
          5 => 'DT[0-99]*',
          6 => 'EX[0-99]*',
          7 => 'IV[0-99]*',
          8 => 'KA[0-99]*',
          9 => 'LL[0-99]*',
          10 => 'PL[0-99]*',
          11 => 'ST[0-99]*',
          12 => 'SY[0-99]*',
          13 => 'TD[0-99]*',
          14 => 'TQ[0-99]*',
          15 => 'TR[0-99]*',
          16 => 'YO[0-99]*',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 100' => '30-100',
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '5',
          1 => '6',
          2 => '7',
          3 => '8',
          4 => '9',
          5 => '10',
          6 => '11',
          7 => '12',
          8 => '13',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
          5 => '8',
        ),
      ),
    ),
    4 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 10' => '0-10',
      ),
    ),
  ),
)


 ?>
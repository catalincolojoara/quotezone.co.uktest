<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11673',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '493',
    'companyName' => 'Osborne and Sons - Taxi 2',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'tq@os-ins.co.uk',
        1 => 'info@os-ins.co.uk',
        2 => 'donna@os-ins.co.uk',
        3 => 'donnawye@hotmail.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$_companyDetails["filter_details"][]["WS_ID"]["SKP;LIST;;SKIP if webservice source = Danny Imray. "] = array(           0 => "5fdbf8c1a05cd4cdf82ed72104896c7e",        1 => "785e5997d6ca27649d56af14c5d31141",       );',
    1 => '$_companyDetails["filter_details"][]["vehicle_make"]["SKP;LIST;;SKIP if Vehicle make = TESLA"] = array( 0=>"TE",); ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 66' => '25-66',
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    2 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0-14',
      ),
    ),
    3 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    4 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'AYLESBURY VALE',
          1 => 'BASINGSTOKE & DEANE',
          2 => 'BEDFORD',
          3 => 'BRACKNELL FOREST',
          4 => 'BROXBOURNE',
          5 => 'CHERWELL',
          6 => 'CHILTERN',
          7 => 'DACORUM',
          8 => 'EAST HAMPSHIRE',
          9 => 'EAST HERTFORDSHIRE',
          10 => 'GUILDFORD',
          11 => 'HART',
          12 => 'HERTSMERE',
          13 => 'MID BEDFORDSHIRE',
          14 => 'MILTON KEYNES',
          15 => 'NEWBURY',
          16 => 'NORTH HERTFORDSHIRE',
          17 => 'OXFORD',
          18 => 'READING',
          19 => 'RUSHMOOR',
          20 => 'SOUTH OXFORDSHIRE',
          21 => 'ST ALBANS',
          22 => 'STEVENAGE',
          23 => 'SURREY HEATH',
          24 => 'THREE RIVERS',
          25 => 'VALE OF WHITE HORSE',
          26 => 'WATFORD',
          27 => 'WAVERLEY',
          28 => 'WELWYN HATFIELD',
          29 => 'WEST OXFORDSHIRE',
          30 => 'WOKING',
          31 => 'WOKINGHAM',
          32 => 'WYCOMBE',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
          6 => '15',
          7 => '16',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
  ),
)


 ?>
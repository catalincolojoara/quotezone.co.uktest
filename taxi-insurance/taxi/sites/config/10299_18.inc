<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10299',
    'siteOrigID' => '',
    'brokerID' => '75',
    'logoID' => '187',
    'companyName' => 'Connect Insurance 10',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Taxi 10 Query:',
    'limit' => 
    array (
      'day' => '5',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'LeadTX@connect-insurance.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 27 and 74' => '27 - 74',
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
          3 => '10',
          4 => '11',
          5 => '12',
          6 => '13',
          7 => '14',
          8 => '15',
          9 => '16',
        ),
      ),
    ),
    5 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 28' => '0 - 28',
      ),
    ),
    6 => 
    array (
      'taxi_used_for' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
        ),
      ),
    ),
    7 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    8 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEEN',
          1 => 'ABERDEEN CENTRAL (INVERURIE)',
          2 => 'ABERDEEN CITY (ABERDEEN)',
          3 => 'ABERDEENSHIRE',
          4 => 'ABERDEENSHIRE NORTH (BANFF)',
          5 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          6 => 'ADUR',
          7 => 'ALLERDALE',
          8 => 'ALNWICK',
          9 => 'ANGUS',
          10 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          11 => 'AYLESBURY VALE',
          12 => 'BABERGH',
          13 => 'BATH & NORTH EAST SOMERSET',
          14 => 'BERWICK ON TWEED',
          15 => 'BLYTH VALLEY',
          16 => 'BOSTON',
          17 => 'BOURNEMOUTH',
          18 => 'BRECKLAND',
          19 => 'BRIDGENORTH',
          20 => 'BROADLAND',
          21 => 'CARADON',
          22 => 'CARMARTHENSHIRE',
          23 => 'CARRICK',
          24 => 'CASTLE MORPETH',
          25 => 'CEREDIGION',
          26 => 'CHICHESTER',
          27 => 'CLACKMANANSHIRE (ALLOA)',
          28 => 'CONGLETON',
          29 => 'CONGLETON / CHESHIRE EAST',
          30 => 'CONWY',
          31 => 'COTSWOLD',
          32 => 'DARLINGTON',
          33 => 'DENBIGSHIRE',
          34 => 'DUMFRIES & GALLOWAY',
          35 => 'EAST AYRSHIRE (KILMARNOCK)',
          36 => 'EAST DEVON',
          37 => 'EAST DORSET',
          38 => 'EAST LOTHIAN (HADDINGTON)',
          39 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          40 => 'EASTBOURNE',
          41 => 'EASTLEIGH',
          42 => 'EDEN',
          43 => 'EDINBURGH',
          44 => 'FALKIRK',
          45 => 'FIFE COUNCIL (KIRKCALDY)',
          46 => 'FLINTSHIRE',
          47 => 'FOREST HEATH',
          48 => 'FOREST OF DEAN',
          49 => 'GREAT YARMOUTH',
          50 => 'GUERNSEY',
          51 => 'GWYNEDD',
          52 => 'HAMBLETON',
          53 => 'HARROGATE',
          54 => 'HEREFORDSHIRE',
          55 => 'HIGHLAND COUNCIL (INVERNESS)',
          56 => 'ISLE OF ANGLESEY',
          57 => 'ISLE OF MAN',
          58 => 'ISLE OF WIGHT',
          59 => 'KENNET',
          60 => 'KERRIER',
          61 => 'KINGS LYNN & WEST NORFOLK',
          62 => 'LEOMINSTER',
          63 => 'MACCLESFIELD / CHESHIRE EAST',
          64 => 'MALVERN HILLS',
          65 => 'MENDIP',
          66 => 'MID DEVON',
          67 => 'MID SUFFOLK',
          68 => 'MIDLOTHIAN COUNCIL',
          69 => 'MONMOUTHSHIRE',
          70 => 'MORAY (ELGIN)',
          71 => 'NORTH AYRSHIRE (IRVINE)',
          72 => 'NORTH CORNWALL',
          73 => 'NORTH DEVON',
          74 => 'NORTH DORSET',
          75 => 'NORTH LICOLNSHIRE',
          76 => 'NORTH NORFOLK',
          77 => 'NORTH SHROPSHIRE',
          78 => 'NORTH SOMERSET',
          79 => 'NORTH WILTSHIRE',
          80 => 'NORTHUMBERLAND',
          81 => 'ORKNEY ISLANDS (KIRKWALL)',
          82 => 'PEMBROKESHIRE',
          83 => 'PENWITH',
          84 => 'PERTH & KINROSS',
          85 => 'PLYMOUTH',
          86 => 'POWYS',
          87 => 'PURBECK',
          88 => 'RESTORMEL',
          89 => 'RUTLAND',
          90 => 'RYEDALE',
          91 => 'SALISBURY',
          92 => 'SCARBOROUGH',
          93 => 'SCOTTISH BORDERS',
          94 => 'SHETLAND ISLANDS (LERWICK)',
          95 => 'SHREWSBURY',
          96 => 'SHREWSBURY & ATCHAM / SHROPSHIRE COUNCIL',
          97 => 'SHROPSHIRE COUNTY',
          98 => 'SOUTH AYRSHIRE (AYR)',
          99 => 'SOUTH GLOUCESTER',
          100 => 'SOUTH LANARKSHIRE (HAMILTON)',
          101 => 'SOUTH LANARKSHIRE (LANARK)',
          102 => 'SOUTH LANARKSHIRE (RUTHERGLEN)',
          103 => 'SOUTH NORTHAMPTONSHIRE',
          104 => 'SOUTH SHROPSHIRE',
          105 => 'SOUTH SOMERSET',
          106 => 'SPELTHORNE',
          107 => 'ST EDMUNDSBURY',
          108 => 'STIRLING',
          109 => 'STROUD',
          110 => 'SUFFOLK COASTAL',
          111 => 'TAUNTON DEANE',
          112 => 'TEIGNBRIDGE',
          113 => 'TELFORD & WREKIN',
          114 => 'TEWKESBURY',
          115 => 'TORBAY',
          116 => 'TORFAEN',
          117 => 'TORRIDGE',
          118 => 'TYNEDALE',
          119 => 'WANSBECK',
          120 => 'WAVENEY',
          121 => 'WEST DEVON',
          122 => 'WEST DORSET',
          123 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          124 => 'WEST LINDSEY',
          125 => 'WEST SOMERSET',
          126 => 'WEST WILTSHIRE',
          127 => 'WESTERN ISLES (STORNOWAY)',
          128 => 'WEYMOUTH & PORTLAND',
          129 => 'WILTSHIRE COUNTY',
          130 => 'WORCESTER',
          131 => 'WREKIN',
          132 => 'WYCHAVON',
          133 => 'YORK',
        ),
      ),
    ),
    9 => 
    array (
      'registration_number_question' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'N',
        ),
      ),
    ),
  ),
)


 ?>
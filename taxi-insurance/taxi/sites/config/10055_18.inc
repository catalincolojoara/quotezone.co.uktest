<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10055',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => 632,
    'companyName' => 'Xtracover Insurance',
    'offline_from_plugin' => true,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '5',
      'hour' => '1',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Leads@zeeniya.com',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$DISdd   = $_SESSION["_YourDetails_"]["date_of_insurance_start_dd"]; $DISmm   = $_SESSION["_YourDetails_"]["date_of_insurance_start_mm"]; $DISyyyy = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"]; $day   = date("d"); $month = date("m"); $year  = date("Y"); $daysOfInsuranceStart = (mktime(0,0,0,$DISmm,$DISdd,$DISyyyy) - mktime(0,0,0,$month,$day,$year)) / 86400; $timeFilter    = ""; $dateDdText    = date("N"); switch($dateDdText) {case "1": case "2": case "3": case "4": case "5": {if($daysOfInsuranceStart > 20) $timeFilter = "skip"; } break; } if($timeFilter == "skip") $_companyDetails["filter_details"][]["BSK_SKP_WRK_DAYS_DIS"]["ACC;VAL;EQL;IF Quote Day = Monday, Tuesday, Wednesday, Thursday Friday then Insurance Start Date must be between 1 - 20 days (inclusive) away from Quote Date."]  = "BSK_SKP"; ',
    1 => '$DISdd   = $_SESSION["_YourDetails_"]["date_of_insurance_start_dd"]; $DISmm   = $_SESSION["_YourDetails_"]["date_of_insurance_start_mm"]; $DISyyyy = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"]; $day   = date("d"); $month = date("m"); $year  = date("Y"); $daysOfInsuranceStart = (mktime(0,0,0,$DISmm,$DISdd,$DISyyyy) - mktime(0,0,0,$month,$day,$year)) / 86400; $timeFilter    = ""; $dateDdText    = date("N"); switch($dateDdText) {case "6": case "7": {if($daysOfInsuranceStart < 3 || $daysOfInsuranceStart > 20) $timeFilter = "skip"; } break; } if($timeFilter == "skip") $_companyDetails["filter_details"][]["BSK_SKP_WRK_WEEK_DIS"]["ACC;VAL;EQL;IF Quote Day = Saturday, Sunday then Insurance Start Date must be between 3 - 20 days (inclusive) away from Quote Date."]  = "BSK_SKP"; ',
    2 => '$_companyDetails["filter_details"][]["vehicle_make"]["ACC;LIST;;ACCEPT if Vehicle Make = BMW CHEVROLET CHRYSLER CITROEN FORD HONDA HYUNDAI KIA MERCEDES MERCEDES-BENZ NISSAN PEUGEOT RENAULT SEAT SKODA TOYOTA VAUXHALL VOLKSWAGEN VOLVO"] = array(0=>"BM", 1=>"CH", 2=>"CB", 3=>"CN", 4=>"FO", 5=>"HD", 6=>"HY", 7=>"KA", 8=>"MC", 9=>"NA", 10=>"PU", 11=>"RN", 12=>"SE", 13=>"SK", 14=>"TY", 15=>"VX", 16=>"VW", 17=>"VO", ); ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    1 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'CAMDEN',
          1 => 'CRAWLEY',
          2 => 'ELMBRIDGE',
          3 => 'ENFIELD',
          4 => 'EPSOM & EWELL',
          5 => 'HOUNSLOW',
          6 => 'KINGSTON UPON THAMES ROYAL',
          7 => 'LONDON PCO',
          8 => 'WINDSOR & MAIDENHEAD',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
          2 => '4',
          3 => '5',
          4 => '6',
          5 => '7',
        ),
      ),
    ),
    3 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
          3 => '4',
          4 => '5',
          5 => '6',
        ),
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 60' => '25-60',
      ),
    ),
    5 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
  ),
)


 ?>
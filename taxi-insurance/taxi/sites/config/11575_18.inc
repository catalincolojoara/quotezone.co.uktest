<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11575',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '704',
    'companyName' => 'Premier Insurance Centre - Taxi',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'week' => '10',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'jeremy@premierins.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 41 and 60' => '41-60',
      ),
    ),
    1 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'CHESHIRE EAST COUNCIL',
          1 => 'CHESHIRE WEST & CHESTER (BLUE) CHESTER',
          2 => 'CHESHIRE WEST & CHESTER (GREEN) VALE ROYAL',
          3 => 'CHESHIRE WEST & CHESTER (RED) ELLESMERE PORT',
        ),
      ),
    ),
    2 => 
    array (
      'claims_5_years' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'No',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '5',
          1 => '6',
          2 => '7',
          3 => '8',
          4 => '9',
          5 => '10',
        ),
      ),
    ),
  ),
)


 ?>
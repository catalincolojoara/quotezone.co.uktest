<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '9907',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '91',
    'companyName' => 'One Answer - QZ049 SB TAXI',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'One Answer - QZ049 SB TAXI',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'QZ049_lead@1answer.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 40 and 100' => '40-100',
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '12',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '13',
        ),
      ),
    ),
    4 => 
    array (
      'year_of_manufacture' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1901',
          1 => '1902',
          2 => '1903',
          3 => '1904',
          4 => '1905',
          5 => '1906',
          6 => '1907',
          7 => '1908',
          8 => '1909',
          9 => '1910',
          10 => '1911',
          11 => '1912',
          12 => '1913',
          13 => '1914',
          14 => '1915',
          15 => '1916',
          16 => '1917',
          17 => '1918',
          18 => '1919',
          19 => '1920',
          20 => '1921',
          21 => '1922',
          22 => '1923',
          23 => '1924',
          24 => '1925',
          25 => '1926',
          26 => '1927',
          27 => '1928',
          28 => '1929',
          29 => '1930',
          30 => '1931',
          31 => '1932',
          32 => '1933',
          33 => '1934',
          34 => '1935',
          35 => '1936',
          36 => '1937',
          37 => '1938',
          38 => '1939',
          39 => '1940',
          40 => '1941',
          41 => '1942',
          42 => '1943',
          43 => '1944',
          44 => '1945',
          45 => '1946',
          46 => '1947',
          47 => '1948',
          48 => '1949',
          49 => '1950',
          50 => '1951',
          51 => '1952',
          52 => '1953',
          53 => '1954',
          54 => '1955',
          55 => '1956',
          56 => '1957',
          57 => '1958',
          58 => '1959',
          59 => '1960',
          60 => '1961',
          61 => '1962',
          62 => '1963',
          63 => '1964',
          64 => '1965',
          65 => '1966',
          66 => '1967',
          67 => '1968',
          68 => '1969',
          69 => '1970',
          70 => '1971',
          71 => '1972',
          72 => '1973',
          73 => '1974',
          74 => '1975',
          75 => '1976',
          76 => '1977',
          77 => '1978',
          78 => '1979',
          79 => '1980',
          80 => '1981',
          81 => '1982',
          82 => '1983',
          83 => '1984',
          84 => '1985',
          85 => '1986',
          86 => '1987',
          87 => '1988',
          88 => '1989',
          89 => '1990',
          90 => '1991',
          91 => '1992',
          92 => '1993',
          93 => '1994',
          94 => '1995',
          95 => '1996',
          96 => '1997',
          97 => '1998',
          98 => '1999',
        ),
      ),
    ),
    5 => 
    array (
      'private_car_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '8',
          3 => '9',
          4 => '10',
        ),
      ),
    ),
    7 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'AMBER VALLEY',
          1 => 'ANGUS',
          2 => 'ANGUS COUNCIL (FORFAR)',
          3 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          4 => 'ARUN',
          5 => 'ASHFIELD',
          6 => 'ASHFORD',
          7 => 'BABERGH',
          8 => 'BASSETLAW',
          9 => 'BATH & NORTH EAST SOMERSET',
          10 => 'BLABY',
          11 => 'BOLSOVER',
          12 => 'CHARNWOOD',
          13 => 'CHICHESTER',
          14 => 'CHRISTCHURCH',
          15 => 'CORBY',
          16 => 'CORNWALL COUNCIL',
          17 => 'CRAWLEY',
          18 => 'DACORUM',
          19 => 'DARTFORD',
          20 => 'DURHAM',
          21 => 'EAST DEVON',
          22 => 'EAST DORSET',
          23 => 'EDEN',
          24 => 'EXETER',
          25 => 'FENLAND',
          26 => 'GOSPORT',
          27 => 'HAMBLETON',
          28 => 'HASTINGS',
          29 => 'HORSHAM',
          30 => 'ISLE OF WIGHT',
          31 => 'KNOWSLEY',
          32 => 'MID DEVON',
          33 => 'NEWARK & SHERWOOD',
          34 => 'NORTH DEVON',
          35 => 'NORTH DORSET',
          36 => 'NORTH EAST DERBYSHIRE',
          37 => 'NORTH LANARKSHIRE (MOTHERWELL)',
          38 => 'NORTH LANARKSHIRE(SOUTH/CENTRAL)',
          39 => 'NUNEATON & BEDWORTH',
          40 => 'PURBECK',
          41 => 'REIGATE AND BANSTEAD',
          42 => 'SEDGEMOOR',
          43 => 'SHROPSHIRE COUNTY',
          44 => 'SOUTH LANARKSHIRE',
          45 => 'SOUTH LANARKSHIRE (HAMILTON)',
          46 => 'SOUTH LANARKSHIRE (LANARK)',
          47 => 'SOUTH LANARKSHIRE (RUTHERGLEN)',
          48 => 'ST EDMUNDSBURY',
          49 => 'TEIGNBRIDGE',
          50 => 'TENDRING',
          51 => 'TEST VALLEY',
          52 => 'THREE RIVERS',
          53 => 'TORRIDGE',
          54 => 'WEST DEVON',
          55 => 'WEST DORSET',
          56 => 'WEST SOMERSET',
          57 => 'WINCHESTER',
          58 => 'WYRE FORREST',
        ),
      ),
    ),
  ),
)


 ?>
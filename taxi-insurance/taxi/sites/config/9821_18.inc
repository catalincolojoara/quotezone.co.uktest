<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '9821',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '28',
    'companyName' => 'Coversure Huntingdon 2',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'nickru@coversure.co.uk',
        1 => 'richardch@coversure.co.uk',
        2 => 'paulfo@coversure.co.uk',
        3 => 'jamiecl@coversure.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:30-17:00',
          2 => '09:30-17:00',
          3 => '09:30-17:00',
          4 => '09:30-17:00',
          5 => '09:30-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 64' => '25-64',
      ),
    ),
    2 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    3 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
    4 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEEN CENTRAL (INVERURIE)',
          1 => 'ABERDEEN CITY (ABERDEEN)',
          2 => 'ABERDEENSHIRE',
          3 => 'ABERDEENSHIRE NORTH (BANFF)',
          4 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          5 => 'ANGUS',
          6 => 'ANGUS COUNCIL (FORFAR)',
          7 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          8 => 'CAITHNESS (HIGHLANDS - WICK)',
          9 => 'CARLISLE',
          10 => 'CLACKMANANSHIRE (ALLOA)',
          11 => 'CORBY',
          12 => 'DUMFRIES & GALLOWAY',
          13 => 'DUNDEE',
          14 => 'EAST AYRSHIRE (KILMARNOCK)',
          15 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          16 => 'EAST LOTHIAN (HADDINGTON)',
          17 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          18 => 'EDINBURGH',
          19 => 'FALKIRK',
          20 => 'FIFE COUNCIL (KIRKCALDY)',
          21 => 'HIGHLAND COUNCIL (INVERNESS)',
          22 => 'HUNTINGDON',
          23 => 'INVERCLYDE (GREENOCK)',
          24 => 'INVERNESS (HIGHLANDS)',
          25 => 'KETTERING',
          26 => 'MIDLOTHIAN COUNCIL',
          27 => 'MILTON KEYNES',
          28 => 'MORAY (ELGIN)',
          29 => 'NORTH AYRSHIRE (IRVINE)',
          30 => 'ORKNEY ISLANDS (KIRKWALL)',
          31 => 'PERTH & KINROSS',
          32 => 'RENFREWSHIRE (PAISLEY)',
          33 => 'SCOTTISH BORDERS',
          34 => 'SHETLAND ISLANDS (LERWICK)',
          35 => 'SOUTH AYRSHIRE (AYR)',
          36 => 'STIRLING',
          37 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          38 => 'WEST LOTHIAN (LIVINGSTON)',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10191',
    'siteOrigID' => '',
    'brokerID' => '69',
    'logoID' => '654',
    'companyName' => 'High Gear Insurance - Taxi (PP 1)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'High Gear Insurance - Taxi (PP 1)',
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'qz@highgear.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'csv' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-16:30',
          2 => '09:00-16:30',
          3 => '09:00-16:30',
          4 => '09:00-16:30',
          5 => '09:00-16:30',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 74' => ' 25 - 74',
      ),
    ),
    2 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 15' => '15',
      ),
    ),
    4 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 10' => '0 - 10',
      ),
    ),
    5 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '3',
        ),
      ),
    ),
    6 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '6',
          1 => '7',
          2 => '8',
          3 => '9',
          4 => '10',
          5 => '11',
          6 => '12',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '5',
          1 => '6',
          2 => '7',
          3 => '8',
          4 => '9',
          5 => '10',
          6 => '11',
          7 => '12',
          8 => '13',
        ),
      ),
    ),
    8 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 3000 and 35000' => ' 3000 - 35000',
      ),
    ),
    9 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
          3 => '4',
          4 => '5',
          5 => '6',
          6 => '7',
          7 => '8',
          8 => '9',
          9 => '10',
        ),
      ),
    ),
    10 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BD[0-99]*',
          1 => 'BT[0-99]*',
          2 => 'M[0-99]*',
        ),
      ),
    ),
    11 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BIRMINGHAM',
          1 => 'BLACKBURN WITH DARWEN',
          2 => 'BOLTON',
          3 => 'BRADFORD',
          4 => 'CALDERDALE',
          5 => 'LEEDS',
          6 => 'LIVERPOOL',
          7 => 'LONDON PCO',
          8 => 'LUTON',
          9 => 'MANCHESTER',
          10 => 'NEWCASTLE UPON TYNE',
          11 => 'OLDHAM',
          12 => 'ROSSENDALE',
          13 => 'SALFORD',
          14 => 'SANDWELL',
          15 => 'WOLVERHAMPTON',
        ),
      ),
    ),
    12 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
  ),
)


 ?>
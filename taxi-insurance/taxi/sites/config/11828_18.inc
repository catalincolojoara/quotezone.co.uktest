<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11828',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '632',
    'companyName' => 'Xtracover Insurance',
    'offline_from_plugin' => false,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'hour' => '1',
      'day' => '2',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Leads@zeeniya.com',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$DISdd   = $_SESSION["_YourDetails_"]["date_of_insurance_start_dd"]; $DISmm   = $_SESSION["_YourDetails_"]["date_of_insurance_start_mm"]; $DISyyyy = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"]; $day   = date("d"); $month = date("m"); $year  = date("Y"); $daysOfInsuranceStart = (mktime(0,0,0,$DISmm,$DISdd,$DISyyyy) - mktime(0,0,0,$month,$day,$year)) / 86400; $timeFilter    = ""; $dateDdText    = date("N"); switch($dateDdText) {case "1": case "2": case "3": case "4": case "5": {if($daysOfInsuranceStart > 20) $timeFilter = "skip"; } break; } if($timeFilter == "skip") $_companyDetails["filter_details"][]["BSK_SKP_WRK_DAYS_DIS"]["ACC;VAL;EQL;IF Quote Day = Monday, Tuesday, Wednesday, Thursday Friday then Insurance Start Date must be between 1 - 20 days (inclusive) away from Quote Date."]  = "BSK_SKP"; ',
    1 => '$DISdd   = $_SESSION["_YourDetails_"]["date_of_insurance_start_dd"]; $DISmm   = $_SESSION["_YourDetails_"]["date_of_insurance_start_mm"]; $DISyyyy = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"]; $day   = date("d"); $month = date("m"); $year  = date("Y"); $daysOfInsuranceStart = (mktime(0,0,0,$DISmm,$DISdd,$DISyyyy) - mktime(0,0,0,$month,$day,$year)) / 86400; $timeFilter    = ""; $dateDdText    = date("N"); switch($dateDdText) {case "6": case "7": {if($daysOfInsuranceStart < 3 || $daysOfInsuranceStart > 20) $timeFilter = "skip"; } break; } if($timeFilter == "skip") $_companyDetails["filter_details"][]["BSK_SKP_WRK_WEEK_DIS"]["ACC;VAL;EQL;IF Quote Day = Saturday, Sunday then Insurance Start Date must be between 3 - 20 days (inclusive) away from Quote Date."]  = "BSK_SKP"; ',
    2 => '$_companyDetails["filter_details"][]["vehicle_make"]["ACC;LIST;;ACCEPT if Vehicle Make list EB2-61142"] = array( 0=>"AU", 1=>"BM", 2=>"CH", 3=>"CB", 4=>"CN", 5=>"FO", 6=>"HD", 7=>"HY", 8=>"KA", 9=>"MC", 10=>"NA", 11=>"PU", 12=>"RN", 13=>"SE", 14=>"SK", 15=>"TY", 16=>"VX", 17=>"VW", 18=>"VO", ); ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-23:59',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 60' => '25-60',
      ),
    ),
    2 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    3 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BIRMINGHAM',
          1 => 'CAMDEN',
          2 => 'CRAWLEY',
          3 => 'ELMBRIDGE',
          4 => 'ENFIELD',
          5 => 'EPSOM & EWELL',
          6 => 'HOUNSLOW',
          7 => 'KINGSTON UPON THAMES ROYAL',
          8 => 'LONDON PCO',
          9 => 'WINDSOR & MAIDENHEAD',
        ),
      ),
    ),
    4 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
  ),
)


 ?>
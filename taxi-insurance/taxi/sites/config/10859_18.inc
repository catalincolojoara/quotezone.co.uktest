<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10859',
    'siteOrigID' => '',
    'brokerID' => '33',
    'logoID' => '91',
    'companyName' => 'One Answer - QZ044 QZ EQ Taxi',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'One Answer - QZ044 QZ EQ Taxi',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'QZ044_lead@1answer.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-18:00',
          6 => '00:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
          5 => '9',
          6 => '10',
          7 => '11',
          8 => '12',
        ),
      ),
    ),
    2 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    4 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 2500 and 100000' => '2500 - 100000',
      ),
    ),
    5 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '3',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    7 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BARKING',
          1 => 'BARNET',
          2 => 'BARNSLEY',
          3 => 'BELFAST',
          4 => 'BEXLEY',
          5 => 'BIRMINGHAM',
          6 => 'BLACKBURN WITH DARWEN',
          7 => 'BLACKPOOL',
          8 => 'BOLTON',
          9 => 'BRADFORD',
          10 => 'BRENT',
          11 => 'BROMLEY',
          12 => 'BROMSGROVE',
          13 => 'BURY',
          14 => 'CALDERDALE',
          15 => 'CAMDEN',
          16 => 'CHESTERFIELD',
          17 => 'COVENTRY',
          18 => 'CROYDEN',
          19 => 'DARTFORD',
          20 => 'DERBY',
          21 => 'DUDLEY',
          22 => 'EALING',
          23 => 'ENFIELD',
          24 => 'GEDLING',
          25 => 'GREENWICH',
          26 => 'HACKNEY',
          27 => 'HALTON',
          28 => 'HAMMERSMITH',
          29 => 'HARINGEY',
          30 => 'HAVERING',
          31 => 'HILLINGDON',
          32 => 'HOUNSLOW',
          33 => 'ISLINGTON',
          34 => 'KENSINGTON',
          35 => 'KINGSTON UPON THAMES ROYAL',
          36 => 'KIRKLEES',
          37 => 'KNOWSLEY',
          38 => 'LAMBETH',
          39 => 'LEEDS',
          40 => 'LEICESTER',
          41 => 'LEWISHAM',
          42 => 'LIVERPOOL',
          43 => 'LONDON PCO',
          44 => 'LUTON',
          45 => 'MANCHESTER',
          46 => 'MERTON',
          47 => 'MIDDLESBOROUGH',
          48 => 'MILTON KEYNES',
          49 => 'NEWCASTLE UPON TYNE',
          50 => 'NEWHAM',
          51 => 'NORTHERN IRELAND',
          52 => 'NOTTINGHAM',
          53 => 'OLDHAM',
          54 => 'PRESTON',
          55 => 'PSV EASTERN',
          56 => 'PSV JERSEY',
          57 => 'PSV NORTH EAST',
          58 => 'PSV NORTH WEST',
          59 => 'REDBRIDGE',
          60 => 'RICHMOND',
          61 => 'ROCHDALE',
          62 => 'ROSSENDALE',
          63 => 'SALFORD',
          64 => 'SANDWELL',
          65 => 'SEFTON',
          66 => 'SHEFFIELD',
          67 => 'SLOUGH',
          68 => 'SOLIHULL',
          69 => 'SOUTH TYNESIDE',
          70 => 'SOUTHWARK',
          71 => 'STOCKPORT',
          72 => 'SUNDERLAND',
          73 => 'SUTTON',
          74 => 'TAMWORTH',
          75 => 'THURROCK',
          76 => 'TOWER HAMLETS',
          77 => 'TRAFFORD',
          78 => 'WAKEFIELD',
          79 => 'WALSALL',
          80 => 'WALTHAM FOREST',
          81 => 'WANDSWORTH',
          82 => 'WARRINGTON',
          83 => 'WATFORD',
          84 => 'WESTMINSTER',
          85 => 'WIGAN',
          86 => 'WIRRALL',
          87 => 'WOLVERHAMPTON',
        ),
      ),
    ),
    8 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    9 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 65' => ' 25 - 65',
      ),
    ),
    10 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
  ),
)


 ?>
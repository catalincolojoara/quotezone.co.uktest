<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10638',
    'siteOrigID' => '',
    'brokerID' => '82',
    'logoID' => '685',
    'companyName' => 'Insurance Online - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '1',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxileads@insuranceonline.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '  $vehicleMake = $_SESSION["_YourDetails_"]["vehicle_make"]; $vehicleModel = $_SESSION["_YourDetails_"]["vehicle_model"]; $yearOfManufacture = $_SESSION["_YourDetails_"]["year_of_manufacture"]; $flag = "true"; $currentYear = date("Y"); $makeArray = array(  "MT" => array (15),  "PU" => array ("E7" =>7, "EURO7TAXI" =>7, "PARTNER" =>3, "PREMIER" =>3),  "L2" => array ("TX4" =>15, "TXI" =>15, "TXII" =>15),  "NA" => array ("NV200" =>15),  "MC" => array ("VITO" =>7),  "CN" => array ("BERLINGO" =>3, "DISPATCH" =>3),  "FO" => array ("TOURNEO" =>3, "TRANSIT" =>3, "CONNECT" =>3, "GALAXY" =>3),  "SE" => array ("ALHAMBRA" =>3),  "VW" => array ("SHARAN" =>3, "CARAVELLE" =>3, "CADDY" =>3, "T/PORTER" =>3, "T/RPORTER" =>3)  );  foreach($makeArray as $makeArrayKey =>$makeArrayVal)  { 	if($makeArrayKey == $vehicleMake) 	{ 	    foreach($makeArray[$vehicleMake] as $makeArrayValKey =>$makeArrayValVal) 	    { 		    $yearDiff = $currentYear - $makeArrayValVal; 			if($vehicleMake == "MT") 			{                 if($yearDiff <= $yearOfManufacture) 				{ 					$flag = "false"; 						break; 				} 			} 			else if($vehicleModel == $makeArrayValKey) 			{ 				if($yearDiff <= $yearOfManufacture) 				{ 					$flag = "false"; 						break; 				} 			}	 	    } 	}  }  if($flag == "true") 	 $_companyDetails["filter_details"][]["vehicle_make"]["ACC;LIST;;Accept make, model and year of manufacture list"] = array (0 => "789_c",);    ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '4',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    2 => 
    array (
      'private_car_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    3 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEENSHIRE',
          1 => 'BIRMINGHAM',
          2 => 'BRIGHTON & HOVE',
          3 => 'CARDIFF',
          4 => 'COVENTRY',
          5 => 'DERBY',
          6 => 'DUNDEE',
          7 => 'EDINBURGH',
          8 => 'HALTON',
          9 => 'KINGSTON-UPON-HULL',
          10 => 'LANCASTER',
          11 => 'LEEDS',
          12 => 'LEICESTER',
          13 => 'NEWCASTLE UPON TYNE',
          14 => 'NEWPORT',
          15 => 'NORWICH',
          16 => 'NOTTINGHAM',
          17 => 'OXFORD',
          18 => 'PETERBOROUGH',
          19 => 'PLYMOUTH',
          20 => 'PRESTON',
          21 => 'SHEFFIELD',
          22 => 'SOUTH BUCKINGHAM',
          23 => 'SOUTHAMPTON',
          24 => 'ST ALBANS',
          25 => 'STOKE ON TRENT',
          26 => 'SWANSEA',
          27 => 'WAKEFIELD',
          28 => 'YORK',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10111',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => 642,
    'companyName' => 'Jenkinson Insurance',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
        1 => 'michael.kirk@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Craig@jenkinsoninsurance.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 35 and 65' => '35-65',
      ),
    ),
    1 => 
    array (
      'private_car_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '5',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
        ),
      ),
    ),
    3 => 
    array (
      'year_of_manufacture' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '2005',
          1 => '2006',
          2 => '2007',
          3 => '2008',
          4 => '2009',
          5 => '2010',
          6 => '2011',
          7 => '2012',
          8 => '2013',
          9 => '2014',
          10 => '2015',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'AB[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BL[0-99]*',
          4 => 'BT[0-99]*',
          5 => 'CA[0-99]*',
          6 => 'CH[0-99]*',
          7 => 'CW[0-99]*',
          8 => 'DD[0-99]*',
          9 => 'DG[0-99]*',
          10 => 'DH[0-99]*',
          11 => 'DL[0-99]*',
          12 => 'EH[0-99]*',
          13 => 'FK[0-99]*',
          14 => 'FY[0-99]*',
          15 => 'G[0-99]*',
          16 => 'HD[0-99]*',
          17 => 'HS[0-99]*',
          18 => 'HX[0-99]*',
          19 => 'IV[0-99]*',
          20 => 'KA[0-99]*',
          21 => 'KW[0-99]*',
          22 => 'KY[0-99]*',
          23 => 'L[0-99]*',
          24 => 'LA[0-99]*',
          25 => 'M[0-99]*',
          26 => 'ML[0-99]*',
          27 => 'NE[0-99]*',
          28 => 'OL[0-99]*',
          29 => 'PA[0-99]*',
          30 => 'PH[0-99]*',
          31 => 'PR[0-99]*',
          32 => 'SK[0-99]*',
          33 => 'SR[0-99]*',
          34 => 'TD[0-99]*',
          35 => 'TS[0-99]*',
          36 => 'WA[0-99]*',
          37 => 'WN[0-99]*',
          38 => 'ZE[0-99]*',
        ),
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'ABERDEENSHIRE',
          1 => 'BELFAST',
          2 => 'BIRMINGHAM',
          3 => 'BRADFORD',
          4 => 'DERBY',
          5 => 'DUDLEY',
          6 => 'DURHAM',
          7 => 'DVA (NI)',
          8 => 'EAST AYRSHIRE (KILMARNOCK)',
          9 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          10 => 'EDINBURGH',
          11 => 'FALKIRK',
          12 => 'GLASGOW',
          13 => 'LEEDS',
          14 => 'LEICESTER',
          15 => 'LIVERPOOL',
          16 => 'LONDON PCO',
          17 => 'MANCHESTER',
          18 => 'NEWCASTLE UPON TYNE',
          19 => 'NORTHERN IRELAND',
          20 => 'PSV NORTH WEST',
          21 => 'PSV SCOTLAND',
          22 => 'REDCAR & CLEVELAND',
          23 => 'ROSS & CROMARTY (HIGHLAND - DINGWALL)',
          24 => 'SOLIHULL',
          25 => 'SOUTH AYRSHIRE (AYR)',
          26 => 'SUNDERLAND',
          27 => 'WAKEFIELD',
          28 => 'WARRINGTON',
          29 => 'WOLVERHAMPTON',
        ),
      ),
    ),
  ),
)


 ?>
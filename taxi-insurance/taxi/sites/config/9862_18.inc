<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '9862',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => 604,
    'companyName' => 'Insurance 4 Instructors',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'info@insurance-centre.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 60' => '30-60',
      ),
    ),
    2 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BRECKLAND',
          1 => 'BROADLAND',
          2 => 'FOREST HEATH',
          3 => 'GREAT YARMOUTH',
          4 => 'KINGS LYNN & W NORFOLK',
          5 => 'MID SUFFOLK',
          6 => 'NORTH NORFOLK',
          7 => 'NORWICH',
          8 => 'SOUTH NORFOLK',
          9 => 'ST EDMUNDSBURY',
          10 => 'WAVENEY',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
        ),
      ),
    ),
  ),
)


 ?>
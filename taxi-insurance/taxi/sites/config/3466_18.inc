<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '3466',
    'siteOrigID' => '',
    'brokerID' => '69',
    'logoID' => '654',
    'companyName' => 'High Gear Insurance - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
    ),
    'emailSubject' => 'High Gear Insurance - Taxi',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'qz@highgear.co.uk',
        1 => 's.trinder@highgear.co.uk',
        2 => 'alex.drummond@highgear.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'csv' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '4',
        ),
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '5',
          1 => '6',
          2 => '7',
          3 => '8',
          4 => '9',
          5 => '10',
          6 => '11',
          7 => '12',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '5',
          1 => '6',
          2 => '7',
          3 => '8',
          4 => '9',
          5 => '10',
          6 => '11',
          7 => '12',
          8 => '13',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
        ),
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BIRMINGHAM',
          1 => 'BRADFORD',
          2 => 'NORTHERN IRELAND',
        ),
      ),
    ),
    6 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
        ),
      ),
    ),
    7 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    8 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 10' => '0 - 10',
      ),
    ),
    9 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 68' => ' 25 - 68',
      ),
    ),
    10 => 
    array (
      'private_car_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12645',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '702',
    'companyName' => 'Plan Insurance',
    'offline_from_plugin' => false,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'marketingleads@planinsurance.co.uk',
        1 => 'taxisales@planinsurance.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),  
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|17:30-23:59',
          2 => '00:00-09:00|17:30-23:59',
          3 => '00:00-09:00|17:30-23:59',
          4 => '00:00-09:00|17:30-23:59',
          5 => '00:00-09:00',
          6 => '12:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
        ),
      ),
    ),
    2 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 15000 and 100000' => '15000 - 100000',
      ),
    ),
    3 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 9' => '9',
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 28 and 70' => '28-70',
      ),
    ),
    5 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
        ),
      ),
    ),
    6 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BL[0-99]*',
          4 => 'CH[0-99]*',
          5 => 'E[0-99]*',
          6 => 'EC[0-99]*',
          7 => 'HX[0-99]*',
          8 => 'L[0-99]*',
          9 => 'LS[0-99]*',
          10 => 'LU[0-99]*',
          11 => 'M[0-99]*',
          12 => 'OL[0-99]*',
          13 => 'PR[0-99]*',
          14 => 'WD[0-99]*',
          15 => 'WN[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
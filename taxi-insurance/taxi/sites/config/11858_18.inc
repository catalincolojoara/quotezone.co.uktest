<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11858',
    'siteOrigID' => '',
    'brokerID' => '33',
    'logoID' => '1030',
    'companyName' => 'Riviera Insurance (Sc and Wa)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'leads@rivierainsurance.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    1 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0-14',
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    3 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEEN',
          1 => 'ABERDEEN CENTRAL (INVERURIE)',
          2 => 'ABERDEEN CITY (ABERDEEN)',
          3 => 'ABERDEENSHIRE',
          4 => 'ABERDEENSHIRE NORTH (BANFF)',
          5 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          6 => 'BLAENAU GWENT',
          7 => 'CAERPHILLY',
          8 => 'CARMARTHENSHIRE',
          9 => 'CEREDIGION',
          10 => 'CLACKMANANSHIRE (ALLOA)',
          11 => 'DUMFRIES & GALLOWAY',
          12 => 'DUNDEE',
          13 => 'EAST AYRSHIRE (KILMARNOCK)',
          14 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          15 => 'EAST LOTHIAN (HADDINGTON)',
          16 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          17 => 'FALKIRK',
          18 => 'FIFE COUNCIL (KIRKCALDY)',
          19 => 'GWYNEDD',
          20 => 'HIGHLAND COUNCIL (INVERNESS)',
          21 => 'INVERCLYDE (GREENOCK)',
          22 => 'INVERNESS (HIGHLANDS)',
          23 => 'LOCHABER (HIGHLAND - FORT WILLIAM)',
          24 => 'MORAY (ELGIN)',
          25 => 'NAIRN (HIGHLANDS)',
          26 => 'NEATH & PORT TALBOT',
          27 => 'PEMBROKESHIRE',
          28 => 'RHONDDA CYNON TAFF',
          29 => 'ROSS & CROMARTY (HIGHLAND - DINGWALL)',
          30 => 'SCOTTISH BORDERS',
          31 => 'SKYE & LOCHALSH (HIGHLAND - ISLE OF SKYE)',
          32 => 'SOUTH AYRSHIRE (AYR)',
          33 => 'SOUTH LANARKSHIRE',
          34 => 'SOUTH LANARKSHIRE (HAMILTON)',
          35 => 'SOUTH LANARKSHIRE (LANARK)',
          36 => 'SOUTH LANARKSHIRE (RUTHERGLEN)',
          37 => 'STIRLING',
          38 => 'VALE OF GLAMORGAN',
          39 => 'WEST LOTHIAN (LIVINGSTON)',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
          6 => '15',
          7 => '16',
        ),
      ),
    ),
  ),
)


 ?>
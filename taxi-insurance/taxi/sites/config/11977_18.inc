<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11977',
    'siteOrigID' => '',
    'brokerID' => '99',
    'logoID' => '807',
    'companyName' => 'Flag Insurance Brokers',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotes@forhireinsurance.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => 'SKP_ALL_DAY',
          2 => 'SKP_ALL_DAY',
          3 => 'SKP_ALL_DAY',
          4 => 'SKP_ALL_DAY',
          5 => 'SKP_ALL_DAY',
          6 => '00:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'LU[0-99]*',
        ),
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
          3 => '10',
          4 => '11',
          5 => '12',
        ),
      ),
    ),
    3 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'AYLESBURY VALE',
          1 => 'BASINGSTOKE & DEANE',
          2 => 'BOURNEMOUTH',
          3 => 'BRACKNELL FOREST',
          4 => 'BRIGHTON & HOVE',
          5 => 'CHICHESTER',
          6 => 'CHILTERN',
          7 => 'CHRISTCHURCH',
          8 => 'CRAWLEY',
          9 => 'DACORUM',
          10 => 'EAST HAMPSHIRE',
          11 => 'EASTLEIGH',
          12 => 'ELMBRIDGE',
          13 => 'EPSOM & EWELL',
          14 => 'FAREHAM',
          15 => 'GOSPORT',
          16 => 'GUILDFORD',
          17 => 'HART',
          18 => 'HAVANT',
          19 => 'HERTSMERE',
          20 => 'HORSHAM',
          21 => 'ISLE OF WIGHT',
          22 => 'KINGSTON UPON THAMES ROYAL',
          23 => 'LEWES',
          24 => 'MID SUSSEX',
          25 => 'MILTON KEYNES',
          26 => 'MOLE VALLEY',
          27 => 'NEW FOREST',
          28 => 'NEWBURY',
          29 => 'OXFORD',
          30 => 'PORTSMOUTH',
          31 => 'READING',
          32 => 'REIGATE AND BANSTEAD',
          33 => 'RUNNYMEDE',
          34 => 'RUSHMOOR',
          35 => 'SALISBURY',
          36 => 'SLOUGH',
          37 => 'SOUTH BUCKINGHAM',
          38 => 'SOUTH OXFORDSHIRE',
          39 => 'SOUTHAMPTON',
          40 => 'SPELTHORNE',
          41 => 'ST ALBANS',
          42 => 'SURREY HEATH',
          43 => 'SWINDON',
          44 => 'TANDRIDGE',
          45 => 'TEST VALLEY',
          46 => 'THREE RIVERS',
          47 => 'VALE OF WHITE HORSE',
          48 => 'WATFORD',
          49 => 'WAVERLEY',
          50 => 'WELWYN HATFIELD',
          51 => 'WEST BERKSHIRE',
          52 => 'WEST OXFORDSHIRE',
          53 => 'WILTSHIRE COUNTY',
          54 => 'WINCHESTER',
          55 => 'WINDSOR & MAIDENHEAD',
          56 => 'WOKING',
          57 => 'WOKINGHAM',
          58 => 'WORTHING',
          59 => 'WYCOMBE',
        ),
      ),
    ),
  ),
)


 ?>
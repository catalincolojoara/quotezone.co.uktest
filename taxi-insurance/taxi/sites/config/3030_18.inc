<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '3030',
    'siteOrigID' => '',
    'brokerID' => '60',
    'logoID' => '51',
    'companyName' => 'XYZ - Taxi (4)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '1',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotes@xyzinsurance.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$_companyDetails["filter_details"][]["WLID_NO"]["SKP;LIST;;skip if AffiliateID = DNAInsurance2. "] = array(0 => "758",);',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 60' => '25-60',
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '8',
          1 => '9',
          2 => '10',
          3 => '11',
          4 => '12',
          5 => '13',
          6 => '14',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'SKP;VAL;MIN;Skip if minimum value is 9' => '9',
      ),
    ),
    7 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BASILDON',
          1 => 'BRAINTREE',
          2 => 'BRENTWOOD',
          3 => 'CASTLE POINT',
          4 => 'CHELMSFORD',
          5 => 'COLCHESTER',
          6 => 'EPPING FOREST',
          7 => 'HARLOW',
          8 => 'MALDON',
          9 => 'ROCHFORD',
          10 => 'SOUTHEND-ON-SEA',
          11 => 'TENDRING',
          12 => 'THURROCK',
          13 => 'UTTLESFORD',
        ),
      ),
    ),
    8 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    9 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 21' => '0-21',
      ),
    ),
  ),
)


 ?>
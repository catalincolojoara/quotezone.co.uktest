<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '3192',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '721',
    'companyName' => ' Patons Scotland',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '20',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxileads@patonsinsurance.co.uk',
        1 => 'taxi@patonsquotes.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
          5 => '8',
          6 => '9',
          7 => '10',
          8 => '11',
          9 => '12',
        ),
      ),
    ),
    2 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 23' => '0 - 23',
      ),
    ),
    3 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEEN',
          1 => 'ABERDEEN CENTRAL (INVERURIE)',
          2 => 'ABERDEEN CITY (ABERDEEN)',
          3 => 'ABERDEENSHIRE',
          4 => 'ANGUS',
          5 => 'ANGUS COUNCIL (FORFAR)',
          6 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          7 => 'CLACKMANANSHIRE (ALLOA)',
          8 => 'DUMFRIES & GALLOWAY',
          9 => 'DUNDEE',
          10 => 'EAST AYRSHIRE (KILMARNOCK)',
          11 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          12 => 'EAST KILBRIDE',
          13 => 'EAST LOTHIAN (HADDINGTON)',
          14 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          15 => 'EDINBURGH',
          16 => 'FALKIRK',
          17 => 'FIFE COUNCIL (KIRKCALDY)',
          18 => 'GLASGOW CITY',
          19 => 'INVERCLYDE (GREENOCK)',
          20 => 'INVERNESS (HIGHLANDS)',
          21 => 'MIDLOTHIAN COUNCIL',
          22 => 'MORAY (ELGIN)',
          23 => 'NORTH AYRSHIRE (IRVINE)',
          24 => 'NORTH LANARKSHIRE (MOTHERWELL)',
          25 => 'NORTH LANARKSHIRE(SOUTH/CENTRAL)',
          26 => 'ORKNEY ISLANDS (KIRKWALL)',
          27 => 'PERTH & KINROSS',
          28 => 'RENFREWSHIRE (PAISLEY)',
          29 => 'SCOTTISH BORDERS',
          30 => 'SHETLAND ISLANDS (LERWICK)',
          31 => 'SOUTH AYRSHIRE (AYR)',
          32 => 'SOUTH LANARKSHIRE',
          33 => 'STIRLING',
          34 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          35 => 'WEST LOTHIAN (LIVINGSTON)',
          36 => 'WESTERN ISLES (STORNOWAY)',
        ),
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 21 and 75' => '21 - 75',
      ),
    ),
  ),
)


 ?>
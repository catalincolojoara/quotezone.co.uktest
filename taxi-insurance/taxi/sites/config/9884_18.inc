<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '9884',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '91',
    'companyName' => 'One Answer - QZ007 QZ U9 Taxi',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'One Answer - QZ007 QZ U9 Taxi',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'QZ007_lead@1answer.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$claims5Years=$_SESSION["_YourDetails_"]["claims_5_years"]; if($claims5Years == "No") $_companyDetails["filter_details"][]["convictions_5_years"]["SKP;VAL;EQL;Skip if Claims last 5 years = No Claims and Motoring convictions last 5 years = No Convictions ."]  = "No"; ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    1 => 
    array (
      'year_of_manufacture' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1901',
          1 => '1902',
          2 => '1903',
          3 => '1904',
          4 => '1905',
          5 => '1906',
          6 => '1907',
          7 => '1908',
          8 => '1909',
          9 => '1910',
          10 => '1911',
          11 => '1912',
          12 => '1913',
          13 => '1914',
          14 => '1915',
          15 => '1916',
          16 => '1917',
          17 => '1918',
          18 => '1919',
          19 => '1920',
          20 => '1921',
          21 => '1922',
          22 => '1923',
          23 => '1924',
          24 => '1925',
          25 => '1926',
          26 => '1927',
          27 => '1928',
          28 => '1929',
          29 => '1930',
          30 => '1931',
          31 => '1932',
          32 => '1933',
          33 => '1934',
          34 => '1935',
          35 => '1936',
          36 => '1937',
          37 => '1938',
          38 => '1939',
          39 => '1940',
          40 => '1941',
          41 => '1942',
          42 => '1943',
          43 => '1944',
          44 => '1945',
          45 => '1946',
          46 => '1947',
          47 => '1948',
          48 => '1949',
          49 => '1950',
          50 => '1951',
          51 => '1952',
          52 => '1953',
          53 => '1954',
          54 => '1955',
          55 => '1956',
          56 => '1957',
          57 => '1958',
          58 => '1959',
          59 => '1960',
          60 => '1961',
          61 => '1962',
          62 => '1963',
          63 => '1964',
          64 => '1965',
          65 => '1966',
          66 => '1967',
          67 => '1968',
          68 => '1969',
          69 => '1970',
          70 => '1971',
          71 => '1972',
          72 => '1973',
          73 => '1974',
          74 => '1975',
          75 => '1976',
          76 => '1977',
          77 => '1978',
          78 => '1979',
          79 => '1980',
          80 => '1981',
          81 => '1982',
          82 => '1983',
          83 => '1984',
          84 => '1985',
          85 => '1986',
          86 => '1987',
          87 => '1988',
          88 => '1989',
          89 => '1990',
          90 => '1991',
          91 => '1992',
          92 => '1993',
          93 => '1994',
          94 => '1995',
          95 => '1996',
          96 => '1997',
          97 => '1998',
          98 => '1999',
          99 => '2000',
          100 => '2001',
          101 => '2002',
          102 => '2003',
          103 => '2004',
          104 => '2005',
          105 => '2006',
          106 => '2007',
          107 => '2008',
          108 => '2009',
        ),
      ),
    ),
    2 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ADUR',
          1 => 'ALLERDALE',
          2 => 'ARUN',
          3 => 'ASHFORD',
          4 => 'BASINGSTOKE & DEANE',
          5 => 'BATH & NORTH EAST SOMERSET',
          6 => 'BOURNEMOUTH',
          7 => 'BRACKNELL FOREST',
          8 => 'BRIGHTON & HOVE',
          9 => 'BRISTOL',
          10 => 'CAMBRIDGE',
          11 => 'CANTERBURY',
          12 => 'CHELTENHAM',
          13 => 'CHICHESTER',
          14 => 'CHRISTCHURCH',
          15 => 'COLCHESTER',
          16 => 'COTSWOLD',
          17 => 'CRAWLEY',
          18 => 'DOVER',
          19 => 'EAST DEVON',
          20 => 'EAST DORSET',
          21 => 'EAST HAMPSHIRE',
          22 => 'EASTBOURNE',
          23 => 'EASTLEIGH',
          24 => 'ELMBRIDGE',
          25 => 'EPPING FOREST',
          26 => 'EPSOM & EWELL',
          27 => 'EXETER',
          28 => 'FAREHAM',
          29 => 'FENLAND',
          30 => 'FOREST OF DEAN',
          31 => 'GLOUCESTER',
          32 => 'GOSPORT',
          33 => 'GRAVESHAM',
          34 => 'GUILDFORD',
          35 => 'HASTINGS',
          36 => 'HAVANT',
          37 => 'HORSHAM',
          38 => 'IPSWICH',
          39 => 'LEWES',
          40 => 'MAIDSTONE',
          41 => 'MEDWAY',
          42 => 'MID DEVON',
          43 => 'MID SUSSEX',
          44 => 'MOLE VALLEY',
          45 => 'NEW FOREST',
          46 => 'NORTH CORNWALL',
          47 => 'NORTH DEVON',
          48 => 'NORTH DORSET',
          49 => 'NORTH SOMERSET',
          50 => 'NORWICH',
          51 => 'OXFORD',
          52 => 'PETERBOROUGH',
          53 => 'PLYMOUTH',
          54 => 'POOLE',
          55 => 'PORTSMOUTH',
          56 => 'READING',
          57 => 'REIGATE AND BANSTEAD',
          58 => 'ROTHER',
          59 => 'RUNNYMEDE',
          60 => 'SEVENOAKS',
          61 => 'SHROPSHIRE COUNTY',
          62 => 'SOUTH GLOUCESTER',
          63 => 'SOUTH HAMS',
          64 => 'SOUTH OXFORDSHIRE',
          65 => 'SOUTH SOMERSET',
          66 => 'SOUTHAMPTON',
          67 => 'SOUTHEND-ON-SEA',
          68 => 'ST ALBANS',
          69 => 'STROUD',
          70 => 'SUFFOLK COASTAL',
          71 => 'SURREY HEATH',
          72 => 'TANDRIDGE',
          73 => 'TEIGNBRIDGE',
          74 => 'TEST VALLEY',
          75 => 'TONBRIDGE & MALLING',
          76 => 'TORBAY',
          77 => 'TORRIDGE',
          78 => 'TUNBRIDGE WELLS',
          79 => 'WAVENEY',
          80 => 'WAVERLEY',
          81 => 'WEALDEN',
          82 => 'WEST BERKSHIRE',
          83 => 'WEST DEVON',
          84 => 'WEST DORSET',
          85 => 'WEST SOMERSET',
          86 => 'WEYMOUTH & PORTLAND',
          87 => 'WILTSHIRE COUNTY',
          88 => 'WINDSOR & MAIDENHEAD',
          89 => 'WOKING',
          90 => 'WORTHING',
        ),
      ),
    ),
    3 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
          5 => '8',
          6 => '9',
          7 => '10',
          8 => '11',
          9 => '12',
        ),
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 21 and 100' => ' 21 - 100',
      ),
    ),
    5 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
        ),
      ),
    ),
  ),
)


 ?>
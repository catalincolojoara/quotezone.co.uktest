<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '1746',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '870',
    'companyName' => 'Sureplan - Taxi (Scheme 1)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotezone@sureplaninsurance.co.uk',
        1 => 'quotes@spibrokers.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'csv' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '6',
          1 => '7',
          2 => '8',
          3 => '9',
          4 => '10',
          5 => '11',
          6 => '12',
          7 => '13',
        ),
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '5',
          1 => '6',
          2 => '7',
          3 => '8',
          4 => '9',
          5 => '10',
          6 => '11',
          7 => '12',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    4 => 
    array (
      'convictions_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    5 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 100' => '25-100',
      ),
    ),
    6 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 12' => '12',
      ),
    ),
    7 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BA[0-99]*',
          1 => 'BH[0-99]*',
          2 => 'CA[0-99]*',
          3 => 'CM[0-99]*',
          4 => 'CO[0-99]*',
          5 => 'CT[0-99]*',
          6 => 'CW[0-99]*',
          7 => 'DH[0-99]*',
          8 => 'DL[0-99]*',
          9 => 'DN[0-99]*',
          10 => 'DT[0-99]*',
          11 => 'EX[0-99]*',
          12 => 'HG[0-99]*',
          13 => 'HU[0-99]*',
          14 => 'LN[0-99]*',
          15 => 'NE[0-99]*',
          16 => 'SR[0-99]*',
          17 => 'TS[0-99]*',
          18 => 'YO[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11826',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '898',
    'companyName' => 'Complete Cover Group (OOH) (Fare Cover)',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '15',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Comp_Van.QuoteZone@completecovergroup.com',
      ),
    ),
   'TXT' =>
   array (
     'TECHNICAL' =>
        array (
           0 => 'eb2-technical@seopa.com',
        ),
     'MARKETING' =>
        array (
           0 => 'leads@seopa.com',
        ),
     'COMPANY' =>
        array (
           0 => 'Comp_Van.QuoteZone@completecovergroup.com',
        ),
     ),
  ),
  'bespoke_filters' => 
  array (
    0 => 'include_once "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/taxi/steps/functions.php"; $uberDriver = $_SESSION["_YourDetails_"]["uber_driver"]; $pcPrefix = $_SESSION["_YourDetails_"]["postcode_prefix"]; preg_match("/^(.*)\\d+/isU",$pcPrefix,$pcPrefResArray); $pcPrefixLetters = $pcPrefResArray[1]; $pcPrefixLetters = strtoupper($pcPrefixLetters); $accPostcodeArray = array("E","EC","N","NW","SE","SW","W","WC"); $taxiBadge = $_SESSION["_YourDetails_"]["taxi_badge"]; $accTaxiBadgeArr = array("5","6","7","8","9","10","11","12","13"); $DISdd    = $_SESSION["_YourDetails_"]["date_of_insurance_start_dd"]; $DISmm    = $_SESSION["_YourDetails_"]["date_of_insurance_start_mm"]; $DISyyyy  = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"]; $dobDD    = $_SESSION["_YourDetails_"]["date_of_birth_dd"]; $dobMM    = $_SESSION["_YourDetails_"]["date_of_birth_mm"]; $dobYYYY  = $_SESSION["_YourDetails_"]["date_of_birth_yyyy"]; $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd; $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD; $propAge = GetAge($proposerBirthDate,$insuranceStartDate); if(($uberDriver == "Y" && in_array($pcPrefixLetters,$accPostcodeArray))) { if(($propAge <= "35") OR ( !in_array($taxiBadge,$accTaxiBadgeArr)) ) { $_companyDetails["filter_details"][]["BSPK_ACC_UBER_PC_BADGE_AGE"]["ACC;VAL;EQL;Accept ONLY if uber driver, postcode location, taxi  badge and age over 35."]  = "ACC"; } }',
    1 => 'include_once "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/taxi/steps/functions.php"; $uberDriver = $_SESSION["_YourDetails_"]["uber_driver"]; $pcPrefix = $_SESSION["_YourDetails_"]["postcode_prefix"]; preg_match("/^(.*)\\d+/isU",$pcPrefix,$pcPrefResArray); $pcPrefixLetters = $pcPrefResArray[1]; $pcPrefixLetters = strtoupper($pcPrefixLetters); $accPostcodeArray = array("B","BB","BD","CH","CR","DE","DY","G","HA","HD","HX","IG","L","LS","LU","M","NE","NG","OL","PE","RM","S","SK","SL","ST","SW","TW","UB","WF","YO"); $fullUkLicence = $_SESSION["_YourDetails_"]["period_of_licence"]; $DISdd    = $_SESSION["_YourDetails_"]["date_of_insurance_start_dd"]; $DISmm    = $_SESSION["_YourDetails_"]["date_of_insurance_start_mm"]; $DISyyyy  = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"]; $dobDD    = $_SESSION["_YourDetails_"]["date_of_birth_dd"]; $dobMM    = $_SESSION["_YourDetails_"]["date_of_birth_mm"]; $dobYYYY  = $_SESSION["_YourDetails_"]["date_of_birth_yyyy"]; $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd; $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD; $propAge = GetAge($proposerBirthDate,$insuranceStartDate); if(($uberDriver == "Y" && in_array($pcPrefixLetters,$accPostcodeArray))) { if(($propAge <= "35") OR ( $fullUkLicence < "10" ) ) { $_companyDetails["filter_details"][]["period_of_licence"]["ACC;VAL;EQL;Accept ONLY if uber driver, postcode location, full uk licence and age over 35."]  = "ACC"; } }',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '17:00-20:00',
          2 => '17:00-20:00',
          3 => '17:00-20:00',
          4 => '17:00-20:00',
          5 => '17:00-20:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0-14',
      ),
    ),
    3 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 20000' => '0 - 20000',
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'CH[0-99]*',
          4 => 'CR[0-99]*',
          5 => 'DE[0-99]*',
          6 => 'DY[0-99]*',
          7 => 'E[0-99]*',
          8 => 'EC[0-99]*',
          9 => 'G[0-99]*',
          10 => 'HA[0-99]*',
          11 => 'HD[0-99]*',
          12 => 'HX[0-99]*',
          13 => 'IG[0-99]*',
          14 => 'L[0-99]*',
          15 => 'LS[0-99]*',
          16 => 'LU[0-99]*',
          17 => 'M[0-99]*',
          18 => 'N[0-99]*',
          19 => 'NE[0-99]*',
          20 => 'NG[0-99]*',
          21 => 'NW[0-99]*',
          22 => 'OL[0-99]*',
          23 => 'PE[0-99]*',
          24 => 'RM[0-99]*',
          25 => 'S[0-99]*',
          26 => 'SE[0-99]*',
          27 => 'SK[0-99]*',
          28 => 'SL[0-99]*',
          29 => 'ST[0-99]*',
          30 => 'SW[0-99]*',
          31 => 'TW[0-99]*',
          32 => 'UB[0-99]*',
          33 => 'W[0-99]*',
          34 => 'WC[0-99]*',
          35 => 'WF[0-99]*',
          36 => 'YO[0-99]*',
        ),
      ),
    ),
    5 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 100' => '25-100',
      ),
    ),
  ),
)


 ?>
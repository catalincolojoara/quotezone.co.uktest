<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10274',
    'siteOrigID' => '',
    'brokerID' => '33',
    'logoID' => '91',
    'companyName' => 'One Answer - QZ EQ 5 Taxi QZ048',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'One Answer - QZ EQ 5 Taxi QZ048',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'QZ048_lead@1answer.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-17:00',
          6 => '00:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'private_car_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    3 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    4 => 
    array (
      'convictions_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BARKING',
          1 => 'BARNET',
          2 => 'BARNSLEY',
          3 => 'BELFAST',
          4 => 'BEXLEY',
          5 => 'BIRMINGHAM',
          6 => 'BLACKBURN WITH DARWEN',
          7 => 'BLACKPOOL',
          8 => 'BOLTON',
          9 => 'BRADFORD',
          10 => 'BROMLEY',
          11 => 'BROMSGROVE',
          12 => 'BURY',
          13 => 'CAMDEN',
          14 => 'CHESTERFIELD',
          15 => 'COVENTRY',
          16 => 'DARTFORD',
          17 => 'DERBY',
          18 => 'DUDLEY',
          19 => 'DVA (NI)',
          20 => 'EALING',
          21 => 'ENFIELD',
          22 => 'GEDLING',
          23 => 'GREENWICH',
          24 => 'HACKNEY',
          25 => 'HALTON',
          26 => 'HAMMERSMITH',
          27 => 'HARINGEY',
          28 => 'HILLINGDON',
          29 => 'HOUNSLOW',
          30 => 'ISLINGTON',
          31 => 'KENSINGTON',
          32 => 'KINGSTON UPON THAMES ROYAL',
          33 => 'KIRKLEES',
          34 => 'KNOWSLEY',
          35 => 'LAMBETH',
          36 => 'LEEDS',
          37 => 'LEICESTER',
          38 => 'LEWISHAM',
          39 => 'LIVERPOOL',
          40 => 'LONDON PCO',
          41 => 'LUTON',
          42 => 'MANCHESTER',
          43 => 'MERTON',
          44 => 'MIDDLESBOROUGH',
          45 => 'NEWCASTLE UPON TYNE',
          46 => 'NEWHAM',
          47 => 'NORTHERN IRELAND',
          48 => 'REDBRIDGE',
          49 => 'RICHMOND',
          50 => 'ROCHDALE',
          51 => 'ROSSENDALE',
          52 => 'SALFORD',
          53 => 'SHEFFIELD',
          54 => 'SOUTH TYNESIDE',
          55 => 'SOUTHWARK',
          56 => 'STOCKPORT',
          57 => 'SUNDERLAND',
          58 => 'SUTTON',
          59 => 'TAMWORTH',
          60 => 'THURROCK',
          61 => 'TOWER HAMLETS',
          62 => 'WAKEFIELD',
          63 => 'WALSALL',
          64 => 'WALTHAM FOREST',
          65 => 'WANDSWORTH',
          66 => 'WATFORD',
          67 => 'WESTMINSTER',
          68 => 'WIGAN',
          69 => 'WIRRALL',
          70 => 'WOLVERHAMPTON',
        ),
      ),
    ),
    6 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BT[0-99]*',
          4 => 'E[0-99]*',
          5 => 'EC[0-99]*',
          6 => 'G[0-99]*',
          7 => 'HD[0-99]*',
          8 => 'L[0-99]*',
          9 => 'LE[0-99]*',
          10 => 'LS[0-99]*',
          11 => 'M[0-99]*',
          12 => 'N[0-99]*',
          13 => 'NW[0-99]*',
          14 => 'S[0-99]*',
          15 => 'SE[0-99]*',
          16 => 'SW[0-99]*',
          17 => 'W[0-99]*',
          18 => 'WF[0-99]*',
          19 => 'WV[0-99]*',
        ),
      ),
    ),
    7 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 65' => ' 25 - 65',
      ),
    ),
  ),
)


 ?>
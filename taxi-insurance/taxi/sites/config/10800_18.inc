<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10800',
    'siteOrigID' => '',
    'brokerID' => '75',
    'logoID' => '368',
    'companyName' => 'Connect Insurance - Taxi 7',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '3',
      'hour' => '1',
    ),
    'emailSubject' => 'Taxi 7 Query:',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'leadTX@connect-insurance.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$platingAuthority=strtoupper($_SESSION["_YourDetails_"]["plating_authority"]); $platingAuthorityArr = array("WOLVERHAMPTON",); if(in_array($platingAuthority, $platingAuthorityArr)) $_companyDetails["filter_details"][]["postcode_sk"]["ACC;LIST;;If Taxi Plating Authority = Wolverhampton Only Accept if Postcode = WV[0-99]*."]  = array ("WV[0-99]*",); ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
          6 => '15',
          7 => '16',
        ),
      ),
    ),
    2 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 28' => '0-28',
      ),
    ),
    3 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BD8',
          1 => 'BT[0-99]*',
          2 => 'E[0-99]*',
          3 => 'EC[0-99]*',
          4 => 'N[0-99]*',
          5 => 'NW[0-99]*',
          6 => 'SE[0-99]*',
          7 => 'SW[0-99]*',
          8 => 'W[0-99]*',
          9 => 'WC[0-99]*',
        ),
      ),
    ),
    5 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
          3 => '4',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    8 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 74' => '30 - 74',
      ),
    ),
    9 => 
    array (
      'registration_number_question' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'N',
        ),
      ),
    ),
    10 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEEN CENTRAL (INVERURIE)',
          1 => 'ABERDEEN CITY (ABERDEEN)',
          2 => 'ABERDEENSHIRE NORTH (BANFF)',
          3 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          4 => 'ADUR',
          5 => 'ALNWICK',
          6 => 'AMBER VALLEY',
          7 => 'ANGUS',
          8 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          9 => 'ARUN',
          10 => 'ASHFIELD',
          11 => 'BABERGH',
          12 => 'BARNSLEY',
          13 => 'BASSETLAW',
          14 => 'BERWICK ON TWEED',
          15 => 'BLABY',
          16 => 'BLACKPOOL',
          17 => 'BLYTH VALLEY',
          18 => 'BOLSOVER',
          19 => 'BOSTON',
          20 => 'BRECKLAND',
          21 => 'BRIDGENORTH',
          22 => 'BRIGHTON & HOVE',
          23 => 'BROADLAND',
          24 => 'BROMSGROVE',
          25 => 'BROXTOWE',
          26 => 'CAITHNESS (HIGHLANDS - WICK)',
          27 => 'CAMBRIDGE',
          28 => 'CANNOCK CHASE',
          29 => 'CHARNWOOD',
          30 => 'CHESHIRE EAST COUNCIL',
          31 => 'CHESHIRE WEST & CHESTER (BLUE) CHESTER',
          32 => 'CHESHIRE WEST & CHESTER (GREEN) VALE ROYAL',
          33 => 'CHESHIRE WEST & CHESTER (RED) ELLESMERE PORT',
          34 => 'CHESTER',
          35 => 'CHESTERFIELD',
          36 => 'CHORLEY',
          37 => 'CLACKMANANSHIRE (ALLOA)',
          38 => 'COLCHESTER',
          39 => 'CONGLETON',
          40 => 'COVENTRY',
          41 => 'CREWE & NANTWICH',
          42 => 'DERBY',
          43 => 'DERBYSHIRE DALES',
          44 => 'DUDLEY',
          45 => 'DUMFRIES & GALLOWAY',
          46 => 'DUNDEE',
          47 => 'EAST AYRSHIRE (KILMARNOCK)',
          48 => 'EAST CAMBRIDGESHIRE',
          49 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          50 => 'EAST KILBRIDE',
          51 => 'EAST LOTHIAN (HADDINGTON)',
          52 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          53 => 'EAST STAFFORDSHIRE',
          54 => 'EASTBOURNE',
          55 => 'EDINBURGH',
          56 => 'ELLESEMERE PORT & NESTON',
          57 => 'EREWASH',
          58 => 'FENLAND',
          59 => 'FIFE COUNCIL (KIRKCALDY)',
          60 => 'FLINTSHIRE',
          61 => 'FOREST HEATH',
          62 => 'FYLDE',
          63 => 'GATESHEAD',
          64 => 'GEDLING',
          65 => 'GLASGOW CITY',
          66 => 'GREAT YARMOUTH',
          67 => 'HARBOROUGH',
          68 => 'HARROGATE',
          69 => 'HIGH PEAK',
          70 => 'HIGHLAND COUNCIL (INVERNESS)',
          71 => 'HINCKLEY & BOSWORTH',
          72 => 'HUNTINGDON',
          73 => 'INVERCLYDE (GREENOCK)',
          74 => 'IPSWICH',
          75 => 'KINGS LYNN & WEST NORFOLK',
          76 => 'LEICESTER',
          77 => 'LEWES',
          78 => 'LICHFIELD',
          79 => 'MACCLESFIELD / CHESHIRE EAST',
          80 => 'MANSFIELD',
          81 => 'MELTON',
          82 => 'MID SUFFOLK',
          83 => 'MIDLOTHIAN COUNCIL',
          84 => 'MORAY (ELGIN)',
          85 => 'NAIRN (HIGHLANDS)',
          86 => 'NEWARK & SHERWOOD',
          87 => 'NEWCASTLE UNDER LYME',
          88 => 'NEWCASTLE UPON TYNE',
          89 => 'NORTH AYRSHIRE (IRVINE)',
          90 => 'NORTH EAST DERBYSHIRE',
          91 => 'NORTH KESTEVEN',
          92 => 'NORTH LANARKSHIRE (MOTHERWELL)',
          93 => 'NORTH LANARKSHIRE(SOUTH/CENTRAL)',
          94 => 'NORTH NORFOLK',
          95 => 'NORTH TYNESIDE',
          96 => 'NORTHUMBERLAND',
          97 => 'NORWICH',
          98 => 'NUNEATON & BEDWORTH',
          99 => 'OADBY & WIGSTON',
          100 => 'ORKNEY ISLANDS (KIRKWALL)',
          101 => 'PENDLE',
          102 => 'PERTH & KINROSS',
          103 => 'PETERBOROUGH',
          104 => 'PSV SCOTLAND',
          105 => 'REDDITCH',
          106 => 'RENFREWSHIRE (PAISLEY)',
          107 => 'RIBBLE VALLEY',
          108 => 'ROSS & CROMARTY (HIGHLAND - DINGWALL)',
          109 => 'ROTHERHAM',
          110 => 'RUGBY',
          111 => 'RUSHCLIFFE',
          112 => 'RUTLAND',
          113 => 'SCOTTISH BORDERS',
          114 => 'SHEFFIELD',
          115 => 'SKYE & LOCHALSH (HIGHLAND - ISLE OF SKYE)',
          116 => 'SOUTH AYRSHIRE (AYR)',
          117 => 'SOUTH CAMBRIDGESHIRE',
          118 => 'SOUTH DERBYSHIRE',
          119 => 'SOUTH EAST & METROPOLITAN',
          120 => 'SOUTH HOLLAND',
          121 => 'SOUTH KESTEVEN',
          122 => 'SOUTH LANARKSHIRE',
          123 => 'SOUTH LANARKSHIRE (HAMILTON)',
          124 => 'SOUTH LANARKSHIRE (LANARK)',
          125 => 'SOUTH LANARKSHIRE (RUTHERGLEN)',
          126 => 'SOUTH NORFOLK',
          127 => 'SOUTH RIBBLE',
          128 => 'SOUTH TYNESIDE',
          129 => 'ST EDMUNDSBURY',
          130 => 'STAFFORD',
          131 => 'STAFFORDSHIRE MOORLANDS',
          132 => 'STIRLING',
          133 => 'STOCKPORT',
          134 => 'STOKE ON TRENT',
          135 => 'STRATFORD ON AVON',
          136 => 'SUFFOLK COASTAL',
          137 => 'SUNDERLAND',
          138 => 'SUTHERLAND (HIGHLAND - GOLSPIE)',
          139 => 'TENDRING',
          140 => 'UTTLESFORD',
          141 => 'VALE ROYAL',
          142 => 'WALSALL',
          143 => 'WANSBECK',
          144 => 'WARWICK',
          145 => 'WAVENEY',
          146 => 'WEALDEN',
          147 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          148 => 'WEST LOTHIAN (LIVINGSTON)',
          149 => 'WIRRALL',
          150 => 'WOLVERHAMPTON',
          151 => 'WORTHING',
          152 => 'WYRE',
          153 => 'WYRE FORREST',
          154 => 'YORK',
        ),
      ),
    ),
  ),
)


 ?>
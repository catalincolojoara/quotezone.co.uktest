<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10582',
    'siteOrigID' => '',
    'brokerID' => '85',
    'logoID' => '761',
    'companyName' => 'Freeway 1',
    'offline_from_plugin' => false,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotes@freewayinsurance.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '  $filter = "";   $typeOfCover=$_SESSION["_YourDetails_"]["type_of_cover"]; $estimatedValue=$_SESSION["_YourDetails_"]["estimated_value"]; if($typeOfCover == "1")     $_companyDetails["filter_details"][]["estimated_value"]["SKP;VAL;MIN;if Type of Cover is  Fully Comprehensive SKIP if Vehicle Value is Over  50000 ."]  = "50000"; ',
    1 => '   $filter = "";  $typeOfCover=$_SESSION["_YourDetails_"]["type_of_cover"]; $estimatedValue=$_SESSION["_YourDetails_"]["estimated_value"]; if($typeOfCover == "2" || $typeOfCover == "3")     $_companyDetails["filter_details"][]["estimated_value"]["SKP;VAL;MIN;if Type of Cover is Third Party Fire & Theft OR Third Party Only SKIP if Vehicle Value Over 6000 ."]  = "6000"; ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 75' => ' 25 - 75',
      ),
    ),
    1 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'type_of_cover' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
        ),
      ),
    ),
    3 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    4 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 15' => '15',
      ),
    ),
    6 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    7 => 
    array (
      'convictions_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    8 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
        ),
      ),
    ),
    9 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BELFAST',
          1 => 'DVA (NI)',
          2 => 'GUERNSEY',
          3 => 'JERSEY',
          4 => 'LONDON PCO',
          5 => 'NORTHERN IRELAND',
        ),
      ),
    ),
  ),
)


 ?>
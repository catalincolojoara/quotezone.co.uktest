<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10722',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '464',
    'companyName' => 'Coversure Acocks',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Taxi Query (MPV):',
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'tonymc@coversure.co.uk',
        1 => 'jameslo@coversure.co.uk',
        2 => 'lindamc@coversure.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'E[0-99]*',
          2 => 'EC[0-99]*',
          3 => 'M[0-99]*',
          4 => 'N[0-99]*',
          5 => 'NW[0-99]*',
          6 => 'SE[0-99]*',
          7 => 'SW[0-99]*',
          8 => 'W[0-99]*',
          9 => 'WC[0-99]*',
        ),
      ),
    ),
    2 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'CANNOCK CHASE',
          1 => 'COVENTRY',
          2 => 'LICHFIELD',
          3 => 'SANDWELL',
          4 => 'SOLIHULL',
          5 => 'TAMWORTH',
          6 => 'WALSALL',
          7 => 'WARWICK',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_type' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
        ),
      ),
    ),
  ),
)


 ?>
<?php

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10692',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '162',
    'companyName' => 'Crosby (Newcastle)',
    'offline_from_plugin' => false,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'stuart@crosbyinsurance.co.uk',
        1 => 'stan.crosby03@gmail.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '08:00-17:00',
          2 => '08:00-17:00',
          3 => '08:00-17:00',
          4 => '08:00-17:00',
          5 => '08:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_type' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
          5 => '8',
        ),
      ),
    ),
  ),
  'pausedEmails' => 
  array (
    'MAIL' => 
    array (
      'COMPANY' => 
      array (
        0 => 'crosbyinsurance@btinternet.com',
        1 => 'crosbyinsurance@btconnect.com',
        2 => 'dave.burn@crosbyinsurance.co.uk',
        3 => 'stan@crosbyinsurance.co.uk',
      ),
    ),
  ),
)


?>
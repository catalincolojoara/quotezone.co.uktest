<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '9999',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '448',
    'companyName' => 'Brightside (One Insurance Solution) (City)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'hour' => '2',
      'day' => '20',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
        1 => 'michael.kirk@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxi.info@taxi-direct.co.uk',
        1 => 'cityteam@brightsidegroup.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'bespoke_filters' => 
  array (
    0 => '$platingAuthority = trim(strtoupper($_SESSION["_YourDetails_"]["plating_authority"])); $DISdd            = $_SESSION["_YourDetails_"]["date_of_insurance_start_dd"]; $DISmm            = $_SESSION["_YourDetails_"]["date_of_insurance_start_mm"]; $DISyyyy          = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"]; $day   = date("d"); $month = date("m"); $year  = date("Y"); $daysOfInsuranceStart = (mktime(0,0,0,$DISmm,$DISdd,$DISyyyy) - mktime(0,0,0,$month,$day,$year)) / 86400; if($daysOfInsuranceStart > 14 && $platingAuthority == "WOLVERHAMPTON") $_companyDetails["filter_details"][]["BSK EB2-61684"]["ACC;VAL;EQL;If Plating Authority = Wolverhampton SKIP if Insurance Start Date > 14 days after Quote Date."]  = "BSK_SKP"; ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BD[0-99]*',
          1 => 'BT[0-99]*',
        ),
      ),
    ),
    3 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 21' => '0-21',
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 23 and 75' => '23 - 75',
      ),
    ),
    5 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
          5 => '8',
        ),
      ),
    ),
    7 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEEN CITY (ABERDEEN)',
          1 => 'BEDFORD',
          2 => 'COVENTRY',
          3 => 'EPPING FOREST',
          4 => 'LEICESTER',
          5 => 'LONDON PCO',
          6 => 'WOLVERHAMPTON',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11890',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '54',
    'companyName' => 'Quoteline Direct - Taxi (Scotland)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '75',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
    ),
    'TXT' => 
    array (
      'COMPANY' => 
      array (
        0 => 'email.leads@quotelinedirect.co.uk',
      ),
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 100' => '25-100',
      ),
    ),
    2 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
          5 => '8',
          6 => '9',
          7 => '10',
        ),
      ),
    ),
    3 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BD[0-99]*',
          2 => 'BR[0-99]*',
          3 => 'BT[0-99]*',
          4 => 'CR[0-99]*',
          5 => 'E[0-99]*',
          6 => 'EC[0-99]*',
          7 => 'EN[0-99]*',
          8 => 'HA[0-99]*',
          9 => 'IG[0-99]*',
          10 => 'KT[0-99]*',
          11 => 'LS[0-99]*',
          12 => 'N[0-99]*',
          13 => 'NE[0-99]*',
          14 => 'NW[0-99]*',
          15 => 'RM[0-99]*',
          16 => 'S[0-99]*',
          17 => 'SE[0-99]*',
          18 => 'SM[0-99]*',
          19 => 'SR[0-99]*',
          20 => 'SW[0-99]*',
          21 => 'TW[0-99]*',
          22 => 'UB[0-99]*',
          23 => 'W[0-99]*',
          24 => 'WC[0-99]*',
          25 => 'WD[0-99]*',
        ),
      ),
    ),
    4 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEEN',
          1 => 'ABERDEEN CENTRAL (INVERURIE)',
          2 => 'ABERDEEN CITY (ABERDEEN)',
          3 => 'ABERDEENSHIRE',
          4 => 'ABERDEENSHIRE NORTH (BANFF)',
          5 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          6 => 'ANGUS',
          7 => 'ANGUS COUNCIL (FORFAR)',
          8 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          9 => 'BADENOCH & STRATHSPEY (HIGHLAND - KINGUSSIE)',
          10 => 'BERWICK ON TWEED',
          11 => 'CAITHNESS (HIGHLANDS - WICK)',
          12 => 'CLACKMANANSHIRE (ALLOA)',
          13 => 'DUMFRIES & GALLOWAY',
          14 => 'DUNDEE',
          15 => 'EAST AYRSHIRE (KILMARNOCK)',
          16 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          17 => 'EAST KILBRIDE',
          18 => 'EAST LOTHIAN (HADDINGTON)',
          19 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          20 => 'EDINBURGH',
          21 => 'FALKIRK',
          22 => 'FIFE COUNCIL (KIRKCALDY)',
          23 => 'HIGHLAND COUNCIL (INVERNESS)',
          24 => 'INVERCLYDE (GREENOCK)',
          25 => 'LOCHABER (HIGHLAND - FORT WILLIAM)',
          26 => 'MIDLOTHIAN COUNCIL',
          27 => 'MORAY (ELGIN)',
          28 => 'NAIRN (HIGHLANDS)',
          29 => 'NORTH AYRSHIRE (IRVINE)',
          30 => 'NORTH LANARKSHIRE (MOTHERWELL)',
          31 => 'NORTH LANARKSHIRE(SOUTH/CENTRAL)',
          32 => 'ORKNEY ISLANDS (KIRKWALL)',
          33 => 'PERTH & KINROSS',
          34 => 'RENFREWSHIRE (PAISLEY)',
          35 => 'ROSS & CROMARTY (HIGHLAND - DINGWALL)',
          36 => 'SCOTTISH BORDERS',
          37 => 'SHETLAND ISLANDS (LERWICK)',
          38 => 'SKYE & LOCHALSH (HIGHLAND - ISLE OF SKYE)',
          39 => 'SOUTH AYRSHIRE (AYR)',
          40 => 'SOUTH LANARKSHIRE',
          41 => 'SOUTH LANARKSHIRE (HAMILTON)',
          42 => 'SOUTH LANARKSHIRE (LANARK)',
          43 => 'SOUTH LANARKSHIRE (RUTHERGLEN)',
          44 => 'STIRLING',
          45 => 'SUTHERLAND (HIGHLAND - GOLSPIE)',
          46 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          47 => 'WEST LOTHIAN (LIVINGSTON)',
          48 => 'WESTERN ISLES (STORNOWAY)',
        ),
      ),
    ),
  ),
)


 ?>
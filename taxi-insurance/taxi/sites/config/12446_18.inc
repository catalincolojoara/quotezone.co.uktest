<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12446',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '754',
    'companyName' => 'Quotax Insurance',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'daniel.fosker@quotax.net',
        1 => 'leads@quotax.net',
        2 => 'leadsn1@taxi-insurance.london',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-16:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEEN',
          1 => 'ABERDEEN CENTRAL (INVERURIE)',
          2 => 'ABERDEEN CITY (ABERDEEN)',
          3 => 'ABERDEENSHIRE',
          4 => 'ABERDEENSHIRE NORTH (BANFF)',
          5 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          6 => 'ANGUS COUNCIL (FORFAR)',
          7 => 'CLACKMANANSHIRE (ALLOA)',
          8 => 'DUMFRIES & GALLOWAY',
          9 => 'DUNBARTON',
          10 => 'DUNDEE',
          11 => 'EAST AYRSHIRE (KILMARNOCK)',
          12 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          13 => 'EAST LOTHIAN (HADDINGTON)',
          14 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          15 => 'FALKIRK',
          16 => 'FIFE COUNCIL (KIRKCALDY)',
          17 => 'HIGHLAND COUNCIL (INVERNESS)',
          18 => 'INVERNESS (HIGHLANDS)',
          19 => 'MIDLOTHIAN COUNCIL',
          20 => 'MORAY (ELGIN)',
          21 => 'NORTH AYRSHIRE (IRVINE)',
          22 => 'NORTH LANARKSHIRE (MOTHERWELL)',
          23 => 'NORTH LANARKSHIRE(SOUTH/CENTRAL)',
          24 => 'ORKNEY ISLANDS (KIRKWALL)',
          25 => 'PERTH & KINROSS',
          26 => 'RENFREWSHIRE (PAISLEY)',
          27 => 'ROSS & CROMARTY (HIGHLAND - DINGWALL)',
          28 => 'SCOTTISH BORDERS',
          29 => 'SKYE & LOCHALSH (HIGHLAND - ISLE OF SKYE)',
          30 => 'SOUTH AYRSHIRE (AYR)',
          31 => 'SOUTH LANARKSHIRE',
          32 => 'SOUTH LANARKSHIRE (HAMILTON)',
          33 => 'SOUTH LANARKSHIRE (RUTHERGLEN)',
          34 => 'STIRLING',
          35 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          36 => 'WEST LOTHIAN (LIVINGSTON)',
          37 => 'WESTERN ISLES (STORNOWAY)',
        ),
      ),
    ),
  ),
)


 ?>
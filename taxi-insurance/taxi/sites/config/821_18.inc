<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '821',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '29',
    'companyName' => 'Coversure Hyde - Taxi',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '5',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'paulmc3@coversure.co.uk',
        1 => 'rogersm@coversure.co.uk',
        2 => 'ianmo@coversure.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 35 and 100' => '35 - 100',
      ),
    ),
    1 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'ABERDEEN',
          1 => 'ABERDEEN CENTRAL (INVERURIE)',
          2 => 'ABERDEEN CITY (ABERDEEN)',
          3 => 'ABERDEENSHIRE',
          4 => 'ABERDEENSHIRE NORTH (BANFF)',
          5 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          6 => 'ADUR',
          7 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          8 => 'BABERGH',
          9 => 'BADENOCH & STRATHSPEY (HIGHLAND - KINGUSSIE)',
          10 => 'BELFAST',
          11 => 'BIRMINGHAM',
          12 => 'BLACKBURN WITH DARWEN',
          13 => 'BLACKPOOL',
          14 => 'BOLTON',
          15 => 'BRADFORD',
          16 => 'BROMSGROVE',
          17 => 'BURNLEY',
          18 => 'BURY',
          19 => 'CAITHNESS (HIGHLANDS - WICK)',
          20 => 'CALDERDALE',
          21 => 'CHESHIRE EAST COUNCIL',
          22 => 'CHESHIRE WEST & CHESTER (BLUE) CHESTER',
          23 => 'CHESHIRE WEST & CHESTER (GREEN) VALE ROYAL',
          24 => 'CHESHIRE WEST & CHESTER (RED) ELLESMERE PORT',
          25 => 'CREWE & NANTWICH',
          26 => 'DARTFORD',
          27 => 'DVA (NI)',
          28 => 'EASINGTON',
          29 => 'EDINBURGH',
          30 => 'FALKIRK',
          31 => 'FIFE COUNCIL (KIRKCALDY)',
          32 => 'FYLDE',
          33 => 'GLASGOW CITY',
          34 => 'GUERNSEY',
          35 => 'HALTON',
          36 => 'KINGSTON-UPON-HULL',
          37 => 'KIRKLEES',
          38 => 'KNOWSLEY',
          39 => 'LEEDS',
          40 => 'LONDON PCO',
          41 => 'LUTON',
          42 => 'MACCLESFIELD / CHESHIRE EAST',
          43 => 'MANCHESTER',
          44 => 'MID BEDFORDSHIRE',
          45 => 'NEWBURY',
          46 => 'NEWCASTLE UPON TYNE',
          47 => 'NORTH TYNESIDE',
          48 => 'NORTHERN IRELAND',
          49 => 'NORTHUMBERLAND',
          50 => 'OLDHAM',
          51 => 'PRESTON',
          52 => 'ROSSENDALE',
          53 => 'SEFTON',
          54 => 'SHEFFIELD',
          55 => 'SHROPSHIRE COUNTY',
          56 => 'SOUTH RIBBLE',
          57 => 'ST HELENS',
          58 => 'STOCKPORT',
          59 => 'TAMESIDE',
          60 => 'TEESDALE',
          61 => 'TRAFFORD',
          62 => 'WAKEFIELD',
          63 => 'WALSALL',
          64 => 'WIRRALL',
          65 => 'WOLVERHAMPTON',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '9632',
    'siteOrigID' => '',
    'brokerID' => '57',
    'logoID' => '615',
    'companyName' => 'Milestone - Taxi 2 (OOH)',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Milestone - Taxi 2 (OOH)',
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
        1 => 'alexandru.furtuna@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Rob.g@milestonehouse.com',
        1 => 'Nathan.t@milestonehouse.com',
        2 => 'Ryan.c@milestonehouse.com',
        3 => 'Devon.c@milestonehouse.com',
        4 => 'Brenda.b@milestonehouse.com',
        5 => 'Caroline.w@milestonehouse.com',
        6 => 'Shauna.s@milestonehouse.com',
        7 => 'Natalie.g@milestonehouse.com',
        8 => 'Jack.w@milestonehouse.com',
        9 => 'James.h@milestonehouse.com',
        10 => 'Matthew.o@milestonehouse.com',
        11 => 'red@rcdata.co.uk',
        12 => 'Chris.h@milestonehouse.com',
        13 => 'Nicholas.l@milestonehouse.com',
        14 => 'Darren.c@milestonehouse.com',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|16:45-23:45',
          2 => '00:00-09:00|16:45-23:59',
          3 => '00:00-09:00|16:45-23:59',
          4 => '00:00-09:00|16:45-23:59',
          5 => '00:00-09:00|16:45-23:59',
          6 => '00:00-09:00|16:45-23:59',
          7 => '00:00-09:00|16:45-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '5',
          1 => '6',
          2 => '7',
          3 => '8',
          4 => '9',
          5 => '10',
          6 => '11',
          7 => '12',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 9' => '9',
      ),
    ),
    3 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BASILDON',
          1 => 'BIRMINGHAM',
          2 => 'BLACKBURN WITH DARWEN',
          3 => 'BOLSOVER',
          4 => 'BOLTON',
          5 => 'BRISTOL',
          6 => 'BRISTOL CITY OF UA',
          7 => 'BROXTOWE',
          8 => 'BURNLEY',
          9 => 'BURY',
          10 => 'CAERPHILLY',
          11 => 'CANNOCK CHASE',
          12 => 'CARDIFF',
          13 => 'CASTLE MORPETH',
          14 => 'CENTRAL BEDFORDSHIRE',
          15 => 'CHARNWOOD',
          16 => 'CRAVEN',
          17 => 'DERBY',
          18 => 'DUDLEY',
          19 => 'EAST STAFFORDSHIRE',
          20 => 'FLINTSHIRE',
          21 => 'GEDLING',
          22 => 'GUILDFORD',
          23 => 'HERTSMERE',
          24 => 'HIGH PEAK',
          25 => 'HYNDBURN',
          26 => 'KNOWSLEY',
          27 => 'LIVERPOOL',
          28 => 'LONDON PCO',
          29 => 'LUTON',
          30 => 'MACCLESFIELD / CHESHIRE EAST',
          31 => 'MANCHESTER',
          32 => 'MERTHYR TYDFIL',
          33 => 'MIDDLESBOROUGH',
          34 => 'NEWCASTLE UNDER LYME',
          35 => 'NEWCASTLE UPON TYNE',
          36 => 'NORTH EAST DERBYSHIRE',
          37 => 'NORTH TYNESIDE',
          38 => 'NOTTINGHAM',
          39 => 'OLDHAM',
          40 => 'PENDLE',
          41 => 'REDDITCH',
          42 => 'ROCHDALE',
          43 => 'ROCHFORD',
          44 => 'RUSHCLIFFE',
          45 => 'SALFORD',
          46 => 'SANDWELL',
          47 => 'SEFTON',
          48 => 'SLOUGH',
          49 => 'SOLIHULL',
          50 => 'SOUTH DERBYSHIRE',
          51 => 'SOUTH RIBBLE',
          52 => 'SOUTH STAFFORDSHIRE',
          53 => 'SOUTH TYNESIDE',
          54 => 'STAFFORDSHIRE COUNTY',
          55 => 'STAFFORDSHIRE MOORLANDS',
          56 => 'SUNDERLAND',
          57 => 'TEST VALLEY',
          58 => 'VALE OF GLAMORGAN',
          59 => 'WIRRALL',
          60 => 'WOLVERHAMPTON',
        ),
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 70' => '30 - 70',
      ),
    ),
    5 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 21' => '0 - 21',
      ),
    ),
    6 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BL[0-99]*',
          4 => 'BR[0-99]*',
          5 => 'BT[0-99]*',
          6 => 'CR[0-99]*',
          7 => 'CV[0-99]*',
          8 => 'DA[0-99]*',
          9 => 'DY[0-99]*',
          10 => 'E[0-99]*',
          11 => 'EC[0-99]*',
          12 => 'EN[0-99]*',
          13 => 'HA[0-99]*',
          14 => 'IG[0-99]*',
          15 => 'KT[0-99]*',
          16 => 'L[0-99]*',
          17 => 'LU[0-99]*',
          18 => 'M[0-99]*',
          19 => 'ME[0-99]*',
          20 => 'N[0-99]*',
          21 => 'NE[0-99]*',
          22 => 'NW[0-99]*',
          23 => 'OL[0-99]*',
          24 => 'PR[0-99]*',
          25 => 'RM[0-99]*',
          26 => 'SE[0-99]*',
          27 => 'SK[0-99]*',
          28 => 'SL[0-99]*',
          29 => 'SM[0-99]*',
          30 => 'SR[0-99]*',
          31 => 'SS[0-99]*',
          32 => 'SW[0-99]*',
          33 => 'TS[0-99]*',
          34 => 'TW[0-99]*',
          35 => 'UB[0-99]*',
          36 => 'W[0-99]*',
          37 => 'WC[0-99]*',
          38 => 'WD[0-99]*',
          39 => 'WS[0-99]*',
          40 => 'WV[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
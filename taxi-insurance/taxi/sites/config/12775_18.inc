<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12775',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '1017',
    'companyName' => 'Echo Insurance Brokers - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      '30min' => '1',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'sarah@echoinsurance.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => '09:00-13:00',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BELFAST',
          1 => 'BIRMINGHAM',
          2 => 'BLACKBURN WITH DARWEN',
          3 => 'BLACKPOOL',
          4 => 'BOLTON',
          5 => 'BRADFORD',
          6 => 'BURNLEY',
          7 => 'BURY',
          8 => 'CALDERDALE',
          9 => 'CENTRAL BEDFORDSHIRE',
          10 => 'COVENTRY',
          11 => 'DUDLEY',
          12 => 'HYNDBURN',
          13 => 'ISLE OF MAN',
          14 => 'ISLE OF SCILLY',
          15 => 'ISLE OF WIGHT',
          16 => 'KIRKLEES',
          17 => 'KNOWSLEY',
          18 => 'LEEDS',
          19 => 'LIVERPOOL',
          20 => 'LONDON PCO',
          21 => 'LUTON',
          22 => 'MANCHESTER',
          23 => 'NEWCASTLE UPON TYNE',
          24 => 'NORTHERN IRELAND',
          25 => 'NOTTINGHAM',
          26 => 'OLDHAM',
          27 => 'PENDLE',
          28 => 'PRESTON',
          29 => 'RIBBLE VALLEY',
          30 => 'ROCHDALE',
          31 => 'ROSSENDALE',
          32 => 'ROTHERHAM',
          33 => 'SALFORD',
          34 => 'SEFTON',
          35 => 'SHEFFIELD',
          36 => 'SLOUGH',
          37 => 'SOUTH BUCKINGHAM',
          38 => 'SOUTH RIBBLE',
          39 => 'ST HELENS',
          40 => 'STOCKPORT',
          41 => 'STOKE ON TRENT',
          42 => 'TRAFFORD',
          43 => 'WALSALL',
          44 => 'WATFORD',
          45 => 'WIGAN',
          46 => 'WIRRALL',
        ),
      ),
    ),
    2 => 
    array (
      'claims_5_years' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'No',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
          5 => '9',
          6 => '10',
          7 => '11',
          8 => '12',
          9 => '13',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    5 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '5',
          1 => '6',
          2 => '7',
          3 => '8',
          4 => '9',
          5 => '10',
          6 => '11',
          7 => '12',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
          6 => '15',
          7 => '16',
        ),
      ),
    ),
    7 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 73' => '25-73',
      ),
    ),
    8 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
          1 => 'OL[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
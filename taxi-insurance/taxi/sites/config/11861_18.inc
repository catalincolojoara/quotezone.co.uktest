<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11861',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '187',
    'companyName' => 'Connect Insurance - Taxi 8',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '7',
    ),
    'emailSubject' => 'Taxi 8 Query:',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'LeadTX@connect-insurance.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$platingAuthority=strtoupper($_SESSION["_YourDetails_"]["plating_authority"]); $platingAuthorityArr = array("WOLVERHAMPTON",); if(in_array($platingAuthority, $platingAuthorityArr)) $_companyDetails["filter_details"][]["postcode_sk"]["ACC;LIST;;If Taxi Plating Authority = Wolverhampton Only Accept if Postcode = WV[0-99]*."]  = array ("WV[0-99]*",); ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 74' => '25-74',
      ),
    ),
    2 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    3 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BD8',
          1 => 'BT[0-99]*',
          2 => 'E[0-99]*',
          3 => 'EC[0-99]*',
          4 => 'N[0-99]*',
          5 => 'NW[0-99]*',
          6 => 'SE[0-99]*',
          7 => 'SW[0-99]*',
          8 => 'W[0-99]*',
          9 => 'WC[0-99]*',
        ),
      ),
    ),
    5 => 
    array (
      'type_of_cover' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
          2 => '4',
        ),
      ),
    ),
    6 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '8',
          1 => '9',
          2 => '10',
          3 => '11',
          4 => '12',
          5 => '13',
          6 => '14',
          7 => '15',
          8 => '16',
        ),
      ),
    ),
    8 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
          2 => '4',
          3 => '5',
        ),
      ),
    ),
    9 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BARKING',
          1 => 'BARNET',
          2 => 'BEXLEY',
          3 => 'BIRMINGHAM',
          4 => 'BLACKBURN WITH DARWEN',
          5 => 'BOLTON',
          6 => 'BRADFORD',
          7 => 'BRENT',
          8 => 'BRISTOL',
          9 => 'BRISTOL CITY OF UA',
          10 => 'BROMLEY',
          11 => 'BURNLEY',
          12 => 'CAMDEN',
          13 => 'CROYDEN',
          14 => 'EALING',
          15 => 'ENFIELD',
          16 => 'GREENWICH',
          17 => 'HACKNEY',
          18 => 'HAMMERSMITH',
          19 => 'HAVERING',
          20 => 'HILLINGDON',
          21 => 'HOUNSLOW',
          22 => 'ISLINGTON',
          23 => 'KENSINGTON',
          24 => 'KENT',
          25 => 'KINGSTON UPON THAMES ROYAL',
          26 => 'LAMBETH',
          27 => 'LEWISHAM',
          28 => 'LIVERPOOL',
          29 => 'LONDON PCO',
          30 => 'LUTON',
          31 => 'MANCHESTER',
          32 => 'MERTON',
          33 => 'NEWHAM',
          34 => 'OLDHAM',
          35 => 'PRESTON',
          36 => 'REDBRIDGE',
          37 => 'RICHMOND',
          38 => 'ROCHDALE',
          39 => 'ROSSENDALE',
          40 => 'SALFORD',
          41 => 'SLOUGH',
          42 => 'SOUTH RIBBLE',
          43 => 'SOUTHWARK',
          44 => 'SUTTON',
          45 => 'TOWER HAMLETS',
          46 => 'WALTHAM FOREST',
          47 => 'WANDSWORTH',
          48 => 'WESTMINSTER',
        ),
      ),
    ),
    10 => 
    array (
      'registration_number_question' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'N',
        ),
      ),
    ),
    11 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
        ),
      ),
    ),
  ),
)


 ?>
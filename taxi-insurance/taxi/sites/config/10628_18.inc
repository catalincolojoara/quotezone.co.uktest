<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10628',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '91',
    'companyName' => 'One Answer - QZ025 Taxi OOH (1 or more NCB)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'One Answer - QZ025 Taxi OOH (1 or more NCB)',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'QZ025_lead@1answer.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => 'include_once "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/taxi/steps/functions.php"; $privateCarNcb = $_SESSION["_YourDetails_"]["private_car_ncb"]; $ageNCBMsg     = "";     $DISdd    = $_SESSION["_YourDetails_"]["date_of_insurance_start_dd"]; $DISmm    = $_SESSION["_YourDetails_"]["date_of_insurance_start_mm"]; $DISyyyy  = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"]; $dobDD    = $_SESSION["_YourDetails_"]["date_of_birth_dd"]; $dobMM    = $_SESSION["_YourDetails_"]["date_of_birth_mm"]; $dobYYYY  = $_SESSION["_YourDetails_"]["date_of_birth_yyyy"]; $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd; $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD; $propAge = GetAge($proposerBirthDate,$insuranceStartDate); if($propAge < 25) {$_companyDetails["filter_details"][]["private_car_ncb"]["SKP;LIST;; If Age < 25 SKIP if Private car no claims bonus = No NCB "] = array(0 => "0",); } ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|19:00-23:59',
          2 => '00:00-09:00|19:00-23:59',
          3 => '00:00-09:00|19:00-23:59',
          4 => '00:00-09:00|19:00-23:59',
          5 => '00:00-09:00|19:00-23:59',
          6 => '00:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 22 and 100' => '22-100',
      ),
    ),
    2 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0-14',
      ),
    ),
    3 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BA[0-99]*',
          1 => 'BH[0-99]*',
          2 => 'BN[0-99]*',
          3 => 'BR[0-99]*',
          4 => 'BS[0-99]*',
          5 => 'CT[0-99]*',
          6 => 'DA[0-99]*',
          7 => 'DT[0-99]*',
          8 => 'EX[0-99]*',
          9 => 'GL[0-99]*',
          10 => 'GU[0-99]*',
          11 => 'HU[0-99]*',
          12 => 'IP[0-99]*',
          13 => 'ME[0-99]*',
          14 => 'NP[0-99]*',
          15 => 'NR[0-99]*',
          16 => 'OX[0-99]*',
          17 => 'PE[0-99]*',
          18 => 'PL[0-99]*',
          19 => 'PO[0-99]*',
          20 => 'RG[0-99]*',
          21 => 'RH[0-99]*',
          22 => 'SN[0-99]*',
          23 => 'SO[0-99]*',
          24 => 'SP[0-99]*',
          25 => 'SS[0-99]*',
          26 => 'TA[0-99]*',
          27 => 'TN[0-99]*',
          28 => 'TQ[0-99]*',
          29 => 'TR[0-99]*',
        ),
      ),
    ),
    4 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'ADUR',
          1 => 'ARUN',
          2 => 'ASHFORD',
          3 => 'BRADFORD',
          4 => 'BRIGHTON & HOVE',
          5 => 'CRAWLEY',
          6 => 'LEWES',
          7 => 'LONDON PCO',
          8 => 'MID SUSSEX',
          9 => 'REIGATE AND BANSTEAD',
          10 => 'ROTHER',
          11 => 'SEVENOAKS',
          12 => 'TUNBRIDGE WELLS',
          13 => 'WEALDEN',
          14 => 'WORTHING',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
        ),
      ),
    ),
    6 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 1500 and 100000' => '1500 - 100000',
      ),
    ),
    7 => 
    array (
      'type_of_cover' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
        ),
      ),
    ),
    8 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
        ),
      ),
    ),
    9 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '5',
        ),
      ),
    ),
    10 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '15',
          1 => '16',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10940',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '562',
    'companyName' => 'Policywise',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
    ),
    'emailSubject' => 'Taxi (NI) Query:',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
        1 => 'michael.kirk@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Taxi@policywise.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$_companyDetails["filter_details"][]["GROUP_50"]["ACC;VAL;MAX;Skip if Insurance Group > 38 ."]  = "38";',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '8',
          1 => '9',
          2 => '10',
          3 => '11',
          4 => '12',
          5 => '13',
          6 => '14',
          7 => '15',
          8 => '16',
        ),
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 69' => '25-69',
      ),
    ),
    5 => 
    array (
      'estimated_value_sk' => 
      array (
        'SKP;VAL;;Skip if value is between 40000 and 100000' => '40000 - 100000',
      ),
    ),
  ),
)


 ?>
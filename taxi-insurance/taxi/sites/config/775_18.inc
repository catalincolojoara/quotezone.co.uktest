<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '775',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '497',
    'companyName' => 'Insurance Factory (Marker Study) - Taxi 4',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Insurance Factory (Marker Study) - Taxi 4',
    'limit' => 
    array (
      'day' => '30',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'shopquotes@insurancefactory.co.uk',
        1 => 'callcampaign@insurancefactory.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$flag = "skp"; $day     = date("d"); $month   = date("m"); $year    = date("Y"); $todayMk = mktime("0","0","0",$month,$day,$year); $disMM   = $_SESSION["_YourDetails_"]["date_of_insurance_start_mm"]; $disDD   = $_SESSION["_YourDetails_"]["date_of_insurance_start_dd"]; $disYYYY = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"]; $disMk   = mktime("0","0","0",$disMM,$disDD,$disYYYY); $timeDiffDays = (($disMk - $todayMk) / 86400); if($timeDiffDays <= 8) $flag = "acc"; else if($timeDiffDays >= 10 && $timeDiffDays <= 12) $flag = "acc"; else if($timeDiffDays >= 15 && $timeDiffDays <= 16) $flag = "acc"; else if($timeDiffDays >= 21 && $timeDiffDays <= 24) $flag = "acc"; else if($timeDiffDays == 18) $flag = "acc"; if($flag == "skp")$_companyDetails["filter_details"][]["BSK_ACC_DIS"]["ACC;VAL;EQL;From the Quote Date, ACCEPT if Insurance Start Date is 0 - 8 and 10 - 12 and 15 - 16 and 18 and 21 - 24 days"] ="ACC"; ',
    1 => '$flag = "skp"; $DISdd             = $_SESSION["_YourDetails_"]["date_of_insurance_start_dd"]; $DISmm             = $_SESSION["_YourDetails_"]["date_of_insurance_start_mm"]; $DISyyyy           = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"]; $dobDD             = $_SESSION["_YourDetails_"]["date_of_birth_dd"]; $dobMM             = $_SESSION["_YourDetails_"]["date_of_birth_mm"]; $dobYYYY           = $_SESSION["_YourDetails_"]["date_of_birth_yyyy"]; $proposerBirthDate = $dobYYYY."-".$dobMM."-".$dobDD; $insuranceStartDate= $DISyyyy."-".$DISmm."-".$DISdd; $propAge           = GetAge($proposerBirthDate,$insuranceStartDate); if($propAge >= 25 && $propAge <= 29) $flag = "acc"; else if($propAge >= 35 && $propAge <= 69) $flag = "acc"; if($flag == "skp") $_companyDetails["filter_details"][]["BSK_ACC_AGE"]["ACC;VAL;EQL;ACCEPT IF 25 - 29 and 35 - 69 years old"] ="ACC";  ',
    2 => '$vehicleValue             = $_SESSION["_YourDetails_"]["estimated_value"]; $flag = "skp"; 
    if($vehicleValue >= "1000" && $vehicleValue <= "14999") $flag = "acc"; 
    else if($vehicleValue >= "20000" && $vehicleValue <= "29999") $flag = "acc"; 
    else if($vehicleValue >= "35000") $flag = "acc"; 
    if($flag == "skp") $_companyDetails["filter_details"][]["BSK_SKP_VEH_VAL"]["ACC;VAL;EQL;Vehicle Value ACCEPT if 1000-14999 and 20000-29999 and 35000 +"] ="SKP";  ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '4',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '7',
          3 => '8',
          4 => '9',
          5 => '10',
          6 => '11',
          7 => '12',
        ),
      ),
    ),
    2 => 
    array (
      'year_of_manufacture' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2017',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '8',
        ),
      ),
    ),
    5 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'AB[0-99]*',
          1 => 'AL[0-99]*',
          2 => 'B[0-99]*',
          3 => 'BB[0-99]*',
          4 => 'BD[0-99]*',
          5 => 'BL[0-99]*',
          6 => 'BN[0-99]*',
          7 => 'CA[0-99]*',
          8 => 'CB[0-99]*',
          9 => 'CF[0-99]*',
          10 => 'CH[0-99]*',
          11 => 'CM[0-99]*',
          12 => 'CO[0-99]*',
          13 => 'CT[0-99]*',
          14 => 'CV[0-99]*',
          15 => 'CW[0-99]*',
          16 => 'DA[0-99]*',
          17 => 'DD[0-99]*',
          18 => 'DE[0-99]*',
          19 => 'DG[0-99]*',
          20 => 'DH[0-99]*',
          21 => 'DL[0-99]*',
          22 => 'DN[0-99]*',
          23 => 'DY[0-99]*',
          24 => 'E[0-99]*',
          25 => 'EH[0-99]*',
          26 => 'FK[0-99]*',
          27 => 'FY[0-99]*',
          28 => 'G[0-99]*',
          29 => 'GL[0-99]*',
          30 => 'GU[0-99]*',
          31 => 'HD[0-99]*',
          32 => 'HG[0-99]*',
          33 => 'HP[0-99]*',
          34 => 'HR[0-99]*',
          35 => 'HS[0-99]*',
          36 => 'HU[0-99]*',
          37 => 'HX[0-99]*',
          38 => 'IG[0-99]*',
          39 => 'IP[0-99]*',
          40 => 'IV[0-99]*',
          41 => 'KA[0-99]*',
          42 => 'KW[0-99]*',
          43 => 'KY[0-99]*',
          44 => 'LA[0-99]*',
          45 => 'LD[0-99]*',
          46 => 'LE[0-99]*',
          47 => 'LL[0-99]*',
          48 => 'LN[0-99]*',
          49 => 'LS[0-99]*',
          50 => 'LU[0-99]*',
          51 => 'ME[0-99]*',
          52 => 'MK[0-99]*',
          53 => 'ML[0-99]*',
          54 => 'N[0-99]*',
          55 => 'NE[0-99]*',
          56 => 'NG[0-99]*',
          57 => 'NN[0-99]*',
          58 => 'NP[0-99]*',
          59 => 'NR[0-99]*',
          60 => 'OX[0-99]*',
          61 => 'PA[0-99]*',
          62 => 'PE[0-99]*',
          63 => 'PH[0-99]*',
          64 => 'PO[0-99]*',
          65 => 'PR[0-99]*',
          66 => 'RG[0-99]*',
          67 => 'RH[0-99]*',
          68 => 'RM[0-99]*',
          69 => 'S[0-99]*',
          70 => 'SA[0-99]*',
          71 => 'SG[0-99]*',
          72 => 'SL[0-99]*',
          73 => 'SO[0-99]*',
          74 => 'SR[0-99]*',
          75 => 'SS[0-99]*',
          76 => 'ST[0-99]*',
          77 => 'SW[0-99]*',
          78 => 'SY[0-99]*',
          79 => 'TD[0-99]*',
          80 => 'TF[0-99]*',
          81 => 'TN[0-99]*',
          82 => 'TS[0-99]*',
          83 => 'W[0-99]*',
          84 => 'WA[0-99]*',
          85 => 'WD[0-99]*',
          86 => 'WF[0-99]*',
          87 => 'WR[0-99]*',
          88 => 'WS[0-99]*',
          89 => 'WV[0-99]*',
          90 => 'YO[0-99]*',
          91 => 'ZE[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '1237',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '165',
    'companyName' => 'HR Insurance',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'j.elphinstone@hrinsure.co.uk',
        1 => 'QuotezoneTaxiReferrals@hrinsure.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    1 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    3 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 74' => '25-74',
      ),
    ),
    4 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
          1 => '5',
        ),
      ),
    ),
    5 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'AB[0-99]*',
          1 => 'DD[0-99]*',
          2 => 'DG[0-99]*',
          3 => 'EH[0-99]*',
          4 => 'FK[0-99]*',
          5 => 'G[0-99]*',
          6 => 'HS[0-99]*',
          7 => 'IV[0-99]*',
          8 => 'KA[0-99]*',
          9 => 'KW[0-99]*',
          10 => 'KY[0-99]*',
          11 => 'ML[0-99]*',
          12 => 'PA[0-99]*',
          13 => 'PH[0-99]*',
          14 => 'TD[0-99]*',
          15 => 'ZE[0-99]*',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
          6 => '15',
          7 => '16',
        ),
      ),
    ),
    7 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEEN',
          1 => 'ABERDEEN CENTRAL (INVERURIE)',
          2 => 'ABERDEEN CITY (ABERDEEN)',
          3 => 'ABERDEENSHIRE',
          4 => 'ABERDEENSHIRE NORTH (BANFF)',
          5 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          6 => 'ANGUS',
          7 => 'ANGUS COUNCIL (FORFAR)',
          8 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          9 => 'BADENOCH & STRATHSPEY (HIGHLAND - KINGUSSIE)',
          10 => 'BERWICK ON TWEED',
          11 => 'CAITHNESS (HIGHLANDS - WICK)',
          12 => 'CLACKMANANSHIRE (ALLOA)',
          13 => 'DUMFRIES & GALLOWAY',
          14 => 'DUNDEE',
          15 => 'EAST AYRSHIRE (KILMARNOCK)',
          16 => 'EAST KILBRIDE',
          17 => 'EAST LOTHIAN (HADDINGTON)',
          18 => 'FALKIRK',
          19 => 'FIFE COUNCIL (KIRKCALDY)',
          20 => 'HIGHLAND COUNCIL (INVERNESS)',
          21 => 'INVERCLYDE (GREENOCK)',
          22 => 'INVERNESS (HIGHLANDS)',
          23 => 'LOCHABER (HIGHLAND - FORT WILLIAM)',
          24 => 'MIDLOTHIAN COUNCIL',
          25 => 'MORAY (ELGIN)',
          26 => 'NAIRN (HIGHLANDS)',
          27 => 'NORTH AYRSHIRE (IRVINE)',
          28 => 'ORKNEY ISLANDS (KIRKWALL)',
          29 => 'PERTH & KINROSS',
          30 => 'RENFREWSHIRE (PAISLEY)',
          31 => 'ROSS & CROMARTY (HIGHLAND - DINGWALL)',
          32 => 'SCOTTISH BORDERS',
          33 => 'SHETLAND ISLANDS (LERWICK)',
          34 => 'SKYE & LOCHALSH (HIGHLAND - ISLE OF SKYE)',
          35 => 'SOUTH AYRSHIRE (AYR)',
          36 => 'STIRLING',
          37 => 'SUTHERLAND (HIGHLAND - GOLSPIE)',
          38 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          39 => 'WEST LOTHIAN (LIVINGSTON)',
          40 => 'WESTERN ISLES (STORNOWAY)',
        ),
      ),
    ),
  ),
)


 ?>
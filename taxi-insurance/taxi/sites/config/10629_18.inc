<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10629',
    'siteOrigID' => '',
    'brokerID' => '33',
    'logoID' => '91',
    'companyName' => 'One Answer - QZ030 Taxi (OOH)',
    'offline_from_plugin' => false,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'One Answer - QZ030 Taxi (OOH)',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'QZ030_lead@1answer.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => 'include_once "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/taxi/steps/functions.php"; $privateCarNcb = $_SESSION["_YourDetails_"]["private_car_ncb"]; $ageNCBMsg     = "";     $DISdd    = $_SESSION["_YourDetails_"]["date_of_insurance_start_dd"]; $DISmm    = $_SESSION["_YourDetails_"]["date_of_insurance_start_mm"]; $DISyyyy  = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"]; $dobDD    = $_SESSION["_YourDetails_"]["date_of_birth_dd"]; $dobMM    = $_SESSION["_YourDetails_"]["date_of_birth_mm"]; $dobYYYY  = $_SESSION["_YourDetails_"]["date_of_birth_yyyy"]; $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd; $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD; $propAge = GetAge($proposerBirthDate,$insuranceStartDate); if($propAge < 25) {$_companyDetails["filter_details"][]["private_car_ncb"]["SKP;LIST;; If Age < 25 SKIP if Private car no claims bonus = No NCB "] = array(0 => "0",); } ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|19:00-23:59',
          2 => '00:00-09:00|19:00-23:59',
          3 => '00:00-09:00|19:00-23:59',
          4 => '00:00-09:00|19:00-23:59',
          5 => '00:00-09:00|17:00-23:59',
          6 => '00:00-09:00|17:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_type' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '5',
        ),
      ),
    ),
    3 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 2000 and 100000' => ' 2000 - 100000',
      ),
    ),
    4 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '15',
          1 => '16',
        ),
      ),
    ),
    5 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0-14',
      ),
    ),
    6 => 
    array (
      'type_of_cover' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    8 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 20 and 100' => '20-100',
      ),
    ),
    9 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BA[0-99]*',
          1 => 'BH[0-99]*',
          2 => 'BN[0-99]*',
          3 => 'BR[0-99]*',
          4 => 'BS[0-99]*',
          5 => 'CT[0-99]*',
          6 => 'CV[0-99]*',
          7 => 'DA[0-99]*',
          8 => 'DT[0-99]*',
          9 => 'EX[0-99]*',
          10 => 'GL[0-99]*',
          11 => 'GU[0-99]*',
          12 => 'HU[0-99]*',
          13 => 'IP[0-99]*',
          14 => 'ME[0-99]*',
          15 => 'NP[0-99]*',
          16 => 'NR[0-99]*',
          17 => 'OX[0-99]*',
          18 => 'PE[0-99]*',
          19 => 'PL[0-99]*',
          20 => 'PO[0-99]*',
          21 => 'RG[0-99]*',
          22 => 'RH[0-99]*',
          23 => 'S[0-99]*',
          24 => 'SN[0-99]*',
          25 => 'SO[0-99]*',
          26 => 'SP[0-99]*',
          27 => 'SS[0-99]*',
          28 => 'ST[0-99]*',
          29 => 'TA[0-99]*',
          30 => 'TN[0-99]*',
          31 => 'TQ[0-99]*',
          32 => 'TR[0-99]*',
        ),
      ),
    ),
    10 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BARKING',
          1 => 'BARNET',
          2 => 'BEXLEY',
          3 => 'BIRMINGHAM',
          4 => 'BLACKBURN WITH DARWEN',
          5 => 'BOLTON',
          6 => 'BRADFORD',
          7 => 'BRENT',
          8 => 'BROMLEY',
          9 => 'BURNLEY',
          10 => 'CAMDEN',
          11 => 'CRAWLEY',
          12 => 'CROYDEN',
          13 => 'EALING',
          14 => 'ENFIELD',
          15 => 'GREENWICH',
          16 => 'HACKNEY',
          17 => 'HAMMERSMITH',
          18 => 'HARINGEY',
          19 => 'HAVERING',
          20 => 'HILLINGDON',
          21 => 'HOUNSLOW',
          22 => 'ISLINGTON',
          23 => 'KENSINGTON',
          24 => 'KINGSTON UPON THAMES ROYAL',
          25 => 'KNOWSLEY',
          26 => 'LAMBETH',
          27 => 'LEEDS',
          28 => 'LEWISHAM',
          29 => 'LIVERPOOL',
          30 => 'LONDON PCO',
          31 => 'LUTON',
          32 => 'MANCHESTER',
          33 => 'MERTON',
          34 => 'MIDDLESBOROUGH',
          35 => 'MILTON KEYNES',
          36 => 'NEWHAM',
          37 => 'PRESTON',
          38 => 'REDBRIDGE',
          39 => 'RICHMOND',
          40 => 'ROSSENDALE',
          41 => 'SLOUGH',
          42 => 'SOLIHULL',
          43 => 'SOUTHWARK',
          44 => 'SUTTON',
          45 => 'TOWER HAMLETS',
          46 => 'WALTHAM FOREST',
          47 => 'WANDSWORTH',
          48 => 'WARRINGTON',
          49 => 'WESTMINSTER',
        ),
      ),
    ),
    11 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
  ),
)


 ?>
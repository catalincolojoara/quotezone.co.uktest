<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10609',
    'siteOrigID' => '',
    'brokerID' => '96',
    'logoID' => '262',
    'companyName' => 'Insurance Factory (Marker Study) - Taxi 2',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Insurance Factory (Marker Study) - Taxi 2',
    'limit' => 
    array (
      'day' => '1',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'shopquotes@insurancefactory.co.uk',
        1 => 'callcampaign@insurancefactory.co.uk',
        2 => 'LeadGen@insurancefactory.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '4',
        ),
      ),
    ),
    1 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    2 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 3000 and 40000' => '3000 - 40000',
      ),
    ),
    3 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 18' => '0-18',
      ),
    ),
    4 => 
    array (
      'taxi_used_for' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '3',
        ),
      ),
    ),
    5 => 
    array (
      'uber_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'Y',
        ),
      ),
    ),
    6 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 27 and 67' => '27-67',
      ),
    ),
    7 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEEN',
          1 => 'ABERDEEN CENTRAL (INVERURIE)',
          2 => 'ABERDEEN CITY (ABERDEEN)',
          3 => 'ABERDEENSHIRE',
          4 => 'ABERDEENSHIRE NORTH (BANFF)',
          5 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          6 => 'ANGUS',
          7 => 'ANGUS COUNCIL (FORFAR)',
          8 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          9 => 'BADENOCH & STRATHSPEY (HIGHLAND - KINGUSSIE)',
          10 => 'BERWICK ON TWEED',
          11 => 'CAITHNESS (HIGHLANDS - WICK)',
          12 => 'CLACKMANANSHIRE (ALLOA)',
          13 => 'DUMFRIES & GALLOWAY',
          14 => 'DUNDEE',
          15 => 'EAST AYRSHIRE (KILMARNOCK)',
          16 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          17 => 'EAST KILBRIDE',
          18 => 'EAST LOTHIAN (HADDINGTON)',
          19 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          20 => 'EDINBURGH',
          21 => 'FALKIRK',
          22 => 'FIFE COUNCIL (KIRKCALDY)',
          23 => 'HIGHLAND COUNCIL (INVERNESS)',
          24 => 'INVERCLYDE (GREENOCK)',
          25 => 'LOCHABER (HIGHLAND - FORT WILLIAM)',
          26 => 'MIDLOTHIAN COUNCIL',
          27 => 'MORAY (ELGIN)',
          28 => 'NAIRN (HIGHLANDS)',
          29 => 'NEWCASTLE UPON TYNE',
          30 => 'NORTH AYRSHIRE (IRVINE)',
          31 => 'NORTH LANARKSHIRE (MOTHERWELL)',
          32 => 'NORTH LANARKSHIRE(SOUTH/CENTRAL)',
          33 => 'ORKNEY ISLANDS (KIRKWALL)',
          34 => 'PERTH & KINROSS',
          35 => 'RENFREWSHIRE (PAISLEY)',
          36 => 'ROSS & CROMARTY (HIGHLAND - DINGWALL)',
          37 => 'SCOTTISH BORDERS',
          38 => 'SHETLAND ISLANDS (LERWICK)',
          39 => 'SKYE & LOCHALSH (HIGHLAND - ISLE OF SKYE)',
          40 => 'SOUTH AYRSHIRE (AYR)',
          41 => 'SOUTH LANARKSHIRE',
          42 => 'SOUTH LANARKSHIRE (HAMILTON)',
          43 => 'SOUTH LANARKSHIRE (LANARK)',
          44 => 'SOUTH LANARKSHIRE (RUTHERGLEN)',
          45 => 'STIRLING',
          46 => 'SUTHERLAND (HIGHLAND - GOLSPIE)',
          47 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          48 => 'WEST LOTHIAN (LIVINGSTON)',
          49 => 'WESTERN ISLES (STORNOWAY)',
        ),
      ),
    ),
    8 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
          5 => '9',
          6 => '10',
          7 => '11',
          8 => '12',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12434',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '439',
    'companyName' => 'Adelphi Insurance',
    'offline_from_plugin' => true,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '10',
      'hour' => '1',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'sales@adelphi-insurance.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
    'url' => '1',
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
        ),
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 21 and 75' => '21 - 75',
      ),
    ),
    3 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BL[0-99]*',
          3 => 'DH[0-99]*',
          4 => 'HD[0-99]*',
          5 => 'HX[0-99]*',
          6 => 'L[0-99]*',
          7 => 'LE[0-99]*',
          8 => 'LS[0-99]*',
          9 => 'M[0-99]*',
          10 => 'NE[0-99]*',
          11 => 'OL[0-99]*',
          12 => 'PR[0-99]*',
          13 => 'SR[0-99]*',
          14 => 'TS[0-99]*',
          15 => 'WA[0-99]*',
          16 => 'WN[0-99]*',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 8' => '8',
      ),
    ),
    5 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
  ),
)


 ?>
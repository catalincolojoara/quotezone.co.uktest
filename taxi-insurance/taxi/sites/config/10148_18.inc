<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10148',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '448',
    'companyName' => 'Brightside (One Insurance Solution) (Scotland)',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Taxi Query (Scotland): ',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxi.info@oneinsurancesolution.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
          3 => '10',
          4 => '11',
          5 => '12',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 100' => '25-100',
      ),
    ),
    2 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
        ),
      ),
    ),
    3 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEENSHIRE',
          1 => 'ANGUS',
          2 => 'ARGYLE & BUTE',
          3 => 'CLACKMANNANSHIRE',
          4 => 'DUMFRIES & GALLOWAY',
          5 => 'DUNDEE',
          6 => 'EAST AYRSHIRE (KILMARNOCK)',
          7 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          8 => 'EAST LOTHIAN (HADDINGTON)',
          9 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          10 => 'EDINBURGH',
          11 => 'FALKIRK',
          12 => 'FIFE',
          13 => 'GLASGOW',
          14 => 'INVERNESS (HIGHLANDS)',
          15 => 'MORAY',
          16 => 'NORTH AYRSHIRE',
          17 => 'NORTH LANARKSHIRE (NORTH)',
          18 => 'ORKNEY ISLANDS (KIRKWALL)',
          19 => 'PERTH & KINROSS',
          20 => 'RENFREWSHIRE (PAISLEY)',
          21 => 'SCOTTISH BORDERS',
          22 => 'SHETLAND ISLANDS (LERWICK)',
          23 => 'SOUTH AYRSHIRE (AYR)',
          24 => 'SOUTH LANARKSHIRE',
          25 => 'STIRLING',
          26 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          27 => 'WEST LOTHIAN (LIVINGSTON)',
        ),
      ),
    ),
    4 => 
    array (
      'engine_size' => 
      array (
        'SKP;LIST;;Skip electric vehicles.' => 
        array (
          0 => '0',
          1 => '0 ',
          2 => '0 cc',
          3 => '0 CC',
          4 => '0 E',
          5 => '0 e',
        ),
      ),
    ),
  ),
)


 ?>
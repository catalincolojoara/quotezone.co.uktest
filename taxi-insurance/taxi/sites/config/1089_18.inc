<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '1089',
    'siteOrigID' => '',
    'brokerID' => '57',
    'logoID' => '615',
    'companyName' => 'Milestone - Taxi (OOH)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Milestone - Taxi (OOH)',
    'limit' => 
    array (
      'day' => '15',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'Rob.g@milestonehouse.com',
        1 => 'Nathan.t@milestonehouse.com',
        2 => 'Brenda.b@milestonehouse.com',
        3 => 'Caroline.w@milestonehouse.com',
        4 => 'Shauna.s@milestonehouse.com',
        5 => 'Natalie.g@milestonehouse.com',
        6 => 'Jack.w@milestonehouse.com',
        7 => 'James.h@milestonehouse.com',
        8 => 'Matthew.o@milestonehouse.com',
        9 => 'red@rcdata.co.uk',
        10 => 'Nicholas.l@milestonehouse.com',
        11 => 'Zoe.h@milestonehouse.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|17:15-23:59',
          2 => '00:00-09:00|17:15-23:59',
          3 => '00:00-09:00|17:15-23:59',
          4 => '00:00-09:00|17:15-23:59',
          5 => 'SKP_ALL_DAY',
          6 => 'SKP_ALL_DAY',
          7 => '17:15-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 21 and 69' => ' 21 - 69',
      ),
    ),
    2 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'LONDON PCO',
          1 => 'WOLVERHAMPTON',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BL[0-99]*',
          4 => 'BR[0-99]*',
          5 => 'BT[0-99]*',
          6 => 'CF[0-99]*',
          7 => 'CR[0-99]*',
          8 => 'CV[0-99]*',
          9 => 'DA[0-99]*',
          10 => 'DE[0-99]*',
          11 => 'DY[0-99]*',
          12 => 'E[0-99]*',
          13 => 'EC[0-99]*',
          14 => 'EN[0-99]*',
          15 => 'HA[0-99]*',
          16 => 'IG[0-99]*',
          17 => 'KT[0-99]*',
          18 => 'L[0-99]*',
          19 => 'LU[0-99]*',
          20 => 'M[0-99]*',
          21 => 'ME[0-99]*',
          22 => 'N[0-99]*',
          23 => 'NE[0-99]*',
          24 => 'NG[0-99]*',
          25 => 'NW[0-99]*',
          26 => 'OL[0-99]*',
          27 => 'PR[0-99]*',
          28 => 'RM[0-99]*',
          29 => 'SE[0-99]*',
          30 => 'SK[0-99]*',
          31 => 'SL[0-99]*',
          32 => 'SM[0-99]*',
          33 => 'SR[0-99]*',
          34 => 'SS[0-99]*',
          35 => 'ST[0-99]*',
          36 => 'SW[0-99]*',
          37 => 'TS[0-99]*',
          38 => 'TW[0-99]*',
          39 => 'UB[0-99]*',
          40 => 'W[0-99]*',
          41 => 'WA[0-99]*',
          42 => 'WC[0-99]*',
          43 => 'WD[0-99]*',
          44 => 'WN[0-99]*',
          45 => 'WS[0-99]*',
          46 => 'WV[0-99]*',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 15' => '15',
      ),
    ),
    6 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 21' => '0-21',
      ),
    ),
    7 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '511',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '558',
    'companyName' => 'Cover My Cab - Taxi (NSC OH)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Cover My Cab - Taxi NSC (Quotezone Prime)',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotezone@covermycab.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 28 and 69' => '28-69',
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 15' => '15',
      ),
    ),
    4 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '4',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '10',
          1 => '11',
          2 => '12',
          3 => '13',
          4 => '14',
        ),
      ),
    ),
    6 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'AB[0-99]*',
          1 => 'BB[0-99]*',
          2 => 'BD[0-99]*',
          3 => 'BL[0-99]*',
          4 => 'BT[0-99]*',
          5 => 'CW[0-99]*',
          6 => 'DD[0-99]*',
          7 => 'DG[0-99]*',
          8 => 'DH[0-99]*',
          9 => 'DY[0-99]*',
          10 => 'EH[0-99]*',
          11 => 'FK[0-99]*',
          12 => 'FY[0-99]*',
          13 => 'G[0-99]*',
          14 => 'HG[0-99]*',
          15 => 'HS[0-99]*',
          16 => 'HX[0-99]*',
          17 => 'IV[0-99]*',
          18 => 'KA[0-99]*',
          19 => 'KW[0-99]*',
          20 => 'KY[0-99]*',
          21 => 'L[0-99]*',
          22 => 'LS[0-99]*',
          23 => 'M[1-18]*',
          24 => 'ML[0-99]*',
          25 => 'NW[0-99]*',
          26 => 'PA[0-99]*',
          27 => 'PH[0-99]*',
          28 => 'SE[0-99]*',
          29 => 'SK[0-99]*',
          30 => 'SR[0-99]*',
          31 => 'SW[0-99]*',
          32 => 'TD[0-99]*',
          33 => 'TR[0-99]*',
          34 => 'TS[0-99]*',
          35 => 'W[0-99]*',
          36 => 'WA[0-99]*',
          37 => 'WC[0-99]*',
          38 => 'WF[0-99]*',
          39 => 'WN[0-99]*',
          40 => 'ZE[0-99]*',
          41 => 'GY[0-99]*',
          42 => 'JE[0-99]*',
          43 => 'IM[0-99]*',
        ),
      ),
    ),
    7 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 30' => '0 - 30',
      ),
    ),
    8 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'ABERDEEN CITY (ABERDEEN)',
          1 => 'ABERDEENSHIRE',
          2 => 'ABERDEENSHIRE NORTH (BANFF)',
          3 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          4 => 'ALNWICK',
          5 => 'ANGUS',
          6 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          7 => 'BADENOCH & STRATHSPEY (HIGHLAND - KINGUSSIE)',
          8 => 'BELFAST',
          9 => 'BERWICK ON TWEED',
          10 => 'BIRMINGHAM',
          11 => 'BLACKBURN WITH DARWEN',
          12 => 'BLYTH VALLEY',
          13 => 'BOLTON',
          14 => 'BRADFORD',
          15 => 'BURNLEY',
          16 => 'BURY',
          17 => 'CAITHNESS (HIGHLANDS - WICK)',
          18 => 'CALDERDALE',
          19 => 'CARADON',
          20 => 'CARRICK',
          21 => 'CHESTER-LE-STREET',
          22 => 'CLACKMANANSHIRE (ALLOA)',
          23 => 'CREWE & NANTWICH',
          24 => 'DUMFRIES & GALLOWAY',
          25 => 'DUNDEE',
          26 => 'DURHAM',
          27 => 'DVA (NI)',
          28 => 'EASINGTON',
          29 => 'EAST AYRSHIRE (KILMARNOCK)',
          30 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          31 => 'EAST KILBRIDE',
          32 => 'EAST LOTHIAN (HADDINGTON)',
          33 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          34 => 'EDINBURGH',
          35 => 'ELLESEMERE PORT & NESTON',
          36 => 'FALKIRK',
          37 => 'FIFE COUNCIL (KIRKCALDY)',
          38 => 'FLINTSHIRE',
          39 => 'GATESHEAD',
          40 => 'GLASGOW CITY',
          41 => 'GUERNSEY',
          42 => 'HIGHLAND COUNCIL (INVERNESS)',
          43 => 'HYNDBURN',
          44 => 'INVERCLYDE (GREENOCK)',
          45 => 'ISLE OF MAN',
          46 => 'ISLE OF SCILLY',
          47 => 'JERSEY',
          48 => 'KENNET',
          49 => 'KERRIER',
          50 => 'LEEDS',
          51 => 'LIVERPOOL',
          52 => 'LOCHABER (HIGHLAND - FORT WILLIAM)',
          53 => 'LONDON PCO',
          54 => 'MACCLESFIELD / CHESHIRE EAST',
          55 => 'MANCHESTER',
          56 => 'MERTHYR TYDFIL',
          57 => 'MID BEDFORDSHIRE',
          58 => 'MIDLOTHIAN COUNCIL',
          59 => 'MORAY (ELGIN)',
          60 => 'NAIRN (HIGHLANDS)',
          61 => 'NEATH & PORT TALBOT',
          62 => 'NEWCASTLE UPON TYNE',
          63 => 'NEWPORT',
          64 => 'NORTH AYRSHIRE (IRVINE)',
          65 => 'NORTH LANARKSHIRE (MOTHERWELL)',
          66 => 'NORTH LANARKSHIRE(SOUTH/CENTRAL)',
          67 => 'NORTH TYNESIDE',
          68 => 'NORTH WILTSHIRE',
          69 => 'NORTHERN IRELAND',
          70 => 'NORTHUMBERLAND',
          71 => 'OLDHAM',
          72 => 'ORKNEY ISLANDS (KIRKWALL)',
          73 => 'PENWITH',
          74 => 'PERTH & KINROSS',
          75 => 'RENFREWSHIRE (PAISLEY)',
          76 => 'RESTORMEL',
          77 => 'ROCHDALE',
          78 => 'ROSS & CROMARTY (HIGHLAND - DINGWALL)',
          79 => 'ROSSENDALE',
          80 => 'SALFORD',
          81 => 'SALISBURY',
          82 => 'SCOTTISH BORDERS',
          83 => 'SEDGEFIELD',
          84 => 'SHETLAND ISLANDS (LERWICK)',
          85 => 'SKYE & LOCHALSH (HIGHLAND - ISLE OF SKYE)',
          86 => 'SOUTH AYRSHIRE (AYR)',
          87 => 'SOUTH BEDFORDSHIRE',
          88 => 'SOUTH EAST & METROPOLITAN',
          89 => 'SOUTH LANARKSHIRE',
          90 => 'SOUTH LANARKSHIRE (HAMILTON)',
          91 => 'SOUTH LANARKSHIRE (LANARK)',
          92 => 'SOUTH LANARKSHIRE (RUTHERGLEN)',
          93 => 'SOUTH TYNESIDE',
          94 => 'STIRLING',
          95 => 'SUNDERLAND',
          96 => 'SUTHERLAND (HIGHLAND - GOLSPIE)',
          97 => 'SWANSEA',
          98 => 'TAMESIDE',
          99 => 'VALE ROYAL',
          100 => 'WAKEFIELD',
          101 => 'WALSALL',
          102 => 'WANSBECK',
          103 => 'WEAR VALLEY',
          104 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          105 => 'WEST LOTHIAN (LIVINGSTON)',
          106 => 'WEST WILTSHIRE',
          107 => 'WESTERN ISLES (STORNOWAY)',
          108 => 'WIGAN',
          109 => 'WREXHAM',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '1634',
    'siteOrigID' => '',
    'brokerID' => '75',
    'logoID' => '368',
    'companyName' => 'Connect Insurance 9',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '6',
    ),
    'emailSubject' => 'Taxi 9 Query:',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'LeadTX@connect-insurance.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_driver' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '5',
        ),
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 74' => '25 - 74',
      ),
    ),
    3 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
          6 => '15',
          7 => '16',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BD8',
        ),
      ),
    ),
    5 => 
    array (
      'type_of_cover' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '2',
          1 => '3',
          2 => '4',
        ),
      ),
    ),
    6 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 9' => '9',
      ),
    ),
    8 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    9 => 
    array (
      'private_car_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
        ),
      ),
    ),
    10 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
          3 => '4',
        ),
      ),
    ),
    11 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEEN',
          1 => 'ABERDEEN CENTRAL (INVERURIE)',
          2 => 'ABERDEEN CITY (ABERDEEN)',
          3 => 'ABERDEENSHIRE',
          4 => 'ABERDEENSHIRE NORTH (BANFF)',
          5 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          6 => 'ADUR',
          7 => 'ALLERDALE',
          8 => 'ALNWICK',
          9 => 'ANGUS',
          10 => 'ANGUS COUNCIL (FORFAR)',
          11 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          12 => 'AYLESBURY VALE',
          13 => 'BABERGH',
          14 => 'BATH & NORTH EAST SOMERSET',
          15 => 'BERWICK ON TWEED',
          16 => 'BLACKPOOL',
          17 => 'BLYTH VALLEY',
          18 => 'BOSTON',
          19 => 'BOURNEMOUTH',
          20 => 'BRECKLAND',
          21 => 'BRIDGENORTH',
          22 => 'BRIGHTON & HOVE',
          23 => 'BROADLAND',
          24 => 'CARADON',
          25 => 'CARMARTHENSHIRE',
          26 => 'CARRICK',
          27 => 'CASTLE MORPETH',
          28 => 'CEREDIGION',
          29 => 'CHICHESTER',
          30 => 'CLACKMANANSHIRE (ALLOA)',
          31 => 'CONGLETON',
          32 => 'CONGLETON / CHESHIRE EAST',
          33 => 'DUMFRIES & GALLOWAY',
          34 => 'DUNDEE',
          35 => 'EAST AYRSHIRE (KILMARNOCK)',
          36 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          37 => 'EAST LOTHIAN (HADDINGTON)',
          38 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          39 => 'EDEN',
          40 => 'EDINBURGH',
          41 => 'FALKIRK',
          42 => 'FIFE COUNCIL (KIRKCALDY)',
          43 => 'FLINTSHIRE',
          44 => 'FOREST HEATH',
          45 => 'FOREST OF DEAN',
          46 => 'GREAT YARMOUTH',
          47 => 'GUERNSEY',
          48 => 'GWYNEDD',
          49 => 'HAMBLETON',
          50 => 'HARROGATE',
          51 => 'HEREFORDSHIRE',
          52 => 'HIGHLAND COUNCIL (INVERNESS)',
          53 => 'INVERCLYDE (GREENOCK)',
          54 => 'INVERNESS (HIGHLANDS)',
          55 => 'ISLE OF ANGLESEY',
          56 => 'ISLE OF MAN',
          57 => 'ISLE OF WIGHT',
          58 => 'KENNET',
          59 => 'KERRIER',
          60 => 'KINGS LYNN & WEST NORFOLK',
          61 => 'LEOMINSTER',
          62 => 'MACCLESFIELD / CHESHIRE EAST',
          63 => 'MALVERN HILLS',
          64 => 'MENDIP',
          65 => 'MID DEVON',
          66 => 'MID SUFFOLK',
          67 => 'MIDLOTHIAN COUNCIL',
          68 => 'MONMOUTHSHIRE',
          69 => 'MORAY (ELGIN)',
          70 => 'NEWCASTLE UNDER LYME',
          71 => 'NORTH AYRSHIRE (IRVINE)',
          72 => 'NORTH CORNWALL',
          73 => 'NORTH DEVON',
          74 => 'NORTH DORSET',
          75 => 'NORTH EAST LINCOLNSHIRE',
          76 => 'NORTH LANARKSHIRE (MOTHERWELL)',
          77 => 'NORTH LANARKSHIRE(SOUTH/CENTRAL)',
          78 => 'NORTH NORFOLK',
          79 => 'NORTH SHROPSHIRE',
          80 => 'NORTH SOMERSET',
          81 => 'NORTH WILTSHIRE',
          82 => 'NORTHUMBERLAND',
          83 => 'ORKNEY ISLANDS (KIRKWALL)',
          84 => 'PEMBROKESHIRE',
          85 => 'PENWITH',
          86 => 'PERTH & KINROSS',
          87 => 'PLYMOUTH',
          88 => 'POWYS',
          89 => 'PURBECK',
          90 => 'RENFREWSHIRE (PAISLEY)',
          91 => 'RESTORMEL',
          92 => 'RUTLAND',
          93 => 'RYEDALE',
          94 => 'SALISBURY',
          95 => 'SCARBOROUGH',
          96 => 'SCOTTISH BORDERS',
          97 => 'SHETLAND ISLANDS (LERWICK)',
          98 => 'SHREWSBURY',
          99 => 'SHREWSBURY & ATCHAM / SHROPSHIRE COUNCIL',
          100 => 'SHROPSHIRE COUNTY',
          101 => 'SOUTH AYRSHIRE (AYR)',
          102 => 'SOUTH GLOUCESTER',
          103 => 'SOUTH LANARKSHIRE',
          104 => 'SOUTH LANARKSHIRE (HAMILTON)',
          105 => 'SOUTH LANARKSHIRE (LANARK)',
          106 => 'SOUTH LANARKSHIRE (RUTHERGLEN)',
          107 => 'SOUTH NORTHAMPTONSHIRE',
          108 => 'SOUTH SHROPSHIRE',
          109 => 'SOUTH SOMERSET',
          110 => 'SPELTHORNE',
          111 => 'ST EDMUNDSBURY',
          112 => 'STIRLING',
          113 => 'STROUD',
          114 => 'SUFFOLK COASTAL',
          115 => 'TAUNTON DEANE',
          116 => 'TEIGNBRIDGE',
          117 => 'TELFORD & WREKIN',
          118 => 'TEWKESBURY',
          119 => 'TORBAY',
          120 => 'TORFAEN',
          121 => 'TORRIDGE',
          122 => 'TYNEDALE',
          123 => 'WANSBECK',
          124 => 'WAVENEY',
          125 => 'WEST DEVON',
          126 => 'WEST DORSET',
          127 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          128 => 'WEST LINDSEY',
          129 => 'WEST LOTHIAN (LIVINGSTON)',
          130 => 'WEST SOMERSET',
          131 => 'WEST WILTSHIRE',
          132 => 'WESTERN ISLES (STORNOWAY)',
          133 => 'WEYMOUTH & PORTLAND',
          134 => 'WILTSHIRE COUNTY',
          135 => 'WORCESTER',
          136 => 'WREKIN',
          137 => 'WYCHAVON',
          138 => 'YORK',
        ),
      ),
    ),
    12 => 
    array (
      'registration_number_question' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'N',
        ),
      ),
    ),
  ),
)


 ?>
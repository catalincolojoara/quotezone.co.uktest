<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12850',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '558',
    'companyName' => 'Cover My Cab - Taxi (TBU OH)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Cover My Cab - Taxi TBU (Quotezone Prime)',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'tbu@covermycab.com',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 21 and 74' => '21-74',
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '9',
          1 => '10',
          2 => '11',
          3 => '12',
          4 => '13',
          5 => '14',
          6 => '15',
          7 => '16',
        ),
      ),
    ),
    4 => 
    array (
      'type_of_cover' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '4',
        ),
      ),
    ),
    5 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BT[0-99]*',
        ),
      ),
    ),
    6 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'ABERDEEN CITY (ABERDEEN)',
          1 => 'ABERDEENSHIRE',
          2 => 'ABERDEENSHIRE NORTH (BANFF)',
          3 => 'ABERDEENSHIRE SOUTH (STONEHAVEN)',
          4 => 'ALNWICK',
          5 => 'ANGUS',
          6 => 'ARGYLL & BUTE (LOCHGILPHEAD)',
          7 => 'BADENOCH & STRATHSPEY (HIGHLAND - KINGUSSIE)',
          8 => 'BERWICK ON TWEED',
          9 => 'BIRMINGHAM',
          10 => 'BLACKBURN WITH DARWEN',
          11 => 'BLYTH VALLEY',
          12 => 'BOLTON',
          13 => 'BRADFORD',
          14 => 'BURNLEY',
          15 => 'BURY',
          16 => 'CAITHNESS (HIGHLANDS - WICK)',
          17 => 'CALDERDALE',
          18 => 'CARADON',
          19 => 'CARRICK',
          20 => 'CHESTER-LE-STREET',
          21 => 'CLACKMANANSHIRE (ALLOA)',
          22 => 'CREWE & NANTWICH',
          23 => 'DUMFRIES & GALLOWAY',
          24 => 'DUNDEE',
          25 => 'DURHAM',
          26 => 'EASINGTON',
          27 => 'EAST AYRSHIRE (KILMARNOCK)',
          28 => 'EAST DUNBARTONSHIRE (KIRKINTILLOCH)',
          29 => 'EAST KILBRIDE',
          30 => 'EAST LOTHIAN (HADDINGTON)',
          31 => 'EAST RENFREWSHIRE (GIFFNOCH)',
          32 => 'EDINBURGH',
          33 => 'ELLESEMERE PORT & NESTON',
          34 => 'FALKIRK',
          35 => 'FIFE COUNCIL (KIRKCALDY)',
          36 => 'FLINTSHIRE',
          37 => 'GATESHEAD',
          38 => 'GLASGOW CITY',
          39 => 'GUERNSEY',
          40 => 'HIGHLAND COUNCIL (INVERNESS)',
          41 => 'HYNDBURN',
          42 => 'INVERCLYDE (GREENOCK)',
          43 => 'ISLE OF MAN',
          44 => 'ISLE OF SCILLY',
          45 => 'JERSEY',
          46 => 'KENNET',
          47 => 'KERRIER',
          48 => 'LEEDS',
          49 => 'LIVERPOOL',
          50 => 'LOCHABER (HIGHLAND - FORT WILLIAM)',
          51 => 'MACCLESFIELD / CHESHIRE EAST',
          52 => 'MANCHESTER',
          53 => 'MERTHYR TYDFIL',
          54 => 'MID BEDFORDSHIRE',
          55 => 'MIDLOTHIAN COUNCIL',
          56 => 'MORAY (ELGIN)',
          57 => 'NAIRN (HIGHLANDS)',
          58 => 'NEATH & PORT TALBOT',
          59 => 'NEWCASTLE UPON TYNE',
          60 => 'NEWPORT',
          61 => 'NORTH AYRSHIRE (IRVINE)',
          62 => 'NORTH LANARKSHIRE (MOTHERWELL)',
          63 => 'NORTH LANARKSHIRE(SOUTH/CENTRAL)',
          64 => 'NORTH TYNESIDE',
          65 => 'NORTH WILTSHIRE',
          66 => 'NORTHUMBERLAND',
          67 => 'OLDHAM',
          68 => 'ORKNEY ISLANDS (KIRKWALL)',
          69 => 'PENWITH',
          70 => 'PERTH & KINROSS',
          71 => 'RENFREWSHIRE (PAISLEY)',
          72 => 'RESTORMEL',
          73 => 'ROCHDALE',
          74 => 'ROSS & CROMARTY (HIGHLAND - DINGWALL)',
          75 => 'ROSSENDALE',
          76 => 'SALFORD',
          77 => 'SALISBURY',
          78 => 'SCOTTISH BORDERS',
          79 => 'SEDGEFIELD',
          80 => 'SHETLAND ISLANDS (LERWICK)',
          81 => 'SKYE & LOCHALSH (HIGHLAND - ISLE OF SKYE)',
          82 => 'SOUTH AYRSHIRE (AYR)',
          83 => 'SOUTH BEDFORDSHIRE',
          84 => 'SOUTH EAST & METROPOLITAN',
          85 => 'SOUTH LANARKSHIRE',
          86 => 'SOUTH LANARKSHIRE (HAMILTON)',
          87 => 'SOUTH LANARKSHIRE (LANARK)',
          88 => 'SOUTH LANARKSHIRE (RUTHERGLEN)',
          89 => 'SOUTH TYNESIDE',
          90 => 'STIRLING',
          91 => 'SUNDERLAND',
          92 => 'SUTHERLAND (HIGHLAND - GOLSPIE)',
          93 => 'SWANSEA',
          94 => 'TAMESIDE',
          95 => 'VALE ROYAL',
          96 => 'WAKEFIELD',
          97 => 'WALSALL',
          98 => 'WANSBECK',
          99 => 'WEAR VALLEY',
          100 => 'WEST DUNBARTONSHIRE (CLYDEBANK)',
          101 => 'WEST LOTHIAN (LIVINGSTON)',
          102 => 'WEST WILTSHIRE',
          103 => 'WESTERN ISLES (STORNOWAY)',
          104 => 'WIGAN',
          105 => 'WREXHAM',
        ),
      ),
    ),
  ),
)


 ?>
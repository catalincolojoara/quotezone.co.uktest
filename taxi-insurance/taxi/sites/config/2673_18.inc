<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '2673',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '721',
    'companyName' => 'Patons NW Local OOH',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '15',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxileads@patonsinsurance.co.uk',
        1 => 'taxi@patonsquotes.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|17:00-23:59',
          2 => '00:00-09:00|17:00-23:59',
          3 => '00:00-09:00|17:00-23:59',
          4 => '00:00-09:00|17:00-23:59',
          5 => '00:00-09:00|17:00-23:59',
          6 => '00:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'WLID' => 
      array (
        'SKP;VAL;EQL;Skip if source is Danny Imray Two' => '758',
      ),
    ),
    2 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 75' => '25 - 75',
      ),
    ),
    3 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
    4 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 23' => '0 - 23',
      ),
    ),
    5 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BURY',
          1 => 'CHESHIRE EAST COUNCIL',
          2 => 'CHESHIRE WEST & CHESTER (BLUE) CHESTER',
          3 => 'CHESHIRE WEST & CHESTER (GREEN) VALE ROYAL',
          4 => 'CHESHIRE WEST & CHESTER (RED) ELLESMERE PORT',
          5 => 'CHORLEY',
          6 => 'CONGLETON / CHESHIRE EAST',
          7 => 'CREWE & NANTWICH',
          8 => 'FLINTSHIRE',
          9 => 'FYLDE',
          10 => 'HALTON',
          11 => 'HIGH PEAK',
          12 => 'KNOWSLEY',
          13 => 'LANCASTER',
          14 => 'LIVERPOOL',
          15 => 'MACCLESFIELD / CHESHIRE EAST',
          16 => 'MANCHESTER',
          17 => 'PENDLE',
          18 => 'PRESTON',
          19 => 'RIBBLE VALLEY',
          20 => 'SALFORD',
          21 => 'SEFTON',
          22 => 'SOUTH RIBBLE',
          23 => 'ST HELENS',
          24 => 'STOCKPORT',
          25 => 'TAMESIDE',
          26 => 'TRAFFORD',
          27 => 'WARRINGTON',
          28 => 'WEST LANCASHIRE',
          29 => 'WIGAN',
          30 => 'WYRE',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '9675',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '571',
    'companyName' => '2insure',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '5',
      'hour' => '1',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'email2insure@hotmail.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-15:00',
          2 => '09:00-15:00',
          3 => '09:00-15:00',
          4 => '09:00-15:00',
          5 => '09:00-15:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[1-71]',
          1 => 'BB[0-99]',
          2 => 'BD[0-99]',
          3 => 'BL[0-99]',
          4 => 'BT[0-99]',
          5 => 'DY[0-99]',
          6 => 'EH[0-99]',
          7 => 'G[0-99]',
          8 => 'HD[0-99]',
          9 => 'L[0-99]',
          10 => 'LS[0-99]',
          11 => 'M[0-99]',
          12 => 'NE[0-99]',
          13 => 'OL[0-99]',
          14 => 'S[0-99]',
          15 => 'SK[0-99]',
          16 => 'TS[0-99]',
          17 => 'WF[0-99]',
          18 => 'WN[0-99]',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 22 and 100' => '22-100',
      ),
    ),
    5 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
        ),
      ),
    ),
    8 => 
    array (
      'year_of_manufacture' => 
      array (
        'SKP;VAL;MAX;SKIP if Year of manufacture < 2006 e.g. skip 2005, 2004. ' => '2006',
      ),
    ),
    9 => 
    array (
      'mobile_telephone' => 
      array (
        'SKP;VAL;MAX;Only ACCEPT if Mobile phone contains a number. ' => '1',
      ),
    ),
    10 => 
    array (
      'taxi_plating' => 
      array (
        'SKP;LIST;;SKIP if Plating Authority = London PCO' => 
        array (
          0 => 'LONDON PCO',
        ),
      ),
    ),
    11 => 
    array (
      'DIS' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 14' => '14',
      ),
    ),
    12 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    13 => 
    array (
      'convictions_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10717',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '89',
    'companyName' => 'Velos - Taxi',
    'offline_from_plugin' => true,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'motor@velosgroup.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '08:30-17:30',
          2 => '08:30-17:30',
          3 => '08:30-17:30',
          4 => '08:30-17:30',
          5 => '08:30-17:30',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '4',
          1 => '5',
          2 => '6',
          3 => '7',
          4 => '8',
          5 => '9',
          6 => '10',
          7 => '11',
          8 => '12',
          9 => '13',
        ),
      ),
    ),
    2 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BIRMINGHAM',
          1 => 'BLACKBURN WITH DARWEN',
          2 => 'BLACKPOOL',
          3 => 'BRADFORD',
          4 => 'BURNLEY',
          5 => 'ELLESEMERE PORT & NESTON',
          6 => 'GATESHEAD',
          7 => 'HYNDBURN',
          8 => 'KNOWSLEY',
          9 => 'LEEDS',
          10 => 'LIVERPOOL',
          11 => 'LONDON PCO',
          12 => 'LUTON',
          13 => 'MANCHESTER',
          14 => 'MERTON',
          15 => 'MIDDLESBOROUGH',
          16 => 'NEWCASTLE UPON TYNE',
          17 => 'NORTH TYNESIDE',
          18 => 'NOTTINGHAM',
          19 => 'PRESTON',
          20 => 'REDCAR & CLEVELAND',
          21 => 'ROCHDALE',
          22 => 'RUSHCLIFFE',
          23 => 'SALFORD',
          24 => 'SANDWELL',
          25 => 'SEFTON',
          26 => 'SHEFFIELD',
          27 => 'SOUTH EAST & METROPOLITAN',
          28 => 'SOUTH RIBBLE',
          29 => 'SOUTH TYNESIDE',
          30 => 'SUNDERLAND',
          31 => 'TAMESIDE',
          32 => 'TRAFFORD',
          33 => 'WAKEFIELD',
          34 => 'WALSALL',
          35 => 'WATFORD',
          36 => 'WIRRALL',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '10',
          1 => '11',
          2 => '12',
          3 => '13',
          4 => '14',
          5 => '15',
          6 => '16',
        ),
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'AL[0-99]*',
          1 => 'BA[0-99]*',
          2 => 'BH[0-99]*',
          3 => 'BN[0-99]*',
          4 => 'BS[0-99]*',
          5 => 'CA[0-99]*',
          6 => 'CB[0-99]*',
          7 => 'CF[0-99]*',
          8 => 'CH[0-99]*',
          9 => 'CM[0-99]*',
          10 => 'CO[0-99]*',
          11 => 'CT[0-99]*',
          12 => 'CW[0-99]*',
          13 => 'DE[0-99]*',
          14 => 'DH[0-99]*',
          15 => 'DL[0-99]*',
          16 => 'DN[0-99]*',
          17 => 'DT[0-99]*',
          18 => 'EX[0-99]*',
          19 => 'GL[0-99]*',
          20 => 'GU[0-99]*',
          21 => 'HD[0-99]*',
          22 => 'HG[0-99]*',
          23 => 'HP[0-99]*',
          24 => 'HU[0-99]*',
          25 => 'HX[0-99]*',
          26 => 'IP[0-99]*',
          27 => 'LD[0-99]*',
          28 => 'LE[0-99]*',
          29 => 'LL[0-99]*',
          30 => 'LN[0-99]*',
          31 => 'ME[0-99]*',
          32 => 'MK[0-99]*',
          33 => 'NN[0-99]*',
          34 => 'NP[0-99]*',
          35 => 'NR[0-99]*',
          36 => 'OX[0-99]*',
          37 => 'PE[0-99]*',
          38 => 'PL[0-99]*',
          39 => 'PO[0-99]*',
          40 => 'RG[0-99]*',
          41 => 'RH[0-99]*',
          42 => 'SA[0-99]*',
          43 => 'SG[0-99]*',
          44 => 'SK[0-99]*',
          45 => 'SL[0-99]*',
          46 => 'SN[0-99]*',
          47 => 'SO[0-99]*',
          48 => 'SP[0-99]*',
          49 => 'SS[0-99]*',
          50 => 'ST[0-99]*',
          51 => 'SY[0-99]*',
          52 => 'TA[0-99]*',
          53 => 'TN[0-99]*',
          54 => 'TQ[0-99]*',
          55 => 'TR[0-99]*',
          56 => 'WA[0-99]*',
          57 => 'WN[0-99]*',
          58 => 'WR[0-99]*',
          59 => 'YO[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
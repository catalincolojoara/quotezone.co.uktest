<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '3148',
    'siteOrigID' => '',
    'brokerID' => '85',
    'logoID' => '761',
    'companyName' => 'Freeway (PCO)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotes@freewayinsurance.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'xml' => '1',
  ),
  'bespoke_filters' => 
  array (
    0 => '  $filter = "";   $typeOfCover=$_SESSION["_YourDetails_"]["type_of_cover"]; $estimatedValue=$_SESSION["_YourDetails_"]["estimated_value"]; if($typeOfCover == "1")     $_companyDetails["filter_details"][]["estimated_value"]["SKP;VAL;MIN;if Type of Cover is  Fully Comprehensive SKIP if Vehicle Value is Over  50000 ."]  = "50000"; ',
    1 => '   $filter = "";  $typeOfCover=$_SESSION["_YourDetails_"]["type_of_cover"]; $estimatedValue=$_SESSION["_YourDetails_"]["estimated_value"]; if($typeOfCover == "2")     $_companyDetails["filter_details"][]["estimated_value"]["SKP;VAL;MIN;if Type of Cover is  Third Party Fire & Theft SKIP if Vehicle Value Over   6000 ."]  = "6000"; ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 75' => '25 - 75',
      ),
    ),
    1 => 
    array (
      'type_of_cover' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 14' => '14',
      ),
    ),
    4 => 
    array (
      'claims_5_years' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'No',
        ),
      ),
    ),
    5 => 
    array (
      'convictions_5_years' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'No',
        ),
      ),
    ),
    6 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
          3 => '10',
          4 => '11',
          5 => '12',
        ),
      ),
    ),
    7 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'LONDON PCO',
        ),
      ),
    ),
    8 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    9 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
        ),
      ),
    ),
  ),
)


 ?>
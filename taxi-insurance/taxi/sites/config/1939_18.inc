<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '1939',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '0',
    'companyName' => 'Barry Grainger - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'bg_leads@ezeecom.co.uk',
        1 => 'leadsdepartment@bginsurance.co.uk',
        2 => 'Davidharvey@bginsurance.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-17:00',
          2 => '09:00-17:00',
          3 => '09:00-17:00',
          4 => '09:00-17:00',
          5 => '09:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 69' => '30-69',
      ),
    ),
    2 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 2000 and 35000' => '2000 - 35000',
      ),
    ),
    3 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 15' => '15',
      ),
    ),
    5 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    6 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'LONDON PCO',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    8 => 
    array (
      'taxi_capacity' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
        ),
      ),
    ),
    9 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'AL[0-99]*',
          1 => 'BA[0-99]*',
          2 => 'BH[0-99]*',
          3 => 'BN[0-99]*',
          4 => 'CA[0-99]*',
          5 => 'CB[0-99]*',
          6 => 'CF[0-99]*',
          7 => 'CM[0-99]*',
          8 => 'CO[0-99]*',
          9 => 'CT[0-99]*',
          10 => 'CW[0-99]*',
          11 => 'DL[0-99]*',
          12 => 'DT[0-99]*',
          13 => 'EX[0-99]*',
          14 => 'GL[0-99]*',
          15 => 'GU[0-99]*',
          16 => 'HR[0-99]*',
          17 => 'HU[0-99]*',
          18 => 'IP[0-99]*',
          19 => 'LL[0-99]*',
          20 => 'ME[0-99]*',
          21 => 'MK[0-99]*',
          22 => 'NN[0-99]*',
          23 => 'NP[0-99]*',
          24 => 'NR[0-99]*',
          25 => 'OX[0-99]*',
          26 => 'PL[0-99]*',
          27 => 'PO[0-99]*',
          28 => 'RG[0-99]*',
          29 => 'RH[0-99]*',
          30 => 'SA[0-99]*',
          31 => 'SG[0-99]*',
          32 => 'SN[0-99]*',
          33 => 'SO[0-99]*',
          34 => 'SP[0-99]*',
          35 => 'SS[0-99]*',
          36 => 'SY[0-99]*',
          37 => 'TA[0-99]*',
          38 => 'TN[0-99]*',
          39 => 'TQ[0-99]*',
          40 => 'TR[0-99]*',
          41 => 'WR[0-99]*',
          42 => 'YO[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10637',
    'siteOrigID' => '',
    'brokerID' => '82',
    'logoID' => '685',
    'companyName' => 'Insurance Online - Taxi',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'hour' => '5',
      'day' => '10',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxileads@insuranceonline.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BR[1-7]',
          1 => 'CR[0-8]',
          2 => 'DA1',
          3 => 'DA5',
          4 => 'DA6',
          5 => 'DA8',
          6 => 'DA14',
          7 => 'DA15',
          8 => 'DA16',
          9 => 'DA17',
          10 => 'E[1-18]',
          11 => 'EC[1-4]',
          12 => 'EN[1-5]',
          13 => 'HA[0-9]',
          14 => 'IG1',
          15 => 'IG2',
          16 => 'IG3',
          17 => 'IG6',
          18 => 'IG8',
          19 => 'IG11',
          20 => 'KT1',
          21 => 'KT2',
          22 => 'KT3',
          23 => 'KT4',
          24 => 'KT6',
          25 => 'KT9',
          26 => 'N1',
          27 => 'N[3-22]',
          28 => 'NW[1-11]',
          29 => 'RM[1-14]',
          30 => 'SE1',
          31 => 'SE[3-28]',
          32 => 'SM[2-7]',
          33 => 'SW[1-9]',
          34 => 'SW20',
          35 => 'TN14',
          36 => 'TN16',
          37 => 'TW1',
          38 => 'TW3',
          39 => 'TW4',
          40 => 'TW5',
          41 => 'TW7',
          42 => 'TW8',
          43 => 'TW9',
          44 => 'TW10',
          45 => 'TW11',
          46 => 'TW12',
          47 => 'TW13',
          48 => 'TW14',
          49 => 'UB[1-10]',
          50 => 'W[1-14]',
          51 => 'WC1',
          52 => 'WC2',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 65' => '25-65',
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
      ),
    ),
    3 => 
    array (
      'estimated_value_sk' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 25000' => '0 - 25000',
      ),
    ),
    4 => 
    array (
      'year_of_manufacture' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1901',
          1 => '1902',
          2 => '1903',
          3 => '1904',
          4 => '1905',
          5 => '1906',
          6 => '1907',
          7 => '1908',
          8 => '1909',
          9 => '1910',
          10 => '1911',
          11 => '1912',
          12 => '1913',
          13 => '1914',
          14 => '1915',
          15 => '1916',
          16 => '1917',
          17 => '1918',
          18 => '1919',
          19 => '1920',
          20 => '1921',
          21 => '1922',
          22 => '1923',
          23 => '1924',
          24 => '1925',
          25 => '1926',
          26 => '1927',
          27 => '1928',
          28 => '1929',
          29 => '1930',
          30 => '1931',
          31 => '1932',
          32 => '1933',
          33 => '1934',
          34 => '1935',
          35 => '1936',
          36 => '1937',
          37 => '1938',
          38 => '1939',
          39 => '1940',
          40 => '1941',
          41 => '1942',
          42 => '1943',
          43 => '1944',
          44 => '1945',
          45 => '1946',
          46 => '1947',
          47 => '1948',
          48 => '1949',
          49 => '1950',
          50 => '1951',
          51 => '1952',
          52 => '1953',
          53 => '1954',
          54 => '1955',
          55 => '1956',
          56 => '1957',
          57 => '1958',
          58 => '1959',
          59 => '1960',
          60 => '1961',
          61 => '1962',
          62 => '1963',
          63 => '1964',
          64 => '1965',
          65 => '1966',
          66 => '1967',
          67 => '1968',
          68 => '1969',
          69 => '1970',
          70 => '1971',
          71 => '1972',
          72 => '1973',
          73 => '1974',
          74 => '1975',
          75 => '1976',
          76 => '1977',
          77 => '1978',
          78 => '1979',
          79 => '1980',
          80 => '1981',
          81 => '1982',
          82 => '1983',
          83 => '1984',
          84 => '1985',
          85 => '1986',
          86 => '1987',
          87 => '1988',
          88 => '1989',
          89 => '1990',
          90 => '1991',
          91 => '1992',
          92 => '1993',
          93 => '1994',
          94 => '1995',
          95 => '1996',
          96 => '1997',
          97 => '1998',
          98 => '1999',
          99 => '2000',
          100 => '2001',
          101 => '2002',
        ),
      ),
    ),
    5 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '4',
        ),
      ),
    ),
    6 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 14' => '0-14',
      ),
    ),
    7 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    8 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BEDFORD',
          1 => 'BELFAST',
          2 => 'BIRMINGHAM',
          3 => 'BLACKBURN WITH DARWEN',
          4 => 'BOLTON',
          5 => 'BRADFORD',
          6 => 'BRISTOL',
          7 => 'BRISTOL CITY OF UA',
          8 => 'BURNLEY',
          9 => 'BURY',
          10 => 'CALDERDALE',
          11 => 'CHORLEY',
          12 => 'DUDLEY',
          13 => 'DVA (NI)',
          14 => 'HALTON',
          15 => 'HYNDBURN',
          16 => 'KIRKLEES',
          17 => 'KNOWSLEY',
          18 => 'LEEDS',
          19 => 'LIVERPOOL',
          20 => 'LONDON PCO',
          21 => 'LUTON',
          22 => 'MANCHESTER',
          23 => 'MIDDLESBOROUGH',
          24 => 'MILTON KEYNES',
          25 => 'NEWCASTLE UPON TYNE',
          26 => 'NORTHERN IRELAND',
          27 => 'OLDHAM',
          28 => 'PRESTON',
          29 => 'RENFREWSHIRE (PAISLEY)',
          30 => 'ROCHDALE',
          31 => 'ROSSENDALE',
          32 => 'ROTHERHAM',
          33 => 'SALFORD',
          34 => 'SANDWELL',
          35 => 'SEFTON',
          36 => 'SHEFFIELD',
          37 => 'SLOUGH',
          38 => 'SOUTH BUCKINGHAM',
          39 => 'ST HELENS',
          40 => 'STOCKPORT',
          41 => 'STOKE ON TRENT',
          42 => 'SUNDERLAND',
          43 => 'TAMESIDE',
          44 => 'THREE RIVERS',
          45 => 'TRAFFORD',
          46 => 'WAKEFIELD',
          47 => 'WALSALL',
          48 => 'WARRINGTON',
          49 => 'WATFORD',
          50 => 'WIGAN',
          51 => 'WIRRALL',
          52 => 'WOLVERHAMPTON',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10190',
    'siteOrigID' => '',
    'brokerID' => '69',
    'logoID' => '654',
    'companyName' => 'High Gear Insurance - Taxi (TFP 1)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'High Gear Insurance - Taxi (TFP 1)',
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'qz@highgear.co.uk',
      ),
    ),
  ),
  'request' => 
  array (
    'csv' => '1',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-16:30',
          2 => '09:00-16:30',
          3 => '09:00-16:30',
          4 => '09:00-16:30',
          5 => '09:00-16:30',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
          3 => '4',
          4 => '5',
        ),
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '5',
          1 => '6',
          2 => '7',
          3 => '8',
          4 => '9',
          5 => '10',
          6 => '11',
          7 => '12',
        ),
      ),
    ),
    3 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 68' => '30 - 68',
      ),
    ),
    4 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BD[0-99]*',
          1 => 'BT[0-99]*',
          2 => 'M17-27',
        ),
      ),
    ),
    5 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 10' => '0 - 10',
      ),
    ),
    6 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BELFAST',
          1 => 'BLACKBURN WITH DARWEN',
          2 => 'BOLTON',
          3 => 'BRADFORD',
          4 => 'BURNLEY',
          5 => 'BURY',
          6 => 'CALDERDALE',
          7 => 'DVA (NI)',
          8 => 'HARLOW',
          9 => 'HYNDBURN',
          10 => 'KIRKLEES',
          11 => 'KNOWSLEY',
          12 => 'LEEDS',
          13 => 'LIVERPOOL',
          14 => 'MANCHESTER',
          15 => 'NORTHERN IRELAND',
          16 => 'OLDHAM',
          17 => 'ROCHDALE',
          18 => 'SALFORD',
          19 => 'SEFTON',
          20 => 'ST HELENS',
          21 => 'STOCKPORT',
          22 => 'TAMESIDE',
          23 => 'TRAFFORD',
          24 => 'WIGAN',
        ),
      ),
    ),
    7 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MAX;Accept if maximum value is 10' => '10',
      ),
    ),
    8 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
          2 => '5',
          3 => '6',
          4 => '7',
          5 => '8',
          6 => '9',
          7 => '10',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '12367',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '975',
    'companyName' => 'JB Brokers Ltd - Taxi (London PCO)',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'quotes@taxiins.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 66 and 80' => '66-80',
      ),
    ),
    1 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'ACC;VAL;MIN;Accept if minimum value is 5' => '5',
      ),
    ),
    2 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BD[0-99]*',
          1 => 'BT[0-99]*',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '353',
    'siteOrigID' => '',
    'brokerID' => '37',
    'logoID' => '0',
    'companyName' => 'Barry Grainger - Taxi ',
    'offline_from_plugin' => true,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'leadsdepartment@bginsurance.co.uk',
        1 => 'davidharvey@bginsurance.co.uk',
        2 => 'davidharvey@barrygraingerinsurance.co.uk',
        3 => 'bg_leads@ezeecom.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$taxiNCB = $_SESSION["_YourDetails_"]["taxi_ncb"]; $ncbMsg = "Taxi no claims bonus & Private car no claims bonus = No NCB";if($taxiNCB == 0){ $_companyDetails["filter_details"][]["private_car_ncb"]["SKP;LIST;;Skip if Taxi no claims bonus & Private car no claims bonus = No NCB--SKP_LIST [$ncbMsg]"] = array( 0=>"0",);}',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '09:00-18:00',
          2 => '09:00-18:00',
          3 => '09:00-18:00',
          4 => '09:00-18:00',
          5 => '09:00-18:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'taxi_badge' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    2 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
          1 => '2',
          2 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_plating' => 
      array (
        'SKP;LIST;;Skip if Taxi Plating Authority = any from the attached list. ' => 
        array (
          0 => 'NORTHERN IRELAND',
          1 => 'BELFAST',
          2 => 'SEFTON',
          3 => 'KNOWSLEY',
          4 => 'ST HELENS',
          5 => 'HALTON',
          6 => 'BOLTON',
          7 => 'BLACKBURN WITH DARWEN',
          8 => 'HYNDBURN',
          9 => 'ROSSENDALE',
          10 => 'BURNLEY',
          11 => 'BURY',
          12 => 'ROCHDALE',
          13 => 'TRAFFORD',
          14 => 'SALFORD',
          15 => 'TAMESIDE',
          16 => 'CALDERDALE',
          17 => 'LONDON PCO',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_vehicle_age' => 
      array (
        'SKP;VAL;MIN;Skip if Year Of Manufacture > 10 years. ' => '10',
      ),
    ),
    5 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;SKIP if Taxi no claims bonus = No NCB.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    6 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 65' => '30 - 65',
      ),
    ),
    7 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    8 => 
    array (
      'postcode_sk' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BA[0-99]*',
          1 => 'BH[0-99]*',
          2 => 'BN[0-99]*',
          3 => 'CM[0-99]*',
          4 => 'CO[0-99]*',
          5 => 'CT[0-99]*',
          6 => 'DA[0-99]*',
          7 => 'DN[0-99]*',
          8 => 'DT[0-99]*',
          9 => 'EX[0-99]*',
          10 => 'GL[0-99]*',
          11 => 'GU[0-99]*',
          12 => 'HG[0-99]*',
          13 => 'HP[0-99]*',
          14 => 'HR[0-99]*',
          15 => 'ME[0-99]*',
          16 => 'PL[0-99]*',
          17 => 'PO[0-99]*',
          18 => 'RH[0-99]*',
          19 => 'SO[0-99]*',
          20 => 'SP[0-99]*',
          21 => 'TA[0-99]*',
          22 => 'TN[0-99]*',
          23 => 'TQ[0-99]*',
          24 => 'TR[0-99]*',
          25 => 'WR[0-99]*',
        ),
      ),
    ),
    9 => 
    array (
      'convictions_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
  ),
)


 ?>
<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '11551',
    'siteOrigID' => '',
    'brokerID' => '75',
    'logoID' => '187',
    'companyName' => 'Connect Insurance - Taxi 13',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '5',
    ),
    'emailSubject' => 'Taxi 13 Query:',
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'LeadTX@connect-insurance.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$platingAuthority=strtoupper($_SESSION["_YourDetails_"]["plating_authority"]); $platingAuthorityArr = array("WOLVERHAMPTON",); if(in_array($platingAuthority, $platingAuthorityArr)) $_companyDetails["filter_details"][]["postcode_sk"]["ACC;LIST;;If Taxi Plating Authority = Wolverhampton Only Accept if Postcode = WV[0-99]*."]  = array ("WV[0-99]*",); ',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-23:59',
          2 => '00:00-23:59',
          3 => '00:00-23:59',
          4 => '00:00-23:59',
          5 => '00:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 30 and 74' => '30 - 74',
      ),
    ),
    2 => 
    array (
      'taxi_badge' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '8',
          1 => '9',
          2 => '10',
          3 => '11',
          4 => '12',
          5 => '13',
        ),
      ),
    ),
    3 => 
    array (
      'taxi_ncb' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    4 => 
    array (
      'taxi_capacity' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '8',
          1 => '9',
          2 => '10',
          3 => '11',
          4 => '12',
          5 => '13',
          6 => '14',
          7 => '15',
          8 => '16',
        ),
      ),
    ),
    5 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 28' => '0-28',
      ),
    ),
    6 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
        ),
      ),
    ),
    7 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BARKING',
          1 => 'BARNET',
          2 => 'BEDFORD',
          3 => 'BELFAST',
          4 => 'BEXLEY',
          5 => 'BIRMINGHAM',
          6 => 'BLACKBURN WITH DARWEN',
          7 => 'BOLTON',
          8 => 'BRADFORD',
          9 => 'BRENT',
          10 => 'BRISTOL',
          11 => 'BROMLEY',
          12 => 'BURNLEY',
          13 => 'BURY',
          14 => 'CALDERDALE',
          15 => 'CAMDEN',
          16 => 'CROYDEN',
          17 => 'DARTFORD',
          18 => 'DVA (NI)',
          19 => 'EALING',
          20 => 'ENFIELD',
          21 => 'GREENWICH',
          22 => 'HACKNEY',
          23 => 'HALTON',
          24 => 'HAMMERSMITH',
          25 => 'HAVERING',
          26 => 'HILLINGDON',
          27 => 'HOUNSLOW',
          28 => 'ISLINGTON',
          29 => 'KENSINGTON',
          30 => 'KENT',
          31 => 'KINGSTON UPON THAMES ROYAL',
          32 => 'KIRKLEES',
          33 => 'LAMBETH',
          34 => 'LEWISHAM',
          35 => 'LIVERPOOL',
          36 => 'LONDON PCO',
          37 => 'LUTON',
          38 => 'MANCHESTER',
          39 => 'MERTON',
          40 => 'NEWHAM',
          41 => 'NORTHERN IRELAND',
          42 => 'OLDHAM',
          43 => 'PRESTON',
          44 => 'REDBRIDGE',
          45 => 'RICHMOND',
          46 => 'ROCHDALE',
          47 => 'ROSSENDALE',
          48 => 'SALFORD',
          49 => 'SANDWELL',
          50 => 'SLOUGH',
          51 => 'SOUTHWARK',
          52 => 'ST HELENS',
          53 => 'SUTTON',
          54 => 'THREE RIVERS',
          55 => 'TOWER HAMLETS',
          56 => 'TRAFFORD',
          57 => 'WAKEFIELD',
          58 => 'WALTHAM FOREST',
          59 => 'WANDSWORTH',
          60 => 'WATFORD',
          61 => 'WESTMINSTER',
          62 => 'WIGAN',
        ),
      ),
    ),
    8 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BD8',
          1 => 'BT[0-99]*',
          2 => 'E[0-99]*',
          3 => 'EC[0-99]*',
          4 => 'N[0-99]*',
          5 => 'NW[0-99]*',
          6 => 'SE[0-99]*',
          7 => 'SW[0-99]*',
          8 => 'W[0-99]*',
          9 => 'WC[0-99]*',
        ),
      ),
    ),
    9 => 
    array (
      'claims_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    10 => 
    array (
      'convictions_5_years' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'Yes',
        ),
      ),
    ),
    11 => 
    array (
      'taxi_driver' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => '1',
        ),
      ),
    ),
    12 => 
    array (
      'registration_number_question' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'N',
        ),
      ),
    ),
  ),
)


 ?>
<?php

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10837',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '162',
    'companyName' => 'Crosby (Sunderland) - Taxi',
    'offline_from_plugin' => true,
    'ignore_off_status' => true,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'emailSubject' => 'Crosby (Sunderland) - Taxi',
    'limit' => 
    array (
      'day' => '10',
      'hour' => '1',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'stuart@crosbyinsurance.co.uk',
      ),
    ),
  ),
  'bespoke_filters' => 
  array (
    0 => '$typeOfCover  = $_SESSION["_YourDetails_"]["type_of_cover"];     if($typeOfCover == "1")     {       $_companyDetails["filter_details"][]["year_of_manufacture"]["SKP;VAL;MAX;Skip if value selected is < 2008 . "]  = "2008";}',
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '08:00-17:00',
          2 => '08:00-17:00',
          3 => '08:00-17:00',
          4 => '08:00-17:00',
          5 => '08:00-17:00',
          6 => 'SKP_ALL_DAY',
          7 => 'SKP_ALL_DAY',
        ),
      ),
    ),
    1 => 
    array (
      'plating_authority' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'BIRMINGHAM',
          1 => 'BRADFORD',
          2 => 'LIVERPOOL',
          3 => 'LONDON PCO',
          4 => 'MANCHESTER',
        ),
      ),
    ),
    2 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
        ),
      ),
    ),
    3 => 
    array (
      'type_of_cover' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '3',
          1 => '4',
        ),
      ),
    ),
    4 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 21 and 75' => '21 - 75',
      ),
    ),
    5 => 
    array (
      'postcode_sk' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => 'B[0-99]*',
          1 => 'BD[0-99]*',
          2 => 'BT[0-99]*',
          3 => 'E[0-99]*',
          4 => 'EC[0-99]*',
          5 => 'L[0-99]*',
          6 => 'M[0-99]*',
          7 => 'N[0-99]*',
          8 => 'NW[0-99]*',
          9 => 'SE[0-99]*',
          10 => 'SW[0-99]*',
          11 => 'W[0-99]*',
          12 => 'WC[0-99]*',
        ),
      ),
    ),
    6 => 
    array (
      'taxi_ncb' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
        ),
      ),
    ),
    7 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 7' => '0-7',
      ),
    ),
  ),
)


?>
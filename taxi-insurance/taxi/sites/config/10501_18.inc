<?php 

global $_YourDetails;


$_companyDetails=array (
  'plugin_details' => 
  array (
    'siteID' => '10501',
    'siteOrigID' => '',
    'brokerID' => '',
    'logoID' => '721',
    'companyName' => 'Patons Wales OOH',
    'offline_from_plugin' => false,
    'ignore_off_status' => false,
    'testing_company' => false,
    'testing_marketing' => false,
    'send_QA_team' => false,
    'limit' => 
    array (
      'day' => '10',
    ),
  ),
  'emails' => 
  array (
    'MAIL' => 
    array (
      'MARKETING' => 
      array (
        0 => 'leads@seopa.com',
      ),
      'TECHNICAL' => 
      array (
        0 => 'eb2-technical@seopa.com',
      ),
      'COMPANY' => 
      array (
        0 => 'taxi@patonsquotes.co.uk',
      ),
    ),
  ),
  'filter_details' => 
  array (
    0 => 
    array (
      'time' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          1 => '00:00-09:00|17:00-23:59',
          2 => '00:00-09:00|17:00-23:59',
          3 => '00:00-09:00|17:00-23:59',
          4 => '00:00-09:00|17:00-23:59',
          5 => '00:00-09:00|17:00-23:59',
          6 => '00:00-23:59',
          7 => '00:00-23:59',
        ),
      ),
    ),
    1 => 
    array (
      'age_range' => 
      array (
        'ACC;VAL;;Accept if value is between 25 and 75' => '25 - 75',
      ),
    ),
    2 => 
    array (
      'plating_authority' => 
      array (
        'ACC;LIST;;Accept the below list of values.' => 
        array (
          0 => 'BLAENAU GWENT',
          1 => 'BRIDGEND',
          2 => 'CAERPHILLY',
          3 => 'CARDIFF',
          4 => 'CARMARTHENSHIRE',
          5 => 'CEREDIGION',
          6 => 'CONWY',
          7 => 'DENBIGSHIRE',
          8 => 'GWYNEDD',
          9 => 'ISLE OF ANGLESEY',
          10 => 'MERTHYR TYDFIL',
          11 => 'MONMOUTHSHIRE',
          12 => 'NEATH & PORT TALBOT',
          13 => 'PEMBROKESHIRE',
          14 => 'POWYS',
          15 => 'RHONDDA CYNON TAFF',
          16 => 'SWANSEA',
          17 => 'TORFAEN',
          18 => 'VALE OF GLAMORGAN',
          19 => 'WREXHAM',
        ),
      ),
    ),
    3 => 
    array (
      'date_of_insurance_start_lg_new' => 
      array (
        'ACC;VAL;;Accept if value is between 0 and 23' => '0 - 23',
      ),
    ),
    4 => 
    array (
      'period_of_licence' => 
      array (
        'SKP;LIST;;Skip the below list of values.' => 
        array (
          0 => '0',
          1 => '1',
          2 => '2',
        ),
      ),
    ),
  ),
)


 ?>
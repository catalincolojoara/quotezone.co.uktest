<?php

   $error = false;

   //1.GapOptIn Click4Gap Insurance will not accept leads if gap_insurance=No
   $gapInsurance = $_SESSION['_YourDetails_']['gap_insurance'];
   if($gapInsurance == 1)
   {
      $personalizedError[] = "SKIP_RULE_GAP_INS--SKP_NO";
      $error 			   =  true;
   }

   //2.Max vehicle age = 7 years (2003, 2004 ... 2010 are all acceptable)
   $vehicleAge = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'] - $_SESSION['_YourDetails_']['year_of_manufacture'];
   if($vehicleAge > 7)
   {
      $personalizedError[] = "SKIP_RULE_VEH_AGE--AGE_SKP_OVR_7";
      $error 			   =  true;     
   }

   //3.Max vehicle values - £50,000
   $vehicleValue = $_SESSION['_YourDetails_']['estimated_value'];
   if($vehicleValue > 50000)
   {
      $personalizedError[] = "SKIP_RULE_VEH_VALUE--POUND_SKP_OVR_50000";
      $error 			   =  true;
   }


   //5.Skip if Type of cover is not “Fully comprehensive”.
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover != 1)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_ACC_FULL_COMPR";
      $error 			   =  true;
   }


   return $error;

?>

<?php

   $error = false;


   //ACCEPT if "Taxi Use" = "Public Hire"
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse != 2)
   {
      $personalizedError[] = "SKIP_RULE_USE--USE_ACC_PUB_HIRE"; 
      $error               =  true;
   }


   //SKIP if "Taxi Badge" = "No Badge", "Under 6 Months", "6 Months to 1 Year"
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--BADG_SKP_N_BADG_UND6_MONTH_6MONTH"; 
      $error               =  true;
   }

   //    SKIP if "Taxi Driver(s)" = Insured +1 Named Driver
   //                                        Insured and 2+ Named Drivers
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   
   if($taxiDrivers == 3 || $taxiDrivers == 2)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_SKP_INS_NAM_DRV_2+_1+";
      $error                       =  true;
   }


   //Skip if Taxi Year of Manufacture < 2005
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   if($yearOfManufacture < 2005)
   {
      $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--YEAR_SKP_UND_2005";
      $error                       =  true;
   }


   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 26 || $propAge > 70)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_26_AND_OVR_70";
      $error                       =  true;
   }
 
   //Skip if Full UK Licence = Under 6 months, 6 months to 1 year,1 – 2 years,2-3 Years,3-4 Years,4-5 Years
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence < 7)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_5_6_YEARS";
      $error            =  true;
   }
 

   //SKIP if "Taxi Plating Authority" is on the attached SKIP list
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $skipLicensingAuthorityArray = array(
"LONDON PCO",
"BARNET",
"BEDFORD",
"BIRMINGHAM",
"BLACKBURN WITH DARWEN",
"BLACKPOOL",
"BOLTON",
"BRADFORD",
"BRISTOL CITY OF UA",
"BROXTOWE",
"BURY",
"CALDERDALE",
"CAMDEN",
"CANNOCK CHASE",
"CARDIFF",
"CHELMSFORD",
"COVENTRY",
"CROYDEN",
"DARTFORD",
"DERBY",
"EALING",
"EAST RENFREWSHIRE (GIFFNOCH)",
"ELMBRIDGE",
"ENFIELD",
"EPSOM & EWELL",
"GLASGOW",
"GREENWICH",
"HACKNEY",
"HARINGEY",
"HOUNSLOW",
"ISLINGTON",
"KIRKLEES",
"KNOWSLEY",
"LEEDS",
"LIVERPOOL",
"LUTON",
"MANCHESTER",
"MERTON",
"NEWCASTLE UPON TYNE",
"NORTH LANARKSHIRE (NORTH)",
"NORTH TYNESIDE",
"NOTTINGHAM",
"OLDHAM",
"PETERBOROUGH",
"PRESTON",
"READING",
"REDCAR & CLEVELAND",
"RENFREWSHIRE (PAISLEY)",
"ROCHDALE",
"WINDSOR & MAIDENHEAD",
"SALFORD",
"SANDWELL",
"SHEFFIELD",
"SUNDERLAND",
"SLOUGH",
"SOLIHULL",
"SOUTH TYNESIDE",
"STEVENAGE",
"STOCKPORT",
"TAMESIDE",
"THREE RIVERS",
"TRAFFORD",
"WAKEFIELD",
"WALSALL",
"WATFORD",
"WIGAN",
"WIRRALL",
"WOLVERHAMPTON",
"LONDON PCO",
"BELFAST",
"SANDWELL",
"KIRKLEES",
"NEWCASTLE UPON TYNE",
"NEWCASTLE UNDER LYME",
"BIRMINGHAM",
"BEDFORD",
"LUTON",
   );

   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error             =  true;
   }
   
   // SKIP if "Taxi Plating Authority" = "Liverpool" AND  "Taxi No Claims Bonus" = "No NCB"
   if($licesingAuthority == "LIVERPOOL")
   {    
      //skip if ncb = 0
      $drNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
      if($drNCB == 0)
      {
         $personalizedError[] = "SKIP_RULE_NCB_PLT--PLATING_NCB_SKP_NO_NCB";
         $error               =  true;
      }
   }
   

   // SKIP all SMS text leads from being sent to Patons Insurance
   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }
   
   
   return $error;

?>

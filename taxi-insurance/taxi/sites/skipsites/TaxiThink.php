<?php

   $error = false;


   //skip if age is under 23 or over 74 years
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $insTime = mktime("0","0","0",$DISmm,$DISdd,$DISyyyy);
   $dobTime = mktime("0","0","0",$dobMM,$dobDD,$dobYYYY);

   $propAge = ($insTime - $dobTime) / (86400*364.25);

   if($propAge < 23 || $propAge > 74)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_23_AND_OVR_74";
      $error               =  true;
   }

   
   //skip if age is over 9 years
   $vehicleManufYear = $_SESSION["_YourDetails_"]["year_of_manufacture"];
   $disYear          = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"];

   $vehicleAge = $disYear - $vehicleManufYear;

   if($vehicleAge > 9)
   {
      $personalizedError[] = "SKIP_RULE_VEH_AGE--AGE_SKP_OVR_9";
      $error            =  true;
   }

   
   // skip if Full UK licence = 1-2 Years, 2-3 Years
   $period_of_licence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($period_of_licence == 3 || $period_of_licence == 4 )
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_3_YEARS";
      $error               =  true;
   }

   // skip Claims Last 5 Years = Yes
   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
   if($claim5Years == "Yes")
   { 
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_YES";
      $error            =  true;
   }
   
   
   // skip Convictions Last 5 Years = Yes
   $conv5Years = $_SESSION['_YourDetails_']['convictions_5_years'];
   if($conv5Years == "Yes")
   {
      $personalizedError[] = "SKIP_RULE_CONV_LAST_5_YEARS--SKP_YES";
      $error                       =  true;
   }
   
   
   // SKIP postcodes list
   $postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "B",
      "BD",
      "G",
      "EH",
      "N",
      "L",
      "LS",
      "LE",
      "M",
      "S",
      "ST",
      "LU",
      "CV",
      "EC",
      "WC",
      "E",
      "N",
      "NW",
      "SE",
      "SW",
      "W",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error               =  true;
   }
   
   // skip taxi plating authority:
   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

   $skpLicensingAuthorityArray = array(
      "BIRMINGHAM",
      "BRADFORD",
      "GLASGOW",
      "EDINBURGH",
      "NEWCASTLE",
      "SUNDERLAND",
      "LIVERPOOL",
      "LEEDS",
      "LEICESTER",
      "MANCHESTER",
      "ROTHERHAM",
      "SHEFFIELD",
      "STOKE",
      "SOLIHULL",
      "COVENTRY",
      "LUTON",
      "SEFTON",
      "LONDON PCO",
      "NEWCASTLE UPON TYNE",
      "STOKE ON TRENT", 
   );

   if(in_array($licesingAuthority,$skpLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error               =  true;
   }
      
   
   return $error;

?>

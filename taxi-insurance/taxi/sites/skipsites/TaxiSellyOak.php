<?php

   $error = false;

   $drNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   // 1. Selly Oak accepts leads for drivers that have minimum 1 year ncb.
   if($drNCB == 0)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
      $error 			   =  true;
   }

   // 2. Selly Oak will not recieve leads for this licensing authorities
   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

   $skipLicensingAuthorityArray = array(
"BIRMINGHAM",
"LUTON",
"WIRRALL",
"WIGAN",
"WARRINGTON",
"ST HELENS",
"TAMESIDE",
"TRAFFORD",
"KIRKLEES",
"KNOWSLEY",
"BURY",
"BURNLEY",
"OLDHAM",
"ROCHDALE",
"SHEFFIELD",
"WAKEFIELD",
"LEEDS",
"MANCHESTER",
"BLACKBURN WITH DARWEN",
"BRADFORD",
"LIVERPOOL",
   );

   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
  	  $error 			   =  true;
   }

   // 3.Selly Oak accept leads only between 9:30am and 5:30pm Monday – Friday.
   $timeMin    = date("i");
   $timeHh     = date("G");
   $dateDdText = date("N");

   if($dateDdText > 5 || $timeHh < 9 || $timeHh > 17 || ($timeHh == 9 && $timeMin < 30) || ($timeHh == 17 && $timeMin > 30))
   {
      $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:30AM_TO_5:30PM";
      $error 			   =  true;
   }

   $postCode     = $_SESSION['_YourDetails_']['postcode'];

   // 3. Selly Oak will not accept leads from this postcodes :
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BT",
   );  

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error 			   =  true;
   }

   return $error;

?>

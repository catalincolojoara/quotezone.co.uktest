<?php

   $error = false;
   $personalizedError = "";

   //1.SKIP if "Full UK licence" = "1 - 2 Years"
   $periodOfFullUkLicenceHeld = $_SESSION["_YourDetails_"]["period_of_licence"];
   $filteredPeriodOfFullUkLicenceHeldArray = array(
      "3" => "1 - 2 Years",
   );

   if(array_key_exists($periodOfFullUkLicenceHeld,$filteredPeriodOfFullUkLicenceHeldArray))
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--SKP_LIST [".$filteredPeriodOfFullUkLicenceHeldArray[$periodOfFullUkLicenceHeld]."]";
      $error            =  true;
   }

   //2. SKIP if "Age" < 25   > 69
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25 || $propAge > 69)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_BETW_25_AND_69";
      $error 		   =  true;
   }


   //3.SKIP if "Taxi no claims bonus" = "No NCB" "1 Year"
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($taxiNCB < 2)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_2_YEARS";
      $error               =  true;
   }


   //4. ACCEPT only if "Taxi plating authority" = "LONDON PCO"

   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $accLicensingAuthorityArray = array(
   "LONDON PCO",
   );

   if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
      $error               =  true;
   }
   
   
   // SKIP if "Taxi type" = Black Cab
   $taxiType = $_SESSION['_YourDetails_']['taxi_type'];
   if($taxiType == 1)
   {
      $personalizedError[] = "SKIP_RULE_VEH_TYP--TYP_SKP_BLACK_CAB";
      $error            =  true;
   }
   

   return $error;

?>

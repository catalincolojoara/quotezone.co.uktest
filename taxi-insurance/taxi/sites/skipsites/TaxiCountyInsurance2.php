<?php

   $error = false;

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   // SKIP if "Age" < 30
   if($propAge < 30)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_30";
      $error 			   =  true;
   }

   
   //only accept if "Taxi No Claims Bonus" = 1 Year
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB != 1)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_ACC_1_YEAR";
      $error                       =  true;
   }


   //SKIP if Postcode = BT[1-99]*
   $postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BT",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {

      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {
         case '6': //can quote Saturday
               $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT--TIME_SKP_ALL_DAY";
               $error               =  true;
            break;

         case '7':  //Sunday skip
               $personalizedError[] = "SKIP_RULE_SKP_DAY_SUN--TIME_SKP_ALL_DAY";
               $error               = true;
               break;


         case '1':  //can quote Monday-Friday - accept between the hours 8.00am to 20.00
         case '2':  //can quote Monday-Friday - accept between the hours 8.00am to 20.00
         case '3':  //can quote Monday-Friday - accept between the hours 8.00am to 20.00
         case '4':  //can quote Monday-Friday - accept between the hours 8.00am to 20.00
         case '5':  //can quote Monday-Friday - accept between the hours 8.00am to 20.00
            if($timeHh < 8 || $timeHh >= 20)
            {
               $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_8:00AM_TO_8:00PM";
               $error               =  true;
            }
            break;
      }
   }
   
   return $error;
?>

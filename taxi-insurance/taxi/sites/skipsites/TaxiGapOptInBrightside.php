<?php

   $error = false;

   //1.GapOptIn Brightside Insurance will not accept leads if gap_insurance=No
   $gapInsurance = $_SESSION['_YourDetails_']['gap_insurance'];
   if($gapInsurance == 1)
   {
      $personalizedError[] = "SKIP_RULE_GAP_INS--SKP_NO";
      $error 			   =  true;
   }

   //2.Skip if vehicle is over 5 years old. 5 is OK.
 
   $vehicleAge = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'] - $_SESSION['_YourDetails_']['year_of_manufacture'];
   if($vehicleAge > 5)
   {
      $personalizedError[] = "SKIP_RULE_VEH_AGE--AGE_SKP_OVR_5";
      $error 			   =  true;    
   }

   //3.Skip if Type of cover is not “Fully comprehensive”.
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover != 1)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_ACC_FULL_COMPR"; 
      $error 			   =  true;
   }
  
   //4.Skip if vehicle value is greater than £50,000 or smaller then 4,000.
   $estimatedValue = $_SESSION['_YourDetails_']['estimated_value']; 
   if($estimatedValue > 50000 || $estimatedValue < 4000)
   {
      $personalizedError[] = "SKIP_RULE_VEH_VALUE--POUND_SKP_UND_4000_AND_OVR_50000"; 
      $error 			   =  true;
   }
      
   return $error;

?>

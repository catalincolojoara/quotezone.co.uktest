<?php 

   $error = false;

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   // SKIP if "Age" < 24  > 74
                     
   if($propAge < 24 OR $propAge > 74)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_BETW_24_AND_74";
      $error 			   =  true;
   }

   // 3.JM will not receive leads for Black Cab or MPV
//   $taxiType = $_SESSION['_YourDetails_']['taxi_type'];
//   if($taxiType == 1 || $taxiType == 3)
//   {
//      $personalizedError[] = "SKIP_RULE_VEH_TYP--TYP_SKP_BLACK_CAB_OR_MPV";
//      $error 			   =  true;
//   }
   

 /*ACCEPT leads Monday 06:00 - 23:59
                         Tuesday 06:00 - 23:59
                    Wednesday NO LEADS
                        Thursday 06:00 - 23:59
                             Friday 06:00 - 23:59
                         Saturday NO LEADS
                            Sunday NO  LEADS
  */
   
   
 

   $timeHh     = date("G");
   $dateDdNum  = date("N");
   $timeMin    = date("i");
  
   switch($dateDdNum)
   {
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      if($timeHh < 6 )
         {
            $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_SKP_UND_6:00AM";
            $error               =  true;
         }        
      break;
      case '6':
      case '7':
          $personalizedError[] = "SKIP_RULE_SKP_DAY_SUN_SAT--SKIP_RULE_SKP_DAY_SAT_SUN";
          $error 			  =  true;
      break;
   }
   
   
   
   //7. JM filters leads for taxi badge = no badge, under 6 months, 6 months - 1 year if plating authority = LONDON PCO.
   $taxiBadge = $_SESSION["_YourDetails_"]["taxi_badge"];
   $platingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

   if($platingAuthority != "LONDON PCO")
   {
      $filteredBadgeArray = array(
            "1",//  => "No Badge",
            "2",//  => "Under 6 Months",
            "3",//  => "6 Months To 1 Year",
         );

      if(in_array($taxiBadge,$filteredBadgeArray))
      {
         $personalizedError[] = "SKIP_RULE_BADG--SKP_LIST [$taxiBadge]";
         $error =  true;
      }
   }

   $postCode       = $_SESSION['_YourDetails_']['postcode'];
   $postCodePrefix = $_SESSION['_YourDetails_']['postcode_prefix'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
   "GY",
   "IM",
   "JE",
   "BT",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error               = true;
   }

   $taxiRegNumber = $_SESSION['_YourDetails_']['vehicle_registration_number'];
   if($taxiRegNumber == "")
   {
      $personalizedError[] = "SKIP_RULE_REG_NUMBER--SKP_BLANK";
      $error               =  true;
   }


   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $DIS              = $DISyyyy."-".$DISmm."-".$DISdd;
   $inTheFuture21Days = date('Y-m-d',mktime(0,0,0,date('m'), date('d')+21, date('Y')));

   //skip if Insurance Start Date > 21 days after Quote Date
   if($DIS > $inTheFuture21Days)
   {
      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_SKP_21_DAY_AFT";
      $error               = true;
   }
   

//   SKIP if "Max. Number of Passengers" over 7
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity > 7)
   {
         $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_7";
      $error                       =  true;
   }

   // SKIP if "Taxi Driver(s)" = "Insured and 2+ Named Drivers"
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   
   if($taxiDrivers == 3)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_SKP_INS_AND_2+_NAM_DRV";
      $error                       =  true;
   }
   
   return $error;

?>

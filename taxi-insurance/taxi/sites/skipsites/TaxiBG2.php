<?php

   $error = false;

   //can quote Monday-Friday - accept between the hours 8:00am to 5:30pm
   $timeHh     = date("G");
   $timeMin    = date("i");
   $dateDdText = date("N");

   if($dateDdText <= 5 && ($timeHh < 8 || $timeHh > 17 || ($timeHh == 17 && $timeMin >= 30)))
   {
      $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_8:00AM_TO_5:30PM";
      $error =  true;
   }

   if($dateDdText == 6)
   {
         $personalizedError[] = "SKIP_RULE_DAY_SAT--TIME_SKP_ALL_DAY";
         $error               =  true;
   }

   if($dateDdText == 7)
   {
      $personalizedError[] = "SKIP_RULE_SKP_DAY_SUN--TIME_SKP_ALL_DAY";
      $error =  true;
   }

   // Skip if driver age < 40 years old > 69 years old
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);


   if($propAge < 40 || $propAge > 69)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_40_AND_OVR_69";
      $error            =  true;
   }

   // Skip if Taxi badge < 1 year
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_YEAR";
      $error               = true;
   }
   

   // Skip if Taxi Plating Authority equals any of the following:
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $skipLicensingAuthorityArray = array(
"SEFTON",
"KNOWSLEY",
"ST HELENS",
"HALTON",
"BOLTON",
"BLACKBURN WITH DARWEN",
"BELFAST",
"NORTHERN IRELAND",
   );

   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error            =  true;
   }

   // Skip if Year Of Manufacture > 10 years old
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   $curYear           = date('Y');

   if(($curYear - $yearOfManufacture) > 10)
   {
       $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--AGE_SKP_OVR_10";
       $error           =  true;
   }

   // Skip if Taxi No Claims Bonus < 3 years
   $NCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($NCB < 3)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NUM_SKP_UND_3";
      $error            =  true;
   }

   // Skip if Claims Last 5 years = yes
   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
   if($claim5Years == "Yes")
   { 
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_YES";
      $error            =  true;
   }

   // Skip if Convictions Last 5 Years = yes
   $conv5Years = $_SESSION['_YourDetails_']['convictions_5_years'];
   if($conv5Years == "Yes")
   { 
      $personalizedError[] = "SKIP_RULE_CONV_LAST_5_YEARS--SKP_YES";
      $error            =  true;
   }

   // Only Accept leads from the attached list of postcodes
   $postCode     = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
      "AB",
      "AL",
      "BA",
      "BU",
      "BR",
      "BS",
      "CA",
      "CB",
      "CF",
      "CM",
      "CT",
      "DA",
      "DN",
      "DT",
      "EX",
      "GL",
      "GU",
      "HG",
      "HP",
      "HR",
      "IP",
      "LA",
      "LL",
      "LN",
      "LU",
      "ME",
      "MK",
      "ML",
      "NN",
      "OX",
      "PE",
      "PL",
      "PO",
      "RG",
      "RH",
      "RM",
      "S",
      "SG",
      "SM",
      "SN",
      "SO",
      "SP",
      "TA",
      "TN",
      "TQ",
      "TR",
      "WD",
      "WR",
      "YO",
      "NP",
      "SA",
      "SY",
   );
  
   if(! in_array($postcodeToCheck,$acceptedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error            =  true;
   }

   return $error;

?>

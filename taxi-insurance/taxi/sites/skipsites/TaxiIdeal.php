<?php

   $error = false;

   // 1. Only accept leads from the following Taxi plating authorities:

   $platingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   
   $acceptedPlatingAuthiritiesArray = array(
      "DONCASTER",
   );

   if(! in_array($platingAuthority,$acceptedPlatingAuthiritiesArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$platingAuthority]";
  	  $error 			   =  true;
   }
   
   /*
     ACCEPT leads Monday 09:00 - 17:00
                  Tuesday 09:00 - 17:00
                  Wednesday 09:00 - 17:00
                  Thursday 09:00 - 17:00
                  Friday 09:00 - 17:00
                  Saturday NO LEADS
                  Sunday NO LEADS
 */
 
 
 
   if($_SESSION['USER_IP'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMm     = date("i");
      $dateDdText = date("N");

      if($dateDdText > 5 || $timeHh < 9 || $timeHh >= 17)
      {
         $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_5:00PM";
         $error               = true;
      }
   }
   
   //SKIP if "Age" < 25
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error            =  true;
   }


   //SKIP if "Full UK Licence" < 5 Years
   $period_of_licence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($period_of_licence <= 6)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_4_5_YEARS";
      $error                       =  true;
   }


   return $error;
?>

<?php
   //    Motorcade (City) - Taxi    //
   $error = false;

   
   //If Type of Cover = Fully Comprehensive, Third Party Fire & Theft, skip if "Vehicle Age" > 8 Years (eg. skip 2005, 2004, 2003 etc.)
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover == 1   OR  $typeOfCover == 2)
   {
      $vehicleManufYear = $_SESSION["_YourDetails_"]["year_of_manufacture"];
      $disYear          = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"];

      $vehicleAge = $disYear - $vehicleManufYear;

      if($vehicleAge > 8)
      {
         $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_FULL_TPFT_SKP_VEH_AGE_OVR_8";
         $error                       =  true;
      }
   }


   if($typeOfCover == 3)
   {
      $vehicleManufYear = $_SESSION["_YourDetails_"]["year_of_manufacture"];
      $disYear          = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"];

      $vehicleAge = $disYear - $vehicleManufYear;

      if($vehicleAge > 12)
      {
         $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_TPSKP_VEH_AGE_OVR_12";
         $error                       =  true;
      }
   }

   //ACCEPT only the following postcodes
   $postCode        = $_SESSION['_YourDetails_']['postcode'];
   $postCodePrefix  = $_SESSION['_YourDetails_']['postcode_prefix'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];
    
   $accPcodesArray = array(
//      "AL",
      "EC",
      "EN",
      "MK",
      "N",
      "NW",
//      "SG",
      "WC",
   );

   if(! in_array($postcodeToCheck,$accPcodesArray) )
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCodePrefix]";
      $error               = true;
   }
      $accPcodesPrefArray = array(
//      "AL",
      "MK",
//      "SG",
   );

      $accPcodesSecondPrefArray = array(
      "EC",
      "EN",
      "N",
      "NW",
      "WC",
   );
      
      
   /*if postcode = AL[0-99]*, MK[0-99]*, SG[0-99]*
      apply filters below;

      skip if user age < 25 or > 65

      skip if Full UK License = 
      Under 6 Months
      6 Months to 1 Year
      1-2 Years

      skip if Taxi Badge = 
      No Badge
      6 Months to 1 Year

      skip if Taxi No Claims Bonus > 3 Years 
      -----------------------------------------------

      if postcode = EC[0-99]*, EN[0-99]*, N[0-99]*, NW[0-99]*, WC[0-99]*

      apply filters below;

      skip if user age     <    23    or   >    65 
      skip if     Taxi Use = Public Hire

   */

   if(in_array($postcodeToCheck,$accPcodesPrefArray) )
   {
      //skip if user age < 25 or > 65
      $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
      $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
      $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

      $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
      $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
      $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

      $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
      $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

      $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

      if($propAge < 25 OR $propAge > 65)
      {
         $personalizedError[] = "SKIP_RULE_PROP_AGE_POSTCODE--AGE_ACC_BETW_25_AND_65_PCODES";
         $error            =  true;
      }
      
     //skip if Full UK License = Under 6 Months, 6 Months to 1 Year, 1-2 Years 
      $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
      if($ukLicence < 4)
      {
         $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE_PCODES--YEARS_INT_SKP_UND_2_3_YEARS_PCODES";
         $error            =  true;
      }

      //skip if Taxi Badge = No Badge, 6 Months to 1 Year    
      $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
      if($taxiBadge < 4)
      {
         $personalizedError[] = "SKIP_RULE_BADG_PCODES--YEARS_INT_SKP_UND_1_2_YEARS_PCODES";
         $error               = true;
      }

      //skip if Taxi No Claims Bonus > 3 Years 
      $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
      if($taxiNCB > 3)
      {
         $personalizedError[] = "SKIP_RULE_NCB_POSTCODE--NCB_SKP_OVER_3_PCODES";
         $error            =  true;
      }

   }
   elseif(in_array($postcodeToCheck,$accPcodesSecondPrefArray))
   {
      //skip if user age < 23 or > 65
      $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
      $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
      $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

      $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
      $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
      $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

      $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
      $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

      $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

      if($propAge < 23 OR $propAge > 65)
      {
         $personalizedError[] = "SKIP_RULE_PROP_AGE_POSTCODE--AGE_ACC_BETW_23_AND_65_PCODES";
         $error            =  true;
      }

//      $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
//      if($taxiUse == 2)
//      {
//         $personalizedError[] = "SKIP_RULE_USE_PCODES--USE_SKP_PUB_HIRE_PCODES";
//         $error          =  true;
//      }

   }
   
   
////If "Taxi Type" = "Black Cab" SKIP if "Taxi Use" = "Private Hire"
//    $taxiType = $_SESSION["_YourDetails_"]["taxi_type"];  
//    $taxiUse  = $_SESSION["_YourDetails_"]["taxi_used_for"];
//    if($taxiType == 1)
//    {
//       if($taxiUse == 1)
//       {
//          $personalizedError[] = "SKIP_RULE_USE--TYP_BLACK_CAB_SKP_PHIRE";
//          $error          =  true;
//       }
//    }
   
   
   // only ACCEPT Taxi Use = Private Hire
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse != 1)
   {
      $personalizedError[] = "SKIP_RULE_USE--USE_ACC_PRIV_HIRE";
      $error               =  true;
   }
   
   return $error;

?>

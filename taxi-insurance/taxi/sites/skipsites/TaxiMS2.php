<?php

   $error = false;
   
 
   //Only accept the attached Taxi Plating Authorities.
   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

   $accLicensingAuthorityArray = array(
"ADUR",
"ALLERDALE",
"AMBER VALLEY",
"ARUN",
"ASHFIELD",
"ASHFORD",
"BURY",
"BABERGH",
"BARNSLEY",
"BARROW IN FURNESS",
"BASINGSTOKE & DEANE",
"BASSETLAW",
"BATH & N.E SOMERSET",
"BLYTH VALLEY",
"BOLSOVER",
"BOSTON",
"BOURNEMOUTH",
"BRAINTREE",
"BRECKLAND",
"BRIDGENORTH",
"BRIGHTON & HOVE",
"BROADLAND",
"BROMSGROVE",
"BROXTOWE",
"CAMBRIDGE",
"CANTERBURY",
"CARLISLE",
"CASTLE MORPETH",
"CHARNWOOD",
"CHELTENHAM",
"CHERWELL",
"CHESTERFIELD",
"CHICHESTER",
"CHRISTCHURCH",
"YORK",
"COLCHESTER",
"COPELAND",
"CORBY",
"COTSWOLD",
"CRAVEN",
"CRAWLEY",
"DARLINGTON",
"DAVENTRY",
"DERBYSHIRE DALES",
"DERWENTSIDE",
"DONCASTER",
"DOVER",
"DURHAM",
"EASINGTON",
"EAST CAMBRIDGESHIRE",
"EAST DORSET",
"EAST HAMPSHIRE",
"EAST LINDSEY",
"NORTHAMPTON",
"YORK",
"STAFFORD",
"EASTBOURNE",
"EASTLEIGH",
"EDEN",
"EREWASH",
"EXETER",
"FAREHAM",
"FENLAND",
"FOREST HEATH",
"FOREST OF DEAN",
"GEDLING",
"GLOUCESTER",
"GLOUCESTERSHIRE COUNTY",
"GRAVESHAM",
"GUILDFORD",
"HAMBLETON",
"HARBOROUGH",
"HARROGATE",
"HART",
"HARTLEPOOL",
"HASTINGS",
"HAVANT",
"HEREFORD",
"HERTSMERE",
"HIGH PEAK",
"HINCKLEY & BOSWORTH",
"HORSHAM",
"HUNTINGDONSHIRE",
"IPSWICH",
"KENNET",
"KETTERING",
"KINGS LYNN & W NORFOLK",
"KINGSTON-UPON-HULL",
"LANCASTER",
"LEWES",
"LICHFIELD",
"LINCOLN",
"MAIDSTONE",
"MALDON",
"MALVERN HILLS",
"MANSFIELD",
"MELTON",
"MENDIP",
"MID BEDFORDSHIRE",
"MID SUFFOLK",
"MID SUSSEX",
"NEW FOREST",
"NEWARK & SHERWOOD",
"NEWCASTLE UNDER LYME",
"NORTH EAST DERBYSHIRE",
"NORTH EAST LINCOLNSHIRE",
"NORTH KESTEVEN",
"NORTH LICOLNSHIRE",
"NORTH SHROPSHIRE",
"NORTH SOMERSET",
"WARWICK",
"NORTH WEST LEICESTER",
"NORTH WILTSHIRE",
"NORTHAMPTON",
"NORWICH",
"NUNEATON & BEDWORTH",
"OADBY & WIGSTON",
"OSWESTRY",
"OXFORD",
"PETERBOROUGH",
"PLYMOUTH",
"POOLE",
"PORTSMOUTH",
"REDDITCH",
"RIBBLE VALLEY",
"RICHMONDSHIRE",
"ROTHER",
"ROTHERHAM",
"RUGBY",
"RUSHCLIFFE",
"RUSHMOOR",
"RUTLAND",
"RYEDALE",
"SALISBURY",
"SCARBOROUGH",
"SEDGEFIELD",
"SEDGEMOOR",
"SELBY",
"SHEPWAY",
"SHREWSBURY",
"SOUTH CAMBRIDGE",
"SOUTH DERBYSHIRE",
"SOUTH GLOUCESTER",
"SOUTH HOLLAND",
"SOUTH KESTEVEN",
"SOUTH LAKELAND",
"SOUTH NORFOLK",
"SOUTH NORTHANTS",
"SOUTH OXFORDSHIRE",
"SOUTH SHROPSHIRE",
"SOUTH SOMERSET",
"STAFFORD",
"SOUTHAMPTON",
"ST EDMUNDSBURY",
"STAFFORD",
"STAFFS MOORLANDS",
"STEVENAGE",
"STRATFORD ON AVON",
"STROUD",
"SUFFOLK COASTAL",
"SWALE",
"SWINDON",
"TAMWORTH",
"TAUNTON DEANE",
"TEESDALE",
"TENDRING",
"TEST VALLEY",
"TEWKESBURY",
"THANET DISTRICT",
"TORBAY",
"TUNBRIDGE WELLS",
"TYNEDALE",
"UTTLESFORD",
"VALE OF WHITE HORSE",
"WANSBECK",
"WARWICK",
"WAVENEY",
"WAVERLEY",
"WEALDEN",
"WEAR VALLEY",
"WELLINGBOROUGH",
"WEST DORSET",
"WEST LINDSEY",
"WEST OXFORD",
"WEST SOMERSET",
"WEST WILTSHIRE",
"WEYMOUTH & PORTLAND",
"WINCHESTER",
"WORCESTER",
"WORTHING",
"WYRE",
"WYRE FORREST",
   );

   if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
  	   $error 			      =  true;
   }


   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $DIS              = $DISyyyy."-".$DISmm."-".$DISdd;
   $inTheFuture21Days = date('Y-m-d',mktime(0,0,0,date('m'), date('d')+21, date('Y')));

   //skip if Insurance Start Date > 21 days after Quote Date
   if($DIS > $inTheFuture21Days)
   {
      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_SKP_21_DAY_AFT";
      $error               = true;
   }


   $drNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   //cannot quote because of the no claims bonus status - skip if the value selected is less than 2 years
   if($drNCB == 0 or $drNCB == 1)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB_1YEAR";
      $error            =  true;
   }


   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

// cannot quote because of the proposer age - skip if age is under 40 or over 65 years
   if($propAge < 40 || $propAge > 65)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_40_AND_OVR_65";
      $error            =  true;
   }

   // cannot quote because of the taxi badge - skip if the value is less than 2 years
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge <= 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_2_YEARS";
      $error            =  true;
   }

   
// Skip if Claims Last 5 Years = yes
   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
   if($claim5Years == "Yes")
   { 
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_YES";
      $error 			   =  true;
   }
   
//  Skip if Convictions Last 5 Years = yes
   $conv5Years = $_SESSION['_YourDetails_']['convictions_5_years'];
   if($conv5Years == "Yes")
   {
      $personalizedError[] = "SKIP_RULE_CONV_LAST_5_YEARS--SKP_YES";
      $error                       =  true;
   }


   return $error;

?>

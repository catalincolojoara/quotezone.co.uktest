<?php

   $error = false;

   //"HARLOW",
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $acceptLicensingAuthorityArray = array(
"HARLOW",
"CHELMSFORD",
   );

   if( ! in_array($licesingAuthority,$acceptLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
	  $error 			   =  true;
   }

   // Receive leads between all hours Monday - Thursday
   $timeHh     = date("G");
   $timeMin    = date("i");
   $dateDdText = date("N");

   if($dateDdText > 4 AND $dateDdText != 5 )
   {
      $personalizedError[] = "SKIP_RULE_DAY_MON_THU--TIME_ACC_ALL_DAY";
      $error               = true;
   }

   // Receive Leads between 08.00 and 17.30 on Friday.
   if($dateDdText == 5 AND ( $timeHh < 8 || ($timeHh == 17 && $timeMin >= 30) || $timeHh >= 18 ) )
   {
      $personalizedError[] = "SKIP_RULE_DAY_FRI--TIME_ACC_BETW_8:00AM_TO_5:30PM";
      $error               = true;
   }

   //Only accept leads from the following postcodes
   $postCodePrefix = strtoupper($_SESSION['_YourDetails_']['postcode_prefix']);

   $acceptPostcodesPrefix = array(
   "CM22",
   "CM23",
   "CM24",
   "CM1",
   "CM2",
   "CM3",
   "CM4",
   "CM5",
   );

   if(! in_array($postCodePrefix, $acceptPostcodesPrefix))
   {
      $personalizedError[] = "SKIP_RULE_POST_PREF--ACC_LIST [$postCodePrefix]";
      $error               = true;
   }

   return $error;

?>

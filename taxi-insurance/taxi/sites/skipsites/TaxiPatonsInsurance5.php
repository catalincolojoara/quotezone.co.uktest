<?php

   $error = false;



   //ACCEPT if Proposer Age is between 25 - 69 (e.g. accept 25 and accept 69)
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25 || $propAge > 69)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25_AND_OVR_69";
      $error               =  true;
   }

   // ACCEPT if "Taxi no claims bonus" = 1 Year
   // 											  2 Years
   // 											  3 Years
   $taxiNCB    = $_SESSION['_YourDetails_']['taxi_ncb'];   
   $taxiNCBArray = array(
      "1",
      "2",
      "3",
   );
   if(! in_array($taxiNCB, $taxiNCBArray))
   {
      $personalizedError[] = "SKIP_RULE_NCB--ACC_LIST [$taxiNCB]";
      $error               =  true;
   }


   //SKIP if Taxi Plating Authority is in attached SKIP list
   $licesingAuthoritySkp = $_SESSION['_YourDetails_']['plating_authority'];
   $skpLicensingAuthorityArray = array(
      "ABERDEEN",
      "ABERDEENSHIRE",
      "ARGYLE & BUTE",
      "BERWICK ON TWEED",
      "BIRMINGHAM",
      "CAITHNESS (HIGHLANDS)",
      "CLACKMANNANSHIRE",
      "COVENTRY",
      "DUDLEY",
      "DUMFRIES & GALLOWAY",
      "DUNDEE",
      "EAST AYRSHIRE (KILMARNOCK)",
      "EAST DUNBARTONSHIRE (KIRKINTILLOCH)",
      "EAST LOTHIAN (HADDINGTON)",
      "EAST RENFREWSHIRE (GIFFNOCH)",
      "EDINBURGH",
      "FALKIRK",
      "FIFE",
      "GATESHEAD",
      "GLASGOW",
      "HALTON",
      "INVERCLYDE (GREENOCK)",
      "KNOWSLEY",
      "LIVERPOOL",
      "LONDON PCO",
      "LUTON",
      "MIDDLESBOROUGH",
      "MIDLOTHIAN",
      "NEWCASTLE UPON TYNE",
      "NORTH AYRSHIRE",
      "NORTH LANARKSHIRE (NORTH)",
      "PERTH & KINROSS",
      "RENFREWSHIRE (PAISLEY)",
      "ROSSENDALE",
      "SCOTTISH BORDERS",
      "SEFTON",
      "SHETLAND ISLANDS (LERWICK)",
      "SOUTH AYRSHIRE (AYR)",
      "SOUTH LANARKSHIRE",
      "SOUTH STAFFORDSHIRE",
      "SOUTH TYNESIDE",
      "ST HELENS",
      "STIRLING",
      "SUNDERLAND",
      "WEST DUNBARTONSHIRE (CLYDEBANK)",
      "WEST LOTHIAN (LIVINGSTON)",
      "WIRRALL",
      "BELFAST", 
      "NORTHERN IRELAND",
      "DVA (NI)",
      "LEEDS",
      "WAKEFIELD",
      "SOLIHULL",
   );

   if(in_array($licesingAuthoritySkp,$skpLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthoritySkp]";
      $error               =  true;
   }


//    // SKIP all SMS text leads from being sent to Patons Insurance
//    if($_SESSION["USER_IP"] == "80.94.200.13")
//    {
//       $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
//       $error               =  true;
//    }
   
   
   // SKIP if source is 'Danny Imray Two' white label 
   $wlID  = $_SESSION['_QZ_QUOTE_DETAILS_']['wlUserID'];
   $wlMsg = "'Danny Imray Two'";
   if($wlID == "758")
   {
      $personalizedError[] = "SKIP_RULE_COMPANY_WL--SKP_LIST [$wlMsg]";
      $error               =  true;
   }
   
   return $error;
?>

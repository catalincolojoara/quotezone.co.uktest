<?php

   $error = false;

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   //1.  cannot quote because of the proposer age - accept between 25 and 65 years
   if($propAge < 25 OR $propAge > 65)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_BETW_25_AND_65";
      $error 			   =  true;
   }

   //2. Only accept leads for the following Taxi Plating Authorities:
   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

   $acceptLicensingAuthorityArray = array(
      "BRIDGENORTH",
      "CANNOCK CHASE",
      "LICHFIELD",
      "SOUTH STAFFORDSHIRE",
      "TAMWORTH",
      "TELFORD & WREKIN",
      "WALSALL",
   );

   if( ! in_array($licesingAuthority,$acceptLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
  	  $error 			   =  true;
   }

   //3. If Taxi Plating Authority = Wolverhampton, Dudley or Redditch only accept if Taxi use = Public Hire 2
//   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];
//   $taxiUse = $_SESSION['_YourDetails_']['taxi_used_for'];
//
//   $acceptLicensingAuthorityArray = array(
//      "DUDLEY",
//      "REDDITCH",
//      "WOLVERHAMPTON",
//   );
//
//   if(in_array($licesingAuthority,$acceptLicensingAuthorityArray) && $taxiUse == "1" )
//   {
//      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH_USE--LICEN_WOL_DUD_REDD_ACC_USE_PUBL_HIRE";
//      $error	 		   =  true;
//   }
   

   //4. skip if the value selected is 1-2 years or 2-3 years
   $period_of_licence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($period_of_licence == 3 || $period_of_licence == 4 )
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_2_3_YEARS";
      $error 			   =  true;
   }


   //SKIP if Year Of Manufacture < 2005(skip 2005, 2004, 2003 etc...)
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   if($yearOfManufacture < 2005)
   {
      $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--YEAR_SKP_UND_2005";
      $error                       =  true;
   }



   return $error;

?>

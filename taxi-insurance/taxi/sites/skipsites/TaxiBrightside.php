<?php

   $error = false;

     // If Type = Black Cab, Saloon, only accept the value of "8+"
   $taxiType = $_SESSION['_YourDetails_']['taxi_type'];
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];   
   $skipedTaxiTypesArray = array(
      "1",
      "2",
   );	 
   if(in_array($taxiType, $skipedTaxiTypesArray))
   {     
      if($taxiCapacity <= '7')
      {
         $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_ACC_OVR_8";
         $error                       =  true;
      }
   }     
   
   if($taxiType == "3")
   {
       if($taxiCapacity <= '8')
       {
		  $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_ACC_OVR_8";
		  $error                       =  true;
       }
    }
     
   // skip if age is under 25 years
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);
   
   if($propAge < 25)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error 			   =  true;
   }

   return $error;

?>

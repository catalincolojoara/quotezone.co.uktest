<?php

   $error = false;
   $personalizedError = "";

   //1.One answer will get leads only for this postcode --  WRJK-B87
   $postPrefix = $_SESSION['_YourDetails_']['postcode_prefix'];
   $postCode        = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
//       "BN",
//       "DY",
//       "GU",
      "GL",
//      "TN",
//      "RH",
//      "MK",
      "RG",
      "TA",
      "PL",
      "SS",
      "TR",
      "TQ",
      "BH",
      "SN",
      //"B",
      "DA",
      "OX",  // added by request of ticket WRA6-HS9
      "BA",
      "BN",
      "BS",
      "CT",
      "DT",
      "EX",
      "GU",
      "HU",
      "IP",
      "NP",
      "NR",
      "PE",
      "PL",
      "PO",
      "RH",
      "SP",
      "SS",
      "TN",
      "ME",
      "BR",
      "SO",
      "SN",
      "OX",
      "RG",
      "GL",
      "EX",

   );
  
    $postcodePrefArray = array(
//      "S1",
//      "S2",
//      "S3",  
//      "S4",
//      "S5",
//      "S6",
//      "S7",
//      "S8",
//      "S9",
     );
   
   if((! in_array($postcodeToCheck,$acceptedPcodesArray)) && (! in_array($postPrefix,$postcodePrefArray)))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error 			      =  true;
   }
   
   //2.Skip if user age < 21 years old
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 21)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_21";
      $error 		   =  true;
   }
  
   //3.Only Fully Comprehensive
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover != 1)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_ACC_FULL_COMPR";
      $error 		   =  true;
   }

   $licesingAuthoritySkp = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $skpLicensingAuthorityArray = array(
"LONDON PCO",
   );

   if(in_array($licesingAuthoritySkp,$skpLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthoritySkp]";
      $error               =  true;
   }

   return $error;

?>

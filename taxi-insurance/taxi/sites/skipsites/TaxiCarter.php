<?php

   $error = false;

   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

   $skipLicensingAuthorityArray = array(
"LONDON PCO",

   );

   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
          $error                           =  true;
   }

   $postCode     = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BT",
      "B",
      "L",
      "M",
      "CV",
      "BD",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }


   return $error;

?>
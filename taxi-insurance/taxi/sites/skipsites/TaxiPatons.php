<?php

   $error = false;

	
	// SKIP if "Taxi Badge" = No Taxi Badge, Under 6 months
	$taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 3)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_YEAR";
      $error               = true;
   }

	
   // taxi plating authority accepting filter
   $platingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   
   $acceptedPlatingAuthiritiesArray = array(
      "HALTON",
      "ST. HELENS",
      "WARRINGTON",
      "WEST LANCASHIRE",
      "WIGAN",
      "SEFTON",
      "KNOWSLEY",
      "LIVERPOOL", // update BECAUSE of second filter (WRE1-0GV)
      "WIRRAL",
      "MANCHESTER",
      "SALFORD",
      "TRAFFORD",
      "TAMESIDE",
   );

   if(! in_array($platingAuthority,$acceptedPlatingAuthiritiesArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$platingAuthority]";
  	  $error 			   =  true;
   }  
   
   // 2. Skip if driver age < 25 years old
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error               = true;
   }

  
   // SKIP all SMS text leads from being sent to Patons Insurance
   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   // skip BT postcode
   $postCode     = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skippedPcodesArray = array(
      "BT",
   );

   if(in_array($postcodeToCheck,$skippedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }
	
	
	/*
     ACCEPT leads Monday 09:00 - 17:00
                  Tuesday 09:00 - 17:00
                  Wednesday 09:00 - 17:00
                  Thursday 09:00 - 17:00
                  Friday 09:00 - 17:00
                  Saturday NO LEADS
                  Sunday NO LEADS
 */



   if($_SESSION['USER_IP'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMm     = date("i");
      $dateDdText = date("N");

      if($dateDdText > 5 || $timeHh < 9 || $timeHh >= 17)
      {
         $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_5:00PM";
         $error               = true;
      }
   }



   return $error;
?>

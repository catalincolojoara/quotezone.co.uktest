<?php

   $error = false;

   //only accept if postcode = SA[1-99]*
   $postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $accPcodesArray = array(
      "SA",
   );

   if(! in_array($postcodeToCheck,$accPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error                       =  true;
   }

   // skip if name = Test Test (First Name, Surname)
   $firsName  = $_SESSION['_YourDetails_']['first_name'];
   $surname   = $_SESSION['_YourDetails_']['surname'];

   if($firsName == "Test" && $surname == "Test")
   {
      $personalizedError[] = "SKIP_RULE_PROP_NAME--NAME_SKP_TEST";
      $error               =  true;
   }


   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   //skip if user age < 25
   if($propAge < 25)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error                       =  true;
   }

   if($_SESSION['USER_IP'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdText = date("N");

      switch($dateDdText)
      {   
         

         case '1':
         case '2':
         case '3':
         case '4':
         case '5':
         if(($timeHh < 9) || (($timeHh == 9) && ($timeMin < 30)) || ($timeHh >= 17))
         {
            $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:30AM_TO_5:00PM";
            $error                         =  true;
         }
         break;
         case '6':
         case '7':
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
            $error                         =  true;
            break;

      }
   }

   // SKIP if "Affiliate ID" = "dnainsurance2"

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $wlID = $_SESSION['_QZ_QUOTE_DETAILS_']['wlUserID'];

      if($wlID == 758)
      {
         $personalizedError[] = "SKIP_RULE_COMPANY_WL--WL_SKP_DNAINSURANCE2";
         $error               =  true;
      }
   }



   if($_SESSION["_QZ_QUOTE_DETAILS_"]["SERVER_IP"] == "192.168.12.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   //cannot quote because lead is form sms reminder - skip leads sent from 13 server
   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   return $error;

?>
<?php

   $error = false;

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   
   if($propAge < 23)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_23";
      $error 			   =  true;
   }

   $postCode     = $_SESSION['_YourDetails_']['postcode'];

    // 1. Tradex Insurance will not accept leads from this postcodes :
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "N",
      "E",
      "W",
      "SE",
      "SW",
      "EC",
      "NW",
      "CR",
      "UB",
      "TW",
      "IG", 
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error 			   =  true;
   }


   return $error;

?>

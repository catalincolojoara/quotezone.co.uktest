<?php

   $error = false;

  
   //1. Coversure Rubery will receive leads only for this postcodes.
   $postCodePrefix = $_SESSION['_YourDetails_']['postcode_prefix'];

   $acceptedPcodesArray = array(
      "B14",
      "B30", 
      "B31", 
      "B32", 
      "B38", 
      "B47", 
      "B45", 
      "B61", 
      "B62", 
      "B63", 
      "B64", 
      "B90", 
      "B91", 
      "B93", 
      "B94", 
   );

   if(! in_array($postCodePrefix,$acceptedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POST_PREF--ACC_LIST [$postCodePrefix]";
      $error 			   =  true;
   }

   return $error;

?>

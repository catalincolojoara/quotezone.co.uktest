<?php

   $error = false;

   // ADD the following councils to the SKIPPED Taxi Plating Authority filter
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $skipLicensingAuthorityArray = array(
"NEWCASTLE UNDER LYME",
"NEWCASTLE UPON TYNE",
"CARDIFF",
"BLACKBURN WITH DARWEN",
"BLACKPOOL",
"ROSSENDALE",
"WOLVERHAMPTON",
"BARKING",
"BARNET",
"BEXLEY",
"BIRMINGHAM",
"BLACKBURN WITH DARWEN",
"BRADFORD",
"BRENT",
"BROMLEY",
"BURNLEY",
"BURY",
"CALDERDALE",
"CAMDEN",
"CROYDEN",
"EALING",
"ENFIELD",
"LONDON PCO",
"GREENWICH",
"HACKNEY",
"HAMMERSMITH",
"HARINGEY",
"HAVERING",
"HILLINGDON",
"HOUNSLOW",
"HYNDBURN",
"ISLINGTON",
"KENSINGTON",
"KINGSTON UPON THAMES ROYAL",
"KNOWSLEY",
"LAMBETH",
"LEEDS",
"LEWISHAM",
"LIVERPOOL",
"LONDON PCO",
"LONDON PCO",
"LUTON",
"MANCHESTER",
"MERTON",
"NEWHAM",
"OLDHAM",
"REDBRIDGE",
"RICHMOND",
"ROCHDALE",
"SALFORD",
"SEFTON",
"SOUTHWARK",
"ST HELENS",
"STOCKPORT",
"SUTTON",
"TAMESIDE",
"TOWER HAMLETS",
"TRAFFORD",
"WALSALL",
"WALTHAM FOREST",
"WANDSWORTH",
"WARRINGTON",
"WIRRALL",
"BLACKPOOL",
"PRESTON",
"SHEFFIELD",
"MIDDLESBOROUGH",
"NORTH TYNESIDE",
"LEEDS",
   );


   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error 			   =  true;
   }

   // ACCEPT only if "Taxi Driver(s)" = "Insured Only"
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers != 1)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_ACC_INS_ONLY";
      $error                       =  true;
   }

   //Skip if Taxi Year of Manufacture < 2003
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   if($yearOfManufacture < 2003)
   {
      $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--YEAR_SKP_UND_2003";
      $error                       =  true;
   }

   //SKIP if "Estimated Taxi Value (£)" < 2000
   $estimated_value = $_SESSION['_YourDetails_']['estimated_value'];
   if($estimated_value < 2000)
   {
      $personalizedError[] = "SKIP_RULE_VEH_VALUE--POUND_SKP_UND_2000";
      $error                       = true;
   }

   //SKIP if "Insurance Start Date" < "Quote Date" + 2 Days
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   // today date
   $day   = date("d");
   $month = date("m");
   $year  = date("Y");

   $daysOfInsuranceStart = (mktime(0,0,0,$DISmm,$DISdd,$DISyyyy) - mktime(0,0,0,$month,$day,$year)) / (86400);

   if($daysOfInsuranceStart < 7)
   {
      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_SKP_WITH_7_DAY_AFT";
      $error                       =  true;
   }


// SKIP if "Taxi No Claims Bonus" = "No NCB"
//   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
//   if($taxiNCB == 0)
//   {
//      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
//      $error            =  true;
//   }

   //SKIP if "Taxi Registration" field is BLANK
   $taxiRegNumber = $_SESSION['_YourDetails_']['vehicle_registration_number'];
   if($taxiRegNumber == "")
   {
      $personalizedError[] = "SKIP_RULE_REG_NUMBER--SKP_BLANK";
      $error               =  true;
   }


//   SKIP if "Postcode" = BT[0-99]*
   $postCode = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BT",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--POSTCODE_SKP_BT [$postCode]";
      $error                       =  true;
   }


   /* SKIP if "Postcode" = N[0-99]*
                               NW[0-99]*
                               W[0-99]*
                               SW[0-99]*
                               SE[0-99]*
                               E[0-99]*/
   $postCode     = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skpPcodesArray = array(
      "N",
      "NW",
      "W",
      "SW",
      "SE",
      "E",
   );

   if(in_array($postcodeToCheck,$skpPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error 		   =  true;
   }


   //SKIP if "Taxi Badge" = "No Badge", "Under 6 Months", "6 Months to 1 Year"
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--BADG_SKP_N_BADG_UND6_MONTH_6MONTH";
      $error               =  true;
   }

   //SKIP if "Age" < 25
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error               =  true;
   }

   //   2. Skip if Claims Last 5 Years = Yes, Claims Made
   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
   if($claim5Years == "Yes")
   { 
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_YES";
      $error            =  true;
   }

   //   3. Skip if Convictions Last 5 Years = Yes  
   $conv5Years = $_SESSION['_YourDetails_']['convictions_5_years'];
   if($conv5Years == "Yes")
   { 
      $personalizedError[] = "SKIP_RULE_CONV_LAST_5_YEARS--SKP_YES";
      $error            =  true;
   }

   return $error;

?>

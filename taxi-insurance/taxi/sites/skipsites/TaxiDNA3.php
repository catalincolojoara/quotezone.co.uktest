<?php

   $error = false;


   // SKIP if "Max. Number of Passengers" > 8
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity == '8')
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_8";
      $error               =  true;
   }

   // SKIP if "Type of Cover" = Third party fire and theft
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover == 2)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_SKP_TPFT";
      $error               =  true;
   }

   // SKIP if "Full UK Licence" < 2 years
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence < 4)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_2_3_YEARS";
      $error               = true;
   }

   // SKIP if "Taxi No Claims Bonus" > 3 years
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($taxiNCB > 3)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_OVR_3_YEARS";
      $error               =  true;
   }

   // SKIP if "Age" < 21
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 21)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_21";
      $error               =  true;
   }

   // SKIP if "Postcode" = BT[1-99]*
   $postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BT",
      "BD",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error               =  true;
   }

   // SKIP attached Taxi Plating Authorities (DNA3)
   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];
   $skipLicensingAuthorityArray = array(
"BRADFORD",
   );

   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error               =  true;
   }

   //new time filters
   /*
   ACCEPT leads Monday 00:00 - 23:59
                Tuesday 00:00 - 23:59
                Wednesday 00:00 - 23:59
                Thursday 00:00 - 23:59
                Friday 00:00 - 23:59
                Saturday 00:00 - 12:00
                Sunday 00:00 - 23:59
   */

   if($_SESSION['USER_IP'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMm     = date("i");
      $dateDdText = date("N");

      switch($dateDdText)
      {

         case '6': // Sat
         if($timeHh >= 12)
         {
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT--TIME_SKP_OVR_12:00PM";
            $error               =  true;
         }
         break;

      }
   }

   //filter in case of WL DNAINSURANCE1 leads
   $wlID = $_SESSION['_QZ_QUOTE_DETAILS_']['wlUserID'];

   if($wlID == "688")
   {
      $personalizedError[] = "SKIP_RULE_COMPANY_WL--WL_SKP_DNAINSURANCE1";
      $error               =  true;
   }

   if($wlID == "758")
   {
      $personalizedError[] = "SKIP_RULE_COMPANY_WL--WL_SKP_DNAINSURANCE2";
      $error               =  true;
   }
   
   
   // SKIP if Wl source = dnainsurance3
   $wsID = $_SESSION['_QZ_QUOTE_DETAILS_']['ws_id'];

   if($wsID == "5fdbf8c1a05cd4cdf82ed72104896c7e")
   {
      $personalizedError[] = "SKIP_RULE_COMPANY_WL--WL_SKP_DNAINSURANCE3";
      $error               =  true;
   }
   

   return $error;

?>
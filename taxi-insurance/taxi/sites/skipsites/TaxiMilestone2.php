<?php 

   $error = false;


   /*
   SKIP if "Age" < 21
                        > 35*/
   //  cannot quote because of the proposer age - skip if age is under 21 or over 39 years
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 21 || $propAge > 30)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_21_AND_OVR_30";
      $error               = true;
   }



//    ACCEPT only if "Full UK licence" = "2 - 3 Years"
//                                                          "3 - 4 Years"
//                                                          "4 - 5 Years"
   $periodOfFullUkLicenceHeld = $_SESSION["_YourDetails_"]["period_of_licence"];

   $filteredPeriodOfFullUkLicenceHeldArray = array(
//       "1" => "Under 6 Months",
//       "2" => "6 Months To 1 Year",
//	      "3" => "1 - 2 Years",
       "4" => "2 - 3 Years",
       "5" => "3 - 4 Years",
       "6" => "4 - 5 Years",
      );

   if(! array_key_exists($periodOfFullUkLicenceHeld,$filteredPeriodOfFullUkLicenceHeldArray))
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--ACC_LIST [$periodOfFullUkLicenceHeld]";
      $error            =  true;
   }


   $vehicleManufYear = $_SESSION["_YourDetails_"]["year_of_manufacture"];
   $disYear          = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"];
   $vehicleAge = $disYear - $vehicleManufYear;
   if($vehicleAge > 10)
   {
      $personalizedError[] = "SKIP_RULE_VEH_AGE--AGE_SKP_OVR_10";
      $error            =  true;
   }

   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $accLicensingAuthorityArray = array(
"NORTHERN IRELAND",
"NORTHERN IRELAND",
"LONDON PCO",
   );

   if(in_array($licesingAuthority,$accLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
  	  $error 			   =  true;
   }

   $postCode     = $_SESSION['_YourDetails_']['postcode'];

   /*
      ACCEPT leads Monday 08:00 - 17:00
                  Tuesday 08:00 - 17:00
                  Wednesday 08:00 - 17:00
                  Thursday 08:00 - 17:00
                  Friday 08:00 - 17:00
                  Saturday NO LEADS
                  Sunday NO LEADS
               
   */
   
   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {
         case '1':
         case '2':
         case '3':
         case '4':
         case '5':
         if($timeHh < 8 || $timeHh >= 17)
         {
            $personalizedError[] = "SKIP_RULE_SKP_DAY_MON_FRI--TIME_ACC_BETW_8:00AM_TO_5:00PM";
            $error               =  true;
         }
         break;
         case '6': 
         case '7':
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
            $error               =  true;
         break;
      }
   }
 
   // ACCEPT only if "Postcode" 
   
//    $postCode   = $_SESSION['_YourDetails_']['postcode'];
//    preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
//    $postcodeToCheck = $postCodeArr[1];
//    
//    $accPcodesArray = array(
//       "B",
//       "BB",
//       "BL",
//       "CF",
//       "CR",
//       "CV",
//       "DA",
//       "IG",
//       "KT",
//       "L",
//       "LS",
//       "LU",
//       "M",
//       "OL",
//       "S",
//       "TS",
//       "WA",
//       "WN",
//       "WV",
//    );
//    
//    if((! in_array($postcodeToCheck,$accPcodesArray)))
//    {
//       $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
//       $error 			   =  true;
//    }
   
   //cannot quote because lead is form sms reminder - skip leads sent from 13 server
   if($_SESSION["_QZ_QUOTE_DETAILS_"]["SERVER_IP"] == "192.168.12.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   //cannot quote because lead is form sms reminder - skip leads sent from 13 server
   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }
   
   return $error;

?>

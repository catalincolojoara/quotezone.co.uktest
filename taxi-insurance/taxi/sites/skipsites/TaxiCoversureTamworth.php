<?php

   $error = false;
  
   //1. Coversure Tamworth will receive leads only for this postcodes.
   $postCodePrefix = $_SESSION['_YourDetails_']['postcode_prefix'];

   $acceptedPcodesArray = array(
      "B77",
      "B78", 
      "B79", 
      "DE11", 
      "DE12", 
      "WS7", 
      "WS13", 
      "WS14", 
      "WS15", 
   );

   if(! in_array($postCodePrefix,$acceptedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POST_PREF--ACC_LIST [$postCodePrefix]";
      $error 			   =  true;
   }

   return $error;

?>

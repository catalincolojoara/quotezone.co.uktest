<?php

   $error = false;

   //1. Coversure Loughborough will receive leads only for this postcodes:LE11,LE12,LE6,LE7,LE8,LE9,LE16,LE65,LE67,LE18,NG2,NG10,NG11,NG12
   $postCode = $_SESSION['_YourDetails_']['postcode'];
   $postCodePrefix  = $_SESSION['_YourDetails_']['postcode_prefix'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesPrefArray = array(
      "LE11",
      "LE12",
      "LE6",
      "LE7",
      "LE8",
      "LE9",
      "LE16",
      "LE65",
      "LE67",
      "LE18",
      "NG2",
      "NG10",
      "NG11",
      "NG12",
   );

   if(! in_array($postCodePrefix,$acceptedPcodesPrefArray))
   {
      $personalizedError[] = "SKIP_RULE_POST_PREF--ACC_LIST [$postCodePrefix]";
      $error 			   =  true;
   }

   return $error;

?>

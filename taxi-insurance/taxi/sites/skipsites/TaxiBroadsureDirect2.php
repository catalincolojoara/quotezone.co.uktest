<?php

   $error = false;
   // ACCEPT only if "Taxi Plating Authority" = Canterbury, Thanet, Dover, Shepway, Maidstone, Tonbridge & Malling, Tunbridge Wells, Ashford
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $accLicensingAuthorityArray = array(
      "CANTERBURY",
      "THANET DISTRICT",
      "DOVER",
      "SHEPWAY",
      "MAIDSTONE",
      "TONBRIDGE & MALLING",
      "TUNBRIDGE WELLS",
      "ASHFORD",
   );

   if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
      $error               =  true;
   }
 

   //SKIP if "Taxi Plating Authority" = "Sevenoaks","Medway"
   $licesingAuthoritySkp = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $skpLicensingAuthorityArray = array(
      "SEVENOAKS",
      "MEDWAY",
   );

   if(in_array($licesingAuthoritySkp,$skpLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthoritySkp]";
      $error               =  true;
   }

   return $error;
?>

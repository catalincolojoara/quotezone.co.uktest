<?php

   $error = false;

   //1. Skip if Claims Last 5 Years = Yes, Claims Made
   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
   if($claim5Years == "Yes")
   { 
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_YES";
      $error               = true;
   }


   //2. Skip if driver age < 25 years old and > 70 years old
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25 || $propAge > 70)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_BETW_25_AND_70";
      $error               =  true;
   }

   //3. Skip if Taxi Badge = No badge, under 6 months or 6 months to 1 year
   $taxi_badge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxi_badge < 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_2_YEARS";
      $error               =  true;
   }

   //4. Skip if Taxi No Claims Bonus = No NCB
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($taxiNCB < 1)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
      $error 			   =  true;
   }

   //5. Skip all postcodes in the attached list
   $postCode     = strtoupper($_SESSION['_YourDetails_']['postcode']);

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "B",
      "BB",
      "BD",
      "BL",
      "CV",
      "DY",
      "E",
      "EC",
      "EN",
      "HA",
      "HD",
      "HU",
      "HX",
      "IG",
      "L",
      "LE",
      "LS",
      "M",
      "N",
      "NG",
      "NW",
      "OL",
      "S",
      "SE",
      "SK",
      "SW",
      "UB",
      "W",
      "WA",
      "WC",
      "WF",
      "WN",
      "WS",
      "WV",      
   );

   if(in_array($postcodeToCheck, $skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error 			   =  true;
   }

   //6. Keyworker Insure accept leads only between 09 am and 17 pm Monday – Friday.
   $timeMin    = date("i");
   $timeHh     = date("G");
   $dateDdText = date("N");

   if($dateDdText > 5 || $timeHh < 9 || $timeHh > 16)
   {
      $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_4:00PM";
      $error 			   =  true;
   }

   return $error;
?>

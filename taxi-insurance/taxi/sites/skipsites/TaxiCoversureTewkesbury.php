<?php

   $error = false;

   //ACCEPT if Postcode =   GL[0-99]*
//                          BS[0-99]*
//                          WR10
   $postCode       = $_SESSION['_YourDetails_']['postcode'];
   $postCodePrefix = $_SESSION['_YourDetails_']['postcode_prefix'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
         "GL",
         "BS",
   );

   $accPcodesPrefixArray = array(
      "WR10",
   );

   if((! in_array($postCodePrefix,$accPcodesPrefixArray)) && (! in_array($postcodeToCheck,$acceptedPcodesArray)))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error                        =  true;
   }

   $timeHh     = date("G");
   $timeMin    = date("i");
   $dateDdText = date("N");

   if($dateDdText > 5 || $timeHh < 9 || $timeHh > 15)
   {
      $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_4:00PM";
      $error 			   =  true;
   }


   return $error;
?>

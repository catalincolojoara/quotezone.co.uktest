<?php

   $error             = false;

   // ACCEPT if "postcode" = "SN[0-99]*
   $postCode     = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptPcodesArray = array(
      "SN",      
   );

   if(! in_array($postcodeToCheck,$acceptPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error =  true;
   }

   // ACCEPT Monday - Friday 09:00 - 17:00
   if($_SESSION['USER_IP'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMm     = date("i");
      $dateDdText = date("N");

      if($dateDdText > 5 || $timeHh < 9 || $timeHh >= 17)
      {
         $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_5:00PM";
         $error               = true;
      }
   }


   return $error;
?>
<?php

   $error = false;

   // 1. Accept only if taxi type = Black Cab
   $taxiType = $_SESSION['_YourDetails_']['taxi_type'];

   $skipedTaxiTypesArray = array(
      "2" => "Saloon",
      "3" => "MPV",
   );

   if(array_key_exists($taxiType,$skipedTaxiTypesArray))
   {
      $personalizedError[] = "SKIP_RULE_VEH_TYP--SKP_LIST [".$skipedTaxiTypesArray[$taxiType]."]";
      $error 			   =  true;
   }
 
   //2.Only the following plating authorities are acceptable.
   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

   $accLicensingAuthorityArray = array(
"LONDON PCO",
   );

   if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
  	  $error 			   =  true;
   }

   //3.Skip If taxi use = private hire 
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse == 1)
   {
      $personalizedError[] = "SKIP_RULE_USE--USE_SKP_PRIV_HIRE";
      $error			   =  true;
   }


   //skip postcodes ST[1-15]

   $postCode     = strtoupper($_SESSION['_YourDetails_']['postcode']);

   $postCode = trim($postCode);
   $checkPostcode = preg_replace("/\s+/","",$postCode);

   $checkPostcodePrefix = substr($checkPostcode,0,-3);

   $skipedPostcodes = array(
      //"BB55UU",
   );

   if(in_array($checkPostcode, $skipedPostcodes))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error               = true;
   }

   $skipedPrefixesPostcodes = array(
      "^ST[1-9]$",
      "^ST1[0-5]$",
   );

   foreach($skipedPrefixesPostcodes as $index => $pattern)
   {
      if(preg_match("/$pattern/is", $checkPostcodePrefix))
      {
         $personalizedError[] = "SKIP_RULE_POST_PREF--SKP_LIST [$postCode]";
         $error               = true;
         break;
      }
   }


   return $error;
?>

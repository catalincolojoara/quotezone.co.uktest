<?php

   $error = false;
 
    if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
    {
        $timeHh = date("G");
        $dateDdText = date("N");
       
        switch ($dateDdText)
        {
           case '1':
           case '2':
           case '3':
           case '4':
           case '5':
              if($timeHh < 9 || $timeHh >= 17)
              {
                  $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_5:00PM";
                  $error               = true;
              }
           break;
           
           case '6':
           case '7':
                  $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
                  $error               = true;
           break;
        }
    }
    

   $postCode     = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skippedPcodesArray = array(
      "LU", 
   );

   if(in_array($postcodeToCheck,$skippedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error            =  true;
   }  

   // skip if user age < 25
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error               =  true;
   }

   // skip if Full UK License < 4-5 Years
   $period_of_licence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($period_of_licence < 6)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_4_5_YEARS";
      $error            =  true;
   }

   // only accept attached plating authorities
   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

   $acceptLicensingAuthorityArray = array(
"ABERDEEN",
"ABERDEEN",
"ABERDEENSHIRE",
"ABERDEENSHIRE",
"ANGUS",
"ARGYLE & BUTE",
"BADENOCH & STRATHSPEY",
"CAITHNESS (HIGHLANDS)",
"CAMDEN",
"CLYDEBANK",
"DUMFRIES & GALLOWAY",
"DUNDEE",
"EAST AYRSHIRE (KILMARNOCK)",
"EAST KILBRIDE",
"EAST LOTHIAN (HADDINGTON)",
"FALKIRK",
"FIFE",
"HAMILTON",
"INVERNESS (HIGHLANDS)",
"HUNTINGDONSHIRE",
"INVERCLYDE (GREENOCK)",
"INVERNESS (HIGHLANDS)",
"KILMARNOCK & LOUDOUN DISTRICT COUNCIL",
"KIRKALDY DISTRICT COUNCIL",
"LOCHABER (HIGHLAND - FORT WILLIAM)",
"LOTHIAN REGIONAL COUNCIL",
"MIDLOTHIAN",
"MILTON KEYNES",
"MOTHERWELL DISTRICT COUNCIL",
"NAIRN (HIGHLANDS)",
"NORTH AYRSHIRE",
"NORTH HERTS",
"PERTH & KINROSS",
"ROSS & CROMARTY (HIGHLAND - DINGWALL)",
"SKYE & LOCHAISH (HIGHLANDS)",
"SOUTH AYRSHIRE (AYR)",
"STIRLING",
"SUTHERLAND (HIGHLANDS)",
"WEST DUNBARTONSHIRE (CLYDEBANK)",
"WEST LOTHIAN (LIVINGSTON)",
"ORKNEY ISLANDS (KIRKWALL)",
"NORTHAMPTON",
"WELLINGBOROUGH",
"SCOTTISH BORDERS",
"SHETLAND ISLANDS (LERWICK)",
"KETTERING",
"EDEN",
"ALLERDALE",
"CARLISLE",
"CORBY",
"EAST DUNBARTONSHIRE (KIRKINTILLOCH)",
"MORAY",
"SOUTH BEDFORDSHIRE",
   );

   if( ! in_array($licesingAuthority,$acceptLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
     $error             =  true;
   }

   /*
     ACCEPT if "Taxi no claims bonus" > 2 Years OR  ACCEPT if "Private car no claims bonus" > 2 Years
              (e.g. accept as long as one of the above criteria are true)
   */
   
   
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   $privateCarNCB = $_SESSION['_YourDetails_']['private_car_ncb'];
   
   if($taxiNCB <=2 && $privateCarNCB <= 2)
   {
      $personalizedError[] = "SKIP_RULE_NCB_PNCB--NCB_ACC_OVR_2";
      $error               = true;
   }
   

  //ACCEPT if "Taxi badge" > 3 Years (accept 3-4 years and above)
  $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
  if($taxiBadge <= 5)
  {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_2_3_YEARS";
      $error               = true;
  }
   
   //ACCEPT if "Claims last 5 years" = No Claims
   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
   if($claim5Years != "No")
   { 
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--ACC_NO_CLAIMS";
      $error               = true;
   }
  
   // ACCEPT if "Convictions last 5 years" = No
   $conv5Years = $_SESSION['_YourDetails_']['convictions_5_years'];
   if($conv5Years == "Yes")
   {
      $personalizedError[] = "SKIP_RULE_CONV_LAST_5_YEARS--SKP_YES";
      $error                       =  true;
   }
   
   
   
   return $error;

?>

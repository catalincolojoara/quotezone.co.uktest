<?php

   $error = false;
   
   
   /*
      1. ACCEPT if Postcode = SK1-12
                           CW4
                           CW6-12
                           WA16
   */
   
   $postCodePrefix = $_SESSION['_YourDetails_']['postcode_prefix'];
   $accPcodesPrefixArray = array(
      "SK1",
      "SK2",
      "SK3",
      "SK4",
      "SK5",
      "SK6",
      "SK7",
      "SK8",
      "SK9",
      "SK10",
      "SK11",
      "SK12",
      "CW4",
      "CW6",
      "CW7",
      "CW8",
      "CW9",
      "CW10",
      "CW11",
      "CW12",
      "WA16",  
   );
   
   
   if(! in_array($postCodePrefix,$accPcodesPrefixArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCodePrefix]";
      $error               =  true;
   }
   
   //2.Proposer Age is between 25 - 70
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25 || $propAge > 70)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25_AND_OVR_70";
      $error               = true;
   }

   
   //3.ACCEPT if "Full UK licence" > 5 years
   $period_of_licence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($period_of_licence <= 6)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_5_6_YEARS";
      $error 			   =  true;
   }

   
   //4. ACCEPT if "Max. number of passengers" < 9
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity > '8')
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_8";
      $error            =  true;
   } 
   
   /*5. ACCEPT leads MONDAY 0000 -  2359
                   TUESDAY 0000 -  2359
                   WEDNESDAY 0000 -  2359
                   THURSDAY 0000 -  2359
                   FRIDAY 0000 -  1630

    */
   
   if($_SESSION['USER_IP'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMm     = date("i");
      $dateDdText = date("N");

      switch($dateDdText)
      {
         case '1': // Mon
         case '2': // Tue
         case '3': // Wed
         case '4': // Thu
         break;  
        
         case '5': // Fri
         if($timeHh > 16 || ($timeHh == 16 && $timeMm >=30))
         {
                $personalizedError[] = "SKIP_RULE_DAY_FRY--TIME_SKP_OVR_4:30PM";
                $error               =  true;
         }
         break;

         case '6': // Sat
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT--TIME_SKP_ALL_DAY";
            $error               =  true;
         break;
      
         case '7': // Sun
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SUN--TIME_SKP_ALL_DAY";
            $error               =  true;
         break;
      }
   }
   


   return $error;

?>
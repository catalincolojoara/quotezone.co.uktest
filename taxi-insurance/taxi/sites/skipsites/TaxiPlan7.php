<?php

   $error = false;

   // ACCEPT only if "Type of cover" = "Fully comprehensive"
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover != 1)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_ACC_FULL_COMPR";
      $error                       =  true;
   }
   
   
   //SKIP if "Age" < 30   > 70
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 30 || $propAge > 70)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_30_AND_OVR_70";
      $error               =  true;
   }

   
   // SKIP if "Full UK licence" = "1 - 2 Years", "2 - 3 Years", "3 - 4 Years"
   $period_of_licence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($period_of_licence < 6)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_4_YEARS";
      $error            =  true;
   }
   

   // SKIP if "Taxi badge" = "No Badge", "Under 6 Months", "6 Months To 1 Year"
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_2_YEARS";
      $error               = true;
   }
                                   

   // SKIP if "Year of Manufacture" < 2004   
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   if($yearOfManufacture < 2004)
   {
      $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--YEAR_SKP_UND_2004";
      $error                       =  true;
   }                                                                    

   
   // SKIP if "Estimated taxi value (£)" < 20,000 > 75,000
   $estimated_value = $_SESSION['_YourDetails_']['estimated_value'];
   if($estimated_value > 75000)
   {
      $personalizedError[] = "SKIP_RULE_VEH_VALUE--POUND_SKP_OVR_75000";
      $error               = true;
   }
   if($estimated_value < 20000)
   {
      $personalizedError[] = "SKIP_RULE_VEH_VALUE--POUND_SKP_UND_20000";
      $error               = true;
   }
   
   
   // SKIP if "Taxi use" = "Public Hire"
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse == 2)
   {
      $personalizedError[] = "SKIP_RULE_USE_PCODES--USE_SKP_PUB_HIRE";
      $error          =  true;
   }
   
   
   // SKIP if "Taxi driver(s)" = "Insured and 2+ Named Drivers", "Insured and Spouse"
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers > 2)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_SKP_INS_AND_2+_NAM_DRV_OR_SPOUSE";
      $error               =  true;
   }
   
   
   // SKIP if "Taxi type" = "Black Cab"
   $taxiType = $_SESSION['_YourDetails_']['taxi_type'];
   if($taxiType == 1)
   {
      $personalizedError[] = "SKIP_RULE_VEH_TYP--TYP_SKP_BLACK_CAB";
      $error            =  true;
   }
   
   
   // SKIP if "Max. number of passengers" > 7
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity > '7')
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_7";
      $error            =  true;
   }
   
   
   // SKIP if "Postcode" list                                  
   $postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BB",
      "BD",
      "BL",
      "BT",
      "LU",
      "OL",      
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }
   
   

   /*ACCEPT leads Monday 09:00 - 12:30 and 14:00 - 17:30
                        Tuesday 09:00 - 12:30 and 14:00 - 17:30
                   Wednesday 09:00 - 12:30 and 14:00 - 17:30
                       Thursday 09:00 - 12:30 and 14:00 - 17:30
                            Friday 09:00 - 12:30 and 14:00 - 17:30
                        Saturday NO LEADS
                          Sunday 09:00 - 12:30 and 14:00 - 17:30*/

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMm     = date("i");
      $dateDdText = date("N");

      switch($dateDdText)
      {
         case '1': // Mon
         case '2': // Tue
         case '3': // Wed
         case '4': // Thu
         case '5': // Fri
           if($timeHh < 9 || $timeHh > 17 || $timeHh == 13 || ($timeHh == 12 && $timeMm >= 30) || ($timeHh == 17 && $timeMm >= 30))
            {
               $personalizedError[] = "SKIP_RULE_DAY_MON_THU--TIME_ACC_BETW_9:00_12:30_AND_14:00_17:30PM";
               $error               =  true;
            }
            break;

         case '6': // Sat
               $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT--TIME_SKP_ALL_DAY";
               $error               =  true;

            break;

         case '7':  // Sun
              if($timeHh < 9 || $timeHh > 17 || $timeHh == 13 || ($timeHh == 12 && $timeMm >= 30) || ($timeHh == 17 && $timeMm >= 30))
            {
               $personalizedError[] = "SKIP_RULE_DAY_SUN--TIME_ACC_BETW_9:00_12:30_AND_14:00_17:30PM";
               $error               =  true;
            }
            break;

      }
   }
   
            
   return $error;

?>

<?php

   $error = false;


   // 1. Only accept leads from postcodes BT[1-99]*
   $postCode     = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $accPcodesArray = array(
      "BT",
   );

   if(!in_array($postcodeToCheck,$accPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error 		   =  true;
   }

   //2. Oakland will not receive leads at this time.
   $timeHh     = date("G");
   $timeMin    = date("i");
   $dateDdNum  = date("N");

   switch($dateDdNum)
   {
      case '6': // Sat
      case '7': // Sun
         $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
         $error 	      =  true;

         break;

      default: // Mon - Fri
         if(($timeHh < 8) || ($timeHh >= 16))
         {
            $personalizedError[] = "SKIP_RULE_SKP_DAY_MON_FRI--TIME_ACC_BETW_8:00AM_TO_4:00PM";
            $error 		 =  true;
         }

         break;
   }

   //skip if taxi no claims bonus = no ncb, 1 year
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($taxiNCB < 2)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_2_YEARS";
      $error               =  true;
   }

   //4. Skip if taxi use != private hire.
   $taxiUse = $_SESSION['_YourDetails_']['taxi_used_for'];
   if($taxiUse != 1)
   {
      $personalizedError[] = "SKIP_RULE_USE--USE_ACC_PRIV_HIRE";
      $error               = true;
   }

   return $error;
?>

<?php

   $error = false;



	//ACCEPT only if "Taxi plating authority" = see attached
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $skipLicensingAuthorityArray = array(
		"ABERDEEN",
		"ABERDEENSHIRE",
		"ADUR",
		"ALLERDALE",
		"ALNWICK",
		"AMBER VALLEY",
		"ANGUS",
		"ARGYLE & BUTE",
		"BABERGH",
		"BARROW IN FURNESS",
		"BATH & N.E SOMERSET",
		"BERWICK ON TWEED",
		"BOURNEMOUTH",
		"BRACKNELL FOREST",
		"BRAINTREE",
		"BRECKLAND",
		"BRIGHTON & HOVE",
		"BROADLAND",
		"CAMBRIDGE",
		"CANTERBURY",
		"CARADON",
		"CARLISLE",
		"CARMARTHENSHIRE",
		"CARRICK",
		"CEREDIGION",
		"CHELTENHAM",
		"CHESTER",
		"CHESTERFIELD",
		"CHESTER-LE-STREET",
		"CHICHESTER",
		"CHRISTCHURCH",
		"EDINBURGH",
		"YORK",
		"CLACKMANNANSHIRE",
		"COLCHESTER",
		"CONGLETON",
		"CONWY",
		"COPELAND",
		"CORBY",
		"COTSWOLD",
		"CRAVEN",
		"CREWE & NANTWICH",
		"DACORUM",
		"DENBIGSHIRE",
		"DERBYSHIRE DALES",
		"DERWENTSIDE",
		"DONCASTER",
		"DOVER",
		"DUMFRIES & GALLOWAY",
		"DUNDEE",
		"EAST AYRSHIRE (KILMARNOCK)",
		"EAST CAMBRIDGESHIRE",
		"EAST DEVON",
		"EAST DORSET",
		"EAST DUNBARTONSHIRE (KIRKINTILLOCH)",
		"EAST HAMPSHIRE",
		"EAST HERTS",
		"EAST LINDSEY",
		"EAST LOTHIAN (HADDINGTON)",
		"EAST NORTHANTS",
		"EAST RENFREWSHIRE (GIFFNOCH)",
		"EAST RIDING",
		"EAST STAFFORDSHIRE",
		"EASTBOURNE",
		"EASTLEIGH",
		"EDEN",
		"EPPING FOREST",
		"EREWASH",
		"EXETER",
		"FALKIRK",
		"FAREHAM",
		"FENLAND",
		"FIFE",
		"FLINTSHIRE",
		"FOREST HEATH",
		"FOREST OF DEAN",
		"GLASGOW",
		"GLOUCESTER",
		"GOSPORT",
		"GREAT YARMOUTH",
		"GWYNEDD",
		"HAMBLETON",
		"HARLOW",
		"HARROGATE",
		"HART",
		"HAVANT",
		"HEREFORD",
		"INVERNESS (HIGHLANDS)",
		"HUNTINGDONSHIRE",
		"INVERCLYDE (GREENOCK)",
		"IPSWICH",
		"ISLE OF ANGLESEY",
		"ISLE OF MAN",
		"ISLE OF SCILLY",
		"ISLE OF WIGHT",
		"KENNET",
		"KERRIER",
		"KINGS LYNN & W NORFOLK",
		"LEWES",
		"LINCOLN",
		"MALDON",
		"MALVERN HILLS",
		"MELTON",
		"MENDIP",
		"MID DEVON",
		"MID SUFFOLK",
		"MID SUSSEX",
		"MIDLOTHIAN",
		"MONMOUTHSHIRE",
		"MORAY",
		"NEW FOREST",
		"NORTH AYRSHIRE",
		"NORTH CORNWALL",
		"NORTH DEVON",
		"NORTH DORSET",
		"NORTH EAST DERBYSHIRE",
		"NORTH EAST LINCOLNSHIRE",
		"NORTH HERTS",
		"NORTH LANARKSHIRE (NORTH)",
		"NORTH LANARKSHIRE(SOUTH/CENTRAL)",
		"NORTH LICOLNSHIRE",
		"NORTH NORFOLK",
		"NORTH SHROPSHIRE",
		"NORTH SOMERSET",
		"NORTH WILTSHIRE",
		"NORTHAMPTON",
		"NORWICH",
		"OADBY & WIGSTON",
		"ORKNEY ISLANDS (KIRKWALL)",
		"OSWESTRY",
		"OXFORD",
		"PEMBROKE",
		"PENWITH",
		"PERTH & KINROSS",
		"PETERBOROUGH",
		"PLYMOUTH",
		"POOLE",
		"POWYS",
		"PURBECK",
		"REDDITCH",
		"RENFREWSHIRE (PAISLEY)",
		"RESTORMEL",
		"RICHMONDSHIRE",
		"ROTHER",
		"RUTLAND",
		"RYEDALE",
		"SALISBURY",
		"SCARBOROUGH",
		"SCOTTISH BORDERS",
		"SELBY",
		"SEVENOAKS",
		"SHEPWAY",
		"SHETLAND ISLANDS (LERWICK)",
		"SHREWSBURY",
		"SOUTH AYRSHIRE (AYR)",
		"SOUTH BEDFORDSHIRE",
		"SOUTH BUCKINGHAM",
		"SOUTH CAMBRIDGE",
		"SOUTH DERBYSHIRE",
		"SOUTH GLOUCESTER",
		"SOUTH HAMS",
		"SOUTH HEREFORDSHIRE",
		"SOUTH HOLLAND",
		"SOUTH LAKELAND",
		"SOUTH LANARKSHIRE",
		"SOUTH NORFOLK",
		"SOUTH NORTHANTS",
		"SOUTH OXFORDSHIRE",
		"SOUTH SHROPSHIRE",
		"SOUTH SOMERSET",
		"SOUTH STAFFORDSHIRE",
		"SOUTHAMPTON",
		"ST EDMUNDSBURY",
		"STAFFORD",
		"STAFFORDSHIRE COUNTY",
		"STIRLING",
		"STRATFORD ON AVON",
		"STROUD",
		"SUFFOLK COASTAL",
		"SWALE",
		"SWINDON",
		"TEIGNBRIDGE",
		"TENDRING",
		"THANET DISTRICT",
		"TORBAY",
		"TORFAEN",
		"TORRIDGE",
		"UTTLESFORD",
		"VALE OF GLAMORGAN",
		"VALE OF WHITE HORSE",
		"VALE ROYAL",
		"WANSBECK",
		"WAVENEY",
		"WAVERLEY",
		"WEALDEN",
		"WEAR VALLEY",
		"WEST BERKSHIRE",
		"WEST DEVON",
		"WEST DORSET",
		"WEST DUNBARTONSHIRE (CLYDEBANK)",
		"WEST LINDSEY",
		"WEST LOTHIAN (LIVINGSTON)",
		"WEST OXFORD",
		"WEST SOMERSET",
		"WEST WILTSHIRE",
		"WESTERN ISLES (STORNOWAY)",
		"WEYMOUTH & PORTLAND",
		"WORTHING",
		"WREKIN",
		"WREXHAM",
		"WYRE FORREST",

   );

   if(! in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
      $error            =  true;
   }


   //cannot quote because lead is form sms reminder - skip leads sent from 13 server
   if($_SESSION["_QZ_QUOTE_DETAILS_"]["SERVER_IP"] == "192.168.12.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   //cannot quote because lead is form sms reminder - skip leads sent from 13 server
   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }


	//SKIP if "Full UK licence" = "1 - 2 Years"
   //                            "2 - 3 Years"
   //                            "3 - 4 Years"
   $periodOfFullUkLicenceHeld = $_SESSION["_YourDetails_"]["period_of_licence"];
   $filteredPeriodOfFullUkLicenceHeldArray = array(
       "3" => "1 - 2 Years",
       "4" => "2 - 3 Years",
       "5" => "3 - 4 Years",
      );

   if(array_key_exists($periodOfFullUkLicenceHeld,$filteredPeriodOfFullUkLicenceHeldArray))
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--SKP_LIST [".$filteredPeriodOfFullUkLicenceHeldArray[$periodOfFullUkLicenceHeld]."]";
      $error            =  true;
   }

   
	// SKIP if "Taxi badge" = "No Badge"
	//                        "Under 6 Months"
	//                        "6 Months To 1 Year"
	//                        "1 - 2 Years"
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 5)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_2_3_YEARS";
      $error               = true;
   }

   $vehicleManufYear = $_SESSION["_YourDetails_"]["year_of_manufacture"];
   $disYear          = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"];

   $vehicleAge = $disYear - $vehicleManufYear;

   if($vehicleAge > 10)
   {
      $personalizedError[] = "SKIP_RULE_VEH_AGE--AGE_SKP_OVR_10";
      $error            =  true;
   }

	// SKIP if "Age" < 25
	//               > 69
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25 || $propAge > 69)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25_AND_OVR_69";
      $error               =  true;
   }

 /*
      ACCEPT leads Monday 08:00 - 17:00
                  Tuesday 08:00 - 17:00
                  Wednesday 08:00 - 17:00
                  Thursday 08:00 - 17:00
                  Friday 08:00 - 17:00
                  Saturday NO LEADS
                  Sunday NO LEADS
               
   */
   
   

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {
         case '1':
         case '2':
         case '3':
         case '4':
         case '5':
         if($timeHh < 8 || $timeHh >= 17)
         {
            $personalizedError[] = "SKIP_RULE_SKP_DAY_MON_FRI--TIME_ACC_BETW_8:00AM_TO_5:00PM";
            $error               =  true;
         }
         break;
         case '6': 
         case '7':
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
            $error               =  true;
         break;
      }
   }




   return $error;

?>

<?php

   $error = false;

 //3.Only Fully Comprehensive
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover != 1)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_ACC_FULL_COMPR";
      $error                       =  true;
   }

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 28)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_28";
      $error                       =  true;
   }

   //Skip if Taxi Use = Private Hire
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse == 1)
   {
      $personalizedError[] = "SKIP_RULE_USE--USE_SKP_PRIV_HIRE";
      $error               =  true;
   }

   //1.Skip if number of passengers > 7
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity > '7')
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_7";
      $error                       =  true;
   }

   //Skip if Year Of Manufacture < 2004
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   if($yearOfManufacture < 2004)
   {
      $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--YEAR_SKP_UND_2004";
      $error                       =  true;
   }

   //4. Skip if Full UK licence < 5 years.
   $period_of_licence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($period_of_licence < 7)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_5_6_YEARS";
      $error                       =  true;
   }

   //Skip all BT Postcodes = BT[1-99]*
   $postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BT",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }




   $licesingAuthority           = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $acceptedLicensingAuthorityArray = array(
"BIRMINGHAM",
"BRACKNELL FOREST",
"BRISTOL CITY OF UA",
"BROXTOWE",
"CALDERDALE",
"CANNOCK CHASE",
"CHELMSFORD",
"COVENTRY",
"DERBY",
"ELMBRIDGE",
"EPSOM & EWELL",
"KINGSTON-UPON-HULL",
"NEWCASTLE UPON TYNE",
"NEWCASTLE UNDER LYME",
"NORTH TYNESIDE",
"NOTTINGHAM",
"PETERBOROUGH",
"REDCAR & CLEVELAND",
"VALE ROYAL",
"SANDWELL",
"SHEFFIELD",
"SOUTH TYNESIDE",
"STEVENAGE",
"STOCKPORT",
"TAMESIDE",
"WALSALL",
"WOLVERHAMPTON",
"BARKING",
"BARNET",
"BEDFORD",
"BEXLEY",
"BLACKBURN WITH DARWEN",
"BLACKPOOL",
"BOLTON",
"BRADFORD",
"BRENTWOOD",
"BROMLEY",
"BURY",
"CAMDEN",
"CROYDEN",
"DARTFORD",
"EALING",
"ENFIELD",
"GREENWICH",
"HACKNEY",
"HAMMERSMITH",
"HARINGEY",
"HARROGATE",
"HAVERING",
"HILLINGDON",
"HOUNSLOW",
"ISLINGTON",
"KENSINGTON",
"KIRKLEES",
"KNOWSLEY",
"LAMBETH",
"LEEDS",
"LEWISHAM",
"LIVERPOOL",
"LONDON PCO",
"LUTON",
"MANCHESTER",
"MERTON",
"NEWHAM",
"OLDHAM",
"PRESTON",
"READING",
"REDBRIDGE",
"RICHMOND",
"ROCHDALE",
"SALFORD",
"SEFTON",
"SLOUGH",
"SOLIHULL",
"SOUTHWARK",
"ST HELENS",
"SUTTON",
"THREE RIVERS",
"TOWER HAMLETS",
"TRAFFORD",
"WAKEFIELD",
"WANDSWORTH",
"WATFORD",
"WESTMINSTER",
"WIGAN",
"WIRRALL",
   );

   if(! in_array($licesingAuthority,$acceptedLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
      $error               =  true;
   }
  

   return $error;
?>

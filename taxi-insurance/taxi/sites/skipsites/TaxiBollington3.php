<?php

   $error = false;

   //skip if Taxi Badge = No Badge, under 6 Months, 6 Months to 1 Year, 1-2 Years, 2-3 Years, 3-4 Years
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 7)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_4_5_YEARS";
      $error               = true;
   }

   //skip if driver age < 30
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 40)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_40";
      $error               =  true;
   }


   //Skip if Full UK Licence < 9-10 years
   $period_of_licence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($period_of_licence < 11)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_9_10_YEARS";
      $error                       =  true;
   }


   //skip if vehicle age > 5 years
   $vehicleManufYear = $_SESSION["_YourDetails_"]["year_of_manufacture"];
   $disYear          = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"];

   $vehicleAge = $disYear - $vehicleManufYear;

   if($vehicleAge > 5)
   {
      $personalizedError[] = "SKIP_RULE_VEH_AGE--AGE_SKP_OVR_5";
      $error               =  true;
   }


   //Skip if Max. Number of Passengers > 8
   $taxiType     = $_SESSION['_YourDetails_']['taxi_type'];
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiType != 3)//MPV
   {
      if($taxiCapacity == '8')
      {
         $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_8";
         $error                       =  true;
      }
   }
   else
   {
      if($taxiCapacity > '8')
      {
         $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_8";
         $error                       =  true;
      }
   }


   // SKIP if "Taxi No Claims Bonus" = No NCB, 1 Year, 2 Years, 3 Years
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB < 4)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_4_YEARS";
      $error                       =  true;
   }


   //Skip if claims in the last 5 years = Yes
   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
   if($claim5Years == "Yes")
   {
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_YES";
      $error               = true;
   }

   //Skip if convictions last 5 years = Yes
   $conv5Years = $_SESSION['_YourDetails_']['convictions_5_years'];
   if($conv5Years == "Yes")
   {
      $personalizedError[] = "SKIP_RULE_CONV_LAST_5_YEARS--SKP_YES";
      $error                       =  true;
   }



   // only accept attached Taxi Plating Authorities 
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $accLicensingAuthorityArray = array(
"BLACKBURN WITH DARWEN",
"BOLTON",
"BRADFORD",
"BURY",
"BURNLEY",
"CALDERDALE",
"ELLESMERE PORT",
"HALTON",
"KIRKLEES",
"KNOWSLEY",
"LEEDS",
"LIVERPOOL",
"LONDON PCO",
"MANCHESTER",
"OLDHAM",
"ROCHDALE",
"ROSSENDALE",
"SALFORD",
"SEFTON",
"SHEFFIELD",
"STOCKPORT",
"ST HELENS",
"SUNDERLAND",
"TRAFFORD",
"TAMESIDE",
"WARRINGTON",
"WIGAN",
   );

   if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
     $error             =  true;
   }


   //SKIP if "Age" > 68
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge > 68)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_OVR_68";
      $error               =  true;
   }



   // Accept leads from Monday over 9am until Friday under 5pm
   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {
         case '6': // Sat
         case '7': // Sun
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
            $error                           =  true;
            break;

         case '1': // Mon
            if($timeHh < 9)
            {
               $personalizedError[] = "SKIP_RULE_DAY_MON--TIME_ACC_MON_OVR_9:00AM_FRI_SKP_OVR_17:00_SKP_SAT_SUN";
               $error                               =  true;
            }
         break;
         case '2': // Tue
         break;
         case '3': // Wed
         break;
         case '4': // Thu
         break;
         case '5': // Fri
            if($timeHh >= 17)
            {
               $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_MON_OVR_9:00AM_FRI_SKP_OVR_17:00_SKP_SAT_SUN";
               $error                               =  true;
            }
         break;
      }
   }

   return $error;

?>

<?php

   $error = false;

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   //  1. Skip if user age < 35 years and age > 69 years
   if($propAge < 35)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_35";
      $error 			   =  true;
   }

   if($propAge > 69)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_OVR_69";
      $error 			   =  true;
   }

   // Skip if Maximum Number of Passengers > 8
   $taxiType     = $_SESSION['_YourDetails_']['taxi_type'];
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   
   if($taxiType == 3)
   {
      if($taxiCapacity > 8)
      {
         $personalizedError[] = "SKIP_RULE_VEH_TYP_NO_PASS--TYP_MPV_SKP_OVR_8";
         $error =  true;
      }
   }
   else
   {
      if($taxiCapacity == 8)
      {
         $personalizedError[] = "SKIP_RULE_VEH_TYP_NO_PASS--TYP_N_MPV_SKP_8";
         $error 			  =  true;
      }
   }

   // Skip if Claims Last 5 Years = Yes
   $hadClaims = $_SESSION['_YourDetails_']['claims_5_years'];

   if($hadClaims == "Yes")
   {
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_YES";
      $error 			   =  true;
   }

   // Skip if Taxi No Claims Bonus < 3 years
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB < 3)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_3_YEARS";
      $error 			   =  true;
   }

   // Skip if Taxi Badge < 5 years
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];

   if($taxiBadge < 8)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_5_6_YEARS";
      $error 			   =  true;
   }

   // Skip if Full UK Licence < 5 years 
   $licYears = $_SESSION['_YourDetails_']['period_of_licence'];

   if($licYears < 7)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_5_6_YEARS";
      $error 			   =  true;
   }

   //    Only accept leads from the following Taxi plating authorities:
// "ASHFORD",
// "HASTINGS",
// "ROTHER",
// "WEALDEN",
// "EASTBOURNE",
// "LEWES",
// "TUNBRIDGE WELLS",
// "BRIGHTON & HOVE",
   $acceptedPlatingAuthiritiesArray = array(
"ASHFORD",
"HASTINGS",
"ROTHER",
"WEALDEN",
"EASTBOURNE",
"LEWES",
"TUNBRIDGE WELLS",
"BRIGHTON & HOVE",
                                       );

   $platingAuthority = $_SESSION['_YourDetails_']['plating_authority'];
   $platingAuthority = strtolower($platingAuthority);
   $platingAuthority = ucwords($platingAuthority);

   if(! in_array($platingAuthority,$acceptedPlatingAuthiritiesArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$platingAuthority]";
	  $error 			   =  true;
   }




   return $error;

?>

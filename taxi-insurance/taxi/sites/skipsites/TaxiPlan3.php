<?php

   $error = false;

   $wlID = $_SESSION['_QZ_QUOTE_DETAILS_']['wlUserID'];
   if($wlID == "758")
   {
      $personalizedError[] = "SKIP_RULE_COMPANY_WL--WL_SKP_DNAINSURANCE2";
      $error               =  true;
   }

   $postCode        = $_SESSION['_YourDetails_']['postcode'];

   
   //1. Skip if Type of Cover = Third Party or Third Party Fire & Theft
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover == 2 || $typeOfCover == 3)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_SKP_TPO_TPFT";
      $error                       =  true;
   }

   //3. Skip if Full UK licence < 5 years
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence <= 6)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_5_YEARS";
      $error               = true;
   }

   //4. SKIP if "Taxi badge" = "No Badge"
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge == 1)
   {
      $personalizedError[] = "SKIP_RULE_BADG--BADG_SKP_N_BADG";
      $error               = true;
   }

   /// THIS FILTER SHOULD WORK ONLY FOR OUR IP 
//   if($_SESSION["USER_IP"] == "86.125.114.56")
//   {
      $taxiType = $_SESSION['_YourDetails_']['taxi_type'];
      if($taxiType == 1)
      {
         $personalizedError[] = "SKIP_RULE_VEH_TYP--TYP_SKP_BLACK_CAB";
         $error            =  true;
      }
//   }

   
   /*ACCEPT leads Monday 09:00 - 12:30 and 14:00 - 17:30
                        Tuesday 09:00 - 12:30 and 14:00 - 17:30
                   Wednesday 09:00 - 12:30 and 14:00 - 17:30
                       Thursday 09:00 - 12:30 and 14:00 - 17:30
                            Friday 09:00 - 12:30 and 14:00 - 17:30
                        Saturday NO LEADS
                          Sunday NO LEADS*/

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      // Motor Trade Plan accept leads only between 9:00 and 17:30 Monday – Thursday and Fri between 9:00 and 17:30.
      $timeHh     = date("G");
      $timeMm     = date("i");
      $dateDdText = date("N");

      switch($dateDdText)
      {
         case '1': // Mon
         case '2': // Tue
         case '3': // Wed
         case '4': // Thu
         case '5': // Fri
           if($timeHh < 9 || $timeHh > 17 || $timeHh == 13 || ($timeHh == 12 && $timeMm >= 30) || ($timeHh == 17 && $timeMm >= 30))
            {
               $personalizedError[] = "SKIP_RULE_DAY_MON_THU--TIME_ACC_BETW_9:00_12:30_AND_14:00_17:30PM";
               $error               =  true;
            }
            break;

         case '6': // Sat
               $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT--TIME_SKP_ALL_DAY";
               $error               =  true;

            break;

         case '7':  // Sun
               $personalizedError[] = "SKIP_RULE_SKP_DAY_SUN--TIME_SKP_ALL_DAY";
               $error               =  true;

            break;
      }
   }
   
   
   //SKIP if "Age" < 28   > 70                      
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);
   

   if($propAge < 28 || $propAge > 70)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_BETW_28_AND_70";
      $error            =  true;
   }

   
   // SKIP if "Year of manufacture" < 2003   (E.g. SKIP if 2002, 2001, 2000, .... 1980, etc)
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   if($yearOfManufacture < 2003)
   {
      $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--YEAR_SKP_UND_2003";
      $error            =  true;
   }
   
   
   // SKIP if "Estimated taxi value (£)" < 8,000 > 85,000
   $estimated_value = $_SESSION['_YourDetails_']['estimated_value'];
   if($estimated_value > 85000)
   {
      $personalizedError[] = "SKIP_RULE_VEH_VALUE--SKP_LIST ['over 85000']";
      $error               = true;
   }
   if($estimated_value < 8000)
   {
      $personalizedError[] = "SKIP_RULE_VEH_VALUE--SKP_LIST ['under 8000']";
      $error               = true;
   }
   
   
   // SKIP if "Taxi use" = "Public Hire"
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse == 2)
   {
      $personalizedError[] = "SKIP_RULE_USE_PCODES--USE_SKP_PUB_HIRE";
      $error          =  true;
   }


   // SKIP if "Taxi driver(s)" = "Insured and 2+ Named Drivers"
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers == 3)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_SKP_INS_AND_2+_NAM_DRV";
      $error               =  true;
   }
   
   
   // SKIP if "Max. number of passengers" > 7
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity > '7')
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_7";
      $error            =  true;
   }
   
   
   // SKIP if "Postcode"
   $postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "B",
      "BB",
      "BD",
      "BL",
      "BT",
      "CH",
      "E",
      "HX",
      "LS",
      "LU",
      "M",
      "OL",
      "PR",
      "WD",
      "WN",
      "L",      
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }
   

   return $error;

?>

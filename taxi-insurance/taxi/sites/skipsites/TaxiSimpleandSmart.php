<?php

   $error = false;
   
   
   // skip user age < 25
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error            =  true;
   }
   
   
   // skip if taxi no claims bonus = No NCB, 1 Year
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB < 2)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB_1YEAR";
      $error                       =  true;
   }
   
   
   /*time filter
   Monday - Accept ALL DAY
   Tuesday - Accept ALL DAY
   Wednesday - Accept ALL DAY
   Thursday - Accept ALL DAY
   Friday - Skip hours over 5pm
   Saturday - Skip all day
   Sunday - Skip all day*/
   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {

      $timeHh     = date("G");
      $timeMm     = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {
         case '1': // Mon
         case '2': // Tue
         case '3': // Wed
         case '4': // Thu
            break;
            
         case '5': // Fri
            if($timeHh >= 17)
            {
               $personalizedError[] = "SKIP_RULE_DAY_FRI--TIME_SKP_OVR_5:00PM";
               $error               =  true;
            }            
            break; 
            
         case '6': // Sat            
            {
               $personalizedError[] = "SKIP_RULE_DAY_SAT--TIME_SKP_ALL_DAY";
               $error               =  true;
            }            
            break; 

         case '7': // Sun
            {
               $personalizedError[] = "SKIP_RULE_DAY_SUN--TIME_SKP_ALL_DAY";
               $error               =  true;
            }
            break;
      }
   }
   

   // skip if postcode prefix = from attached list
   $postCode        = $_SESSION['_YourDetails_']['postcode'];  
   $postCodePreffix = $_SESSION['_YourDetails_']['postcode_prefix']; 
   $postCodeSuffix  = $_SESSION['_YourDetails_']['postcode_number'];    
      
      
   $filter          = "skip"; // skip or accept   
   $postCodeArray   = array(  
      "BT[0-99]*",
      "S[0-99]*",
      "BD[0-99]*",
      "PR[0-99]*",
      "BL[0-99]*",
      "TF[0-99]*",
      "B10",
      "B9",
      "B8",
      "B12",
      "B20",
      "B6",
      "B11",
      "BR1",
      "BR2",
      "BR3",
      "BR4",
      "BR5",
      "BR6",
      "BR7",
      "CR0",
      "CR1",
      "CR2",
      "CR4",
      "CR5",
      "CR8",
      "DA1",
      "DA14",
      "DA15",
      "DA16",
      "DA17",
      "DA5",
      "DA6",
      "DA8",
      "E1",
      "E10",
      "E11",
      "E12",
      "E13",
      "E14",
      "E15",
      "E16",
      "E17",
      "E18",
      "E2",
      "E3",
      "E4",
      "E5",
      "E6",
      "E7",
      "E8",
      "E9",
      "EC1",
      "EC2",
      "EC3",
      "EC3R",
      "EC4",
      "EN1",
      "EN2",
      "EN3",
      "EN4",
      "EN5",
      "HA0",
      "HA1",
      "HA2",
      "HA3",
      "HA4",
      "HA5",
      "HA6",
      "HA7",
      "HA8",
      "HA9",
      "IG1",
      "IG11",
      "IG2",
      "IG3",
      "IG6",
      "IG8",
      "KT1",
      "KT2",
      "KT3",
      "KT4",
      "KT6",
      "KT9",
      "N1",
      "N10",
      "N11",
      "N12",
      "N13",
      "N14",
      "N15",
      "N16",
      "N17",
      "N18",
      "N19",
      "N2",
      "N20",
      "N21",
      "N22",
      "N3",
      "N4",
      "N5",
      "N6",
      "N7",
      "N8",
      "N9",
      "NW1",
      "NW10",
      "NW11",
      "NW2",
      "NW3",
      "NW4",
      "NW5",
      "NW6",
      "NW7",
      "NW8",
      "NW9",
      "RM1",
      "RM10",
      "RM12",
      "RM13",
      "RM14",
      "RM2",
      "RM3",
      "RM4",
      "RM5",
      "RM6",
      "RM7",
      "RM8",
      "RM9",
      "SE1",
      "SE10",
      "SE11",
      "SE12",
      "SE13",
      "SE14",
      "SE15",
      "SE16",
      "SE17",
      "SE18",
      "SE19",
      "SE2",
      "SE20",
      "SE21",
      "SE22",
      "SE23",
      "SE24",
      "SE25",
      "SE26",
      "SE27",
      "SE28",
      "SE3",
      "SE4",
      "SE5",
      "SE6",
      "SE7",
      "SE8",
      "SE9",
      "SM2",
      "SM3",
      "SM4",
      "SM5",
      "SM6",
      "SM7",
      "SW1",
      "SW10",
      "SW11",
      "SW12",
      "SW13",
      "SW14",
      "SW15",
      "SW16",
      "SW17",
      "SW18",
      "SW19",
      "SW1A",
      "SW2",
      "SW20",
      "SW3",
      "SW4",
      "SW5",
      "SW6",
      "SW7",
      "SW8",
      "SW9",
      "TN14",
      "TN16",
      "TW1",
      "TW10",
      "TW11",
      "TW12",
      "TW13",
      "TW14",
      "TW3",
      "TW4",
      "TW5",
      "TW7",
      "TW8",
      "TW9",
      "UB1",
      "UB10",
      "UB2",
      "UB3",
      "UB4",
      "UB5",
      "UB6",
      "UB7",
      "UB8",
      "UB9",
      "W1",
      "W10",
      "W11",
      "W12",
      "W13",
      "W14",
      "W1M",
      "W2",
      "W3",
      "W4",
      "W5",
      "W6",
      "W7",
      "W8",
      "W9",
      "WC1",
      "WC2",  
         ); 
   
   $filterStatus = "";     
   
   preg_match("/(.*)\d/isU",$postCodePreffix,$pregLettersToCheck);   
   $lettersPostCodePreffix = $pregLettersToCheck[1];  
   
   preg_match("/[0-9]+/",$postCodePreffix,$pregNumbersToCheck);   
   $numbersPostCodePreffix = $pregNumbersToCheck[0];  
   
   $prefLettAndNum = $lettersPostCodePreffix.$numbersPostCodePreffix;   
   
   foreach($postCodeArray as $keypostCodeArray=>$skpPostcode)  
   {  
      if((preg_match("/[A-Z]*/",$skpPostcode,$pregMatchLetters)) && (preg_match("/\[(.*)\-/",$skpPostcode,$pregMinInterval)) && (preg_match("/\-(.*)\]/",$skpPostcode,$pregMaxInterval))) 
      {  
         $prefixLettersArr = $pregMatchLetters[0]; 
         $minIntervalArr   = $pregMinInterval[1];  
         $maxIntervalArr   = $pregMaxInterval[1];  
      }  
      else  
      {  
         $prefixLettersArr = ""; 
         $minIntervalArr   = ""; 
         $maxIntervalArr   = ""; 
      }  
         
         
      //checking postcodes like BT[1-99]* 
      if((preg_match("/\*/isU",$skpPostcode,$pregMatchAll)))   
      {  
         if(($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr))) 
         {  
            //"postcode has been accepted \n"; //only accept list 
            $filterStatus = true;   
            break;   
         }  
      }  
   
      //checking postcodes like BT[1-99]  
      elseif(preg_match("/^[A-Z]{1,2}\[[0-9]{1,2}-[0-9]{1,2}\]$/isU",$skpPostcode,$pregMatchAll))  
      {  
         if(($postCodePreffix == $prefLettAndNum) && ($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))  
         {              
            $filterStatus = true;   
            break;   
         }  
      }  
         
      //checking postcodes like BT3 9  
      elseif((preg_match("/(.*)\s+/isU",$skpPostcode, $pregMatchSpace)))   
      {        
      $sufixNumber = substr($postCodeSuffix,0,-2); 
      $postCodeToCheck = $postCodePreffix." ".$sufixNumber; 
            
         if($skpPostcode == $postCodeToCheck)   
         {  
            $filterStatus = true;   
            break;   
         }              
      }  
   
      //checking postcodes like BT339AA OR BT39AA  
      elseif(ctype_alpha(substr($skpPostcode,-2))) 
      {  
      $postCodeStrip = str_replace(" ","",$postCode); 
   
         if($skpPostcode == $postCodeStrip)  
         {  
            $filterStatus = true;   
            break;   
         }              
      }  
   
      //checking postcodes preffix like BT1, BT1A  
      elseif((preg_match("/^$postCodePreffix$/",$skpPostcode)))   
      {  
         if($postCodePreffix == $skpPostcode);  
         {  
            $filterStatus = true;   
            break;   
         }  
      }  
   }  
   
   if($filter == "accept") 
   {  
      if($filterStatus == false) 
      {  
         $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]\n"; 
         $error               =  true; 
      }  
   }  
   elseif($filter == "skip")  
   {  
      if($filterStatus==true) 
      {  
         $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";   
         $error               =  true; 
      }  
   }  // end of Postcode Filter
   
   
   
   return $error;
?>

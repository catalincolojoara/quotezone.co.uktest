<?php

   $error = false;


   //ACCEPT ONLY if "Max Number of Passengers = 4"
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity != '4')
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_ACC_4";
      $error               =  true;
   }

   //SKIP if "Type of Cover = Third Party"
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover == 3)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_SKP_TP";
      $error                       =  true;
   }
 
   //SKIP if "Full UK Licence < 5 Years"
   $period_of_licence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($period_of_licence < 7)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_5_6_YEARS";
      $error                       =  true;
   }

   //SKIP if "Age < 25" "Age > 70"
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25 || $propAge > 70)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_BETW_25_AND_70";
      $error                       =  true;
   }
           
   //SKIP if "Taxi Badge = No Badge"
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge == 1 )
   {
      $personalizedError[] = "SKIP_RULE_BADG--BADG_SKP_N_BADG";
      $error                       =  true;
   }

   //SKIP if "Claims in Last 5 Years = Yes, Claims Made"
   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
   if($claim5Years == "Yes")
   {
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_YES";
      $error                       =  true;
   }
 
   //SKIP the attached TAXI POSTCODES
   $postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "E",
      "L",
      "M",
      "OL",
      "BD",
      "LU",
      "BL",
      "SK",
      "WA",
      "BB",
      "BT",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }


   //ACCEPT leads Monday - Friday 10:30 - 16:30
   $timeMin    = date("i");
   $timeHh     = date("G");
   $dateDdText = date("N");

   if($dateDdText > 5 || $timeHh < 10 || $timeHh > 16 || ($timeHh == 10 && $timeMin < 30) || ($timeHh == 16 && $timeMin >= 30))
   {
      $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_10:30AM_TO_4:30PM";
      $error                       =  true;
   }

   // Skip if Taxi No Claims Bonus = 4 years or 5+ years
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB == 4)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_4_YEARS";
      $error               =  true;
   }

   if($taxiNCB == 5)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_5+_YEARS";
      $error               =  true;
   }

   return $error;

?>

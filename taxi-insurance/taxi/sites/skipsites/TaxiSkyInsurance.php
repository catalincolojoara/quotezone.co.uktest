<?php

   $error = false;
   // ACCEPT if "Taxi Plating Authority" = HERTSMERE

   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $platingAuthoritiesArray = array (
   "HERTSMERE",
   );
   
   if(! in_array($licesingAuthority,$platingAuthoritiesArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error               =  true;
   }

   return $error;
?>

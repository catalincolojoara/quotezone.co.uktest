<?php

   $error = false;

  /*
      ACCEPT if "Taxi plating authority" = Kingston-Upon-Hull
                                           East Riding
  */



      $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

      $accLicensingAuthorityArray = array(
         "KINGSTON-UPON-HULL",
         "EAST RIDING",
      );

      if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
      {
         $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
         $error 			   =  true;
      }


   return $error;

?>
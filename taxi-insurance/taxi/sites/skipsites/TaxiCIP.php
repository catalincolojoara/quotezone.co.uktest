<?php

   $error = false;

   // 1. CIP Insurance will receive leads only for this postcodes.
   $postCode       = $_SESSION['_YourDetails_']['postcode'];
   $postCodePrefix = $_SESSION['_YourDetails_']['postcode_prefix'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
      "BT", 
   );

   if(! in_array($postcodeToCheck,$acceptedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error 			   =  true;
   }   

   $skipPcodesPrefixArray = array(
      "BT47",
      "BT48",
   );

   if(in_array($postCodePrefix,$skipPcodesPrefixArray))
   {
      $personalizedError[] = "SKIP_RULE_POST_PREF--SKP_LIST [$postCodePrefix]";
      $error 			   =  true;
   }

   // 2. CIP Insurance will not receive leads for year of manufacture less then 2000
   $yearOfManufacture       = $_SESSION['_YourDetails_']['year_of_manufacture'];

   if($yearOfManufacture < 2000)
   {
      $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--YEAR_SKP_UND_2000";
      $error 			   =  true;
   }

   // 3. CIP Insurance will not receive leads for taxi capacity over 9
   $taxiCapacity       = $_SESSION['_YourDetails_']['taxi_capacity'];

   if($taxiCapacity > 9)
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_9";
      $error 			   =  true;
   }

   // 4. CIP Insurance will not receive leads for taxi NCB under 2 years
   $taxiNCB       = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB < 2)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_2_YEARS";
      $error 			   =  true;
   }

   // 5. CIP Insurance will not receive leads for taxi with full UK Licence under 5 years
   $periodOfLicence       = $_SESSION['_YourDetails_']['period_of_licence'];

   if($periodOfLicence < 7)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_5_6_YEARS";
      $error 			   =  true;
   }

   // 6. CIP Insurance will not receive leads for taxi badge under 2 years
   $taxiBadge       = $_SESSION['_YourDetails_']['taxi_badge'];

   if($taxiBadge < 5)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_2_3_YEARS";
      $error 			   =  true;
   }

   // 7. CIP Insurance will not receive leads for proposer under 25 or over 65 years old
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25 || $propAge > 65)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_BETW_25_AND_65";
      $error 			   =  true;
   }

   return $error;

?>

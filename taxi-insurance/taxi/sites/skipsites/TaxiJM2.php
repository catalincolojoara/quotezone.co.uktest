<?php
 
   $error = false;

   // ACCEPT only if "Max. number of passengers" = 8
   //                                                                              9
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity < 8 || $taxiCapacity > 9)
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_UND_8_OVR_9";
      $error               =  true;
   }

   //ACCEPT only if "Taxi driver(s)" = "Insured Only"
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers != 1)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_ACC_INS_ONLY";
      $error                       =  true;
   }


	//SKIP if "Full UK licence" < "5 - 6 Years"
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence < 7)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_5_6_YEARS";
      $error 			   =  true;
   }

   //ACCEPT only if "Taxi plating authority" = see attached
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $acceptedLicensingAuthorityArray = array(
"ADUR",
"ALLERDALE",
"AMBER VALLEY",
"ARUN",
"ASHFIELD",
"ASHFORD",
"BURY",
"BABERGH",
"BARNSLEY",
"BARROW IN FURNESS",
"BASILDON",
"BASINGSTOKE & DEANE",
"BASSETLAW",
"BATH & N.E SOMERSET",
"BLABY",
"BLYTH VALLEY",
"BOLSOVER",
"BOSTON",
"BOURNEMOUTH",
"BRAINTREE",
"BRECKLAND",
"BRENTWOOD",
"BRIGHTON & HOVE",
"BROADLAND",
"BROMSGROVE",
"BROXBOURNE",
"CAMBRIDGE",
"CANTERBURY",
"CARLISLE",
"CARRICK",
"CASTLE POINT",
"CHARNWOOD",
"CHELTENHAM",
"CHERWELL",
"CHESTERFIELD",
"CHICHESTER",
"CHILTERN",
"CHORLEY",
"CHRISTCHURCH",
"COLCHESTER",
"COPELAND",
"CORBY",
"COTSWOLD",
"CRAVEN",
"CRAWLEY",
"DACORUM",
"DARLINGTON",
"DAVENTRY",
"DERBYSHIRE DALES",
"DONCASTER",
"DOVER",
"DURHAM",
"EAST CAMBRIDGESHIRE",
"EAST DEVON",
"EAST DORSET",
"EAST HAMPSHIRE",
"EAST HERTS",
"EAST LINDSEY",
"NORTHAMPTON",
"YORK",
"STAFFORD",
"EASTBOURNE",
"EASTLEIGH",
"EDEN",
"EPPING FOREST",
"EREWASH",
"EXETER",
"FAREHAM",
"FENLAND",
"FOREST HEATH",
"FOREST OF DEAN",
"FYLDE",
"GATESHEAD",
"GEDLING",
"GLOUCESTER",
"GLOUCESTERSHIRE COUNTY",
"GOSPORT",
"GRAVESHAM",
"GUILDFORD",
"HALTON",
"HAMBLETON",
"HARBOROUGH",
"HARLOW",
"HARROGATE",
"HART",
"HARTLEPOOL",
"HASTINGS",
"HAVANT",
"HEREFORD",
"HERTSMERE",
"HIGH PEAK",
"HINCKLEY & BOSWORTH",
"HORSHAM",
"HUNTINGDONSHIRE",
"HYNDBURN",
"IPSWICH",
"ISLE OF WIGHT",
"KETTERING",
"LANCASTER",
"LEICESTER",
"LEWES",
"LICHFIELD",
"LINCOLN",
"MAIDSTONE",
"MALDON",
"MALVERN HILLS",
"MANSFIELD",
"MEDWAY",
"MELTON",
"MENDIP",
"MID DEVON",
"MID SUFFOLK",
"MID SUSSEX",
"MIDDLESBOROUGH",
"MILTON KEYNES",
"MOLE VALLEY",
"NEW FOREST",
"NEWARK & SHERWOOD",
"NEWCASTLE UPON TYNE",
"NEWCASTLE UNDER LYME",
"NORTH DEVON",
"NORTH DORSET",
"NORTH EAST DERBYSHIRE",
"NORTH EAST LINCOLNSHIRE",
"NORTH NORFOLK",
"NORTH SHROPSHIRE",
"NORTH TYNESIDE",
"NORTHUMBERLAND",
"WARWICK",
"NORTH WEST LEICESTER",
"NORTHAMPTON",
"NORWICH",
"NUNEATON & BEDWORTH",
"OADBY & WIGSTON",
"OXFORD",
"PENDLE",
"PETERBOROUGH",
"PLYMOUTH",
"POOLE",
"PORTSMOUTH",
"PURBECK",
"REDCAR & CLEVELAND",
"REDDITCH",
"REIGATE AND BANSTEAD",
"RIBBLE VALLEY",
"RICHMONDSHIRE",
"ROCHFORD",
"ROSSENDALE",
"ROTHER",
"ROTHERHAM",
"RUGBY",
"RUNNYMEDE",
"RUSHCLIFFE",
"RUSHMOOR",
"RUTLAND",
"RYEDALE",
"SCARBOROUGH",
"SEDGEMOOR",
"SELBY",
"SEVENOAKS",
"SHEPWAY",
"SLOUGH",
"SOUTH BUCKINGHAM",
"SOUTH CAMBRIDGE",
"SOUTH DERBYSHIRE",
"SOUTH GLOUCESTER",
"SOUTH HAMS",
"SOUTH HOLLAND",
"SOUTH KESTEVEN",
"SOUTH LAKELAND",
"SOUTH NORFOLK",
"SOUTH NORTHANTS",
"SOUTH OXFORDSHIRE",
"SOUTH RIBBLE",
"SOUTH SOMERSET",
"STAFFORD",
"SOUTH TYNESIDE",
"SOUTHAMPTON",
"SOUTHEND-ON-SEA",
"SPELTHORNE",
"ST ALBANS",
"ST EDMUNDSBURY",
"STAFFORD",
"STAFFS MOORLANDS",
"STEVENAGE",
"STOCKTON ON TEES",
"STOKE ON TRENT",
"STRATFORD ON AVON",
"STROUD",
"SUFFOLK COASTAL",
"SUNDERLAND",
"SURREY HEATH",
"SWALE",
"SWINDON",
"TAMWORTH",
"TANDRIDGE",
"TAUNTON DEANE",
"TEIGNBRIDGE",
"TENDRING",
"TEST VALLEY",
"TEWKESBURY",
"THANET DISTRICT",
"THURROCK",
"TONBRIDGE & MALLING",
"TORBAY",
"TORRIDGE",
"TUNBRIDGE WELLS",
"UTTLESFORD",
"VALE OF WHITE HORSE",
"WANSBECK",
"WARRINGTON",
"WARWICK",
"WAVENEY",
"WAVERLEY",
"WEALDEN",
"WELLINGBOROUGH",
"WELWYN HATFIELD",
"WEST BERKSHIRE",
"WEST DEVON",
"WEST DORSET",
"WEST LANCASHIRE",
"WEST LINDSEY",
"WEST OXFORD",
"WEST SOMERSET",
"WEYMOUTH & PORTLAND",
"WINCHESTER",
"WOKING",
"WOKINGHAM",
"WORCESTER",
"WORTHING",
"WYCHAVON",
"WYCOMBE",
"WYRE FORREST",
"WYRE",
"YORK",
   );

   if(! in_array($licesingAuthority,$acceptedLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
          $error                           =  true;
   }


   /*ACCEPT leads Monday 06:00 - 23:59
                         Tuesday 06:00 - 23:59
                    Wednesday NO LEADS
                        Thursday 06:00 - 23:59
                             Friday 06:00 - 23:59
                         Saturday NO LEADS
                            Sunday NO  LEADS
  */
   
   
 

   $timeHh     = date("G");
   $dateDdNum  = date("N");
   $timeMin    = date("i");
  
   switch($dateDdNum)
   {
      case '1':
      case '2':
      if($timeHh < 6 )
         {
            $personalizedError[] = "SKIP_RULE_DAY_MON_TUE--TIME_SKP_UND_6:00AM";
            $error               =  true;
         }
         
      break;
      case '3':
         {
            $personalizedError[] = "SKIP_RULE_DAY_WED--TIME_SKP_ALL_DAY";
            $error               =  true;
         }
         
      break;
      case '4':
         if($timeHh < 6 )
         {
            $personalizedError[] = "SKIP_RULE_DAY_THU--TIME_SKP_UND_6:00AM";
            $error               =  true;
         }
         
      break;
      case '5':
         if($timeHh < 6)
         {
            $personalizedError[] = "SKIP_RULE_DAY_FRI--TIME_SKP_UND_6:00AM";
            $error               =  true;
         }
      break;
      case '6':
      case '7':
          $personalizedError[] = "SKIP_RULE_SKP_DAY_SUN_SAT--SKIP_RULE_SKP_DAY_SAT_SUN";
          $error          =  true;
      break;
   }

   
   return $error;

?>

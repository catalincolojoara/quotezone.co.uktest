<?php

   $error = false;
   
   //2. Advance will not receive leads at this time.
   $timeHh     = date("G");
   $timeMin    = date("i");
   $dateDdNum  = date("N");

   switch($dateDdNum)
   {
      case '6': // Sat
         if(($timeHh < 6) || ($timeHh > 12) || ($timeHh == 12 && $timeMin > 15))
         {
            $personalizedError[] = "SKIP_RULE_DAY_SAT--TIME_ACC_BETW_6:00AM_TO_12:15PM";
            $error 				 =  true;
         }
       
         break;

      case '7':  // Sun
         $personalizedError[] = "SKIP_RULE_SKP_DAY_SUN--TIME_SKP_ALL_DAY";
         $error 			  =  true;

         break;

      default: // Mon - Fri
         if($timeHh < 7 || $timeHh >= 19)
         {
            $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_7:00AM_TO_7:00PM";
            $error               = true;
         }
   }

   // skip if driver age < 35
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 35)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_35";
      $error               =  true;
   }

   // skip BT[1-99]* postcodes
   $postCode     = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skippedPcodesArray = array(
      "BT",
   );

   if(in_array($postcodeToCheck,$skippedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }

   // only accept Taxi No Claims Bonus = 5+ Years
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB != 5)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_ACC_5+_YEARS";
      $error               =  true;
   }

   // skip if Claims Last 5 Years = Yes, Claims Made
   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
   if($claim5Years == "Yes")
   { 
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_YES";
      $error            =  true;
   }

   // skip if Convictions last 5 Years  Yes
   $conv5Years = $_SESSION['_YourDetails_']['convictions_5_years'];
   if($conv5Years == "Yes")
   { 
      $personalizedError[] = "SKIP_RULE_CONV_LAST_5_YEARS--SKP_YES";
      $error            =  true;
   }

   return $error;
?>

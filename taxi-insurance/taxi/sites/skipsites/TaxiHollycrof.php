<?php

   $error = false;

//   1.	ACCEPT leads MONDAY - FRIDAY 0900 - 1700

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeMin    = date("i");
      $timeHh     = date("G");
      $dateDdText = date("N");

      switch($dateDdText)
      {
      	case '1':
      	case '2':
      	case '3':
      	case '4':
      	case '5':
	      if($timeHh < 9  || $timeHh >= 17)
	      {
	         $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_5:00PM";
	         $error               =  true;
	      }
	      break;
      	case '6':
      	case '7':
      	     $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
             $error               =  true;
          break;
      }
   }
   

//   SKIP if Postcode = BT[0-99]*

   $postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BT",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }

   
   return $error;
?>

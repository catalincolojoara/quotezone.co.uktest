<?php

//////////////////////////////////////////////////////////
/// INACTIVE Plugin. Please Go To ../config/1744_18.inc///
//////////////////////////////////////////////////////////

   $error = false;

   //SKIP if "Full UK licence" = "Under 6 Months", "6 Months To 1 Year"
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence <= 2)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_NO_BG_6_MONTH_ONE_YEAR";
      $error               = true;
   }

   //SKIP if "Taxi badge" = "No Badge"
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];

   if($taxiBadge == 1)
   {
      $personalizedError[] = "SKIP_RULE_BADG--BADG_SKP_N_BADG";
      $error                       =  true;
   }

   //Skip if user is less than 25 years old ; Skip if user is greater than than 70 years old
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 30 || $propAge > 70)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_30_AND_OVR_70";
      $error               =  true;
   }

   //Skip the follow list of plating authorities:
   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];
   $skipLicensingAuthorityArray = array(
"TAMESIDE",
"BARKING",
"BARNET",
"BEXLEY",
"BLACKBURN WITH DARWEN",
"BOLTON",
"BRADFORD",
"BRENT",
"BROMLEY",
"BURY",
"CAMDEN",
"CROYDEN",
"DARTFORD",
"EALING",
"LONDON PCO",
"GREENWICH",
"HACKNEY",
"HAMMERSMITH",
"HARINGEY",
"HILLINGDON",
"KIRKLEES",
"KNOWSLEY",
"LAMBETH",
"LEEDS",
"LEWISHAM",
"LIVERPOOL",
"LONDON PCO",
"MANCHESTER",
"MERTON",
"NEWHAM",
"OLDHAM",
"ROCHDALE",
"SALFORD",
"SEFTON",
"SOUTHWARK",
"ST HELENS",
"TOWER HAMLETS",
"TRAFFORD",
"WAKEFIELD",
"WANDSWORTH",
"WATFORD",
"WESTMINSTER",
"WIGAN",
   );

   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error                           =  true;
   }

   //Skip postcodes: BT[1-99]*
   $postCode = $_SESSION['_YourDetails_']['postcode'];
   $pcPrefix = $_SESSION['_YourDetails_']['postcode_prefix'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $pcPrefNumber = str_replace($postcodeToCheck,"",$pcPrefix);

   $skipPcodesArray = array(
      "BT",
      "BD",
      "BB",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }

   //skip pc prefix
   $skipPcPrefixArray = array(
      "OL" => "1-13",
      "B" => "1-30",
      "M" => "1-30",
      "L" => "1-30",
   );

   foreach($skipPcPrefixArray as $pcPrefixToSkip => $pcPrefixToSkipLimits)
   {

      $pcPrefixNumberLimitsExplode = explode("-",$pcPrefixToSkipLimits);

      $minLimit = $pcPrefixNumberLimitsExplode[0];
      $maxLimit = $pcPrefixNumberLimitsExplode[1];

      if($pcPrefixToSkip == $postcodeToCheck)
      {
         if(($pcPrefNumber >= $minLimit) AND ($pcPrefNumber <= $maxLimit))
         {
            $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
            $error                       =  true;
         }
      }
   }



   if($_SESSION['USER_IP'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMm     = date("i");
      $dateDdText = date("N");

      switch($dateDdText)
      {
         case '1':
         case '2':
         case '3':
         case '4':
         case '5':
         if($timeHh < 9 || $timeHh >= 19)
         {
            $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_7:00PM";
            $error               =  true;
         }
         break;

         case '6': // Sat
         if($timeHh < 9 || $timeHh >= 13)
         {
            $personalizedError[] = "SKIP_RULE_DAY_SAT--TIME_ACC_BETW_9:00AM_TO_1:00PM";
            $error               =  true;
         }
         break;

         case '7':
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SUN--TIME_SKP_ALL_DAY";
            $error               =  true;
         break;
      }
   }


   //filter in case of WL DNAINSURANCE1 leads
   $wlID = $_SESSION['_QZ_QUOTE_DETAILS_']['wlUserID'];
      if($wlID == "688")
      {
	  $personalizedError[] = "SKIP_RULE_COMPANY_WL--WL_SKP_DNAINSURANCE1";
	  $error               =  true;
      }


      if($wlID == "758")
      {
	  $personalizedError[] = "SKIP_RULE_COMPANY_WL--WL_SKP_DNAINSURANCE2";
	  $error               =  true;
      }

   return $error;

?>
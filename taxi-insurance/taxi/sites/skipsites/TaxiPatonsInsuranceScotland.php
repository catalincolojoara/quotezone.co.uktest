<?php

   $error = false;


   // 1.ACCEPT if "Taxi plating authority"
   $platingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   
   $acceptedPlatingAuthiritiesArray = array(
      "ABERDEEN",
      "ABERDEEN",
      "ABERDEENSHIRE",
      "ANGUS",
      "ARGYLE & BUTE",
      "CLACKMANNANSHIRE",
      "DUMFRIES & GALLOWAY",
      "DUNDEE",
      "EAST AYRSHIRE (KILMARNOCK)",
      "EAST DUNBARTONSHIRE (KIRKINTILLOCH)",
      "EAST LOTHIAN (HADDINGTON)",
      "EAST RENFREWSHIRE (GIFFNOCH)",
      "EDINBURGH",
      "GLASGOW",
      "INVERCLYDE (GREENOCK)",
      "FALKIRK",
      "FIFE",
      "FIFE",
      "INVERNESS (HIGHLANDS)",
      "MIDLOTHIAN",
      "MORAY",
      "NORTH AYRSHIRE",
      "NORTH LANARKSHIRE (NORTH)",
      "ORKNEY ISLANDS (KIRKWALL)",
      "PERTH & KINROSS",
      "RENFREWSHIRE (PAISLEY)",
      "SCOTTISH BORDERS",
      "SHETLAND ISLANDS (LERWICK)",
      "SOUTH AYRSHIRE (AYR)",
      "STIRLING",
      "WEST DUNBARTONSHIRE (CLYDEBANK)",
      "WEST LOTHIAN (LIVINGSTON)",
      "WESTERN ISLES (STORNOWAY)",
      "SOUTH LANARKSHIRE",     
   );

   if(! in_array($platingAuthority,$acceptedPlatingAuthiritiesArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$platingAuthority]";
  	  $error 			   =  true;
   }  
   
   // 2.ACCEPT if Proposer Age is between 25 and 74 inclusive 
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25 || $propAge > 74)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_BETW_25_AND_74";
      $error               = true;
   }
   
   
   //3.ACCEPT if "Full UK licence" > 2 Years
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence == 3)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_1_2_YEARS";
      $error               = true;
   }
  

   // SKIP all SMS text leads from being sent to Patons Insurance
   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }


/*
     ACCEPT leads Monday 09:00 - 17:00
                  Tuesday 09:00 - 17:00
                  Wednesday 09:00 - 17:00
                  Thursday 09:00 - 17:00
                  Friday 09:00 - 17:00
                  Saturday NO LEADS
                  Sunday NO LEADS
 */



   if($_SESSION['USER_IP'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMm     = date("i");
      $dateDdText = date("N");

      if($dateDdText > 5 || $timeHh < 9 || $timeHh >= 17)
      {
         $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_5:00PM";
         $error               = true;
      }
   }
  

   return $error;
?>

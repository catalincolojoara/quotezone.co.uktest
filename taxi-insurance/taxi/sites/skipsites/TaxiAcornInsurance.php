<?php

   $error = false;

   //Skip if Year Of Manufacture < 2004
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   if($yearOfManufacture <= 1999)
   {
      $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--YEAR_SKP_UND_2000";
      $error                       =  true;
   }


   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
      {
      // Only receive leads from Monday to Friday, 9.00am to 5.00pm daily
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {
         case '6': // Sat
         case '7': // Sun
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
            $error                           =  true;

            break;

         case '1': // Mon
         case '2': // Tue
         case '3': // Wed
         case '4': // Thu
         case '5': // Fri
         break;
      }
   }



   // SKIP if "Age" < 25
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error            =  true;
   }



   //ACCEPT if "Full UK Licence" >= 5 Years (E.G. accept "5-6 Years")
   $period_of_licence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($period_of_licence < 7)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_5_6_YEARS";
      $error                       =  true;
   }

   //only accept Taxi Driver(s) = Insured Only, Insured +1 Named Driver
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers > 2)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_ACC_INS_ONLY_INS_NAMED1+";
      $error                       =  true;
   }

  // ACCEPT if "Taxi Plating Authority" =
   $platingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $acceptedPlatingAuthiritiesArray = array(
"DERBY",
"BRADFORD",
"LEEDS",
"NEWPORT",
"PRESTON",
"NOTTINGHAM",
"NORWICH",
"MILTON KEYNES",
"CARDIFF",
"DONCASTER",
"SUNDERLAND",
"MIDDLESBOROUGH",
"NEWCASTLE UPON TYNE",
"ROTHERHAM",
"PLYMOUTH",
"PETERBOROUGH",
"LINCOLN",
   );

   if(! in_array($platingAuthority,$acceptedPlatingAuthiritiesArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$platingAuthority]";
          $error                           =  true;
   }


   //2.Skip if the Taxi No Claims Bonus = No NCB
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB == 0)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
      $error            =  true;
   }

   
   // SKIP if "Best Time To Call" = Evening (5.30pm - 8pm)
   $bestTimeCall = $_SESSION['_YourDetails_']['best_time_call'];
   if($bestTimeCall == 4)
   {
      $personalizedError[] = "SKIP_RULE_BST_CALL--TIME_TO_CALL_SKP_EVEN";
      $error               =  true;
   }

   
   return $error;
?>

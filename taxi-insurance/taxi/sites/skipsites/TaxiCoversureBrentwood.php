<?php

   $error = false;

   //1.skip if Year of Manufacture < 2003 (skip 2002, 2001 etc)
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   if($yearOfManufacture < 2003)
   {
      $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--YEAR_SKP_UND_2003";
      $error                       =  true;
   }


   //2. skip if Max. Number of Passengers > 5
//   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
//   if($taxiCapacity > '5')
//   {
//      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_5";
//      $error               =  true;
//   }

   //3. only accept Plating Authority = BRENTWOOD
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $accLicensingAuthorityArray = array(
      "BRENTWOOD",
   );

   if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
      $error               =  true;
   }


   /*4. only ACCEPT Postcode =
                              CM4
                              CM11
                              CM12
                              CM13
                              CM14
                              CM15
                              RM4
   */
   $postCodePrefix = strtoupper($_SESSION['_YourDetails_']['postcode_prefix']);

   $acceptPostcodesPrefix = array(
      "CM4",
      "CM11",
      "CM12",
      "CM13",
      "CM14",
      "CM15",
      "RM4",
   );

   if(! in_array($postCodePrefix, $acceptPostcodesPrefix))
   {
      $personalizedError[] = "SKIP_RULE_POST_PREF--ACC_LIST [$postCodePrefix]";
      $error               = true;
   }


   //5. SKIP if "Age" < 25

//   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
//   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
//   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];
//
//   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
//   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
//   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];
//
//   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
//   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;
//
//   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);
//
//   if($propAge < 25)
//   {
//      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
//      $error            =  true;
//   }

   // 6. skip if "Insurance Start Date" > 5 days after "Quote Date"
//   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
//   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
//   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];
//
//   $DIS              = $DISyyyy."-".$DISmm."-".$DISdd;
//   $inTheFuture5Days = date('Y-m-d',mktime(0,0,0,date('m'), date('d')+5, date('Y')));
//
//   if($DIS > $inTheFuture5Days)
//   {
//      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_SKP_5_DAY_OVR";
//      $error               = true;
//   }
//

   //7.skip if Taxi Badge = No Badge/ Under 6 Months/ 6 Months to 1 Year
//   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
//   if($taxiBadge < 4)
//   {
//      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_NO_BG_6_MONTH_ONE_YEAR";
//      $error               = true;
//   }

   //8.skip if Full UK Licence = Under 6 Months  6 Months to 1 Year  1-2 Years  2-3 Years
//   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
//   if($ukLicence  <= 4)
//   {
//      $personalizedError[] = "SKIP_RULE_DRIVING_LICENCE--YEARS_INT_SKP_UND_3_4_YEARS";
//      $error               = true;
//   }

   //only accept from Monday at 00.01 to Friday at 16.30  (ACCEPT all times in between)
   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");


      switch($dateDdNum)
      {
         case '5': // Fri
            if($timeHh > 16 || ($timeHh == 16 && $timeMin >= 30))
            {
               $personalizedError[] = "SKIP_RULE_SKP_DAY_FRI--TIME_SKP_OVR_4:30PM";
               $error               =  true;
            }
         break;

         case '6': // Sat
         case '7': // Sun
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
            $error               =  true;
         break;

      }
   }

   //skip if Taxi No Claims Bonus = No NCB
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($taxiNCB < 1)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
      $error 			   =  true;
   }

   return $error;
?>

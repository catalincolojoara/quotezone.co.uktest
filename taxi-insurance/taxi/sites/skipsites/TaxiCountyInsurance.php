<?php

   $error = false;

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   // SKIP if "Age" < 30
   if($propAge < 30)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_30";
      $error 			   =  true;
   }

   
   // SKIP if "Taxi no claims bonus" = "No NCB"
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   
   if($taxiNCB == 0)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
      $error               =  true;
   }


   //SKIP if Postcode = BT[1-99]*
   $postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BT",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }

   //SKIP if "Affiliate ID" = "S8V5N5" 
   $affiliateID = $_COOKIE["AFFID"];
   $affiliateID = explode("-", $affiliateID);
   if($affiliateID[0] == "S8V5N5")
   {
      $personalizedError[] = "SKIP_RULE_AFFILIATE_ID--AFF_ID_SKP_S8V5N5";
      $error               = true;
   }

   /*
"BURNLEY",
"BURY",
"BIRMINGHAM",
"BRADFORD",
"LEEDS",
"LIVERPOOL",
"MANCHESTER",
"KIRKLEES",
"LONDON PCO",
"LONDON PCO",
"LONDON PCO",
"TRAFFORD",
    * 
    */
   
   
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $platingAuthoritiesArray = array (
"BURNLEY",
"BURY",
"BIRMINGHAM",
"BRADFORD",
"LEEDS",
"LIVERPOOL",
"MANCHESTER",
"KIRKLEES",
"LONDON PCO",
"LONDON PCO",
"LONDON PCO",
"TRAFFORD",
   );
   
   if(in_array($licesingAuthority,$platingAuthoritiesArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error               =  true;
   }
   
   
  /* SKIP if "Postcode" = BB[0-99]*
                               BL[0-99]*
                               B[0-99]*
                               BD[0-99]*
                               LS[0-99]*
                               L[0-99]*
                               M[0-99]*
                               HD[0-99]*
                               EC[0-99]*
                               WC[0-99]*
                               E[0-99]*
                               N[0-99]*
                               NW[0-99]*
                               SE[0-99]*
                               SW[0-99]*
                               W[0-99]*

*/
   $postCode        = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];
   
   $postCodeArray = array (
         "BB",
         "BL",
         "B",
         "BD",
         "LS",
         "L",
         "M",
         "HD",
         "EC",
         "WC",
         "E",
         "N",
         "NW",
         "SE",
         "SW",
         "W",
   );
   
    if(in_array($postcodeToCheck,$postCodeArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error 		   =  true;
   }
   
   return $error;
?>

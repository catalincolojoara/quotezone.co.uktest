<?php

   $error = false;

   $postCode     = $_SESSION['_YourDetails_']['postcode'];

   // 1. Advance Insurance 2 will accept leads only from this postcodes : BA & SN
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
      "BA",
      "SN",
   );

   if(! in_array($postcodeToCheck,$acceptedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error 			   =  true;
   }


   // ACCEPT if "Taxi use" = Private Hire
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse != 1)
   {
      $personalizedError[] = "SKIP_RULE_USE--USE_ACC_PRIV_HIRE";
      $error               =  true;
   }

   
   //1. SKIP if user age < 25 or > 79
   $DISdd    = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm    = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy  = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD    = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM    = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY  = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd; 

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25 || $propAge > 79)
   { 
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25_AND_OVR_79";
      $error               = true;
   } 
   // ACCEPT if "Taxi no claims bonus" > 2 Years
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB < 3)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_3_YEARS";
      $error            =  true;
   }

   // ACCEPT if "Claims last 5 years" = No Claims
   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
   if($claim5Years == "Yes")
   {
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_YES";
      $error               = true;
   }

   // ACCEPT if "Convictions last 5 years" = No
   $conv5Years = $_SESSION['_YourDetails_']['convictions_5_years'];
   if($conv5Years == "Yes")
   {
      $personalizedError[] = "SKIP_RULE_CONV_LAST_5_YEARS--SKP_YES";
      $error                       =  true;
   }
   
   
   // skip leads from .13 server
   if($_SESSION["_QZ_QUOTE_DETAILS_"]["SERVER_IP"] == "192.168.12.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }


//   /*
//      SKIP Monday-Friday - 7:00am to 5:30pm
//      SKIP Saturday AFTER 6:00am
//      SKIP Sunday BEFORE 9:00pm
//
//   */
//
//   if($_SESSION["USER_IP"] != "86.125.114.56")
//   {
//      $timeHh     = date("G");
//      $timeMin    = date("i");
//      $dateDdNum  = date("N");
//
//      switch($dateDdNum)
//      {
//
//         case '1':
//         case '2':
//         case '3':
//         case '4':
//         case '5':
//         if((($timeHh >= 7) && ($timeHh < 17)) || ($timeHh == 17 && $timeMin <= 30))
//            {
//               $personalizedError[] = "SKIP_RULE_DAY_FRI--TIME_SKP_BETW_7:00AM_TO_5:30PM";
//               $error               =  true;
//            }
//         break;
//
//         case '6': // Sat
//            if($timeHh >= 6)
//            {
//               $personalizedError[] = "SKIP_RULE_DAY_SAT--TIME_SKP_OVR_6:00AM";
//               $error               = true;
//            }
//         break;
//
//         case '7':  // Sun
//            if($timeHh < 9)
//            {
//               $personalizedError[] = "SKIP_RULE_DAY_SUN--TIME_SKP_UND_9:00AM";
//               $error               = true;
//            }
//         break;
//      }
//   }



   return $error;
?>

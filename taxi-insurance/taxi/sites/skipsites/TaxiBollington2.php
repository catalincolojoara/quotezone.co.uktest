<?php

   $error = false;

   //skip if Taxi Badge = No Badge, under 6 Months, 6 Months to 1 Year, 1-2 Years
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 5)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_2_3_YEARS";
      $error               = true;
   }

   //SKIP if "Age" > 68
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge > 68)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_OVR_68";
      $error               =  true;
   }


   //Skip if Full UK Licence < 9-10 years
   $period_of_licence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($period_of_licence < 11)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_9_10_YEARS";
      $error                       =  true;
   }


   // skip if Year of Manufacture > 7 Years Old 
   $vehicleManufYear = $_SESSION["_YourDetails_"]["year_of_manufacture"];
   $disYear          = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"];

   $vehicleAge = $disYear - $vehicleManufYear;

   if($vehicleAge > 7)
   {
      $personalizedError[] = "SKIP_RULE_VEH_AGE--AGE_SKP_OVR_7";
      $error               =  true;
   }


   //Skip if Max. Number of Passengers > 8
   $taxiType     = $_SESSION['_YourDetails_']['taxi_type'];
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiType != 3)//MPV
   {
      if($taxiCapacity == '8')
      {
         $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_8";
         $error                       =  true;
      }
   }
   else
   {
      if($taxiCapacity > '8')
      {
         $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_8";
         $error                       =  true;
      }
   }

   // SKIP if "Taxi No Claims Bonus" = No NCB, 1 Year
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB < 2)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB_1YEAR";
      $error                       =  true;
   }


   // only accept attached Taxi Plating Authorities
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $accLicensingAuthorityArray = array(
"ADUR",
"BURY",
"BARNSLEY",
"BASILDON",
"BASINGSTOKE & DEANE",
"BASSETLAW",
"BEDFORD",
"BIRMINGHAM",
"BLABY",
"BLAENAU GWENT",
"BLYTH VALLEY",
"BRENTWOOD",
"BRIDGEND",
"BRISTOL CITY OF UA",
"BROMSGROVE",
"BROXBOURNE",
"BROXTOWE",
"CAERPHILLY",
"CAMBRIDGE",
"CANNOCK CHASE",
"CARDIFF",
"CHARNWOOD",
"CHELMSFORD",
"CHERWELL",
"CHESTERFIELD",
"CHICHESTER",
"CHILTERN",
"CHORLEY",
"COLCHESTER",
"CONWY",
"COVENTRY",
"CRAWLEY",
"DACORUM",
"DARLINGTON",
"DARTFORD",
"DENBIGSHIRE",
"DERBY",
"DONCASTER",
"DOVER",
"DUDLEY",
"EAST HERTS",
"NORTHAMPTON",
"EASTBOURNE",
"ELMBRIDGE",
"EPPING FOREST",
"EPSOM & EWELL",
"EREWASH",
"FAREHAM",
"FLINTSHIRE",
"GATESHEAD",
"GEDLING",
"GLOUCESTERSHIRE COUNTY",
"GUILDFORD",
"GWYNEDD",
"HARLOW",
"HART",
"HARTLEPOOL",
"HASTINGS",
"HAVANT",
"HEREFORD",
"HERTSMERE",
"HYNDBURN",
"KENNET",
"LEICESTER",
"LEWES",
"LUTON",
"MAIDSTONE",
"MANSFIELD",
"MIDDLESBOROUGH",
"MID SUSSEX",
"MEDWAY",
"MERTHYR TYDFIL",
"MILTON KEYNES",
"MOLE VALLEY",
"MONMOUTHSHIRE",
"NEATH PORT TALBOT",
"NEWARK & SHERWOOD",
"NEWCASTLE UPON TYNE",
"NEWPORT",
"NORTH EAST DERBYSHIRE",
"NORTH HERTS",
"NORTH TYNESIDE",
"NORTH WEST LEICESTER",
"NORTHAMPTON",
"NOTTINGHAM",
"NUNEATON & BEDWORTH",
"OADBY & WIGSTON",
"OSWESTRY",
"OXFORD",
"PENDLE",
"PEMBROKE",
"PLYMOUTH",
"PORTSMOUTH",
"PRESTON",
"READING",
"REDCAR & CLEVELAND",
"REDDITCH",
"REIGATE AND BANSTEAD",
"RHONDDA CYNON TAFF",
"RIBBLE VALLEY",
"ROTHERHAM",
"RUGBY",
"RUNNYMEDE",
"RUSHMOOR",
"SANDWELL",
"SEVENOAKS",
"SHEPWAY",
"SHREWSBURY",
"SLOUGH",
"SOLIHULL",
"SOUTH BEDFORDSHIRE",
"SOUTH BUCKINGHAM",
"SOUTH GLOUCESTER",
"SOUTH RIBBLE",
"SOUTH SHROPSHIRE",
"SOUTH TYNESIDE",
"SPELTHORNE",
"ST ALBANS",
"STAFFORD",
"STEVENAGE",
"STOKE ON TRENT",
"STROUD",
"SWANSEA",
"SWINDON",
"TAMWORTH",
"TANDRIDGE",
"TELFORD & WREKIN",
"TENDRING",
"THANET DISTRICT",
"THREE RIVERS",
"THURROCK",
"TONBRIDGE & MALLING",
"VALE OF GLAMORGAN",
"WAKEFIELD",
"WALSALL",
"WATFORD",
"WEALDEN",
"WEAR VALLEY",
"WELLINGBOROUGH",
"WELWYN HATFIELD",
"WEST BERKSHIRE",
"WEST LANCASHIRE",
"WINDSOR & MAIDENHEAD",
"WIRRALL",
"WOKING",
"WOKINGHAM",
"WOLVERHAMPTON",
"WREXHAM",
   );

   if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
     $error             =  true;
   }


   // Accept leads from Monday over 9am until Friday under 5pm
   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {
         case '6': // Sat
         case '7': // Sun
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
            $error                           =  true;
            break;

         case '1': // Mon
            if($timeHh < 9)
            {
               $personalizedError[] = "SKIP_RULE_DAY_MON--TIME_ACC_MON_OVR_9:00AM_FRI_SKP_OVR_17:00_SKP_SAT_SUN";
               $error                               =  true;
            }
         break;
         case '2': // Tue
         break;
         case '3': // Wed
         break;
         case '4': // Thu
         break;
         case '5': // Fri
            if($timeHh >= 17)
            {
               $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_MON_OVR_9:00AM_FRI_SKP_OVR_17:00_SKP_SAT_SUN";
               $error                               =  true;
            }
         break;
      }
   }


   return $error;

?>

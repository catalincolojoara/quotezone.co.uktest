<?php

   $error = false;

   //Receive leads Monday to Friday, 8.00am to 5.00pm
   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeMin    = date("i");
      $timeHh     = date("G");
      $dateDdText = date("N");

      if($dateDdText > 5 || $timeHh < 8 || $timeHh >= 17)
      {
         $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_8:00AM_TO_5:00PM";
         $error               = true;
      }
   }

   //Skip if age < 25 years old and > 70 years old
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25 || $propAge > 65)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25_AND_OVR_65";
      $error               = true;
   }


   //not receive leads if taxi use is public hire.
//   $taxiUsedFor = $_SESSION['_YourDetails_']['taxi_used_for'];
//   if($taxiUsedFor == 2)
//   {
//      $personalizedError[] = "SKIP_RULE_USE--USE_SKP_PUB_HIRE";
//      $error            =  true;
//   }
//
//
//   //skip if Taxi Badge = No Badge, 6 Months to 1 Year
//   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
//   if($taxiBadge < 4)
//   {
//      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_2_YEARS";
//      $error               = true;
//   }
//
//
//   //Skip if Max. Number of Passengers < 8
//   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
//   if($taxiCapacity < '8')
//   {
//      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_UND_8";
//      $error                       =  true;
//   }


   //only accept the following postcodes
   $postCode = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $accPcodesArray = array(
      "AB",
      "AL",
      "BA",
      "BH",
      "BL",
      "BN",
      "BR",
      "BS",
      "CA",
      "CB",
      "CF",
      "CM",
      "CO",
      "CR",
      "CT",
      "CV",
      "CW",
      "DA",
      "DD",
      "DE",
      "DG",
      "DH",
      "DL",
      "DN",
      "DT",
      "DY",
      "EH",
      "EN",
      "EX",
      "FK",
      "FY",
      "GL",
      "GU",
      "HA",
      "HD",
      "HG",
      "HP",
      "HR",
      "HS",
      "HU",
      "HX",
      "IG",
      "IP",
      "IV",
      "IM",
      "KA",
      "KT",
      "KW",
      "KY",
      "LA",
      "LD",
      "LE",
      "LL",
      "LN",
      "ME",
      "MK",
      "ML",
      "NG",
      "NN",
      "NP",
      "NR",
      "OL",
      "OX",
      "PE",
      "PH",
      "PL",
      "PO",
      "PR",
      "RG",
      "RH",
      "RM",
      "SA",
      "SG",
      "SL",
      "SM",
      "SN",
      "SO",
      "SP",
      "SR",
      "SS",
      "ST",
      "SY",
      "TA",
      "TD",
      "TF",
      "TN",
      "TQ",
      "TR",
      "TW",
      "UB",
      "WA",
      "WD",
      "WF",
      "WN",
      "WR",
      "WS",
      "WV",
      "YO",
      "ZE",
   );

   if(!in_array($postcodeToCheck,$accPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error                       =  true;
   }


   // skip Plating Authority = London PCO

   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $skpLicensingAuthorityArray = array(
      "LONDON PCO",
   );

   if(in_array($licesingAuthority,$skpLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error               =  true;
   }
   
   
   // SKIP if Lead Source = SMS lead
   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }
   
   
   // skip if insurance start date > 21 days after quote day
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   // today date
   $day   = date("d");
   $month = date("m");
   $year  = date("Y");

   $daysOfInsuranceStart = (mktime(0,0,0,$DISmm,$DISdd,$DISyyyy) - mktime(0,0,0,$month,$day,$year)) / (86400);

   if($daysOfInsuranceStart > 21)
   {
      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_SKP_21_DAY_AFT";
      $error               =  true;
   }
   

   return $error;

?>
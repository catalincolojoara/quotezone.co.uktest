<?php

   $error = false;

   // 1. ACCEPT if "Taxi Plating Authority" =
   $platingAuthority = $_SESSION['_YourDetails_']['plating_authority'];
   $acceptedPlatingAuthiritiesArray = array(
"ALLERDALE",
"AMBER VALLEY",
"BURY",
"BOLTON",
"BURNLEY",
"CHARNWOOD",
"COLCHESTER",
"CRAVEN",
"CREWE & NANTWICH",
"DURHAM",
"EAST HERTS",
"YORK",
"EPPING FOREST",
"FLINTSHIRE",
"GRAVESHAM",
"HARROGATE",
"HERTSMERE",
"KIRKLEES",
"LEEDS",
"LEICESTER",
"LEWES",
"LICHFIELD",
"MACCLESFIELD",
"NEWCASTLE UNDER LYME",
"NORTH LICOLNSHIRE",
"PENDLE",
"PLYMOUTH",
"POOLE",
"READING",
"RICHMONDSHIRE",
"ROCHFORD",
"ROTHERHAM",
"SALFORD",
"SOUTH BUCKINGHAM",
"SOUTH KESTEVEN",
"SWINDON",
"TAUNTON DEANE",
"TRAFFORD",
"WREXHAM",
   );

   if(! in_array($platingAuthority,$acceptedPlatingAuthiritiesArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$platingAuthority]";
          $error                           =  true;
   }

   // SKIP if "Taxi Plating Authority" = "Liverpool" AND  "Taxi No Claims Bonus" = "No NCB"
//   if($platingAuthority == "LIVERPOOL")
//   {    
//      //skip if ncb = 0
//      $drNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
//      if($drNCB == 0)
//      {
//         $personalizedError[] = "SKIP_RULE_NCB_PLT--PLATING_NCB_SKP_NO_NCB";
//         $error               =  true;
//      }
//   }
//   
   //ACCEPT if Proposer age is between 25 and 64 (e.g. accept 25 and accept 64)
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25 || $propAge > 64)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_BETW_25_AND_64";
      $error                       =  true;
   }

   //ACCEPT if "Taxi Use" = "Private Hire"
//   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
//   if($taxiUse != 1)
//   {
//      $personalizedError[] = "SKIP_RULE_USE--USE_ACC_PRIV_HIRE";
//      $error               =  true;
//   }

   //SKIP if "Full UK Licence" = "Under 6 Months", "6 Months To 1 Year"
//   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
//   if($ukLicence < 3)
//   {
//      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_1_2_YEARS";
//      $error               = true;
//   }

   //SKIP if "Taxi No Claims Bonus" = "4 Years", "5+ Years"
   $drNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($drNCB > 3)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_OVR_3_YEARS";
      $error               =  true;
   }
   
   // ACCEPT if "Year of manufacture" < 10 years from Quote Date
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   $curYear           = date('Y');

   if(($curYear - $yearOfManufacture) >= 10)
   {
       $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--AGE_SKP_OVR_9";
       $error           =  true;
   }
   
   
   // SKIP all SMS text leads from being sent to Patons Insurance
   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }
   
   
   return $error;

?>
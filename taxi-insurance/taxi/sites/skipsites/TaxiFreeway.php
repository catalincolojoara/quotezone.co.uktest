<?php

   $error = false;

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   
// 1. cannot quote because of the proposer age - skip if age is under 25 years  or   over 75 years
   if($propAge < 25 || $propAge > 75)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25_AND_OVR_75";
      $error 		   =  true;
   }
 

   //Freeway filters  if Full UK Licence = Under 6 Months,6 Months to 1 Year,1-2 Years,2-3 Years,3-4 Years,4-5 Years
   $periodOfFullUkLicenceHeld = $_SESSION["_YourDetails_"]["period_of_licence"];

   $filteredPeriodOfFullUkLicenceHeldArray = array(
      "1" => "Under 6 Months",
      "2" => "6 Months To 1 Year",
      "3" => "1 - 2 Years",
      "4" => "2 - 3 Years",
      "5" => "3 - 4 Years",
      "6" => "4 - 5 Years",
      );

   if(array_key_exists($periodOfFullUkLicenceHeld,$filteredPeriodOfFullUkLicenceHeldArray))
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--SKP_LIST [".$filteredPeriodOfFullUkLicenceHeldArray[$periodOfFullUkLicenceHeld]."]";
      $error 			   =  true;
   }

   //Freeway filters if Taxi Badge = No Badge,Under 6 Months,6 Months to 1 Year
   $taxiBagde = $_SESSION["_YourDetails_"]["taxi_badge"];

   $filteredTaxyBadgeArray = array(
      "1" => "No Badge",
      "2" => "Under 6 Months",
      "3" => "6 Months To 1 Year",
    );

   if(array_key_exists($taxiBagde,$filteredTaxyBadgeArray))
   {
      $personalizedError[] = "SKIP_RULE_BADG--SKP_LIST [".$filteredTaxyBadgeArray[$taxiBagde]."]";
      $error 			   =  true;
   }
    
   
   //If Taxi Plating Authority = London PCO, Birmingham
//    $platingAuthorities = $_SESSION["_YourDetails_"]["plating_authority"];
// 
//    $filteredPlatingAuthorityArray = array(
//       "LONDON PCO",
//    );
// 
//    if(in_array($platingAuthorities,$filteredPlatingAuthorityArray))
//    {
//       $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$platingAuthorities]";
//	  	 $error 			  =  true;
//    }


   
   //Skip if convictions last 5 years = Yes
   $conv5Years = $_SESSION['_YourDetails_']['convictions_5_years'];
   if($conv5Years == "Yes")
   {
      $personalizedError[] = "SKIP_RULE_CONV_LAST_5_YEARS--SKP_YES";
      $error                       =  true;
   }

   //please update this plugin so that they do not receive leads that come from SMS renewals WR50-5B2
   if($_SESSION["_QZ_QUOTE_DETAILS_"]["SERVER_IP"] == "192.168.12.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   //please update this plugin so that they do not receive leads that come from SMS renewals WR50-5B2 - second check
   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   
   //only accept Taxi Driver(s) = Insured Only
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers != 1)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_ACC_INS_ONLY";
      $error                       =  true;
   }
   
   
   // skip if Taxi Plating Authority 
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $skipLicensingAuthorityArray = array(
      "BELFAST",
      "DVA (NI)",
      "NORTHERN IRELAND",
      "PSV EASTERN",
      "PSV JERSEY",
      "PSV NORTH EAST",
      "PSV NORTH WEST",
      "PSV SCOTLAND",
      "PSV WALES",
      "PSV WEST MIDLANDS",
      "PSV WESTERN",
   );

   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error               =  true;
   }
   
   
   /*If 'Type of cover' = Third Party Fire and Theft
                  SKIP if 'Taxi plating authority' = 
                  Birmingham
                  Blackburn with Darwen
                  Bolton
                  Bradford
                  Bury
                  Leeds
                  Liverpool
                  Luton
                  Manchester
                  Oldham
                  Rochdale
                  Rossendale
                  Salford
                  South Lakeland
                  Walsall
                  Watford */

   $typeOfCover       = $_SESSION['_YourDetails_']['type_of_cover'];               
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $filterMsg         = 'Type of cover = '. $typeOfCover.' AND Taxi plating authority = '.$licesingAuthority; 
   
   $skipLicensingAuthorityArray = array(
      "BIRMINGHAM",
      "BLACKBURN WITH DARWEN",
      "BOLTON",
      "BRADFORD",
      "BURY",
      "LEEDS",
      "LIVERPOOL",
      "LUTON",
      "MANCHESTER",
      "OLDHAM",
      "ROCHDALE",
      "ROSSENDALE",
      "SALFORD",
      "SOUTH LAKELAND",
      "WALSALL",
      "WATFORD",
   );

   
   if($typeOfCover == 2)
   {
      if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
      {
         $personalizedError[] = "SKIP_RULE_TYP_COV_PLATH_AUTH--SKP_LIST [$filterMsg]";
         $error               =  true;
      }
   }
   
   //Skip if claims in the last 5 years = Yes
   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
   if($claim5Years == "Yes")
   {
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_YES";
      $error               = true;
   }
	
	
	// cannot quote because of the postcode - skip a list of values (check the skipped list bellow) BT
	$postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BT",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error               =  true;
   }

   
//  //  If Taxi Plating Authority = London PCO, Birmingham only ACCEPT if BOTH "Claims Last 5 Years = No"  and  "Convictions Last 5 Years = No" 
//     $claim5YearsSpe = $_SESSION['_YourDetails_']['claims_5_years'];
//     $conv5YearsSpe = $_SESSION['_YourDetails_']['convictions_5_years'];
//     $platingAuthoritiesSpe = $_SESSION["_YourDetails_"]["plating_authority"];
//     $filteredPlatingAuthorityList = array(
//        "LONDON PCO",
//        "BIRMINGHAM",
//     );
//  
//     if(in_array($platingAuthoritiesSpe,$filteredPlatingAuthorityList))
//     {
//       if($claim5YearsSpe == "Yes" || $conv5YearsSpe == "Yes")
//       { 
//         $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--LICEN_LONDON_PCO_BIRMIN";
//         $error               = true;
//       }
//     }
   
   return $error;
?>

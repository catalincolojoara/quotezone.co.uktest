<?php

   $error = false;

   //acc postcodes.
   $postCode       = $_SESSION['_YourDetails_']['postcode'];
   $postCodePrefix = $_SESSION['_YourDetails_']['postcode_prefix'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
         "GU",
         "KT",
   );

   $accPcodesPrefixArray = array(
      "TW20",
      "TW19",
      "SL3",
      "SL4",
   );

   if((! in_array($postCodePrefix,$accPcodesPrefixArray)) && (! in_array($postcodeToCheck,$acceptedPcodesArray)))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error                        =  true;
   }

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
      {
      // Only receive leads from Monday to Friday, 9.00am to 5.00pm daily
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {
         case '6': // Sat
         case '7': // Sun
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
            $error                           =  true;

            break;

         case '1': // Mon
         case '2': // Tue
         case '3': // Wed
         case '4': // Thu
         case '5': // Fri
            if($timeHh < 9 || $timeHh >= 17)
            {
               $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_5:00PM";
               $error                               =  true;
            }
         break;
      }
   }


   return $error;
?>

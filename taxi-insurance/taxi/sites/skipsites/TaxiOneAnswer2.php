<?php

   $error = false;
   $personalizedError = "";

   //1.One answer will get leads only for this postcode  -- updated with wr:46342
   $postCode        = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
//       "BN",
//       "TN",
//       "RH",
//       "GL",
//       "GU",
//       "NR",
//       "SN",
//       "BA",
//       "BH",
//       "SP",
//       "DT",
//       "EX",
//       "PL",
//       "TA",
//       "TQ",
//       "TR",
//       "CT",
//       "DA",
//       "IP",
//       "ME",
//       "PE",
//       "PO",
//       "BR",
//       "SS",
//       "FK",
//       "AB",
//       "KY",
//       "IV",
//       "KA",
//       "SE",
//       "MK",
//       "B",
//       "BB",
//       "ME",
//       "BR",
//       "SO",
         "HG",
         "IP",
         "DD",
         "YO",
         "LL",
         "CB",
         "LN",
         "HP",
         "PA",
         "HR",
         "HX",
         "KT",
         "MK",
         "CA",
      );

   if(! in_array($postcodeToCheck,$acceptedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error                       =  true;
   }

   //2.Skip if user age < 22 years old
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 22)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_22";
      $error                       =  true;
   }

   //3.Only Fully Comprehensive
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover != 1)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_ACC_FULL_COMPR";
      $error                       =  true;
   }
   
   
// "LONDON PCO",
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $platingAuthoritiesArray = array (
"LONDON PCO",
// "LONDON PCO",
   );
   
   if(in_array($licesingAuthority,$platingAuthoritiesArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error               =  true;
   }

   return $error;

?>

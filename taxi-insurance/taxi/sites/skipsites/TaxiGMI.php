<?php

   $error = false;

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   // 1.GMI accepts leads for users aged 35 or over.
   if($propAge < 35)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_35";
      $error 			   =  true;
   }

   // 2.GMI will not receive leads for the postcode $postCode.
   $postCode     = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "L", 
      "M", 
      "NE",
      "BD",
      "SR",
      "B",
      "LS",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error 			   =  true;
   }

   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   $vehicleAge = ($DISyyyy - $yearOfManufacture);

   // 3.GMI accepts leads for vehicles that are maximum 10 years old.
   if($vehicleAge > 10)
   {
      $personalizedError[] = "SKIP_RULE_VEH_AGE--AGE_SKP_OVR_10";
      $error 			   =  true; 
   }

   $drNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   // 4.GMI accepts leads for drivers that have minimum 2 year ncb.
   if($drNCB < 2)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_2_YEARS";
      $error 			   =  true;
   }

   // 5.GMI will not receive leads if taxi use is public hire.
   $taxiUsedFor = $_SESSION['_YourDetails_']['taxi_used_for'];

   if($taxiUsedFor == 2)
   {
      $personalizedError[] = "SKIP_RULE_USE--USE_SKP_PUB_HIRE";
      $error 			   =  true;
   }

   return $error;

?>

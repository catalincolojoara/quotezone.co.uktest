<?php

   $error = false;

   $postCode     = $_SESSION['_YourDetails_']['postcode'];

   //1. Coversure Reddich will accept all quotes that come form postcodes that start with
   // B60,B61,B97,B98,all WR and all HR
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
      "WR",
      "HR",
   );

   $acceptedPcodesPrefArray = array(
      "B60",
      "B61",
      "B97",
      "B98",
   );

   $postCodePrefix  = $_SESSION['_YourDetails_']['postcode_prefix'];

   if((! in_array($postcodeToCheck,$acceptedPcodesArray)) && (! in_array($postCodePrefix,$acceptedPcodesPrefArray)))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error 			   =  true;
   }

   return $error;

?>

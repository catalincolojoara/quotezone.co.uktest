<?php

   $error = false;
 
 
   // ACCEPT only if "Type of cover" = "Fully comprehensive"
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover != 1)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_ACC_FULL_COMPR";
      $error         =  true;
   }
   
   
   // SKIP if "Age" < 25  > 65
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25 || $propAge > 65)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25_AND_OVR_65";
      $error               =  true;
   }
   
   
   // SKIP if "Full UK licence" = "1 - 2 Years"
   $periodOfFullUkLicenceHeld = $_SESSION["_YourDetails_"]["period_of_licence"];

   $filteredPeriodOfFullUkLicenceHeldArray = array(
//       "1" => "Under 6 Months",
//       "2" => "6 Months To 1 Year",
      "3" => "1 - 2 Years",
//       "4" => "2 - 3 Years",
//       "5" => "3 - 4 Years",
//       "6" => "4 - 5 Years",
      );

   if(array_key_exists($periodOfFullUkLicenceHeld,$filteredPeriodOfFullUkLicenceHeldArray))
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_2_YEARS";
      $error               =  true;
   }
   
   
   // SKIP if "Taxi badge" = "No Badge", "Under 6 Months", "6 Months To 1 Year"
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_YEAR";
      $error               = true;
   }
   
   
   // SKIP if "Taxi no claims bonus" = "No NCB"
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB == 0)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
      $error               =  true;
   }
   

   // SKIP if "Taxi driver(s)" = "Insured and 2+ Named Drivers"
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   
   if($taxiDrivers == 3)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_SKP_INS_AND_2+_NAM_DRV";
      $error               =  true;
   }
   
   
   // SKIP if "Estimated taxi value (£)" > 35,000
   $estimated_value = $_SESSION['_YourDetails_']['estimated_value'];
   if($estimated_value > 35000)
   {
      $personalizedError[] = "SKIP_RULE_VEH_VALUE--POUND_SKP_OVR_35000";
      $error               = true;
   }
   
   
   // SKIP if "Max. number of passengers" > 7
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity > 7)
   {
         $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_7";
         $error               =  true;
   }
   
   
   // ACCEPT only the attached Postcode
   $postCode        = $_SESSION['_YourDetails_']['postcode'];  
   $postCodePreffix = $_SESSION['_YourDetails_']['postcode_prefix']; 
   $postCodeSuffix  = $_SESSION['_YourDetails_']['postcode_number'];    
      
   
   
   $filter          = "accept";// skip or accept   
   $postCodeArray   = array(  
      "CF1",
      "CF[3-24]*",
      "CF[31-48]*",
      "CF[61-64]",
      "CF[71-72]*",
      "CF[81-83]*",
      "CH1",
      "CH[3-8]*",
      "LL[11-49]*",
      "LL[51-62]*",
      "LL[64-78]*",
      "NP[1-7]*",
      "NP[9-10]*",
      "NP[19-20]*",
      "NP23",
      "NP44",
      "SA4",
      "SA[14-20]*",
      "SA[31-48]*",
      "SA[61-73]*",
      "SY[13-14]*",
      "SY20",
      "SY[23-25]*",  
         ); 
   
   $filterStatus = "";     
   
   preg_match("/(.*)\d/isU",$postCodePreffix,$pregLettersToCheck);   
   $lettersPostCodePreffix = $pregLettersToCheck[1];  
   
   preg_match("/[0-9]+/",$postCodePreffix,$pregNumbersToCheck);   
   $numbersPostCodePreffix = $pregNumbersToCheck[0];  
   
   $prefLettAndNum = $lettersPostCodePreffix.$numbersPostCodePreffix;   
   
   foreach($postCodeArray as $keypostCodeArray=>$skpPostcode)  
   {  
      if((preg_match("/[A-Z]*/",$skpPostcode,$pregMatchLetters)) && (preg_match("/\[(.*)\-/",$skpPostcode,$pregMinInterval)) && (preg_match("/\-(.*)\]/",$skpPostcode,$pregMaxInterval))) 
      {  
         $prefixLettersArr = $pregMatchLetters[0]; 
         $minIntervalArr   = $pregMinInterval[1];  
         $maxIntervalArr   = $pregMaxInterval[1];  
      }  
      else  
      {  
         $prefixLettersArr = ""; 
         $minIntervalArr   = ""; 
         $maxIntervalArr   = ""; 
      }  
         
         
      //checking postcodes like BT[1-99]* 
      if((preg_match("/\*/isU",$skpPostcode,$pregMatchAll)))   
      {  
         if(($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr))) 
         {  
            //"postcode has been accepted \n"; //only accept list 
            $filterStatus = true;   
            break;   
         }  
      }  
   
      //checking postcodes like BT[1-99]  
      elseif(preg_match("/^[A-Z]{1,2}\[[0-9]{1,2}-[0-9]{1,2}\]$/isU",$skpPostcode,$pregMatchAll))  
      {  
         if(($postCodePreffix == $prefLettAndNum) && ($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))  
         {              
            $filterStatus = true;   
            break;   
         }  
      }  
         
      //checking postcodes like BT3 9  
      elseif((preg_match("/(.*)\s+/isU",$skpPostcode, $pregMatchSpace)))   
      {        
      $sufixNumber = substr($postCodeSuffix,0,-2); 
      $postCodeToCheck = $postCodePreffix." ".$sufixNumber; 
            
         if($skpPostcode == $postCodeToCheck)   
         {  
            $filterStatus = true;   
            break;   
         }              
      }  
   
      //checking postcodes like BT339AA OR BT39AA  
      elseif(ctype_alpha(substr($skpPostcode,-2))) 
      {  
      $postCodeStrip = str_replace(" ","",$postCode); 
   
         if($skpPostcode == $postCodeStrip)  
         {  
            $filterStatus = true;   
            break;   
         }              
      }  
   
      //checking postcodes preffix like BT1, BT1A  
      elseif((preg_match("/^$postCodePreffix$/",$skpPostcode)))   
      {  
         if($postCodePreffix == $skpPostcode);  
         {  
            $filterStatus = true;   
            break;   
         }  
      }  
   }  
   
   if($filter == "accept") 
   {  
      if($filterStatus == false) 
      {  
         $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]\n"; 
         $error               =  true; 
      }  
   }  
   elseif($filter == "skip")  
   {  
      if($filterStatus==true) 
      {  
         $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";   
         $error               =  true; 
      }  
   }  
 
 
   return $error;
?>

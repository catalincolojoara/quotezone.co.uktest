<?php

   $error = false;

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 23)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_23";
      $error 			   =  true;
   }

   $skipLicensingAuthorityArray = array(
"BARKING",
"BARNET",
"BEXLEY",
"BIRMINGHAM",
"BRADFORD",
"BRENTWOOD",
"BROMLEY",
"CAMDEN",
"CROYDEN",
"DARTFORD",
"EALING",
"ENFIELD",
"LONDON PCO",
"GREENWICH",
"HACKNEY",
"HAMMERSMITH",
"HARINGEY",
"HARROGATE",
"HAVERING",
"HILLINGDON",
"HOUNSLOW",
"ISLINGTON",
"KENSINGTON",
"LAMBETH",
"LEEDS",
"LEWISHAM",
"LONDON PCO",
"MERTON",
"NEWHAM",
"REDBRIDGE",
"RICHMONDSHIRE",
"SOUTHWARK",
"SUTTON",
"TOWER HAMLETS",
"WANDSWORTH",
"WESTMINSTER",
   );

   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
  	  $error 			   =  true;
   }

   return $error;
?>

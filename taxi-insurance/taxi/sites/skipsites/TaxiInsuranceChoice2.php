<?php

   $error = false;

   
   // SKIP if "Taxi plating authority" = see attached
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $skipLicensingAuthorityArray = array(
      "BURY",
      "BLACKBURN WITH DARWEN",
      "BOLTON",
      "BRADFORD",
      "CALDERDALE",
      "GLASGOW",
      "HALTON",
      "HYNDBURN",
      "KEIGHLEY",
      "KIRKLEES",
      "KNOWSLEY",
      "LEEDS",
      "LONDON PCO",
      "LIVERPOOL",
      "LUTON",
      "MANCHESTER",
      "OLDHAM",
      "PRESTON",
      "RIBBLE VALLEY",
      "ROCHDALE",
//      "ROSSENDALE",
      "SALFORD",
      "SANDWELL",
      "STOCKPORT",
      "ST HELENS",
      "TAMESIDE",
      "TRAFFORD",
      "WIGAN",
      "WESTMINSTER",
   );


   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error               =  true;
   }
   
   
   // SKIP if "Postcode" = see attached
   $postCode        = $_SESSION['_YourDetails_']['postcode'];  
   $postCodePreffix = $_SESSION['_YourDetails_']['postcode_prefix']; 
   $postCodeSuffix  = $_SESSION['_YourDetails_']['postcode_number'];    
      
   
   
   $filter          = "skip";// skip or accept   
   $postCodeArray   = array(  
      "B[1-29]*",
      "B[42-44]*",
      "BB[0-99]*",
      "BD[0-20]*",
      "BD22",
      "BD[24-99]*",
      "BL[0-99]*",
      "BS[1-3]*",
      "DY[0-99]*",
      "GY[0-99]*",
      "HD[0-99]*",
      "HS[0-99]*",
      "HX[0-99]*",
      "IG[0-99]*",
      "JE[0-99]*",
      "E[0-99]*",
      "EC[0-99]*",
      "L[0-99]*",
      "LS[0-99]*",
      "LU[0-99]*",
      "M[0-99]*",
      "NE[0-99]*",
      "OL[0-99]*",
      "PR[0-99]*",
      "UB[0-99]*",
      "S[1-14]*",
      "SK[0-99]*",
      "SR[0-99]*",
      "WA[0-99]*",
      "WN[0-99]*",
      "WV2",
      "BT[0-99]*",
         ); 
   
   $filterStatus = "";     
   
   preg_match("/(.*)\d/isU",$postCodePreffix,$pregLettersToCheck);   
   $lettersPostCodePreffix = $pregLettersToCheck[1];  
   
   preg_match("/[0-9]+/",$postCodePreffix,$pregNumbersToCheck);   
   $numbersPostCodePreffix = $pregNumbersToCheck[0];  
   
   $prefLettAndNum = $lettersPostCodePreffix.$numbersPostCodePreffix;   
   
   foreach($postCodeArray as $keypostCodeArray=>$skpPostcode)  
   {  
      if((preg_match("/[A-Z]*/",$skpPostcode,$pregMatchLetters)) && (preg_match("/\[(.*)\-/",$skpPostcode,$pregMinInterval)) && (preg_match("/\-(.*)\]/",$skpPostcode,$pregMaxInterval))) 
      {  
         $prefixLettersArr = $pregMatchLetters[0]; 
         $minIntervalArr   = $pregMinInterval[1];  
         $maxIntervalArr   = $pregMaxInterval[1];  
      }  
      else  
      {  
         $prefixLettersArr = ""; 
         $minIntervalArr   = ""; 
         $maxIntervalArr   = ""; 
      }  
         
         
      //checking postcodes like BT[1-99]* 
      if((preg_match("/\*/isU",$skpPostcode,$pregMatchAll)))   
      {  
         if(($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr))) 
         {  
            //"postcode has been accepted \n"; //only accept list 
            $filterStatus = true;   
            break;   
         }  
      }  
   
      //checking postcodes like BT[1-99]  
      elseif(preg_match("/^[A-Z]{1,2}\[[0-9]{1,2}-[0-9]{1,2}\]$/isU",$skpPostcode,$pregMatchAll))  
      {  
         if(($postCodePreffix == $prefLettAndNum) && ($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))  
         {              
            $filterStatus = true;   
            break;   
         }  
      }  
         
      //checking postcodes like BT3 9  
      elseif((preg_match("/(.*)\s+/isU",$skpPostcode, $pregMatchSpace)))   
      {        
      $sufixNumber = substr($postCodeSuffix,0,-2); 
      $postCodeToCheck = $postCodePreffix." ".$sufixNumber; 
            
         if($skpPostcode == $postCodeToCheck)   
         {  
            $filterStatus = true;   
            break;   
         }              
      }  
   
      //checking postcodes like BT339AA OR BT39AA  
      elseif(ctype_alpha(substr($skpPostcode,-2))) 
      {  
      $postCodeStrip = str_replace(" ","",$postCode); 
   
         if($skpPostcode == $postCodeStrip)  
         {  
            $filterStatus = true;   
            break;   
         }              
      }  
   
      //checking postcodes preffix like BT1, BT1A  
      elseif((preg_match("/^$postCodePreffix$/",$skpPostcode)))   
      {  
         if($postCodePreffix == $skpPostcode);  
         {  
            $filterStatus = true;   
            break;   
         }  
      }  
   }  
   
   if($filter == "accept") 
   {  
      if($filterStatus == false) 
      {  
         $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]\n"; 
         $error               =  true; 
      }  
   }  
   elseif($filter == "skip")  
   {  
      if($filterStatus==true) 
      {  
         $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";   
         $error               =  true; 
      }  
   }  // end of Postcode Filter
   
   
   // SKIP if "Age" < 23 > 70
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 23|| $propAge > 70)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--Skip under 23 and  over 70 years";
      $error               =  true;
   }
   
     
//   // SKIP if "Full UK licence" = "1 - 2 Years"
//   $periodOfFullUkLicenceHeld = $_SESSION["_YourDetails_"]["period_of_licence"];
//
//   $filteredPeriodOfFullUkLicenceHeldArray = array(
////       "1" => "Under 6 Months",
////       "2" => "6 Months To 1 Year",
//      "3" => "1 - 2 Years",
////       "4" => "2 - 3 Years",
////       "5" => "3 - 4 Years",
////       "6" => "4 - 5 Years",
//      );
//
//   if(array_key_exists($periodOfFullUkLicenceHeld,$filteredPeriodOfFullUkLicenceHeldArray))
//   {
//      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--SKP_LIST [".$filteredPeriodOfFullUkLicenceHeldArray[$periodOfFullUkLicenceHeld]."]";
//      $error               =  true;
//   }

   
//   // SKIP if "Convictions last 5 years" = "Yes"
//   $claim5Years = $_SESSION['_YourDetails_']['convictions_5_years'];
//   if($claim5Years == "Yes")
//   {
//      $personalizedError[] = "SKIP_RULE_CONV_LAST_5_YEARS--SKP_YES";
//      $error               = true;
//   }
//
                                      
   // SKIP if "Taxi driver(s)" = "Insured and 2+ Named Drivers",
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers == 3)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_SKP_INS_AND_2+_NAM_DRV";
      $error               =  true;
   }  
   

   // SKIP if "Insurance Start Date" > "Quote Date" + 14 Days
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   // today date
   $day   = date("d");
   $month = date("m");
   $year  = date("Y");

   $daysOfInsuranceStart = (mktime(0,0,0,$DISmm,$DISdd,$DISyyyy) - mktime(0,0,0,$month,$day,$year)) / (86400);

   if($daysOfInsuranceStart > 14)
   {
      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_SKP_14_DAY_AFT";
      $error               =  true;
   }
   

   // SKIP if "Vehicle Age" > 12 years    i.e. SKIP if "Year of manufacture" = 1991, 1990, 1989, etc....
   $vehicleManufYear = $_SESSION["_YourDetails_"]["year_of_manufacture"];
   $disYear          = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"];

   $vehicleAge = $disYear - $vehicleManufYear;

   if($vehicleAge > 15)
   {
      $personalizedError[] = "SKIP_RULE_VEH_AGE--NUM_SKP_OVR_15";
      $error               =  true;
   }
      
      
   // If "Taxi no claims bonus" = "No NCB" SKIP if "Private car no claims bonus" = "No NCB",  "1 Year", "2 Years"
   $taxiNCB       = $_SESSION['_YourDetails_']['taxi_ncb'];
   $privateCarNCB = $_SESSION['_YourDetails_']['private_car_ncb'];
   $ncbMsg        = "'Taxi no claims bonus' = 'No NCB' and 'Private car no claims bonus' under 3 Years";

   if($taxiNCB == 0)
   {
      if($privateCarNCB < 3)
      {
         $personalizedError[] = "SKIP_RULE_NCB_PNCB--SKP_LIST [$ncbMsg]";
         $error                       =  true;
      }
   }   

   // SKIP if "Taxi Use" = Public Hire
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse == 2)
   {
      $personalizedError[] = "SKIP_RULE_USE--USE_SKP_PUB_HIRE";
      $error               =  true;
   }


   return $error;

?>
<?php

   $error = false;
   
   $postCode = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $accPcodesArray = array(
         "BL",
         "CA",
         "SK",
         "WA",
         "AB",
         "CH",
   );

   if(! in_array($postcodeToCheck,$accPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error                       =  true;
   }
   
   // 2. SKIP if Taxi Type = MPV
   $taxiType = $_SESSION['_YourDetails_']['taxi_type'];
   if($taxiType == 3)
   {
      $personalizedError[] = "SKIP_RULE_VEH_TYP--TYP_SKP_MPV";
      $error            =  true;
   }

//  3.SKIP if Taxi NCB = No NCB 1 Year
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($taxiNCB < 2)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_2_YEARS";
      $error 			   =  true;
   }
   
// 4.SKIP if Year of Manufacture > 2008
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   if($yearOfManufacture > 2008)
   {
      $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--YEAR_SKP_OVR_2008";
      $error                       =  true;
   }
   
// 5. SKIP if Type of Cover = Third Party
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if( $typeOfCover == 3)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_SKP_TPO";
      $error               =  true;
   }
   
//    //6. only accept leads monday - friday
//    $timeHh     = date("G");
//    $dateDdText = date("N");
// 
//    if($dateDdText > 5)
//    {
//       $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
//       $error               = true;
//    }

   return $error;

?>

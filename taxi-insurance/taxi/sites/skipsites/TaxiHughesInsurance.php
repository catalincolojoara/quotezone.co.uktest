<?php

   $error = false;

   // Skip if Driver age < 25 years old.
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error               =  true;
   }

   // Only accept leads if Postcode = BT[1-99]*
   $postCode     = strtoupper($_SESSION['_YourDetails_']['postcode']);

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptPcodesArray = array(
      "BT",
   );

   if( ! in_array($postcodeToCheck, $acceptPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error               =  true;
   }

   // Receive leads from Monday to Friday, 8.00am to 6.00pm only
   $timeMin    = date("i");
   $timeHh     = date("G");
   $dateDdText = date("N");

   if($dateDdText > 5 || $timeHh < 8 || $timeHh >= 18)
   {
      $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_8:00AM_TO_6:00PM";
      $error               =  true;
   }

   // 4. Skip if Max. Number of Passengers > 7
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity > '7')
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_7";
      $error                       =  true;
   }
   return $error;
?>

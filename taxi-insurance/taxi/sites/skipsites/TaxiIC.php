<?php 

   $error = false;

   //1.ACCEPT only if "Taxi driver(s)" = "Insured Only"
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers != 1)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_ACC_INS_ONLY";
      $error                       =  true;
   }


   //2. SKIP if "Age" < 30 > 69
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge  < 30 || $propAge > 69)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_30_AND_OVR_69";
      $error            =  true;
   }

   /*3.SKIP if "Full UK licence" =  "1 - 2 Years"
                                  "2 - 3 Years"
                                  "3 - 4 Years"
                                  "4 - 5 Years"
   */

   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence < 7)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_5_6_YEARS";
      $error               =  true;
   }

   /*4.SKIP if "Taxi no claims bonus" = "No NCB"   "1 Year"*/
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($taxiNCB < 2)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_2_YEARS";
      $error               =  true;
   }

   /*5. SKIP if "Taxi badge" = "No Badge"
                              "Under 6 Months"
                              "6 Months To 1 Year"
                              "1 - 2 Years"
   */

   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge <= 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_2_3_YEARS";
      $error               = true;
   }

   // 6. SKIP if "Year of manufacture" < 2001
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   if($yearOfManufacture < 2001)
   {
      $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--YEAR_SKP_UND_2001";
      $error            =  true;
   }


   //7. ACCEPT only if "Taxi plating authority" = attached
   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];
   $acceptedLicensingAuthorityArray = array(
      "AYLESBURY VALE",
      "BARROW IN FURNESS",
      "BASINGSTOKE & DEANE",
      "BEDFORD",
      "BRIDGEND",
      "BRIGHTON & HOVE",
      "BRISTOL CITY OF UA",
      "CAMBRIDGE",
      "CARDIFF",
      "CHELTENHAM",
      "CORBY",
      "COVENTRY",
      "DAVENTRY",
      "DERBY",
      "DONCASTER",
      "EASTBOURNE",
      "EXETER",
      "FAREHAM",
      "GLOUCESTER",
      "HARLOW",
      "KETTERING",
      "KINGSTON-UPON-HULL",
      "MILTON KEYNES",
      "NEWARK & SHERWOOD",
      "NEWPORT",
      "NORTHAMPTON",
      "NOTTINGHAM",
      "NUNEATON & BEDWORTH",
      "OXFORD",
      "PEMBROKE",
      "ROTHERHAM",
      "RUGBY",
      "SOUTHAMPTON",
      "WARWICK",
      "WEST LINDSEY",
      "WOKING",
      "WREXHAM",
      "WYCOMBE",
      "YORK",

   );

   if(! in_array($licesingAuthority,$acceptedLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
      $error                           =  true;
   }
   
   
   // SKIP if "Insurance Start Date" >  28 Days from Quote Date
   $day     = date("d");
   $month   = date("m");
   $year    = date("Y");
   $todayMk = mktime("0","0","0",$month,$day,$year);

   $disMM   = $_SESSION["_YourDetails_"]["date_of_insurance_start_mm"];
   $disDD   = $_SESSION["_YourDetails_"]["date_of_insurance_start_dd"];
   $disYYYY = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"];
   $disMk   = mktime("0","0","0",$disMM,$disDD,$disYYYY);

   $timeDiffDays = (($disMk - $todayMk) / 86400);

   if($timeDiffDays > 28)
   {
      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_SKP_28_DAY_AFT";
      $error               =  true;
   }
   

   return $error;
?>

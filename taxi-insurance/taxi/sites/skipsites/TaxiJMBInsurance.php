<?php

   $error             = false;

//   2. SKIP if "Taxi No Claims Bonus" = "No NCB"

   $taxiNCB    = $_SESSION['_YourDetails_']['taxi_ncb'];   
   if($taxiNCB == 0)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
      $error               =  true;
   }
   
   $privateCarNCB = $_SESSION['_YourDetails_']['private_car_ncb'];
   if($privateCarNCB == 0)
   {
      $personalizedError[] = "SKIP_RULE_PVT_NCB--NCB_SKP_NO_NCB";
      $error               =  true;
   }
 // 3. Skip if Taxi Badge = No badge, Under 6 months, 6 months to 1 year
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--BADG_SKP_N_BADG_UND6_MONTH_6MONTH";
      $error               = true;
   }
   
// 4. SKIP if "Age" < 25
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);
   if($propAge < 25) 
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error 			   =  true;
   }

  
 /* 5. ACCEPT leads Monday 09:00 - 17:00
                Tuesday 09:00 - 17:00
                Wednesday 09:00 - 17:00
                Thursday 09:00 - 17:00
                Friday 09:00 - 17:00
                Saturday NO LEADS
                Sunday NO LEADS
   */


   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {
      	case '1':
      	case '2':
      	case '3':
      	case '4':
      	case '5':
      	if($timeHh < 9 || $timeHh >= 17)
      	{
      		$personalizedError[] = "SKIP_RULE_SKP_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_5:00PM";
            $error               =  true;
      	}
      	break;
      	case '6': 
      	case '7':
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
            $error               =  true;
         break;
      }
   }


      //Only the following plating authorities are acceptable.
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $accLicensingAuthorityArray = array(
"ADUR",
"ARUN",
"BRIGHTON & HOVE",
"EASTBOURNE",
"LEWES",
"WORTHING",
"ASHFORD",
"HASTINGS",
"ROTHER",
"SEVENOAKS",
   );

   if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
     $error             =  true;
   }
   
   
   // SKIP if "Postcode" = BB[0-99]*
   //                      BD[0-99]*
   
   $postCode     = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BB",
      "BD", 
      "B",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error               =  true;
   }
   
     
   //SKIP if "Best Day To Call" = "Best Day To Call" = "Saturday"  "Sunday"   
   $bestDayToCall = $_SESSION['_YourDetails_']['best_day_call'];
   if($bestDayToCall == 6 || $bestDayToCall == 7)
   {
      $personalizedError[] = "SKIP_RULE_BSD_CALL--DAY_TO_CALL_SKP_SAT_SUN";
      $error               = true;
   } 
   
   //skip if Insurance Start Date > 21 days after Quote Date
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   // today date
   $day   = date("d");
   $month = date("m");
   $year  = date("Y");

   $daysOfInsuranceStart = (mktime(0,0,0,$DISmm,$DISdd,$DISyyyy) - mktime(0,0,0,$month,$day,$year)) / (86400);

   if($daysOfInsuranceStart > 21)
   {
      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_SKP_21_DAY_AFT";
      $error                       =  true;
   }
   
   
   return $error;
?>
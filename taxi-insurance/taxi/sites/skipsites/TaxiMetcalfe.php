<?php

   $error = false;

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   // 1.Skip if driver age < 25 years old and > 70 years old
   if($propAge < 50 || $propAge > 70)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_50_OR_OVR_69";
      $error 			   =  true;
   }

   //2.Skip if Full UK licence < 5 years. 
   $period_of_licence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($period_of_licence <= 6)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_4_5_YEARS";
      $error 			   =  true;
   }

   //3.Skip if Taxi Badge = No badge
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge == 1 )
   {
      $personalizedError[] = "SKIP_RULE_BADG--BADG_SKP_N_BADG";
      $error 			   =  true;
   }

   //4.Skip if Type of Cover = Thrid party fire & theft or Third party
//   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
//   if($typeOfCover == 3)
//   {
//      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_SKP_TP";
//      $error               =  true;
//   }


   //5. Think will only recieve leads for this licensing authorities
   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

   $skipLicensingAuthorityArray = array(
"SEFTON",
"LIVERPOOL",
"KNOWSLEY",
"ST HELENS",
"BLACKBURN WITH DARWEN",
"LEEDS",
"BIRMINGHAM",
"LUTON",
"NEWCASTLE UPON TYNE",
"SOLIHULL",
"DUDLEY",
"CRAVEN",
"WOLVERHAMPTON",
"HALTON",
"WAKEFIELD",
"ROTHERHAM",
"SHEFFIELD",
"SANDWELL",
"COVENTRY",
"WELWYN HATFIELD",
"LEICESTER",
"DERBY",
"NOTTINGHAM",
"NORTHAMPTON",
"STOKE ON TRENT",
"DONCASTER",
"YORK",
"SOUTH TYNESIDE",
"NORTH TYNESIDE",
"THANET DISTRICT",
"CALDERDALE",
"BOLTON",
"BURNLEY",
"BRADFORD",
"BURY",
"HYNDBURN",
"KIRKLEES",
"LONDON PCO",
"MANCHESTER",
"OLDHAM",
"ROCHDALE",
"ROSSENDALE",
"SALFORD",
"STOCKPORT",
"TAMESIDE",
"TRAFFORD",
"WIGAN",
"CARDIFF",
"SLOUGH",
"BELFAST",
"NORTHERN IRELAND",
"GLASGOW",
"EDINBURGH",
"INVERNESS (HIGHLANDS)",
   );

   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
  	  $error 			   =  true;
   }

   
   //6. ACCEPT if "Taxi driver(s)" = Insured Only
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers != 1)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_ACC_INS_ONLY";
      $error                       =  true;
   }
   
   //7. ACCEPT if "Claims last 5 years" = No Claims
   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
   if($claim5Years == "Yes")
   {
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_YES";
      $error               = true;
   }
   
   /* 8.ACCEPT if "Taxi no claims bonus" > 5 years
             AND/OR
        ACCEPT if "Private car no claims bonus" = 5+ Years
   */
   
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   $privateCarNCB = $_SESSION['_YourDetails_']['private_car_ncb'];
   
   if($taxiNCB <= 4)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_ACC_OVR_4";
      $error               = true;
   }
   
   
   //9. ACCEPT if "Type of cover" = Fully Comprehensive
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover != 1)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_ACC_FULL_COMPR"; 
      $error 			   =  true;
   }
   

   /*10. ACCEPT leads MONDAY 0000 -  2359
                   TUESDAY 0000 -  2359
                   WEDNESDAY 0000 -  2359
                   THURSDAY 0000 -  2359
                   FRIDAY 0000 -  1630

    */
   
   if($_SESSION['USER_IP'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMm     = date("i");
      $dateDdText = date("N");

      switch($dateDdText)
      {
         case '1': // Mon
         case '2': // Tue
         case '3': // Wed
         case '4': // Thu
         break;  
        
         case '5': // Fri
         if($timeHh > 16 || ($timeHh == 16 && $timeMm >= 30))
         {
                $personalizedError[] = "SKIP_RULE_DAY_FRY--TIME_SKP_OVR_4:30PM";
                $error               =  true;
         }
         break;

         case '6': // Sat
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT--TIME_SKP_ALL_DAY";
            $error               =  true;
         break;
      
         case '7': // Sun
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SUN--TIME_SKP_ALL_DAY";
            $error               =  true;
         break;
      }
   }
   
   
   
   return $error;
?>

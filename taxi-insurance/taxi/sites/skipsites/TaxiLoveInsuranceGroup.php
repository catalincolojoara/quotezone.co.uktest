<?php

   $error = false;

   
   //ACCEPT if age > 24
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   
   if($propAge <= 24)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_OVR_24";
      $error            =  true;
   }
   
   
   
   //2. Skip if Full UK licence < 2 years
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence < 4)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_2_3_YEARS";
      $error               = true;
   }
   
   

   return $error;
?>

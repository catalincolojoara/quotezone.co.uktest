<?php

   $error = false;

   $postCode     = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesPrefArray = array(
      "KT1",
      "KT2",
      "KT3",
      "KT4",
      "KT5",
      "KT6",
      "KT7",
      "KT8",
      "KT9",
      "KT10",
      "KT11",
      "KT12",
      "KT13",
      "KT14",
      "KT15",
      "KT16",
      "KT17",
      "KT18",
      "KT19",
      "KT21",
      "TW1",
      "TW2",
      "TW3",
      "TW4",
      "TW5",
      "TW6",
      "TW7",
      "TW8",
      "TW9",
      "TW10",
      "TW11",
      "TW12",
      "TW13",
      "TW14",
      "TW15",
      "TW16",
      "TW17",
      "TW18",
      "TW19",
      "TW20",
      "SM1",
      "SM2",
      "SM3",
      "SM4",
      "SM5",
      "SW6",
      "SW13",
      "SW14",
      "SW15",
      "SW20",
      "GU25",
      "W4",
   );

   $postCodePrefix  = $_SESSION['_YourDetails_']['postcode_prefix'];

   if(! in_array($postCodePrefix,$acceptedPcodesPrefArray))
   {
      $personalizedError[] = "SKIP_RULE_POST_PREF--ACC_LIST [$postCodePrefix]";
      $error 			   =  true;
   }

   return $error;

?>

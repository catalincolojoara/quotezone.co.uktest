<?php

   $error = false;

   //Skip postcodes: BT[1-99]*
   $postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BT",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }


   /*SKIP Plating Authority =
         Bolton
         Birmingham
         Blackburn with Darwen
         Bradford
         Calderdale
         Dudley
         Sheffield
         Hyndburn
         Kirklees
         Knowsley
         Luton
         Oldham
         Sandwell
         St Helens
         Wakefield
         Rochdale
         Preston
         Liverpool
         London PCO
         Walsall
         Manchester
         Bury
         Rossendale
   */

   $platingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $skipPlatingAuthiritiesArray = array(
      "BOLTON",
      "BIRMINGHAM",
      "BLACKBURN WITH DARWEN",
      "BRADFORD",
      "CALDERDALE",
      "DUDLEY",
      "SHEFFIELD",
      "HYNDBURN",
      "KIRKLEES",
      "KNOWSLEY",
      "LUTON",
      "OLDHAM",
      "SANDWELL",
      "ST HELENS",
      "WAKEFIELD",
      "ROCHDALE",
      "PRESTON",
      "LIVERPOOL",
      "LONDON PCO",
      "WALSALL",
      "MANCHESTER",
      "BURY",
      "ROSSENDALE",
   );

   if(in_array($platingAuthority,$skipPlatingAuthiritiesArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$platingAuthority]";
      $error                           =  true;
   }


   // skip if age is under 25 years
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error 			   =  true;
   }


   // cannot quote because of number of passengers - only accept if value selected is under 9

   $taxiType     = $_SESSION['_YourDetails_']['taxi_type'];
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiType != 3)//MPV
   {
      if($taxiCapacity == '8')
      {
         $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_8";
         $error                       =  true;
      }
   }
   else
   {
      if($taxiCapacity > '8')
      {
         $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_8";
         $error                       =  true;
      }
   }



   return $error;

?>

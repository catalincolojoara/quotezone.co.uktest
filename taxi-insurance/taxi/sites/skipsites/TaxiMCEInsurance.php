<?php

   $error = false;

   //Only accept taxi plating authority
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $acceptLicensingAuthorityArray = array(
      "REDDITCH",
      "SOUTH NORTHAMPTONSHIRE",
      "NORTHAMPTON",
      "OXFORD",
      "SOUTH OXFORDSHIRE",
      "WEST OXFORDHIRE",
      "WELLINGBOROUGH",
      "BEDFORD",
      "WORCESTER",
      "MILTON KEYNES",
      "CHERWELL",
      "LONDON PCO",//need this for this filter -> IF Taxi Plating Authority = London PCO accept IF "Taxi Use" = Public Hire 
   );

   if(!in_array($licesingAuthority,$acceptLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
      $error               =  true;
   }

   //If 'Taxi Plating Authority' = London PCO ACCEPT if 'Taxi Use' = Public Hire AND 'Taxi no claims bonus' < 3 years
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   $taxiUse = $_SESSION['_YourDetails_']['taxi_used_for'];
   if($licesingAuthority == "LONDON PCO")
   {
      if($taxiUse != '2' || $taxiNCB >= '3')
      {
         $personalizedError[] = "SKIP_RULE_LICENCE_AUTH_USE--PLATING_USE_ACC_PUB_HIRE_AND_NCB_UND_3";
         $error             =  true;
      }
   }


      //ACCEPT if driver age > 29
      $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
      $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
      $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

      $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
      $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
      $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

      $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
      $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

      $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

      if($propAge < 30)
      {
         $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_OVR_29";
         $error                       =  true;
      }


      //2. ACCEPT if "Full UK Licence" = "5 - 6 Years", "6 - 7 years", "7 - 8 years", "8 - 9 years", "9 - 10 years", "Over 10 years",
      $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
      if($ukLicence < 7)
      {
         $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_ACC_OVR_4_5_YEARS";
         $error               = true;
      }

     //SKIP if "Year Of Manufacture" > 10 Years from Quote Date (e.g. skip if vehicle is over 10 years old)
      $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
      $curYear           = date('Y');

      if(($curYear - $yearOfManufacture) > 10)
      {
          $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--AGE_SKP_OVR_10";
          $error           =  true;
      }

   
      
   return $error;
?>
<?php

   $error = false;

   //SKIP if "Taxi plating authority" = Attached SKIP list
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $skipLicensingAuthorityArray = array(
"BIRMINGHAM",
"BLACKBURN WITH DARWEN",
"BOLTON",
"BRADFORD",
"BURY",
"DUDLEY",
"EDINBURGH",
"GLASGOW",
"KNOWSLEY",
"LEEDS",
"LEICESTER",
"LIVERPOOL",
"LONDON PCO",
"LUTON",
"NEWCASTLE UPON TYNE",
"REDCAR & CLEVELAND",
"ROCHDALE",
"SALFORD",
"SEFTON",
"SLOUGH",
"SOLIHULL",
"SOUTH RIBBLE",
"STOCKPORT",
"STOCKTON ON TEES",
"SUNDERLAND",
"THREE RIVERS",
"TRAFFORD",
"WAKEFIELD",
"WALSALL",
"WARRINGTON",
"WATFORD",
"WEST LANCASHIRE",
"WIGAN",
"WIRRALL",
"DVA (NI)",
"NORTHERN IRELAND",
"LONDON PCO",
   );

   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error            =  true;
   }

   //SKIP if Proposer Age > 69
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   
   if($propAge > 69)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_OVR_69";
      $error            =  true;
   }

   //SKIP if "Private car no claims bonus" < 3 Years
   $privateCarNCB = $_SESSION['_YourDetails_']['private_car_ncb'];

   if($privateCarNCB < 3)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_1_YEAR_2";
      $error                       =  true;
   }

   //SKIP if "Taxi badge" < 2 Years
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge <= 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_2_YEARS";
      $error               = true;
   }

   // SKIP if "Max. number of passengers" > 7
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity > '7')
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_7";
      $error            =  true;
   }


   // ACCEPT leads MONDAY - FRIDAY (ALL DAY)
   // ACCEPT leads SATURDAY 1300 - 2000
   // ACCEPT leads SUNDAY 1300 - 2200
   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeMin    = date("i");
      $timeHh     = date("G");
      $dateDdText = date("N");

      switch($dateDdText)
      {
         case '1':
         case '2':
         case '3':
         case '4':
         case '5':
         break;   

         case '6':
         if($timeHh < 13  || $timeHh >= 20)
         {
             $personalizedError[] = "SKIP_RULE_DAY_SUN--TIME_ACC_BETW_13:00PM_TO_8:00PM";
             $error               =  true;
         }
         break;

         case '7':
         if($timeHh < 13  || $timeHh >= 22)
         {
             $personalizedError[] = "SKIP_RULE_DAY_SAT--TIME_ACC_BETW_13:00PM_TO_10:00PM";
             $error               =  true;
         }
         break;
      }
   }

   
   //SKIP if "Year of manufacture" > 10 Years from Quote Date (e.g. skip if vehicle is over 10 years old)
    
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   $curYear           = date('Y');

   if(($curYear - $yearOfManufacture) > 10)
   {
       $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--AGE_SKP_OVR_10";
       $error           =  true;
   }


   // SKIP if Postcode = BT[0-99]*
   $postCode     = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BT",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error               = true;
   }
   
   
   
   return $error;

?>

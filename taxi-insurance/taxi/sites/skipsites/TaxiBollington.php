<?php

   $error = false;

   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 5)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_2_3_YEARS";
      $error               = true;
   }

   // skip if driver age < 30
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 30 || $propAge > 65)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_30_AND_OVR_65";
      $error               =  true;
   }

   // skip if Year of Manufacture > 8 Years Old
   $vehicleManufYear = $_SESSION["_YourDetails_"]["year_of_manufacture"];
   $disYear          = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"];

   $vehicleAge = $disYear - $vehicleManufYear;

   if($vehicleAge > 8)
   {
      $personalizedError[] = "SKIP_RULE_VEH_AGE--AGE_SKP_OVR_8";
      $error               =  true;
   }

   // Accept leads from Monday over 9am until Friday under 5pm
   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {
         case '6': // Sat
         case '7': // Sun
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
            $error                           =  true;
            break;

         case '1': // Mon
            if($timeHh < 9)
            {
               $personalizedError[] = "SKIP_RULE_DAY_MON--TIME_ACC_MON_OVR_9:00AM_FRI_SKP_OVR_17:00_SKP_SAT_SUN";
               $error                               =  true;
            }
         break;
         case '2': // Tue
         break;
         case '3': // Wed
         break;
         case '4': // Thu
         break;
         case '5': // Fri
            if($timeHh >= 17)
            {
               $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_MON_OVR_9:00AM_FRI_SKP_OVR_17:00_SKP_SAT_SUN";
               $error                               =  true;
            }
         break;
      }
   }

   //Skip if Max. Number of Passengers  8
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity == 8)
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_8";
      $error                       =  true;
   }

   //Skip if Full UK Licence < 9 -10 years
   $period_of_licence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($period_of_licence < 11)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_9_10_YEARS";
      $error                       =  true;
   }


   //Skip the following list of postcodes: BT[1-99]*
   $postCode       = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BT",     
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error               =  true;
   }

//   only accept Plating Authority from 
      $platingAuthorities = $_SESSION['_YourDetails_']['plating_authority'];  
      $platingAuthorities = strtoupper($platingAuthorities);
      $platingAuthorities = trim($platingAuthorities);

      $platingAuthoritiesArray = array(
            "ALLERDALE",
            "AMBER VALLEY",
            "ARUN",
            "ASHFIELD",
            "BABERGH",
            "BARROW IN FURNESS",
            "BATH & NE SOMERSET",
            "BERWICK ON TWEED",
            "BLACKPOOL",
            "BOSTON",
            "BOURNEMOUTH",
            "BRAINTREE",
            "BRECKLAND",
            "BRIDGENORTH",
            "BRIGHTON & HOVE",
            "BROADLAND",
            "CANTERBURY",
            "CARADON",
            "CARLISLE",
            "CASTLE MORPETH",
            "CASTLE POINT",
            "CHELTENHAM",
            "CHESHIRE EAST",
            "CHESTER",
            "CHESTER",
            "CHRISTCHURCH",
            "CONGLETON",
            "COPELAND",
            "CORBY",
            "COTSWOLD",
            "CRAVEN",
            "CREWE & NANTWICH",
            "DARLINGTON",
            "DAVENTRY",
            "DERBYSHIRE DALES",
            "DERWENTSIDE",
            "DONCASTER",
            "DURHAM",
            "EAST CAMBRIDGESHIRE",
            "EAST DEVON",
            "EAST DORSET",
            "EAST HAMPSHIRE",
            "EAST LINDSEY",
            "YORK",
            "STAFFORD",
            "EASTLEIGH",
            "EDEN",
            "EXETER",
            "FENLAND",
            "FOREST HEATH",
            "FOREST OF DEAN",
            "FYLDE",
            "GLOUCESTER",
            "GRAVESHAM",
            "GUERNSEY",
            "HAMBLETON",
            "HARBOROUGH",
            "HARROGATE",
            "HIGH PEAK",
            "HUNTINGDONSHIRE",
            "IPSWICH",
            "ISLE OF ANGLESEY",
            "ISLE OF MAN",
            "ISLE OF WIGHT",
            "KINGSTON UPON HULL",
            "KINGS LYNN & W NORFOLK",
            "LANCASTER",
            "LICHFIELD",
            "LINCOLN",
            "MACCLESFIELD",
            "MALVERN HILLS",
            "MENDIP",
            "MID DEVON",
            "MID SUFFOLK",
            "NEWCASTLE UNDER LYME",
            "NEW FOREST",
            "NORTH CORNWALL",
            "NORTH DEVON",
            "NORTH DORSET",
            "NORTH EAST LINCOLNSHIRE",
            "NORTH LICOLNSHIRE",
            "NORTH NORFOLK",
            "NORTH SOMERSET",
            "NORTH WILTSHIRE",
            "YORK",
            "NORTHUMBERLAND",
            "NORWICH",
            "PENWITH",
            "PETERBOROUGH",
            "POOLE",
            "PURBECK",
            "RICHMONDSHIRE",
            "ROCHFORD",
            "ROTHER",
            "RYEDALE",
            "SALISBURY",
            "SCARBOROUGH",
            "SEDGEMOOR",
            "SELBY",
            "SOUTH CAMBRIDGE",
            "SOUTH DERBYSHIRE",
            "SOUTH HAMS",
            "SOUTH HOLLAND",
            "SOUTH KESTEVEN",
            "SOUTH LAKELAND",
            "SOUTH NORFOLK",
            "SOUTH NORTHANTS",
            "SOUTH OXFORDSHIRE",
            "SOUTH SOMERSET",
            "STAFFORD",
            "SOUTHAMPTON",
            "SOUTHEND-ON-SEA",
            "ST EDMUNDSBURY",
            "GUERNSEY",
            "SUFFOLK COASTAL",
            "TAUNTON DEANE",
            "TEESDALE",
            "TEIGNBRIDGE",
            "TEST VALLEY",
            "TEWKESBURY",
            "THAMESDOWN",
            "TORBAY",
            "TORFAEN",
            "TORRIDGE",
            "TUNBRIDGE WELLS",
            "TYNEDALE",
            "UTTLESFORD",
            "VALE OF WHITE HORSE",
            "WANSBECK",
            "WAVENEY",
            "WEST DEVON",
            "WEST DORSET",
            "WEST LINDSEY",
            "WEST OXFORD",
            "WEST SOMERSET",
            "WEST WILTSHIRE",
            "WEYMOUTH & PORTLAND",
            "WILTSHIRE",
            "WINCHESTER",
            "WORCESTER",
            "WYCHAVON",
            "WYRE",
            "WYRE FORREST",
            "YORK",
      );
   
     if(!in_array($platingAuthorities, $platingAuthoritiesArray))
     {
        $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST";
        $error                       =  true;
     }
      
   return $error;

?>

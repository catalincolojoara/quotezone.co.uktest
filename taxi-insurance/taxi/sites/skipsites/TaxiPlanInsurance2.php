<?php

   $error = false;

   $wlID = $_SESSION['_QZ_QUOTE_DETAILS_']['wlUserID'];
   if($wlID == "758")
   {
      $personalizedError[] = "SKIP_RULE_COMPANY_WL--WL_SKP_DNAINSURANCE2";
      $error               =  true;
   }

   $taxiType        = $_SESSION['_YourDetails_']['taxi_type'];
   $taxiUse         = $_SESSION['_YourDetails_']['taxi_used_for'];
   $taxiCapacity    = $_SESSION['_YourDetails_']['taxi_capacity'];
   $estimated_value = $_SESSION['_YourDetails_']['estimated_value'];

   //1. SKIP LEADS FROM the postcodes in the attached list
   $postCode        = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = strtoupper($postCodeArr[1]);

   $skipPcodesArray = array(
      "B",
      "BB",
      "BD",
      "BL",
      "BT",
      "CH",
      "E",
      "HX",
      "L",
      "LS",
      "LU",
      "M",
      "OL",
      "PR",
      "WD",
      "WN",
   );

   if( in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error 			   =  true;
   } 

   /// THIS FILTER SHOULD WORK ONLY FOR OUR IP 
//   if($_SESSION["USER_IP"] == "86.125.114.56")
//   {
      $taxiType = $_SESSION['_YourDetails_']['taxi_type'];
      if($taxiType == 1)
      {
         $personalizedError[] = "SKIP_RULE_VEH_TYP--TYP_SKP_BLACK_CAB";
         $error            =  true;
      }
//   }

   //6.Skip if Type of Cover = Thrid party fire & theft or Third party
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover != 1)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_SKP_TPO_TPFT";
      $error 			   =  true;
   }

   // ACCEPT leads only Monday - Friday 09:00 - 17:30
   $timeHh     = date("G");
   $timeMm     = date("i");
   $dateDdText = date("N");
if($_SERVER["REMOTE_ADDR"] != "86.125.114.56" && $_SERVER["REMOTE_ADDR"] != "109.234.194.242")
   {
   if($dateDdText > 5 || $timeHh < 9 || $timeHh > 17 || ($timeHh == 17 && $timeMm >= 30))
   {
      $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_5:30PM";
      $error                       =  true;
   }
   }
   
   // SKIP if "Age" < 28  > 70
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 28 || $propAge > 70)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_BETW_28_AND_70";
      $error            =  true;
   }
   
   
   // SKIP if "Full UK licence" = "1 - 2 Years", "2 - 3 Years", "3 - 4 Years", "4 - 5 Years"
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence < 7)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_5_YEARS";
      $error               =  true;
   }
   
   
   // SKIP if "Taxi badge" = "No Badge", "Under 6 Months", "6 Months To 1 Year"
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_YEAR";
      $error               = true;
   }
   
   
   //SKIP if "Taxi no claims bonus" = "No NCB"
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($taxiNCB < 1)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_1_YEAR";
      $error               =  true;
   }
   
   
   // SKIP if "Year of manufacture" < 2006 (E.g SKIP if 2005, 2004, 2003, ..., 1990, etc...)
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];

   if($yearOfManufacture < 2006)
   {
      $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--YEAR_SKP_UND_2006"; 
      $error               =  true;
   }
   
   
   // SKIP if "Estimated taxi value (£)" < 5,000  
   $estimated_value = $_SESSION['_YourDetails_']['estimated_value'];   
   if($estimated_value < 5000)
   {
      $personalizedError[] = "SKIP_RULE_VEH_VALUE--POUND_SKP_UND_5000";
      $error               = true;
   }                                                

   
   // SKIP if "Taxi driver(s)" = "Insured and 2+ Named Drivers"
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers == 3)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_SKP_INS_AND_2+_NAM_DRV";
      $error               =  true;
   }                                    


   // SKIP if "Max. number of passengers" > 7
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity > '7')
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_7";
      $error            =  true;
   }
      

   return $error;

?>

<?php

   $error = false;

   $postCode     = $_SESSION['_YourDetails_']['postcode'];
   $taxiType     = $_SESSION['_YourDetails_']['taxi_type'];
   $taxiUse      = $_SESSION['_YourDetails_']['taxi_used_for'];



   //   2. Skip if Claims Last 5 Years = Yes, Claims Made
//   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
//   if($claim5Years == "Yes")
//   {
//      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_YES";
//      $error 			   =  true;
//   }

   
   //   4. Skip if Year Of Manufacture > 10 years old e.g. Skip if Year Of Manufacture = 2000,1999 etc
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   $curYear           = date('Y');

   if(($curYear - $yearOfManufacture) > 10)
   {
       $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--AGE_SKP_OVR_10";
       $error 			   =  true;
   }


   // 5.Skip if Driver age < 27 years old and > 65 years old
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);


   if($propAge < 27 || $propAge > 65)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_BETW_27_AND_65";
      $error 			   =  true;
   }


   //   6. Skip if Taxi Plating Authority = any from the attached list
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $skipLicensingAuthorityArray = array(
      "NORTHERN IRELAND",
      "BELFAST",
      "SEFTON",
      "KNOWSLEY",
      "ST HELENS",
      "HALTON",
      "BOLTON",
      "BLACKBURN WITH DARWEN",
      "HYNDBURN",
      "ROSSENDALE",
      "BURNLEY",
      "BURY",
      "ROCHDALE",
      "TRAFFORD",
      "SALFORD",
      "TAMESIDE",
      "CALDERDALE",
   );

   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error 			   =  true;
   }

  
   //   7. Only receive leads from the following postcodes: TN[1-99]*, ME[1-99]*, BN[1-99]*, CT[1-99]*
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
      "BN",
      "CT",
      "GU",
      "ME",
      "RH",
      "TN",
      "BH",
      "DT",
      "SO",
      "PO",
      "GU",
      "PL",
   );
  
   if(! in_array($postcodeToCheck,$acceptedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error 			   =  true;
   }

   
   // Can quote Monday-Friday - accept between the hours 9:00am to 6:00pm
   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdText = date("N");

      if($dateDdText <= 5 && ($timeHh < 9 || $timeHh >= 18 ))
      {
         $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_6:00PM";
         $error =  true;
      }

      if($dateDdText == 6)
      {
            $personalizedError[] = "SKIP_RULE_DAY_SAT--TIME_SKP_ALL_DAY";
            $error               =  true;
      }

      if($dateDdText == 7)
      {
         $personalizedError[] = "SKIP_RULE_SKP_DAY_SUN--TIME_SKP_ALL_DAY";
         $error =  true;
      }
   }

   
   //Skip if Taxi badge < 1 year
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_YEAR";
      $error               = true;
   }


   //cannot quote because of the vehicle driver status - accept only if the value selected is 'Insured Only' or 'Insured +1 Named Driver' or 'Insured +2 Named Drivers'
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers > 3)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_ACC_INS_ONLY_INS_NAMED_2+_NAM_DRV";
      $error                       =  true;
   }

   return $error;

?>

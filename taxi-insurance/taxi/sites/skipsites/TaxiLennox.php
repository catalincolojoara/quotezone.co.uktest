<?php

   $error = false;

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   // Skip if Driver age < 25 years old
   if($propAge < 25)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error               = true;
   }

   // Skip if Taxi No Claims Bonus = 4 years or 5+ years
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   //if( $taxiNCB == 4 OR $taxiNCB == 5 )
   //{
   //   $personalizedError[] = "Skip if Taxi No Claims Bonus = 4 years or 5+ years.<br>";
   //   $error               = true;
   //}
   
   if($taxiNCB == 4)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_4_YEARS";
      $error               =  true;
   }
   
   if($taxiNCB == 5)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_5+_YEARS";
      $error               =  true;
   }

   // Only receive leads from the following postcodes:BT[1-35]*,BT[37-46]*,BT[49-99]*
   $postCode       = $_SESSION['_YourDetails_']['postcode'];
   $postCodePrefix = $_SESSION['_YourDetails_']['postcode_prefix'];

   $accPcodesPrefArray = array(
      //BT[1-35]*
      "BT1", "BT2", "BT3", "BT4", "BT5", "BT6", "BT7", "BT8", "BT9", "BT10", "BT11", "BT12", "BT13", "BT14", "BT15", "BT16", "BT17", "BT18", "BT19", "BT20", "BT21", "BT22", "BT23", "BT24", "BT25", "BT26", "BT27", "BT28", "BT29", "BT30", "BT31", "BT32", "BT33", "BT34", "BT35",
      //BT[37-46]*
      "BT37", "BT38", "BT39", "BT40", "BT41", "BT42", "BT43", "BT44", "BT45", "BT46",
      //BT[49-99]*
      "BT49", "BT50", "BT51", "BT52", "BT53", "BT54", "BT55", "BT56", "BT57", "BT58", "BT59", "BT60", "BT61", "BT62", "BT63", "BT64", "BT65", "BT66", "BT67", "BT68", "BT69", "BT70", "BT71", "BT72", "BT73", "BT74", "BT75", "BT76", "BT77", "BT78", "BT79", "BT80", "BT81", "BT82", "BT83", "BT84", "BT85", "BT86", "BT87", "BT88", "BT89", "BT90", "BT91", "BT92", "BT93", "BT94", "BT95", "BT96", "BT97", "BT98", "BT99",
   );

   if(!in_array($postCodePrefix,$accPcodesPrefArray))
   {
      $personalizedError[] = "SKIP_RULE_POST_PREF--ACC_LIST [$postCodePrefix]";
      $error               = true;
   }

   return $error;

?>

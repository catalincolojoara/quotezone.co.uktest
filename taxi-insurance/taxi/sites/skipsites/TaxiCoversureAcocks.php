<?php

   $error = false;

   //1.skip if Year of Manufacture > 9 years old
    $vehicleManufYear = $_SESSION["_YourDetails_"]["year_of_manufacture"];
    $disYear          = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"];
    $vehicleAge = $disYear - $vehicleManufYear;

    if($vehicleAge > 9)
    {
       $personalizedError[] = "SKIP_RULE_TYP_COV--AGE_SKP_OVR_9";
       $error                       =  true;
    }
   
  

   // only accept Plating Authority;
   $LicensingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $licesingAuthorityArray= array(
"BRIDGENORTH",
"BROMSGROVE",
"DUDLEY",
"SANDWELL",
"WALSALL",
"WORCESTER",
"REDDITCH",
"WYCHAVON",
"COVENTRY",
"NORTH WARWICKS",
"WARWICK",

);

   if(! in_array($LicensingAuthority,$licesingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
      $error 			   =  true;
   }

   //3. skip if Taxi No Claims Bonus = No NCB, 1 Year
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB < 2)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB_1YEAR";
      $error                       =  true;
   }

   // ACCEPT Monday - Friday 09:00 - 17:00
   if($_SESSION['USER_IP'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMm     = date("i");
      $dateDdText = date("N");

      if($dateDdText > 5 || $timeHh < 9 || $timeHh >= 17)
      {
         $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_5:00PM";
         $error               = true;
      }
   }
   
   // only accept postcodes B[0-99]*
   $postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
      "B",
   );

   if(! in_array($postcodeToCheck,$acceptedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error            =  true;
   }
   
   
   
   return $error;
?>

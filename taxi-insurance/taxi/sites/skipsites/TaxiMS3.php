<?php

   $error = false;


   //1. Skip if full UK license is < 5 years
   $fullLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($fullLicence <= 6)
   {
      $personalizedError[] = "SKIP_RULE_DRIVING_LICENCE--YEARS_INT_SKP_UND_5_YEARS";
      $error               = true;
   }
   
   //2. Skip if Taxi NCB < 2 years
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($taxiNCB < 2)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_2_YEARS";
      $error 			   =  true;
   }
   
   //3. Skip if Taxi Badge < 2 years
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge <= 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_2_YEARS";
      $error               = true;
   }
   
   //4.Only accept the following postcodes:  OX[0-99]* HP[0-99]*
   $postCode     = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
      "OX",
      "HP", 
   );

   if(! in_array($postcodeToCheck,$acceptedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error 			   =  true;
   }
   
   //5.Only accept Age range 30/70
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 30 || $propAge > 70)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_30_AND_OVR_70";
      $error               =  true;
   }
   
   return $error;

?>

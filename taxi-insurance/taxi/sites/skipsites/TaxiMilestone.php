<?php 

   $error = false;

   $postCode     = $_SESSION['_YourDetails_']['postcode'];
   

   /*
      ACCEPT leads Monday 08:00 - 17:00
                  Tuesday 08:00 - 17:00
                  Wednesday 08:00 - 17:00
                  Thursday 08:00 - 17:00
                  Friday 08:00 - 17:00
                  Saturday NO LEADS
                  Sunday NO LEADS
               
   */
   
   

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {
         case '1':
         case '2':
         case '3':
         case '4':
         case '5':
         if($timeHh < 8 || $timeHh >= 17)
         {
            $personalizedError[] = "SKIP_RULE_SKP_DAY_MON_FRI--TIME_ACC_BETW_8:00AM_TO_5:00PM";
            $error               =  true;
         }
         break;
         case '6': 
         case '7':
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
            $error               =  true;
         break;
      }
   }



   //1.Only the following plating authorities are acceptable.
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $accLicensingAuthorityArray = array(
"HARROGATE",
   );

   if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
  	  $error 			   =  true;
   }

//    //1. Milestone will get leads only for this postcode
//    preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
//    $postcodeToCheck = $postCodeArr[1];
// 
//    $okPcodesArray = array(
//       "LS",
//       "BD",
//       "WF"
//    );
// 
//    if(! in_array($postcodeToCheck,$okPcodesArray))
//    {
//       $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
//       $error 			   =  true;
//    }


//    $drNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
// 
//    // 3.Milestone accepts leads for drivers that have minimum 1 year ncb.
//    if($drNCB == 0)
//    {
//       $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
//       $error 			   =  true;
//    }


   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   //4. Milestone will not get leads for users aged under 25 or over 69.
   if($propAge < 21 || $propAge > 69)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_21_AND_OVR_69";
      $error 			   =  true;
   }

//    //If user age > 29 cannot quote because of the taxi badge - skip if the value is under 2-3 years
//    $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
//    
//    if($propAge > 29)
//    {
//       if($taxiBadge < 5)
//       {
//          $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_2_3_YEARS_AGE_OVR_29";
//          $error            =  true;
//       }
//    }

   //please update this plugin so that they do not receive leads that come from SMS renewals WR50-5B2
   if($_SESSION["_QZ_QUOTE_DETAILS_"]["SERVER_IP"] == "192.168.12.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   //please update this plugin so that they do not receive leads that come from SMS renewals WR50-5B2 - second check
   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }



	//SKIP if "Full UK licence" = "1 - 2 Years"
   $periodOfFullUkLicenceHeld = $_SESSION["_YourDetails_"]["period_of_licence"];

   $filteredPeriodOfFullUkLicenceHeldArray = array(
//       "1" => "Under 6 Months",
//       "2" => "6 Months To 1 Year",
      "3" => "1 - 2 Years",
//       "4" => "2 - 3 Years",
//       "5" => "3 - 4 Years",
//       "6" => "4 - 5 Years",
      );

   if(array_key_exists($periodOfFullUkLicenceHeld,$filteredPeriodOfFullUkLicenceHeldArray))
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--SKP_LIST [".$filteredPeriodOfFullUkLicenceHeldArray[$periodOfFullUkLicenceHeld]."]";
      $error            =  true;
   }

	//SKIP if "Year of manufacture" < "Current Year" - 10 Years
   $vehicleManufYear = $_SESSION["_YourDetails_"]["year_of_manufacture"];
   $disYear          = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"];
   $vehicleAge = $disYear - $vehicleManufYear;
   if($vehicleAge > 10)
   {
      $personalizedError[] = "SKIP_RULE_VEH_AGE--AGE_SKP_OVR_10";
      $error            =  true;
   }

   return $error;

?>

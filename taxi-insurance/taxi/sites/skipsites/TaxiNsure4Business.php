<?php

   $error = false;


//    ACCEPT leads Monday 00:00 - 23:59
//                            Tuesday 00:00 - 23:59
//                      Wednesday 00:00 - 23:59
//                         Thursday 00:00 - 23:59
//                               Friday 00:00 - 19:00
//                            Saturday NO LEADS
//                            Sunday 19:00 - 23:59


  if($_SESSION['USER_IP'] != "86.125.114.56")
  {
     $timeHh     = date("G");
     $timeMin    = date("i");
     $dateDdNum  = date("N");

     switch($dateDdNum)
     {
        case '6': // Sat
               $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT--TIME_SKP_ALL_DAY";
               $error                           =  true;
           break;

        case '7': // Sun
           if($timeHh < 19)
           {
               $personalizedError[] = "SKIP_RULE_DAY_SUN--TIME_SKP_UND_7:00PM";
               $error                           =  true;
           }
           break;

        case '1': // Mon
        case '2': // Tue
        case '3': // Wed
        case '4': // Thu
        break;


        case '5': // Fri
           if($timeHh >= 19)
           {
              $personalizedError[] = "SKIP_RULE_DAY_FRI--TIME_SKP_OVR_7:00PM";
              $error                               =  true;
           }
        break;
     }
  }

   //SKIP if Postcode = BT[0-99]*
   $postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BT",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }


   //SKIP if "Taxi Type" = "MPV"
   $taxiType = $_SESSION['_YourDetails_']['taxi_type'];
   if($taxiType == 3)
   {
      $personalizedError[] = "SKIP_RULE_VEH_TYP--TYP_SKP_MPV";
      $error            =  true;
   }


   // SKIP if "Taxi No Claims Bonus" > 2 years
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($taxiNCB >= 3)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_OVR_2_YEARS";
      $error               =  true;
   }
  

//   SKIP if "Insurance Start Date" > "Quote Date" + 13 Days
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   // today date
   $day   = date("d");
   $month = date("m");
   $year  = date("Y");

   $daysOfInsuranceStart = (mktime(0,0,0,$DISmm,$DISdd,$DISyyyy) - mktime(0,0,0,$month,$day,$year)) / (86400);

   if($daysOfInsuranceStart > 13)
   {
      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_SKP_13_DAY_AFT";
      $error                       =  true;
   }
   
//   SKIP if "Age" < 23 
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 23)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_23";
      $error               =  true;
   }
   
//   SKIP if "Max. Number of Passengers" > 4
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity > 4)
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_4";
      $error                       =  true;
   }
   
//   ACCEPT if "Taxi Driver(s)" = Insured Only
    
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers != 1)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_ACC_INS_ONLY";
      $error                       =  true;
   }
   
   
   return $error;

?>
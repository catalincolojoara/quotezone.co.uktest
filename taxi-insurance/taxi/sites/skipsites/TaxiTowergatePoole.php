<?php

   $error = false;
 
   //1. Towergate Nottingham will get leads only for this postcode
   $postCode        = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
      "TR",
      "PL",
      "TQ",
      "DT",
      "BH",
      "SP",
      "SO",
      "PO",
   );
  
   if(! in_array($postcodeToCheck,$acceptedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error 			   =  true;
   }

   //2.Skip if age > 60. 60 is acceptable. 
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge > 60)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_OVR_60";
      $error 			   =  true;
   }

   //3. Towergate Poole accept leads only between 09 am and 16:30 pm Monday – Friday.
   $timeMin    = date("i");
   $timeHh     = date("G");
   $dateDdText = date("N");

   if($dateDdText > 5 || $timeHh < 9 || $timeHh > 16 || ($timeHh == 16 && $timeMin > 30))
   {
      $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_4:30PM";
      $error 			   =  true;
   }


   return $error;

?>

<?php

   $error = false;

   /*only ACCEPT postcode =

      WV[0-99]*
      DY[0-99]*
      TF[0-99]*
      SY[0-99]*
      ST[0-99]*

      B[64-98]
   */

   $postCode       = $_SESSION['_YourDetails_']['postcode'];
   $postCodePrefix = $_SESSION['_YourDetails_']['postcode_prefix'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
      "WV",
      "DY",
      "TF",
      "SY",
      "ST",
   );

   $accPcodesPrefixArray = array(
      "B64",
      "B65",
      "B66",
      "B67",
      "B68",
      "B69",
      "B70",
      "B71",
      "B72",
      "B73",
      "B74",
      "B75",
      "B76",
      "B77",
      "B78",
      "B79",
      "B80",
      "B81",
      "B82",
      "B83",
      "B84",
      "B85",
      "B86",
      "B87",
      "B88",
      "B89",
      "B90",
      "B91",
      "B92",
      "B93",
      "B94",
      "B95",
      "B96",
      "B97",
      "B98",
   );

   if((! in_array($postCodePrefix,$accPcodesPrefixArray)) && (! in_array($postcodeToCheck,$acceptedPcodesArray)))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error                        =  true;
   }


   // skip if value selected is "1-2 Years"
   $fullLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($fullLicence == 3)
   {
      $personalizedError[] = "SKIP_RULE_DRIVING_LICENCE--YEARS_INT_SKP_1_2_YEARS";
      $error               = true;
   }

   // skip if User Age < 25
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   
   if($propAge < 25)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error            =  true;
   }


   /*only ACCEPT licensing authority =

      Wolverhampton
      Dudley
      Sandwell
      Telford & Wrekin
      North Shropshire
      South Shropshire
      South Staffordshire
      Wyre Forrest

   */
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $accpLicensingAuthorityArray = array(
   "WOLVERHAMPTON",
   "DUDLEY",
   "SANDWELL",
   "TELFORD & WREKIN",
   "NORTH SHROPSHIRE",
   "SOUTH SHROPSHIRE",
   "SOUTH STAFFORDSHIRE",
   "WYRE FORREST",
      );

   if(! in_array($licesingAuthority,$accpLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
      $error               =  true;
   }

   // SKIP if Taxi Driver(s) = Insured and 2+ Named Drivers
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers == 3)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_SKP_INS_AND_2+_NAM_DRV";
      $error                       =  true;
   }

   // SKIP if Max. number of passengers > 9
   $numberOfPassengers = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($numberOfPassengers >  9 )
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_9";
      $error                      =  true;
   }

   /* Monday - accept all day
      Tuesday - accept all day
      Wednesday - accept all day
      Thursday - accept all day
      Friday - SKIP hours over 5pm
      Saturday - SKIP all day
      Sunday - SKIP all day
   */


   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");


      switch($dateDdNum)
      {
         case '5': // Fri
            if($timeHh >= 17)
            {
               $personalizedError[] = "SKIP_RULE_SKP_DAY_FRI--TIME_SKP_OVR_5:00PM";
               $error               =  true;
            }
         break;

         case '6': // Sat
         case '7': // Sun
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
            $error               =  true;
         break;

      }
   }

   // skip leads from .13 server
   if($_SESSION["_QZ_QUOTE_DETAILS_"]["SERVER_IP"] == "192.168.12.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }


   return $error;

?>

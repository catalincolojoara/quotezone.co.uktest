<?php

   $error = false;

   // MCE will receive leads only for this postcodes.
   $postCode     = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
      "NN",
      "MK",
      "ST",
      "SL",
   );

   if(! in_array($postcodeToCheck,$acceptedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error 			   =  true;
   }

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   // 2. MCE will not accept leads for users aged under 25.
   if($propAge < 25)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error 			   =  true;
   }

   // 3. MCE accepts leads for drivers that have minimum 1 year ncb.
   $drNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($drNCB == 0)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
      $error 			   =  true;
   }

   // 4. MCE accepts leads for drivers that have minimum 1 year Taxi Badge
   $badge = $_SESSION['_YourDetails_']['taxi_badge'];

   if($badge < 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_2_YEARS";
      $error 			   =  true;
   }

   
   //Only the following plating authorities are acceptable.
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $accLicensingAuthorityArray = array(
"RUTLAND",
"CAMBRIDGE",
"SOUTH CAMBRIDGE",
"SEVENOAKS",
"SOUTH BUCKINGHAM",
"WOKINGHAM",
"ST EDMUNDSBURY",
"CENTRAL BEDFORDSHIRE",
"BURY",
"GEDLING",
   );

   if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
     $error             =  true;
   }
   
   // SKIP if "Year Of Manufacture" > 10 Years from Quote Date (e.g. skip if vehicle is over 10 years old)
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   $curYear           = date('Y');

   if(($curYear - $yearOfManufacture) > 10)
   {
       $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--AGE_SKP_OVR_10";
       $error           =  true;
   }

   
   
   return $error;

?>

<?php

   $error = false;


   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   //ACCEPT if Age > 29
   if($propAge <= 29)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_OVR_29";
      $error            =  true;
   }

   //Skip if Max. Number of Passengers > 8
   $taxiType     = $_SESSION['_YourDetails_']['taxi_type'];
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiType != 3)//MPV
   {
      if($taxiCapacity == '8')
      {
         $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_8";
         $error                       =  true;
      }
   }
   else
   {
      if($taxiCapacity > '8')
      {
         $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_8";
         $error                       =  true;
      }
   }


   //SKIP if "Taxi No Claims Bonus" = "No NCB"

   $taxiNCB    = $_SESSION['_YourDetails_']['taxi_ncb'];   
   if($taxiNCB == 0)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
      $error               =  true;
   }


   //ACCEPT if "Full UK Licence" >=3 years
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence < 5)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_3_4_YEARS";
      $error               = true;
   }

   //ACCEPT if "Taxi Badge" >=2 Years
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 5)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_2_3_YEARS";
      $error               = true;
   }

   //ACCEPT if "Taxi Plating Authority" = London PCO
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $accLicensingAuthorityArray = array(
      "LONDON PCO",
   );

   if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
      $error               =  true;
   }

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      //ACCEPT leads Monday - Friday 10:30 - 16:30
      $timeMin    = date("i");
      $timeHh     = date("G");
      $dateDdText = date("N");

      if($dateDdText > 5 || $timeHh < 9 || $timeHh > 18 || ($timeHh == 9 && $timeMin < 30) || ($timeHh == 18 && $timeMin >= 30))
      {
         $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:30AM_TO_6:30PM";
         $error                       =  true;
      }
   }
   
//   ACCEPT if Insurance Start Date > 2 Days from Quote date
   
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   // today date
   $day   = date("d");
   $month = date("m");
   $year  = date("Y");

   $daysOfInsuranceStart = (mktime(0,0,0,$DISmm,$DISdd,$DISyyyy) - mktime(0,0,0,$month,$day,$year)) / (86400);

   if($daysOfInsuranceStart <= 2 )
   {
      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_ACC_2_DAY_AFT";
      $error                       =  true;
   }


   return $error;
?>

<?php

   $error = false;

   
   /*ACCEPT leads Monday 00:00 - 09:00 and 17:30 - 23:59
                         Tuesday 00:00 - 09:00 and 17:30 - 23:59
                    Wednesday 00:00 - 09:00 and 17:30 - 23:59
                        Thursday 00:00 - 09:00 and 17:30 - 23:59
                             Friday 00:00 - 09:00 and 17:30 - 23:59
                         Saturday 00:00 - 23:59
                           Sunday 00:00 - 23:59*/                           
   if($_SERVER["REMOTE_ADDR"] != "86.125.114.56" && $_SERVER["REMOTE_ADDR"] != "109.234.194.242")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdText = date("N");

      switch($dateDdText)
      {
         case '6': 
         case '7':  
         break;
         
         case '1':  
         case '2':  
         case '3':  
         case '4':  
         case '5':  
               if(($timeHh > 8 && ($timeHh == 17 && $timeMin < 30)) || ($timeHh > 8 && $timeHh < 17))
               {
                  $personalizedError[] = "SKIP_RULE_SKP_DAY_MON_FRI--TIME_SKP_BETW_9:00AM_TO_5:30PM";
                  $error =  true;
               }
            break;
      }
   }                           

   
   //SKIP if "Age" < 34 > 75
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 34 || $propAge > 75)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--SKP_LIST ['under 34 or over 75']";
      $error               =  true;
   }
   
      
   //ACCEPT only if "Taxi type" = "Black Cab"
   $taxiType = $_SESSION['_YourDetails_']['taxi_type'];
   if($taxiType != 1)
   {
      $personalizedError[] = "SKIP_RULE_VEH_TYP--TYP_ACC_BLACK_CAB";
      $error            =  true;
   }
  

   //ACCEPT only if "Taxi plating authority" = "London PCO"
   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];
   $acceptedLicensingAuthorityArray = array(
      "LONDON PCO",
   );

   
   if(! in_array($licesingAuthority,$acceptedLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
      $error                           =  true;
   }

   
   // SKIP if "Taxi use" = "Private Hire"
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse == 1)
   {
      $personalizedError[] = "SKIP_RULE_USE--USE_SKP_PRIV_HIRE";
      $error               =  true;
   }
   
      
   // SKIP if "Taxi badge" = "No Badge", "Under 6 Months", "6 Months To 1 Year"
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_2_YEARS";
      $error               = true;
   }                                 
   
      
   return $error;

?>

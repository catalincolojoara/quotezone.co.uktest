<?php


   $error = false;

   $postCode     = $_SESSION['_YourDetails_']['postcode'];

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   // today date
   $day   = date("d");
   $month = date("m");
   $year  = date("Y");

   // 1. Insurance start date no more than 21 days from date quote requested.
   $daysOfInsuranceStart = (mktime(0,0,0,$DISmm,$DISdd,$DISyyyy) - mktime(0,0,0,$month,$day,$year)) / (86400);

   if($daysOfInsuranceStart > 21)
   {
      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_SKP_21_DAY_AFT";
      $error 			   =  true;
   }

   //2. Crawford Davis will not get leads for this postcode
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BD",
      "B",
      "BT",
      "E",
      "BL",
      "LS",
      "RM",
      "S",
      "LU",
      "NE",
      "CF",
      "M",
      "NG",
      "DN",
      "LE",
      "OL",
      "WA",
      "IG",
      "BS",
      "L",
      "SK",
      "SR",
      "TS",
      "WF",
      "WS",
      "BB",
      "CH",
      "G",
      "HA",
      "NW",
      "TW",
      "W",
      "CR",
      "DA",
      "DY",
      "EN",
      "HD",
      "N",
      "SE",
      "WV",
      "UB",
      "BR",
      "HX",
      "NO",
      "PR",
      "WN",
      "EX",
      "KT",
      "LL",
      "RG",
      "SA",
      "SM",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error 			   =  true;
   }

   // 3. Crawford Davis will not get leads if insurance start date is today.
   if($day == $DISdd AND $month == $DISmm AND $year == $DISyyyy)
   {
      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_SKP_TODAY";
      $error 			   =  true;
   }

   return $error;

?>

<?php

   $error = false;

   //1. Towergate Nottingham will get leads only for this postcode
   $postCode        = $_SESSION['_YourDetails_']['postcode'];
   $postCodePrefix  = $_SESSION['_YourDetails_']['postcode_prefix'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];
   $acceptedPcodesArray = array(
      "NG", 
      "DE", 
      "LN", 
      "CT",
      "PE",
   );
  
   $acceptedPcodesPrefArray = array(
      "LE11",
      "S43",
      "S44",
      "S81",
   );

   $skipPcodesPrefArray = array(
      "PE1",
   );

   if((! in_array($postcodeToCheck,$acceptedPcodesArray)) && (! in_array($postCodePrefix,$acceptedPcodesPrefArray)))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error 			   =  true;
   }
   if(in_array($postCodePrefix,$skipPcodesPrefArray))
   {
      $personalizedError[] = "SKIP_RULE_POST_PREF--SKP_LIST [$postCodePrefix]";
      $error               =  true;
  }
   return $error;

?>

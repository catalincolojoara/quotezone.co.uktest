<?php

   $error = false;

   //1. Coversure Nuneaton will not receive leads for this postcode.
   $postCode     = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
      "CV", 
   );

   if(! in_array($postcodeToCheck,$acceptedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error 			   =  true;
   }

   //2.Skip if the Taxi No Claims Bonus = No NCB
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB == 0)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
      $error			   =  true;
   }
   

   return $error;

?>

<?php

   $error = false;



   // accept postcodes 
   $postCode   = $_SESSION['_YourDetails_']['postcode'];
   $postPrefix = $_SESSION['_YourDetails_']['postcode_prefix'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $accPcodesArray = array(
      "BA",
      "BH",
      "BS",
      "DT",
      "EX",
      "GL",
      "NP",
      "PL",
      "SN",
      "SP",
      "TA",
      "TQ",
      "TR",
      "BN",
      "BR",
      "CR",
      "CT",
      "DA",
      "GU",
      "HP",
      "KT",
      "ME",
      "MK",
      "OX",
      "PO",
      "RG",
      "RH",
      "SL",
      "SM",
      "SO",
      "TN",
      "TW",
      "UB",
      "WD",
   );

   $postcodePrefArray = array(
      "BB1",
      "BB10",
      "BB11",
      "BB12",
      "BB2",
      "BB3",
      "BB4",
      "BB5",
      "BB6",
      "BB7",
      "BB8",
      "BB9",
      "BD23",
      "BL0",
      "BL6",
      "BL7",
      "FY1",
      "FY2",
      "FY3",
      "FY4",
      "FY5",
      "FY6",
      "FY7",
      "FY8",
      "L37",
      "L39",
      "L40",
      "LA1",
      "LA2",
      "LA3",
      "LA4",
      "LA5",
      "LA6",
      "OL12",
      "OL13",
      "PR1",
      "PR2",
      "PR3",
      "PR4",
      "PR5",
      "PR6",
      "PR7",
      "PR8",
      "PR9",
      "WN6",
      "WN8",
   );

   if((! in_array($postcodeToCheck,$accPcodesArray)) && (! in_array($postPrefix,$postcodePrefArray)))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error 			   =  true;
   }

   //Skip if claims in the last 5 years = Yes
   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
   if($claim5Years == "Yes")
   {
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_YES";
      $error               = true;
   }

   //Skip if convictions last 5 years = Yes
   $conv5Years = $_SESSION['_YourDetails_']['convictions_5_years'];
   if($conv5Years == "Yes")
   {
      $personalizedError[] = "SKIP_RULE_CONV_LAST_5_YEARS--SKP_YES";
      $error                       =  true;
   }


// "LONDON PCO",
   $licesingAuthoritySkp = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $skpLicensingAuthorityArray = array(
"LONDON PCO",
   );

   if(in_array($licesingAuthoritySkp,$skpLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthoritySkp]";
      $error               =  true;
   }


   //skip if Year of Manufacture < 2008  (.eg. skip 2007, 2006, 2005 etc)
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];

   if($yearOfManufacture < 2008)
   {
      $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--YEAR_SKP_UND_2008"; 
      $error                       =  true;
   }

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");
      switch($dateDdNum)
      {
         case '1':
         case '2':
         case '3':
         case '4':
         case '5':
            if($timeHh < 8 || $timeHh >= 20 )
            {
               $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_8:00AM_TO_8:00PM";
               $error               =  true;
            }
         
            break;
         case '6':
               $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT--TIME_SKP_ALL_DAY";
               $error               =  true;
            break;

         case '7':
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SUN--TIME_SKP_ALL_DAY";
            $error               =  true;
            break;

      }
   }
   
//   skip if Best Time to Call = Evening (5.30pm - 8pm)
   $bestTimeCall = $_SESSION['_YourDetails_']['best_time_call'];
   if($bestTimeCall == 4)
   {
      $personalizedError[] = "SKIP_RULE_BST_CALL--TIME_TO_CALL_SKP_EVEN";
      $error               =  true;
   }

   // Skip if Taxi No Claims Bonus < 3 years
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB < 3)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_3_YEARS";
      $error            =  true;
   }

   //Skip if Taxi badge is less than "2-3 years"
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];

   if($taxiBadge < 5)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_2_3_YEARS";
      $error                       =  true;
   }

   return $error;

?>

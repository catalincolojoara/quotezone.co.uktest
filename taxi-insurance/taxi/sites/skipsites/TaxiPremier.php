<?php

   $error = false;

   //1. Skip if taxi NCB < 2.
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($taxiNCB < 2)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_2_YEARS";
      $error 			   =  true;
   }

   //2. Skip if user < 30 years old.
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 30)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_30";
      $error 			   =  true;
   }
 
   //3.Only the following plating authorities are acceptable.
   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

   $accLicensingAuthorityArray = array(
"BASINGSTOKE & DEANE",
"BRACKNELL FOREST",
"ELMBRIDGE",
"GUILDFORD",
"HART",
"READING",
"RUNNYMEDE",
"RUSHMOOR",
"SURREY HEATH",
"WAVERLEY",
"WEST BERKSHIRE",
"WINDSOR & MAIDENHEAD",
"WOKING",
"WOKINGHAM",
   );

   if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
  	  $error 			   =  true;
   }

   //4.Skip if Full UK Licence = Under 6 months, 6 months to 1 year,1 – 2 years,2-3 Years,3-4 Years,4-5 Years
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence < 7)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_5_6_YEARS";
      $error 			   =  true;
   }

   //Also please update this plugin so that they do not receive leads that come from SMS renewals
   if($_SESSION["_QZ_QUOTE_DETAILS_"]["SERVER_IP"] == "192.168.12.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   //Also please update this plugin so that they do not receive leads that come from SMS renewals - second check
   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }
  
   return $error;
?>

<?php

   $error = false;

   //Only receive taxi leads from the following postcodes:
   $postCode        = $_SESSION['_YourDetails_']['postcode'];
   $postCodePrefix  = $_SESSION['_YourDetails_']['postcode_prefix'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesPrefArray = array(
      "ME",
      "CT",
      "TN",
   );

   if(! in_array($postcodeToCheck,$acceptedPcodesPrefArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error 			   =  true;
   }

   return $error;
?>

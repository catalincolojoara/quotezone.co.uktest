<?php

   $error = false;


   //ACCEPT if ’Max. Number of Passengers’ > 7
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity > 7)
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_ACC_OVR_7";
      $error               =  true;
   }

   //SKIP if "Taxi driver(s)" = Insured and 2+ named drivers
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers == 3)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_SKP_INS_AND_2+_NAM_DRV";
      $error                       =  true;
   }



   //ACCEPT if Proposer Age is between 25 and 69 (e.g. accept 25 and accept 69)
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   
   if($propAge < 25 || $propAge > 69)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_BETW_25_AND_69";
      $error            =  true;
   }

   //SKIP if "Estimated taxi value (£)" < £2000
   $estimated_value = $_SESSION['_YourDetails_']['estimated_value'];
   if($estimated_value < 2000)
   {
      $personalizedError[] = "SKIP_RULE_VEH_VALUE--POUND_SKP_UND_2000";
      $error            = true;
   }

   //SKIP if "Full UK licence" < 2 years
   $period_of_licence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($period_of_licence == 3)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_1_2_YEARS";
      $error                       =  true;
   }

   //SKIP if Year of manufacture > 11 years from Quote date
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];
   $curYear           = date('Y');

   if(($curYear - $yearOfManufacture) > 11)
   {
      $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--AGE_SKP_OVR_11";
      $error           =  true;
   }

   
   //ACCEPT if "Taxi registration" has a value entered
   $taxiRegNumber = $_SESSION['_YourDetails_']['vehicle_registration_number'];
   if($taxiRegNumber == "")
   {
      $personalizedError[] = "SKIP_RULE_REG_NUMBER--SKP_BLANK";
      $error               =  true;
   }

   //SKIP if "Taxi badge" < 2 years
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge <= 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_YEARS_INT_SKP_UND_2_3_YEARS";
      $error               = true;
   }

      /*SKIP if Taxi Plating Authority =
                                          Berwick Upon Tweed
                                          Birmingham
                                          Blackburn with Darwen
                                          Bolton
                                          Burnley
                                          Bury
                                          Calderdale
                                          Bradford
                                          Craven
                                          Halton
                                          Hyndburn
                                          Keighley
                                          Kirklees
                                          Knowsley
                                          Leeds
                                          Liverpool
                                          Luton
                                          Manchester
                                          Oldham
                                          Wigan
   */

   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $skipLicensingAuthorityArray = array(
      "BERWICK ON TWEED",
      "BIRMINGHAM",
      "BLACKBURN WITH DARWEN",
      "BOLTON",
      "BURNLEY",
      "BURY",
      "CALDERDALE",
      "BRADFORD",
      "CRAVEN",
      "HALTON",
      "HYNDBURN",
      "KEIGHLEY",
      "KIRKLEES",
      "KNOWSLEY",
      "LEEDS",
      "LIVERPOOL",
      "LUTON",
      "MANCHESTER",
      "OLDHAM",
      "WIGAN",

   );

   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error               =  true;
   }

   //ACCEPT Leads MONDAY - FRIDAY 1000 - 1800 each day
   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
      {
      // Only receive leads from Monday to Friday, 9.00am to 5.00pm daily
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {
         case '6': // Sat
         case '7': // Sun
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
            $error                           =  true;

            break;

         case '1': // Mon
         case '2': // Tue
         case '3': // Wed
         case '4': // Thu
         case '5': // Fri
            if($timeHh < 10 || $timeHh >= 18)
            {
               $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_10:00AM_TO_6:00PM";
               $error                               =  true;
            }
         break;
      }
   }


   //SKIP if Postcode is in attached SKIP list
   $postCode       = $_SESSION['_YourDetails_']['postcode'];
   $postCodePrefix = $_SESSION['_YourDetails_']['postcode_prefix'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BB",
      "BD",
      "BL",
      "GY",
      "HS",
      "HD",
      "IM",
      "JE",
      "L",
      "LU",
      "M",
      "OL",
      "PR",
      "WA",
      "WN",
      "E",
      "EC",
      "EN",
      "HA",
      "N",
      "NW",
      "SE",
      "SW",
      "TW",
      "UB",
      "W",
      "WC",
      "DA",
      "BT",
   );

   $skippedPcodesArray = array(
                  "B1",
                  "B2",
                  "B3",
                  "B4",
                  "B5",
                  "B6",
                  "B7",
                  "B8",
                  "B9",
                  "B10",
                  "B11",
                  "B12",
                  "B13",
                  "B14",
                  "B15",
                  "B16",
                  "B17",
                  "B18",
                  "B19",
                  "B20",
                  "B21",
                  "B23",
                  "B24",
                  "B25",
                  "B26",
                  "B27",
                  "B28",
                  "B29",
                  "B42",
                  "B43",
                  "B44",
                  "BA1",
                  "BS2",
                  "BS3",
                  "BS4",
                  "BS5",
                  "BS6",
                  "DE1",
                  "DY1",
                  "DY2",
                  "DY4",
                  "HX1",
                  "LE4",
                  "LE5",
                  "LS1",
                  "LS2",
                  "LS3",
                  "LS4",
                  "LS5",
                  "LS6",
                  "LS7",
                  "LS8",
                  "LS9",
                  "LS10",
                  "LS11",
                  "LS12",
                  "LS13",
                  "LS14",
                  "LS15",
                  "LS16",
                  "LS17",
                  "LS18",
                  "LS25",
                  "LS28",
                  "NG1",
                  "NG5",
                  "S1",
                  "S2",
                  "S3",
                  "S4",
                  "S5",
                  "S6",
                  "S7",
                  "S8",
                  "S9",
                  "S10",
                  "S11",
                  "S12",
                  "S13",
                  "S14",
                  "SR1",
                  "SR2",
                  "SR3",
                  "SR4",
                  "SR5",
                  "WS1", 
                  "WS2",
                  "WS10",
   );

   if((in_array($postCodePrefix,$skippedPcodesArray)) || ( in_array($postcodeToCheck,$skipPcodesArray)))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                        =  true;
   }


   // SKIP if Insurance Start Date > 21 days from QUote date
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   // today date
   $day   = date("d");
   $month = date("m");
   $year  = date("Y");

   $daysOfInsuranceStart = (mktime(0,0,0,$DISmm,$DISdd,$DISyyyy) - mktime(0,0,0,$month,$day,$year)) / (86400);

   if($daysOfInsuranceStart > 21)
   {
      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_ACC_LES_21_DAY_AFT";
      $error               =  true;
   }

   // SKIP if "Taxi use" = Public Hire
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse == 2)
   {
      $personalizedError[] = "SKIP_RULE_USE_PCODES--USE_SKP_PUB_HIRE";
      $error          =  true;
   }
	
	
   /*IF "Taxi no claims bonus" = 0
                                 1 year
                                 2 years
                                 3 years

      ONLY ACCEPT if "Private car no claims bonus" = 4 years    5+ years
   */

   $taxiNCB       = $_SESSION['_YourDetails_']['taxi_ncb'];
   $privateCarNCB = $_SESSION['_YourDetails_']['private_car_ncb'];

   if($taxiNCB == 1 ||  $taxiNCB == 2 || $taxiNCB == 3 || $taxiNCB == 0)
   {
         $privateCarNCBArray = array(
            "4",
            "5",
         );
      
         if(! in_array($privateCarNCB,$privateCarNCBArray))
         {
            $personalizedError[] = "SKIP_RULE_NCB_PNCB--ACC_LIST ['IF Taxi no claims bonus  is 0 ,1,2,3 ONLY ACCEPT if Private car no claims bonus = 4 years or 5+ years']";
            $error               = true;
         }
   }

   return $error;
?>

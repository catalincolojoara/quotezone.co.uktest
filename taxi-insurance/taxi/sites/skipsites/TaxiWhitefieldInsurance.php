<?php

   $error = false;


   //1.ACCEPT if Proposer Age > 29
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   
   if($propAge <= 29)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_OVR_29";
      $error            =  true;
   }

   
   //2. ACCEPT if "Type of cover" = Fully Comprehensive
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover != 1)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_ACC_FULL_COMPR"; 
      $error 			   =  true;
   }
   
   
   //3. ACCEPT if "Taxi no claims bonus" > 2 Years
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($taxiNCB < 3)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_3_YEARS";
      $error            =  true;
   }
   
   //4. ACCEPT if "Taxi use" = Private Hire
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse != 1)
   {
      $personalizedError[] = "SKIP_RULE_USE--USE_ACC_PRIV_HIRE";
      $error               =  true;
   }
   
 
   /*5. ACCEPT if "Best Day To Call" =  Monday
                                        Tuesday
                                        Wednesday
                                        Thursday
                                        Friday
   */
   
  
   
    if($_SESSION['USER_IP'] != "86.125.114.56")
    {
      //6. ACCEPT Leads MONDAY - FRIDAY 0900 - 1600
      $timeHh     = date("G");
      $dateDdText = date("N");

      if($dateDdText > 5 || $timeHh < 9 || $timeHh >= 16)
      {
         $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_4:00PM";
         $error               = true;
      }
    }

    
    /*SKIP if "Best Time To Call" = Evening (5.30pm - 8pm)*/
   
   $bestTimeCall = $_SESSION['_YourDetails_']['best_time_call'];
   if($bestTimeCall == "4")
   {
      $personalizedError[] = "SKIP_RULE_BST_CALL--TIME_TO_CALL_SKP_EVEN";
      $error               =  true;
   }
   
   return $error;

?>

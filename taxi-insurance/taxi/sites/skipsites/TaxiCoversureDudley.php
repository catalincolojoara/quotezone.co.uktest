<?php

   $error = false;
  
   //1. Coversure Rubery will receive leads only for this postcodes.
   $postCodePrefix = $_SESSION['_YourDetails_']['postcode_prefix'];

   $acceptedPcodesArray = array(
      "B65", 
      "B69", 
      "B70", 
      "DY1", 
      "DY2", 
      "DY3", 
      "DY4", 
      "DY5", 
      "DY6", 
      "DY8", 
      "DY9", 
      "WV14", 
      "WV2", 
      "WV4", 
      "WV5", 
      "B20", 
      "B42", 
      "B43", 
      "B71", 
      "WS1", 
      "WS10", 
      "WS11", 
      "WS12",
      "WS2", 
      "WS3", 
      "WS4", 
      "WS5", 
      "WS6", 
      "WS9",
   );

   if(! in_array($postCodePrefix,$acceptedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POST_PREF--ACC_LIST [$postCodePrefix]";
      $error 			   =  true;
   }

   return $error;

?>

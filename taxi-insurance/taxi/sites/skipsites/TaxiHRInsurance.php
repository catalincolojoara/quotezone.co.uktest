<?php

   $error = false;

   // 1. H&R Insurance will not get leads for this licensing authority.
   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

   $acceptedLicensingAuthorityArray = array(
"ABERDEEN",
"ABERDEEN",
"ABERDEENSHIRE",
"ABERDEENSHIRE",
"ANGUS",
"ARGYLE & BUTE",
"BADENOCH & STRATHSPEY",
"BERWICK ON TWEED",
"CAITHNESS (HIGHLANDS)",
"CLACKMANNANSHIRE",
"DUMFRIES & GALLOWAY",
"DUNDEE",
"EAST AYRSHIRE (KILMARNOCK)",
"EAST DUNBARTONSHIRE (KIRKINTILLOCH)",
"EAST KILBRIDE",
"EAST LOTHIAN (HADDINGTON)",
"EAST RENFREWSHIRE (GIFFNOCH)",
"EDINBURGH",
"FALKIRK",
"FIFE",
"INVERNESS (HIGHLANDS)",
"INVERCLYDE (GREENOCK)",
"LOCHABER (HIGHLAND - FORT WILLIAM)",
"MIDLOTHIAN",
"MORAY",
"NAIRN (HIGHLANDS)",
"NORTH AYRSHIRE",
"NORTH LANARKSHIRE (NORTH)",
"ORKNEY ISLANDS (KIRKWALL)",
"PERTH & KINROSS",
"RENFREWSHIRE (PAISLEY)",
"ROSS & CROMARTY (HIGHLAND - DINGWALL)",
"SCOTTISH BORDERS",
"SHETLAND ISLANDS (LERWICK)",
"SKYE & LOCHAISH (HIGHLANDS)",
"SOUTH AYRSHIRE (AYR)",
"SOUTH LANARKSHIRE",
"SOUTH LANARKSHIRE",
"SOUTH LANARKSHIRE",
"STIRLING",
"SUTHERLAND (HIGHLANDS)",
"WEST DUNBARTONSHIRE (CLYDEBANK)",
"WEST LOTHIAN (LIVINGSTON)",
"WESTERN ISLES (STORNOWAY)",
   );

   if(! in_array($licesingAuthority,$acceptedLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
  	  $error 			   =  true;
   }

   // 2. H&R Insurance accepts leads for drivers that have minimum 1 year ncb.
   $drNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($drNCB == 0)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
      $error 			   =  true;
   }

   // cannot quote because of the proposer age - skip if age is under 25 years or > 70
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25 || $propAge > 70)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25_AND_OVR_70";
      $error            =  true;
   }


   //Also please update this plugin so that they do not receive leads that come from SMS renewals
   if($_SESSION["_QZ_QUOTE_DETAILS_"]["SERVER_IP"] == "192.168.12.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   //Also please update this plugin so that they do not receive leads that come from SMS renewals - second check
   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   //skip if Taxi Driver(s) = Insured and 2+ Named Drivers
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers == 3)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_SKP_INS_AND_2+_NAM_DRV";
      $error                       =  true;
   }

   return $error;

?>

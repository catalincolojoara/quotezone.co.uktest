<?php

   $error = false;

//  SKIP if "Taxi No Claims Bonus" = No NCB
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB == 0)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
      $error                       =  true;
   }

   // SKIP if "Age" < 30
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 30)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_30";
      $error            =  true;
   }

//SKIP if "Taxi Badge" < 2 Years
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge <= 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_2_YEARS";
      $error               =  true;
   }

   // skip if Full UK Licence is under "4-5 Years"
   $period_of_licence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($period_of_licence < 6)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_4_5_YEARS";
      $error                       =  true;
   }

   // only accept the attached postcode list
      //////////////////////////////////////////////////////
      //$postCode        = $_SESSION['_YourDetails_']['postcode'];
      //$postCodePreffix = $_SESSION['_YourDetails_']['postcode_prefix'];
      //$postCodeSuffix  = $_SESSION['_YourDetails_']['postcode_number']; 
      //$postCodeArray   = array("BT55ZU", "BT3 9", "BT[1-99]*", "BT[1-99]", "BT[2-15]*", "BT[2-15]", "BT[1-1]*, "BT[1-1]", "BT21");
      //$filter          = "accept" or "skip";
      //////////////////////////////////////////////////////

   $postCode        = $_SESSION['_YourDetails_']['postcode'];
   $postCodePreffix = $_SESSION['_YourDetails_']['postcode_prefix'];
   $postCodeSuffix  = $_SESSION['_YourDetails_']['postcode_number']; 


   $filter          = "accept";
   $postCodeArray   = array(
         "HG[0-99]*",
         "DL[0-99]*",
         "TS[0-99]*",
         "YO[0-99]*",
         "SR8",
         );

   $filterStatus = "";   

   preg_match("/(.*)\d/isU",$postCodePreffix,$pregLettersToCheck);
   $lettersPostCodePreffix = $pregLettersToCheck[1];

   preg_match("/[0-9]+/",$postCodePreffix,$pregNumbersToCheck);
   $numbersPostCodePreffix = $pregNumbersToCheck[0];

   $prefLettAndNum = $lettersPostCodePreffix.$numbersPostCodePreffix;

   foreach($postCodeArray as $keypostCodeArray=>$skpPostcode)
   {
      if((preg_match("/[A-Z]*/",$skpPostcode,$pregMatchLetters)) && (preg_match("/\[(.*)\-/",$skpPostcode,$pregMinInterval)) && (preg_match("/\-(.*)\]/",$skpPostcode,$pregMaxInterval)))
      {
         $prefixLettersArr = $pregMatchLetters[0];
         $minIntervalArr   = $pregMinInterval[1];
         $maxIntervalArr   = $pregMaxInterval[1];
      }
      else 
      {
         $prefixLettersArr = "";
         $minIntervalArr   = "";
         $maxIntervalArr   = "";
      }
      
      
      //checking postcodes like BT[1-99]*
      if((preg_match("/\*/isU",$skpPostcode,$pregMatchAll)))
      {
         if(($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
         {
            //"postcode has been accepted \n"; //only accept list
            $filterStatus = true;
            break;
         }
      }

      //checking postcodes like BT[1-99]
      elseif(preg_match("/^[A-Z]{1,2}\[[0-9]{1,2}-[0-9]{1,2}\]$/isU",$skpPostcode,$pregMatchAll))
      {
         if(($postCodePreffix == $prefLettAndNum) && ($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
         {            
            $filterStatus = true;
            break;
         }
      }
      
      //checking postcodes like BT3 9
      elseif((preg_match("/(.*)\s+/isU",$skpPostcode, $pregMatchSpace)))
      {      
      $sufixNumber = substr($postCodeSuffix,0,-2);
      $postCodeToCheck = $postCodePreffix." ".$sufixNumber;
         
         if($skpPostcode == $postCodeToCheck)
         {
            $filterStatus = true;
            break;
         }             
      }

      //checking postcodes like BT339AA OR BT39AA
      elseif(ctype_alpha(substr($skpPostcode,-2)))
      {
      $postCodeStrip = str_replace(" ","",$postCode);

         if($skpPostcode == $postCodeStrip)
         {
            $filterStatus = true;
            break;
         }             
      }

      //checking postcodes preffix like BT1, BT1A
      elseif((preg_match("/^$postCodePreffix$/",$skpPostcode)))
      {
         if($postCodePreffix == $skpPostcode);
         {
            $filterStatus = true;
            break;
         }
      }
   }

   if($filter == "accept")
   {
      if($filterStatus == false)
      {
         $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]\n";
         $error               =  true;
      }
   }
   elseif($filter == "skip")
   {
      if($filterStatus==true)
      {
         $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
         $error               =  true;
      }
   }

   //SKIP if "Taxi Plating Authority" = "LONDON PCO"
   $licesingAuthoritySkp = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $skpLicensingAuthorityArray = array(
      "LONDON PCO",
   );

   if(in_array($licesingAuthoritySkp,$skpLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthoritySkp]";
      $error               =  true;
   }

   //Also please update this plugin so that they do not receive leads that come from SMS renewals
   if($_SESSION["_QZ_QUOTE_DETAILS_"]["SERVER_IP"] == "192.168.12.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   //Also please update this plugin so that they do not receive leads that come from SMS renewals - second check
   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   return $error;

?>

<?php

   $error = false;

   
   /* 
      ACCEPT leads Monday 08:00 - 17:00
                  Tuesday 08:00 - 17:00
                  Wednesday 08:00 - 17:00
                  Thursday 08:00 - 17:00
                  Friday 08:00 - 17:00
                  Saturday NO LEADS
                  Sunday NO LEADS
               
   */
   

   $postCode     = $_SESSION['_YourDetails_']['postcode'];

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {
         case '1':
         case '2':
         case '3':
         case '4':
         case '5':
         if($timeHh < 8 || $timeHh >= 17)
         {
            $personalizedError[] = "SKIP_RULE_SKP_DAY_MON_FRI--TIME_ACC_BETW_8:00AM_TO_5:00PM";
            $error               =  true;
         }
         break;
         case '6': 
         case '7':
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
            $error               =  true;
         break;
      }
   }

// "NOTTINGHAM",
// "DUDLEY",
// "SWANSEA",
   $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

   $accLicensingAuthorityArray = array(
"NOTTINGHAM",
"DUDLEY",
"SWANSEA",
   );

   if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
      $error               =  true;
   }


   // SKIP if "Full UK licence" = "Under 6 Months"
   //                                            "6 Months To 1 Year"
   //                                            "1 - 2 Years"
   //                                            "2 - 3 Years"
   //                                            "3 - 4 Years"
   //                                            "4 - 5 Years"
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence <= 6)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_4_5_YEARS";
      $error               = true;
   }

   // SKIP if "Taxi Badge" = "No Badge"
   //                                      "Under 6 Months"
   //                                      "6 Months To 1 Year"
   //                                      "1 - 2 Years"
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge <= 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_2_YEARS";
      $error               = true;
   }


   // SKIP if "Claims last 5 years" = "No Claims"
   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
   if($claim5Years != "Yes")
   { 
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_NO";
      $error               = true;
   }


   //SKIP if Lead Source = SMS - skip leads sent from 13 server
   if($_SESSION["_QZ_QUOTE_DETAILS_"]["SERVER_IP"] == "192.168.12.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }
   
   if($_SESSION["USER_IP"] == "10.2.12.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }   
   
   return $error;

?>

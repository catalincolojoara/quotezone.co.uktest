<?php

   $error = false;

   $postCode     = $_SESSION['_YourDetails_']['postcode'];

   // 1. A-Plan Insurance will not accept leads from this postcode: 
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
      "BA",
      "BH",
      "BN",
      "BS",
      "CO",
      "CT",
      "DT",
      "EX",
      "GL",
      "GU",
      "HP",
      "HR",
      "IP",
      "MK",
      "NR",
      "OX",
      "PE",
      "PL",
      "PO",
      "RG",
      "RH",
      "SG",
      "SN",
      "SO",
      "SP",
      "TA",
      "TN",
      "TQ",
      "TR",
      "WR",
   );

   if(! in_array($postcodeToCheck,$acceptedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error 			   =  true;
   }


   //Skip if user < 30 years old.
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 30)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_30";
      $error            =  true;
   }

   return $error;
?>

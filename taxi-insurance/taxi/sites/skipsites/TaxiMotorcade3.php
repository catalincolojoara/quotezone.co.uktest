<?php

 // Motorcade (Palace) 
   $error = false;

   //If Type of Cover = Fully Comprehensive, Third Party Fire & Theft, skip if "Vehicle Age" > 8 Years (eg. skip 2005, 2004, 2003 etc.)
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover == 1   OR  $typeOfCover == 2)
   {
      $vehicleManufYear = $_SESSION["_YourDetails_"]["year_of_manufacture"];
      $disYear          = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"];

      $vehicleAge = $disYear - $vehicleManufYear;

      if($vehicleAge > 8)
      {
         $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_FULL_TPFT_SKP_VEH_AGE_OVR_8";
         $error                       =  true;
      }
   }


   if($typeOfCover == 3)
   {
      $vehicleManufYear = $_SESSION["_YourDetails_"]["year_of_manufacture"];
      $disYear          = $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"];

      $vehicleAge = $disYear - $vehicleManufYear;

      if($vehicleAge > 12)
      {
         $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_TPSKP_VEH_AGE_OVR_12";
         $error                       =  true;
      }
   }

// 1. ACCEPT only if "Postcode"
   $postCode        = $_SESSION['_YourDetails_']['postcode'];
   $postCodePrefix  = $_SESSION['_YourDetails_']['postcode_prefix'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];
    
   $accPcodesPArray = array(
      "BN",
      "BR",
      "CR",
      "CT",
      "DA",
      "RH",
      "SE",
      "SM",
      "TN",
   );

   if(! in_array($postcodeToCheck,$accPcodesPArray) )
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCodePrefix]";
      $error               = true;
   }
     
   
   

/*if postcode = BN[0-99]*, CT[0-99]*, RH[0-99]*, TN[0-99]* : 
 * apply filters below : 
 * 1. skip if user age < 25 or > 65
   2. skip if Full UK License = Under 6 Months 6 Months to 1 Year 1-2 Years
 * 3. skip if Taxi Badge = No Badge 6 Months to 1 Year
 * 4. skip if Taxi No Claims Bonus > 3 Years 
 */ 
  
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   
   $acceptPcodesArraySpecial1 = array(
         "BN",
         "CT",
         "RH",
         "TN"
   );

//   skip if user age < 25 or > 65
   if(($propAge < 25 || $propAge > 65) && (in_array($postcodeToCheck,$acceptPcodesArraySpecial1)))
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE_POSTCODE--AGE_ACC_BETW_25_AND_65_PCODES [$postCode]";
      $error               =  true;
   }
   
//   skip if Full UK License = Under 6 Months 6 Months to 1 Year 1-2 Years
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if(($ukLicence <= 3) && (in_array($postcodeToCheck,$acceptPcodesArraySpecial1)))
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE_PCODES--YEARS_INT_SKP_UND_3_YEARS_POSTCODE";
      $error               = true;
   }
   
//   skip if Taxi Badge = No Badge 6 Months to 1 Year
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if(($taxiBadge == 1 ||$taxiBadge == 3) && (in_array($postcodeToCheck,$acceptPcodesArraySpecial1)))
   {
      $personalizedError[] = "SKIP_RULE_BADG_PCODES--BADG_SKP_N_BADG_6_MONTH_TO_YEAR_POSTCODE";
      $error                       =  true;
   }
  
//  skip if Taxi No Claims Bonus > 3 Years 
    $NCB = $_SESSION['_YourDetails_']['taxi_ncb'];
    if(($NCB > 3) && (in_array($postcodeToCheck,$acceptPcodesArraySpecial1)))
    {
       $personalizedError[] = "SKIP_RULE_NCB_POSTCODE--NUM_SKP_OVR_3_POSTCODE";
       $error            =  true;
    }
    
    
/*if postcode = BR[0-99]*
                CR[0-99]*
                DA[0-99]*
                SE[0-99]*
                SM[0-99]*

apply filters below:

skip if user age     <    23    or   >    65 
skip if  	Taxi Use = Public Hire
* 
*/
    
     $acceptPcodesArraySpecial2 = array(
         "BR",
         "CR",
         "DA",
         "SE",
         "SM",
   );
    
 // skip if user age     <    23    or   >    65 
 
   if(($propAge < 23 || $propAge > 65) && (in_array($postcodeToCheck,$acceptPcodesArraySpecial2)))
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE_POSTCODE--AGE_ACC_BETW_23_AND_65_PCODES [$postCode]";
      $error               =  true;
   }
    
//   skip if  	Taxi Use = Public Hire
//   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
//   if(($taxiUse == 2) && (in_array($postcodeToCheck,$acceptPcodesArraySpecial2)))
//   {
//      $personalizedError[] = "SKIP_RULE_USE_PCODES--USE_SKP_PUB_HIRE_PCODES";
//      $error               =  true;
//   }
    
   //If "Taxi Type" = "Black Cab" SKIP if "Taxi Use" = "Private Hire"
//    $taxiType = $_SESSION["_YourDetails_"]["taxi_type"];  
//    $taxiUse  = $_SESSION["_YourDetails_"]["taxi_used_for"];
//    if($taxiType == 1)
//    {
//       if($taxiUse == 1)
//       {
//          $personalizedError[] = "SKIP_RULE_USE--TYP_BLACK_CAB_SKP_PHIRE";
//          $error          =  true;
//       }
//    }
   
// only ACCEPT Taxi Use = Private Hire
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse != 1)
   {
      $personalizedError[] = "SKIP_RULE_USE--USE_ACC_PRIV_HIRE";
      $error               =  true;
   }   
   
    return $error;

?>

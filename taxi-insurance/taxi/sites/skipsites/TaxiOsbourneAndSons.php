<?php

   $error = false;

   // SKIP if Wl source = dnainsurance
   $wsID = $_SESSION['_QZ_QUOTE_DETAILS_']['ws_id'];

   if($wsID == "5fdbf8c1a05cd4cdf82ed72104896c7e")
   {
      $personalizedError[] = "SKIP_RULE_COMPANY_WL--SKP_LIST [$wsID]";
      $error               =  true;
   }
   
   
   // SKIP if Wl source = dnainsurance
   $wsID = $_SESSION['_QZ_QUOTE_DETAILS_']['ws_id'];

   if($wsID == "785e5997d6ca27649d56af14c5d31141")
   {
      $personalizedError[] = "SKIP_RULE_COMPANY_WL--SKP_LIST [$wsID]";
      $error               =  true;
   }

	//SKIP if 'Taxi no claims bonus' = 'No NCB'
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB == 0)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
      $error                       =  true;
   }

	//SKIP if 'Taxi badge' = 'No badge', 'under 6 months', '6 months to 1 year'
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_2_YEARS";
      $error               = true;
   }

	//SKIP if 'Full UK licence' = '1 - 2 years'
   $periodOfFullUkLicenceHeld = $_SESSION["_YourDetails_"]["period_of_licence"];

   $filteredPeriodOfFullUkLicenceHeldArray = array(
      "3" => "1 - 2 Years",
      );

   if(array_key_exists($periodOfFullUkLicenceHeld,$filteredPeriodOfFullUkLicenceHeldArray))
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--SKP_LIST [".$filteredPeriodOfFullUkLicenceHeldArray[$periodOfFullUkLicenceHeld]."]";
      $error            =  true;
   }

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   //SKIP if Age < 25 years
   if($propAge < 25)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error 			   =  true;
   }

   //Please SKIP if age > 65 years old
   if($propAge > 65)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_OVR_65";
      $error            =  true;
   }
 

	// If 'Taxi use' = 'Private Hire'
	// ACCEPT 'Taxi plating authority' =
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse == 1)
	{
		$licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

		$accLicensingAuthorityArray = array(
			"LONDON PCO",
			"ELMBRIDGE",
			"RUNNYMEDE",
			"EPSOM & EWELL",
			"CRAWLEY",
			"HORSHAM",
			"MID SUSSEX",
			"REIGATE AND BANSTEAD",
			"MOLE VALLEY",
			"TANDRIDGE",
		);

		if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
		{
			$personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
		$error 			   =  true;
		}
	}

	// If 'Taxi use' = 'Public Hire'
	// ACCEPT 'Taxi plating authority' =
   if($taxiUse == 2)
	{
		$licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

		$accLicensingAuthorityArray = array(
			"ELMBRIDGE",
			"RUNNYMEDE",
			"EPSOM & EWELL",
			"CRAWLEY",
			"HORSHAM",
			"MID SUSSEX",
			"REIGATE AND BANSTEAD",
			"MOLE VALLEY",
			"TANDRIDGE",
		);

		if(! in_array($licesingAuthority,$accLicensingAuthorityArray))
		{
			$personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
		$error 			   =  true;
		}
	}

   //If 'Taxi use' = 'Private Hire'  AND if 'Taxi type' = 'Black Cab', then SKIP if 'Taxi plating authority' = 'London PCO'
   $taxiType = $_SESSION['_YourDetails_']['taxi_type'];
   if($taxiType == 1)
   {
      if($taxiUse == 1) 
      {
         $licesingAuthority = $_SESSION['_YourDetails_']['plating_authority'];

         $accLicensingAuthorityArray = array(
            "LONDON PCO",
         );

         if(in_array($licesingAuthority,$accLicensingAuthorityArray))
         {
            $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_TAXI_USE_TAXI_TYPE_PL_AUTH";
            $error 			   =  true;
         }
      }
   }
   
   
   // SKIP all SMS text leads
   if($_SESSION["USER_IP"] == "10.2.12.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }
	
	
	if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }
	
		
	if($_SESSION["_QZ_QUOTE_DETAILS_"]["SERVER_IP"] == "192.168.12.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }

   
   
   // SKIP if 'Insurance start date' is greater than 28 days after the quote submission date.
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $DIS              = $DISyyyy."-".$DISmm."-".$DISdd;
   $inTheFutureDays  = date('Y-m-d',mktime(0,0,0,date('m'), date('d')+28, date('Y')));

   
   if($DIS > $inTheFutureDays)
   {
      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_SKP_28_DAY_AFT";
      $error               = true;
   }

   return $error;

?>

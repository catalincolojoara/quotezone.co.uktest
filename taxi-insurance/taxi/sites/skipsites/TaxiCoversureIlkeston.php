<?php

   $error = false;

   
   // ACCEPT if Postcode= DE[0-99]*, NG9, NG10, NG16
   $postCode       = $_SESSION['_YourDetails_']['postcode'];
   $postCodePrefix = $_SESSION['_YourDetails_']['postcode_prefix'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
         "DE",         
   );

   $accPcodesPrefixArray = array(
      "NG9",
      "NG10",
      "NG16",
   );

   if((! in_array($postCodePrefix,$accPcodesPrefixArray)) && (! in_array($postcodeToCheck,$acceptedPcodesArray)))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error                        =  true;
   }
   
   
   // SKIP if "Full UK licence" < 5 Years
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence <= 6)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_5_6_YEARS";
      $error               = true;
   }
   
   
   // SKIP if "Taxi badge" < 1 year
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_YEAR";
      $error               =  true;
   }
      
   return $error;
?>

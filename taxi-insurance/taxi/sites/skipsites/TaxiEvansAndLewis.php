<?php

   $error = false;

   /*
    *1. ACCEPT leads Monday 09:00 - 17:00
                   Tuesday 09:00 - 17:00
                   Wednesday 09:00 - 17:00
                   Thursday 09:00 - 17:00
                   Friday 09:00 - 17:00
                   Saturday NO LEADS
                   Sunday NO LEADS

    */
   
   
   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {
         case '6': // Sat
         case '7': //Sun   
         $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
         $error               =  true;
         break;

         case '1': // Mon
         case '2': // Tue
         case '3': // Wed
         case '4': // Thu
         case '5': // Fri
         
         if($timeHh < 9 || $timeHh >= 17)
         {
            $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_7:00PM";
            $error               =  true;
         }
         break;
      }
   }


   //2. SKIP if age < 30
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 30 || $propAge > 69)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_30_AND_OVR_69";
      $error         =  true;
   }

   /*3. SKIP if "Taxi no claims bonus" = "No NCB"
                                          "1 Year"
                                          "2 Years"
                                          "3 Years"
                                          "4 Years"

   */
   
   $NCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($NCB <= 4)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NUM_SKP_UND_4";
      $error            =  true;
   }

   // acc a list of postcode 
   
   $postCode        = $_SESSION['_YourDetails_']['postcode'];
   $postCodePreffix = $_SESSION['_YourDetails_']['postcode_prefix'];
   $postCodeSuffix  = $_SESSION['_YourDetails_']['postcode_number']; 


   $filter          = "skip";
   $postCodeArray   = array(
         "B[1-42]",
         "B[64-71]",
         "BB[1-5]",
         "BB[8-18]",
         "BD[1-22]",
         "BL[1-4]",
         "BL9",
         "CV[1-30]",
         "DY[1-11]",
         "E[0-99]*",
         "EC[0-99]*",
         "EN[1-8]",
         "HA[0-99]*",
         "HD[1-6]",
         "HU[1-14]",
         "HX[0-99]*",
         "IG[0-99]*",
         "L[0-99]*",
         "LE[1-5]",
         "LS[1-19]",
         "M[0-99]*",
         "N[0-99]*",
         "NG[1-31]",
         "NW[0-99]*",
         "OL[0-99]*",
         "S[1-31]",
         "S[37-81]",
         "SE[0-99]*",
         "SK[1-8]",
         "SK[10-16]",
         "SW[0-99]*",
         "UB[0-99]*",
         "W[0-99]*",
         "WA[1-12]",
         "WC[0-99]*",
         "WF[0-99]*",
         "WN[0-99]*",
         "WS[1-10]",
         "WV[1-14]",
   );

   $filterStatus = "";   

   preg_match("/(.*)\d/isU",$postCodePreffix,$pregLettersToCheck);
   $lettersPostCodePreffix = $pregLettersToCheck[1];

   preg_match("/[0-9]+/",$postCodePreffix,$pregNumbersToCheck);
   $numbersPostCodePreffix = $pregNumbersToCheck[0];

   $prefLettAndNum = $lettersPostCodePreffix.$numbersPostCodePreffix;

   foreach($postCodeArray as $keypostCodeArray=>$skpPostcode)
   {
      if((preg_match("/[A-Z]*/",$skpPostcode,$pregMatchLetters)) && (preg_match("/\[(.*)\-/",$skpPostcode,$pregMinInterval)) && (preg_match("/\-(.*)\]/",$skpPostcode,$pregMaxInterval)))
      {
         $prefixLettersArr = $pregMatchLetters[0];
         $minIntervalArr   = $pregMinInterval[1];
         $maxIntervalArr   = $pregMaxInterval[1];
      }
      else 
      {
         $prefixLettersArr = "";
         $minIntervalArr   = "";
         $maxIntervalArr   = "";
      }
      
      
      //checking postcodes like BT[1-99]*
      if((preg_match("/\*/isU",$skpPostcode,$pregMatchAll)))
      {
         if(($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
         {
            //"postcode has been accepted \n"; //only accept list
            $filterStatus = true;
            break;
         }
      }

      //checking postcodes like BT[1-99]
      elseif(preg_match("/^[A-Z]{1,2}\[[0-9]{1,2}-[0-9]{1,2}\]$/isU",$skpPostcode,$pregMatchAll))
      {
         if(($postCodePreffix == $prefLettAndNum) && ($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
         {            
            $filterStatus = true;
            break;
         }
      }
      
      //checking postcodes like BT3 9
      elseif((preg_match("/(.*)\s+/isU",$skpPostcode, $pregMatchSpace)))
      {      
      $sufixNumber = substr($postCodeSuffix,0,-2);
      $postCodeToCheck = $postCodePreffix." ".$sufixNumber;
         
         if($skpPostcode == $postCodeToCheck)
         {
            $filterStatus = true;
            break;
         }             
      }

      //checking postcodes like BT339AA OR BT39AA
      elseif(ctype_alpha(substr($skpPostcode,-2)))
      {
      $postCodeStrip = str_replace(" ","",$postCode);

         if($skpPostcode == $postCodeStrip)
         {
            $filterStatus = true;
            break;
         }             
      }

      //checking postcodes preffix like BT1, BT1A
      elseif((preg_match("/^$postCodePreffix$/",$skpPostcode)))
      {
         if($postCodePreffix == $skpPostcode);
         {
            $filterStatus = true;
            break;
         }
      }
   }

//   if($filter == "accept")
//   {
//      if($filterStatus == false)
//      {
//         $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]\n";
//         $error               =  true;
//      }
//   }
   if($filter == "skip")
   {
      if($filterStatus==true)
      {
         $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
         $error               =  true;
      }
   }

   return $error;

?>

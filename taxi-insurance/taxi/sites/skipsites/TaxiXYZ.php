<?php

   $error = false;

   //1. accept between 30 and 60 years
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 30 || $propAge > 60)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_BETW_30_AND_60";
      $error               =  true;
   }


   //3. Skip if Taxi Badge = No badge, Under 6 months, 6 months to 1 year
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 4)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_2_YEARS";
      $error               = true;
   }

   // SKIP if "Insurance Start Date" > 30 days
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $DIS              = $DISyyyy."-".$DISmm."-".$DISdd;
   $inTheFutureDays = date('Y-m-d',mktime(0,0,0,date('m'), date('d')+30, date('Y')));

   
   if($DIS > $inTheFutureDays)
   {
      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_SKP_30_DAY_AFT";
      $error               = true;
   }

   // SKIP if "Taxi No Claims Bonus" = No NCB, 1 Year
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB < 2)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB_1YEAR";
      $error                       =  true;
   }

   //4. cannot quote because of the vehicle age - skip if age is over 8 years
   $yearOfManufacture    = $_SESSION['_YourDetails_']['year_of_manufacture'];
   $dateOfInsuranceStartYear = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $vehicleAge = $dateOfInsuranceStartYear - $yearOfManufacture;

   if($vehicleAge > 8)
   {
      $personalizedError[] = "SKIP_RULE_VEH_AGE--AGE_SKP_OVR_8";
      $error 			   =  true;
   }

   //5. Skip if Claims last 5 years = Yes, Claims Made
   $claim5Years = $_SESSION['_YourDetails_']['claims_5_years'];
   if($claim5Years == "Yes")
   { 
      $personalizedError[] = "SKIP_RULE_CLAIM_LAST_5Y--SKP_YES";
      $error               = true;
   }

   //6. Skip if max. Number of passengers > 7
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity > '7')
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_7";
      $error 			   =  true;
   }

   //Skip if Taxi Plating Authority:
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $skipLicensingAuthorityArray = array(
      "BERWICK ON TWEED",
      "BIRMINGHAM",
      "BLACKBURN WITH DARWEN",
      "BOLTON",
      "CALDERDALE",
      "BRADFORD",
      "CRAVEN",
      "HALTON",
      "HYNDBURN",
      "KEIGHLEY",
      "KIRKLEES",
      "KNOWSLEY",
      "LEEDS",
      "LIVERPOOL",
      "LONDON PCO",
      "LUTON",
      "MANCHESTER",
      "OLDHAM",
      "WIGAN",
      "PENDLE",
      "PRESTON",
      "REDDITCH",
      "RIBBLE VALLEY",
      "ROCHDALE",
      "ROSSENDALE",
      "SALFORD",
      "SANDWELL",
      "SEFTON",
      "SLOUGH",
      "SOLIHULL",
      "SOUTH BEDFORDSHIRE",
      "SOUTH BUCKINGHAM",
      "ST HELENS",
      "STOCKPORT",
      "TAMESIDE",
      "TAMWORTH",
      "THURROCK",
      "TRAFFORD",
   );

   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error               =  true;
   }

   //7.skip postcodes
   $postCode = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BB",
      "BD",
      "BL",
      "BT",
      "GY",
      "IG",
      "IM",
      "JE",
      "E",
      "EC",
      "L",
      "LS",
      "LU",
      "M",
      "OL",
      "UB",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }

   //skip if AffiliateID = DNAInsurance2
   $wlID = $_SESSION['_QZ_QUOTE_DETAILS_']['wlUserID'];
   if($wlID == "758")
   {
      $personalizedError[] = "SKIP_RULE_COMPANY_WL--WL_SKP_DNAINSURANCE2";
      $error               =  true;
   }

   // only accept Taxi Type = Private Hire
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse != 1)
   {
      $personalizedError[] = "SKIP_RULE_USE--USE_ACC_PRIV_HIRE";
      $error               =  true;
   }

   // skip if Full UK License = 1-2 Years
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence ==  3)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_1_2_YEARS";
      $error               =  true;
   }



return $error;
?>

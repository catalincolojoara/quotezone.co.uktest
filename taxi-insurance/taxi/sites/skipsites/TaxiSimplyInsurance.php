<?php

   $error             = false;




	//skip BT[1-99]* postcodes
   $postCode     = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BT",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error               =  true;
   }


	// lead times:
	// 8. can quote Tuesday-Thursday - skip hours between 1:00am and 7:00am
	// 9. can quote Friday - skip hours between 1:00am and 7:00am and over 2:00pm
	// 10. cannot quote Saturday - skip all day
	// 11. can quote Sunday - skip hours under 3:00pm
   if($_SERVER["REMOTE_ADDR"] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {

         case '1':
            break;

         case '2':
         case '3':
         case '4':
         if(($timeHh >= 1) AND ($timeHh < 7))
         {
            $personalizedError[] = "SKIP_RULE_DAY_TUE_THU--TIME_SKP_BETW_1:00AM_AND_7:00AM";
            $error               =  true;
         }

            break;

         case '5':  // update times for Friday to stop leads at 2pm 
         if((($timeHh >= 1) AND ($timeHh < 7)) || ($timeHh >= 14))
         {
            $personalizedError[] = "SKIP_RULE_DAY_FRI--TIME_SKP_BETW_1:00AM_AND_7:00AM_AND_OVR_2:00PM";
            $error               =  true;
         }
            break;

         case '6':
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT--TIME_SKP_ALL_DAY";
            $error               =  true;
            break;

         case '7':
            if($timeHh < 15)
            {
               $personalizedError[] = "SKIP_RULE_DAY_SUN--TIME_SKP_UND_3:00PM";
               $error               =  true;
            }
            break;
      }
   }

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 30)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_30";
      $error               = true;
   }
   
   //    skip if Taxi No Claims Bonus = 
   //    No NCB
   //    1 Year
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($taxiNCB < 2)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB_1YEAR";
      $error                       =  true;
   }
   
   //SKIP if Full UK License =  1-2 Years
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence ==  3)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_1_2_YEARS";
      $error               =  true;
   }


   //skip postcodes = 
   $postCode = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "EC",
      "WC",
      "E",
      "N",
      "NW",
      "SE",
      "SW",
      "W",
      "B",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }

   return $error;
?>
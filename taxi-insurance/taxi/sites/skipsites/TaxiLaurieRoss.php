<?php

   $error = false;

   //1. ACCEPT only if "Postcode" = G[0-99]*    ML[0-99]*                                              
   $postCode        = $_SESSION['_YourDetails_']['postcode'];
   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
      "G",
      "ML", 
   );
  
   if(! in_array($postcodeToCheck,$acceptedPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error 			   =  true;
   }

   return $error;

?>

<?php

//////////////////////////////////////////////////////////
/// INACTIVE Plugin. Please Go To ../config/3466_18.inc///
//////////////////////////////////////////////////////////

   $error = false;

   /*1. SKIP if "Postcode" = BT[0-99]*/
   $postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skpPcodesArray = array(
      "BT",
   );

   if(in_array($postcodeToCheck,$skpPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                         =  true;
   }

   /*2. ACCEPT only if "Taxi no claims bonus" = "No NCB"
                                              "1 Year"
                                              "2 Years"
   */

   $taxiNCB    = $_SESSION['_YourDetails_']['taxi_ncb'];
   $taxiNCBArray = array(
      "0",
      "1",
      "2",
   );
   if(! in_array($taxiNCB, $taxiNCBArray))
   {
      $personalizedError[] = "SKIP_RULE_NCB--ACC_LIST [$taxiNCB]";
      $error               =  true;
   }


   //3. If "Taxi type" = "MPV" SKIP if "Max. number of passengers" > 8

   $taxiType     = $_SESSION['_YourDetails_']['taxi_type'];
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiType == 3) //MPV
   {
      if($taxiCapacity > '8')
      {
         $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_8";
         $error                       =  true;
      }
   }
   

   //5. skip if User Age < 25
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   
   if($propAge < 25)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25";
      $error            =  true;
   }

   // 6. ACCEPT only if "Taxi driver(s)" = "Insured Only"
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers != 1)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_ACC_INS_ONLY";
      $error                       =  true;
   }


   /* 7. ACCEPT leads Monday 00:00 - 23:59
                      Tuesday 00:00 - 23:59
                      Wednesday 00:00 - 23:59
                      Thursday 00:00 - 23:59
                      Friday 00:00 - 19:00
                      Saturday NO LEADS
                      Sunday 20:00 - 23:59
   */
   if($_SESSION['USER_IP'] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdNum  = date("N");

      switch($dateDdNum)
      {
         case '6': // Sat
            $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT--TIME_SKP_ALL_DAY";
            $error                           =  true;
         break;

         case '7': // Sun
            if($timeHh <= 19)
            {
               $personalizedError[] = "SKIP_RULE_DAY_SUN--TIME_SKP_UND_8:00PM";
               $error                           =  true;
            }
         break;

         case '1': // Mon
         case '2': // Tue
         case '3': // Wed
         case '4': // Thu
         break;


         case '5': // Fri
            if($timeHh >= 19)
            {
               $personalizedError[] = "SKIP_RULE_DAY_FRI--TIME_SKP_OVR_7:00PM";
               $error                               =  true;
            }
         break;
      }
   }

   
   // SKIP if "Taxi badge" = "No Badge", "Under 6 Months"
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 3)
   {
      $personalizedError[] = "SKIP_RULE_BADG--BADG_SKP_N_BADG_UND6_MONTH";
      $error               =  true;
   }
   
   
   // SKIP if "Insurance Start Date" > "Quote Date" + 6 Days
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   // today date
   $day   = date("d");
   $month = date("m");
   $year  = date("Y");

   $daysOfInsuranceStart = (mktime(0,0,0,$DISmm,$DISdd,$DISyyyy) - mktime(0,0,0,$month,$day,$year)) / (86400);

   if($daysOfInsuranceStart > 6)
   {
      $personalizedError[] = "SKIP_RULE_INS_DATE--INS_STRT_DATE_SKP_6_DAY_OVR";
      $error                       =  true;
   }
   
   
   return $error;

?>

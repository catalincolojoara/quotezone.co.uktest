<?php

   $error = false;

   //Skip if user is less than 25 years old ; Skip if user is greater than than 70 years old
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 28 || $propAge > 70)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_28_AND_OVR_70";
      $error 		   =  true;
   }

   // Skip if UK License is less than "4-5 years"
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence <= 6)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_SKP_UND_4_5_YEARS";
      $error               = true;
   }

   // Skip if Taxi badge is less than "2-3 years"
   $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
   if($taxiBadge < 5)
   {
      $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_2_3_YEARS";
      $error               =  true;
   }

   //Skip if Type of cover = "third party" or "third party fire and theft"
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if( $typeOfCover == 2 || $typeOfCover == 3)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_SKP_TP_TPFT";
      $error               =  true;
   }

   //Skip if Taxi use = "Private hire"
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse == 1)
   {
      $personalizedError[] = "SKIP_RULE_USE--USE_SKP_PRIV_HIRE";
      $error               =  true;
   }

   //Skip if Year of Manufacture  is before 2005
   $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];

   if($yearOfManufacture < 2007)
   {
      $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--YEAR_SKP_UND_2007";
      $error               =  true;
   }
 

   //SKIP if "Max. Number of Passengers" > 8
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity == '8')
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_8";
      $error               =  true;
   }

   // ACCEPT only if "Taxi Driver(s)" = "Insured Only"
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers != 1)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_ACC_INS_ONLY";
      $error                       =  true;
   }

   //Skip the follow list of plating authorities
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $skipLicensingAuthorityArray = array(
      "LONDON PCO",
      "EAST RENFREWSHIRE (GIFFNOCH)",
      "GLASGOW",
      "NORTH LANARKSHIRE (NORTH)",
      "RENFREWSHIRE (PAISLEY)",
      "CARDIFF",
      "BIRMINGHAM",
      "BRACKNELL FOREST",
      "BRISTOL CITY OF UA",
      "BROXTOWE",
      "CALDERDALE",
      "CANNOCK CHASE",
      "CHELMSFORD",
      "COVENTRY",
      "DERBY",
      "ELMBRIDGE",
      "EPSOM & EWELL",
      "KINGSTON UPON THAMES ROYAL",
      "NEWCASTLE UPON TYNE",
      "NORTH TYNESIDE",
      "NOTTINGHAM",
      "PETERBOROUGH",
      "REDCAR & CLEVELAND",
      "WINDSOR & MAIDENHEAD",
      "SANDWELL",
      "SHEFFIELD",
      "SOUTH TYNESIDE",
      "STEVENAGE",
      "STOCKPORT",
      "TAMESIDE",
      "WALSALL",
      "WOLVERHAMPTON",
      "BARKING",
      "BARNET",
      "BEDFORD",
      "BEXLEY",
      "BLACKBURN WITH DARWEN",
      "BLACKPOOL",
      "BOLTON",
      "BRADFORD",
      "BRENT",
      "BROMLEY",
      "BURY",
      "CAMDEN",
      "CROYDEN",
      "DARTFORD",
      "EALING",
      "ENFIELD",
      "LONDON PCO",
      "GREENWICH",
      "HACKNEY",
      "HAMMERSMITH",
      "HARINGEY",
      "HARROGATE",
      "HAVERING",
      "HILLINGDON",
      "HOUNSLOW",
      "ISLINGTON",
      "KENSINGTON",
      "KIRKLEES",
      "KNOWSLEY",
      "LAMBETH",
      "LEEDS",
      "LEWISHAM",
      "LIVERPOOL",
      "LONDON PCO",
      "LUTON",
      "MANCHESTER",
      "MERTON",
      "NEWHAM",
      "OLDHAM",
      "PRESTON",
      "READING",
      "REDBRIDGE",
      "RICHMOND",
      "ROCHDALE",
      "SALFORD",
      "SEFTON",
      "SLOUGH",
      "SOLIHULL",
      "SOUTHWARK",
      "ST HELENS",
      "SUTTON",
      "THREE RIVERS",
      "TOWER HAMLETS",
      "TRAFFORD",
      "WAKEFIELD",
      "WALTHAM FOREST",
      "WANDSWORTH",
      "WATFORD",
      "WESTMINSTER",
      "WIGAN",
      "WIRRALL",

   );

   if(in_array($licesingAuthority,$skipLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
          $error                           =  true;
   }

   //Skip postcodes: BT[1-99]*
   $postCode = $_SESSION['_YourDetails_']['postcode'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $skipPcodesArray = array(
      "BT",
		"BD",
   );

   if(in_array($postcodeToCheck,$skipPcodesArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
      $error                       =  true;
   }

   
   // ACCEPT leads 0900 - 1900 MONDAY - FRIDAY each day
   // ACCEPT Leads SATURDAY 0900 - 1300 
   if($_SERVER["REMOTE_ADDR"] != "86.125.114.56")
   {
      $timeHh     = date("G");
      $timeMin    = date("i");
      $dateDdText = date("N");

      switch($dateDdText)
      {
         case '6': //can quote Saturday
            if($timeHh < 9 || $timeHh >= 13)
            {
               $personalizedError[] = "SKIP_RULE_DAY_SAT--TIME_ACC_BETW_9:00AM_TO_1:00PM";
               $error               = true;
            }
               break;

         case '7':  //Sunday skip
               $personalizedError[] = "SKIP_RULE_SKP_DAY_SUN--TIME_SKP_ALL_DAY";
               $error               = true;
               break;


         case '1':
         case '2': 
         case '3':    
         case '4':           
         case '5':  //fri
               if($timeHh < 9 || $timeHh >= 19)
               {
                  $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_7:00PM";
                  $error =  true;
               }
            break;
      }
   }
   
   
   //filter in case of WL DNAINSURANCE1 leads
   $wlID = $_SESSION['_QZ_QUOTE_DETAILS_']['wlUserID'];

   if($wlID == "688")
   {
      $personalizedError[] = "SKIP_RULE_COMPANY_WL--WL_SKP_DNAINSURANCE1";
      $error               =  true;
   }

   if($wlID == "758")
   {
      $personalizedError[] = "SKIP_RULE_COMPANY_WL--WL_SKP_DNAINSURANCE2";
      $error               =  true;
   }
   
   
   // SKIP if Wl source = dnainsurance3
   $wsID = $_SESSION['_QZ_QUOTE_DETAILS_']['ws_id'];

   if($wsID == "5fdbf8c1a05cd4cdf82ed72104896c7e")
   {
      $personalizedError[] = "SKIP_RULE_COMPANY_WL--WL_SKP_DNAINSURANCE3";
      $error               =  true;
   }
      
      

   return $error;

?>

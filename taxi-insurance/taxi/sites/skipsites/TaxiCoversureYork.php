<?php

   $error = false;



   //Only accept leads for the following Taxi Plating Authorities: composed with postcode filter.
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $acceptLicensingAuthorityArray = array(
"YORK",
"HARROGATE",
"SELBY",
"WAKEFIELD",
"LEEDS",
   );
   $plAuthority = true;
   if( ! in_array($licesingAuthority,$acceptLicensingAuthorityArray))
   {
         $plAuthority = false;
//       $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
//       $error             =  true;
   }




   $postCode        = $_SESSION['_YourDetails_']['postcode'];
   $postCodePreffix = $_SESSION['_YourDetails_']['postcode_prefix'];
   $postCodeSuffix  = $_SESSION['_YourDetails_']['postcode_number']; 
   

   $filter          = "accept";
   $postCodeArray   = array(
         "YO[0-99]*",
         "HG1",
         "HG2",
         "HG5",
         "LS22",
         "LS23", 
         "LS24",
         );

   $filterStatus = "";   

   preg_match("/(.*)\d/isU",$postCodePreffix,$pregLettersToCheck);
   $lettersPostCodePreffix = $pregLettersToCheck[1];

   preg_match("/[0-9]+/",$postCodePreffix,$pregNumbersToCheck);
   $numbersPostCodePreffix = $pregNumbersToCheck[0];

   $prefLettAndNum = $lettersPostCodePreffix.$numbersPostCodePreffix;

   foreach($postCodeArray as $keypostCodeArray=>$skpPostcode)
   {
      if((preg_match("/[A-Z]*/",$skpPostcode,$pregMatchLetters)) && (preg_match("/\[(.*)\-/",$skpPostcode,$pregMinInterval)) && (preg_match("/\-(.*)\]/",$skpPostcode,$pregMaxInterval)))
      {
         $prefixLettersArr = $pregMatchLetters[0];
         $minIntervalArr   = $pregMinInterval[1];
         $maxIntervalArr   = $pregMaxInterval[1];
      }
      else 
      {
         $prefixLettersArr = "";
         $minIntervalArr   = "";
         $maxIntervalArr   = "";
      }
      
      
      //checking postcodes like BT[1-99]*
      if((preg_match("/\*/isU",$skpPostcode,$pregMatchAll)))
      {
         if(($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
         {
            //"postcode has been accepted \n"; //only accept list
            $filterStatus = true;
            break;
         }
      }

      //checking postcodes like BT[1-99]
      elseif(preg_match("/^[A-Z]{1,2}\[[0-9]{1,2}-[0-9]{1,2}\]$/isU",$skpPostcode,$pregMatchAll))
      {
         if(($postCodePreffix == $prefLettAndNum) && ($lettersPostCodePreffix == $prefixLettersArr) && (($numbersPostCodePreffix >= $minIntervalArr)&&($numbersPostCodePreffix <= $maxIntervalArr)))
         {            
            $filterStatus = true;
            break;
         }
      }
      
      //checking postcodes like BT3 9
      elseif((preg_match("/(.*)\s+/isU",$skpPostcode, $pregMatchSpace)))
      {      
      $sufixNumber = substr($postCodeSuffix,0,-2);
      $postCodeToCheck = $postCodePreffix." ".$sufixNumber;
         
         if($skpPostcode == $postCodeToCheck)
         {
            $filterStatus = true;
            break;
         }             
      }

      //checking postcodes like BT339AA OR BT39AA
      elseif(ctype_alpha(substr($skpPostcode,-2)))
      {
      $postCodeStrip = str_replace(" ","",$postCode);

         if($skpPostcode == $postCodeStrip)
         {
            $filterStatus = true;
            break;
         }             
      }

      //checking postcodes preffix like BT1, BT1A
      elseif((preg_match("/^$postCodePreffix$/",$skpPostcode)))
      {
         if($postCodePreffix == $skpPostcode);
         {
            $filterStatus = true;
            break;
         }
      }
   }
   $flagPostcode = true;
   if($filter == "accept")
   {
      if($filterStatus == false)

      {
            $flagPostcode = false;
//          $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]\n";
//          $error               =  true;
      }
   }
   elseif($filter == "skip")
   {
      if($filterStatus==true)
      {

         $personalizedError[] = "SKIP_RULE_POSTCODE--SKP_LIST [$postCode]";
         $error               =  true;
      }
   }

   //filter composed of Plating Authority and Postcode
   if($flagPostcode == false && $plAuthority == false)
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH_POSTCODE--ACC_LIST";
      $error               =  true;
   }


   //ACCEPT if age > 24
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   
   if($propAge <= 24)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_ACC_OVR_24";
      $error            =  true;
   }


   //Skip if Taxi No Claims Bonus = No NCB
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($taxiNCB < 1)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
      $error            =  true;
   }
  

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
      {
         // Only receive leads from Monday to Friday, 9.00am to 5.00pm daily
         $timeHh     = date("G");
         $timeMin    = date("i");
         $dateDdNum  = date("N");

         switch($dateDdNum)
         {
            case '6': // Sat
            case '7': // Sun
               $personalizedError[] = "SKIP_RULE_SKP_DAY_SAT_SUN--TIME_SKP_ALL_DAY";
               $error                           =  true;

               break;

            case '1': // Mon
            case '2': // Tue
            case '3': // Wed
            case '4': // Thu
            case '5': // Fri
               if($timeHh < 9 || $timeHh >= 17)
               {
                  $personalizedError[] = "SKIP_RULE_DAY_MON_FRI--TIME_ACC_BETW_9:00AM_TO_5:00PM";
                  $error                               =  true;
               }
            break;
         }
      }

   // ACCEPT if "Taxi Make" contains the following text string: "Peugeot", "Peaugot", "Peogeot", "Peugeot" "Peugeout", "Pueguet", "Pueget"
   $taxiMake        = $_SESSION['_YourDetails_']['taxi_make'];
   $searchTaxiMake  = array("Peugeot", "Peaugot", "Peogeot", "Peugeot", "Peugeout", "Pueguet", "Pueget");
   $foundMake       = false;
   foreach ($searchTaxiMake as $searchString1)
   { 
      if(preg_match("/\b($searchString1)\b/", $taxiMake))     
      {
          $foundMake = true;
          break;
      }
   }

   if(! $foundMake)
   {
      $personalizedError[] = "SKIP_RULE_MAKE--ACC_LIST";
      $error               = true;
   }


   // ACCEPT if "Taxi Model" contains the following text string: "Expert", "Partner"
   $taxiModel         = $_SESSION['_YourDetails_']['taxi_model'];
   $searchTaxiModel   = array("Expert", "Partner");
   $foundModel        = false;

   foreach($searchTaxiModel as $searchString2)
   {
      if(preg_match("/\b($searchString2)\b/", $taxiModel))     
      {
          $foundModel = true;
          break;
      }
   }

   if(! $foundModel)
   {
      $personalizedError[] = "SKIP_RULE_MODEL--ACC_LIST";
      $error               = true;
   }
   

   if($foundModel == true && $foundMake == true)
   {
      //ACCEPT if "Year Of Manufacture" > 2008 -> E.g. Only Peugeot Expert / Panther vehicles must be newer than 2008 
      $yearOfManufacture = $_SESSION['_YourDetails_']['year_of_manufacture'];

      if($yearOfManufacture <= 2008)
      {
         $personalizedError[] = "SKIP_RULE_MANUFACTURE_YEAR--MAKE_PEUG_MODEL_EXP_AND_PART_ACC_MAN_YEAR_OVR_2008"; 
         $error                       =  true;
      }
   }

// SKIP if "Best Time To Call" = Evening (5.30pm - 8pm)
   $bestTimeCall = $_SESSION['_YourDetails_']['best_time_call'];
   if($bestTimeCall == 4)
   {
      $personalizedError[] = "SKIP_RULE_BST_CALL--TIME_TO_CALL_SKP_EVEN";
      $error               =  true;
   }
   
   
   // ACCEPT if "Full UK Licence" > 2 years
   $ukLicence = $_SESSION['_YourDetails_']['period_of_licence'];
   if($ukLicence <= 3)
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--YEARS_INT_ACC_OVR_2_YEARS";
      $error               = true;
   }
   

return $error;

?>

<?php
 
   $error = false;

   // ACCEPT only if "Max. number of passengers" = 8
   //                                                                              9
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity < 8 || $taxiCapacity > 9)
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_UND_8_OVR_9";
      $error               =  true;
   }

   //ACCEPT only if "Taxi driver(s)" = "Insured Only"
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers != 1)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_ACC_INS_ONLY";
      $error                       =  true;
   }

   //ACCEPT only if "Taxi plating authority" = see attached
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);

   $acceptedLicensingAuthorityArray = array(
"BARNET",
"CAMDEN",
"CROYDEN",
"EALING",
"ENFIELD",
"GREENWICH",
"HACKNEY",
"HARINGEY",
"HAVERING",
"HOUNSLOW",
"ISLINGTON",
"LONDON PCO",
"MERTON",
   );

   if(! in_array($licesingAuthority,$acceptedLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
          $error                           =  true;
   }


    /*ACCEPT leads Monday 06:00 - 23:59
                         Tuesday 06:00 - 23:59
                    Wednesday NO LEADS
                        Thursday 06:00 - 23:59
                             Friday 06:00 - 23:59
                         Saturday NO LEADS
                            Sunday NO  LEADS
  */
   
   
 

   $timeHh     = date("G");
   $dateDdNum  = date("N");
   $timeMin    = date("i");
  
   switch($dateDdNum)
   {
      case '1':
      case '2':
      if($timeHh < 6 )
         {
            $personalizedError[] = "SKIP_RULE_DAY_MON_TUE--TIME_SKP_UND_6:00AM";
            $error               =  true;
         }
         
      break;
      case '3':
         {
            $personalizedError[] = "SKIP_RULE_DAY_WED--TIME_SKP_ALL_DAY";
            $error               =  true;
         }
         
      break;
      case '4':
         if($timeHh < 6 )
         {
            $personalizedError[] = "SKIP_RULE_DAY_THU--TIME_SKP_UND_6:00AM";
            $error               =  true;
         }
         
      break;
      case '5':
         if($timeHh < 6)
         {
            $personalizedError[] = "SKIP_RULE_DAY_FRI--TIME_SKP_UND_6:00AM";
            $error               =  true;
         }
      break;
      case '6':
      case '7':
          $personalizedError[] = "SKIP_RULE_SKP_DAY_SUN_SAT--SKIP_RULE_SKP_DAY_SAT_SUN";
          $error          =  true;
      break;
   }

   // SKIP if "Taxi no claims bonus" = "No NCB"
   //                                                     "1 Year"
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
   if($taxiNCB < 2)
   {
      $personalizedError[] = "SKIP_RULE_NCB--YEARS_INT_SKP_UND_2_YEARS";
      $error               =  true;
   }

   return $error;

?>

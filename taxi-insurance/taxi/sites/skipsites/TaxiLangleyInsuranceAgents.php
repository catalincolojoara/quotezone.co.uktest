<?php

   $error = false;
/*1.ACCEPT if Postcode=
                        DH1-DH9
                        DL1-DL5
                        DL12-DL17
                        NE16
                        NE39
                        TS21
                        TS28
                        TS29
  */


   $postCodePrefix = $_SESSION['_YourDetails_']['postcode_prefix'];
   $skipPcodesPrefixArray = array(
      "DH1",
      "DH2",
      "DH3",
      "DH4",
      "DH5",
      "DH6",
      "DH7",
      "DH8",
      "DH9",
      "DL1",
      "DL2",
      "DL3",
      "DL4",
      "DL5",
      "DL12",
      "DL13",
      "DL14",
      "DL15",
      "DL16",
      "DL17",
      "NE16",
      "NE39",
      "TS21",
      "TS28",
      "TS29",
   );


   if(! in_array($postCodePrefix,$skipPcodesPrefixArray))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error                        =  true;
   }


return $error;
?>

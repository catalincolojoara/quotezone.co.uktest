<?php

   $error = false;


   //ACCEPT only if "Type of cover" = "Fully comprehensive"
   $typeOfCover = $_SESSION['_YourDetails_']['type_of_cover'];
   if($typeOfCover != 1)
   {
      $personalizedError[] = "SKIP_RULE_TYP_COV--COV_TYP_ACC_FULL_COMPR";
      $error         =  true;
   }

   // skip if age is under 25 or over 60 years
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25 || $propAge > 60)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25_AND_OVR_60";
      $error               =  true;
   }

   //SKIP if "Full UK licence" = "1 - 2 Years"
   $periodOfFullUkLicenceHeld = $_SESSION["_YourDetails_"]["period_of_licence"];

   $filteredPeriodOfFullUkLicenceHeldArray = array(
//       "1" => "Under 6 Months",
//       "2" => "6 Months To 1 Year",
      "3" => "1 - 2 Years",
//       "4" => "2 - 3 Years",
//       "5" => "3 - 4 Years",
//       "6" => "4 - 5 Years",
      );

   if(array_key_exists($periodOfFullUkLicenceHeld,$filteredPeriodOfFullUkLicenceHeldArray))
   {
      $personalizedError[] = "SKIP_RULE_FULL_UK_DRV_LICENCE--SKP_LIST [".$filteredPeriodOfFullUkLicenceHeldArray[$periodOfFullUkLicenceHeld]."]";
      $error            =  true;
   }

   //SKIP if "Taxi no claims bonus" = "No NCB"
   $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];

   if($taxiNCB == 0)
   {
      $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
      $error                       =  true;
   }

   //ACCEPT only if "Taxi driver(s)" = "Insured Only"
   $taxiDrivers = $_SESSION['_YourDetails_']['taxi_driver'];
   if($taxiDrivers != 1)
   {
      $personalizedError[] = "SKIP_RULE_VEH_DRIVER--DRV_ACC_INS_ONLY";
      $error                       =  true;
   }

   //SKIP if "Year of manufacture" > "Quote Date" + 9 Years    (For example accept if 1994, 1996, 2010, skip if 1993, 1978, 1990)
   $yearOfManufacture    = $_SESSION['_YourDetails_']['year_of_manufacture'];
   $dateOfInsuranceStartYear = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $vehicleAge = $dateOfInsuranceStartYear - $yearOfManufacture;

   if($vehicleAge > 9)
   {
      $personalizedError[] = "SKIP_RULE_VEH_AGE--AGE_SKP_OVR_9";
      $error            =  true;
   }

   //SKIP if "Max. number of passengers" > 7
   $taxiCapacity = $_SESSION['_YourDetails_']['taxi_capacity'];
   if($taxiCapacity > '7')
   {
      $personalizedError[] = "SKIP_RULE_NO_PASS--NUM_SKP_OVR_7";
      $error            =  true;
   }

   //ACCEPT only attached Taxi Plating Authorities
   $licesingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $acceptedLicensingAuthorityArray = array(
"BASILDON",
"BRAINTREE",
"BRENTWOOD",
"CASTLE POINT",
"CHELMSFORD",
"COLCHESTER",
"EPPING FOREST",
"HARLOW",
"MALDON",
"ROCHFORD",
"TENDRING",
"UTTLESFORD",
"SOUTHEND-ON-SEA",
   );

   if(! in_array($licesingAuthority,$acceptedLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--SKP_LIST [$licesingAuthority]";
      $error               =  true;
   }


   //skip if AffiliateID = DNAInsurance2
   $wlID = $_SESSION['_QZ_QUOTE_DETAILS_']['wlUserID'];
   if($wlID == "758")
   {
      $personalizedError[] = "SKIP_RULE_COMPANY_WL--WL_SKP_DNAINSURANCE2";
      $error               =  true;
   }
   
   return $error;
?>

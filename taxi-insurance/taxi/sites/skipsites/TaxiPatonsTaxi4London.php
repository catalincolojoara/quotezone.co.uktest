<?php

   $error = false;


   //ACCEPT if "Taxi Plating Authority" = "London PCO"
   $licesingAuthority           = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $acceptedLicensingAuthorityArray = array(
         "LONDON PCO",       
   );

   if(! in_array($licesingAuthority,$acceptedLicensingAuthorityArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$licesingAuthority]";
      $error               =  true;
   }

   //ACCEPT If Proposer Age is between 25 - 65 (e.g. accept 25 and accept 65)
   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25 || $propAge > 65)
   {
      $personalizedError[] = "SKIP_RULE_PROP_AGE--AGE_SKP_UND_25_AND_OVR_65";
      $error               = true;
   }


   //SKIP if "Taxi No Claims Bonus" = No NCB
//    $taxiNCB = $_SESSION['_YourDetails_']['taxi_ncb'];
//    if($taxiNCB == 0)
//    {
//       $personalizedError[] = "SKIP_RULE_NCB--NCB_SKP_NO_NCB";
//       $error               = true;
//    }

   //ACCEPT if "Taxi Badge" > 1 year
//    $taxiBadge = $_SESSION['_YourDetails_']['taxi_badge'];
//    if($taxiBadge < 4)
//    {
//       $personalizedError[] = "SKIP_RULE_BADG--YEARS_INT_SKP_UND_1_2_YEARS";
//       $error               = true;
//    }

   //ACCEPT if "Taxi Use" = Public Hire
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse != 2)
   {
      $personalizedError[] = "SKIP_RULE_USE--USE_ACC_PUB_HIRE"; 
      $error               =  true;
   }


      $taxiType = $_SESSION['_YourDetails_']['taxi_type'];
      if($taxiType != 1)
      {
         $personalizedError[] = "SKIP_RULE_VEH_TYP--TYP_ACC_BLACK_CAB";
         $error            =  true;
      }
      
      
   // SKIP all SMS text leads from being sent to Patons Insurance
   if($_SESSION["USER_IP"] == "80.94.200.13")
   {
      $personalizedError[] = "SKIP_RULE_SMS_REMINDER--SKP_13_SERVER";
      $error               =  true;
   }
   

   return $error;

?>
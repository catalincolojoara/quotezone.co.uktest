<?php

   $error = false;

   //acc postcodes.
   $postCode       = $_SESSION['_YourDetails_']['postcode'];
   $postCodePrefix = $_SESSION['_YourDetails_']['postcode_prefix'];

   preg_match("/^(.*)\d/isU",$postCode,$postCodeArr);
   $postcodeToCheck = $postCodeArr[1];

   $acceptedPcodesArray = array(
         "CM",
         "SS",
         "CO",
         "RM",
   );



   if((! in_array($postcodeToCheck,$acceptedPcodesArray)))
   {
      $personalizedError[] = "SKIP_RULE_POSTCODE--ACC_LIST [$postCode]";
      $error                        =  true;
   }


   //ACCEPT if "Taxi Use" = "Private Hire"
   $taxiUse = $_SESSION["_YourDetails_"]["taxi_used_for"];
   if($taxiUse != 1)
   {
      $personalizedError[] = "SKIP_RULE_USE--USE_ACC_PRIV_HIRE";
      $error               =  true;
   }


   // ACCEPT if "Taxi Plating Authority" =
   $platingAuthority = strtoupper($_SESSION['_YourDetails_']['plating_authority']);
   $acceptedPlatingAuthiritiesArray = array(
"CHELMSFORD",
"BRENTWOOD",
"BRAINTREE",
"EPPING FOREST",
"HARLOW",
"MALDON",
"COLCHESTER",
"TENDRING",
"THURROCK",
"BASILDON",
"SOUTHEND-ON-SEA",
"ROCHFORD",
"CASTLE POINT",
"BROXBOURNE",
"EAST HERTS",
"UTTLESFORD",
   );

   if(! in_array($platingAuthority,$acceptedPlatingAuthiritiesArray))
   {
      $personalizedError[] = "SKIP_RULE_LICENCE_AUTH--ACC_LIST [$platingAuthority]";
          $error                           =  true;
   }


   return $error;
?>

<?php

   $_resSite['offline'] = false;

   //QUOTE SEARCHING
   $_resSite['siteID']           = '1634';

   // leave empty if we dont have fake sites
   $_resSite['siteOrigID']       = '';

   include_once "../../modules/globals.inc";
   include_once "Site.php";

   $objSite = new CSite();

   if(! $resSiteDetails = $objSite->GetSite($_resSite['siteID']))
      $objSite->GetError();

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   { 
      if($resSiteDetails['status'] == 'OFF')
      {
         $_resSite['offline'] = true;
         return;
      }
   }

   $_resSite['siteOrigID'] = $resSiteDetails['master_site_id'];

   //new -set offline site from admin - implenent this in case you want to use it from the database
   include_once "SetOfflineLeadsModule.php";
   $objSetOfflineLeads = new CSetOfflineLeadsModule();

   if($objSetOfflineLeads->CheckSetOfflineLeadsStatusBySiteId($_resSite['siteID']))
   {
      $_resSite['offline'] = true;
      return;
   }
   //end new -set offline site from admin - implenent this in case you want to use it from the database
   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      // maximum 1 unique lead per hour
      $leadsLimit = 1;
      $leads = GetSiteLeadsPerPeriod($_resSite['siteID'],"hour");

      if($leads >= $leadsLimit)
      {
         $_resSite['offline'] = true;
         return;
      }
      
      // maximum 5 unique leads per day
      $leadsLimit = 5;
      $leads = GetSiteLeadsPerPeriod($_resSite['siteID'],"day");

      if($leads >= $leadsLimit)
      {
         $_resSite['offline'] = true;
         return;
      }
   }

?>

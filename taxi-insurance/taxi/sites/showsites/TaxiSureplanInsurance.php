<?php

   $_resSite['offline'] = false;

   //QUOTE SEARCHING
   $_resSite['siteID']           = '1746';

   // leave empty if we dont have fake sites
   $_resSite['siteOrigID']       = '';

   include_once "../../modules/globals.inc";
   include_once "Site.php";
   //include_once "QuoteStatus.php";

   $objSite        = new CSite();
   //$objQuoteStatus = new CQuoteStatus();

   if(! $resSiteDetails = $objSite->GetSite($_resSite['siteID']))
      $objSite->GetError();

   $_resSite['offline'] = false;
   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      if($resSiteDetails['status'] == 'OFF')
      {
         $_resSite['offline'] = true;
         return;
      } 
   }

   $_resSite['siteOrigID'] = $resSiteDetails['master_site_id'];

   //new -set offline site from admin - implenent this in case you want to use it from the database
   include_once "SetOfflineLeadsModule.php";
   $objSetOfflineLeads = new CSetOfflineLeadsModule();

   if($objSetOfflineLeads->CheckSetOfflineLeadsStatusBySiteId($_resSite['siteID']))
   {
      $_resSite['offline'] = true;
      return;
   }
   //end new -set offline site from admin - implenent this in case you want to use it from the database


?>

<?php

   $_resSite['offline'] = false;

   //QUOTE SEARCHING
   $_resSite['siteID']           = '897';

   // leave empty if we dont have fake sites
   $_resSite['siteOrigID']       = '';

   include_once "../../modules/globals.inc";
   include_once "Site.php";

   $objSite = new CSite();

   if(! $resSiteDetails = $objSite->GetSite($_resSite['siteID']))
      $objSite->GetError();

   // Database Status
   if($resSiteDetails['status'] == 'OFF')
   {
      $_resSite['offline'] = true;
      return;
   }

   $_resSite['siteOrigID'] = $resSiteDetails['master_site_id'];

   //new -set offline site from admin - implenent this in case you want to use it from the database
   include_once "SetOfflineLeadsModule.php";
   $objSetOfflineLeads = new CSetOfflineLeadsModule();

   if($objSetOfflineLeads->CheckSetOfflineLeadsStatusBySiteId($_resSite['siteID']))
   {
      $_resSite['offline'] = true;
      return;
   }
   //end new -set offline site from admin - implenent this in case you want to use it from the database

   // leads limit : 40 per month
   $leadsLimit = 40;
   $leads = GetSiteLeadsPerPeriod($_resSite['siteID'],"month");

   if($leads >= $leadsLimit)
   {
      $_resSite['offline'] = true;
      return;
   }

?>

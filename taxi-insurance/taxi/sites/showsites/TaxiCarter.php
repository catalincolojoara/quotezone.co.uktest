<?php

   $_resSite['offline'] = false;

   //QUOTE SEARCHING
   $_resSite['siteID']           = '1828';

   // leave empty if we dont have fake sites
   $_resSite['siteOrigID']       = '';

   include_once "../../modules/globals.inc";
   include_once "Site.php";

   $objSite = new CSite();

   if(! $resSiteDetails = $objSite->GetSite($_resSite['siteID']))
      $objSite->GetError();

   if($resSiteDetails['status'] == 'OFF')
   {
      $_resSite['offline'] = true;
      return;
   }

   $_resSite['siteOrigID'] = $resSiteDetails['master_site_id'];

   //new -set offline site from admin - implenent this in case you want to use it from the database
   include_once "SetOfflineLeadsModule.php";
   $objSetOfflineLeads = new CSetOfflineLeadsModule();

   if($objSetOfflineLeads->CheckSetOfflineLeadsStatusBySiteId($_resSite['siteID']))
   {
      $_resSite['offline'] = true;
      return;
   }
   //end new -set offline site from admin - implenent this in case you want to use it from the database

   //MAXIMUM 20 unique leads
   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      $leadsLimitAll = 20;
      $leadsAll      = GetSiteLeadsPerPeriod($_resSite['siteID'],"all");

      //send alert emails
      //Can you send an email to leadgen@seopa.com whenever they have received 10 unique leads AND when they have 20 unique leads

      $leadsNoToSendEmail = $leadsAll + 1;

      //test
      $message = "Carter Insurance has received $leadsNoToSendEmail in all for Taxi\n\n Cheers \n\n";
      $subject = "Carter Insurance has received $leadsNoToSendEmail in all for Taxi";

      if($leadsAll == "19")
      {
         mail('alexandru.furtuna@seopa.com', $subject, $message);
         mail('bogdan.turi@seopa.com', $subject, $message);
         mail('leadgen@seopa.com', $subject, $message);
      }

      if($leadsAll >= $leadsLimitAll)
      {
         $_resSite['offline'] = true;
         return;
      }
   }
?>

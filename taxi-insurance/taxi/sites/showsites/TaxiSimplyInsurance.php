<?php

   $_resSite['offline'] = false;

   //QUOTE SEARCHING
   $_resSite['siteID']           = '3202';

   // leave empty if we dont have fake sites
   $_resSite['siteOrigID']       = '';

   include_once "../../modules/globals.inc";
   include_once "Site.php";

   $objSite = new CSite();

   if(! $resSiteDetails = $objSite->GetSite($_resSite['siteID']))
      $objSite->GetError();

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      if($resSiteDetails['status'] == 'OFF')
      {
         $_resSite['offline'] = true;
         return;
      }
   }

   //new -set offline site from admin - implenent this in case you want to use it from the database
   include_once "SetOfflineLeadsModule.php";
   $objSetOfflineLeads = new CSetOfflineLeadsModule();

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      if($objSetOfflineLeads->CheckSetOfflineLeadsStatusBySiteId($_resSite['siteID']))
      {
         $_resSite['offline'] = true;
         return;
      }
   }

   //end new -set offline site from admin - implenent this in case you want to use it from the database
   


	// add limit;
	// Maximum of 7 unique leads per day
	// Maximum 1 unique lead per hour
	// maximum 200 leads per all
   if($_SESSION['USER_IP'] != "86.125.114.56")
   {
         $leadsLimit = 7;
         $leads = GetSiteLeadsPerPeriod($_resSite['siteID'],"day");

         if($leads >= $leadsLimit)
         {
            $_resSite['offline'] = true;
            return;
         }

         $leadsLimit = 1;
         $leads = GetSiteLeadsPerPeriod($_resSite['siteID'],"hour");

         if($leads >= $leadsLimit)
         {
            $_resSite['offline'] = true;
            return;
         }


         $leadsLimit = 250;
         $leads = GetSiteLeadsPerPeriod($_resSite['siteID'],"all");

         if($leads >= $leadsLimit)
         {
            $_resSite['offline'] = true;
            return;
         }
   }
?>

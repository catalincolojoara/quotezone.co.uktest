<?php

   $_resSite['offline'] = false;

   //QUOTE SEARCHING
   $_resSite['siteID']           = '511';

   // leave empty if we dont have fake sites
   $_resSite['siteOrigID']       = '';

   include_once "../../modules/globals.inc";
   include_once "Site.php";
   //include_once "QuoteStatus.php";

   $objSite        = new CSite();
   //$objQuoteStatus = new CQuoteStatus();

   if(! $resSiteDetails = $objSite->GetSite($_resSite['siteID']))
      $objSite->GetError();

   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      if($resSiteDetails['status'] == 'OFF')
      {
         $_resSite['offline'] = true;
         return;
      }
   }

   $_resSite['siteOrigID'] = $resSiteDetails['master_site_id'];

   //new -set offline site from admin - implenent this in case you want to use it from the database
   
   if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
   {
      include_once "SetOfflineLeadsModule.php";
      $objSetOfflineLeads = new CSetOfflineLeadsModule();

      if($objSetOfflineLeads->CheckSetOfflineLeadsStatusBySiteId($_resSite['siteID']))
      {
         $_resSite['offline'] = true;
         return;
      }
   }
   //end new -set offline site from admin - implenent this in case you want to use it from the database


   //WR29-3FJ 
//    $todayDate = date("Y-m-d");
// 
//    $datesInWhichWeSendJust10LeadsPerDay = array(
//       "2011-09-06",
//       "2011-09-08",
//       "2011-09-12",
//       "2011-09-13",
//       "2011-09-14",
//       "2011-09-15",
//       "2011-09-16",
//       "2011-09-19",
//       "2011-09-29",
//       "2011-09-30",
//       "2011-10-03",
//       "2011-10-04",
//       "2011-10-05",
//       "2011-10-06",
//       "2011-10-07",
//       "2011-10-17",
//       "2011-10-18",
//       "2011-10-19",
//       "2011-10-20",
//       "2011-10-21",
//       "2011-10-24",
//       "2011-10-27",
//       "2011-10-28",
//       "2011-11-02",
//    );
// 
//    $leadLimitsOnSpecialDate = 10;
//    $leads = GetSiteLeadsPerPeriod($_resSite['siteID'],"day");
// 
//    if(in_array($todayDate,$datesInWhichWeSendJust10LeadsPerDay))
//    {
//       if($leads >= $leadLimitsOnSpecialDate)
//       {
//          $_resSite['offline'] = true;
//          return;
//       }
//    }

   // leads limit : 27 per day
   
    if($_SERVER['REMOTE_ADDR'] != "86.125.114.56")
    {
      $leadsLimit = 27;

      $leads = GetSiteLeadsPerPeriod($_resSite['siteID'],"day");

      if($leads >= $leadsLimit)
      {
         $_resSite['offline'] = true;
         return;
      }
    }

?>

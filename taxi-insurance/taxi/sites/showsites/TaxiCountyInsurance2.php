<?php

   $_resSite['offline'] = false;

   //QUOTE SEARCHING
   $_resSite['siteID']           = '2714';

   // leave empty if we dont have fake sites
   $_resSite['siteOrigID']       = '';

   include_once "../../modules/globals.inc";
   include_once "Site.php";

   $objSite = new CSite();

   if(! $resSiteDetails = $objSite->GetSite($_resSite['siteID']))
      $objSite->GetError();

   if($_SESSION["USER_IP"] != "86.125.114.56")
   {
      if($resSiteDetails['status'] == 'OFF')
      {
         $_resSite['offline'] = true;
         return;
      }
   }

   //new -set offline site from admin - implenent this in case you want to use it from the database
   if($_SESSION["USER_IP"] != "86.125.114.56")
   {
      include_once "SetOfflineLeadsModule.php";
      $objSetOfflineLeads = new CSetOfflineLeadsModule();

      if($objSetOfflineLeads->CheckSetOfflineLeadsStatusBySiteId($_resSite['siteID']))
      {
         $_resSite['offline'] = true;
         return;
      }
   }
   //end new -set offline site from admin - implenent this in case you want to use it from the database
    

   if($_SESSION["USER_IP"] != "86.125.114.56")
   {
      //MAXIMUM 1 unique leads per hour
      $leadsLimitHour = 1;
      $leads = GetSiteLeadsPerPeriod($_resSite['siteID'],"hour");

      if($leads >= $leadsLimitHour)
      {
         $_resSite['offline'] = true;
         return;
      }


      //MAXIMUM 5 unique leads per day
      $leadsLimit = 5;
      $leads = GetSiteLeadsPerPeriod($_resSite['siteID'],"day");

      if($leads >= $leadsLimit)
      {
         $_resSite['offline'] = true;
         return;
      }
   }  

?>

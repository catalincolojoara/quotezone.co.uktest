<?php

   $_resSite['offline'] = false;

   //QUOTE SEARCHING
   $_resSite['siteID']           = '3228';

   // leave empty if we dont have fake sites
   $_resSite['siteOrigID']       = '';

   include_once "../../modules/globals.inc";
   include_once "Site.php";

   $objSite = new CSite();

   if(! $resSiteDetails = $objSite->GetSite($_resSite['siteID']))
      $objSite->GetError();

   if($_SESSION['USER_IP'] != "86.125.114.56")
	{
		if($resSiteDetails['status'] == 'OFF')
		{
			$_resSite['offline'] = true;
			return;
		}  
	}

   $_resSite['siteOrigID'] = $resSiteDetails['master_site_id'];

   //new -set offline site from admin - implenent this in case you want to use it from the database
   include_once "SetOfflineLeadsModule.php";
   $objSetOfflineLeads = new CSetOfflineLeadsModule();

   if($objSetOfflineLeads->CheckSetOfflineLeadsStatusBySiteId($_resSite['siteID']))
   {
      $_resSite['offline'] = true;
      return;
   }
   //end new -set offline site from admin - implenent this in case you want to use it from the database
   if($_SESSION['USER_IP'] != "86.125.114.56")
	{
		// Maximum 50 leads per week
      $leadsLimit = 50;
		$leads = GetSiteLeadsPerPeriod($_resSite['siteID'],"week");

		if($leads >= $leadsLimit)
		{
			$_resSite['offline'] = true;
			return;
		}
		
		// Maximum 10 leads per day		
		$leadsLimit = 10;
      $leads = GetSiteLeadsPerPeriod($_resSite['siteID'],"day");

      if($leads >= $leadsLimit)
      {
         $_resSite['offline'] = true;
         return;
      }
	}


?>

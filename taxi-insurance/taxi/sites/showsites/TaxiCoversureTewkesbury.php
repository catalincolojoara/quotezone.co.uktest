<?php

   $_resSite['offline'] = false;

   //QUOTE SEARCHING
   $_resSite['siteID']           = '1360';

   // leave empty if we dont have fake sites
   $_resSite['siteOrigID']       = '';

   include_once "../../modules/globals.inc";
   include_once "Site.php";
   include_once "QuoteStatus.php";

   $objSite        = new CSite();
   //$objQuoteStatus = new CQuoteStatus();

    // check site status
    if(! $resSiteDetails = $objSite->GetSite($_resSite['siteID']))
       $objSite->GetError();

    $_resSite['offline'] = false;
    if($resSiteDetails['status'] == 'OFF')
       $_resSite['offline'] = true;

   //start new -set offline site from admin - implenent this in case you want to use it from the database

   include_once "SetOfflineLeadsModule.php";
   $objSetOfflineLeads = new CSetOfflineLeadsModule();

   if($objSetOfflineLeads->CheckSetOfflineLeadsStatusBySiteId($_resSite['siteID']))
      $_resSite['offline'] = true;

   //end new -set offline site from admin - implenent this in case you want to use it from the database

   if($_SESSION['USER_IP'] != "86.125.114.56")
   {
         //MAXIMUM 2 Leads per day
         $leadsLimit = 2;
         $leads = GetSiteLeadsPerPeriod($_resSite['siteID'],"day");

         if($leads >= $leadsLimit)
         {
            $_resSite['offline'] = true;
            return;
         }
   }

?>
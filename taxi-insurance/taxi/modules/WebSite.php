<?php

/*****************************************************************************/
/*                                                                           */
/*  CWebSite class interface                                                 */
/*                                                                           */
/*  (C) 2004 Eugen Savin (seugen@abc.ro)                                     */
/*                                                                           */
/*****************************************************************************/

define("WEB_SITE_INCLUDED", "1");

include_once "Template.php";
include_once "MySQL.php";
include_once "User.php";
include_once "IpBan.php";

if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CWebSite
//
// [DESCRIPTION]:  CWebSite class interface
//
// [FUNCTIONS]:    void Initialise();
//                 void SetTitle($title = "");
//
//                 virtual bool Prepare();
//                 void SetWorkArea($workAreaContent = "");
//                 void Show();
//                 bool Run();
//
//                 dbh  GetDbHandle();
//                 int  GetUserID();
//                 user GetUserObj();
//                 string GetTmplsDir();
//
//                 void Error($errString = "");
//                 string GetError();
//                 void SetOfflineMessage($message);
//                 void Close();
//
// [CREATED BY]:   Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CWebSite
{
    // class-internal variables
    // database handler
    var $dbh;         // database server handle
    var $userObj;     // user object

    var $userID;      // user ID if logged on
    var $userName;    // user Name if logged on
    var $userEmail;   // user Email if logged on
    var $admin;       // admin name

    var $menuType;           // menu type related to user's actions
    var $siteContent;        // web site content
    var $siteTitle;          // the title of the shown web page
    var $templates;          // templates array("file name" => "file contents")
    var $templatesDirectory; // templates directory
    var $loadTemplates;      // keep the templates that will be pre-loaded in this array
    var $metaTags;           // key the meta tags entries array

    var $googleAds;   // google ads flag
    var $msnAds;      // msn ads flag
    var $overtureAds; // overture ads flag
    var $amadesaTracker; // amadesa js tracker
    var $advivaTracker; // adviva js tracker
    var $startAdvivaTracker; // adviva js tracker
    var $quoteUserID;        // quote user id used by the google/indextools javascript
    var $trackerPage;        // used for tracking purposes

    var $piwikTracker;     //piwik js tracker
    var $piwikUserTracking;  //tracks piwik user details

    var $searchVisionAds;    // searchvision ads flag
    var $svQuoteUserID;      //quoteUserId for searchvision
    var $svAmount;           // esv amount

    var $googleTracker;      // google tracker
    var $googleStep;         // google step

    var $strERR;      // last SITE error string

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CWebSite
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CWebSite($templatesDir="")
{
   $this->strERR = "";

   $this->userID    = 0;
   $this->userName  = "";
   $this->userEmail = "";
   $this->admin     = "";

   $this->menuType = COMMON_MENU;

   $this->siteContent = "";
   $this->siteTitle   = "";
   $this->templates   = array();
   $this->loadTemplates = array();
   $this->metaTags['robots']      = 'index,follow'; //default

   $this->imageLevel         = "0";
   $this->imageDir           = "";
   $this->googleAds          = false;
   $this->googleTracker      = false;
   $this->googleStep         = "";
   $this->advivaTracker      = false;
   $this->startAdvivaTracker = false;
   $this->overtureAds        = false;
   $this->msnAds             = false;
   $this->amadesaTracker     = '';
   $this->quoteUserID        = 0;
   $this->trackerPage        = "";

   $this->piwikTracker       = '';
   $this->piwikUserTracking  = '';

   $this->searchVisionAds    = false;
   $this->svQuoteUserID      = 0;
   $this->svAmount           = 0;

   $this->imagesPreloaded       = "";

   // read template files
   $this->templatesDirectory = TEMPLATES_DIR;

   if(! empty($templatesDir))
      $this->templatesDirectory = $templatesDir;

   $this->Initialise();

   $objIpBan = new CIpBan();
   if($objIpBan->IsIpBanned($_SERVER['REMOTE_ADDR']))
      $this->SetBannedIpWebsite();

// uncomment this if you want to put offline all websites car, van , home , bike ...
//   $this->SetOfflineWebsite();

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Initialise
//
// [DESCRIPTION]:   Initialises the web site class.
//                  - initialise php session
//                  - get userID in case someone's logged in
//                  - get menu type
//                  - read all files located  in the templates dir and put them
//                    into an array: key = file name, value = file content
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Initialise()
{
   session_start();

   $this->menuType  = COMMON;
   $this->userID    = 0;
   $this->userName  = "";
   $this->userEmail = "";
   $this->admin     = $_SERVER["REMOTE_USER"];

   $this->dbh = new CMySQL();

   if(! $this->dbh->Open(DBNAME, DBHOST, DBUSER, DBPASS))
   {
      $this->strERR = $this->dbh->GetError();
      return;
   }

   $this->userObj = new CUser($this->dbh);

   if(! empty($_SESSION["session_key"]))
   {
      $this->userID    = $this->userObj->GetUserID($_SESSION["session_key"]);
      $this->userEmail = $_SESSION["userEmail"];

      if($this->userID)
         $this->menuType = SIMPLE_USER;
   }

   if(! empty($this->admin))
      $this->menuType = ADMIN_USER;

   if(! is_dir($this->templatesDirectory))
   {
      $this->strERR = GetErrorString("DIR_NOT_FOUND").": [".$this->templatesDirectory."]";
      return;
   }

   $this->loadTemplates = explode(" ", PRELOAD_TEMPLATES);
   foreach($this->loadTemplates as $tmplFile)
   {
      if(! ($fh = fopen($this->templatesDirectory."/$tmplFile", "r")))
      {
         $this->strERR = GetErrorString("CANNOT_OPEN_FILE").": [".$this->templatesDirectory."/$tmplFile]";
         return;
      }

      $this->templates[$tmplFile] = fread($fh, filesize($this->templatesDirectory."/$tmplFile"));
      fclose($fh);
   }

   // rewriting header in case of custom affiliate header
   //if($_SERVER['REMOTE_ADDR'] == '86.125.114.56')
   {
      if ($_COOKIE['AFFID'])
      {
         include_once "Affiliates.php";
         $objAff = new CAffiliates();

         $affiliateAid = $_COOKIE['AFFID'];
         list($affPrefix,$affSuffix) = explode("-",$affiliateAid);

         $affiliateID = $objAff->GetAffiliatesIDBySufixAndPrefix($affPrefix,$affSuffix);

         $affiliateHeaderFile = 'affiliates/'.$affiliateID.'/Header.tmpl';
         //clearstatcache();

         if (file_exists(TEMPLATES_DIR."/".$affiliateHeaderFile))
         {
            $this->SetHeaderTmpl($affiliateHeaderFile);

            $_SESSION['HEADER_OVERWRITEN'] = "1";
         }
      }
   }


  return;

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetTitle
//
// [DESCRIPTION]:   Set the web site title
//
// [PARAMETERS]:    $title = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetTitle($title = "")
{
   $this->siteTitle = $title;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetMetaTag
//
// [DESCRIPTION]:   Set the meta tags of pages
//
// [PARAMETERS]:    $name="", $content=""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel Istvancsek (null@seopa.com) 2010-07-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetMetaTag($name="", $content)
{
   if(empty($name))
      return;
      
   if(empty($content))
      return;

   $this->metaTags[$name] = $content;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetHeaderTmpl
//
// [DESCRIPTION]:   Set the header template
//
// [PARAMETERS]:    $tmplName = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetHeaderTmpl($tmplName = "Header.tmpl")
{

      if(! ($fh = fopen($this->templatesDirectory."/$tmplName", "r")))
      {
         $this->strERR = GetErrorString("CANNOT_OPEN_FILE").": [".$this->templatesDirectory."/$tmplName]";
         return;
      }

      $this->templates['Header.tmpl'] = fread($fh, filesize($this->templatesDirectory."/$tmplName"));
      fclose($fh);
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: LoadTooltipFile
//
// [DESCRIPTION]:   Load tooltip file
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:
//
// [CREATED BY]:    Ando Ciupav Cristian, @2010
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function LoadTooltipFile()
{
    $objTemplate = new CTemplate();
    $objTemplate->SetTemplateDirectory($this->templatesDirectory);
    $objTemplate->Load('tooltip.tmpl');
    return $objTemplate->GetContent();

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: LoadStaticTooltipFile
//
// [DESCRIPTION]:   Load static tooltip file
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:
//
// [CREATED BY]:    Ando Ciupav Cristian, @2010
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function LoadStaticTooltipFile()
{
    $objTemplate = new CTemplate();
    $objTemplate->SetTemplateDirectory($this->templatesDirectory);
    $objTemplate->Load('staticTooltip.tmpl');
    return $objTemplate->GetContent();
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Prepare
//
// [DESCRIPTION]:   Prepare the web site. Replace template vars
//                  with the values based on a specific algorithm we define below
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Prepare()
{
   if(! empty($this->strERR))
      return false;

   $tmpl = new CTemplate();

   $tmpl->SetTemplateDirectory($this->templatesDirectory);

   if(! $tmpl->Load(WEB_SITE_FIRST_TMPL))
   {
      $this->strERR = $tmpl->GetError();
      return false;
   }

   $tmplVars = array();

   $tmplVars["site.title"]  = $this->siteTitle;
   $tmplVars["site.header"] = $this->templates['Header.tmpl'];
   $tmplVars["site.footer"] = $this->templates['Footer.tmpl'];

   foreach($this->metaTags as $name => $content)
   {
      $tmplVars["site.meta"]  .= "<meta name=\"".$name."\" content=\"".$content."\"> \n";
   }

   if($this->menuType == COMMON)
      $tmplVars["site.menu"] = $this->templates['CommonMenu.tmpl'];
   else if($this->menuType == SIMPLE_USER)
      $tmplVars["site.menu"] = $this->templates['UserMenu.tmpl'];
   else if($this->menuType == ADVANCED_USER)
      $tmplVars["site.menu"] = $this->templates['AdvancedMenu.tmpl'];
   else if($this->menuType == ADMIN_USER)
      $tmplVars["site.menu"] = $this->templates['AdminMenu.tmpl'];

   $tmplVars["site.work_area"] = $this->workAreaContent;

   $tmplVars["SITE.TOOLTIP"] = $this->LoadTooltipFile();
   $tmplVars["SITE.STATIC_TOOLTIP"] = $this->LoadStaticTooltipFile();

   $tmplVars["header.menu"] = $this->templates['HeaderMenu.tmpl'];

   $tmplVars["DIR"] = $this->imageDir;

   $tmplVars["LEVEL"] = $this->imageLevel;

   $tmplVars["IMAGES"] = $this->imagesPreloaded;

   if($this->googleAds)
   {
      $tmplGoogleAds = new CTemplate();

      if(! $tmplGoogleAds->Load("GoogleAds.tmpl"))
      {
         $this->strERR = $tmplGoogleAds->GetError();
         return false;
      }

      $googlePrepArray["ESORDER_ID"] = $this->svQuoteUserID;
      $googlePrepArray["ESAMOUNT"]   = $this->svAmount;

      $tmplGoogleAds->Prepare($googlePrepArray);

      $tmplVars["GOOGLE_ADS"] = $tmplGoogleAds->GetContent();
   }

   //set steps details for each system
   if($this->googleTracker)
   {
      $tmplVars["GOOGLE_STEP"] = $this->googleStep;
   }


   if($this->overtureAds)
   {
      $tmplOvertureAds = new CTemplate();
      if(! $tmplOvertureAds->Load("OvertureAds.tmpl"))
      {
         $this->strERR = $tmpl->GetError();
         return false;
      }

      $tmplVars["OVERTURE_ADS"] = $tmplOvertureAds->GetContent();
   }

   if($this->advivaTracker)
   {
      $tmplAdviva = new CTemplate();

      if(! $tmplAdviva->Load("AdvivaTracker.tmpl"))
      {
         $this->strERR = $tmplAdviva->GetError();
         return false;
      }

      $tmplVars["ADVIVA_TRACKER"] = $tmplAdviva->GetContent();
   }

   if($this->startAdvivaTracker)
   {
      $tmplAdviva = new CTemplate();

      if(! $tmplAdviva->Load("StartAdvivaTracker.tmpl"))
      {
         $this->strERR = $tmplAdviva->GetError();
         return false;
      }

      $tmplVars["ADVIVA_TRACKER"] = $tmplAdviva->GetContent();
   }

   if($this->piwikTracker)
   {
      $tmplPiwikTracker = new CTemplate();

      if(! $tmplPiwikTracker->Load("PiwikTracker.tmpl"))
      {
         $this->strERR = $tmplPiwikTracker->GetError();
         return false;
      }

      if($_COOKIE['AFFID'])
         $this->piwikTracker = $this->piwikTracker."_affiliate";

      $piwikPrepArray["virtual_document_title"] = $this->piwikTracker;
      $piwikPrepArray["user_tracking"]          = $this->piwikUserTracking;
 
      $tmplPiwikTracker->Prepare($piwikPrepArray);

      $tmplVars["PIWIK_TRACKER"] = $tmplPiwikTracker->GetContent();
   }

   if($this->searchVisionAds)
   {
      $tmplSearchVisionAds = new CTemplate();

      if(! $tmplSearchVisionAds->Load("SearchVisionTaxi.tmpl"))
      {
         $this->strERR = $tmplSearchVisionAds->GetError();
         return false;
      }

      $searchVisionTaxiPrepArray["ESORDER_ID"] = $this->svQuoteUserID;
      $searchVisionTaxiPrepArray["ESAMOUNT"]   = $this->svAmount;
      $tmplSearchVisionAds->Prepare($searchVisionTaxiPrepArray);

      $tmplVars["SEARCH_VISION_TAXI"] = $tmplSearchVisionAds->GetContent();
   }


   if($this->amadesaTracker)
      $tmplVars["amadesa_tracker"] = $this->amadesaTracker;

   if($this->trackerPage)
      $tmplVars["tracker_page"] = $this->trackerPage;

   if($this->msnAds)
   {
      $tmplMsnAds = new CTemplate();
      if(! $tmplMsnAds->Load("MsnAds.tmpl"))
      {
         $this->strERR = $tmpl->GetError();
         return false;
      }

      $tmplVars["MSN_ADS"] = $tmplMsnAds->GetContent();
   }

   // set current year
   $tmplVars["current_year.value"] = date("Y");

   // testing bike navigation menu
   $tmplVars["MENU"] = $this->menu;

   // load templates trough ajax
   $tmplVars["SHOW_TEMPLATE"] = $this->showTemplate;

   $tmpl->Prepare($tmplVars);

   $this->siteContent = $tmpl->GetContent();

   // clean up some session vars
   unset($_SESSION["loginErrorMessage"]);

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetSearchVisionAds
//
// [DESCRIPTION]:   set searchvision ads for taxi
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alex (alexandru.furtuna@seopa.com) 2009-11-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetSearchVisionAds($quoteUserID, $esvAmount)
{
   $this->searchVisionAds = true;
   $this->svQuoteUserID   = $quoteUserID;
   $this->svAmount        = $esvAmount;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetMenu
//
// [DESCRIPTION]:   Set the menu
//
// [PARAMETERS]:    $menuContent = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel ISTVANCSEK (null@seopa.com) 2007-03-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetMenu($menuContent = "")
{
   //print($menuContent);
   $this->menu = $menuContent;

   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetLoadTemplates
//
// [DESCRIPTION]:   Set the menu
//
// [PARAMETERS]:    $menuContent = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel ISTVANCSEK (null@seopa.com) 2007-03-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetShowTemplate($template = "")
{
   $this->showTemplate = "ShowTemplate('$template')";

   return;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetWorkArea
//
// [DESCRIPTION]:   Set the work area content
//
// [PARAMETERS]:    $workAreaContent = ""
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetWorkArea($workAreaContent = "")
{
   $this->workAreaContent = $workAreaContent;

   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Show
//
// [DESCRIPTION]:   Show template content
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Show()
{
   print $this->siteContent;
   flush();
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetImageHeaderLevel
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetImageHeaderLevel($imageLevel)
{
   $this->imageLevel = $imageLevel;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetImageHeaderLevel
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetImagesPreloaded($images)
{
   foreach($images as $name)
   {
      $this->imagesPreloaded .= "'".$name."',";
   }

   $this->imagesPreloaded = substr($this->imagesPreloaded, 0, strlen($this->imagesPreloaded) - 1);

}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetImageHeaderLevel
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetImageDir($imageDir="")
{
   $this->imageDir = $imageDir;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetGoogleAds
//
// [DESCRIPTION]:   set searchvision ads for 4x4
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alex (alexandru.furtuna@seopa.com) 2009-11-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetGoogleAds($quoteUserID, $esvAmount)
{
   $this->googleAds       = true;
   $this->svQuoteUserID   = $quoteUserID;
   $this->svAmount        = $esvAmount;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetGoogleTracker
//
// [DESCRIPTION]:   set searchvision ads for 4x4
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alex (alexandru.furtuna@seopa.com) 2009-11-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetGoogleTracker($googleStep)
{
   $this->googleTracker = true;
   $this->googleStep    = $googleStep;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetOvertureAds
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetOvertureAds()
{
   $this->overtureAds = true;
}


//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetAmadesaTracker
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetAmadesaTracker($amadesaCode='')
{
   $this->amadesaTracker = $amadesaCode;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetTrackerPage
//
// [DESCRIPTION]:   set the page name for the tracker system
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetTrackerPage($pageName='')
{
   $this->trackerPage = $pageName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetMsnAds
//
// [DESCRIPTION]:   set the name of image from header
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetMsnAds()
{
   $this->msnAds = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetPiwikTracker
//
// [DESCRIPTION]:   sets the piwik title in tracking code
//
// [PARAMETERS]:    $documentTitle
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetPiwikTracker($documentTitle='',$userTracking='')
{
   $this->piwikTracker      = $documentTitle;
   $this->piwikUserTracking = $userTracking;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetAdvivaTracker
//
// [DESCRIPTION]:   set the adviva tracker
//
// [PARAMETERS]:    $quoteUserID=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Racu Daniel (null@seopa.com) 2008-06-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetAdvivaTracker()
{
   $this->advivaTracker = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetAdvivaTracker
//
// [DESCRIPTION]:   set the adviva tracker
//
// [PARAMETERS]:    $quoteUserID=0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Racu Daniel (null@seopa.com) 2008-06-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetStartAdvivaTracker()
{
   $this->startAdvivaTracker = true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Run
//
// [DESCRIPTION]:   Run the website
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Run()
{
   if(! $this->Prepare())
      return false;

   $this->PrepareCookie();

   $this->Show();

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetDbHandle
//
// [DESCRIPTION]:   Return the database handle
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  database handle
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetDbHandle()
{
   return $this->dbh;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserID
//
// [DESCRIPTION]:   Return the user ID
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  user ID
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserID()
{
   return $this->userID;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetUserObj
//
// [DESCRIPTION]:   Return the user object
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  user object
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetUserObj()
{
   return $this->userObj;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetTmplDir
//
// [DESCRIPTION]:   Return the templates directory
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  string
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetTmplDir()
{
   return TEMPLATES_DIR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Error
//
// [DESCRIPTION]:   Show error and exit program!
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Error($errString = "")
{
   $this->SetWorkArea($errString);
   $this->Run();
   exit(0);
}

//////////////////////////////////////////////////////////////////////////////FB
// [FUNCTION NAME]: GetError
//
// [DESCRIPTION]:   Retrieve the last error message
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  Error string if there is any, empty string otherwise
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetError()
{
    return $this->strERR;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ShowPopupDivContent($textToSet="")
//
// [DESCRIPTION]:   show the div on the page
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function  ShowPopupDivContent($textToSet="")
{
  $this->showPopUpDiv = $textToSet;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    $this->dbh->Close();
    $this->templates   = array();
    $this->siteContent = "";

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetOfflineWebsite
//
// [DESCRIPTION]:   Show offline website message
//
// [PARAMETERS]:    $message
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel Istvancsek (null@seopa.com) 2007-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetOfflineWebsite($message='This site is currently under maintenance. Please come back in 10 minutes.')
{
    $this->Error("<br><br><br><br><b>$message<br><br><br><br>");

    return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetBannedIpWebsite
//
// [DESCRIPTION]:   Show ip banned website message
//
// [PARAMETERS]:    $message
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel Istvancsek (null@seopa.com) 2007-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetBannedIpWebsite()
{
    $objTemplate = new CTemplate();

    $objTemplate->SetTemplateDirectory($this->templatesDirectory);
    $objTemplate->Load('IpBanned.tmpl');
    $objTemplate->Show();
    exit(0);

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckCookieSessionID
//
// [DESCRIPTION]:   Check cookie session id
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none | string
//
// [CREATED BY]:    Gabriel Istvancsek (null@seopa.com) 2007-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function PrepareCookie()
{
   if(! $cookieSession = $this->GetCookieSessionID())
      return;

   if(! preg_match_all("/\<form.*action=\"(.+)\".*\>/isU",$this->siteContent, $matches))
      return;

   //unique urls
   foreach($matches[1] as $index => $url)
      $resAllUrls[$url] = '1';

   foreach($resAllUrls as $url => $value)
   {
      $prepUrl = $url."?cs=$cookieSession";
      if(preg_match("/\?/", $url))
         $prepUrl = $url."&cs=$cookieSession";

      //$prepUrl = preg_quote($prepUrl);
      $url     = preg_quote($url);
      $this->siteContent = preg_replace("/$url/", "$prepUrl", $this->siteContent);
   }

}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetCookieSessionID
//
// [DESCRIPTION]:   Get cookie session id
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none | string
//
// [CREATED BY]:    Gabriel Istvancsek (null@seopa.com) 2007-05-13
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetCookieSessionID()
{
   if(defined('COOKIE_SESSION_ID'))
      return COOKIE_SESSION_ID;

   return false;
}

}// end of CWebSite class

?>

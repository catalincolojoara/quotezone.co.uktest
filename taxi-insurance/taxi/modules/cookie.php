<?php

   //force session_start
   session_start();

   if(! empty($_SERVER['REMOTE_ADDR']))
   {
      // check if we have cookie enabled and define it
      if(empty($_COOKIE[session_name()]))
      {
         //get cookie via GET or POST
         if(! empty($_GET['cs']))
            define('COOKIE_SESSION_ID',$_GET['cs']);

         if(! empty($_POST['cs']))
            define('COOKIE_SESSION_ID',$_POST['cs']);

         if(preg_match("/loans.quotezone.co.uk/i", $_SERVER['HTTP_REFERER']))
         {
            if(! defined('COOKIE_SESSION_ID'))
            {
               $urlInfo    = parse_url($_SERVER['HTTP_REFERER']);
               parse_str($urlInfo['query'], $getParams);

               if(! empty($getParams['cs']))
                 define('COOKIE_SESSION_ID',$getParams['cs']);
            }
         }

         if(defined('COOKIE_SESSION_ID'))
         {
            $currentCookieSession = session_id();

            $SESSION = $_SESSION;

            if($currentCookieSession != COOKIE_SESSION_ID)
            {
               session_destroy();

               session_id(COOKIE_SESSION_ID);
               session_start();

               foreach($SESSION as $key => $value)
                  if(empty($_SESSION[$key]))
                     $_SESSION[$key] = $value;
            }
         }

         if(! defined('COOKIE_SESSION_ID'))
            define("COOKIE_SESSION_ID",session_id());

      }
   }

?>

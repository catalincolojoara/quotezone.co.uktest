<?php

/*****************************************************************************/
/*                                                                           */
/*   CTaxiMasterEngine class interface                                       */
/*                                                                           */
/*  (C) 2012 Furtuna Alexandru (alexandru.furtuna@seopa.com)                 */
/*                                                                           */
/*****************************************************************************/
include_once "TAXIEngine.php";
include_once "Url.php";
include_once "Site.php";
include_once "Session.php";
include_once "ArList.php";
include_once "Navigation.php";
include_once "Postcode.php";
include_once "functions.php";
include_once "YourDetailsElements.php";
include_once "EmailContent.php";

//outbound nedded
include_once "LeadsCompanyEmails.php";
include_once "SMTP.php";
include_once "File.php";
include_once "Outbound.php";

error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CTaxiMasterEngine
//
// [DESCRIPTION]:  CTaxiMasterEngine class interface, remote site data exchange support,
//                 PHP version
//
// [FUNCTIONS]:   bool   InitSite()
//                string PrepareUrl($url = "")
//                bool   ProcessResponse()
//                bool   ProcessLastPage()
//                bool   CheckResponseErrors()
//                array  GetResultElements()
//                void   PrepareQuote($SESSION)
//
// [CREATED BY]:   Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CTaxiMasterEngine extends CTAXIEngine
{

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CTaxiMasterEngine
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CTaxiMasterEngine($siteId, $fileName, $scanningFlag, $debugMode, $testingMode)
{
   CTAXIEngine::CTAXIEngine("taxi-insurance.quotezone.co.uk", $fileName, $scanningFlag, $debugMode, $testingMode);

   $this->SetFailureResponse("There are errors on this page, please correct");

   $this->SetUnableQuoteOnlineServerMessage("Unfortunately we are unable to provide you with an on-line aplication");
   $this->SetOfflineServerMessage("The online aplication system is currently offline");

   $this->SetProtocol("https");

   $this->siteID = $siteId;
           

}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InitSite
//
// [DESCRIPTION]:   Initialise a remote site session
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InitSite()
{
   return true;
}

/////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: PrepareQuote
//
// [DESCRIPTION]:   send email
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Moraru Valeriu (null@seopa.com) 2011-04-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DoOutbound()
{
   //error_reporting(E_ALL);
   include_once "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/modules/SendProtectedCSV.php";

   global $ACRUX_IP_ARRAY;
   global $_YourDetails;
   global $QATeamEmails;


   $outboundObj         = new COutbound();
   $mailObj             = new CSMTP();
   $objFile             = new CFile();
   $objSite             = new CSite();
   $objPostcode         = new CPostcode();

   $telephoneConsent    = "Yes";
   if(isset($this->session['consent_statements_values']['companyMarketing']))
   {
      $emailConsent     = $this->session['consent_statements_values']['companyMarketing']['Email']==""?"No":"Yes";
      $smsConsent       = $this->session['consent_statements_values']['companyMarketing']['SMS']==""?"No":"Yes";
   }
   else
   {
      $emailConsent     = "";
      $smsConsent       = "";
   }

   $newPostcodeDetails = $objPostcode->GeoPostCodeLookup($this->session['_YourDetails_']['postcode'], $this->session['_YourDetails_']['house_number_or_name']);
   $houseNumberOrName  = $newPostcodeDetails['building_name_and_number'];
   $addressLine1       = $newPostcodeDetails['line1'];
   $streetName         = $newPostcodeDetails['road'];
   $town               = $newPostcodeDetails['post_town'];
   $county             = $newPostcodeDetails['county'];
   $postcode           = $this->session['_YourDetails_']['postcode'];
   
   $dob                       = $this->session['_YourDetails_']['date_of_birth'];
   $dobDD                     = $this->session['_YourDetails_']['date_of_birth_dd'];
   $dobMM                     = $this->session['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY                   = $this->session['_YourDetails_']['date_of_birth_yyyy'];
   
   $doq                       = $this->session['_YourDetails_']['start_date'];
   $doqDD                     = $this->session['_YourDetails_']['start_date_dd'];
   $doqMM                     = $this->session['_YourDetails_']['start_date_mm'];
   $doqYYYY                   = $this->session['_YourDetails_']['start_date_yyyy'];
   
   $dis                       = $this->session['_YourDetails_']['date_of_insurance_start'];
   $disDD                     = $this->session['_YourDetails_']['date_of_insurance_start_dd'];
   $disMM                     = $this->session['_YourDetails_']['date_of_insurance_start_mm'];
   $disYYYY                   = $this->session['_YourDetails_']['date_of_insurance_start_yyyy'];
   
   $title                     = $this->session['_YourDetails_']['title'];
   $firstName                 = $this->session['_YourDetails_']['first_name'];
   $lastName                  = $this->session['_YourDetails_']['surname'];
   $daytimeTelephone          = $this->session['_YourDetails_']['daytime_telephone'];
   $mobileTelephone           = $this->session['_YourDetails_']['mobile_telephone'];
   $email                     = $this->session['_YourDetails_']['email_address'];   
   $quoteRef                  = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];

   $registrationNumberQuestion = $this->session['_YourDetails_']['registration_number_question'] == "Y"?"Yes":"No";
   $registrationNumber         = $this->session['_YourDetails_']['vehicle_registration_number'];
   $taxiMake                   = $_YourDetails['vehicle_make'][$this->session['_YourDetails_']['vehicle_make']];
   $taxiModel                  = $this->session['_YourDetails_']['taxi_model'];
   $abiCode                    = $this->session['_YourDetails_']['vehicle_confirm'];
   $yearOfManufacture          = $this->session['_YourDetails_']['year_of_manufacture'];
   $vehicleMileage             = $_YourDetails['vehicle_mileage'][$this->session['_YourDetails_']['vehicle_mileage']];
   $vehicleMileage             = str_replace(","," ",$vehicleMileage);
   $engineSize                 = $this->session['_YourDetails_']['engine_size'];
   $gearboxType                = $this->session['_YourDetails_']['transmission_type'];
   $uberDriver                 = $this->session['_YourDetails_']['uber_driver'] == "Y"?"Yes":"No";
   $estimatedValue             = $this->session['_YourDetails_']['estimated_value'];
   $taxiType                   = $_YourDetails["taxi_type"][$this->session['_YourDetails_']['taxi_type']];
   $taxiUse                    = $_YourDetails["taxi_used_for"][$this->session['_YourDetails_']['taxi_used_for']]; 
   $taxiCapacity               = $_YourDetails["taxi_capacity"][$this->session['_YourDetails_']['taxi_capacity']];
   $platingAuthority           = $this->session['_YourDetails_']['plating_authority'];
   $typeOfCover                = $_YourDetails["type_of_cover"][$this->session['_YourDetails_']['type_of_cover']];
   $taxiNcb                    = $_YourDetails["taxi_ncb"][$this->session['_YourDetails_']['taxi_ncb']];
   $privateCarNcb              = $_YourDetails["private_car_ncb"][$this->session['_YourDetails_']['private_car_ncb']];
   $fullUkLicence              = $_YourDetails["period_of_licence"][$this->session['_YourDetails_']['period_of_licence']];
   $taxiBadge                  = $_YourDetails["taxi_badge"][$this->session['_YourDetails_']['taxi_badge']];
   $taxiDrivers                = $_YourDetails["taxi_driver"][$this->session['_YourDetails_']['taxi_driver']];
   $claims5Years               = $_YourDetails["claims_5_years"][$this->session['_YourDetails_']['claims_5_years']];
   $convictions5Years          = $_YourDetails["convictions_5_years"][$this->session['_YourDetails_']['convictions_5_years']];


   $mailObj->SetHost("10.2.12.12");
   $this->session["_QZ_QUOTE_DETAILS_"]["SERVER_IP"] = "10.2.24.134";
   CTAXIEngine::DoOutbound();

   $logID  = $this->session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];

   if($this->outbounds['MAIL'])
   {
      // get emails to send from db
      $sendEmailsArray = array();
      //$sendEmailsArray = $objLeadsCompanyEmails->GetAllEmailsToSendForCompanyBySiteID($this->siteID);

      $sendEmailsArray = $this->session['_COMPANY_DETAILS_'][$this->siteID]["EMAILS"];

      //print_r($sendEmailsArray);

      $message_body = GetEmailTemplate($this->session);

      if($this->session['_COMPANY_DETAILS_'][$this->siteID]['EMAILSUBJECT'] && $this->session['_COMPANY_DETAILS_'][$this->siteID]['EMAILSUBJECT'] != '')
         $subject = $this->session['_COMPANY_DETAILS_'][$this->siteID]['EMAILSUBJECT']." ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];
      else
         $subject = "Taxi query : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];

      $headers  = 'MIME-Version: 1.0' . "\r\n";
      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      //$headers .= "From: <non-standard-home@quotezone.co.uk>\r\n";
      //$headers .= "X-Sender: <non-standard-home@quotezone.co.uk>\r\n";
      $headers .= "X-Mailer: PHP\r\n"; // mailer
      $headers .= "X-Priority: 1\r\n"; // Urgent message!
      //$headers .= "Return-Path: <non-standard-home@quotezone.co.uk>\r\n";

      $mailFrom    = "taxi@quotezone.co.uk";
      $mailSubject = $subject;
      $mailBody    = $message_body;
      $emailId     = "";
      $mailToAll   = "";

      foreach($sendEmailsArray as $emailType=>$emailsArray)
      {
         //$countEmails = count($emailsArray);

         switch($emailType)
         {
            case "COMPANY":
               if((! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk") OR ($this->session["_COMPANY_DETAILS_"][$this->siteID]['testing_company']))
               {
                  $countEmails = count($emailsArray);
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject;

                     $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"MAIL",$this->siteID);
                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }
               }
               break;

            case "MARKETING":

               if((! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk") OR ($this->session["_COMPANY_DETAILS_"][$this->siteID]['testing_marketing']))
               {
                  $countEmails = count($emailsArray);
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject." ".$this->session["_COMPANY_DETAILS_"][$this->siteID]["COMPANY_NAME"];

                     if(isset($this->session['FROM_RERUN_QUOTE_DETAILS']) && $this->session['FROM_RERUN_QUOTE_DETAILS']['ID'] != '')
                        $mailSubject .= " - Resubmit: ".$this->session['FROM_RERUN_QUOTE_DETAILS']['DAY_SELECT']." (".$this->session['FROM_RERUN_QUOTE_DETAILS']['TIME_SELECT'].")";

                     $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"MAIL",$this->siteID);
                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }
               }
               break;

            case "TECHNICAL":
               $countEmails = count($emailsArray);
               //send emails to TECHNICAL department
               for($k=0;$k<$countEmails;$k++)
               {
                  $mailTo      = $emailsArray[$k];
                  print "send to:".$mailTo."\n";

                  $mailSubject = $subject." ".$this->session["_COMPANY_DETAILS_"][$this->siteID]["COMPANY_NAME"];

                  if(isset($this->session['FROM_RERUN_QUOTE_DETAILS']) && $this->session['FROM_RERUN_QUOTE_DETAILS']['ID'] != '')
                     $mailSubject .= " - Resubmit: ".$this->session['FROM_RERUN_QUOTE_DETAILS']['DAY_SELECT']." (".$this->session['FROM_RERUN_QUOTE_DETAILS']['TIME_SELECT'].")";

                  if($mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"MAIL",$this->siteID))
                     print "Email sent to $mailTo \n\n";

                  $emailId   .= $mailObj->GetMessageID().",";
                  $mailToAll .= $mailTo.",";
               }


               //send to QZ TEAM
               if($this->session["_COMPANY_DETAILS_"][$this->siteID]['send_QA_team'])
               {
                  foreach($QATeamEmails as $mailTo)
                  {
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject." ".$this->session["_COMPANY_DETAILS_"][$this->siteID]["COMPANY_NAME"];

                     if($mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"MAIL",$this->siteID))
                        print "Email sent to $mailTo \n\n";
                  }
               }

               break;


               //send emails to TECHNICAL department
               for($k=0;$k<$countEmails;$k++)
               {
                  $mailTo      = $emailsArray[$k];
                  print "send to:".$mailTo."\n";

                  $mailSubject = $subject." ".$this->session["_COMPANY_DETAILS_"][$this->siteID]["COMPANY_NAME"];

                  if($mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"MAIL",$this->siteID))
                     print "Email sent to $mailTo \n\n";

                  $emailId   .= $mailObj->GetMessageID().",";
                  $mailToAll .= $mailTo.",";
               }



         }// switch
      }// foreach

      // remove last coma
      $emailId   = substr($emailId,0,-1);
      $mailToAll = substr($mailToAll,0,-1);

      $outboundObj->UpdateOutboundField($this->outbounds['MAIL'],'sent_status', 1);
      $outboundObj->UpdateOutboundField($this->outbounds['MAIL'],'extra_params', $emailId);

      $sentContent =  "\nHEADERS: ".$headers."\nFROM:".$mailFrom."\nTO:".$mailToAll."\nSUBJECT: ".$mailSubject."\nBODY: ".$mailBody;

      $this->DoOutboundFiles("mail", $sentContent);

   } //end if MAIL

   if($this->outbounds['TXT'])
   {

      //get the emails from the file
      $sendEmailsArray = array();
      $sendEmailsArray = $this->session['_COMPANY_DETAILS_'][$this->siteID]["TXT"];

      $textTmplArr = array(513 => "DNA_NEW_BADGE", 1744 => "DNA_TEXT2", 1867 => "DNA_TEXT4", 1671 => "ALLEN_TXT_1671", 11826 => "COMPLETE_COVER_TXT_11826", 11257 => "JMB_TXT_11257", 12382 => "JMB_TXT_12382", );
      
      foreach($textTmplArr as $textTmplKey => $textTmplVal)
      {
         if($this->siteID == $textTmplKey)
            $textTemplate = $textTmplVal;    
      }
    

      //text requests
      if(array_key_exists($this->siteID, $textTmplArr))   
      {
         $textString = GetEmailTemplate($this->session,$textTemplate);

         $subject = "Taxi query : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         //$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         //$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject;
         $mailBody    = $textString;
         $emailId     = "";
         $mailToAll   = "";

         foreach($sendEmailsArray as $emailType=>$emailsArray)
         {
            $countEmails = count($emailsArray);

            switch($emailType)
            {
               case "COMPANY":
                  if((! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk") OR ($this->session["_COMPANY_DETAILS_"][$this->siteID]['testing_company']))
                  {
                     for($k=0;$k<$countEmails;$k++)
                     {
                        $mailTo = $emailsArray[$k];
                        print "send to:".$mailTo."\n";

                        $mailSubject = $subject;

                        $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"TXT",$this->siteID);
                        $emailId   .= $mailObj->GetMessageID().",";
                        $mailToAll .= $mailTo.",";
                     }
                  }
                  break;

               case "MARKETING":

                  if((! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk") OR ($this->session["_COMPANY_DETAILS_"][$this->siteID]['testing_marketing']))
                  {
                     for($k=0;$k<$countEmails;$k++)
                     {
                        $mailTo = $emailsArray[$k];
                        print "send to:".$mailTo."\n";

                        $mailSubject = $subject." ".$this->session["_COMPANY_DETAILS_"][$this->siteID]["COMPANY_NAME"];

                        if(isset($this->session['FROM_RERUN_QUOTE_DETAILS']) && $this->session['FROM_RERUN_QUOTE_DETAILS']['ID'] != '')
                           $mailSubject .= " - Resubmit: ".$this->session['FROM_RERUN_QUOTE_DETAILS']['DAY_SELECT']." (".$this->session['FROM_RERUN_QUOTE_DETAILS']['TIME_SELECT'].")";

                        $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"TXT",$this->siteID);
                        $emailId   .= $mailObj->GetMessageID().",";
                        $mailToAll .= $mailTo.",";
                     }
                  }
                  break;

               case "TECHNICAL":

                  //send emails to TECHNICAL department
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo      = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject." ".$this->session["_COMPANY_DETAILS_"][$this->siteID]["COMPANY_NAME"];

                     if(isset($this->session['FROM_RERUN_QUOTE_DETAILS']) && $this->session['FROM_RERUN_QUOTE_DETAILS']['ID'] != '')
                        $mailSubject .= " - Resubmit: ".$this->session['FROM_RERUN_QUOTE_DETAILS']['DAY_SELECT']." (".$this->session['FROM_RERUN_QUOTE_DETAILS']['TIME_SELECT'].")";

                     $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"TXT",$this->siteID);
                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }
                  break;
            }// switch
         }// foreach

         // remove last coma
         $emailId   = substr($emailId,0,-1);
         $mailToAll = substr($mailToAll,0,-1);

         $outboundObj->UpdateOutboundField($this->outbounds['TXT'],'sent_status', 1);
         $outboundObj->UpdateOutboundField($this->outbounds['TXT'],'extra_params', $emailId);

         $sentContent =  "\nHEADERS: ".$headers."\nFROM:".$mailFrom."\nTO:".$mailToAll."\nSUBJECT: ".$mailSubject."\nBODY: ".$mailBody;

         $this->DoOutboundFiles("txt", $sentContent);
      }//end DNA text request
      else
      {
         //NORMAL TXT EMAIL

         $message_body = GetEmailTemplate($this->session,"TEXT");

         if($this->session['_COMPANY_DETAILS_'][$this->siteID]['EMAILSUBJECT'] && $this->session['_COMPANY_DETAILS_'][$this->siteID]['EMAILSUBJECT'] != '')
            $subject = $this->session['_COMPANY_DETAILS_'][$this->siteID]['EMAILSUBJECT']." ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];
         else
           $subject = "Taxi query : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <non-standard-home@quotezone.co.uk>\r\n";
         //$headers .= "X-Sender: <non-standard-home@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         //$headers .= "Return-Path: <non-standard-home@quotezone.co.uk>\r\n";

         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject;
         $mailBody    = $message_body;
         $emailId     = "";
         $mailToAll   = "";

         foreach($sendEmailsArray as $emailType=>$emailsArray)
         {
            //$countEmails = count($emailsArray);

            switch($emailType)
            {
               case "COMPANY":
                  if((! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk") OR ($this->session["_COMPANY_DETAILS_"][$this->siteID]['testing_company']))
                  {
                     $countEmails = count($emailsArray);
                     for($k=0;$k<$countEmails;$k++)
                     {
                        $mailTo = $emailsArray[$k];
                        print "send to:".$mailTo."\n";

                        $mailSubject = $subject;

                        $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"TXT",$this->siteID);
                        $emailId   .= $mailObj->GetMessageID().",";
                        $mailToAll .= $mailTo.",";
                     }
                  }
                  break;

               case "MARKETING":

                  if((! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk") OR ($this->session["_COMPANY_DETAILS_"][$this->siteID]['testing_marketing']))
                  {
                     $countEmails = count($emailsArray);
                     for($k=0;$k<$countEmails;$k++)
                     {
                        $mailTo = $emailsArray[$k];
                        print "send to:".$mailTo."\n";

                        $mailSubject = $subject." ".$this->session["_COMPANY_DETAILS_"][$this->siteID]["COMPANY_NAME"];

                        $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"TXT",$this->siteID);
                        $emailId   .= $mailObj->GetMessageID().",";
                        $mailToAll .= $mailTo.",";
                     }
                  }
                  break;

               case "TECHNICAL":
                  $countEmails = count($emailsArray);
                  //send emails to TECHNICAL department
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo      = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject." ".$this->session["_COMPANY_DETAILS_"][$this->siteID]["COMPANY_NAME"];

                     if($mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"TXT",$this->siteID))
                        print "Email sent to $mailTo \n\n";

                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }


                  //send to QZ TEAM
                  if($this->session["_COMPANY_DETAILS_"][$this->siteID]['send_QA_team'])
                  {
                     foreach($QATeamEmails as $mailTo)
                     {
                        print "send to:".$mailTo."\n";

                        $mailSubject = $subject." ".$this->session["_COMPANY_DETAILS_"][$this->siteID]["COMPANY_NAME"];

                        if($mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"TXT",$this->siteID))
                           print "Email sent to $mailTo \n\n";
                     }
                  }

                  break;

                  //send emails to TECHNICAL department
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo      = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject." ".$this->session["_COMPANY_DETAILS_"][$this->siteID]["COMPANY_NAME"];

                     if($mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"TXT",$this->siteID))
                        print "Email sent to $mailTo \n\n";

                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }



            }// switch
         }// foreach

         // remove last coma
         $emailId   = substr($emailId,0,-1);
         $mailToAll = substr($mailToAll,0,-1);

         $outboundObj->UpdateOutboundField($this->outbounds['TXT'],'sent_status', 1);
         $outboundObj->UpdateOutboundField($this->outbounds['TXT'],'extra_params', $emailId);

         $sentContent =  "\nHEADERS: ".$headers."\nFROM:".$mailFrom."\nTO:".$mailToAll."\nSUBJECT: ".$mailSubject."\nBODY: ".$mailBody;

         $this->DoOutboundFiles("txt", $sentContent);

      }//end NORMAL TXT Email

   } //end if txt
   
   
   if($this->outbounds['XML'])
   {
      if((($this->session["_COMPANY_DETAILS_"]['12382']["REQUEST_XML"] == 1) AND ($this->siteID == 12382) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['11257']["REQUEST_XML"] == 1) AND ($this->siteID == 11257) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) )
      // if((($this->session["_COMPANY_DETAILS_"]['12382']["REQUEST_XML"] == 1) AND ($this->siteID == 12382) ) || (($this->session["_COMPANY_DETAILS_"]['11257']["REQUEST_XML"] == 1) AND ($this->siteID == 11257) ) )
       {         
         $telephoneTypeIDNo         = str_split($daytimeTelephone,2);
         $telephoneTypeID           = $telephoneTypeIDNo[0] == "07"?"3AJPQ7C6":"3AJPQ7C4";
         $mobileTypeIDNo            = str_split($mobileTelephone,2);
         $mobileTypeID              = $mobileTypeIDNo[0] == "07"?"3AJPQ7C6":"3AJPQ7C4";
         $titleIdsArr               = array('Mr' => '003', 'Mrs' => '004', 'Ms' => '005', 'Miss' => '002', );
         $typeOfCover               = $this->session["_YourDetails_"]["type_of_cover"];
         $vehicleRegistrationnumber = $this->session["_YourDetails_"]["vehicle_registration_number"];
         $estimatedvalue            = $this->session["_YourDetails_"]["estimated_value"];
         $vehicleMake               = $_YourDetails["vehicle_make"][$this->session["_YourDetails_"]["vehicle_make"]];
         $vehicleModel              = $this->session["_YourDetails_"]["vehicle_model"];
         $yearOfManufacture         = $this->session["_YourDetails_"]["year_of_manufacture"];
         $taxiNcb                   = $this->session["_YourDetails_"]["taxi_ncb"];

         // $objVehicle        = new CVehicle();
         // $vehicleCodeID     = $objVehicle->GetVehicleCode($vehicleConfirm);
         // $vehicleDetails    = $objVehicle->GetVehicle($vehicleCodeID);
         // $gear              = $vehicleDetails['transmission_type'];
         // $bodyType          = $vehicleDetails['body_type'];
         // $doors             = $vehicleDetails['doors'];         
         
         $xmlData = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <soap:Body>
      <Quote xmlns="http://TGSL/TES_Aggregator">
         <lxmlTGSLXML>
            <tgsl xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.transactorgsl.com/schemas http://localhost/TES_Aggregator/schemas/tgsl.xsd" xmlns="http://www.transactorgsl.com/schemas">
               <aggregator>quotezone-leads-taxi</aggregator>
               <domain>http://localhost</domain>
               <partner>
                  <partnerName>jmb</partnerName>
                  <broker>jmb</broker>
               </partner>
               <product>cv</product>
            </tgsl>
         </lxmlTGSLXML>
         <lstrRiskXML>
            <cvRisk xmlns="http://www.transactorgsl.com/schemas" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.transactorgsl.com/schemas http://localhost/TES_Aggregator/Schemas/TGSL/TGSLCVSchema.xsd">
               <insuredParty instance="1">
                  <accessToOtherVehicles>false</accessToOtherVehicles>
                  <client>true</client>
                  <dateLicenceObtained>2008-12-20T00:00:00</dateLicenceObtained>
                  <dob>'.$dobYYYY.'-'.$dobMM.'-'.$dobDD.'T00:00:00</dob>
                  <email>'.$email.'</email>
                  <forename>'.$firstName.'</forename>
                  <licenceTypeID>F</licenceTypeID>
                  <numberOfVehiclesAccessTo>0</numberOfVehiclesAccessTo>
                  <relationshipID>P</relationshipID>
                  <surname>'.$lastName.'</surname>
                  <titleID>'.$titleIdsArr[$title].'</titleID>
               </insuredParty>
               <correspondanceAddress instance="1">
                  <city>'.$town.'</city>
                  <country />
                  <county/>
                  <house>'.$houseNumberOrName.'</house>
                  <locality/>
                  <postcode>'.$postcode.'</postcode>
                  <street>'.$streetName.'</street>
               </correspondanceAddress>
               <occupation instance="1" insuredPartyInstance="1">
                  <employersBusinessID>304</employersBusinessID>
                  <employmentStatusID>E</employmentStatusID>
                  <occupationID>C39</occupationID>
                  <parttime>false</parttime>
                  <primaryOccupation>true</primaryOccupation>
               </occupation>
               <telephone instance="1">
                  <telephoneNumber>'.$daytimeTelephone.'</telephoneNumber>
                  <telephoneTypeID>'.$telephoneTypeID.'</telephoneTypeID>
               </telephone><telephone instance="2">
                  <telephoneNumber>'.$mobileTelephone.'</telephoneNumber>
                  <telephoneTypeID>'.$mobileTypeID.'</telephoneTypeID>
                  </telephone><cover instance="1">
                  <coverRequiredID>0'.$typeOfCover.'</coverRequiredID>
                  <coverStartDate>'.$disYYYY.'-'.$disMM.'-'.$disDD.'T00:00:00</coverStartDate>
               </cover>
               <vehicle instance="1">
                  <abiCode>53547701</abiCode>
                  <currentMileage>0</currentMileage>
                  <fuelTypeID>001</fuelTypeID>
                  <gearboxTypeID>001</gearboxTypeID>
                  <registrationNumber>'.$vehicleRegistrationnumber.'</registrationNumber>
                  <value>'.$estimatedvalue.'.00</value>
                  <vehicleCC>2993</vehicleCC>         
                  <vehicleMake>'.$vehicleMake.'</vehicleMake>
                  <vehicleMark>S 7 STR</vehicleMark>
                  <vehicleModel>'.$vehicleModel.'</vehicleModel>
                  <yearOfManufacture>'.$yearOfManufacture.'</yearOfManufacture>
                  <carriesGoods>false</carriesGoods>
                  <goodsCarriedID>0</goodsCarriedID>
               </vehicle>
               <ncd instance="1">
                  <ncdYears>'.$taxiNcb.'</ncdYears>
               </ncd>
            </cvRisk>
         </lstrRiskXML>
      </Quote>
   </soap:Body>
</soap:Envelope>';        
               

         // $url      = "https://jmb-drs-uat.i-wonder.hosting/TES_Aggregator/Aggregator.asmx";                 
         $url      = "https://jmb-drs.i-wonder.hosting/TES_Aggregator/Aggregator.asmx";                 

         $header[] = "POST /TES_Aggregator/Aggregator.asmx HTTP/1.1";
         $header[] = "Host: jmb-drs.i-wonder.hosting";
         $header[] = "Content-Type: text/xml; charset=utf-8";
         $header[] = "Content-length: ".strlen($xmlData);
         $header[] = "SOAPAction: http://TGSL/TES_Aggregator/Quote";
         $header[] = "Connection: close \r\n";
         $header[] = $xmlData;


         $ch = curl_init();

         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
         curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 15);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_HEADER, 1);

         $result = curl_exec($ch);

         curl_close ($ch);
        
         $xmlMessageBody = prepareXMLMessageBody($xmlData, $result);
         
         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <motorhome@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <motorhome@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer3421
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <motorhome@quotezone.co.uk>\r\n";


         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject. " Taxi JMB $this->sideID $quoteRef - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject. " Taxi JMB - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);
         
         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if(preg_match("/Bad Request/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);
         else
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);

         $sentContent =  "\nURL: ".$url."\n<br>PARAMS:".$xmlData;

         $this->DoOutboundFiles("xml", $sentContent,$result);
      }

      if((($this->session["_COMPANY_DETAILS_"]['12434']["REQUEST_XML"] == 1) AND ($this->siteID == 12434)  AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))|| (($this->session["_COMPANY_DETAILS_"]['2942']["REQUEST_XML"] == 1) AND ($this->siteID == 2942)  AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")))
      // if((($this->session["_COMPANY_DETAILS_"]['12434']["REQUEST_XML"] == 1) AND ($this->siteID == 12434))|| (($this->session["_COMPANY_DETAILS_"]['2942']["REQUEST_XML"] == 1) AND ($this->siteID == 2942)))
      {

         $platingAuthority = urlencode($platingAuthority);

         $data = array(
               "do_you_know_registration_number" => $registrationNumberQuestion,
               "registration_number"             => $registrationNumber,
               "estimated_taxi_value"            => $estimatedValue,
               "taxi_type"                       => $taxiType,
               "taxi_use"                        => $taxiUse,
               "max_passengers"                  => $taxiCapacity,
               "licensing_authority"             => $platingAuthority,
               "type_of_cover"                   => $typeOfCover,
               "taxi_ncb"                        => $taxiNcb,
               "private_car_ncb"                 => $privateCarNcb,
               "full_uk_licence"                 => $fullUkLicence,
               "taxi_badge"                      => $taxiBadge,
               "taxi_drivers"                    => $taxiDrivers,
               "claims_last_5years"              => $claims5Years,
               "convictions_last_5_years"        => $convictions5Years,
               "title"                           => $title,
               "first_name"                      => $firstName,
               "surname"                         => $lastName,
               "date_of_birth"                   => $dob,
               "preferred_telephone_number"      => $daytimeTelephone,
               "additional_telephone_number"     => $mobileTelephone,
               "email_address"                   => $email,
               "postcode"                        => $postcode,
               "insurance_start_date"            => $dis
            );      

         $params .= "first_name=".urlencode($firstName);
         $params .= "&surname=".urlencode($lastName);
         $params .= "&mobile=".urlencode($daytimeTelephone);
          if ($mobileTelephone)
            $params .= "&other_phone=".urlencode($mobileTelephone);
         $params .= "&email=".$email;
         $params .= "&type=adelphi-taxi";
        

         $params .= "&data=".json_encode($data);

         $url    = "https://api.wdphones.eu/lead/add?key=MebgNGvsky1naT5dOS9-9oB01J_fHyW9";

         //post
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
         curl_setopt($ch, CURLOPT_TIMEOUT, 30);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         $result = curl_exec($ch);
         curl_close($ch);

         $printParams = str_replace("?key=MebgNGvsky1naT5dOS9-9oB01J_fHyW9","",$params);
         $url = str_replace("?key=MebgNGvsky1naT5dOS9-9oB01J_fHyW9","",$url);
         $printParams = str_replace("&","\r\n&",$printParams);
         $xmlMessageBody = $printParams."[$result]"; 
         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <courier@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <courier@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <courier@quotezone.co.uk>\r\n";

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject. " Adelphi Taxi id$this->siteID - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject. " Adelphi Taxi id$this->siteID - XML Method ";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if(! strlen($result))
             $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 3); //NO RESPONSE
           if( preg_match("/{\"saved\":true}/i",$result))
             $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1); //OK RESPONSE
           else
             $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);//BAD RESPONSE
      
           $this->DoOutboundFiles("xml", $xmlString, $result);
      }

      if(($this->session["_COMPANY_DETAILS_"]['1867']["REQUEST_XML"] == 1) AND ($this->siteID == 1867) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      // if(($this->session["_COMPANY_DETAILS_"]['1867']["REQUEST_XML"] == 1) AND ($this->siteID == 1867))
      {
         if($this->siteID == 1867)
         {
            $subjectDetails   = " - DNA - Taxi ID:1867 - XML Method";
            $cost             = getSiteDetailsForEmail('1867');
            $leadPrice        = $cost['lead_price'];
         }

         
         $xmlString = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mot=\"http://rodsystem.co.uk/webservice/wspost/taxi-insurance\">
                  <soapenv:Header/>
                  <soapenv:Body>
                     <mot:PostDataIn>  
                        <mot:sUN>quotezone</mot:sUN>  
                        <mot:sPW>seopa123</mot:sPW>   
                        <mot:sData> 
                        <data>   
                        <lead>   
                        <accountid>7802</accountid>   
                        <key>D1FDtBiWGHGLLocspCPFWf3bnkvnSVLu</key>
                        <leadgroup>38302</leadgroup>
                        <site>12512</site>
                        <introducer>40554</introducer>
                        <source>QZ1</source>
                        <medium></medium>
                        <term></term>
                        <cost>".$leadPrice."</cost>
                        <value></value>
                        <title>".$title."</title>
                        <firstname>".$firstName."</firstname>
                        <lastname>".$lastName."</lastname>
                        <dobday>".$dobDD."</dobday>
                        <dobmonth>".$dobMM."</dobmonth>
                        <dobyear>".$dobYYYY."</dobyear>
                        <company></company>
                        <jobtitle></jobtitle>
                        <phone1>".$daytimeTelephone."</phone1>
                        <phone2>".$mobileTelephone."</phone2>
                        <fax></fax>
                        <email>".$email."</email>
                        <address>".$houseNumberOrName." ".$streetName.".</address>
                        <address2></address2>
                        <address3></address3>
                        <towncity>".$town."</towncity>
                        <postcode>".$postcode."</postcode>
                        <notes></notes>
                        <taxiregistration>".$this->session['_YourDetails_']['vehicle_registration_number']."</taxiregistration>
                        <taximake>".$_YourDetails["vehicle_make"][$this->session['_YourDetails_']['vehicle_make']]."</taximake>
                        <taximodel>".$this->session['_YourDetails_']['taxi_model']."</taximodel>
                        <estimatedvalue>".$this->session['_YourDetails_']['estimated_value']."</estimatedvalue>
                        <yearofmanufacture>".$this->session['_YourDetails_']['year_of_manufacture']."</yearofmanufacture>
                        <vehiclemileage></vehiclemileage>
                        <taxitype>".$_YourDetails["taxi_type"][$this->session['_YourDetails_']['taxi_type']]."</taxitype>
                        <taxiuse>".$_YourDetails["taxi_used_for"][$this->session['_YourDetails_']['taxi_used_for']]."</taxiuse>
                        <maxnumberofpassengers>".$_YourDetails["taxi_capacity"][$this->session['_YourDetails_']['taxi_capacity']]."</maxnumberofpassengers>
                        <taxiplatingauthorities>".str_replace(array("&","&amp;"),"and",$this->session['_YourDetails_']['plating_authority'])."</taxiplatingauthorities>
                        <typeofcover>".str_replace(array("&","&amp;"),"and",$_YourDetails["type_of_cover"][$this->session['_YourDetails_']['type_of_cover']])."</typeofcover>
                        <taxinoclaimsbonus>".$_YourDetails["taxi_ncb"][$this->session['_YourDetails_']['taxi_ncb']]."</taxinoclaimsbonus>
                        <fulluklicence>".$_YourDetails["period_of_licence"][$this->session['_YourDetails_']['period_of_licence']]."</fulluklicence>
                        <taxibadge>".$_YourDetails["taxi_badge"][$this->session['_YourDetails_']['taxi_badge']]."</taxibadge>
                        <claimslast5years>".$_YourDetails["claims_5_years"][$this->session['_YourDetails_']['claims_5_years']]."</claimslast5years>
                        <convictionslast5years>".$_YourDetails["convictions_5_years"][$this->session['_YourDetails_']['convictions_5_years']]."</convictionslast5years>
                        <areyouinterestedingapinsurance></areyouinterestedingapinsurance>
                        <besttimetocall></besttimetocall>
                        <bestdaytocall></bestdaytocall>
                        <insurancestartday>".$disDD."</insurancestartday>
                        <insurancestartmonth>".$disMM."</insurancestartmonth>
                        <insurancestartyear>".$disYYYY."</insurancestartyear>
                        <premium></premium>
                        <insureddate></insureddate>
                        <enginesize>".$this->session['_YourDetails_']['engine_size']."</enginesize>
                        <policyrefrence></policyrefrence>
                        <taxidrivers>".$_YourDetails["taxi_driver"][$this->session['_YourDetails_']['taxi_driver']]."</taxidrivers>
                        <typeofinsurance></typeofinsurance>
                        <notes></notes>
                        </lead></data></mot:sData>
                        </mot:PostDataIn>
                     </soapenv:Body>
                  </soapenv:Envelope>
                        ";

            
                        
            $url = "http://rodsystem.co.uk/webservice/wspost/taxi-insurance?wsdl";   

            $ch       = curl_init();
            $header   = array();
            $header[] = "POST /webservice/wspost/taxi-insurance?wsdl HTTP/1.1";
            $header[] = "Host: rodsystem.co.uk";
            $header[] = "Content-Type: text/xml; charset=utf-8";
            $header[] = "Content-length: ".strlen($xmlString);
            $header[] = "SOAPAction: \"http://rodsystem.co.uk/webservice/wspost/taxi-insurance/PostDataIn\"";
            $header[] = "Cache-Control: no-cache";
            $header[] = "Connection: close \r\n";
            $header[] = $xmlString;

            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 180);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_HEADER, 1);

            $result = curl_exec($ch);
            $error  = curl_error($ch).curl_errno($ch);
            curl_close ($ch); 

         $xmlMessageBody = prepareXMLMessageBody($xmlString, $result); 

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";


         //eb2
         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject.$subjectDetails;
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         //seopa
         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject.$subjectDetails;
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if(! strlen($result))
           $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 3); //NO RESPONSE
                          
         if( preg_match("/>Bad Request</i",$result))
           $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2); //OK RESPONSE
         else
           $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);//BAD RESPONSE

         $this->DoOutboundFiles("xml", $xmlString, $result); 
      }


      // Think - Taxi 2 (taxi) 11753
      if(($this->session["_COMPANY_DETAILS_"]['11753']["REQUEST_XML"] == 1) AND ($this->siteID == 11753) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk") )
       //if(($this->session["_COMPANY_DETAILS_"]['11753']["REQUEST_XML"] == 1) AND ($this->siteID == 11753) )
       {

         function sendPostData($url, $username, $password, $data)
         {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            $response = curl_exec($ch);
            curl_close($ch);

            // RESPONCE
            $data = json_decode($response, true);
            foreach ($data as &$value)
            {
               return $value;
            }
         }

         // URL & CREDENTIALS
         $url      = 'https://www.thinkinsurance.co.uk/leadInput.php';
         $username = 'quotezone';
         $password = '381!GLm;K#';

         $title    = $this->session['_YourDetails_']['title'];
         if($title == "Mr")
            $title = "1";
         else if($title == "Mrs")
            $title = "2";
         else if($title == "Miss")
            $title = "3";
         else if($title == "Ms")
            $title = "4";
         else if($title == "Dr")
            $title = "5";
         else
            $title = "1";

         // DATA TO INSERT
         $firstName     = $this->session['_YourDetails_']['first_name'];
         $lastName      = $this->session['_YourDetails_']['surname'];
         $DOB           = $this->session['_YourDetails_']['date_of_birth_dd']."-".$this->session['_YourDetails_']['date_of_birth_mm']."-".$this->session['_YourDetails_']['date_of_birth_yyyy'];
         $telephone     = $this->session['_YourDetails_']['daytime_telephone'];
         $email         = $this->session['_YourDetails_']['email_address'];
         $postcode      = str_replace(" ","",$this->session['_YourDetails_']['postcode']);
         $product       = "23";
         $filterGroup   = "1";

         $dataArray = array(
            'filterGroup' => urlencode($filterGroup),
            'title' => urlencode($title),
            'firstName' => urlencode($firstName),
            'lastName' => urlencode($lastName),
            'DOB' => urlencode($DOB),
            'telephone' => urlencode($telephone),
            'email' => urlencode($email),
            'postcode' => urlencode($postcode),
            'product' => urlencode($product),
            'contactEmail'=> 1,
            'contactText' => 1,
            'contactPhone'=> 1,
         );

         $xmlData = json_encode($dataArray);

         $result         = sendPostData($url,$username,$password,$xmlData);
         $xmlMessageBody = prepareXMLMessageBody($xmlData, $result);

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "motortrade@quotezone.co.uk";
         $mailSubject = $subject. " Taxi Think Insurance 2 id 11753 - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "motortrade@quotezone.co.uk";
         $mailSubject = $subject. " Taxi Think Insurance 2 id 11753 - XML Method ";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if(! strlen($result))
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 3); //NO RESPONSE

         if( preg_match("/Data Retrieved Correct/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1); //OK RESPONSE
         else
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);//BAD RESPONSE

         $this->DoOutboundFiles("xml", $xmlData, $result);
      }



      if(($this->session["_COMPANY_DETAILS_"]['12064']["REQUEST_XML"] == 1) AND ($this->siteID == 12064) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      // if(($this->session["_COMPANY_DETAILS_"]['12064']["REQUEST_XML"] == 1) AND ($this->siteID == 12064))
      {
            $vehicleMileage                  = $_YourDetails['vehicle_mileage'][$this->session['_YourDetails_']['vehicle_mileage']];
            $vehicleMileage                  = str_replace(","," ",$vehicleMileage);
            $claimsLast5Years                = $_YourDetails["claims_5_years"][$this->session['_YourDetails_']['claims_5_years']];
            $claimsLast5Years                = str_replace(","," ",$claimsLast5Years);
            $motoringConvictionsLast5Years   = str_replace(","," ",$_YourDetails["convictions_5_years"][$this->session['_YourDetails_']['convictions_5_years']]);

            $title        = $this->session['_YourDetails_']['title'];
            $firstName      = $this->session['_YourDetails_']['first_name'];
            $surname      = $this->session['_YourDetails_']['surname'];
            $birthDate    = $this->session['_YourDetails_']['date_of_birth'];
            $dayTimePhone = $this->session['_YourDetails_']['daytime_telephone'];
            $mobilePhone  = $this->session['_YourDetails_']['mobile_telephone'];
            $postcode     = $this->session['_YourDetails_']['postcode'];
            $email        = $this->session['_YourDetails_']['email_address'];

            $dateOfInsuranceStart = $this->session['_YourDetails_']['date_of_insurance_start_dd']."/".$this->session['_YourDetails_']['date_of_insurance_start_mm']."/".$this->session['_YourDetails_']['date_of_insurance_start_yyyy'];

            $licAuth = str_replace(","," ",$this->session['_YourDetails_']['plating_authority']);
            $licAuth = urlencode($licAuth);

            $data =
            array(
               "do_you_know_registration_number" => $this->session['_YourDetails_']['registration_number_question']=="Y"?"yes":"no",
               "registration_number" => urlencode($this->session['_YourDetails_']['vehicle_registration_number']),
               "estimated_taxi_value" => urlencode($this->session['_YourDetails_']['estimated_value']),
               "taxi_type" => urlencode($_YourDetails["taxi_type"][$this->session['_YourDetails_']['taxi_type']]),
               "taxi_use" => urlencode($_YourDetails["taxi_used_for"][$this->session['_YourDetails_']['taxi_used_for']]),
               "max_passengers" => urlencode($_YourDetails["taxi_capacity"][$this->session['_YourDetails_']['taxi_capacity']]),
               "licensing_authority" => $licAuth,
               "type_of_cover" => urlencode($_YourDetails["type_of_cover"][$this->session['_YourDetails_']['type_of_cover']]),
               "taxi_ncb" => urlencode($_YourDetails["taxi_ncb"][$this->session['_YourDetails_']['taxi_ncb']]),
               "private_car_ncb" => urlencode($_YourDetails["private_car_ncb"][$this->session['_YourDetails_']['private_car_ncb']]),
               "full_uk_licence" => urlencode($_YourDetails["period_of_licence"][$this->session['_YourDetails_']['period_of_licence']]),
               "taxi_badge" => urlencode($_YourDetails["taxi_badge"][$this->session['_YourDetails_']['taxi_badge']]),
               "taxi_drivers" => urlencode($_YourDetails["taxi_driver"][$this->session['_YourDetails_']['taxi_driver']]),
               "claims_last_5years" => urlencode($claimsLast5Years),
               "convictions_last_5_years" => urlencode($motoringConvictionsLast5Years),
               "title"                => urlencode($title),
               "first_name"           => urlencode($firstName),
               "surname"              => urlencode($surname),
               "date_of_birth"        => urlencode($birthDate),
               "preferred_telephone_number" => urlencode($dayTimePhone),
               "additional_telephone_number" => urlencode($mobilePhone),
               "email_address"        => urlencode($email),
               "postcode"             => urlencode($postcode),
               "insurance_start_date" => urlencode($dateOfInsuranceStart),
            
         );
         
         $phone2='';
         if ($mobilePhone)
         {
            $phone1 = $mobilePhone;
            $phone2 = $dayTimePhone;
         }
         else
            $phone1 = $dayTimePhone;   
         
         $params .= "first_name=".urlencode($firstName);
         $params .= "&surname=".urlencode($surname);
         $params .= "&mobile=".urlencode($phone1);         
         $params .= "&email=".urlencode($email);
         $params .= "&type=taxi";
         if ($phone2)
            $params .= "&other_phone=".urlencode($phone2);
         $params .= "&data=".json_encode($data);

         //encode the string
         //$encodedParams = urlencode($params);

         $url    = "https://api.wdphones.eu/lead/add?key=mwQ57X9pa2I2LeGspHuiIcQzozrciICm";

         //post
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
         curl_setopt($ch, CURLOPT_TIMEOUT, 30);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         $result = curl_exec($ch);
         curl_close($ch);
         
         $printParams = str_replace("?key=mwQ57X9pa2I2LeGspHuiIcQzozrciICm","",$params);
         $printParams = str_replace("&","\r\n&",$printParams);
         $url = str_replace("?key=mwQ57X9pa2I2LeGspHuiIcQzozrciICm","",$url); //remove password from email.

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi Well Dunn XML Lead Request";         
         $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);
         
         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi Well Dunn XML Lead Request";
         $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);  
   
      
         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if(! strlen($result))
             $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 3); //NO RESPONSE
           if( preg_match("/{\"saved\":true}/i",$result) || preg_match("/Quarantined/i",$result))
             $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1); //OK RESPONSE
           else
             $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);//BAD RESPONSE
      
           $this->DoOutboundFiles("xml", $xmlString, $result);
      }//end if url well dunn

      if((($this->session["_COMPANY_DETAILS_"]['9626']["REQUEST_XML"] == 1) AND ($this->siteID == 9626) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['10245']["REQUEST_XML"] == 1) AND ($this->siteID == 10245) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['11115']["REQUEST_XML"] == 1) AND ($this->siteID == 11115) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['11670']["REQUEST_XML"] == 1) AND ($this->siteID == 11670) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) )
      // if((($this->session["_COMPANY_DETAILS_"]['9626']["REQUEST_XML"] == 1) AND ($this->siteID == 9626)) || (($this->session["_COMPANY_DETAILS_"]['10245']["REQUEST_XML"] == 1) AND ($this->siteID == 10245)) || (($this->session["_COMPANY_DETAILS_"]['11115']["REQUEST_XML"] == 1) AND ($this->siteID == 11115)) || (($this->session["_COMPANY_DETAILS_"]['11670']["REQUEST_XML"] == 1) AND ($this->siteID == 11670)) )
      {
         $dateOfInsuranceStart = $this->session['_YourDetails_']['date_of_insurance_start_yyyy']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_dd'];

           $dateAndTime          = date("Y/m/d H:i:s");

           $quoteRef          = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];
           $postcode          = $this->session['_YourDetails_']['postcode'];
           $title             = $this->session['_YourDetails_']['title'];
           $firstName         = $this->session['_YourDetails_']['first_name'];
           $lastName          = $this->session['_YourDetails_']['surname'];
           $name              = $title." ".$firstName." ".$lastName;
           $daytime_telephone = $this->session['_YourDetails_']['daytime_telephone'];
           $mobile_telephone  = $this->session['_YourDetails_']['mobile_telephone'];
           $email             = $this->session['_YourDetails_']['email_address'];
           $businessTrade     = $this->session['_YourDetails_']['business_trade'];
           $businessTrade     = str_replace(array("&","&amp;"),"and",$businessTrade);

           $dest = $daytime_telephone?$daytime_telephone:$mobile_telephone;

           $destinationArr = array(9626=>"SO",10245=>"FP",11115=>"SO",11670=>"FP",);

           $daytime_telephone = $this->session['_YourDetails_']['daytime_telephone'];
           $mobile_telephone  = $this->session['_YourDetails_']['mobile_telephone'];
           $mobile            = substr($mobile_telephone, 0,2);
           $mobile == "07"?$daytime_telephone=$mobile_telephone:$daytime_telephone;
           $dest              = $daytime_telephone?$daytime_telephone:$mobile_telephone;

           //consent details - for not details are not on the system
           if(isset($this->session['consent_statements_values']['companyMarketing']['Email']))
           {
              $emailConsent = "No"; 
              if($this->session['consent_statements_values']['companyMarketing']['Email'] == 1)
                 $emailConsent = "Yes";
           }
           else
           $emailConsent = "Yes"; //update to No once consent added on the system   

           if(isset($this->session['consent_statements_values']['companyMarketing']['SMS']))
           {
              $smsConsent = "No"; //update to No once consent added on the system
              if($this->session['consent_statements_values']['companyMarketing']['SMS'] == 1)
                 $smsConsent = "Yes";
           }
           else
           $smsConsent = "Yes";   

            // $url  = "https://testwsleads.eldoninsurance.co.uk:8444/TestWSLeads/Eldon.LeadsWebService.Leads.svc";
           $url  = "https://wsleads.eldoninsurance.co.uk:443/WSLeads/Eldon.LeadsWebService.Leads.svc";

           $xmlData = '<?xml version="1.0" encoding="utf-8"?>
                            <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:lead="http://www.eldoninsurance.co.uk/schemas/leads" xmlns:eld="http://schemas.datacontract.org/2004/07/Eldon.LeadsWebService">
                              <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                              <wsa:Action>http://www.eldoninsurance.co.uk/schemas/leads/ILeads/SubmitLead</wsa:Action>
                            <wsa:To>'.$url.'</wsa:To>
                            </soap:Header>
                              <soap:Body>
                              <lead:SubmitLead>
                                 <!--Optional:-->
                                 <lead:lead>
                                    <!--Optional:-->
                                    <eld:AdditionalItem1></eld:AdditionalItem1>
                                    <!--Optional:-->
                                    <eld:AdditionalItem10></eld:AdditionalItem10>
                                    <!--Optional:-->
                                    <eld:AdditionalItem11></eld:AdditionalItem11>
                                    <!--Optional:-->
                                    <eld:AdditionalItem12></eld:AdditionalItem12>
                                    <!--Optional:-->
                                    <eld:AdditionalItem13></eld:AdditionalItem13>
                                    <!--Optional:-->
                                    <eld:AdditionalItem14></eld:AdditionalItem14>
                                    <!--Optional:-->
                                    <eld:AdditionalItem15></eld:AdditionalItem15>
                                    <!--Optional:-->
                                    <eld:AdditionalItem16></eld:AdditionalItem16>
                                    <!--Optional:-->
                                    <eld:AdditionalItem17></eld:AdditionalItem17>
                                    <!--Optional:-->
                                    <eld:AdditionalItem18></eld:AdditionalItem18>
                                    <!--Optional:-->
                                    <eld:AdditionalItem19></eld:AdditionalItem19>
                                    <!--Optional:-->
                                    <eld:AdditionalItem2></eld:AdditionalItem2>
                                    <!--Optional:-->
                                    <eld:AdditionalItem20></eld:AdditionalItem20>
                                    <!--Optional:-->
                                    <eld:AdditionalItem98>ContactBySms</eld:AdditionalItem98>
                                    <!--Optional:-->
                                    <eld:AdditionalItem98Value>'.$smsConsent.'</eld:AdditionalItem98Value>
                                    <!--Optional:-->
                                    <eld:AdditionalItem99>ContactByEmail</eld:AdditionalItem99>
                                    <!--Optional:-->
                                    <eld:AdditionalItem99Value>'.$emailConsent.'</eld:AdditionalItem99Value>
                                    <!--Optional:-->
                                    <eld:AdditionalItem3></eld:AdditionalItem3>
                                    <!--Optional:-->
                                    <eld:AdditionalItem4></eld:AdditionalItem4>
                                    <!--Optional:-->
                                    <eld:AdditionalItem5></eld:AdditionalItem5>
                                    <!--Optional:-->
                                    <eld:AdditionalItem6></eld:AdditionalItem6>
                                    <!--Optional:-->
                                    <eld:AdditionalItem7></eld:AdditionalItem7>
                                    <!--Optional:-->
                                    <eld:AdditionalItem8></eld:AdditionalItem8>
                                    <!--Optional:-->
                                    <eld:AdditionalItem9></eld:AdditionalItem9>
                                    <!--Optional:-->
                                    <eld:ClassOfUse></eld:ClassOfUse>
                                    <!--Optional:-->
                                    <eld:ClientPostcode>'.$postcode.'</eld:ClientPostcode>
                                    <eld:CoverDate>'.$dateOfInsuranceStart.'</eld:CoverDate>
                                    <eld:Destination>'.$destinationArr[$this->siteID].'</eld:Destination>
                                    <!--Optional:-->
                                    <eld:Email>'.$email.'</eld:Email>
                                    <!--Optional:-->
                                    <eld:Insurer></eld:Insurer>
                                    <!--Optional:-->
                                    <eld:Name>'.$name.'</eld:Name>
                                    <!--Optional:-->
                                    <eld:Occupation></eld:Occupation>
                                    <!--Optional:-->
                                    <eld:PolicyCover></eld:PolicyCover>
                                    <!--Optional:-->
                                    <eld:Premium>0.00</eld:Premium>
                                    <eld:ProductType>TX</eld:ProductType>
                                    <eld:Reference>'.$quoteRef.'</eld:Reference>
                                    <eld:Sender>Quotezone</eld:Sender>
                                    <eld:Source>QZone Taxi</eld:Source>
                                    <eld:Tel1>'.$dest.'</eld:Tel1>
                                    <!--Optional:-->
                                    <eld:Tel2></eld:Tel2>
                                 </lead:lead>
                              </lead:SubmitLead>
                              </soap:Body>
                            </soap:Envelope>';
          
             
           $header[] = 'SOAPAction: "http://www.eldoninsurance.co.uk/schemas/leads/ILeads/SubmitLead"';
           $header[] = 'Content-Type: application/soap+xml;charset=UTF-8;action="http://www.eldoninsurance.co.uk/schemas/leads/ILeads/SubmitLead"';
           $header[] = "Content-length: ".strlen($xmlData);
           $header[] = "Host: wsleads.eldoninsurance.co.uk:443";
           $header[] = "Connection: close \r\n";
           $header[] = $xmlData;

           
           $ch = curl_init();
           curl_setopt($ch, CURLOPT_URL,$url);
           curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
           curl_setopt($ch, CURLOPT_POST, TRUE);
           //curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
           curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
           curl_setopt($ch, CURLOPT_TIMEOUT, 300);
           curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
           curl_setopt($ch, CURLOPT_VERBOSE, 1);
           curl_setopt($ch, CURLOPT_HEADER, 1);
           $result = curl_exec($ch);
           $error  = curl_error($ch);
           curl_close($ch);


           print "\n========== result ===============\n";
           print $result;
           print "\n========== result ===============\n";

           $now = date("H:i:s");

           $xmlMessageBody = prepareXMLMessageBody($xmlData, $result);

           $headers  = 'MIME-Version: 1.0' . "\r\n";
           $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
           //$headers .= "From: <motorhome@quotezone.co.uk>\r\n";
           ////$headers .= "X-Sender: <motorhome@quotezone.co.uk>\r\n";
           $headers .= "X-Mailer: PHP\r\n"; // mailer
           $headers .= "X-Priority: 1\r\n"; // Urgent message!
           ////$headers .= "Return-Path: <motorhome@quotezone.co.uk>\r\n";

            if($this->siteID == 9626)
               $emailSubjDetails = " Taxi Private Hire Go Skippy (9626)- XML Method";
            if($this->siteID == 10245)
               $emailSubjDetails = " Taxi Public Hire Go Skippy (10245)- XML Method";          
            if($this->siteID == 11115)
               $emailSubjDetails = " Taxi (Eldon) Go Skippy (11115)- XML Method";
            if($this->siteID == 11670)
               $emailSubjDetails = " Taxi (PCO) Go Skippy BCD (11670)- XML Method";

           $mailTo      = "eb2-technical@seopa.com";
           $mailFrom    = "taxi@quotezone.co.uk";
           $mailSubject = $subject. $emailSubjDetails;
           $mailBody    = $xmlMessageBody;
           $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

           $mailTo      = "leads@seopa.com";
           $mailFrom    = "taxi@quotezone.co.uk";
           $mailSubject = $subject. $emailSubjDetails;
           $mailBody    = $xmlMessageBody;
           $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);


           $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

           if( preg_match("/OK/i",$result))
               $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
           else
               $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

           $sentContent =  "\nURL: ".$url."\nPARAMS:".$params;

           $this->DoOutboundFiles("xml", $sentContent,$result);
         }
   }
   
   
   if($this->outbounds['XML'])
   {
     if(($this->session["_COMPANY_DETAILS_"]['399']["REQUEST_XML"] == 1) AND ($this->siteID == 399) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
     {
         $taxiUsedFor    = $this->session['_YourDetails_']['taxi_used_for'];
         if ($taxiUsedFor == "1")
            $taxiUsedFor = "Private";
         else
            $taxiUsedFor = "Public";

         $taxiType           = $this->session['_YourDetails_']['taxi_type'];
         $taxiTypeOfCover    = $this->session['_YourDetails_']['type_of_cover'];
         $taxiVehicleMileage = $this->session['_YourDetails_']['vehicle_mileage'];
         $taxiMake           = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel          = $this->session['_YourDetails_']['taxi_model'];
         $taxiManufYear      = $this->session['_YourDetails_']['year_of_manufacture'];
         $estimatedValue     = $this->session['_YourDetails_']['estimated_value'];
         $taxiCapacity       = $this->session['_YourDetails_']['taxi_capacity'];
         $taxiNcb            = $this->session['_YourDetails_']['taxi_ncb'];

         $bestDayToCallValue  = '';
         $bestTimeToCallValue = $this->session['_YourDetails_']['best_time_call'];
         $dateAndTime         = date("Y/m/d H:i:s");
      
         $dateOfInsuranceStart = $this->session['_YourDetails_']['date_of_insurance_start_dd']."/".$this->session['_YourDetails_']['date_of_insurance_start_mm']."/".$this->session['_YourDetails_']['date_of_insurance_start_yyyy'];
      
         $title            = $this->session['_YourDetails_']['title'];
         $firstName        = $this->session['_YourDetails_']['first_name'];
         $surname          = $this->session['_YourDetails_']['surname'];
         $birthDate        = $this->session['_YourDetails_']['date_of_birth_dd'] ."/". $this->session['_YourDetails_']['date_of_birth_mm'] ."/". $this->session['_YourDetails_']['date_of_birth_yyyy'];
         $dayTimePhone     = $this->session['_YourDetails_']['daytime_telephone'];
         $mobilePhone      = $this->session['_YourDetails_']['mobile_telephone'];
         $postcode         = $this->session['_YourDetails_']['postcode'];
         $houseNumber      = $this->session['_YourDetails_']['house_number_or_name'];
         $addr1            = $this->session['_YourDetails_']['address_line1'];
         $addr2            = $this->session['_YourDetails_']['address_line2'];
         $addr3            = $this->session['_YourDetails_']['address_line3'];
         $addr4            = $this->session['_YourDetails_']['address_line4'];
         $email            = $this->session['_YourDetails_']['email_address'];
         $bestTime         = $this->session['_YourDetails_']['best_time_call'];
         $bestDay          = '';
         $webref           = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];
         $sex = "";
         if ($this->session['_YourDetails_']['title'] == "Mr")
            $sex = "M";
         else 
            $sex = "F";
         $dateAndTime         = date("Y/m/d H:i:s");
      
         $message_body = "<?xml version=\"1.0\"?>
            <clientdata>
               <referrerid>48</referrerid>
               <webref>$webref</webref>
                  <proposer>
                     <fname>$firstName</fname>
                     <sname>$surname</sname>
                     <coname></coname>
                     <add1>$houseNumber</add1>
                     <add2>$addr2</add2>
                     <town>$addr3</town>
                     <county>$addr4</county>
                     <postcode>$postcode</postcode>
                     <tel>$dayTimePhone</tel>
                     <mobile>$mobilePhone</mobile>
                     <email>$email</email>
                     <dob>$birthDate</dob>
                     <sex>$sex</sex>
                     <mstatus></mstatus>
                  </proposer>
                  <driver>
                     <fname>$firstName</fname>
                     <sname>$surname</sname>
                     <dob>$birthDate</dob>
                     <relationshiptoprop>Proposer</relationshiptoprop>
                     <employmentstatus></employmentstatus>
                     <mainocc></mainocc>
                     <bustype></bustype>
                     <liclength></liclength>
                     <badgelength></badgelength>
                  </driver>
                  <taxi>
                     <make>$taxiMake</make>
                     <modelandderivative>$taxiModel</modelandderivative>
                     <body>$taxiType</body>
                     <engsize></engsize>
                     <year>$taxiManufYear</year>
                     <value>$estimatedValue</value>
                     <reg></reg>
                     <postcode>$postcode</postcode>
                     <storagelocation></storagelocation>
                     <ncb>$taxiNcb</ncb>
                     <ncbtype>taxi</ncbtype>
                     <use>$taxiUsedFor</use>
                     <mileage>$taxiVehicleMileage</mileage>
                     <platingauth></platingauth>
                     <hiretype>$taxiUsedFor</hiretype>
                     <drvrestrict></drvrestrict>
                     <security></security>
                     <addncb></addncb>
                     <addncbtype></addncbtype>
                     <purchdate></purchdate>
                  </taxi>
            </clientdata>
         ";

         //old URL : https://www.shlquotes.co.uk/xmlImport.php
         //new URL : http://www.shlquotes.co.uk/leads/quotezone/taxi/

         $url    = "http://www.shlquotes.co.uk/leads/quotezone/taxi/";
         $ch     = curl_init();
         $header = array();
         $header[] = "POST /leads/quotezone/taxi/ HTTP/1.1";
         $header[] = "Host: www.shlquotes.co.uk";
         $header[] = "Content-Type: application/x-www-form-urlencoded";
         $header[] = "Connection: close \r\n";
         $header[] = $message_body;

         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $message_body);
         curl_setopt($ch, CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 30);
         curl_setopt($ch, CURLOPT_VERBOSE, 1);
         curl_setopt($ch, CURLOPT_HEADER, 1);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  0);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         $result = curl_exec($ch);
         curl_close ($ch);
    
         print "\n========== result ===============\n";
         print $result;
         print "\n========== result ===============\n";


            $xmlMessageBody = prepareXMLMessageBody($message_body, $result); 

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
            ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!
            ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

            //eb2
            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Staveley Head - XML Method";
            $mailBody    = $xmlMessageBody;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

            //seopa
            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Staveley Head - XML Method";
            $mailBody    = $xmlMessageBody;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         
         
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

            if( preg_match("/Success/i",$result))
               $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
            else
               $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

            $sentContent =  "\nURL: ".$url."\nPARAMS:".$message_body;

            $this->DoOutboundFiles("xml", $sentContent,$result);
     }


      if(($this->session["_COMPANY_DETAILS_"]['12233']["REQUEST_XML"] == 1) AND ($this->siteID == 12233) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      // if(($this->session["_COMPANY_DETAILS_"]['12233']["REQUEST_XML"] == 1) AND ($this->siteID == 12233) )
     {
         $taxiUsedFor    = $this->session['_YourDetails_']['taxi_used_for'];
         if ($taxiUsedFor == "1")
            $taxiUsedFor = "Private";
         else
            $taxiUsedFor = "Public";

         $taxiType           = $this->session['_YourDetails_']['taxi_type'];
         $taxiTypeOfCover    = $this->session['_YourDetails_']['type_of_cover'];
         $taxiVehicleMileage = $this->session['_YourDetails_']['vehicle_mileage'];
         $taxiMake           = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel          = $this->session['_YourDetails_']['taxi_model'];
         $taxiManufYear      = $this->session['_YourDetails_']['year_of_manufacture'];
         $estimatedValue     = $this->session['_YourDetails_']['estimated_value'];
         $taxiCapacity       = $this->session['_YourDetails_']['taxi_capacity'];
         $taxiNcb            = $this->session['_YourDetails_']['taxi_ncb'];

         $bestDayToCallValue  = '';
         $bestTimeToCallValue = $this->session['_YourDetails_']['best_time_call'];
         $dateAndTime         = date("Y/m/d H:i:s");
      
         $dateOfInsuranceStart = $this->session['_YourDetails_']['date_of_insurance_start_dd']."/".$this->session['_YourDetails_']['date_of_insurance_start_mm']."/".$this->session['_YourDetails_']['date_of_insurance_start_yyyy'];
      
         $title            = $this->session['_YourDetails_']['title'];
         $firstName        = $this->session['_YourDetails_']['first_name'];
         $surname          = $this->session['_YourDetails_']['surname'];
         $birthDate        = $this->session['_YourDetails_']['date_of_birth_dd'] ."/". $this->session['_YourDetails_']['date_of_birth_mm'] ."/". $this->session['_YourDetails_']['date_of_birth_yyyy'];
         $dayTimePhone     = $this->session['_YourDetails_']['daytime_telephone'];
         $mobilePhone      = $this->session['_YourDetails_']['mobile_telephone'];
         $postcode         = $this->session['_YourDetails_']['postcode'];
         $houseNumber      = $this->session['_YourDetails_']['house_number_or_name'];
         $addr1            = $this->session['_YourDetails_']['address_line1'];
         $addr2            = $this->session['_YourDetails_']['address_line2'];
         $addr3            = $this->session['_YourDetails_']['address_line3'];
         $addr4            = $this->session['_YourDetails_']['address_line4'];
         $email            = $this->session['_YourDetails_']['email_address'];
         $bestTime         = $this->session['_YourDetails_']['best_time_call'];
         $bestDay          = '';
         $webref           = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];
         $sex = "";
         if ($this->session['_YourDetails_']['title'] == "Mr")
            $sex = "M";
         else 
            $sex = "F";
         $dateAndTime         = date("Y/m/d H:i:s");
      
         $message_body = "<?xml version=\"1.0\"?>
            <clientdata>
               <referrerid>48</referrerid>
               <webref>$webref</webref>
                  <proposer>
                     <fname>$firstName</fname>
                     <sname>$surname</sname>
                     <coname></coname>
                     <add1>$houseNumber</add1>
                     <add2>$addr2</add2>
                     <town>$addr3</town>
                     <county>$addr4</county>
                     <postcode>$postcode</postcode>
                     <tel>$dayTimePhone</tel>
                     <mobile>$mobilePhone</mobile>
                     <email>$email</email>
                     <dob>$birthDate</dob>
                     <sex>$sex</sex>
                     <mstatus></mstatus>
                  </proposer>
                  <driver>
                     <fname>$firstName</fname>
                     <sname>$surname</sname>
                     <dob>$birthDate</dob>
                     <relationshiptoprop>Proposer</relationshiptoprop>
                     <employmentstatus></employmentstatus>
                     <mainocc></mainocc>
                     <bustype></bustype>
                     <liclength></liclength>
                     <badgelength></badgelength>
                  </driver>
                  <taxi>
                     <make>$taxiMake</make>
                     <modelandderivative>$taxiModel</modelandderivative>
                     <body>$taxiType</body>
                     <engsize></engsize>
                     <year>$taxiManufYear</year>
                     <value>$estimatedValue</value>
                     <reg></reg>
                     <postcode>$postcode</postcode>
                     <storagelocation></storagelocation>
                     <ncb>$taxiNcb</ncb>
                     <ncbtype>taxi</ncbtype>
                     <use>$taxiUsedFor</use>
                     <mileage>$taxiVehicleMileage</mileage>
                     <platingauth></platingauth>
                     <hiretype>$taxiUsedFor</hiretype>
                     <drvrestrict></drvrestrict>
                     <security></security>
                     <addncb></addncb>
                     <addncbtype></addncbtype>
                     <purchdate></purchdate>
                  </taxi>
            </clientdata>
         ";

         //old URL : https://www.shlquotes.co.uk/xmlImport.php
         //new URL : http://www.shlquotes.co.uk/leads/quotezone/taxi/

         $url    = "http://www.shlquotes.co.uk/leads/quotezone/taxi/";
         $ch     = curl_init();
         $header = array();
         $header[] = "POST /leads/quotezone/taxi/ HTTP/1.1";
         $header[] = "Host: www.shlquotes.co.uk";
         $header[] = "Content-Type: application/x-www-form-urlencoded";
         $header[] = "Connection: close \r\n";
         $header[] = $message_body;

         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $message_body);
         curl_setopt($ch, CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 30);
         curl_setopt($ch, CURLOPT_VERBOSE, 1);
         curl_setopt($ch, CURLOPT_HEADER, 1);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  0);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         $result = curl_exec($ch);
         curl_close ($ch);
    
         print "\n========== result ===============\n";
         print $result;
         print "\n========== result ===============\n";


            $xmlMessageBody = prepareXMLMessageBody($message_body, $result); 

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
            ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!
            ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

            //eb2
            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Staveley Head OOH- XML Method";
            $mailBody    = $xmlMessageBody;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

            //seopa
            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Staveley Head OOH - XML Method";
            $mailBody    = $xmlMessageBody;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         
         
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

            if( preg_match("/Success/i",$result))
               $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
            else
               $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

            $sentContent =  "\nURL: ".$url."\nPARAMS:".$message_body;

            $this->DoOutboundFiles("xml", $sentContent,$result);
     }

       //Brightside - Taxi 3 xml request
      if(($this->session["_COMPANY_DETAILS_"]['9999']["REQUEST_XML"] == 1) AND ($this->siteID == 9999)  AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {

         $subject        = "Taxi query : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];
         $quoteRef       = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];
         $firstName      = $this->session['_YourDetails_']['first_name'];
         $lastName       = $this->session['_YourDetails_']['surname'];
         $email          = $this->session['_YourDetails_']['email_address'];
         $houseNrName    = $this->session['_YourDetails_']['house_number_or_name'];
         $houseNrName    = str_replace("&", "and", $houseNrName);

         $DOB            = $this->session['_YourDetails_']['date_of_birth_dd']."-".$this->session['_YourDetails_']['date_of_birth_mm']."-".$this->session['_YourDetails_']['date_of_birth_yyyy'];
         $DIS            = $this->session['_YourDetails_']['date_of_insurance_start_dd']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_yyyy']." ".date("H:m");
         $inceptionDate  = $this->session['_YourDetails_']['date_of_insurance_start_dd']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_yyyy'];

         $postcode       = $this->session['_YourDetails_']['postcode_prefix'].$this->session['_YourDetails_']['postcode_number'];
         $telephone      = $this->session['_YourDetails_']['daytime_telephone'];
         $mobilePhone    = $this->session['_YourDetails_']['mobile_telephone'];
         $taxiMake       = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel      = $this->session['_YourDetails_']['taxi_model'];
         
         if($this->session['_YourDetails_']['daytime_telephone'] == "")
            $telephone   = $this->session['_YourDetails_']['mobile_telephone'];

         if($this->session['_YourDetails_']['mobile_telephone'] == "")
            $mobilePhone = $this->session['_YourDetails_']['daytime_telephone'];
    
         $estimatedValue = $this->session['_YourDetails_']['estimated_value'];

         $quoteDateTime  = date("d-m-Y H:i");

         $xmlData    = '<?xml version="1.0" encoding="utf-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:brig="http://www.brightsideinsurance.co.uk/">
            <soapenv:Header/>
            <soapenv:Body>
               <brig:UploadLead>
               <!--Optional:-->
               <brig:RecordXML>
                  &lt;TopXLeadRequest&gt;
                     &lt;QuoteReference&gt;'.$quoteRef.'&lt;/QuoteReference&gt;
                     &lt;Forename&gt;'.$firstName.'&lt;/Forename&gt;
                     &lt;Surname&gt;'.$lastName.'&lt;/Surname&gt;
                     &lt;DateOfBirth&gt;'.$DOB.'&lt;/DateOfBirth&gt;
                     &lt;PhoneNumber1&gt;'.$telephone.'&lt;/PhoneNumber1&gt;
                     &lt;PhoneNumber2&gt;'.$mobilePhone.'&lt;/PhoneNumber2&gt;
                     &lt;EmailAddress1&gt;'.$email.'&lt;/EmailAddress1&gt;
                     &lt;AddressLine1&gt;'.$houseNrName.'&lt;/AddressLine1&gt;
                     &lt;Postcode&gt;'.$postcode.'&lt;/Postcode&gt;
                     &lt;Make&gt;'.$taxiMake.'&lt;/Make&gt;
                     &lt;Model&gt;'.$taxiModel.'&lt;/Model&gt;
                     &lt;Premium&gt;&lt;/Premium&gt;
                     &lt;TopXPosition&gt;1&lt;/TopXPosition&gt;
                     &lt;InceptionDate&gt;'.$inceptionDate.'&lt;/InceptionDate&gt;
                     &lt;QuoteProviderID&gt;14&lt;/QuoteProviderID&gt;                            
                     &lt;QuoteTypeID&gt;215&lt;/QuoteTypeID&gt;
                     &lt;QuoteDateTime&gt;'.$quoteDateTime.'&lt;/QuoteDateTime&gt;
                  &lt;/TopXLeadRequest&gt;
               </brig:RecordXML>
               </brig:UploadLead>
            </soapenv:Body>
         </soapenv:Envelope>';

         $url = "https://leadsource.brightsidegroup.co.uk/insert.asmx";

         $ch = curl_init();

         $header[] = "POST /insert.asmx HTTP/1.1";
         $header[] = "Host: leadsource.brightsidegroup.co.uk";
         // $header[] = "Content-Type: application/soap+xml; charset=utf-8";
         $header[] = "Content-Type: text/xml; charset=utf-8";
         $header[] = "Content-length: ".strlen($xmlData);
         $header[] = "SOAPAction: \"http://www.brightsideinsurance.co.uk/UploadLead\"";
         $header[] = "Connection: close \r\n";
         $header[] = $xmlData;

         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
         curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 180);

         curl_setopt($ch, CURLOPT_VERBOSE, 1);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_HEADER, 1);

         $result  = curl_exec($ch);
         $erro    = curl_error($ch);
         $errNo   = curl_errno($ch);

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";


         if(! preg_match("/Record Accepted/i",$result))
         {

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!

            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Brightside (OIS TQ) - Taxi (SW) 9999 - XML Method NEW 1st Request Failed";
            $mailBody    = "Response is a failure :\n $result($erro) \n";
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Brightside (OIS TQ) - Taxi (SW) 9999 - XML Method NEW 1st Request Failed";
            $mailBody    = "Response is a failure :\n $result($erro) \n";
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

            //sleep 10 seconds then retry to send thed deta
            sleep(10);

            //rerun the request
            $result  = curl_exec($ch);
            $erro    = curl_error($ch);
            $errNo   = curl_errno($ch);
         }

        
         curl_close ($ch);

         $xmlMessageBody = prepareXMLMessageBody($xmlData, $result);

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Brightside (OIS TQ) - Taxi (SW) 9999 - XML Method NEW";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Brightside (OIS TQ) - Taxi (SW) 9999 - XML Method NEW";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if( preg_match("/Record Accepted/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
         else
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

         $sentContent =  "\nURL: ".$url."\n<br>PARAMS:".$xmlData;

         $this->DoOutboundFiles("xml", $sentContent,$result);

         //END NEW brightsideinsurance REQUEST
      }

      // Brightside - Taxi  xml request
      if(($this->session["_COMPANY_DETAILS_"]['400']["REQUEST_XML"] == 1) AND ($this->siteID == 400)  AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {
         
         $subject = "Taxi query : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];
              
         $quoteRef     = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];
         $firstName    = $this->session['_YourDetails_']['first_name'];
         $lastName     = $this->session['_YourDetails_']['surname'];


         $email        = $this->session['_YourDetails_']['email_address'];
         $houseNrName  = $this->session['_YourDetails_']['house_number_or_name'];
         $houseNrName  = str_replace("&", "and", $houseNrName);


         $DOB  = $this->session['_YourDetails_']['date_of_birth_dd']."-".$this->session['_YourDetails_']['date_of_birth_mm']."-".$this->session['_YourDetails_']['date_of_birth_yyyy'];
         $DIS  = $this->session['_YourDetails_']['date_of_insurance_start_dd']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_yyyy']." ".date("H:m");
         $inceptionDate  = $this->session['_YourDetails_']['date_of_insurance_start_dd']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_yyyy'];

         $postcode     = $this->session['_YourDetails_']['postcode_prefix'].$this->session['_YourDetails_']['postcode_number'];
         $telephone    = $this->session['_YourDetails_']['daytime_telephone'];
         $mobilePhone  = $this->session['_YourDetails_']['mobile_telephone'];
         $taxiMake             = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel            = $this->session['_YourDetails_']['taxi_model'];
         
             if($this->session['_YourDetails_']['daytime_telephone'] == "")
               $telephone = $this->session['_YourDetails_']['mobile_telephone'];
            
             if($this->session['_YourDetails_']['mobile_telephone'] == "")
               $mobilePhone = $this->session['_YourDetails_']['daytime_telephone'];
    
         $estimatedValue   = $this->session['_YourDetails_']['estimated_value'];

         $quoteDateTime  = date("d-m-Y H:i");


         $xmlData    = '<?xml version="1.0" encoding="utf-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:brig="http://www.brightsideinsurance.co.uk/">
            <soapenv:Header/>
            <soapenv:Body>
               <brig:UploadLead>
               <!--Optional:-->
               <brig:RecordXML>
                  &lt;TopXLeadRequest&gt;
                     &lt;QuoteReference&gt;'.$quoteRef.'&lt;/QuoteReference&gt;
                     &lt;Forename&gt;'.$firstName.'&lt;/Forename&gt;
                     &lt;Surname&gt;'.$lastName.'&lt;/Surname&gt;
                     &lt;DateOfBirth&gt;'.$DOB.'&lt;/DateOfBirth&gt;
                     &lt;PhoneNumber1&gt;'.$telephone.'&lt;/PhoneNumber1&gt;
                     &lt;PhoneNumber2&gt;'.$mobilePhone.'&lt;/PhoneNumber2&gt;
                     &lt;EmailAddress1&gt;'.$email.'&lt;/EmailAddress1&gt;
                     &lt;AddressLine1&gt;'.$houseNrName.'&lt;/AddressLine1&gt;
                     &lt;Postcode&gt;'.$postcode.'&lt;/Postcode&gt;
                     &lt;Make&gt;'.$taxiMake.'&lt;/Make&gt;
                     &lt;Model&gt;'.$taxiModel.'&lt;/Model&gt;
                     &lt;Premium&gt;&lt;/Premium&gt;
                     &lt;TopXPosition&gt;1&lt;/TopXPosition&gt;
                     &lt;InceptionDate&gt;'.$inceptionDate.'&lt;/InceptionDate&gt;
                     &lt;QuoteProviderID&gt;14&lt;/QuoteProviderID&gt;                            
                     &lt;QuoteTypeID&gt;170&lt;/QuoteTypeID&gt;
                     &lt;QuoteDateTime&gt;'.$quoteDateTime.'&lt;/QuoteDateTime&gt;
                  &lt;/TopXLeadRequest&gt;
               </brig:RecordXML>
               </brig:UploadLead>
            </soapenv:Body>
         </soapenv:Envelope>';

         $url = "https://leadsource.brightsidegroup.co.uk/insert.asmx";

         $ch = curl_init();

         $header[] = "POST /insert.asmx HTTP/1.1";
         $header[] = "Host: leadsource.brightsidegroup.co.uk";
         // $header[] = "Content-Type: application/soap+xml; charset=utf-8";
         $header[] = "Content-Type: text/xml; charset=utf-8";
         $header[] = "Content-length: ".strlen($xmlData);
         $header[] = "SOAPAction: \"http://www.brightsideinsurance.co.uk/UploadLead\"";
         $header[] = "Connection: close \r\n";
         $header[] = $xmlData;

         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
         curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 180);

         curl_setopt($ch, CURLOPT_VERBOSE, 1);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_HEADER, 1);

         $result  = curl_exec($ch);
         $erro    = curl_error($ch);
         $errNo   = curl_errno($ch);

         if(! preg_match("/Record Accepted/i",$result))
         {
            //send email with the failed response
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!

            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Brightside (OIS TQ) - Taxi (MPV) 400 - XML Method NEW - RETRY";
            $mailBody    = $xmlMessageBody;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Brightside(OIS TQ) - Taxi (MPV) 400 - XML Method - RETRY";
            $mailBody    = $xmlMessageBody;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

            //sleep 10 seconds then retry to send thed deta
            sleep(10);

            //rerun the request
            $result  = curl_exec($ch);
            $erro    = curl_error($ch);
            $errNo   = curl_errno($ch);
         }
        
         curl_close ($ch);

         $xmlMessageBody = prepareXMLMessageBody($xmlData, $result);

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Brightside (OIS TQ) - Taxi (MPV) 400 - XML Method NEW";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Brightside(OIS TQ) - Taxi (MPV) 400 - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if( preg_match("/Record Accepted/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
         else
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

         $sentContent =  "\nURL: ".$url."\n<br>PARAMS:".$xmlData;

         $this->DoOutboundFiles("xml", $sentContent,$result);
         //NEW brightsideinsurance REQUEST
      }
   }
   
   
   if($this->outbounds['XML'])
   {
     //if(($this->session["_COMPANY_DETAILS_"]['9832']["REQUEST_XML"] == 1) AND ($this->siteID == 9832))
     if(($this->session["_COMPANY_DETAILS_"]['9832']["REQUEST_XML"] == 1) AND ($this->siteID == 9832) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
     {
         $taxiUsedFor    = $this->session['_YourDetails_']['taxi_used_for'];
         if ($taxiUsedFor == "1")
            $taxiUsedFor = "Private";
         else
            $taxiUsedFor = "Public";

         $taxiType           = $this->session['_YourDetails_']['taxi_type'];
         $taxiTypeOfCover    = $this->session['_YourDetails_']['type_of_cover'];
         $taxiVehicleMileage = $this->session['_YourDetails_']['vehicle_mileage'];
         $taxiMake           = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel          = $this->session['_YourDetails_']['taxi_model'];
         $taxiManufYear      = $this->session['_YourDetails_']['year_of_manufacture'];
         $estimatedValue     = $this->session['_YourDetails_']['estimated_value'];
         $taxiCapacity       = $this->session['_YourDetails_']['taxi_capacity'];
         $taxiNcb            = $this->session['_YourDetails_']['taxi_ncb'];

         $bestDayToCallValue  = '';
         $bestTimeToCallValue = $this->session['_YourDetails_']['best_time_call'];
         $dateAndTime         = date("Y/m/d H:i:s");
      
         $dateOfInsuranceStart = $this->session['_YourDetails_']['date_of_insurance_start_dd']."/".$this->session['_YourDetails_']['date_of_insurance_start_mm']."/".$this->session['_YourDetails_']['date_of_insurance_start_yyyy'];
      
         $title            = $this->session['_YourDetails_']['title'];
         $firstName        = $this->session['_YourDetails_']['first_name'];
         $surname          = $this->session['_YourDetails_']['surname'];
         $birthDate        = $this->session['_YourDetails_']['date_of_birth_dd'] ."/". $this->session['_YourDetails_']['date_of_birth_mm'] ."/". $this->session['_YourDetails_']['date_of_birth_yyyy'];
         $dayTimePhone     = $this->session['_YourDetails_']['daytime_telephone'];
         $mobilePhone      = $this->session['_YourDetails_']['mobile_telephone'];
         $postcode         = $this->session['_YourDetails_']['postcode'];
         $houseNumber      = $this->session['_YourDetails_']['house_number_or_name'];
         $addr1            = $this->session['_YourDetails_']['address_line1'];
         $addr2            = $this->session['_YourDetails_']['address_line2'];
         $addr3            = $this->session['_YourDetails_']['address_line3'];
         $addr4            = $this->session['_YourDetails_']['address_line4'];
         $email            = $this->session['_YourDetails_']['email_address'];
         $bestTime         = $this->session['_YourDetails_']['best_time_call'];
         $bestDay          = '';
         $webref           = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];
         $sex = "";
         if ($this->session['_YourDetails_']['title'] == "Mr" or $this->session['_YourDetails_']['title'] == "Mrs")
            $sex = "M";
         else 
            $sex = "F";
         $dateAndTime         = date("Y/m/d H:i:s");
      
         $message_body = "<?xml version=\"1.0\"?>
            <clientdata>
               <referrerid>48</referrerid>
               <webref>$webref</webref>
                  <proposer>
                     <fname>$firstName</fname>
                     <sname>$surname</sname>
                     <coname></coname>
                     <add1>$houseNumber</add1>
                     <add2>$addr2</add2>
                     <town>$addr3</town>
                     <county>$addr4</county>
                     <postcode>$postcode</postcode>
                     <tel>$dayTimePhone</tel>
                     <mobile>$mobilePhone</mobile>
                     <email>$email</email>
                     <dob>$birthDate</dob>
                     <sex>$sex</sex>
                     <mstatus></mstatus>
                  </proposer>
                  <driver>
                     <fname>$firstName</fname>
                     <sname>$surname</sname>
                     <dob>$birthDate</dob>
                     <relationshiptoprop>Proposer</relationshiptoprop>
                     <employmentstatus></employmentstatus>
                     <mainocc></mainocc>
                     <bustype></bustype>
                     <liclength></liclength>
                     <badgelength></badgelength>
                  </driver>
                  <taxi>
                     <make>$taxiMake</make>
                     <modelandderivative>$taxiModel</modelandderivative>
                     <body>$taxiType</body>
                     <engsize></engsize>
                     <year>$taxiManufYear</year>
                     <value>$estimatedValue</value>
                     <reg></reg>
                     <postcode>$postcode</postcode>
                     <storagelocation></storagelocation>
                     <ncb>$taxiNcb</ncb>
                     <ncbtype>taxi</ncbtype>
                     <use>$taxiUsedFor</use>
                     <mileage>$taxiVehicleMileage</mileage>
                     <platingauth></platingauth>
                     <hiretype>$taxiUsedFor</hiretype>
                     <drvrestrict></drvrestrict>
                     <security></security>
                     <addncb></addncb>
                     <addncbtype></addncbtype>
                     <purchdate></purchdate>
                  </taxi>
            </clientdata>
         ";

         //old URL : https://www.shlquotes.co.uk/xmlImport.php
         //new URL : http://www.shlquotes.co.uk/leads/quotezone/taxi/
         $url    = "http://www.shlquotes.co.uk/leads/quotezone/taxi/";
         $ch     = curl_init();
         $header = array();
         $header[] = "POST /leads/quotezone/taxi/ HTTP/1.1";
         $header[] = "Host: www.shlquotes.co.uk";
         $header[] = "Content-Type: application/x-www-form-urlencoded";
         $header[] = "Connection: close \r\n";
         $header[] = $message_body;

         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $message_body);
         curl_setopt($ch, CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 30);
         curl_setopt($ch, CURLOPT_VERBOSE, 1);
         curl_setopt($ch, CURLOPT_HEADER, 1);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  0);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         $result = curl_exec($ch);
         curl_close ($ch);
    
         print "\n========== result ===============\n";
         print $result;
         print "\n========== result ===============\n";


            $xmlMessageBody = prepareXMLMessageBody($message_body, $result);

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
            ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!
            ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

            //eb2
            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Staveley Head 2 - XML Method";
            $mailBody    = $xmlMessageBody;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

            //seopa
            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Staveley Head 2 - XML Method";
            $mailBody    = $xmlMessageBody;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         
         
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

            if( preg_match("/Success/i",$result))
               $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
            else
               $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

            $sentContent =  "\nURL: ".$url."\nPARAMS:".$message_body;

            $this->DoOutboundFiles("xml", $sentContent,$result);
     }
   }     
   

   if($this->outbounds['XML'])
   {
      //PLAN INSURANCE - TAXI XML -- 10 -- PLUGINS
      if((($this->session["_COMPANY_DETAILS_"]['347']["REQUEST_XML"] == 1) AND ($this->siteID == 347) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['1085']["REQUEST_XML"] == 1) AND ($this->siteID == 1085) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['2062']["REQUEST_XML"] == 1) AND ($this->siteID == 2062) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['2063']["REQUEST_XML"] == 1) AND ($this->siteID == 2063) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['2952']["REQUEST_XML"] == 1) AND ($this->siteID == 2952) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['3293']["REQUEST_XML"] == 1) AND ($this->siteID == 3293) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['3430']["REQUEST_XML"] == 1) AND ($this->siteID == 3430) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['3431']["REQUEST_XML"] == 1) AND ($this->siteID == 3431) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['11307']["REQUEST_XML"] == 1) AND ($this->siteID == 11307) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['11309']["REQUEST_XML"] == 1) AND ($this->siteID == 11309) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['11305']["REQUEST_XML"] == 1) AND ($this->siteID == 11305) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['11306']["REQUEST_XML"] == 1) AND ($this->siteID == 11306) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['12644']["REQUEST_XML"] == 1) AND ($this->siteID == 12644) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['12645']["REQUEST_XML"] == 1) AND ($this->siteID == 12645) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['12642']["REQUEST_XML"] == 1) AND ($this->siteID == 12642) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['12647']["REQUEST_XML"] == 1) AND ($this->siteID == 12647) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['12643']["REQUEST_XML"] == 1) AND ($this->siteID == 12643) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['12646']["REQUEST_XML"] == 1) AND ($this->siteID == 12646) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) )
       // if((($this->session["_COMPANY_DETAILS_"]['347']["REQUEST_XML"] == 1) AND ($this->siteID == 347)) || (($this->session["_COMPANY_DETAILS_"]['1085']["REQUEST_XML"] == 1) AND ($this->siteID == 1085)) || (($this->session["_COMPANY_DETAILS_"]['2062']["REQUEST_XML"] == 1) AND ($this->siteID == 2062)) || (($this->session["_COMPANY_DETAILS_"]['2063']["REQUEST_XML"] == 1) AND ($this->siteID == 2063)) || (($this->session["_COMPANY_DETAILS_"]['2952']["REQUEST_XML"] == 1) AND ($this->siteID == 2952)) || (($this->session["_COMPANY_DETAILS_"]['3293']["REQUEST_XML"] == 1) AND ($this->siteID == 3293)) || (($this->session["_COMPANY_DETAILS_"]['3430']["REQUEST_XML"] == 1) AND ($this->siteID == 3430)) || (($this->session["_COMPANY_DETAILS_"]['3431']["REQUEST_XML"] == 1) AND ($this->siteID == 3431)) || (($this->session["_COMPANY_DETAILS_"]['11307']["REQUEST_XML"] == 1) AND ($this->siteID == 11307)) || (($this->session["_COMPANY_DETAILS_"]['11309']["REQUEST_XML"] == 1) AND ($this->siteID == 11309)) || (($this->session["_COMPANY_DETAILS_"]['11305']["REQUEST_XML"] == 1) AND ($this->siteID == 11305)) || (($this->session["_COMPANY_DETAILS_"]['11306']["REQUEST_XML"] == 1) AND ($this->siteID == 11306)) || (($this->session["_COMPANY_DETAILS_"]['12644']["REQUEST_XML"] == 1) AND ($this->siteID == 12644)) || (($this->session["_COMPANY_DETAILS_"]['12645']["REQUEST_XML"] == 1) AND ($this->siteID == 12645)) || (($this->session["_COMPANY_DETAILS_"]['12642']["REQUEST_XML"] == 1) AND ($this->siteID == 12642)) || (($this->session["_COMPANY_DETAILS_"]['12647']["REQUEST_XML"] == 1) AND ($this->siteID == 12647)) || (($this->session["_COMPANY_DETAILS_"]['12643']["REQUEST_XML"] == 1) AND ($this->siteID == 12643)) || (($this->session["_COMPANY_DETAILS_"]['12646']["REQUEST_XML"] == 1) AND ($this->siteID == 12646)))
        {
         $planInsuranceArr = array(
                                 347 => "PlanInsurance-TaxiAAABQuotezoneTaxiNBCB",
                                 1085 => "PlanInsurance-TaxiNBQuotezoneC2OfficeHours",
                                 2062 => "PlanInsurance-TaxiNBQuotezoneC1OutOfHours",
                                 2063 => "PlanInsurance-TaxiNBQuotezoneC2OutOfHours",
                                 2952 => "PlanInsurance-TaxiNBQuotezoneBCOfficeHours",
                                 3293 => "PlanInsurance-TaxiNBQuotezoneBCOutOfOfficeHours",
                                 3430 => "PlanInsurance-TaxiNBQuotezoneC3OfficeHours",
                                 3431 => "PlanInsurance-TaxiNBQuotezoneC3OutOfHours",
                                 11307 => "Plan Insurance - Taxi NB Quotezone BC Weekend (taxi)",
                                 11309 => "Plan Insurance - Taxi NB Quotezone C1 Weekend (taxi)",
                                 11305 => "Plan Insurance - Taxi NB Quotezone C2 Weekend (taxi)",
                                 11306 => "Plan Insurance - Taxi NB Quotezone C3 Weekend (taxi)",
                                 12644 => "Plan Insurance - Taxi (CHAUFFEUR 15K VEHICLE C4) OH (taxi)",
                                 12645 => "Plan Insurance - Taxi (CHAUFFEUR 15K VEHICLE C4) OOH (taxi)",
                                 12642 => "Plan Insurance - Taxi (PRIVH 12 VEHICLE C4) OH (taxi)",
                                 12647 => "Plan Insurance - Taxi (PRIVH 12 VEHICLE C4) OOH (taxi)",
                                 12643 => "Plan Insurance - Taxi (PUBH 7K VEHICLE C4) OH (taxi)",
                                 12646 => "Plan Insurance - Taxi (PUBH 7K VEHICLE C4) OOH (taxi)",
                              );


         foreach($planInsuranceArr as $keyId => $companyNameValue)
         {
            if($this->siteID == $keyId)
            {
               $xmlString        = GetEmailTemplate($this->session,"PLAN_XML_".$keyId);  
               $mailSubject      = $subject." - $companyNameValue - id$keyId - XML Method";  
            }
         }
     
         //$url = "http://onlinequotetest.planinsurance.co.uk:8991/OnlineQuote.OnlineQuoteHandler.svc?singleWsdl";
         // $url = "http://onlinequote.planinsurance.co.uk:8991/onlinequote.onlinequotehandler.svc?singleWsdl";
         $url = "http://onlinequote.planinsurance.co.uk:8991/OnlineQuote.OnlineQuoteHandler.svc";
                 

         $ch       = curl_init();
         $header   = array();
         $header[] = "POST /OnlineQuote.OnlineQuoteHandler.svc HTTP/1.1";
         $header[] = "Host: onlinequote.planinsurance.co.uk:8991";
         $header[] = "Content-Type: text/xml; charset=utf-8";
         $header[] = "Content-length: ".strlen($xmlString);
         $header[] = "SOAPAction: \"http://tempuri.org/IOnlineQuoteHandler/AddDataLeadGeneratorGDPR\"";
         $header[] = "Cache-Control: no-cache";
         $header[] = "Connection: close \r\n";
         $header[] = $xmlString;

         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
         curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 180);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_VERBOSE, true);
         curl_setopt($ch, CURLOPT_HEADER, 1);

         $result = curl_exec($ch);
         $error  = curl_error($ch).curl_errno($ch);
         curl_close ($ch);                 
            
         $xmlMessageBody = prepareXMLMessageBody($xmlString, $result);

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <limo@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <limo@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <limo@quotezone.co.uk>\r\n";

         //eb2
         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         // $mailSubject = $subject." - Taxi AAAB Quotezone Taxi NB CB - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         //seopa
         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         // $mailSubject = $subject."- Taxi AAAB Quotezone Taxi NB CB - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         //company email 
         $mailTo      = "Norman@planinsurance.co.uk";
         $mailFrom    = "taxi@quotezone.co.uk";
         // $mailSubject = $subject."- Taxi AAAB Quotezone Taxi NB CB - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);


      $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

      if(! strlen($result))
        $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 3); //NO RESPONSE

      if( preg_match("/<AddDataLeadGeneratorGDPRResult>true<\/AddDataLeadGeneratorGDPRResult>/i",$result))
        $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1); //OK RESPONSE
      else
        $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);//BAD RESPONSE

      $this->DoOutboundFiles("xml", $xmlString, $result);            
         
      }// END of TOTAL of 8 plugins
   }

   
   if($this->outbounds['CSV'])
   {   
      $mailFrom  = "taxi@quotezone.co.uk";   
       
      if((($this->session["_COMPANY_DETAILS_"]['1746']["REQUEST_CSV"] == 1) AND ($this->siteID == 1746) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['11647']["REQUEST_CSV"] == 1) AND ($this->siteID == 11647) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")))
       //if((($this->session["_COMPANY_DETAILS_"]['1746']["REQUEST_CSV"] == 1) AND ($this->siteID == 1746)) || (($this->session["_COMPANY_DETAILS_"]['11647']["REQUEST_CSV"] == 1) AND ($this->siteID == 11647)))
      {
          $consentEmail = 'No';
          if(isset($this->session['consent_statements_values']['companyMarketing']['Email']))
          {
              if($this->session['consent_statements_values']['companyMarketing']['Email'] == 1)
                  $consentEmail = 'Yes';
          }

          $consentSMS = 'No';
          if(isset($this->session['consent_statements_values']['companyMarketing']['SMS']))
          {
              if($this->session['consent_statements_values']['companyMarketing']['SMS'] == 1)
                  $consentSMS = 'Yes';
          }

         $contentHeader = "QUOTE DATE, QUOTE TIME, QUOTE REFRENCE, DO YOU KNOW THE REGISTRATION NUMBER, REGISTRATION NUMBER, VEHICLE MAKE, VEHICLE MODEL, YEAR OF MANUFACTURE, ENGINE SIZE AND TYPE, ESTIMATED TAXI VALUE, TAXI TYPE, MAIN TAXI USE, ARE YOU AN UBER DRIVER, MAXIMUM NUMBER OF PASSENGERS, TAXI LICENSING AUTHORITY, TYPE OF COVER, TAXI NCB, PRIVATE CAR NCB, FULL UK DRIVING LICENSE, TAXI BADGE, TAXI DRIVERS, CLAIMS LAST 5 YEARS, MOTORING CONVICTIONS LAST 5 YEARS, FIRST NAME/SURNAME, PREFERRED TELEPHONE NUMBER , ADDITIONAL TELEPHONE NUMBER (OPTIONAL), EMAIL ADDRESS, ADDRESS LINE 1 , TOWN/CITY , COUNTY , POSTCODE , DATE OF BIRTH , DATE OF INSURANCE START, TELEPHONE CONTACT , EMAIL CONTACT , SMS CONTACT,\n";

         $contentRows = "";
         $contentRows = $doq.",".date("H:i:s").",".$quoteRef.",".$registrationNumberQuestion.",".$registrationNumber.",".$taxiMake.",".$taxiModel.",".$yearOfManufacture.",".$engineSize.",".$estimatedValue.",".$taxiType.",".$taxiUse.",".$uberDriver.",".$taxiCapacity.",".$platingAuthority.",".$typeOfCover.",".$taxiNcb.",".$privateCarNcb.",".$fullUkLicence.",".$taxiBadge.",".$taxiDrivers.","."\"".$claims5Years."\"".",".$convictions5Years.",".$firstName." ".$lastName.",".$daytimeTelephone.",".$mobileTelephone.",".$email.",".$addressLine1.",".$town.",".$county.",".$postcode.",".$dob.",".$dis.",Yes,".$consentEmail.",".$consentSMS;
   

         $password   = "QuotezoneSP1";
         $nameOfCSV  = "Quotezone_Taxi_Lead_".$quoteRef."_".$this->siteID;
         $csvContent = "";
         $csvContent .= $contentHeader;
         $csvContent .= $contentRows;
         

         $csvSubjectDetails = array(
                                    "1746"=>"Sureplan (Private Hire) CSV Method",
                                    "11647"=>"Sureplan (Public Hire) CSV Method",
                                    );
        
         $csvEmailsArr = array(
                              $csvSubjectDetails[$this->siteID]  => array(
                                                            "quotezone@sureplaninsurance.co.uk",
                                                            "quotes@spibrokers.co.uk",
                                                            "eb2-technical@seopa.com",
                                                            "leads@seopa.com",
                                                                  ),
                                                            );   

     
         //send CSV and get csvOutboundDetailsArr
         $csvOutboundDetailsArr = array();
         $csvOutboundDetailsArr = SendProtectedCSV($outboundObj,$mailObj,$subject,$mailFrom,$csvEmailsArr,$nameOfCSV,$csvContent,$password);   
         $outboundObj->UpdateOutboundField($this->outbounds['CSV'], 'sent_status', 1);
         $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 1);
         $outboundObj->UpdateOutboundField($this->outbounds['CSV'], 'extra_params', $csvOutboundDetailsArr["emailId"]);
         $this->DoOutboundFiles("csv", $csvOutboundDetailsArr["sentContent"]); 
      }

      //CSV Outbound for Insurance Choice
      if(($this->session["_COMPANY_DETAILS_"]['1798']["REQUEST_CSV"] == 1) AND ($this->siteID == 1798) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      // if(($this->session["_COMPANY_DETAILS_"]['1798']["REQUEST_CSV"] == 1) AND ($this->siteID == 1798))
      {
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= "From: <campervan@quotezone.co.uk>\r\n";
            $headers .= "X-Sender: <campervan@quotezone.co.uk>\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!
            $headers .= "Return-Path: <campervan@quotezone.co.uk>\r\n";    


            $doYouKnowTheRegistrationNumber  = $this->session['_YourDetails_']['vehicle_registration_number'];
            $registrationNumber              = $this->session['_YourDetails_']['vehicle_registration_number'];
            $taxiMake                        = $this->session['_YourDetails_']['taxi_make'];
            $taxiModel                       = $this->session['_YourDetails_']['taxi_model'];
            $yearOfManufacture               = $this->session['_YourDetails_']['year_of_manufacture'];
            $vehicleMileage                  = $_YourDetails['vehicle_mileage'][$this->session['_YourDetails_']['vehicle_mileage']];
            $vehicleMileage                  = str_replace(","," ",$vehicleMileage);
            $estimatedTaxiValue              = $this->session['_YourDetails_']['estimated_value'];
            $taxiType                        = $_YourDetails["taxi_type"][$this->session['_YourDetails_']['taxi_type']];
            $taxiUse                         = $_YourDetails["taxi_used_for"][$this->session['_YourDetails_']['taxi_used_for']];
            $maximumNumberOfPassengers       = $_YourDetails["taxi_capacity"][$this->session['_YourDetails_']['taxi_capacity']];
            $taxiLicensingAuthority          = str_replace(","," ",$this->session['_YourDetails_']['plating_authority']);
            $typeOfCover                     = $_YourDetails["type_of_cover"][$this->session['_YourDetails_']['type_of_cover']];
            $taxiNoClaimsBonus               = $_YourDetails["taxi_ncb"][$this->session['_YourDetails_']['taxi_ncb']];
            $privateCarNoClaimsBonus         = $_YourDetails["private_car_ncb"][$this->session['_YourDetails_']['private_car_ncb']];
            $fullUkLicence                   = $_YourDetails["period_of_licence"][$this->session['_YourDetails_']['period_of_licence']];
            $taxiBadge                       = $_YourDetails["taxi_badge"][$this->session['_YourDetails_']['taxi_badge']]; 
            $taxiDriver                      = $_YourDetails["taxi_driver"][$this->session['_YourDetails_']['taxi_driver']];
            $claimsLast5Years                = $_YourDetails["claims_5_years"][$this->session['_YourDetails_']['claims_5_years']];
            $claimsLast5Years                = str_replace(","," ",$claimsLast5Years);
            $motoringConvictionsLast5Years   = str_replace(","," ",$_YourDetails["convictions_5_years"][$this->session['_YourDetails_']['convictions_5_years']]);
            $title                           = $this->session['_YourDetails_']['title'];  
            $firstName                       = $this->session['_YourDetails_']['first_name'];
            $surname                         = $this->session['_YourDetails_']['surname'];  
            $dateOfBirth                     = $this->session['_YourDetails_']['date_of_birth'];  
            $preferredTelephoneNumber        = $this->session['_YourDetails_']['daytime_telephone']; 
            $additionalTelephoneNumber       = $this->session['_YourDetails_']['mobile_telephone'];
            $emailAddress                    = $this->session['_YourDetails_']['email_address'];             
            $postcode                        = $this->session['_YourDetails_']['postcode'];  
            $houseNumberName                 = $this->session['_YourDetails_']['house_number_or_name'];  
            $addressLine                     = $this->session['_YourDetails_']['address_line1'];  
            $streetName                      = $this->session['_YourDetails_']['address_line2'];  
            $townCity                        = $this->session['_YourDetails_']['address_line3'];  
            $county                          = $this->session['_YourDetails_']['address_line4'];  
            $whenWouldYouLikeCoverToBegin    = $this->session['_YourDetails_']['start_date']; 
            $quoteRef                        = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];


            $contentHeader = "line of business,source,Do you know the registration number,Registration number,Taxi Make,Taxi Model,Year Of Manufacture,Vehicle Mileage,Estimated taxi value,Taxi type,Taxi use,Maximum number of passengers,Taxi licensing authority,Type of cover,Taxi no claims bonus,Private car no claims bonus,Full UK licence,Taxi badge,Taxi driver,Claims last 5 years,Motoring convictions last 5 years,Title,First name,Surname,Date of birth,Preferred telephone number,Additional telephone number,Email address,Postcode,House Number/Name,Address Line 1,Street Name,Town/City,County,When would you like cover to begin,Quote Ref\n";

            $contentRows = "";
            $contentRows = "Taxi,Quotezone,".$doYouKnowTheRegistrationNumber.",".$registrationNumber.",".$taxiMake.",".$taxiModel.",".$yearOfManufacture.",".$vehicleMileage.",".$estimatedTaxiValue.",".$taxiType.",".$taxiUse.",".$maximumNumberOfPassengers.",".$taxiLicensingAuthority.",".$typeOfCover.",".$taxiNoClaimsBonus.",".$privateCarNoClaimsBonus.",".$fullUkLicence.",".$taxiBadge.",".$taxiDriver.",".$claimsLast5Years.",".$motoringConvictionsLast5Years.",".$title.",".$firstName.",".$surname.",".$dateOfBirth.",".$preferredTelephoneNumber.",".$additionalTelephoneNumber.",".$emailAddress.",".$postcode.",".$houseNumberName.",".$addressLine.",".$streetName.",".$townCity.",".$county.",".$whenWouldYouLikeCoverToBegin.",".$quoteRef;


            $today       = date("Y-m-d-H-i-s");
            $csvFile     = "taxi_ph_".$firstName."_".$surname."_".date('dmY')."_".date('his').".csv";
            $csvFileName = "/tmp/$csvFile";

            $contentOfFile  = "";
            $contentOfFile .= $contentHeader;
            $contentOfFile .= $contentRows;

            $fh = fopen($csvFileName,"w+");
            fwrite($fh,$contentOfFile);
            fclose($fh);

            $ftpServer   = "ftp.ezeecom.co.uk";
            $ftpUserName = "seopa2msg";
            $ftpUserPass = "^Ra15S\&MsG";

            $file = $csvFileName;

            $remoteFile = $csvFile;

            $ftpError = "";
            $result = "";

            exec("lftp sftp://".$ftpUserName.":".$ftpUserPass."@".$ftpServer."  -e \"set sftp:connect-program 'ssh -o PubkeyAuthentication=false'; cd choice; put ".$file."; bye\" >/dev/null 2>&1");

            @unlink($csvFileName);

            //seopa
            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." - Taxi - Insurance Choice (Public Hire) CSV";
            $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
            $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers,$this->session,"CSV",$this->siteID);

            //eb2
            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." - Taxi - Insurance Choice (Public Hire) CSV";
            $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
            $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers,$this->session,"CSV",$this->siteID);

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 1);

            $this->DoOutboundFiles("csv", $contentOfFile);

      }//end //CSV Outbound for Insurance Choice
//CSV Outbound for Insurance Choice
      if(($this->session["_COMPANY_DETAILS_"]['1888']["REQUEST_CSV"] == 1) AND ($this->siteID == 1888) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      // if(($this->session["_COMPANY_DETAILS_"]['1888']["REQUEST_CSV"] == 1) AND ($this->siteID == 1888))
      {
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= "From: <campervan@quotezone.co.uk>\r\n";
            $headers .= "X-Sender: <campervan@quotezone.co.uk>\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!
            $headers .= "Return-Path: <campervan@quotezone.co.uk>\r\n";    


            $doYouKnowTheRegistrationNumber  = 
            $registrationNumber              = $this->session['_YourDetails_']['vehicle_registration_number'];
            $taxiMake                        = $this->session['_YourDetails_']['taxi_make'];
            $taxiModel                       = $this->session['_YourDetails_']['taxi_model'];
            $yearOfManufacture               = $this->session['_YourDetails_']['year_of_manufacture'];
            $vehicleMileage                  = $_YourDetails['vehicle_mileage'][$this->session['_YourDetails_']['vehicle_mileage']];
            $vehicleMileage                  = str_replace(","," ",$vehicleMileage);
            $estimatedTaxiValue              = $this->session['_YourDetails_']['estimated_value'];
            $taxiType                        = $_YourDetails["taxi_type"][$this->session['_YourDetails_']['taxi_type']];
            $taxiUse                         = $_YourDetails["taxi_used_for"][$this->session['_YourDetails_']['taxi_used_for']];
            $maximumNumberOfPassengers       = $_YourDetails["taxi_capacity"][$this->session['_YourDetails_']['taxi_capacity']];
            $taxiLicensingAuthority          = str_replace(","," ",$this->session['_YourDetails_']['plating_authority']);
            $typeOfCover                     = $_YourDetails["type_of_cover"][$this->session['_YourDetails_']['type_of_cover']];
            $taxiNoClaimsBonus               = $_YourDetails["taxi_ncb"][$this->session['_YourDetails_']['taxi_ncb']];
            $privateCarNoClaimsBonus         = $_YourDetails["private_car_ncb"][$this->session['_YourDetails_']['private_car_ncb']];
            $fullUkLicence                   = $_YourDetails["period_of_licence"][$this->session['_YourDetails_']['period_of_licence']];
            $taxiBadge                       = $_YourDetails["taxi_badge"][$this->session['_YourDetails_']['taxi_badge']]; 
            $taxiDriver                      = $_YourDetails["taxi_driver"][$this->session['_YourDetails_']['taxi_driver']];
            $claimsLast5Years                = $_YourDetails["claims_5_years"][$this->session['_YourDetails_']['claims_5_years']];
            $claimsLast5Years                = str_replace(","," ",$claimsLast5Years);
            $motoringConvictionsLast5Years   = str_replace(","," ",$_YourDetails["convictions_5_years"][$this->session['_YourDetails_']['convictions_5_years']]);
            $title                           = $this->session['_YourDetails_']['title'];  
            $firstName                       = $this->session['_YourDetails_']['first_name'];
            $surname                         = $this->session['_YourDetails_']['surname'];  
            $dateOfBirth                     = $this->session['_YourDetails_']['date_of_birth'];  
            $preferredTelephoneNumber        = $this->session['_YourDetails_']['daytime_telephone']; 
            $additionalTelephoneNumber       = $this->session['_YourDetails_']['mobile_telephone'];
            $emailAddress                    = $this->session['_YourDetails_']['email_address'];             
            $postcode                        = $this->session['_YourDetails_']['postcode'];  
            $houseNumberName                 = $this->session['_YourDetails_']['house_number_or_name'];  
            $addressLine                     = $this->session['_YourDetails_']['address_line1'];  
            $streetName                      = $this->session['_YourDetails_']['address_line2'];  
            $townCity                        = $this->session['_YourDetails_']['address_line3'];  
            $county                          = $this->session['_YourDetails_']['address_line4'];  
            $whenWouldYouLikeCoverToBegin    = $this->session['_YourDetails_']['start_date']; 
            $quoteRef                        = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];


            $contentHeader = "line of business,source,Do you know the registration number,Registration number,Taxi Make,Taxi Model,Year Of Manufacture,Vehicle Mileage,Estimated taxi value,Taxi type,Taxi use,Maximum number of passengers,Taxi licensing authority,Type of cover,Taxi no claims bonus,Private car no claims bonus,Full UK licence,Taxi badge,Taxi driver,Claims last 5 years,Motoring convictions last 5 years,Title,First name,Surname,Date of birth,Preferred telephone number,Additional telephone number,Email address,Postcode,House Number/Name,Address Line 1,Street Name,Town/City,County,When would you like cover to begin,Quote Ref\n";

            $contentRows = "";
            $contentRows = "Taxi,Quotezone,".$doYouKnowTheRegistrationNumber.",".$registrationNumber.",".$taxiMake.",".$taxiModel.",".$yearOfManufacture.",".$vehicleMileage.",".$estimatedTaxiValue.",".$taxiType.",".$taxiUse.",".$maximumNumberOfPassengers.",".$taxiLicensingAuthority.",".$typeOfCover.",".$taxiNoClaimsBonus.",".$privateCarNoClaimsBonus.",".$fullUkLicence.",".$taxiBadge.",".$taxiDriver.",".$claimsLast5Years.",".$motoringConvictionsLast5Years.",".$title.",".$firstName.",".$surname.",".$dateOfBirth.",".$preferredTelephoneNumber.",".$additionalTelephoneNumber.",".$emailAddress.",".$postcode.",".$houseNumberName.",".$addressLine.",".$streetName.",".$townCity.",".$county.",".$whenWouldYouLikeCoverToBegin.",".$quoteRef;


            $today       = date("Y-m-d-H-i-s");
            $csvFile     = "taxi_2_".$firstName."_".$surname."_".date('dmY')."_".date('his').".csv";
            $csvFileName = "/tmp/$csvFile";

            $contentOfFile  = "";
            $contentOfFile .= $contentHeader;
            $contentOfFile .= $contentRows;

            $fh = fopen($csvFileName,"w+");
            fwrite($fh,$contentOfFile);
            fclose($fh);

            $ftpServer   = "ftp.ezeecom.co.uk";
            $ftpUserName = "seopa2msg";
            $ftpUserPass = "^Ra15S\&MsG";

            $file = $csvFileName;

            $remoteFile = $csvFile;

            $ftpError = "";
            $result = "";

            exec("lftp sftp://".$ftpUserName.":".$ftpUserPass."@".$ftpServer."  -e \"set sftp:connect-program 'ssh -o PubkeyAuthentication=false'; cd choice; put ".$file."; bye\" >/dev/null 2>&1");

            @unlink($csvFileName);

            //seopa
            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." - Taxi - Insurance Choice 2 CSV";
            $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
            $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers,$this->session,"CSV",$this->siteID);

            //eb2
            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." - Taxi - Insurance Choice 2 CSV";
            $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
            $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers,$this->session,"CSV",$this->siteID);

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 1);

            $this->DoOutboundFiles("csv", $contentOfFile);

      }//end //CSV Outbound for Insurance Choice
//CSV Outbound for Insurance Choice
      if(($this->session["_COMPANY_DETAILS_"]['9602']["REQUEST_CSV"] == 1) AND ($this->siteID == 9602) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      // if(($this->session["_COMPANY_DETAILS_"]['9602']["REQUEST_CSV"] == 1) AND ($this->siteID == 9602))
      {
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= "From: <campervan@quotezone.co.uk>\r\n";
            $headers .= "X-Sender: <campervan@quotezone.co.uk>\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!
            $headers .= "Return-Path: <campervan@quotezone.co.uk>\r\n";    


            $doYouKnowTheRegistrationNumber  = 
            $registrationNumber              = $this->session['_YourDetails_']['vehicle_registration_number'];
            $taxiMake                        = $this->session['_YourDetails_']['taxi_make'];
            $taxiModel                       = $this->session['_YourDetails_']['taxi_model'];
            $yearOfManufacture               = $this->session['_YourDetails_']['year_of_manufacture'];
            $vehicleMileage                  = $_YourDetails['vehicle_mileage'][$this->session['_YourDetails_']['vehicle_mileage']];
            $vehicleMileage                  = str_replace(","," ",$vehicleMileage);
            $estimatedTaxiValue              = $this->session['_YourDetails_']['estimated_value'];
            $taxiType                        = $_YourDetails["taxi_type"][$this->session['_YourDetails_']['taxi_type']];
            $taxiUse                         = $_YourDetails["taxi_used_for"][$this->session['_YourDetails_']['taxi_used_for']];
            $maximumNumberOfPassengers       = $_YourDetails["taxi_capacity"][$this->session['_YourDetails_']['taxi_capacity']];
            $taxiLicensingAuthority          = str_replace(","," ",$this->session['_YourDetails_']['plating_authority']);
            $typeOfCover                     = $_YourDetails["type_of_cover"][$this->session['_YourDetails_']['type_of_cover']];
            $taxiNoClaimsBonus               = $_YourDetails["taxi_ncb"][$this->session['_YourDetails_']['taxi_ncb']];
            $privateCarNoClaimsBonus         = $_YourDetails["private_car_ncb"][$this->session['_YourDetails_']['private_car_ncb']];
            $fullUkLicence                   = $_YourDetails["period_of_licence"][$this->session['_YourDetails_']['period_of_licence']];
            $taxiBadge                       = $_YourDetails["taxi_badge"][$this->session['_YourDetails_']['taxi_badge']]; 
            $taxiDriver                      = $_YourDetails["taxi_driver"][$this->session['_YourDetails_']['taxi_driver']];
            $claimsLast5Years                = $_YourDetails["claims_5_years"][$this->session['_YourDetails_']['claims_5_years']];
            $claimsLast5Years                = str_replace(","," ",$claimsLast5Years);
            $motoringConvictionsLast5Years   = str_replace(","," ",$_YourDetails["convictions_5_years"][$this->session['_YourDetails_']['convictions_5_years']]);
            $title                           = $this->session['_YourDetails_']['title'];  
            $firstName                       = $this->session['_YourDetails_']['first_name'];
            $surname                         = $this->session['_YourDetails_']['surname'];  
            $dateOfBirth                     = $this->session['_YourDetails_']['date_of_birth'];  
            $preferredTelephoneNumber        = $this->session['_YourDetails_']['daytime_telephone']; 
            $additionalTelephoneNumber       = $this->session['_YourDetails_']['mobile_telephone'];
            $emailAddress                    = $this->session['_YourDetails_']['email_address'];             
            $postcode                        = $this->session['_YourDetails_']['postcode'];  
            $houseNumberName                 = $this->session['_YourDetails_']['house_number_or_name'];  
            $addressLine                     = $this->session['_YourDetails_']['address_line1'];  
            $streetName                      = $this->session['_YourDetails_']['address_line2'];  
            $townCity                        = $this->session['_YourDetails_']['address_line3'];  
            $county                          = $this->session['_YourDetails_']['address_line4'];  
            $whenWouldYouLikeCoverToBegin    = $this->session['_YourDetails_']['start_date']; 
            $quoteRef                        = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];


            $contentHeader = "line of business,source,Do you know the registration number,Registration number,Taxi Make,Taxi Model,Year Of Manufacture,Vehicle Mileage,Estimated taxi value,Taxi type,Taxi use,Maximum number of passengers,Taxi licensing authority,Type of cover,Taxi no claims bonus,Private car no claims bonus,Full UK licence,Taxi badge,Taxi driver,Claims last 5 years,Motoring convictions last 5 years,Title,First name,Surname,Date of birth,Preferred telephone number,Additional telephone number,Email address,Postcode,House Number/Name,Address Line 1,Street Name,Town/City,County,When would you like cover to begin,Quote Ref\n";

            $contentRows = "";
            $contentRows = "Taxi,Quotezone,".$doYouKnowTheRegistrationNumber.",".$registrationNumber.",".$taxiMake.",".$taxiModel.",".$yearOfManufacture.",".$vehicleMileage.",".$estimatedTaxiValue.",".$taxiType.",".$taxiUse.",".$maximumNumberOfPassengers.",".$taxiLicensingAuthority.",".$typeOfCover.",".$taxiNoClaimsBonus.",".$privateCarNoClaimsBonus.",".$fullUkLicence.",".$taxiBadge.",".$taxiDriver.",".$claimsLast5Years.",".$motoringConvictionsLast5Years.",".$title.",".$firstName.",".$surname.",".$dateOfBirth.",".$preferredTelephoneNumber.",".$additionalTelephoneNumber.",".$emailAddress.",".$postcode.",".$houseNumberName.",".$addressLine.",".$streetName.",".$townCity.",".$county.",".$whenWouldYouLikeCoverToBegin.",".$quoteRef;


            $today       = date("Y-m-d-H-i-s");
            $csvFile     = "taxi_ph_ooh_".$firstName."_".$surname."_".date('dmY')."_".date('his').".csv";
            $csvFileName = "/tmp/$csvFile";

            $contentOfFile  = "";
            $contentOfFile .= $contentHeader;
            $contentOfFile .= $contentRows;

            $fh = fopen($csvFileName,"w+");
            fwrite($fh,$contentOfFile);
            fclose($fh);

            $ftpServer   = "ftp.ezeecom.co.uk";
            $ftpUserName = "seopa2msg";
            $ftpUserPass = "^Ra15S\&MsG";

            $file = $csvFileName;

            $remoteFile = $csvFile;

            $ftpError = "";
            $result = "";

            exec("lftp sftp://".$ftpUserName.":".$ftpUserPass."@".$ftpServer."  -e \"set sftp:connect-program 'ssh -o PubkeyAuthentication=false'; cd choice; put ".$file."; bye\" >/dev/null 2>&1");

            @unlink($csvFileName);

            //seopa
            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." - Taxi - Insurance Choice (Public Hire OOH) CSV";
            $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
            $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers,$this->session,"CSV",$this->siteID);

            //eb2
            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." - Taxi - Insurance Choice (Public Hire OOH) CSV";
            $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
            $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers,$this->session,"CSV",$this->siteID);

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 1);

            $this->DoOutboundFiles("csv", $contentOfFile);

      }//end //CSV Outbound for Insurance Choice

      if(($this->session["_COMPANY_DETAILS_"]['3466']["REQUEST_CSV"] == 1) AND ($this->siteID == 3466) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      // if(($this->session["_COMPANY_DETAILS_"]['3466']["REQUEST_CSV"] == 1) AND ($this->siteID == 3466) )
      {
         //error_reporting(E_ALL);
         
         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/plain; charset=iso-8859-1'."\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         
         $contentHeader = "Quote Date,Quote Time,Taxi Used For,Uber Driver,Taxi Type,Type Of Cover,Taxi Make,Taxi Model,Year Of Manufacture,Vehicle Mileage,Estimated Value,Taxi Capacity,Taxi No Claims Bonus,Private Car No Claims Bonus,Taxi Plating Authority,Taxi Registration,Full UK Licence,Taxi Badge,Taxi Driver(s),Claims Last 5 Years,Convictions Last 5 Years,Title,First Name,Surname,Telephone,Mobile Phone,Best Time To Call,Email Address,Postcode,House Number/Name,Address Line 1,Street Name,Town/City,County,Date Of Birth,Date Of Insurance Start,Quote Reference,Telephone Consent,Email Consent,SMS Consent,\n";

                       
         $quoteDate                 = $this->session['_YourDetails_']['start_date'];  
         $quoteTime                 = date("H:i:s"); 
         $taxiUsedFor               = $_YourDetails["taxi_used_for"][$this->session['_YourDetails_']['taxi_used_for']];
         $uberDriver                = $_YourDetails["uber_driver"][$this->session['_YourDetails_']['uber_driver']];
         $taxiType                  = $_YourDetails["taxi_type"][$this->session['_YourDetails_']['taxi_type']];
         $typeOfCover               = $_YourDetails["type_of_cover"][$this->session['_YourDetails_']['type_of_cover']];
         $taxiMake                  = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel                 = $this->session['_YourDetails_']['taxi_model'];
         $yearOfManufacture         = $this->session['_YourDetails_']['year_of_manufacture'];
         $vehicleMileage            = $_YourDetails['vehicle_mileage'][$this->session['_YourDetails_']['vehicle_mileage']];
         $vehicleMileage            = str_replace(",","",$vehicleMileage);
            $estimatedValue            = $this->session['_YourDetails_']['estimated_value'];
         $taxiCapacity              = $_YourDetails["taxi_capacity"][$this->session['_YourDetails_']['taxi_capacity']];
         $taxiNoClaimsBonus         = $_YourDetails["taxi_ncb"][$this->session['_YourDetails_']['taxi_ncb']];
         $privateCarNoClaimsBonus   = $_YourDetails["private_car_ncb"][$this->session['_YourDetails_']['private_car_ncb']];
         $taxiPlatingAuthority      = $this->session['_YourDetails_']['plating_authority'];
         $taxiRegistration          = $this->session['_YourDetails_']['vehicle_registration_number'];
         $fullUKLicence             = $_YourDetails["period_of_licence"][$this->session['_YourDetails_']['period_of_licence']];
         $taxiBadge                 = $_YourDetails["taxi_badge"][$this->session['_YourDetails_']['taxi_badge']]; 
         $taxiDrivers               = $_YourDetails["taxi_driver"][$this->session['_YourDetails_']['taxi_driver']];
         $claimsLast5Years          = $_YourDetails["claims_5_years"][$this->session['_YourDetails_']['claims_5_years']];
            $claimsLast5Years       = str_replace(",","",$claimsLast5Years);
         $convictionsLast5Years     = $_YourDetails["convictions_5_years"][$this->session['_YourDetails_']['convictions_5_years']];
         $title                     = $this->session['_YourDetails_']['title'];  
         $firstName                 = $this->session['_YourDetails_']['first_name'];
         $surname                   = $this->session['_YourDetails_']['surname'];  
         $telephoneNumber           = $this->session['_YourDetails_']['daytime_telephone']; 
         $mobilePhone               = $this->session['_YourDetails_']['mobile_telephone'];
         $bestTimeToCall            = $_YourDetails["best_time_call"][$this->session['_YourDetails_']['best_time_call']];
         $email                     = $this->session['_YourDetails_']['email_address'];
         $postcode                  = $this->session['_YourDetails_']['postcode'];  
         $dateOfBirth               = $this->session['_YourDetails_']['date_of_birth'];  
         $dateOfInsuranceStart      = $this->session['_YourDetails_']['start_date'];  
         $quoteRef                  = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];


         $contentRows = "";
         $contentRows = "$quoteDate,$quoteTime,$taxiUsedFor,$uberDriver,$taxiType,$typeOfCover,$taxiMake,$taxiModel,$yearOfManufacture,$vehicleMileage,$estimatedValue,$taxiCapacity,$taxiNoClaimsBonus,$privateCarNoClaimsBonus,$taxiPlatingAuthority,$taxiRegistration,$fullUKLicence,$taxiBadge,$taxiDrivers,$claimsLast5Years,$convictionsLast5Years,$title,$firstName,$surname,$telephoneNumber,$mobilePhone,$bestTimeToCall,$email,$postcode,$houseNumberOrName,$addressLine1,$streetName,$town,$county,$dateOfBirth,$dateOfInsuranceStart,$quoteRef,$telephoneConsent,$emailConsent,$smsConsent,\n";

         $today       = date("Y-m-d-H:i:s");
         $csvFile     = "Quotezone-High-Gear-Taxi-".$today.".csv";
         $csvFileName = "/tmp/$csvFile";

         $contentOfFile  = "";
         $contentOfFile .= $contentHeader;
         $contentOfFile .= $contentRows;

         $fh = fopen($csvFileName,"w+");
         fwrite($fh,$contentOfFile);
         fclose($fh);

         $ftpServer   = "ftp.highgear.co.uk";
         $ftpUserName = "quotezone@highgear.co.uk";
         $ftpUserPass = "!VywXB5NXJll";

            //FTP & explicit FTPS port: 21

         $file = $csvFileName;

         $remoteFile = $csvFile;

         $ftpError = "";

         // set up basic connection
         $connId = ftp_connect($ftpServer);
      
         if(! $connId)
            $ftpError .= "<br>ftp_connect(".$ftpServer.") function return false<br>";

         // login with username and password
         $loginResult = ftp_login($connId, $ftpUserName, $ftpUserPass);

         if(! $loginResult)
            $ftpError .= "<br>ftp_login() function return false (".$ftpUserName."@".$ftpUserPass.")<br>";

         // turn passive mode on
         if( ! ftp_pasv($connId, true))
            $ftpError .= "<br>ftp_pasv() function return false <br>";

			
		// Initiate the Upload
		// upload a file
         if (ftp_put($connId, $remoteFile, $file, FTP_BINARY))
         {
            $result = "successfully uploaded $file\n";

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 1);
         }
         else
         {
            $ftpError .= "<br>ftp_put($connId, $remoteFile, $file, FTP_BINARY) function return false <br>";

            $result = "There was a problem while uploading $file\n";

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 2);
         }

         // close the connection
         ftp_close($connId);

         @unlink($csvFileName);
         
         

         //seopa
         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi - High Gear FTP";
         $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
         $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers,$this->session,"CSV",$this->siteID);
         
         //eb2
         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi - High Gear $this->siteID $quoteRef FTP";
         $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
         $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers,$this->session,"CSV",$this->siteID);


         $this->DoOutboundFiles("ftp", $contentOfFile,$result."<br>".$ftpError);
      }
      
      if(($this->session["_COMPANY_DETAILS_"]['10212']["REQUEST_CSV"] == 1) AND ($this->siteID == 10212) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {
         //error_reporting(E_ALL);
         
         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/plain; charset=iso-8859-1'."\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         
         $contentHeader = "Quote Date,Quote Time,Taxi Used For,Uber Driver,Taxi Type,Type Of Cover,Taxi Make,Taxi Model,Year Of Manufacture,Vehicle Mileage,Estimated Value,Taxi Capacity,Taxi No Claims Bonus,Private Car No Claims Bonus,Taxi Plating Authority,Taxi Registration,Full UK Licence,Taxi Badge,Taxi Driver(s),Claims Last 5 Years,Convictions Last 5 Years,Title,First Name,Surname,Telephone,Mobile Phone,Best Time To Call,Email Address,Postcode,House Number/Name,Address Line 1,Street Name,Town/City,County,Date Of Birth,Date Of Insurance Start,Quote Reference,\n";

                       
         $quoteDate                 = $this->session['_YourDetails_']['start_date'];  
         $quoteTime                 = date("H:i:s"); 
         $taxiUsedFor               = $_YourDetails["taxi_used_for"][$this->session['_YourDetails_']['taxi_used_for']];
         $uberDriver                = $_YourDetails["uber_driver"][$this->session['_YourDetails_']['uber_driver']];
         $taxiType                  = $_YourDetails["taxi_type"][$this->session['_YourDetails_']['taxi_type']];
         $typeOfCover               = $_YourDetails["type_of_cover"][$this->session['_YourDetails_']['type_of_cover']];
         $taxiMake                  = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel                 = $this->session['_YourDetails_']['taxi_model'];
         $yearOfManufacture         = $this->session['_YourDetails_']['year_of_manufacture'];
         $vehicleMileage            = $_YourDetails['vehicle_mileage'][$this->session['_YourDetails_']['vehicle_mileage']];
         $vehicleMileage            = str_replace(",","",$vehicleMileage);
         $estimatedValue            = $this->session['_YourDetails_']['estimated_value'];
         $taxiCapacity              = $_YourDetails["taxi_capacity"][$this->session['_YourDetails_']['taxi_capacity']];
         $taxiNoClaimsBonus         = $_YourDetails["taxi_ncb"][$this->session['_YourDetails_']['taxi_ncb']];
         $privateCarNoClaimsBonus   = $_YourDetails["private_car_ncb"][$this->session['_YourDetails_']['private_car_ncb']];
         $taxiPlatingAuthority  = $this->session['_YourDetails_']['plating_authority'];
         $taxiRegistration      = $this->session['_YourDetails_']['vehicle_registration_number'];
         $fullUKLicence         = $_YourDetails["period_of_licence"][$this->session['_YourDetails_']['period_of_licence']];
         $taxiBadge             = $_YourDetails["taxi_badge"][$this->session['_YourDetails_']['taxi_badge']]; 
         $taxiDrivers           = $_YourDetails["taxi_driver"][$this->session['_YourDetails_']['taxi_driver']];
         $claimsLast5Years      = $_YourDetails["claims_5_years"][$this->session['_YourDetails_']['claims_5_years']];
         $claimsLast5Years      = str_replace(",","",$claimsLast5Years);
         $convictionsLast5Years = $_YourDetails["convictions_5_years"][$this->session['_YourDetails_']['convictions_5_years']];
         $title                 = $this->session['_YourDetails_']['title'];  
         $firstName             = $this->session['_YourDetails_']['first_name'];
         $surname               = $this->session['_YourDetails_']['surname'];  
         $telephoneNumber       = $this->session['_YourDetails_']['daytime_telephone']; 
         $mobilePhone           = $this->session['_YourDetails_']['mobile_telephone'];
         $bestTimeToCall        = $_YourDetails["best_time_call"][$this->session['_YourDetails_']['best_time_call']];
         $email                 = $this->session['_YourDetails_']['email_address'];
         $postcode              = $this->session['_YourDetails_']['postcode'];
         $houseNumberOrName     = $newPostcodeDetails['building_name_and_number'];
         $addressLine1          = $newPostcodeDetails['line1'];
         $streetName            = $newPostcodeDetails['road'];
         $town                  = $newPostcodeDetails['post_town'];
         $county                = $newPostcodeDetails['county'];
         $dateOfBirth           = $this->session['_YourDetails_']['date_of_birth'];  
         $dateOfInsuranceStart  = $this->session['_YourDetails_']['start_date'];  
         $quoteRef              = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];


         $contentRows = "";
         $contentRows = "$quoteDate,$quoteTime,$taxiUsedFor,$uberDriver,$taxiType,$typeOfCover,$taxiMake,$taxiModel,$yearOfManufacture,$vehicleMileage,$estimatedValue,$taxiCapacity,$taxiNoClaimsBonus,$privateCarNoClaimsBonus,$taxiPlatingAuthority,$taxiRegistration,$fullUKLicence,$taxiBadge,$taxiDrivers,$claimsLast5Years,$convictionsLast5Years,$title,$firstName,$surname,$telephoneNumber,$mobilePhone,$bestTimeToCall,$email,$postcode,$houseNumberOrName,$addressLine1,$streetName,$town,$county,$dateOfBirth,$dateOfInsuranceStart,$quoteRef,\n";
     
         $today       = date("Y-m-d-H:i:s");
         $csvFile     = "Quotezone-High-Gear-Taxi-6-".$today.".csv";
         $csvFileName = "/tmp/$csvFile";

         $contentOfFile  = "";
         $contentOfFile .= $contentHeader;
         $contentOfFile .= $contentRows;

         $fh = fopen($csvFileName,"w+");
         fwrite($fh,$contentOfFile);
         fclose($fh);

         $ftpServer   = "ftp.highgear.co.uk";
         $ftpUserName = "quotezone@highgear.co.uk";
         $ftpUserPass = "!VywXB5NXJll";

            //FTP & explicit FTPS port: 21

         $file = $csvFileName;

         $remoteFile = $csvFile;

         $ftpError = "";

         // set up basic connection
         $connId = ftp_connect($ftpServer);
      
         if(! $connId)
            $ftpError .= "<br>ftp_connect(".$ftpServer.") function return false<br>";

         // login with username and password
         $loginResult = ftp_login($connId, $ftpUserName, $ftpUserPass);

         if(! $loginResult)
            $ftpError .= "<br>ftp_login() function return false (".$ftpUserName."@".$ftpUserPass.")<br>";

         // turn passive mode on
         if( ! ftp_pasv($connId, true))
            $ftpError .= "<br>ftp_pasv() function return false <br>";

			
		// Initiate the Upload
		// upload a file
         if (ftp_put($connId, $remoteFile, $file, FTP_BINARY))
         {
            $result = "successfully uploaded $file\n";

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 1);
         }
         else
         {
            $ftpError .= "<br>ftp_put($connId, $remoteFile, $file, FTP_BINARY) function return false <br>";

            $result = "There was a problem while uploading $file\n";

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 2);
         }

         // close the connection
         ftp_close($connId);

         @unlink($csvFileName);
         
         

         //seopa
         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi - High Gear 6 CSV";
         $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
         $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers,$this->session,"CSV",$this->siteID);
         
         //eb2
         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi - High Gear 6 CSV";
         $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
         $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers,$this->session,"CSV",$this->siteID);


         $this->DoOutboundFiles("csv", $contentOfFile,$result."<br>".$ftpError);
      }
      
      
      if(($this->session["_COMPANY_DETAILS_"]['10253']["REQUEST_CSV"] == 1) AND ($this->siteID == 10253) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      // if(($this->session["_COMPANY_DETAILS_"]['10253']["REQUEST_CSV"] == 1) AND ($this->siteID == 10253))
      {
         //error_reporting(E_ALL);
         
         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/plain; charset=iso-8859-1'."\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         
         $contentHeader = "Quote Date,Quote Time,Taxi Used For,Uber Driver,Taxi Type,Type Of Cover,Taxi Make,Taxi Model,Year Of Manufacture,Vehicle Mileage,Estimated Value,Taxi Capacity,Taxi No Claims Bonus,Private Car No Claims Bonus,Taxi Plating Authority,Taxi Registration,Full UK Licence,Taxi Badge,Taxi Driver(s),Claims Last 5 Years,Convictions Last 5 Years,Title,First Name,Surname,Telephone,Mobile Phone,Best Time To Call,Email Address,Postcode,House Number/Name,Address Line 1,Street Name,Town/City,County,Date Of Birth,Date Of Insurance Start,Quote Reference,Telephone Consent,Email Consent,SMS Consent,\n";

                       
         $quoteDate                 = $this->session['_YourDetails_']['start_date'];  
         $quoteTime                 = date("H:i:s"); 
         $taxiUsedFor               = $_YourDetails["taxi_used_for"][$this->session['_YourDetails_']['taxi_used_for']];
         $uberDriver                = $_YourDetails["uber_driver"][$this->session['_YourDetails_']['uber_driver']];
         $taxiType                  = $_YourDetails["taxi_type"][$this->session['_YourDetails_']['taxi_type']];
         $typeOfCover               = $_YourDetails["type_of_cover"][$this->session['_YourDetails_']['type_of_cover']];
         $taxiMake                  = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel                 = $this->session['_YourDetails_']['taxi_model'];
         $yearOfManufacture         = $this->session['_YourDetails_']['year_of_manufacture'];
         $vehicleMileage            = $_YourDetails['vehicle_mileage'][$this->session['_YourDetails_']['vehicle_mileage']];
         $vehicleMileage            = str_replace(",","",$vehicleMileage);
         $estimatedValue            = $this->session['_YourDetails_']['estimated_value'];
         $taxiCapacity              = $_YourDetails["taxi_capacity"][$this->session['_YourDetails_']['taxi_capacity']];
         $taxiNoClaimsBonus         = $_YourDetails["taxi_ncb"][$this->session['_YourDetails_']['taxi_ncb']];
         $privateCarNoClaimsBonus   = $_YourDetails["private_car_ncb"][$this->session['_YourDetails_']['private_car_ncb']];
         $taxiPlatingAuthority      = $this->session['_YourDetails_']['plating_authority'];
         $taxiRegistration          = $this->session['_YourDetails_']['vehicle_registration_number'];
         $fullUKLicence             = $_YourDetails["period_of_licence"][$this->session['_YourDetails_']['period_of_licence']];
         $taxiBadge                 = $_YourDetails["taxi_badge"][$this->session['_YourDetails_']['taxi_badge']]; 
         $taxiDrivers               = $_YourDetails["taxi_driver"][$this->session['_YourDetails_']['taxi_driver']];
         $claimsLast5Years          = $_YourDetails["claims_5_years"][$this->session['_YourDetails_']['claims_5_years']];
            $claimsLast5Years       = str_replace(",","",$claimsLast5Years);
         $convictionsLast5Years     = $_YourDetails["convictions_5_years"][$this->session['_YourDetails_']['convictions_5_years']];
         $title                     = $this->session['_YourDetails_']['title'];  
         $firstName                 = $this->session['_YourDetails_']['first_name'];
         $surname                   = $this->session['_YourDetails_']['surname'];  
         $telephoneNumber           = $this->session['_YourDetails_']['daytime_telephone']; 
         $mobilePhone               = $this->session['_YourDetails_']['mobile_telephone'];
         $bestTimeToCall            = $_YourDetails["best_time_call"][$this->session['_YourDetails_']['best_time_call']];
         $email                     = $this->session['_YourDetails_']['email_address'];
         $postcode                  = $this->session['_YourDetails_']['postcode'];  
         $dateOfBirth               = $this->session['_YourDetails_']['date_of_birth'];  
         $dateOfInsuranceStart      = $this->session['_YourDetails_']['start_date'];  
         $quoteRef                  = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];


         $contentRows = "";
         $contentRows = "$quoteDate,$quoteTime,$taxiUsedFor,$uberDriver,$taxiType,$typeOfCover,$taxiMake,$taxiModel,$yearOfManufacture, $vehicleMileage,$estimatedValue,$taxiCapacity,$taxiNoClaimsBonus,$privateCarNoClaimsBonus,$taxiPlatingAuthority,$taxiRegistration,$fullUKLicence,$taxiBadge,$taxiDrivers,$claimsLast5Years,$convictionsLast5Years,$title,$firstName,$surname,$telephoneNumber,$mobilePhone,$bestTimeToCall,$email,$postcode,$houseNumberOrName,$addressLine1,$streetName,$town,$county,$dateOfBirth,$dateOfInsuranceStart,$quoteRef,$telephoneConsent,$emailConsent,$smsConsent,\n";
     
         $today       = date("Y-m-d-H:i:s");
         $csvFile     = "Quotezone-High-Gear-Taxi-Weekends-".$today.".csv";
         $csvFileName = "/tmp/$csvFile";

         $contentOfFile  = "";
         $contentOfFile .= $contentHeader;
         $contentOfFile .= $contentRows;

         $fh = fopen($csvFileName,"w+");
         fwrite($fh,$contentOfFile);
         fclose($fh);

         $ftpServer   = "ftp.highgear.co.uk";
         $ftpUserName = "quotezone@highgear.co.uk";
         $ftpUserPass = "!VywXB5NXJll";

            //FTP & explicit FTPS port: 21

         $file = $csvFileName;

         $remoteFile = $csvFile;

         $ftpError = "";

         // set up basic connection
         $connId = ftp_connect($ftpServer);
      
         if(! $connId)
            $ftpError .= "<br>ftp_connect(".$ftpServer.") function return false<br>";

         // login with username and password
         $loginResult = ftp_login($connId, $ftpUserName, $ftpUserPass);

         if(! $loginResult)
            $ftpError .= "<br>ftp_login() function return false (".$ftpUserName."@".$ftpUserPass.")<br>";

         // turn passive mode on
         if( ! ftp_pasv($connId, true))
            $ftpError .= "<br>ftp_pasv() function return false <br>";

			
		// Initiate the Upload
		// upload a file
         if (ftp_put($connId, $remoteFile, $file, FTP_BINARY))
         {
            $result = "successfully uploaded $file\n";

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 1);
         }
         else
         {
            $ftpError .= "<br>ftp_put($connId, $remoteFile, $file, FTP_BINARY) function return false <br>";

            $result = "There was a problem while uploading $file\n";

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 2);
         }

         // close the connection
         ftp_close($connId);

         @unlink($csvFileName);
         
         

         //seopa
         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi - High Gear Weekends CSV";
         $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
         $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers,$this->session,"CSV",$this->siteID);
         
         //eb2
         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi - High Gear Weekends $this->siteID $quoteRef CSV";
         $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
         $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers,$this->session,"CSV",$this->siteID);


         $this->DoOutboundFiles("csv", $contentOfFile,$result."<br>".$ftpError);
      }

   } //end if FTP METHOD 
   
   // TAXI FREEWAY XML
   if($this->outbounds['XML'])
   {
            //We have included all Freeway XML plugins here
            if((($this->session["_COMPANY_DETAILS_"]['1152']["REQUEST_XML"] == 1) AND ($this->siteID == 1152) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) ||         (($this->session["_COMPANY_DETAILS_"]['3147']["REQUEST_XML"] == 1) AND ($this->siteID == 3147) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) ||           (($this->session["_COMPANY_DETAILS_"]['3148']["REQUEST_XML"] == 1) AND ($this->siteID == 3148) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) ||            (($this->session["_COMPANY_DETAILS_"]['10582']["REQUEST_XML"] == 1) AND ($this->siteID == 10582) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) ||            (($this->session["_COMPANY_DETAILS_"]['11990']["REQUEST_XML"] == 1) AND ($this->siteID == 11990) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))  )
               
              // if((($this->session["_COMPANY_DETAILS_"]['1152']["REQUEST_XML"] == 1) AND ($this->siteID == 1152)) ||     (($this->session["_COMPANY_DETAILS_"]['3147']["REQUEST_XML"] == 1) AND ($this->siteID == 3147)) ||        (($this->session["_COMPANY_DETAILS_"]['3148']["REQUEST_XML"] == 1) AND ($this->siteID == 3148)) ||            (($this->session["_COMPANY_DETAILS_"]['10582']["REQUEST_XML"] == 1) AND ($this->siteID == 10582)) ||            (($this->session["_COMPANY_DETAILS_"]['11990']["REQUEST_XML"] == 1) AND ($this->siteID == 11990)))
            {
            //fileds
            //Taxi Registration         <registrationNumber> <vehicle>      Must be upper case and formatted as registration number.
            $registrationNumber = $this->session['_YourDetails_']['vehicle_registration_number'];
            $registrationNumber = strtoupper($registrationNumber);

            //Taxi Make                 <vehicleMake>        <vehicle>      Must be upper case.
            $vehicleMake = $_YourDetails['vehicle_make'][$this->session['_YourDetails_']['vehicle_make']];
            $vehicleMake = strtoupper($vehicleMake);

            //Taxi Model                <vehicleModel>       <vehicle>      Must be upper case.
            $vehicleModel = $this->session['_YourDetails_']['taxi_model'];
            $vehicleModel = strtoupper($vehicleModel);

            //Estimated Taxi Value      <value>              <vehicle>      Money values no decimal places.
            $estimatedValue = $this->session['_YourDetails_']['estimated_value'];

            //Year of Manufacture       <yearOfManufacture>  <vehicle>      Four numeric values only, no spaces.
            $yearOfManufacture = $this->session['_YourDetails_']['year_of_manufacture'];

            //Vehicle Mileage           <annualMileage>      <vehicle>      Should be a numeric value no decimal places.
            $annualMileage      = $_YourDetails["vehicle_mileage"][$this->session['_YourDetails_']['vehicle_mileage']];

            if($annualMileage == "Less than 10000")
            {
               $annualMileage      = str_replace("Less than ","",$annualMileage);
            }
            elseif($annualMileage == "Greater than 150,000")
            {
               $annualMileage      = str_replace("Greater than ","",$annualMileage);
            }
            else
            {
               $annualMileageArray = explode("-",$annualMileage);
               $annualMileage      = $annualMileageArray[1];
            }

            $annualMileage      = str_replace(",","",$annualMileage);
            $annualMileage = "0";

            //Taxi Type                 <bodyTypeID>         <vehicle>      Translate List Values: 'Black Cab' = 22, 'Saloon' = 01 and 'MPV' = 62
            //"1" => "Black Cab",
            //"2" => "Saloon",
            //"3" => "MPV",
            //If <bodyTypeID> = 22 THEN
            //<hackneyCarriage> = true ELSE   <hackneyCarriage> = false

            $hackneyCarriage = 'false';
            $bodyType = $this->session['_YourDetails_']['taxi_type'];
            switch($bodyType)
            {
               case '1':
                  $bodyTypeID = "22";
                  $hackneyCarriage = 'true';
                  break;

               case '2':
                  $bodyTypeID = "01";
                  break;

               case '3':
                  $bodyTypeID = "62";
                  break;

               case '4':
                  $bodyTypeID = "C7";
                  break;

               case '5':
                  $bodyTypeID = "0";
                  break;

            }

            $hackneyCarriage = $bodyTypeID == "22"?$hackneyCarriage = 'true':$hackneyCarriage = 'false';

            // fuelTypeID  Petrol (P) = 002 , Diesel (D) = 001
            $engineSize = $this->session['_YourDetails_']['engine_size'];
            if(preg_match("/P/isU", $engineSize, $matches))
               $fuelTypeID = $matches[0];

            if(preg_match("/D/isU", $engineSize, $matches))
               $fuelTypeID = $matches[0];

            if($fuelTypeID == "P")
               $fuelTypeID = "002";

            if($fuelTypeID == "D")
               $fuelTypeID = "001";

            // gearboxTypeID   Automatic (A) = 001  Manual (M) = 002

            $gearboxTypeID = $this->session['_YourDetails_']['transmission_type'];

            if($gearboxTypeID == "A")
               $gearboxTypeID = "001";

            if($gearboxTypeID == "M")
               $gearboxTypeID = "002";

            // <vehicleMark>Series</vehicleMark>
            $series = $this->session['_YourDetails_']['series'];
            if($registrationNumber == "")
               $series = "";

            //<vehicleCC>Engine_CC="2499"</vehicleCC>
            $engineSize = $this->session['_YourDetails_']['engine_size'];
            if(preg_match("/^([0-9]+)/", $engineSize, $matches))
               $engineCC = $matches[1];


            //Taxi Use                  <vehicleUseID>       <insuredParty> Translate List Values: 'Private Hire' = 15 and 'Public Hire' = 17
            //"1" => "Private Hire",
            //"2" => "Public Hire",
            //Apply Logic: If <vehicleUseID> = 15 THEN
            //<qualificationID> = 614 ELSE
            //<qualificationID> = 613

            $qualificationID = "";
            $taxiUse = $this->session['_YourDetails_']['taxi_used_for'];
            switch($taxiUse)
            {
               case '1':
                  $taxiUseID       = "15";
                  $qualificationID =  "614";
                  //$qualificationID =  "614";
                  break;

               case '2':
                  $taxiUseID = "17";
                  $qualificationID =  "613";
                  //$qualificationID =  "613";
                  break;
              case '3':
                  $taxiUseID = "15";
                  $qualificationID =  "613";
                  break;
            }

            $qualificationID = $taxiUseID=="15"?"614":"613";

            //Max. Number of Passengers <numberOfSeats>      <vehicle>      Numeric fields only no decimal places.
            $numberOfSeats = $this->session['_YourDetails_']['taxi_capacity'];

            //Taxi Plating Authority   <taxiAreaID>          <vehicle>       Awaiting Mike

            //Type of Cover            <coverRequiredID>     <cover>         Translate List Values:
            //Fully comprehensive = 01,
            //Third Party Fire And Theft = 02
            //Third Party = 03
            //"1" => "Fully comprehensive",
            //"2" => "Third party fire and theft",
            //"3" => "Third party",

            $typeOfCover = $this->session['_YourDetails_']['type_of_cover'];

            switch($typeOfCover)
            {
               case '1':
                  $coverRequiredID = "01";
                  break;

               case '2':
                  $coverRequiredID = "02";
                  break;

               case '3':
                  $coverRequiredID = "03";
                  break;
            }


            //Taxi No Claim Bonus      <ncdYears>            <ncd>           Numeric values only. Translate List Value: 'No NCB' = 0 and '5 Years +' = 5.
            $ncdYears = $this->session['_YourDetails_']['taxi_ncb'];

            //Full UK Licence          <dateLicenceObtained> <insuredParty>  See Below Full UK Logic, this should be returned as a date/time.1996-03-18T00:00:00
//          "1"  => "Under 6 Months",
//          "2"  => "6 Months To 1 Year",
//          "3"  => "1 - 2 Years",
//          "4"  => "2 - 3 Years",
//          "5"  => "3 - 4 Years",
//          "6"  => "4 - 5 Years",
//          "7"  => "5 - 6 Years",
//          "8"  => "6 - 7 Years",
//          "9"  => "7 - 8 Years",
//          "10" => "8 - 9 Years",
//          "11" => "9 - 10 Years",
//          "12" => "Over 10 Years",

            $fullUkLicObtained = $this->session['_YourDetails_']['period_of_licence'];

            switch($fullUkLicObtained)
            {

               case '1':
                  $licDate = date("Y-m-d",mktime("0","0","0",date("m")-5,date("d"),date("Y")));
                  break;

               case '2':
                  $licDate = date("Y-m-d",mktime("0","0","0",date("m")-11,date("d"),date("Y")));
                  break;

               case '3':
                  $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-1));
                  break;

               case '4':
                  $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-2));
                  break;

               case '5':
                  $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-3));
                  break;

               case '6':
                  $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-4));
                  break;

               case '7':
                  $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-5));
                  break;

               case '8':
                  $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-6));
                  break;

               case '9':
                  $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-7));
                  break;

               case '10':
                  $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-8));
                  break;

               case '11':
                  $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-9));
                  break;

               case '12':
                  $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-10));
                  break;
            }

            $licDateTime = $licDate."T00:00:00";

            //NOT FOUND IN THE EXAMPLE XML FILE!!!
            //Taxi badge               <datePassed>          <qualification> See Below Taxi Badge Logic, this should be returned as a date/time
            $taxiBadgeDate = $this->session['_YourDetails_']['taxi_badge'];

            switch($taxiBadgeDate)
            {
               case '1':
                  $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")));
                  break;

               case '2':
                  $badgeDate = date("Y-m-d",mktime("0","0","0",date("m")-5,date("d"),date("Y")));
                  break;

               case '3':
                  $badgeDate = date("Y-m-d",mktime("0","0","0",date("m")-11,date("d"),date("Y")));
                  break;

               case '4':
                  $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-1));
                  break;

               case '5':
                  $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-2));
                  break;

               case '6':
                  $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-3));
                  break;

               case '7':
                  $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-4));
                  break;

               case '8':
                  $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-5));
                  break;

               case '9':
                  $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-6));
                  break;

               case '10':
                  $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-7));
                  break;

               case '11':
                  $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-8));
                  break;

               case '12':
                  $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-9));
                  break;

               case '13':
                  $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-10));
                  break;
            }

            $datePassed = $badgeDate."T00:00:00";

            //Claims Last 5 years                            <claim>         Quotezone do not capture claims information

            //Convictions Last 5 years                       <conviction>    Quotezone do not capture conviction information

            //Title                    <titleID>         <insuredParty>          Translate List Values: 'Mr' = 003, 'Mrs' = 004, 'Ms' = 005 and 'Miss' = 002
            $title = $this->session['_YourDetails_']['title'];

            switch($title)
            {
               case 'Mr':
                  $titleID = "003";
                  $sex     = "Male";
                  break;

               case 'Mrs':
                  $titleID = "004";
                  $sex     = "Female";
                  break;

               case 'Ms':
                  $titleID = "005";
                  $sex     = "Female";
                  break;

               case 'Miss':
                  $titleID = "002";
                  $sex     = "Female";
                  break;
            }

            //First Name(s)            <forename>        <insuredParty>          Non numeric field
            $firstName = $this->session['_YourDetails_']['first_name'];

            //Surname                  <surname>         <insuredParty>          Non numeric field
            $surname = $this->session['_YourDetails_']['surname'];

            //Date Of Birth            <dob>             <insuredParty>          Date/Time field
            $dob = $this->session['_YourDetails_']['date_of_birth_yyyy']."-".$this->session['_YourDetails_']['date_of_birth_mm']."-".$this->session['_YourDetails_']['date_of_birth_dd']."T00:00:00";

            //Telephone                <telephoneNumber> <telephone>             Numeric field formatted as telephone number, <telephoneTypeID>should be set to '3AJPQ7C4'.
            $telephoneNumber = $this->session['_YourDetails_']['daytime_telephone'];

            if(substr($telephoneNumber, 0, 2) == "07")
            {
               $telNumberXML = "<telephone instance=\"1\">
               <telephoneNumber>$telephoneNumber</telephoneNumber>
               <telephoneTypeID>3AJPQ7C6</telephoneTypeID>
            </telephone>";
            }
            else
            {
               $telNumberXML = "<telephone instance=\"1\">
               <telephoneNumber>$telephoneNumber</telephoneNumber>
               <telephoneTypeID>3AJPQ7C4</telephoneTypeID>
            </telephone>";
            }

            //Mobile Phone             <telephoneNumber> <telephone>             Numeric field formatted as telephone number, <telephoneTypeID>should be set to '3AJPQ7C5'.
            $mobileNumber = $this->session['_YourDetails_']['mobile_telephone'];

            if($mobileNumber == '')
            {
               $mobileTelXML = "<telephone instance=\"1\">
               <telephoneNumber></telephoneNumber>
               <telephoneTypeID></telephoneTypeID>
            </telephone>";
            }

            if(substr($mobileNumber, 0, 2) == "07")
            {
               $mobileTelXML = "<telephone instance=\"1\">
               <telephoneNumber>$mobileNumber</telephoneNumber>
               <telephoneTypeID>3AJPQ7C6</telephoneTypeID>
            </telephone>";
            }

            if((substr($mobileNumber, 0, 2) != "07") && ($mobileNumber != ''))
            {
               $mobileTelXML = "<telephone instance=\"1\">
               <telephoneNumber>$mobileNumber</telephoneNumber>
               <telephoneTypeID>3AJPQ7C4</telephoneTypeID>
            </telephone>";
            }




            //Email Address            <email>           <insuredParty>          Formatted as email address.
            $email = $this->session['_YourDetails_']['email_address'];

            //Post Code                <postcode>        <correspondanceAddress>
            $postCode = $this->session['_YourDetails_']['postcode'];

            //Post Code Lookup Results <house>           <correspondanceAddress>
            $house = $this->session['_YourDetails_']['house_number_or_name'];
            

            //Post Code Lookup Results <street>          <correspondanceAddress>
            $street = $this->session['_YourDetails_']['address_line2'];

            //Post Code Lookup Results <locality>        <correspondanceAddress>
            $locality = $this->session['_YourDetails_']['address_line3'];

            //Post Code Lookup Results <county>          <correspondanceAddress>
            $county = $this->session['_YourDetails_']['address_line4'];

            //Post Code Lookup Results <country>         <correspondanceAddress>
            //defaulted

            //Post Code Lookup Results <city>            <correspondanceAddress>
            $city = $locality;

            //<qualificationID>             <qualification> Apply Logic: If <vehicleUseID> = 15 THEN                                                      <qualificationID> = 614 ELSE <qualificationID> = 613
            //tag not in example

            //<yearsWithCurrentInsurer>     <cover>         Apply Logic: <yearsNcdPriorToClaim> - 1
            //we dont ask ncd prior to claim
            $yearsWithCurrentInsurer = $ncdYears - 1;
            if($yearsWithCurrentInsurer < 0)
               $yearsWithCurrentInsurer = 0;

            //<previousInsuranceExpiryDate> <cover>         Apply Logic: <coverStartDate> - 1 Minute i.e.                                                      14/02/2012  00:00 = 13/02/2012 23:59

            $DISMkTime = mktime("0","0","0",$this->session['_YourDetails_']['date_of_insurance_start_mm'],$this->session['_YourDetails_']['date_of_insurance_start_dd'],$this->session['_YourDetails_']['date_of_insurance_start_yyyy']);

            //2011-09-29 00:00
            $DIS = $this->session['_YourDetails_']['date_of_insurance_start_yyyy']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_dd']."T00:01:00";

            //$previousInsuranceExpiryDate = date("Y-m-d",(mktime("0","0","0",$this->session['_YourDetails_']['date_of_insurance_start_mm'],$this->session['_YourDetails_']['date_of_insurance_start_dd'],$this->session['_YourDetails_']['date_of_insurance_start_yyyy']) - 1));
            //$previousInsuranceExpiryDate = $previousInsuranceExpiryDate."T23:59:59";

            $previousInsuranceExpiryDate = date("Y-m-d");
            $previousInsuranceExpiryDate = $previousInsuranceExpiryDate."T23:59:59";

            //ukResidenceYears
            $ukResidenceYears = $this->session['_YourDetails_']['date_of_insurance_start_yyyy'] - $this->session['_YourDetails_']['date_of_birth_yyyy'];


            //<previousInsurance>           <cover>         Apply Logic: If <yearsNcdPriorToClaim> = 0                                                      THEN <previousInsurance> = false ELSE  <previousInsurance> = true
            //we dont ask ncd prior to claim


            if($ncdYears > 0)
               $previousInsurance = "true";
            else
               $previousInsurance = "false";


            //<hackneyCarriage>             <vehicle>       Apply Logic: If <bodyTypeID> = 22 THEN                                                      <hackneyCarriage> = true ELSE   <hackneyCarriage> = false

            $dateOfPurchaseDate = date("Y-m-d");
            $dateOfPurchase     = $dateOfPurchaseDate."T00:00:00";

            $taxiPlatingAuthority = strtoupper($this->session["_YourDetails_"]["plating_authority"]);
            $taxiPlatingAuthorityArray = array(
               "ABERDEEN"=>"1",
               "ABERDEENSHIRE"=>"2",
               "ADUR"=>"3",
               "ALLERDALE"=>"4",
               "ALNWICK"=>"5",
               "AMBER VALLEY"=>"6",
               "ANGUS"=>"7",
               "ARGYLL & BUTE (LOCHGILPHEAD)"=>"8",
               "ARUN"=>"9",
               "ASHFIELD"=>"10",
               "ASHFORD"=>"11",
               "AYLESBURY VALE"=>"12",
               "BABERGH"=>"13",
               "BADENOCH & STRATHSPEY (HIGHLAND - KINGUSSIE)"=>"14",
               "BARKING"=>"187",
               "BARNET"=>"187",
               "BARNSLEY"=>"15",
               "BARROW IN FURNESS"=>"16",
               "BASILDON"=>"17",
               "BASINGSTOKE & DEANE"=>"18",
               "BASSETLAW"=>"19",
               "BATH & NORTH EAST SOMERSET"=>"20",
               "BEDFORD"=>"21",
               "BERWICK ON TWEED"=>"22",
               "BEXLEY"=>"187",
               "BIRMINGHAM"=>"23",
               "BLABY"=>"24",
               "BLACKBURN WITH DARWEN"=>"25",
               "BLACKPOOL"=>"26",
               "BLAENAU GWENT"=>"27",
               "BLYTH VALLEY"=>"28",
               "BOLSOVER"=>"29",
               "BOLTON"=>"30",
               "BOSTON"=>"31",
               "BOURNEMOUTH"=>"32",
               "BRACKNELL FOREST"=>"33",
               "BRADFORD"=>"34",
               "BRAINTREE"=>"35",
               "BRECKLAND"=>"36",
               "BRENT"=>"187",
               "BRENTWOOD"=>"37",
               "BRIDGEND"=>"38",
               "BRIDGENORTH"=>"39",
               "BRIGHTON & HOVE"=>"40",
               "BRISTOL CITY OF UA"=>"41",
               "BROADLAND"=>"42",
               "BROMLEY"=>"187",
               "BROMSGROVE"=>"43",
               "BROXBOURNE"=>"44",
               "BROXTOWE"=>"45",
               "BURNLEY"=>"46",
               "BURY"=>"47",
               "BURY ST EDMUNDS"=>"48",
               "CAERPHILLY"=>"49",
               "CAITHNESS (HIGHLANDS - WICK)"=>"50",
               "CALDERDALE"=>"51",
               "CAMBRIDGE"=>"52",
               "CAMDEN"=>"187",
               "CANNOCK CHASE"=>"53",
               "CANTERBURY"=>"54",
               "CARADON"=>"55",
               "CARDIFF"=>"56",
               "CARLISLE"=>"57",
               "CARMARTHENSHIRE"=>"58",
               "CARRICK"=>"59",
               "CASTLE MORPETH"=>"60",
               "CASTLE POINT"=>"61",
               "CENTRAL BEDFORDSHIRE"=>"297",
               "CEREDIGION"=>"62",
               "CHANNEL ISLANDS"=>"408",
               "CHARNWOOD"=>"63",
               "CHELMSFORD"=>"64",
               "CHELTENHAM"=>"65",
               "CHERWELL"=>"66",
               "CHESTER"=>"67",
               "CHESTERFIELD"=>"68",
               "CHESTER-LE-STREET"=>"69",
               "CHICHESTER"=>"70",
               "CHILTERN"=>"71",
               "CHORLEY"=>"72",
               "CHRISTCHURCH"=>"73",
               "CLACKMANANSHIRE (ALLOA)"=>"74",
               "CLYDEBANK"=>"75",
               "CLYDESDALE"=>"76",
               "COLCHESTER"=>"77",
               "CONGLETON"=>"78",
               "CONWY"=>"79",
               "COPELAND"=>"80",
               "CORBY"=>"81",
               "COTSWOLD"=>"82",
               "COUNTY OF HEREFORD"=>"83",
               "COVENTRY"=>"84",
               "CRAVEN"=>"85",
               "CRAWLEY"=>"86",
               "CREWE & NANTWICH"=>"87",
               "CROYDEN"=>"187",
               "DACORUM"=>"88",
               "DARLINGTON"=>"89",
               "DARTFORD"=>"90",
               "DAVENTRY"=>"91",
               "DENBIGSHIRE"=>"92",
               "DERBY"=>"93",
               "DERBYSHIRE DALES"=>"94",
               "DERWENTSIDE"=>"95",
               "DEVON COUNTY"=>"413",
               "DONCASTER"=>"96",
               "DOVER"=>"97",
               "DUDLEY"=>"98",
               "DUMFRIES & GALLOWAY"=>"99",
               "DUNBARTON"=>"100",
               "DUNDEE"=>"101",
               "DURHAM"=>"102",
               "EALING"=>"187",
               "EASINGTON"=>"103",
               "EAST AYRSHIRE (KILMARNOCK)"=>"104",
               "EAST CAMBRIDGESHIRE"=>"105",
               "EAST DEVON"=>"106",
               "EAST DORSET"=>"107",
               "EAST DUNBARTONSHIRE (KIRKINTILLOCH)"=>"108",
               "EAST HAMPSHIRE"=>"109",
               "EAST HERTS"=>"110",
               "EAST HERTFORDSHIRE"=>"110",
               "EAST KILBRIDE"=>"111",
               "EAST LINDSEY"=>"112",
               "EAST LOTHIAN (HADDINGTON)"=>"113",
               "EAST NORTHANTS"=>"114",
               "EAST RENFREWSHIRE (GIFFNOCH)"=>"115",
               "EAST RIDING OF YORKSHIRE"=>"116",
               "EAST STAFFORDSHIRE"=>"117",
               "EASTBOURNE"=>"118",
               "EASTLEIGH"=>"119",
               "EDEN"=>"120",
               "EDINBURGH"=>"121",
               "ELLESEMERE PORT & NESTON"=>"122",
               "ELMBRIDGE"=>"123",
               "ENFIELD"=>"187",
               "EPPING FOREST"=>"124",
               "EPSOM & EWELL"=>"125",
               "EREWASH"=>"126",
               "EXETER"=>"127",
               "FALKIRK"=>"128",
               "FAREHAM"=>"129",
               "FENLAND"=>"130",
               "FIFE COUNCIL (KIRKCALDY)"=>"131",
               "FLINTSHIRE"=>"132",
               "FOREST HEATH"=>"133",
               "FOREST OF DEAN"=>"134",
               "FYLDE"=>"135",
               "GATESHEAD"=>"136",
               "GEDLING"=>"137",
               "GILLINGHAM"=>"138",
               "GLASGOW CITY"=>"139",
               "GLOUCESTER"=>"140",
               "GLOUCESTERSHIRE COUNTY"=>"414",
               "GOSPORT"=>"141",
               "GRAVESHAM"=>"142",
               "GREAT YARMOUTH"=>"143",
               "GREENWICH"=>"187",
               "GUERNSEY"=>"410",
               "GUILDFORD"=>"144",
               "GWYNEDD"=>"145",
               "HACKNEY"=>"187",
               "HALTON"=>"146",
               "HAMBLETON"=>"147",
               "HAMILTON"=>"148",
               "HAMMERSMITH"=>"187",
               "HARBOROUGH"=>"149",
               "HARINGEY"=>"187",
               "HARLOW"=>"150",
               "HARROGATE"=>"151",
               "HART"=>"152",
               "HARTLEPOOL"=>"153",
               "HASTINGS"=>"154",
               "HAVANT"=>"155",
               "HAVERING"=>"187",
               "HEREFORDSHIRE"=>"156",
               "HERTSMERE"=>"157",
               "HIGH PEAK"=>"158",
               "HILLINGDON"=>"187",
               "HINCKLEY & BOSWORTH"=>"160",
               "HORSHAM"=>"161",
               "HOUNSLOW"=>"187",
               "HUNTINGDON"=>"162",
               "HYNDBURN"=>"163",
               "INVERCLYDE (GREENOCK)"=>"164",
               "HIGHLAND COUNCIL (INVERNESS)"=>"165",
               "IPSWICH"=>"166",
               "ISLE OF ANGLESEY"=>"167",
               "ISLE OF MAN"=>"168",
               "ISLE OF SCILLY"=>"169",
               "ISLE OF WIGHT"=>"170",
               "ISLINGTON"=>"187",
               "JERSEY"=>"409",
               "KEIGHLEY"=>"421",
               "KENNET"=>"171",
               "KENSINGTON"=>"187",
               "KENT"=>"412",
               "KERRIER"=>"172",
               "KETTERING"=>"173",
               "KINGS LYNN & WEST NORFOLK"=>"174",
               "KINGSTON UPON THAMES ROYAL"=>"187",
               "KINGSTON-UPON-HULL"=>"175",
               "KIRKLEES"=>"176",
               "KNOWSLEY"=>"177",
               "LAMBETH"=>"187",
               "LANCASTER"=>"178",
               "LEEDS"=>"179",
               "LEICESTER"=>"180",
               "LEOMINSTER"=>"181",
               "LEWES"=>"182",
               "LEWISHAM"=>"187",
               "LICHFIELD"=>"183",
               "LINCOLN"=>"184",
               "LIVERPOOL"=>"185",
               "LOCHABER (HIGHLAND - FORT WILLIAM)"=>"159",
               "LONDON PCO"=>"187",
               "LUTON"=>"188",
               "MACCLESFIELD / CHESHIRE EAST"=>"189",
               "MAIDSTONE"=>"190",
               "MALDON"=>"191",
               "MALVERN HILLS"=>"192",
               "MANCHESTER"=>"193",
               "MANSFIELD"=>"194",
               "MEDWAY"=>"195",
               "MELTON"=>"196",
               "MENDIP"=>"197",
               "MERTHYR TYDFIL"=>"198",
               "MERTON"=>"187",
               "MID BEDFORDSHIRE"=>"199",
               "MID DEVON"=>"200",
               "MID SUFFOLK"=>"201",
               "MID SUSSEX"=>"202",
               "MIDDLESBOROUGH"=>"203",
               "MIDLOTHIAN COUNCIL"=>"204",
               "MILTON KEYNES"=>"205",
               "MOLE VALLEY"=>"206",
               "MONMOUTHSHIRE"=>"207",
               "MORAY (ELGIN)"=>"208",
               "MOTHERWELL DISTRICT COUNCIL"=>"",
               "NAIRN (HIGHLANDS)"=>"209",
               "NEATH & PORT TALBOT"=>"210",
               "NEW FOREST"=>"211",
               "NEWARK & SHERWOOD"=>"212",
               "NEWBURY"=>"213",
               "NEWCASTLE UNDER LYME"=>"214",
               "NEWCASTLE UPON TYNE"=>"215",
               "NEWHAM"=>"187",
               "NEWPORT"=>"216",
               "NORTH AYRSHIRE (IRVINE)"=>"217",
               "NORTH CORNWALL"=>"218",
               "NORTH DEVON"=>"219",
               "NORTH DORSET"=>"220",
               "NORTH EAST DERBYSHIRE"=>"221",
               "NORTH EAST LINCOLNSHIRE"=>"222",
               "NORTH HERTFORDSHIRE"=>"223",
               "NORTH KESTEVEN"=>"224",
               "NORTH LANARKSHIRE (MOTHERWELL)"=>"225",
               "NORTH LANARKSHIRE(SOUTH/CENTRAL)"=>"226",
               "NORTH LICOLNSHIRE"=>"227",
               "NORTH NORFOLK"=>"228",
               "NORTH SHROPSHIRE"=>"229",
               "NORTH SOMERSET"=>"230",
               "NORTH TYNESIDE"=>"231",
               "NORTH WARWICKSHIRE"=>"232",
               "NORTH WEST LEICESTERSHIRE"=>"233",
               "NORTH WILTSHIRE"=>"234",
               "NORTH YORKSHIRE COUNTY"=>"415",
               "NORTHAMPTON"=>"235",
               "NORTHUMBERLAND"=>"411",
               "NORWICH"=>"236",
               "NOTTINGHAM"=>"237",
               "NUNEATON & BEDWORTH"=>"238",
               "OADBY & WIGSTON"=>"239",
               "OLDHAM"=>"240",
               "ORKNEY ISLANDS (KIRKWALL)"=>"241",
               "OSWESTRY"=>"242",
               "OXFORD"=>"243",
               "OXFORDSHIRE COUNTY"=>"416",
               "SHREWSBURY & ATCHAM / SHROPSHIRE COUNCIL"=>"416",
               "PEMBROKESHIRE"=>"244",
               "PENDLE"=>"245",
               "PENWITH"=>"246",
               "PERTH & KINROSS"=>"247",
               "PETERBOROUGH"=>"248",
               "PLYMOUTH"=>"249",
               "POOLE"=>"250",
               "PORTSMOUTH"=>"251",
               "POWYS"=>"252",
               "PRESTON"=>"253",
               "PURBECK"=>"254",
               "READING"=>"255",
               "REDBRIDGE"=>"187",
               "REDCAR & CLEVELAND"=>"256",
               "REDDITCH"=>"257",
               "REIGATE AND BANSTEAD"=>"258",
               "RENFREWSHIRE (PAISLEY)"=>"259",
               "RESTORMEL"=>"260",
               "RHONDDA CYNON TAFF"=>"261",
               "RIBBLE VALLEY"=>"262",
               "RICHMOND"=>"187",
               "RICHMONDSHIRE"=>"263",
               "ROCHDALE"=>"264",
               "ROCHESTER"=>"265",
               "ROCHFORD"=>"266",
               "ROSS & CROMARTY (HIGHLAND - DINGWALL)"=>"267",
               "ROSSENDALE"=>"268",
               "ROTHER"=>"269",
               "ROTHERHAM"=>"270",
               "RUGBY"=>"272",
               "RUNNYMEDE"=>"273",
               "RUSHCLIFFE"=>"274",
               "RUSHMOOR"=>"275",
               "RUTLAND"=>"277",
               "RYEDALE"=>"278",
               "SALFORD"=>"279",
               "SALISBURY"=>"280",
               "SANDWELL"=>"281",
               "SCARBOROUGH"=>"282",
               "SCOTTISH BORDERS"=>"283",
               "SEDGEFIELD"=>"284",
               "SEDGEMOOR"=>"285",
               "SEFTON"=>"286",
               "SELBY"=>"287",
               "SEVENOAKS"=>"288",
               "SHEFFIELD"=>"289",
               "SHEPWAY"=>"290",
               "SHETLAND ISLANDS (LERWICK)"=>"291",
               "SHREWSBURY"=>"292",
               "SHROPSHIRE COUNTY"=>"417",
               "SKYE & LOCHALSH (HIGHLAND - ISLE OF SKYE)"=>"293",
               "SLOUGH"=>"294",
               "SOLIHULL"=>"295",
               "SOUTH AYRSHIRE (AYR)"=>"296",
               "SOUTH BEDFORDSHIRE"=>"297",
               "SOUTH BUCKINGHAM"=>"298",
               "SOUTH CAMBRIDGESHIRE"=>"299",
               "SOUTH DERBYSHIRE"=>"300",
               "SOUTH EAST & METROPOLITAN"=>"187",
               "SOUTH GLOUCESTER"=>"301",
               "SOUTH HAMS"=>"302",
               "SOUTH HEREFORDSHIRE"=>"303",
               "SOUTH HOLLAND"=>"304",
               "SOUTH KESTEVEN"=>"305",
               "SOUTH LAKELAND"=>"306",
               "SOUTH LANARKSHIRE"=>"307",
               "SOUTH NORFOLK"=>"309",
               "SOUTH NORTHAMPTONSHIRE"=>"310",
               "SOUTH OXFORDSHIRE"=>"311",
               "SOUTH RIBBLE"=>"312",
               "SOUTH SHROPSHIRE"=>"313",
               "SOUTH SOMERSET"=>"314",
               "SOUTH STAFFORDSHIRE"=>"315",
               "SOUTH TYNESIDE"=>"316",
               "SOUTHAMPTON"=>"317",
               "SOUTHEND-ON-SEA"=>"318",
               "SOUTHWARK"=>"187",
               "SPELTHORNE"=>"319",
               "ST ALBANS"=>"320",
               "ST EDMUNDSBURY"=>"321",
               "ST HELENS"=>"322",
               "ST HELIER - JERSEY"=>"409",
               "STAFFORD"=>"323",
               "STAFFORDSHIRE COUNTY"=>"420",
               "STAFFORDSHIRE MOORLANDS"=>"324",
               "STEVENAGE"=>"325",
               "STIRLING"=>"326",
               "STOCKPORT"=>"327",
               "STOCKTON ON TEES"=>"328",
               "STOKE ON TRENT"=>"329",
               "STRATFORD ON AVON"=>"330",
               "STROUD"=>"331",
               "SUFFOLK COASTAL"=>"332",
               "SUNDERLAND"=>"333",
               "SURREY HEATH"=>"334",
               "SUTHERLAND (HIGHLAND - GOLSPIE)"=>"335",
               "SUTTON"=>"187",
               "SWALE"=>"336",
               "SWANSEA"=>"337",
               "SWINDON"=>"338",
               "TAMESIDE"=>"339",
               "TAMWORTH"=>"340",
               "TANDRIDGE"=>"341",
               "TAUNTON DEANE"=>"342",
               "TEESDALE"=>"343",
               "TEIGNBRIDGE"=>"344",
               "TELFORD & WREKIN"=>"345",
               "TENDRING"=>"346",
               "TEST VALLEY"=>"347",
               "TEWKESBURY"=>"348",
               "THAMESDOWN"=>"349",
               "THANET BROADSTAIRS"=>"350",
               "THANET DISTRICT"=>"351",
               "THANET MARGATE"=>"352",
               "THANET RAMSGATE"=>"353",
               "THREE RIVERS"=>"354",
               "THURROCK"=>"355",
               "TONBRIDGE & MALLING"=>"356",
               "TORBAY"=>"357",
               "TORFAEN"=>"358",
               "TORRIDGE"=>"359",
               "TOWER HAMLETS"=>"187",
               "TRAFFORD"=>"360",
               "TUNBRIDGE WELLS"=>"361",
               "TYNEDALE"=>"362",
               "UTTLESFORD"=>"363",
               "VALE OF GLAMORGAN"=>"364",
               "VALE OF WHITE HORSE"=>"365",
               "VALE ROYAL"=>"366",
               "WAKEFIELD"=>"367",
               "WALSALL"=>"368",
               "WALTHAM FOREST"=>"187",
               "WANDSWORTH"=>"187",
               "WANSBECK"=>"369",
               "WARRINGTON"=>"370",
               "WARWICK"=>"371",
               "WATFORD"=>"372",
               "WAVENEY"=>"373",
               "WAVERLEY"=>"374",
               "WEALDEN"=>"375",
               "WEAR VALLEY"=>"376",
               "WELLINGBOROUGH"=>"377",
               "WELWYN HATFIELD"=>"378",
               "WEST BERKSHIRE"=>"379",
               "WEST DEVON"=>"380",
               "WEST DORSET"=>"381",
               "WEST DUNBARTONSHIRE (CLYDEBANK)"=>"382",
               "WEST LANCASHIRE"=>"383",
               "WEST LINDSEY"=>"384",
               "WEST LOTHIAN (LIVINGSTON)"=>"385",
               "WEST OXFORDSHIRE"=>"386",
               "WEST SOMERSET"=>"387",
               "WEST SUSSEX COUNTY"=>"418",
               "WEST WILTSHIRE"=>"388",
               "WESTERN ISLES (STORNOWAY)"=>"389",
               "WESTMINSTER"=>"187",
               "WEYMOUTH & PORTLAND"=>"390",
               "WIGAN"=>"391",
               "WILTSHIRE COUNTY"=>"419",
               "WINCHESTER"=>"392",
               "WINDSOR & MAIDENHEAD"=>"393",
               "WIRRALL"=>"394",
               "WOKING"=>"395",
               "WOKINGHAM"=>"396",
               "WOLVERHAMPTON"=>"397",
               "WORCESTER"=>"398",
               "WORTHING"=>"399",
               "WREKIN"=>"400",
               "WREXHAM"=>"401",
               "WYCHAVON"=>"402",
               "WYCOMBE"=>"403",
               "WYRE"=>"404",
               "WYRE FORREST"=>"405",
               "YNYS MON"=>"406",
               "YORK"=>"407",
               "DVA (NI)"=>"0",
               "KILMARNOCK & LOUDOUN DISTRICT COUNCIL"=>"0",
               "KINCARDINE & DEESIDE DISTRICT COUNCIL"=>"0",
               "KIRKALDY DISTRICT COUNCIL"=>"0",
               "LOTHIAN REGIONAL COUNCIL"=>"0",
               "PSV EASTERN"=>"0",
               "PSV JERSEY"=>"0",
               "PSV NORTH EAST"=>"0",
               "PSV NORTH WEST"=>"0",
               "PSV SCOTLAND"=>"0",
               "PSV WALES"=>"0",
               "PSV WEST MIDLANDS"=>"0",
               "PSV WESTERN"=>"0",
               "SOUTH LANARKSHIRE (HIGHLANDS"=>"308",
               "CHESHIRE EAST COUNCIL" => "189",
               "CHESHIRE WEST & CHESTER (RED) ELLESMERE PORT" => "122",
               "CHESHIRE WEST & CHESTER (GREEN) VALE ROYAL" => "366",
               "CHESHIRE WEST & CHESTER (BLUE) CHESTER" => "67",
               "CORNWALL COUNCIL" => "218",
               "ABERDEEN CENTRAL (INVERURIE)" => "1",
               "ABERDEEN  CITY (ABERDEEN)" => "1",
               "ABERDEENSHIRE NORTH (BANFF)" => "2",
               "ABERDEENSHIRE SOUTH (STONEHAVEN)" => "2",
               "ANGUS COUNCIL (FORFAR)" => "7",
               "BRISTOL" => "41",
               "CONGLETON / CHESHIRE EAST" => "78",
               "SHREWSBURY & ATCHAM / SHROPSHIRE COUNCIL" => "292",
               "SOUTH LANARKSHIRE (HAMILTON)" => "307",
               "SOUTH LANARKSHIRE (LANARK)" => "307",
               "SOUTH LANARKSHIRE (RUTHERGLEN)" => "307",
               "THANET" => "351",
            );

            $taxiPlatingAuthorityID = $taxiPlatingAuthorityArray[$taxiPlatingAuthority];

            $privateCarNCB          = $this->session['_YourDetails_']['private_car_ncb'];


            $claimsXML        = "<claim instance=\"1\" insuredPartyInstance=\"1\">
                                 <atFault>true</atFault>
                                 <claimDetailsID>1</claimDetailsID>
                                 <claimStatusID>003</claimStatusID>
                                 <claimTypeID>A</claimTypeID>
                                 <dateOfClaim>2013-04-02T00:00:00</dateOfClaim>
                                 <injuryInvolved>true</injuryInvolved>
                                 <ncdAffected>false</ncdAffected>
                                 <settledDate>2005-10-02T00:00:00</settledDate>
                                 <totalCost>60000</totalCost>
                              </claim>";
            $convictionsXML   = "<conviction instance=\"1\" insuredPartyInstance=\"1\">
                                 <banLengthInMonths>12</banLengthInMonths>
                                 <dateOfConviction>2013-04-02T00:00:00</dateOfConviction>
                                 <disqualified>true</disqualified>
                                 <fine>100</fine>
                                 <offenceCodeID>DD10</offenceCodeID>
                                 <pending>false</pending>
                                 <points>12</points>
                              </conviction>";


            $claimMade = $this->session['_YourDetails_']['claims_5_years'];
            $convMade  = $this->session['_YourDetails_']['convictions_5_years'];

            //            Claims / Conv =  N N  fwquotezone
            //            Claims / Conv =  Y N  fwquotezoneCF
            //            Claims / Conv =  N Y  fwquotezoneCO
            //            Claims / Conv =  Y Y  fwquotezoneCC
            if($claimMade == "No" && $convMade  == "No")
            {
               $aggregatorXML    = "fwquotezone";
               $claimsXML        = "";
               $convictionsXML   = "";
            }

            if($claimMade == "Yes" && $convMade  == "No")
            {
               $aggregatorXML    = "fwquotezoneCF";
               $convictionsXML   = "";
            }

            if($claimMade == "No" && $convMade  == "Yes")
            {
               $aggregatorXML    = "fwquotezoneCO";
               $claimsXML        = "";
            }

            if($claimMade == "Yes" && $convMade  == "Yes")
               $aggregatorXML    = "fwquotezoneCC";

            if(($this->session["_COMPANY_DETAILS_"]['10582']["REQUEST_XML"] == 1) AND ($this->siteID == 10582))
            {
               $houseNumber = explode(" ", $house);
               $house = $houseNumber[0]; 
            }

            if(($this->session["_COMPANY_DETAILS_"]['1152']["REQUEST_XML"] == 1) AND ($this->siteID == 1152))
            {
               $houseNumber = explode(" ", $house);
               $house = $houseNumber[0]; 
            }

            if(($this->session["_COMPANY_DETAILS_"]['3147']["REQUEST_XML"] == 1) AND ($this->siteID == 3147))
            {
               $houseNumber = explode(" ", $house);
               $house = $houseNumber[0]; 
            }
            
            if(($this->session["_COMPANY_DETAILS_"]['3148']["REQUEST_XML"] == 1) AND ($this->siteID == 3148))
            {
               $houseNumber = explode(" ", $house);
               $house = $houseNumber[0]; 
            }

            // <yearsNcdPriorToClaim> = 0
            // THEN
            // <previousInsurance> = false
            // ELSE
            // <previousInsurance> = true

            $xmlTemplate = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                           <soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
                           <soap:Body>
                              <Quote xmlns=\"http://TGSL/TES_Aggregator\">
                                 <lxmlTGSLXML>
                                 <tgsl xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://localhost/TES_Aggregator/schemas/tgsl.xsd\" xmlns=\"http://www.transactorgsl.com/schemas\">
                                    <aggregator>$aggregatorXML</aggregator>
                                    <domain>http://localhost</domain>
                                    <partner>
                                       <partnerName>freeway</partnerName>
                                       <broker>freeway</broker>
                                    </partner>
                                    <product>cv</product>
                                 </tgsl>
                                 </lxmlTGSLXML>
                                 <lstrRiskXML>
                                 <cvRisk xmlns=\"http://www.transactorgsl.com/schemas\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://localhost/TES_Aggregator/Schemas/TGSL/TGSLCVSchema.xsd\">
                                    <insuredParty instance=\"1\">
                                       <accessToOtherVehicles>true</accessToOtherVehicles>
                                       <client>true</client>
                                       <clubMember>false</clubMember>
                                       <dateLicenceObtained>$licDateTime</dateLicenceObtained>
                                       <dob>$dob</dob>
                                       <email>$email</email>
                                       <forename>$firstName</forename>
                                       <homeOwner>true</homeOwner>
                                       <insurancePreviouslyRefused>false</insurancePreviouslyRefused>
                                       <licenceRestrictionsID>8</licenceRestrictionsID>
                                       <licenceTypeID>F</licenceTypeID>
                                       <mainDriver>true</mainDriver>
                                       <maritalStatusID></maritalStatusID>
                                       <mothersMaidenName></mothersMaidenName>
                                       <numberOfVehiclesAccessTo>0</numberOfVehiclesAccessTo>
                                       <password></password>
                                       <sex>$sex</sex>
                                       <surname>$surname</surname>
                                       <titleID>$titleID</titleID>
                                       <ukResidenceYears>$ukResidenceYears</ukResidenceYears>
                                       <vehicleUseID>$taxiUseID</vehicleUseID>
                                    </insuredParty>
                                    <correspondanceAddress instance=\"1\">
                                       <city>$city</city>
                                       <country>United Kingdom</country>
                                       <county>$county</county>
                                       <house>".$house."</house>
                                       <locality>$locality</locality>
                                       <postcode>$postCode</postcode>
                                       <street>$street</street>
                                    </correspondanceAddress>
                                    <occupation instance=\"1\" insuredPartyInstance=\"1\">
                                       <employersBusinessID>703</employersBusinessID>
                                       <employmentStatusID>S</employmentStatusID>
                                       <occupationID>D34</occupationID>
                                       <parttime>false</parttime>
                                       <primaryOccupation>true</primaryOccupation>
                                    </occupation>
                                    $telNumberXML $mobileTelXML
                                    <qualification instance=\"1\" insuredPartyInstance=\"1\">
                                       <datePassed>$datePassed</datePassed>
                                       <qualificationID>$qualificationID</qualificationID>
                                    </qualification>
                                    <cover instance=\"1\">
                                       <coverRequiredID>$coverRequiredID</coverRequiredID>
                                       <coverStartDate>$DIS</coverStartDate>
                                       <previousInsurance>$previousInsurance</previousInsurance>
                                       <previousInsuranceExpiryDate>$previousInsuranceExpiryDate</previousInsuranceExpiryDate>
                                       <previousInsurancePolicyNumber>LEADIMPORT</previousInsurancePolicyNumber>
                                       <previousInsurerID>997</previousInsurerID>
                                       <yearsWithCurrentInsurer>$yearsWithCurrentInsurer</yearsWithCurrentInsurer>
                                       <yearsNcdPriorToClaim>$ncdYears</yearsNcdPriorToClaim>
                                       <voluntaryExcess>150</voluntaryExcess>
                                    </cover>
                                    ".$claimsXML."
                                    ".$convictionsXML."
                                    <vehicle instance=\"1\">
                                       <personalisedPlate>false</personalisedPlate>
                                       <abiCode>0</abiCode>
                                       <annualMileage>$annualMileage</annualMileage>
                                       <bodyTypeID>".$bodyTypeID."</bodyTypeID>
                                       <businessMileage>0</businessMileage>
                                       <countryOfFirstRegistrationID>GB</countryOfFirstRegistrationID>
                                       <currentMileage>0</currentMileage>
                                       <dateOfPurchase>$dateOfPurchase</dateOfPurchase>
                                       <fuelTypeID>$fuelTypeID</fuelTypeID>
                                       <gearboxTypeID>$gearboxTypeID</gearboxTypeID>
                                       <imported>false</imported>
                                       <keeperID>1</keeperID>
                                       <kitCar>false</kitCar>
                                       <leftHandDrive>false</leftHandDrive>
                                       <numberOfSeats>$numberOfSeats</numberOfSeats>
                                       <ownerID>1</ownerID>
                                       <overnightParkingID>1</overnightParkingID>
                                       <pricePaid>0</pricePaid>
                                       <qPlated>false</qPlated>
                                       <qPlatedReasonID>0</qPlatedReasonID>
                                       <registrationNumber>$registrationNumber</registrationNumber>
                                       <value>$estimatedValue</value>
                                       <vehicleCC>$engineCC</vehicleCC>
                                       <vehicleMake>$vehicleMake</vehicleMake>
                                       <vehicleMark>$series</vehicleMark>
                                       <vehicleModel>$vehicleModel</vehicleModel>
                                       <yearOfManufacture>$yearOfManufacture</yearOfManufacture>
                                       <trailer>false</trailer>
                                       <carriesGoods>false</carriesGoods>
                                       <goodsCarriedID>0</goodsCarriedID>
                                       <grossVehicleWeight>0</grossVehicleWeight>
                                       <carryingCapacity>0</carryingCapacity>
                                       <cctv>false</cctv>
                                       <interiorCamera>false</interiorCamera>
                                       <exteriorCameraID>0</exteriorCameraID>
                                       <hackneyCarriage>$hackneyCarriage</hackneyCarriage>
                                       <contractWork>false</contractWork>
                                       <contractDetails></contractDetails>
                                       <licensePlate></licensePlate>
                                       <taxiAreaID>$taxiPlatingAuthorityID</taxiAreaID>
                                    </vehicle>
                                    <ncd instance=\"1\">
                                       <ncdYears>$ncdYears</ncdYears>
                                       <otherVehicleNCDYears>$privateCarNCB</otherVehicleNCDYears>
                                       <protectedNCD>false</protectedNCD>
                                    </ncd>
                                    <garageAddress instance=\"1\">
                                       <city>$city</city>
                                       <country>United Kingdom</country>
                                       <county>$county</county>
                                       <house>$house</house>
                                       <locality>$locality</locality>
                                       <postcode>$postCode</postcode>
                                       <street>$street</street>
                                    </garageAddress>
                                 </cvRisk>
                                 </lstrRiskXML>
                              </Quote>
                           </soap:Body>
                           </soap:Envelope>";


            //http://94.127.98.171/TES_Aggregator/Aggregator.asmx
            // $url = "http://94.127.98.171/TES_Aggregator/Aggregator.asmx";
            $url = "https://myquoteagg.freewayinsurance.co.uk/tes_aggregator/aggregator.asmx";

            $ch = curl_init();

            $header[] = "POST /tes_aggregator/aggregator.asmx HTTP/1.1";
            $header[] = "Host:myquoteagg.freewayinsurance.co.uk";
            //$header[] = "Content-Type: application/soap+xml; charset=utf-8";
            $header[] = "Content-Type: text/xml; charset=utf-8";
            $header[] = "Content-length: ".strlen($xmlTemplate);
            $header[] = "Connection: close \r\n";
            $header[] = $xmlTemplate;

            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 180);

            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);

            $result = curl_exec($ch);

            curl_close ($ch);

            $xmlMessageBody = prepareXMLMessageBody($xmlTemplate, $result);

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
            ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!
            ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

         $siteDetailsArray = $objSite->GetSite($this->siteID);
         $siteNameForEmail = $siteDetailsArray["name"];

         if(($this->session["_COMPANY_DETAILS_"]['1152']["REQUEST_XML"] == 1) AND ($this->siteID == 1152))
            $subjectDetails = "Freeway - Taxi 1";

         if(($this->session["_COMPANY_DETAILS_"]['3147']["REQUEST_XML"] == 1) AND ($this->siteID == 3147))
            $subjectDetails = "Freeway - Taxi 2";

         if(($this->session["_COMPANY_DETAILS_"]['3148']["REQUEST_XML"] == 1) AND ($this->siteID == 3148))
            $subjectDetails = "Freeway - Taxi 3";

         if(($this->session["_COMPANY_DETAILS_"]['10582']["REQUEST_XML"] == 1) AND ($this->siteID == 10582))
            $subjectDetails = "Freeway - Taxi 4";

         if(($this->session["_COMPANY_DETAILS_"]['11990']["REQUEST_XML"] == 1) AND ($this->siteID == 11990))
            $subjectDetails = "Freeway - Taxi id11990";

         //eb2
         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subjectDetails." ".$siteNameForEmail." XML Request ".$quoteRef;
         $mailBody    = "Request : \n".$xmlTemplate." \n\n Result : ".$result;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         //seopa
         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." ".$siteNameForEmail;
         $mailBody    = "Request : \n".$xmlTemplate." \n\n Result : ".$result;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         //company
         $mailTo      = "LeadCheck@freewayinsurance.co.uk";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." ".$siteNameForEmail;
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if(preg_match("/\<Status\>QUOTE\<\/Status\>/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
         else
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

         $sentContent =  "\nURL: ".$url."\n<br>PARAMS:".$xmlTemplate;

         $this->DoOutboundFiles("xml", $sentContent,$result);
      }
   }
   
   if($this->outbounds['CSV'])
   {
      if(($this->session["_COMPANY_DETAILS_"]['9835']["REQUEST_CSV"] == 1) AND ($this->siteID == 9835) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {
         //error_reporting(E_ALL);
         
         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/plain; charset=iso-8859-1'."\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         
         $contentHeader = "Quote Date,Quote Time,Taxi Used For,Uber Driver,Taxi Type,Type Of Cover,Taxi Make,Taxi Model,Year Of Manufacture,Vehicle Mileage,Estimated Value,Taxi Capacity,Taxi No Claims Bonus,Private Car No Claims Bonus,Taxi Plating Authority,Taxi Registration,Full UK Licence,Taxi Badge,Taxi Driver(s),Claims Last 5 Years,Convictions Last 5 Years,Title,First Name,Surname,Telephone,Mobile Phone,Best Time To Call,Email Address,Postcode,House Number/Name,Address Line 1,Street Name,Town/City,County,Date Of Birth,Date Of Insurance Start,Quote Reference,\n";

                       
         $quoteDate                 = $this->session['_YourDetails_']['start_date'];  
         $quoteTime                 = date("H:i:s"); 
         $taxiUsedFor               = $_YourDetails["taxi_used_for"][$this->session['_YourDetails_']['taxi_used_for']];
         $uberDriver                = $_YourDetails["uber_driver"][$this->session['_YourDetails_']['uber_driver']];
         $taxiType                  = $_YourDetails["taxi_type"][$this->session['_YourDetails_']['taxi_type']];
         $typeOfCover               = $_YourDetails["type_of_cover"][$this->session['_YourDetails_']['type_of_cover']];
         $taxiMake                  = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel                 = $this->session['_YourDetails_']['taxi_model'];
         $yearOfManufacture         = $this->session['_YourDetails_']['year_of_manufacture'];
         $vehicleMileage            = $_YourDetails['vehicle_mileage'][$this->session['_YourDetails_']['vehicle_mileage']];
         $vehicleMileage            = str_replace(",","",$vehicleMileage);
            $estimatedValue            = $this->session['_YourDetails_']['estimated_value'];
         $taxiCapacity              = $_YourDetails["taxi_capacity"][$this->session['_YourDetails_']['taxi_capacity']];
         $taxiNoClaimsBonus         = $_YourDetails["taxi_ncb"][$this->session['_YourDetails_']['taxi_ncb']];
         $privateCarNoClaimsBonus   = $_YourDetails["private_car_ncb"][$this->session['_YourDetails_']['private_car_ncb']];
         $taxiPlatingAuthority      = $this->session['_YourDetails_']['plating_authority'];
         $taxiRegistration          = $this->session['_YourDetails_']['vehicle_registration_number'];
         $fullUKLicence             = $_YourDetails["period_of_licence"][$this->session['_YourDetails_']['period_of_licence']];
         $taxiBadge                 = $_YourDetails["taxi_badge"][$this->session['_YourDetails_']['taxi_badge']]; 
         $taxiDrivers               = $_YourDetails["taxi_driver"][$this->session['_YourDetails_']['taxi_driver']];
         $claimsLast5Years          = $_YourDetails["claims_5_years"][$this->session['_YourDetails_']['claims_5_years']];
            $claimsLast5Years       = str_replace(",","",$claimsLast5Years);
         $convictionsLast5Years     = $_YourDetails["convictions_5_years"][$this->session['_YourDetails_']['convictions_5_years']];
         $title                     = $this->session['_YourDetails_']['title'];  
         $firstName                 = $this->session['_YourDetails_']['first_name'];
         $surname                   = $this->session['_YourDetails_']['surname'];  
         $telephoneNumber           = $this->session['_YourDetails_']['daytime_telephone']; 
         $mobilePhone               = $this->session['_YourDetails_']['mobile_telephone'];
         $bestTimeToCall            = $_YourDetails["best_time_call"][$this->session['_YourDetails_']['best_time_call']];
         $email                     = $this->session['_YourDetails_']['email_address'];
         $postcode                  = $this->session['_YourDetails_']['postcode'];  
         $dateOfBirth               = $this->session['_YourDetails_']['date_of_birth'];  
         $dateOfInsuranceStart      = $this->session['_YourDetails_']['start_date'];  
         $quoteRef                  = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];


         $contentRows = "";
         $contentRows = "$quoteDate,$quoteTime,$taxiUsedFor,$uberDriver,$taxiType,$typeOfCover,$taxiMake,$taxiModel,$yearOfManufacture,$vehicleMileage,$estimatedValue,$taxiCapacity,$taxiNoClaimsBonus,$privateCarNoClaimsBonus,$taxiPlatingAuthority,$taxiRegistration,$fullUKLicence,$taxiBadge,$taxiDrivers,$claimsLast5Years,$convictionsLast5Years,$title,$firstName,$surname,$telephoneNumber,$mobilePhone,$bestTimeToCall,$email,$postcode,$houseNumberOrName,$addressLine1,$streetName,$town,$county,$dateOfBirth,$dateOfInsuranceStart,$quoteRef,\n";
     
         $today       = date("Y-m-d-H:i:s");
         $csvFile     = "Quotezone-High-Gear-Taxi-OOH_".$quoteRef."_".$today.".csv";
         $csvFileName = "/tmp/$csvFile";

         $contentOfFile  = "";
         $contentOfFile .= $contentHeader;
         $contentOfFile .= $contentRows;

         $fh = fopen($csvFileName,"w+");
         fwrite($fh,$contentOfFile);
         fclose($fh);

         $ftpServer   = "ftp.highgear.co.uk";
         $ftpUserName = "quotezone@highgear.co.uk";
         $ftpUserPass = "!VywXB5NXJll";

         // $ftpUserName = "qz2@highgear.co.uk";
         // $ftpUserPass = "SU?OyD^xSL{X";

            //FTP & explicit FTPS port: 21

         $file = $csvFileName;

         $remoteFile = $csvFile;

         $ftpError = "";

         // set up basic connection
         $connId = ftp_connect($ftpServer);
      
         if(! $connId)
            $ftpError .= "<br>ftp_connect(".$ftpServer.") function return false<br>";

         // login with username and password
         $loginResult = ftp_login($connId, $ftpUserName, $ftpUserPass);

         if(! $loginResult)
            $ftpError .= "<br>ftp_login() function return false (".$ftpUserName."@".$ftpUserPass.")<br>";

         // turn passive mode on
         if( ! ftp_pasv($connId, true))
            $ftpError .= "<br>ftp_pasv() function return false <br>";

			
		// Initiate the Upload
		// upload a file
         if (ftp_put($connId, $remoteFile, $file, FTP_BINARY))
         {
            $result = "successfully uploaded $file\n";

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 1);
         }
         else
         {
            $ftpError .= "<br>ftp_put($connId, $remoteFile, $file, FTP_BINARY) function return false <br>";

            $result = "There was a problem while uploading $file\n";

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 2);
         }

         // close the connection
         ftp_close($connId);

         @unlink($csvFileName);
         
          

         //seopa
         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi - High Gear OOH FTP";
         $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
         $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers, $this->session, "CSV", $this->siteID);
         
         //eb2
         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi - High Gear OOH FTP";
         $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
         $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers, $this->session,"CSV",$this->siteID);


         $this->DoOutboundFiles("ftp", $contentOfFile,$result."<br>".$ftpError);
      }
      
      if(($this->session["_COMPANY_DETAILS_"]['10189']["REQUEST_CSV"] == 1) AND ($this->siteID == 10189) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {
         //error_reporting(E_ALL);
         
         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/plain; charset=iso-8859-1'."\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         
         $contentHeader = "Quote Date,Quote Time,Taxi Used For,Uber Driver,Taxi Type,Type Of Cover,Taxi Make,Taxi Model,Year Of Manufacture,Vehicle Mileage,Estimated Value,Taxi Capacity,Taxi No Claims Bonus,Private Car No Claims Bonus,Taxi Plating Authority,Taxi Registration,Full UK Licence,Taxi Badge,Taxi Driver(s),Claims Last 5 Years,Convictions Last 5 Years,Title,First Name,Surname,Telephone,Mobile Phone,Best Time To Call,Email Address,Postcode,House Number/Name,Address Line 1,Street Name,Town/City,County,Date Of Birth,Date Of Insurance Start,Quote Reference,\n";

                       
         $quoteDate                 = $this->session['_YourDetails_']['start_date'];  
         $quoteTime                 = date("H:i:s"); 
         $taxiUsedFor               = $_YourDetails["taxi_used_for"][$this->session['_YourDetails_']['taxi_used_for']];
         $uberDriver                = $_YourDetails["uber_driver"][$this->session['_YourDetails_']['uber_driver']];
         $taxiType                  = $_YourDetails["taxi_type"][$this->session['_YourDetails_']['taxi_type']];
         $typeOfCover               = $_YourDetails["type_of_cover"][$this->session['_YourDetails_']['type_of_cover']];
         $taxiMake                  = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel                 = $this->session['_YourDetails_']['taxi_model'];
         $yearOfManufacture         = $this->session['_YourDetails_']['year_of_manufacture'];
         $vehicleMileage            = $_YourDetails['vehicle_mileage'][$this->session['_YourDetails_']['vehicle_mileage']];
         $vehicleMileage            = str_replace(",","",$vehicleMileage);
            $estimatedValue            = $this->session['_YourDetails_']['estimated_value'];
         $taxiCapacity              = $_YourDetails["taxi_capacity"][$this->session['_YourDetails_']['taxi_capacity']];
         $taxiNoClaimsBonus         = $_YourDetails["taxi_ncb"][$this->session['_YourDetails_']['taxi_ncb']];
         $privateCarNoClaimsBonus   = $_YourDetails["private_car_ncb"][$this->session['_YourDetails_']['private_car_ncb']];
         $taxiPlatingAuthority      = $this->session['_YourDetails_']['plating_authority'];
         $taxiRegistration          = $this->session['_YourDetails_']['vehicle_registration_number'];
         $fullUKLicence             = $_YourDetails["period_of_licence"][$this->session['_YourDetails_']['period_of_licence']];
         $taxiBadge                 = $_YourDetails["taxi_badge"][$this->session['_YourDetails_']['taxi_badge']]; 
         $taxiDrivers               = $_YourDetails["taxi_driver"][$this->session['_YourDetails_']['taxi_driver']];
         $claimsLast5Years          = $_YourDetails["claims_5_years"][$this->session['_YourDetails_']['claims_5_years']];
            $claimsLast5Years       = str_replace(",","",$claimsLast5Years);
         $convictionsLast5Years     = $_YourDetails["convictions_5_years"][$this->session['_YourDetails_']['convictions_5_years']];
         $title                     = $this->session['_YourDetails_']['title'];  
         $firstName                 = $this->session['_YourDetails_']['first_name'];
         $surname                   = $this->session['_YourDetails_']['surname'];  
         $telephoneNumber           = $this->session['_YourDetails_']['daytime_telephone']; 
         $mobilePhone               = $this->session['_YourDetails_']['mobile_telephone'];
         $bestTimeToCall            = $_YourDetails["best_time_call"][$this->session['_YourDetails_']['best_time_call']];
         $email                     = $this->session['_YourDetails_']['email_address'];
         $postcode                  = $this->session['_YourDetails_']['postcode'];  
         $dateOfBirth               = $this->session['_YourDetails_']['date_of_birth'];  
         $dateOfInsuranceStart      = $this->session['_YourDetails_']['start_date'];  
         $quoteRef                  = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];


         $contentRows = "";
         $contentRows = "$quoteDate,$quoteTime,$taxiUsedFor,$uberDriver,$taxiType,$typeOfCover,$taxiMake,$taxiModel,$yearOfManufacture,$vehicleMileage,$estimatedValue,$taxiCapacity,$taxiNoClaimsBonus,$privateCarNoClaimsBonus,$taxiPlatingAuthority,$taxiRegistration,$fullUKLicence,$taxiBadge,$taxiDrivers,$claimsLast5Years,$convictionsLast5Years,$title,$firstName,$surname,$telephoneNumber,$mobilePhone,$bestTimeToCall,$email,$postcode,$houseNumberOrName,$addressLine1,$streetName,$town,$county,$dateOfBirth,$dateOfInsuranceStart,$quoteRef,\n";
     
         $today       = date("Y-m-d-H:i:s");
         $csvFile     = "Quotezone-High-Gear-Taxi-3_".$quoteRef."_".$today.".csv";
         $csvFileName = "/tmp/$csvFile";

         $contentOfFile  = "";
         $contentOfFile .= $contentHeader;
         $contentOfFile .= $contentRows;

         $fh = fopen($csvFileName,"w+");
         fwrite($fh,$contentOfFile);
         fclose($fh);

         $ftpServer   = "ftp.highgear.co.uk";
         // $ftpUserName = "quotezone@highgear.co.uk";
         // $ftpUserPass = "!VywXB5NXJll";

         $ftpUserName = "qz2@highgear.co.uk";
         $ftpUserPass = "SU?OyD^xSL{X";

            //FTP & explicit FTPS port: 21

         $file = $csvFileName;

         $remoteFile = $csvFile;

         $ftpError = "";

         // set up basic connection
         $connId = ftp_connect($ftpServer);
      
         if(! $connId)
            $ftpError .= "<br>ftp_connect(".$ftpServer.") function return false<br>";

         // login with username and password
         $loginResult = ftp_login($connId, $ftpUserName, $ftpUserPass);

         if(! $loginResult)
            $ftpError .= "<br>ftp_login() function return false (".$ftpUserName."@".$ftpUserPass.")<br>";

         // turn passive mode on
         if( ! ftp_pasv($connId, true))
            $ftpError .= "<br>ftp_pasv() function return false <br>";

			
		// Initiate the Upload
		// upload a file
         if (ftp_put($connId, $remoteFile, $file, FTP_BINARY))
         {
            $result = "successfully uploaded $file\n";

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 1);
         }
         else
         {
            $ftpError .= "<br>ftp_put($connId, $remoteFile, $file, FTP_BINARY) function return false <br>";

            $result = "There was a problem while uploading $file\n";

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 2);
         }

         // close the connection
         ftp_close($connId);

         @unlink($csvFileName);
         
          

         //seopa
         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi - High Gear 3 FTP";
         $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
         $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers, $this->session,"CSV",$this->siteID);
         
         //eb2
         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi - High Gear 3 FTP";
         $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
         $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers, $this->session,"CSV",$this->siteID);


         $this->DoOutboundFiles("ftp", $contentOfFile,$result."<br>".$ftpError);
      }
      
      if(($this->session["_COMPANY_DETAILS_"]['10190']["REQUEST_CSV"] == 1) AND ($this->siteID == 10190) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {
         //error_reporting(E_ALL);
         
         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/plain; charset=iso-8859-1'."\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         
         $contentHeader = "Quote Date,Quote Time,Taxi Used For,Uber Driver,Taxi Type,Type Of Cover,Taxi Make,Taxi Model,Year Of Manufacture,Vehicle Mileage,Estimated Value,Taxi Capacity,Taxi No Claims Bonus,Private Car No Claims Bonus,Taxi Plating Authority,Taxi Registration,Full UK Licence,Taxi Badge,Taxi Driver(s),Claims Last 5 Years,Convictions Last 5 Years,Title,First Name,Surname,Telephone,Mobile Phone,Best Time To Call,Email Address,Postcode,House Number/Name,Address Line 1,Street Name,Town/City,County,Date Of Birth,Date Of Insurance Start,Quote Reference,\n";

                       
         $quoteDate                 = $this->session['_YourDetails_']['start_date'];  
         $quoteTime                 = date("H:i:s"); 
         $taxiUsedFor               = $_YourDetails["taxi_used_for"][$this->session['_YourDetails_']['taxi_used_for']];
         $uberDriver                = $_YourDetails["uber_driver"][$this->session['_YourDetails_']['uber_driver']];
         $taxiType                  = $_YourDetails["taxi_type"][$this->session['_YourDetails_']['taxi_type']];
         $typeOfCover               = $_YourDetails["type_of_cover"][$this->session['_YourDetails_']['type_of_cover']];
         $taxiMake                  = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel                 = $this->session['_YourDetails_']['taxi_model'];
         $yearOfManufacture         = $this->session['_YourDetails_']['year_of_manufacture'];
         $vehicleMileage            = $_YourDetails['vehicle_mileage'][$this->session['_YourDetails_']['vehicle_mileage']];
         $vehicleMileage            = str_replace(",","",$vehicleMileage);
         $estimatedValue            = $this->session['_YourDetails_']['estimated_value'];
         $taxiCapacity              = $_YourDetails["taxi_capacity"][$this->session['_YourDetails_']['taxi_capacity']];
         $taxiNoClaimsBonus         = $_YourDetails["taxi_ncb"][$this->session['_YourDetails_']['taxi_ncb']];
         $privateCarNoClaimsBonus   = $_YourDetails["private_car_ncb"][$this->session['_YourDetails_']['private_car_ncb']];
         $taxiPlatingAuthority      = $this->session['_YourDetails_']['plating_authority'];
         $taxiRegistration          = $this->session['_YourDetails_']['vehicle_registration_number'];
         $fullUKLicence             = $_YourDetails["period_of_licence"][$this->session['_YourDetails_']['period_of_licence']];
         $taxiBadge                 = $_YourDetails["taxi_badge"][$this->session['_YourDetails_']['taxi_badge']]; 
         $taxiDrivers               = $_YourDetails["taxi_driver"][$this->session['_YourDetails_']['taxi_driver']];
         $claimsLast5Years          = $_YourDetails["claims_5_years"][$this->session['_YourDetails_']['claims_5_years']];
            $claimsLast5Years       = str_replace(",","",$claimsLast5Years);
         $convictionsLast5Years     = $_YourDetails["convictions_5_years"][$this->session['_YourDetails_']['convictions_5_years']];
         $title                     = $this->session['_YourDetails_']['title'];  
         $firstName                 = $this->session['_YourDetails_']['first_name'];
         $surname                   = $this->session['_YourDetails_']['surname'];  
         $telephoneNumber           = $this->session['_YourDetails_']['daytime_telephone']; 
         $mobilePhone               = $this->session['_YourDetails_']['mobile_telephone'];
         $bestTimeToCall            = $_YourDetails["best_time_call"][$this->session['_YourDetails_']['best_time_call']];
         $email                     = $this->session['_YourDetails_']['email_address'];
         $postcode                  = $this->session['_YourDetails_']['postcode'];  
         $dateOfBirth               = $this->session['_YourDetails_']['date_of_birth'];  
         $dateOfInsuranceStart      = $this->session['_YourDetails_']['start_date'];  
         $quoteRef                  = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];


         $contentRows = "";
         $contentRows = "$quoteDate,$quoteTime,$taxiUsedFor,$uberDriver,$taxiType,$typeOfCover,$taxiMake,$taxiModel,$yearOfManufacture, $vehicleMileage,$estimatedValue,$taxiCapacity,$taxiNoClaimsBonus,$privateCarNoClaimsBonus,$taxiPlatingAuthority,$taxiRegistration,$fullUKLicence,$taxiBadge,$taxiDrivers,$claimsLast5Years,$convictionsLast5Years,$title,$firstName,$surname,$telephoneNumber,$mobilePhone,$bestTimeToCall,$email,$postcode,$houseNumberOrName,$addressLine1,$streetName,$town,$county,$dateOfBirth,$dateOfInsuranceStart,$quoteRef,\n";
     
         $today       = date("Y-m-d-H:i:s");
         $csvFile     = "Quotezone-High-Gear-Taxi-4_".$quoteRef."_".$today.".csv";
         $csvFileName = "/tmp/$csvFile";

         $contentOfFile  = "";
         $contentOfFile .= $contentHeader;
         $contentOfFile .= $contentRows;

         $fh = fopen($csvFileName,"w+");
         fwrite($fh,$contentOfFile);
         fclose($fh);

         $ftpServer   = "ftp.highgear.co.uk";
         // $ftpUserName = "quotezone@highgear.co.uk";
         // $ftpUserPass = "!VywXB5NXJll";

         $ftpUserName = "qz2@highgear.co.uk";
         $ftpUserPass = "SU?OyD^xSL{X";

            //FTP & explicit FTPS port: 21

         $file = $csvFileName;

         $remoteFile = $csvFile;

         $ftpError = "";

         // set up basic connection
         $connId = ftp_connect($ftpServer);
      
         if(! $connId)
            $ftpError .= "<br>ftp_connect(".$ftpServer.") function return false<br>";

         // login with username and password
         $loginResult = ftp_login($connId, $ftpUserName, $ftpUserPass);

         if(! $loginResult)
            $ftpError .= "<br>ftp_login() function return false (".$ftpUserName."@".$ftpUserPass.")<br>";

         // turn passive mode on
         if( ! ftp_pasv($connId, true))
            $ftpError .= "<br>ftp_pasv() function return false <br>";

			
		// Initiate the Upload
		// upload a file
         if (ftp_put($connId, $remoteFile, $file, FTP_BINARY))
         {
            $result = "successfully uploaded $file\n";

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 1);
         }
         else
         {
            $ftpError .= "<br>ftp_put($connId, $remoteFile, $file, FTP_BINARY) function return false <br>";

            $result = "There was a problem while uploading $file\n";

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 2);
         }

         // close the connection
         ftp_close($connId);

         @unlink($csvFileName);
         
          

         //seopa
         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi - High Gear 4 FTP";
         $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
         $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers, $this->session,"CSV",$this->siteID);
         
         //eb2
         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi - High Gear 4 FTP";
         $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
         $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers, $this->session,"CSV",$this->siteID);


         $this->DoOutboundFiles("ftp", $contentOfFile,$result."<br>".$ftpError);
      }
      
      if(($this->session["_COMPANY_DETAILS_"]['10191']["REQUEST_CSV"] == 1) AND ($this->siteID == 10191) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {
         //error_reporting(E_ALL);
         
         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/plain; charset=iso-8859-1'."\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         
         $contentHeader = "Quote Date,Quote Time,Taxi Used For,Uber Driver,Taxi Type,Type Of Cover,Taxi Make,Taxi Model,Year Of Manufacture,Vehicle Mileage,Estimated Value,Taxi Capacity,Taxi No Claims Bonus,Private Car No Claims Bonus,Taxi Plating Authority,Taxi Registration,Full UK Licence,Taxi Badge,Taxi Driver(s),Claims Last 5 Years,Convictions Last 5 Years,Title,First Name,Surname,Telephone,Mobile Phone,Best Time To Call,Email Address,Postcode,House Number/Name,Address Line 1,Street Name,Town/City,County,Date Of Birth,Date Of Insurance Start,Quote Reference,\n";

                       
         $quoteDate                 = $this->session['_YourDetails_']['start_date'];  
         $quoteTime                 = date("H:i:s"); 
         $taxiUsedFor               = $_YourDetails["taxi_used_for"][$this->session['_YourDetails_']['taxi_used_for']];
         $uberDriver                = $_YourDetails["uber_driver"][$this->session['_YourDetails_']['uber_driver']];
         $taxiType                  = $_YourDetails["taxi_type"][$this->session['_YourDetails_']['taxi_type']];
         $typeOfCover               = $_YourDetails["type_of_cover"][$this->session['_YourDetails_']['type_of_cover']];
         $taxiMake                  = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel                 = $this->session['_YourDetails_']['taxi_model'];
         $yearOfManufacture         = $this->session['_YourDetails_']['year_of_manufacture'];
         $vehicleMileage            = $_YourDetails['vehicle_mileage'][$this->session['_YourDetails_']['vehicle_mileage']];
         $vehicleMileage            = str_replace(",","",$vehicleMileage);
            $estimatedValue            = $this->session['_YourDetails_']['estimated_value'];
         $taxiCapacity              = $_YourDetails["taxi_capacity"][$this->session['_YourDetails_']['taxi_capacity']];
         $taxiNoClaimsBonus         = $_YourDetails["taxi_ncb"][$this->session['_YourDetails_']['taxi_ncb']];
         $privateCarNoClaimsBonus   = $_YourDetails["private_car_ncb"][$this->session['_YourDetails_']['private_car_ncb']];
         $taxiPlatingAuthority      = $this->session['_YourDetails_']['plating_authority'];
         $taxiRegistration          = $this->session['_YourDetails_']['vehicle_registration_number'];
         $fullUKLicence             = $_YourDetails["period_of_licence"][$this->session['_YourDetails_']['period_of_licence']];
         $taxiBadge                 = $_YourDetails["taxi_badge"][$this->session['_YourDetails_']['taxi_badge']]; 
         $taxiDrivers               = $_YourDetails["taxi_driver"][$this->session['_YourDetails_']['taxi_driver']];
         $claimsLast5Years          = $_YourDetails["claims_5_years"][$this->session['_YourDetails_']['claims_5_years']];
            $claimsLast5Years       = str_replace(",","",$claimsLast5Years);
         $convictionsLast5Years     = $_YourDetails["convictions_5_years"][$this->session['_YourDetails_']['convictions_5_years']];
         $title                     = $this->session['_YourDetails_']['title'];  
         $firstName                 = $this->session['_YourDetails_']['first_name'];
         $surname                   = $this->session['_YourDetails_']['surname'];  
         $telephoneNumber           = $this->session['_YourDetails_']['daytime_telephone']; 
         $mobilePhone               = $this->session['_YourDetails_']['mobile_telephone'];
         $bestTimeToCall            = $_YourDetails["best_time_call"][$this->session['_YourDetails_']['best_time_call']];
         $email                     = $this->session['_YourDetails_']['email_address'];
         $postcode                  = $this->session['_YourDetails_']['postcode'];  
         $dateOfBirth               = $this->session['_YourDetails_']['date_of_birth'];  
         $dateOfInsuranceStart      = $this->session['_YourDetails_']['start_date'];  
         $quoteRef                  = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];


         $contentRows = "";
         $contentRows = "$quoteDate,$quoteTime,$taxiUsedFor,$uberDriver,$taxiType,$typeOfCover,$taxiMake,$taxiModel,$yearOfManufacture,$vehicleMileage,$estimatedValue,$taxiCapacity,$taxiNoClaimsBonus,$privateCarNoClaimsBonus,$taxiPlatingAuthority,$taxiRegistration,$fullUKLicence,$taxiBadge,$taxiDrivers,$claimsLast5Years,$convictionsLast5Years,$title,$firstName,$surname,$telephoneNumber,$mobilePhone,$bestTimeToCall,$email,$postcode,$houseNumberOrName,$addressLine1,$streetName,$town,$county,$dateOfBirth,$dateOfInsuranceStart,$quoteRef,\n";
     
         $today       = date("Y-m-d-H:i:s");
         $csvFile     = "Quotezone-High-Gear-Taxi-5-".$today.".csv";
         $csvFileName = "/tmp/$csvFile";

         $contentOfFile  = "";
         $contentOfFile .= $contentHeader;
         $contentOfFile .= $contentRows;

         $fh = fopen($csvFileName,"w+");
         fwrite($fh,$contentOfFile);
         fclose($fh);

         $ftpServer   = "ftp.highgear.co.uk";
         $ftpUserName = "quotezone@highgear.co.uk";
         $ftpUserPass = "!VywXB5NXJll";

            //FTP & explicit FTPS port: 21

         $file = $csvFileName;

         $remoteFile = $csvFile;

         $ftpError = "";

         // set up basic connection
         $connId = ftp_connect($ftpServer);
      
         if(! $connId)
            $ftpError .= "<br>ftp_connect(".$ftpServer.") function return false<br>";

         // login with username and password
         $loginResult = ftp_login($connId, $ftpUserName, $ftpUserPass);

         if(! $loginResult)
            $ftpError .= "<br>ftp_login() function return false (".$ftpUserName."@".$ftpUserPass.")<br>";

         // turn passive mode on
         if( ! ftp_pasv($connId, true))
            $ftpError .= "<br>ftp_pasv() function return false <br>";

			
		// Initiate the Upload
		// upload a file
         if (ftp_put($connId, $remoteFile, $file, FTP_BINARY))
         {
            $result = "successfully uploaded $file\n";

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 1);
         }
         else
         {
            $ftpError .= "<br>ftp_put($connId, $remoteFile, $file, FTP_BINARY) function return false <br>";

            $result = "There was a problem while uploading $file\n";

            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'sent_status', 1);
            $outboundObj->UpdateOutboundField($this->outbounds['CSV'],'received_status', 2);
         }

         // close the connection
         ftp_close($connId);

         @unlink($csvFileName);
         
          

         //seopa
         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi - High Gear 5 FTP";
         $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
         $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers, $this->session,"CSV",$this->siteID);
         
         //eb2
         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi - High Gear 5 FTP";
         $mailBody    = "Content : \n".$contentOfFile."\nRESULT : ".$result."\n\n ERROR : ".$ftpError."\n\n";
         $mailObj->SendMail("taxi@quotezone.co.uk", $mailTo, $mailSubject, $mailBody, $headers, $this->session,"CSV",$this->siteID);


         $this->DoOutboundFiles("ftp", $contentOfFile,$result."<br>".$ftpError);
      }

   } //end if FTP METHOD 


   if($this->outbounds['URL'])
   {
      if((($this->session["_COMPANY_DETAILS_"]['12434']["REQUEST_URL"] == 1) AND ($this->siteID == 12434) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['2942']["REQUEST_URL"] == 1) AND ($this->siteID == 2942) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) ) 
      // if((($this->session["_COMPANY_DETAILS_"]['12434']["REQUEST_URL"] == 1) AND ($this->siteID == 12434))|| (($this->session["_COMPANY_DETAILS_"]['2942']["REQUEST_URL"] == 1) AND ($this->siteID == 2942)))
       {
         $licenceObtainedArr      = array("0"=>"0", "1"=>"0", "2"=>"1", "3"=>"2", "4"=>"3", "5"=>"4", "6"=>"5", "7"=>"6", "8"=>"7", "9"=>"8", "10"=>"9", "11"=>"10", "12"=>"11",);
         $licenceObtained      = ($doqYYYY-($licenceObtainedArr[$this->session['_YourDetails_']['period_of_licence']]))."-".$doqMM."-".$doqDD."T00:00:00";
         $titleIdsArr          = array('Mr' => '003', 'Mrs' => '004', 'Ms' => '005', 'Miss' => '002', );
         $titleId              = $titleIdsArr[$title];
         $telephoneTypeIDNo    = str_split($daytimeTelephone,2);
         $telephoneTypeID      = $telephoneTypeIDNo[0] == "07"?"3AJPQ7C6":"3AJPQ7C4";
         $mobileTypeIDNo       = str_split($mobileTelephone,2);
         $mobileTypeID         = $mobileTypeIDNo[0] == "07"?"3AJPQ7C6":"3AJPQ7C4";         
         $typeOfCoverArr = array("1"=>"1","2"=>"2","3"=>"3","4"=>"3",);

         if(preg_match("/P/isU", $engineSize, $matches))
            $fuelTypeID = $matches[0];
         if(preg_match("/D/isU", $engineSize, $matches))
            $fuelTypeID = $matches[0];

         if($fuelTypeID == "P")
            $fuelTypeID = "002";

         if($fuelTypeID == "D")
            $fuelTypeID = "001";

         // gearboxTypeID   Automatic (A) = 001  Manual (M) = 002           

         if($gearboxType == "A")
            $gearboxTypeID = "001";

         if($gearboxType == "M")
            $gearboxTypeID = "002";
       
      $engineSizeArr = explode(" ", $engineSize);
      $engineSizeCC  = $engineSizeArr[0];

         $xmlData = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <soap:Body>
      <Quote xmlns="http://TGSL/TES_Aggregator">
         <lxmlTGSLXML>
            <tgsl xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.transactorgsl.com/schemas http://localhost/TES_Aggregator/schemas/tgsl.xsd" xmlns="http://www.transactorgsl.com/schemas">
               <aggregator>quotezone-leads-taxi</aggregator>
               <domain>http://localhost</domain>
               <partner>
                  <partnerName>adelphi</partnerName>
                  <broker>adelphi</broker>
               </partner>
               <product>cv</product>
            </tgsl>
         </lxmlTGSLXML>
         <lstrRiskXML>
            <cvRisk xmlns="http://www.transactorgsl.com/schemas" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.transactorgsl.com/schemas http://localhost/TES_Aggregator/Schemas/TGSL/TGSLCVSchema.xsd">
               <insuredParty instance="1">
                  <accessToOtherVehicles>false</accessToOtherVehicles>
                  <client>true</client>
                  <dateLicenceObtained>'.$licenceObtained.'</dateLicenceObtained>
                  <dob>'.$dobYYYY.'-'.$dobMM.'-'.$dobDD.'T00:00:00</dob>
                  <email>'.$email.'</email>
                  <forename>'.$firstName.'</forename>
                  <licenceTypeID>F</licenceTypeID>
                  <numberOfVehiclesAccessTo>0</numberOfVehiclesAccessTo>
                  <relationshipID>P</relationshipID>
                  <surname>'.$lastName.'</surname>
                  <titleID>'.$titleId.'</titleID>
               </insuredParty>
               <correspondanceAddress instance="1">
                  <city>'.$town.'</city>
                  <country />
                  <county/>
                  <house>'.$houseNumberOrName.'</house>
                  <locality/>
                  <postcode>'.$postcode.'</postcode>
                  <street>'.$streetName.'</street>
               </correspondanceAddress>
               <occupation instance="1" insuredPartyInstance="1">
                  <employersBusinessID>703</employersBusinessID>
                  <employmentStatusID>E</employmentStatusID>
                  <occupationID>D34</occupationID>
                  <parttime>false</parttime>
                  <primaryOccupation>true</primaryOccupation>
               </occupation>
               <telephone instance="1">
                  <telephoneNumber>'.$daytimeTelephone.'</telephoneNumber>
                  <telephoneTypeID>'.$telephoneTypeID.'</telephoneTypeID>
               </telephone>';
               if($mobileTelephone)
               {
                  $xmlData .= '<telephone instance="2">
                    <telephoneNumber>'.$mobileTelephone.'</telephoneNumber>
                    <telephoneTypeID>'.$mobileTypeID.'</telephoneTypeID>
                  </telephone>';
               }
   $xmlData .= '<cover instance="1">
                  <coverRequiredID>0'.$typeOfCoverArr[$this->session["_YourDetails_"]["type_of_cover"]].'</coverRequiredID>
                  <coverStartDate>'.$disYYYY.'-'.$disMM.'-'.$disDD.'T00:00:00</coverStartDate>
               </cover>
               <vehicle instance="1">
                  <abiCode>'.$abiCode.'</abiCode>
                  <currentMileage>0</currentMileage>
                  <fuelTypeID>'.$fuelTypeID.'</fuelTypeID>
                  <gearboxTypeID>'.$gearboxTypeID.'</gearboxTypeID>';
                  if($registrationNumber)
                  {
                     $xmlData .= '<registrationNumber>'.str_replace(" ","",$registrationNumber).'</registrationNumber>';
                  }

   $xmlData .= '<value>'.$estimatedValue.'.00</value>
                  <vehicleCC>'.$engineSizeCC.'</vehicleCC>
                  <vehicleMake>'.$taxiMake.'</vehicleMake>
                  <vehicleMark>S 7 STR</vehicleMark>
                  <vehicleModel>'.$taxiModel.'</vehicleModel>
                  <yearOfManufacture>'.$yearOfManufacture.'</yearOfManufacture>
                  <carriesGoods>false</carriesGoods>
                  <goodsCarriedID>0</goodsCarriedID>
               </vehicle>
               <ncd instance="1">
                  <ncdYears>'.$this->session["_YourDetails_"]["taxi_ncb"].'</ncdYears>
               </ncd>
            </cvRisk>
         </lstrRiskXML>
      </Quote>
   </soap:Body>
</soap:Envelope>';
         
         // $url = "https://drs.adelphiquote.co.uk/TES_Aggregator/Aggregator.asmx";
         $url = "https://adelphi-drs.i-wonder.hosting/TES_Aggregator/Aggregator.asmx";

         $header[] = "POST /TES_Aggregator/Aggregator.asmx HTTP/1.1";
         $header[] = "Host: adelphi-drs.i-wonder.hosting";
         $header[] = "Content-Type: text/xml; charset=utf-8";
         $header[] = "Content-length: ".strlen($xmlData);
         $header[] = "SOAPAction: http://TGSL/TES_Aggregator/Quote";
         $header[] = "Connection: close \r\n";
         $header[] = $xmlData;

         $ch = curl_init();

         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
         curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 15);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_HEADER, 1);

         $result = curl_exec($ch);

         curl_close ($ch);

         $xmlMessageBody = prepareXMLMessageBody($xmlData, $result);
         
         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <courier@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <courier@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <courier@quotezone.co.uk>\r\n";

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject. " Adelphi Taxi id$this->siteID - URL Method";
         $mailBody    = $xmlMessageBody;         
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject. " Adelphi Taxi id$this->siteID - URL Method ";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);

         $outboundObj->UpdateOutboundField($this->outbounds['URL'],'sent_status', 1);

         if(! strlen($result))
             $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 3); //NO RESPONSE
           if( preg_match("/\<status\>Lead Saved\<\/status\>/i",$result))
             $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 1); //OK RESPONSE
           else
             $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 2);//BAD RESPONSE
      
           $this->DoOutboundFiles("url", $xmlString, $result);
      }
      // +-------+-------------------------------------------------------+--------+
      // | id    | name                                                  | status |
      // +-------+-------------------------------------------------------+--------+
      // |  2161 | County Insurance (Southport) - Taxi (Brady & Jones)   | ON     |
      // |  2714 | County Insurance (Southport) - Taxi (Worthing (0 NCB) | ON     |
      // | 10419 | County Insurance (Southport) - Taxi (Cardiff)         | ON     |
      // | 10711 | County Insurance (Southport) - Taxi                   | ON     |
      // | 11191 | County Insurance (Southport) - Taxi (Worthing)        | ON     |
      // | 11296 | County Insurance (Southport) - Taxi (Liverpool)       | ON     |
      // | 11602 | County Insurance (Worthing) - Taxi (IOM)              | ON     |
      // | 11727 | County Insurance - Taxi (Taxi Choice Heckmondwike)    | ON     |
      // | 11728 | County Insurance - Taxi (Taxi Choice Heckmondwike 2)  | ON     |
      // +-------+-------------------------------------------------------+--------+
      if((($this->session["_COMPANY_DETAILS_"]['2161']["REQUEST_URL"] == 1) AND ($this->siteID == 2161) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['2714']["REQUEST_URL"] == 1) AND ($this->siteID == 2714) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['10419']["REQUEST_URL"] == 1) AND ($this->siteID == 10419) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['10711']["REQUEST_URL"] == 1) AND ($this->siteID == 10711) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['11191']["REQUEST_URL"] == 1) AND ($this->siteID == 11191) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['11296']["REQUEST_URL"] == 1) AND ($this->siteID == 11296) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['11602']["REQUEST_URL"] == 1) AND ($this->siteID == 11602) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['11727']["REQUEST_URL"] == 1) AND ($this->siteID == 11727) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) || (($this->session["_COMPANY_DETAILS_"]['11728']["REQUEST_URL"] == 1) AND ($this->siteID == 11728) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")) )
      //if((($this->session["_COMPANY_DETAILS_"]['2161']["REQUEST_URL"] == 1) AND ($this->siteID == 2161)) ||          (($this->session["_COMPANY_DETAILS_"]['2714']["REQUEST_URL"] == 1) AND ($this->siteID == 2714)) ||          (($this->session["_COMPANY_DETAILS_"]['10419']["REQUEST_URL"] == 1) AND ($this->siteID == 10419)) ||          (($this->session["_COMPANY_DETAILS_"]['10711']["REQUEST_URL"] == 1) AND ($this->siteID == 10711)) ||          (($this->session["_COMPANY_DETAILS_"]['11191']["REQUEST_URL"] == 1) AND ($this->siteID == 11191)) ||          (($this->session["_COMPANY_DETAILS_"]['11296']["REQUEST_URL"] == 1) AND ($this->siteID == 11296)) ||          (($this->session["_COMPANY_DETAILS_"]['11602']["REQUEST_URL"] == 1) AND ($this->siteID == 11602)) ||          (($this->session["_COMPANY_DETAILS_"]['11727']["REQUEST_URL"] == 1) AND ($this->siteID == 11727)) ||          (($this->session["_COMPANY_DETAILS_"]['11728']["REQUEST_URL"] == 1) AND ($this->siteID == 11728)) )
      {
         //lead data
         $title               = $this->session['_YourDetails_']['title'];
         $firstName           = $this->session['_YourDetails_']['first_name'];
         $sureName            = $this->session['_YourDetails_']['surname'];
         $name                = $firstName." ".$sureName;
         $dob                 = $this->session['_YourDetails_']['date_of_birth_dd']."/".$this->session['_YourDetails_']['date_of_birth_mm']."/".$this->session['_YourDetails_']['date_of_birth_yyyy'];
         $phone               = $this->session['_YourDetails_']['daytime_telephone'];
         $mobile              = $this->session['_YourDetails_']['mobile_telephone'];
         $bestTimeToCall      = $_YourDetails['best_time_call'][$this->session['_YourDetails_']['best_time_call']];
         $emailAddress        = $this->session['_YourDetails_']['email_address'];
         $houseNameOrNumber   = urldecode($this->session['_YourDetails_']['house_number_or_name']);
         $streetName          = $this->session['_YourDetails_']['address_line2'];
         $town                = $this->session['_YourDetails_']['address_line3'];
         $county              = $this->session['_YourDetails_']['address_line4'];
         $postcode            = $this->session['_YourDetails_']['postcode'];
         $policyStartDate     = $this->session['_YourDetails_']['date_of_insurance_start_dd']."/".$this->session['_YourDetails_']['date_of_insurance_start_mm']."/".$this->session['_YourDetails_']['date_of_insurance_start_yyyy'];

         $taxiUsedFor               = $_YourDetails["taxi_used_for"][$this->session["_YourDetails_"]["taxi_used_for"]];
         $uberDriver                = $_YourDetails["uber_driver"][$this->session["_YourDetails_"]["uber_driver"]];
         $taxiType                  = $_YourDetails["taxi_type"][$this->session["_YourDetails_"]["taxi_type"]];
         $typeOfCover               = $_YourDetails["type_of_cover"][$this->session["_YourDetails_"]["type_of_cover"]];
         $taxiMake                  = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel                 = $this->session['_YourDetails_']['taxi_model'];
         $yearOfManufacture         = $this->session['_YourDetails_']['year_of_manufacture'];
         $estimatedValue            = $this->session['_YourDetails_']['estimated_value'];
         $maximumNumberOfPassengers = $_YourDetails["taxi_capacity"][$this->session["_YourDetails_"]["taxi_capacity"]];
         $taxiNoClaimsBonus         = $_YourDetails["taxi_ncb"][$this->session["_YourDetails_"]["taxi_ncb"]];
         $privateCarNoClaimsBonus   = $_YourDetails["private_car_ncb"][$this->session["_YourDetails_"]["private_car_ncb"]];
         $taxiLicensingAuthority    = $this->session['_YourDetails_']['plating_authority'];
         $taxiRegistration          = $this->session['_YourDetails_']['vehicle_registration_number'];
         $fullUkLicence             = $_YourDetails["period_of_licence"][$this->session["_YourDetails_"]["period_of_licence"]];
         $taxiBadge                 = $_YourDetails["taxi_badge"][$this->session["_YourDetails_"]["taxi_badge"]];
         $taxiDriver                = $_YourDetails["taxi_driver"][$this->session["_YourDetails_"]["taxi_driver"]];
         $claimsLast5Years          = $_YourDetails["claims_5_years"][$this->session["_YourDetails_"]["claims_5_years"]];
         $convictionsLast5Years     = $_YourDetails["convictions_5_years"][$this->session["_YourDetails_"]["convictions_5_years"]];       
         
         $quoteRef                  = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];

         //consent details - for not details are not on the system
         if(isset($this->session['consent_statements_values']['companyMarketing']['Email']))
         {
            $emailConsent = "No"; 
            if($this->session['consent_statements_values']['companyMarketing']['Email'] == 1)
               $emailConsent = "Yes";
         }
         else
            $emailConsent = "Yes"; //update to No once consent added on the system   

         if(isset($this->session['consent_statements_values']['companyMarketing']['SMS']))
         {
            $smsConsent = "No"; //update to No once consent added on the system
            if($this->session['consent_statements_values']['companyMarketing']['SMS'] == 1)
               $smsConsent = "Yes";
         }
         else
            $smsConsent = "Yes";   

         // $url                 = "https://api.countyins.com/api/v1/";
         $url                 = "https://api.countyins.com/api/v1/lead_u.php";


         $leadArray     = array(      
                                 'address'      => $houseNameOrNumber,
                                 'email'        => $emailAddress,
                                 'lead_ref'     => $quoteRef,
                                 'name'         => $name,
                                 'other_fields' => array(
                                                'TAXI USED FOR'                => $taxiUsedFor,
                                                'UBER DRIVER'                  => $uberDriver,
                                                'TAXI TYPE'                    => $taxiType,
                                                'TYPE OF COVER'                => $typeOfCover,
                                                'TAXI MAKE'                    => $taxiMake,
                                                'TAXI MODEL'                   => $taxiModel,
                                                'YEAR OF MANUFACTURE'          => $yearOfManufacture,
                                                'ESTIMATED VALUE'              => $estimatedValue,
                                                'MAXIMUM NUMBER OF PASSENGERS' => $maximumNumberOfPassengers,
                                                'TAXI NO CLAIMS BONUS'         => $taxiNoClaimsBonus,
                                                'PRIVATE CAR NO CLAIMS BONUS'  => $privateCarNoClaimsBonus,
                                                'TAXI LICENSING AUTHORITY'     => $taxiLicensingAuthority,
                                                'TAXI REGISTRATION'            => $taxiRegistration,
                                                'FULL UK LICENCE'              => $fullUkLicence,
                                                'TAXI BADGE'                   => $taxiBadge,
                                                'TAXI DRIVER(S)'               => $taxiDriver,
                                                'CLAIMS LAST 5 YEARS'          => $claimsLast5Years,
                                                'CONVICTIONS LAST 5 YEARS'     => $convictionsLast5Years,
                                                'DATE OF BIRTH'                => $dob,
                                                'DATE OF INSURANCE START'      => $policyStartDate,
                                                'EMAIL'                        => $emailConsent,
                                                'SMS'                          => $smsConsent
                                                ),             
                                 'phone_landline' => $phone,
                                 'phone_mobile'   => $mobile,               
                                 'postcode'       => $postcode,
                              );
         //encode to json
         $authorizationArr = array( "10419" => "z74SKPfs15AB1059MF23H6Yh35eFlz94",
                                    "10711" => "KEMD23aBiOZ8Y4623N8541HrOdY16Ta1",
                                    "11191" => "hYhY21A1Ye1uNSM84824sR24xY76gs1N",
                                    "11602" => "hYhY21A1Ye1uNSM84824sR24xY76gs1N",
                                    "11728" => "am1lyCxOds1S9T2XoYRTZ84aN4Ehrx50",
                                    "11727" => "am1lyCxOds1S9T2XoYRTZ84aN4Ehrx50",
                                    "11296" => "g73Nid5e9Oq1v2vjzynO7Ou13S53uPM7",
                                    "2714"  => "5fRC4iqpq486U9R3uPC3VNUnZ382ObtI",
                                    "2161"  => "W07kW0ABjTf800Degz5Khyu4ijZg0on1",
                                    );
         $authorizationKey = $authorizationArr[$this->siteID];
         $postParams = json_encode($leadArray);

         $ch = curl_init();
         curl_setopt($ch,CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , 'Authorization: Bearer '.$authorizationKey));
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_POSTFIELDS,$postParams);
         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         $result = curl_exec($ch);
         // $result = json_decode($result);
         $response      = "\r\n >>> SENT PARAMS: ".$postParams."\r\n >>> HEADERS: ".$headers."\r\n >>> CURL GET INFO: ".var_export(curl_getinfo($ch),true)."\r\n >>> CURL ERROR: ".curl_error($ch)."\r\n >>>RESULT: ".$result;
         curl_close($ch);

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <coach@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <coach@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <coach@quotezone.co.uk>\r\n";


         if($this->siteID == 2161)
            $mailSubject = $subject." County Insurance (Southport) id2161 - Taxi (Brady & Jones)";
         else if($this->siteID == 2714)
            $mailSubject = $subject." County Insurance (Southport) id2714 - Taxi (Worthing (0 NCB)";
         else if($this->siteID == 10419)
            $mailSubject = $subject." County Insurance (Southport) id10419 - Taxi (Cardiff)";
         else if($this->siteID == 10711)
            $mailSubject = $subject." County Insurance (Southport) id10711 - Taxi";
         else if($this->siteID == 11191)
            $mailSubject = $subject." County Insurance (Southport) id11191 - Taxi (Worthing)";
         else if($this->siteID == 11296)
            $mailSubject = $subject." County Insurance (Southport) id11296 - Taxi (Liverpool)";
         else if($this->siteID == 11602)
            $mailSubject = $subject." County Insurance (Worthing) id11602 - Taxi (IOM)";
         else if($this->siteID == 11727)
            $mailSubject = $subject." County Insurance id11727 - Taxi (Taxi Choice Heckmondwike)";
         else if($this->siteID == 11728)
            $mailSubject = $subject." County Insurance id11728 - Taxi (Taxi Choice Heckmondwike 2)";


         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         // $mailSubject = $subject." County Insurance (Fleet Crewe) id10330 - Business Fleet (3-5 Vehicles)";
         $mailBody    = var_export($response,true)."\r\n $this->siteID $authorizationKey \r\n";
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);
         
         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         // $mailSubject = $subject." County Insurance (Fleet Crewe) id10330 - Business Fleet (3-5 Vehicles)";
         $mailBody    = var_export($response,true)."\r\n";
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);

         $outboundObj->UpdateOutboundField($this->outbounds['URL'],'sent_status', 1);

         if(! strlen($response['http_code']))
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 3); //NO RESPONSE

         if( preg_match("/\"status\":\"OK\"/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 1); //OK RESPONSE
         else
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 2);//BAD RESPONSE

         $this->DoOutboundFiles("url", $leadArray, $response);
      }  

      if(($this->session["_COMPANY_DETAILS_"]['1798']["REQUEST_URL"] == 1) AND ($this->siteID == 1798) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {
         $daytime_telephone    = $this->session['_YourDetails_']['daytime_telephone'];
         $mobile_telephone     = $this->session['_YourDetails_']['mobile_telephone'];

         $dest = $daytime_telephone?$daytime_telephone:$mobile_telephone;

         $title                = $this->session['_YourDetails_']['title'];
         $firstName            = $this->session['_YourDetails_']['first_name'];
         $lastName             = $this->session['_YourDetails_']['surname'];
         $dateOfBirth          = $this->session['_YourDetails_']['date_of_birth_dd'].$this->session['_YourDetails_']['date_of_birth_mm'].$this->session['_YourDetails_']['date_of_birth_yyyy'];
         $emailAddress         = $this->session['_YourDetails_']['email_address'];
         $postCode             = $this->session['_YourDetails_']['postcode_prefix'].$this->session['_YourDetails_']['postcode_number'];
         $dateOfInsuranceStart = $this->session['_YourDetails_']['date_of_insurance_start_mm'].$this->session['_YourDetails_']['date_of_insurance_start_dd'].$this->session['_YourDetails_']['date_of_insurance_start_yyyy'];
         $taxiRegistration     = $this->session['_YourDetails_']['vehicle_registration_number'];
         $taxiMake             = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel            = $this->session['_YourDetails_']['taxi_model'];
         $estimatedValue       = $this->session['_YourDetails_']['estimated_value'];
         $yearOfManufactures   = $this->session['_YourDetails_']['year_of_manufacture'];
         $vehicleMileage       = $this->session['_YourDetails_']['vehicle_mileage'];
         $taxiType             = $this->session['_YourDetails_']['taxi_type'];
         $taxiuse              = $this->session['_YourDetails_']['taxi_used_for'];
         $numberOfPasegers     = $this->session['_YourDetails_']['taxi_capacity'];
         $platingAuthority     = $this->session['_YourDetails_']['plating_authority'];
         $typeOfCover          = $this->session['_YourDetails_']['type_of_cover'];
         $noClaimBonus         = $this->session['_YourDetails_']['taxi_ncb'];
         $fullLicence          = $this->session['_YourDetails_']['period_of_licence'];
         $taxiBadge            = $this->session['_YourDetails_']['taxi_badge'];
         $claimsLastYears      = $this->session['_YourDetails_']['claims_5_years'];
         $convictionsLastYears = $this->session['_YourDetails_']['convictions_5_years'];
         $houseNrOrName        = $this->session['_YourDetails_']['house_number_or_name'];
         $engineSize           = $this->session['_YourDetails_']['engine_size'];

         $dateAndTime          = date("Y/m/d H:i:s");

         //new params
         $params  = "?login=insctaxi";
         $params .= "&password=insctaxi";
         $params .= "&destday=".urlencode($daytime_telephone);
         $params .= "&desteve=".urlencode($mobile_telephone);
         $params .= "&info_a._title=".urlencode($title);
         $params .= "&info_b._first_name=".urlencode($firstName);
         $params .= "&info_c._surname=".urlencode($lastName);
         $params .= "&info_d._DOB=".urlencode($dateOfBirth);
         $params .= "&info_e._email_address=".urlencode($emailAddress);
   //      $params .= "&info_x._address=".urlencode($houseNrOrName);
         $params .= "&info_f._postcode=".urlencode($postCode);
         $params .= "&info_g._insurance_start_date=".urlencode($dateOfInsuranceStart);
         $params .= "&info_h._taxi_registration=".urlencode($taxiRegistration);
         $params .= "&info_i._taxi_make=".urlencode($taxiMake);
         $params .= "&info_j._taxi_model=".urlencode($taxiModel);
         $params .= "&info_k._estimated_taxi_value=".urlencode($estimatedValue);
         $params .= "&info_l._year_of_manufacture=".urlencode($yearOfManufactures);
         $params .= "&info_m._vehicle_mileage=".urlencode($vehicleMileage);
         $params .= "&info_n._taxi_type=".urlencode($taxiType);
         $params .= "&info_o._taxi_use".urlencode($taxiuse);
         $params .= "&info_p._max_number_of_passengers=".urlencode($numberOfPasegers);
         $params .= "&info_q._taxi_plating_authority=".urlencode($platingAuthority);
         $params .= "&info_r._type_of_cover=".urlencode($typeOfCover);
         $params .= "&info_s._taxi_no_claims_bonus=".urlencode($noClaimBonus);
         $params .= "&info_t._full_uk_licence=".urlencode($fullLicence);
         $params .= "&info_u._taxi_badge".urlencode($taxiBadge);
         $params .= "&info_v._claims_last_5_years=".urlencode($claimsLastYears);
         $params .= "&info_w._convictions_last_5_years=".urlencode($convictionsLastYears);
         $params .= "&info_x._engine_size=".urlencode($engineSize);

         $url      = "https://secure.optilead.co.uk/followup/";
         $leadcall = $url.$params;

         //Get
         $ch = curl_init();

         curl_setopt($ch, CURLOPT_URL, $leadcall);
         curl_setopt($ch, CURLOPT_TIMEOUT, 30);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         $result = curl_exec($ch);
         curl_close($ch);

				$printParams = str_replace("?login=insctaxi&password=insctaxi","",$params);
				$printParams = str_replace("&","\r\n&",$printParams);
				
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
            //$headers .= "From: <Convicted Driver@quotezone.co.uk>\r\n";
            ////$headers .= "X-Sender: <Convicted Driver@quotezone.co.uk>\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!
            ////$headers .= "Return-Path: <Convicted Driver@quotezone.co.uk>\r\n";

            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Insurance Choice (Public Hire) LeadCall-Optilead Request";
            $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);
            
            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Insurance Choice (Public Hire) LeadCall-Optilead Request";
            $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);  
				
		         
         $outboundObj->UpdateOutboundField($this->outbounds['URL'],'sent_status', 1);

         if(! strlen($result))
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 3); //NO RESPONSE

         if(preg_match("/OK/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 1); //OK RESPONSE
         else
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 2);//BAD RESPONSE

         $this->DoOutboundFiles("url", $leadcall, $result);
      }//insurance choice 1 , 2 URL request  LEADCALL
   }//end LEAD CALL method
	
	
	// LEADCALL Method -- Insurance Choice - Taxi (Public Hire OOH) 
	if($this->outbounds['URL'])
   {
     // leadcall request Coversure Leyton - 11081
     if(($this->session["_COMPANY_DETAILS_"]['11081']["REQUEST_URL"] == 1) AND ($this->siteID == 11081) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
    // if(($this->session["_COMPANY_DETAILS_"]['11081']["REQUEST_URL"] == 1) AND ($this->siteID == 11081))
    {         
            $timeHh     = date("G");
            $timeMm     = date("i");
            $dateDdText = date("N");

            $timeFilter = "accept";
            switch($dateDdText)
            {
               case '1': // Mon
                  if($timeHh < 9)
                     $timeFilter = "skip";
                  break;
               case '2': // Tue
               case '3': // Wed
               case '4': // Thu
               case '5': // Fri
                 if($timeHh >= 17)
                  $timeFilter = "skip";
                  break;
               case '6': // Sat           
               case '7':  // Sun
                    $timeFilter = "skip";
                  break;
         }

         if($timeFilter == "accept")
         {   
            $daytime_telephone = $this->session['_YourDetails_']['daytime_telephone'];
    
            $systemType        = "Taxi";

            $params = "?clientid=8844&username=securedial&password=K200aaf139xxd";
            $params .= "&ddi=08888000699";
            $params .= "&destination=".urlencode($daytime_telephone);
            $params .= "&ref=888";
            $params .= "&dttm=";

            $ch = curl_init();

            $url    = "https://secure.telecomstats.co.uk/IVRAPI/Scripts/OpusDialler.aspx";

            $leadcall = $url.$params;

            //Get
            curl_setopt($ch, CURLOPT_URL, $leadcall);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($ch);
            curl_close($ch);

            print "\n========== result ===============\n";
            print $result;
            print "\n========== result ===============\n";

      
               $printParams = str_replace("?login=securedial&password=K200aaf139xxd","",$params);
               $printParams = str_replace("&","\r\n&",$printParams);
               
               $headers  = 'MIME-Version: 1.0' . "\r\n";
               $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
               //$headers .= "From: <motorhome@quotezone.co.uk>\r\n";
               ////$headers .= "X-Sender: <motorhome@quotezone.co.uk>\r\n";
               $headers .= "X-Mailer: PHP\r\n"; // mailer
               $headers .= "X-Priority: 1\r\n"; // Urgent message!
               ////$headers .= "Return-Path: <motorhome@quotezone.co.uk>\r\n";

               $mailTo      = "eb2-technical@seopa.com";
               $mailFrom    = "taxi@quotezone.co.uk";
               $mailSubject = $subject." Taxi Coversure Leyton";
               $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
               $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);
               
               $mailTo      = "leads@seopa.com";
               $mailFrom    = "taxi@quotezone.co.uk";
               $mailSubject = $subject." Taxi Coversure Leyton";
               $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
               $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);  
            
            

            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'sent_status', 1);

            if(preg_match("/OK/i",$result))
               $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 1);
            else
               $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 2);

            $sentContent =  "\nURL: ".$url."\nPARAMS:".$params;

            $this->DoOutboundFiles("url", $sentContent,$result);
         }
      }//end Leadcall Coversure Leyton - 11081

     // leadcall request Coversure Leyton - 11100
     if(($this->session["_COMPANY_DETAILS_"]['11100']["REQUEST_URL"] == 1) AND ($this->siteID == 11100) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
     //if(($this->session["_COMPANY_DETAILS_"]['11100']["REQUEST_URL"] == 1) AND ($this->siteID == 11100))
      {
         $daytime_telephone = $this->session['_YourDetails_']['daytime_telephone'];
 
         $systemType        = "Taxi";

         $params = "?clientid=8844&username=securedial&password=K200aaf139xxd";
         $params .= "&ddi=08888000699";
         $params .= "&destination=".urlencode($daytime_telephone);
         $params .= "&ref=888";
         $params .= "&dttm=";

         $ch = curl_init();

         $url    = "https://secure.telecomstats.co.uk/IVRAPI/Scripts/OpusDialler.aspx";

         $leadcall = $url.$params;

         //Get
         curl_setopt($ch, CURLOPT_URL, $leadcall);
         curl_setopt($ch, CURLOPT_TIMEOUT, 30);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         $result = curl_exec($ch);
         curl_close($ch);

         print "\n========== result ===============\n";
         print $result;
         print "\n========== result ===============\n";

   
            $printParams = str_replace("?login=securedial&password=K200aaf139xxd","",$params);
            $printParams = str_replace("&","\r\n&",$printParams);
            
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
            //$headers .= "From: <motorhome@quotezone.co.uk>\r\n";
            ////$headers .= "X-Sender: <motorhome@quotezone.co.uk>\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!
            ////$headers .= "Return-Path: <motorhome@quotezone.co.uk>\r\n";

            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi OOH Coversure Leyton";
            $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);
            
            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi OOH Coversure Leyton";
            $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);  

         $outboundObj->UpdateOutboundField($this->outbounds['URL'],'sent_status', 1);

         if(preg_match("/OK/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 1);
         else
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 2);

         $sentContent =  "\nURL: ".$url."\nPARAMS:".$params;

         $this->DoOutboundFiles("url", $sentContent,$result);
      }//end Leadcall Coversure Leyton - 11100

      if(($this->session["_COMPANY_DETAILS_"]['9602']["REQUEST_URL"] == 1) AND ($this->siteID == 9602) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {
         $daytime_telephone    = $this->session['_YourDetails_']['daytime_telephone'];
         $mobile_telephone     = $this->session['_YourDetails_']['mobile_telephone'];

         $dest = $daytime_telephone?$daytime_telephone:$mobile_telephone;

         $title                = $this->session['_YourDetails_']['title'];
         $firstName            = $this->session['_YourDetails_']['first_name'];
         $lastName             = $this->session['_YourDetails_']['surname'];
         $dateOfBirth          = $this->session['_YourDetails_']['date_of_birth_dd'].$this->session['_YourDetails_']['date_of_birth_mm'].$this->session['_YourDetails_']['date_of_birth_yyyy'];
         $emailAddress         = $this->session['_YourDetails_']['email_address'];
         $postCode             = $this->session['_YourDetails_']['postcode_prefix'].$this->session['_YourDetails_']['postcode_number'];
         $dateOfInsuranceStart = $this->session['_YourDetails_']['date_of_insurance_start_mm'].$this->session['_YourDetails_']['date_of_insurance_start_dd'].$this->session['_YourDetails_']['date_of_insurance_start_yyyy'];
         $taxiRegistration     = $this->session['_YourDetails_']['vehicle_registration_number'];
         $taxiMake             = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel            = $this->session['_YourDetails_']['taxi_model'];
         $estimatedValue       = $this->session['_YourDetails_']['estimated_value'];
         $yearOfManufactures   = $this->session['_YourDetails_']['year_of_manufacture'];
         $vehicleMileage       = $this->session['_YourDetails_']['vehicle_mileage'];
         $taxiType             = $this->session['_YourDetails_']['taxi_type'];
         $taxiuse              = $this->session['_YourDetails_']['taxi_used_for'];
         $numberOfPasegers     = $this->session['_YourDetails_']['taxi_capacity'];
         $platingAuthority     = $this->session['_YourDetails_']['plating_authority'];
         $typeOfCover          = $this->session['_YourDetails_']['type_of_cover'];
         $noClaimBonus         = $this->session['_YourDetails_']['taxi_ncb'];
         $fullLicence          = $this->session['_YourDetails_']['period_of_licence'];
         $taxiBadge            = $this->session['_YourDetails_']['taxi_badge'];
         $claimsLastYears      = $this->session['_YourDetails_']['claims_5_years'];
         $convictionsLastYears = $this->session['_YourDetails_']['convictions_5_years'];
         $houseNrOrName        = $this->session['_YourDetails_']['house_number_or_name'];
         $engineSize           = $this->session['_YourDetails_']['engine_size'];

         $dateAndTime          = date("Y/m/d H:i:s");

         //new params
         $params  = "?login=insctaxi";
         $params .= "&password=insctaxi";
         $params .= "&destday=".urlencode($daytime_telephone);
         $params .= "&desteve=".urlencode($mobile_telephone);
         $params .= "&info_a._title=".urlencode($title);
         $params .= "&info_b._first_name=".urlencode($firstName);
         $params .= "&info_c._surname=".urlencode($lastName);
         $params .= "&info_d._DOB=".urlencode($dateOfBirth);
         $params .= "&info_e._email_address=".urlencode($emailAddress);
   //      $params .= "&info_x._address=".urlencode($houseNrOrName);
         $params .= "&info_f._postcode=".urlencode($postCode);
         $params .= "&info_g._insurance_start_date=".urlencode($dateOfInsuranceStart);
         $params .= "&info_h._taxi_registration=".urlencode($taxiRegistration);
         $params .= "&info_i._taxi_make=".urlencode($taxiMake);
         $params .= "&info_j._taxi_model=".urlencode($taxiModel);
         $params .= "&info_k._estimated_taxi_value=".urlencode($estimatedValue);
         $params .= "&info_l._year_of_manufacture=".urlencode($yearOfManufactures);
         $params .= "&info_m._vehicle_mileage=".urlencode($vehicleMileage);
         $params .= "&info_n._taxi_type=".urlencode($taxiType);
         $params .= "&info_o._taxi_use".urlencode($taxiuse);
         $params .= "&info_p._max_number_of_passengers=".urlencode($numberOfPasegers);
         $params .= "&info_q._taxi_plating_authority=".urlencode($platingAuthority);
         $params .= "&info_r._type_of_cover=".urlencode($typeOfCover);
         $params .= "&info_s._taxi_no_claims_bonus=".urlencode($noClaimBonus);
         $params .= "&info_t._full_uk_licence=".urlencode($fullLicence);
         $params .= "&info_u._taxi_badge".urlencode($taxiBadge);
         $params .= "&info_v._claims_last_5_years=".urlencode($claimsLastYears);
         $params .= "&info_w._convictions_last_5_years=".urlencode($convictionsLastYears);
         $params .= "&info_x._engine_size=".urlencode($engineSize);

         $url      = "https://secure.optilead.co.uk/followup/";
         $leadcall = $url.$params;

         //Get
         $ch = curl_init();

         curl_setopt($ch, CURLOPT_URL, $leadcall);
         curl_setopt($ch, CURLOPT_TIMEOUT, 30);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         $result = curl_exec($ch);
         curl_close($ch);

				$printParams = str_replace("?login=insctaxi&password=insctaxi","",$params);
				$printParams = str_replace("&","\r\n&",$printParams);
				
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
            //$headers .= "From: <Convicted Driver@quotezone.co.uk>\r\n";
            ////$headers .= "X-Sender: <Convicted Driver@quotezone.co.uk>\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!
            ////$headers .= "Return-Path: <Convicted Driver@quotezone.co.uk>\r\n";

            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Insurance Choice 3 (OOH) LeadCall-Optilead Request";
            $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);
            
            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Insurance Choice 3 (OOH) LeadCall-Optilead Request";
            $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);  

			         
         $outboundObj->UpdateOutboundField($this->outbounds['URL'],'sent_status', 1);

         if(! strlen($result))
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 3); //NO RESPONSE

         if(preg_match("/OK/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 1); //OK RESPONSE
         else
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 2);//BAD RESPONSE

         $this->DoOutboundFiles("url", $leadcall, $result);
      }//insurance choice Insurance Choice -Public Hire OOH Leadcall Method  LEADCALL


      // One Answer  lead call
      if(($this->session["_COMPANY_DETAILS_"]['858']["REQUEST_URL"] == 1) AND ($this->siteID == 858) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {
         $outboundObj->UpdateOutboundField($this->outbounds['URL'],'sent_status', 1);

         $loginUsername = '1ansqztx';
         $loginPassword = '1ansqztx';

         $dayPnone      = trim($this->session["_YourDetails_"]["daytime_telephone"]);
         $mobilePnone   = trim($this->session["_YourDetails_"]["mobile_telephone"]);

         $dayPnone      = str_replace(" ","",$dayPnone);
         $mobilePnone   = str_replace(" ","",$mobilePnone);

         if(trim($mobilePnone) != "")
         {
            $destPnone = $mobilePnone;
         }
         elseif(trim($dayPnone) != "")
         {
            $destPnone = $dayPnone;
         }

         $title       = $this->session["_YourDetails_"]["title"];
         $firstName   = $this->session["_YourDetails_"]["first_name"];
         $surname     = $this->session["_YourDetails_"]["surname"];
         $email       = urlencode($this->session["_YourDetails_"]["email_address"]);

         $dobDD       = $this->session["_YourDetails_"]["date_of_birth_dd"];
         $dobMM       = $this->session["_YourDetails_"]["date_of_birth_mm"];
         $dobYYYY     = $this->session["_YourDetails_"]["date_of_birth_yyyy"];
         $dobDDMMYYYY = $dobDD."/".$dobMM."/".$dobYYYY;

         if(trim($this->session["_YourDetails_"]["address_line1"]) == "")
         {
            $homeAddress = urlencode($this->session["_YourDetails_"]["house_number_or_name"]." ".$this->session["_YourDetails_"]["address_line4"]);
         }
         else
         {
            $homeAddress = urlencode($this->session["_YourDetails_"]["address_line1"]." ".$this->session["_YourDetails_"]["address_line2"]." ".$this->session["_YourDetails_"]["address_line3"]." ".$this->session["_YourDetails_"]["address_line4"]);
         }

         $postcode = $this->session["_YourDetails_"]["postcode_prefix"].$this->session["_YourDetails_"]["postcode_number"];

         $vehicleReg       = urlencode($this->session["_YourDetails_"]["vehicle_registration_number"]);
         $vehicleMake      = $this->session["_YourDetails_"]["taxi_make"];
         $vehicleModel     = $this->session["_YourDetails_"]["taxi_model"];
         $estimatedValue   = $this->session["_YourDetails_"]["estimated_value"];
         $annualMileage    = urlencode($_YourDetails["vehicle_mileage"][$this->session["_YourDetails_"]["vehicle_mileage"]]);
         $yearOfManuf      = $this->session["_YourDetails_"]["year_of_manufacture"];
         $vehicleType      = urlencode($_YourDetails["taxi_type"][$this->session["_YourDetails_"]["taxi_type"]]);
         $vehicleUse       = urlencode($_YourDetails["taxi_used_for"][$this->session["_YourDetails_"]["taxi_used_for"]]);
         $taxiCapacity     = urlencode($_YourDetails["taxi_capacity"][$this->session["_YourDetails_"]["taxi_capacity"]]);
         $platingAuthority = $this->session["_YourDetails_"]["plating_authority"];
         $typeOfCover      = urlencode($_YourDetails["type_of_cover"][$this->session["_YourDetails_"]["type_of_cover"]]);
         $noClaimsBonus    = urlencode($_YourDetails["taxi_ncb"][$this->session["_YourDetails_"]["taxi_ncb"]]);
         $periodOfLicence  = urlencode($_YourDetails["period_of_licence"][$this->session["_YourDetails_"]["period_of_licence"]]);
         $taxiBadge        = urlencode($_YourDetails["taxi_badge"][$this->session["_YourDetails_"]["taxi_badge"]]);
         $claims_5_years   = urlencode($_YourDetails["claims_5_years"][$this->session["_YourDetails_"]["claims_5_years"]]);

         $disDD       = $this->session["_YourDetails_"]["date_of_insurance_start_dd"];
         $disMM       = $this->session["_YourDetails_"]["date_of_insurance_start_mm"];
         $disYYYY     = $this->session["_YourDetails_"]["date_of_insurance_start_yyyy"];
         $disDDMMYYYY = $disDD.$disMM.$disYYYY;

         $params = "?&login=$loginUsername&password=$loginPassword&dest=$destPnone&destday=$dayPnone&desteve=$mobilePnone&info_A._Title=$title&info_B._First_Name=$firstName&info_C._Last_Name=$surname&Info_D._Home_Address=$homeAddress&info_E._Home_Postcode=$postcode&info_F._Email=$email&info_G._date_of_Birth=$dobDDMMYYYY&info_H._Reg_Number=$vehicleReg&info_I._Vehicle_Make=$vehicleMake&info_J._Vehicle_Model=$vehicleModel&info_K._Taxi_Value=$estimatedValue&info_L._Taxi_Mileage=$annualMileage&info_M._Year_Of_Manufacture=$yearOfManuf&info_N._Taxi_Type=$vehicleType&info_O._Taxi_use=$vehicleUse&info_P._Maximum_number_of_Passengers=$taxiCapacity&info_Q._Taxi_Plating_Authority=$platingAuthority&info_R._Type_Of_Cover=$typeOfCover&info_S._No_Claims_Bonus=$noClaimsBonus&info_T._Full_UK_Licence=$periodOfLicence&info_U._Taxi_Badge=$taxiBadge&info_V._Claims_Last_5_Years=$claims_5_years&info_W._Insurance_Start_date=$disDDMMYYYY";

         $leadCallUrl = "https://secure.optilead.co.uk/followup/$params";

         $ch = curl_init();

         // set URL and other appropriate options
         curl_setopt($ch, CURLOPT_URL, $leadCallUrl);
         curl_setopt($ch, CURLOPT_HEADER, 0);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

         $result = curl_exec($ch);

         curl_close($ch);

         print_r($result);


         $printParams = str_replace("?&login=$loginUsername&password=$loginPassword","",$params);
         $printParams = str_replace("&","\r\n&",$printParams);

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi One Answer LeadCall-Optilead Request";
         $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi One Answer LeadCall-Optilead Request";
         $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);


         if(! preg_match("/ERROR/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 1);
         else
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 2);

         // Ads sent and received data to filestore
         $this->DoOutboundFiles("url", $leadCallUrl, $result);
      } // end One Answer lead call


         // One Anwer 3 lead call
      if(($this->session["_COMPANY_DETAILS_"]['2374']["REQUEST_URL"] == 1) AND ($this->siteID == 2374)  AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {

         $outboundObj->UpdateOutboundField($this->outbounds['URL'],'sent_status', 1);

         $loginUsername = 'QZ1ANSPCO';
         $loginPassword = 'QZ1ANSPCO';

         $dayPnone      = trim($this->session["_YourDetails_"]["daytime_telephone"]);
         $mobilePnone   = trim($this->session["_YourDetails_"]["mobile_telephone"]);

         $dayPnone      = str_replace(" ","",$dayPnone);
         $mobilePnone   = str_replace(" ","",$mobilePnone);

         if(trim($mobilePnone) != "")
         {
            $destPnone = $mobilePnone;
         }
         elseif(trim($dayPnone) != "")
         {
            $destPnone = $dayPnone;
         }

         $title       = $this->session["_YourDetails_"]["title"];
         $firstName   = $this->session["_YourDetails_"]["first_name"];
         $surname     = $this->session["_YourDetails_"]["surname"];
         $email       = urlencode($this->session["_YourDetails_"]["email_address"]);

         $dobDD       = $this->session["_YourDetails_"]["date_of_birth_dd"];
         $dobMM       = $this->session["_YourDetails_"]["date_of_birth_mm"];
         $dobYYYY     = $this->session["_YourDetails_"]["date_of_birth_yyyy"];
         $dobDDMMYYYY = $dobDD."/".$dobMM."/".$dobYYYY;

         if(trim($this->session["_YourDetails_"]["address_line1"]) == "")
         {
            $homeAddress = urlencode($this->session["_YourDetails_"]["house_number_or_name"]." ".$this->session["_YourDetails_"]["address_line4"]);
         }
         else
         {
            $homeAddress = urlencode($this->session["_YourDetails_"]["address_line1"]." ".$this->session["_YourDetails_"]["address_line2"]." ".$this->session["_YourDetails_"]["address_line3"]." ".$this->session["_YourDetails_"]["address_line4"]);
         }

         $postcode = $this->session["_YourDetails_"]["postcode_prefix"].$this->session["_YourDetails_"]["postcode_number"];

         $vehicleReg       = urlencode($this->session["_YourDetails_"]["vehicle_registration_number"]);
         $vehicleMake      = $this->session["_YourDetails_"]["taxi_make"];
         $vehicleModel     = $this->session["_YourDetails_"]["taxi_model"];
         $estimatedValue   = $this->session["_YourDetails_"]["estimated_value"];
         $annualMileage    = urlencode($_YourDetails["vehicle_mileage"][$this->session["_YourDetails_"]["vehicle_mileage"]]);
         $yearOfManuf      = $this->session["_YourDetails_"]["year_of_manufacture"];
         $vehicleType      = urlencode($_YourDetails["taxi_type"][$this->session["_YourDetails_"]["taxi_type"]]);
         $vehicleUse       = urlencode($_YourDetails["taxi_used_for"][$this->session["_YourDetails_"]["taxi_used_for"]]);
         $taxiCapacity     = urlencode($_YourDetails["taxi_capacity"][$this->session["_YourDetails_"]["taxi_capacity"]]);
         $platingAuthority = $this->session["_YourDetails_"]["plating_authority"];
         $typeOfCover      = urlencode($_YourDetails["type_of_cover"][$this->session["_YourDetails_"]["type_of_cover"]]);
         $noClaimsBonus    = urlencode($_YourDetails["taxi_ncb"][$this->session["_YourDetails_"]["taxi_ncb"]]);
         $periodOfLicence  = urlencode($_YourDetails["period_of_licence"][$this->session["_YourDetails_"]["period_of_licence"]]);
         $taxiBadge        = urlencode($_YourDetails["taxi_badge"][$this->session["_YourDetails_"]["taxi_badge"]]);
         $claims_5_years   = urlencode($_YourDetails["claims_5_years"][$this->session["_YourDetails_"]["claims_5_years"]]);

         $disDD       = $this->session["_YourDetails_"]["date_of_insurance_start_dd"];
         $disMM       = $this->session["_YourDetails_"]["date_of_insurance_start_mm"];
         $disYYYY     = $this->session["_YourDetails_"]["date_of_insurance_start_yyyy"];
         $disDDMMYYYY = $disDD.$disMM.$disYYYY;

         $params = "?&login=$loginUsername&password=$loginPassword&dest=$destPnone&destday=$dayPnone&desteve=$mobilePnone&info_A._Title=$title&info_B._First_Name=$firstName&info_C._Last_Name=$surname&Info_D._Home_Address=$homeAddress&info_E._Home_Postcode=$postcode&info_F._Email=$email&info_G._date_of_Birth=$dobDDMMYYYY&info_H._Reg_Number=$vehicleReg&info_I._Vehicle_Make=$vehicleMake&info_J._Vehicle_Model=$vehicleModel&info_K._Taxi_Value=$estimatedValue&info_L._Taxi_Mileage=$annualMileage&info_M._Year_Of_Manufacture=$yearOfManuf&info_N._Taxi_Type=$vehicleType&info_O._Taxi_use=$vehicleUse&info_P._Maximum_number_of_Passengers=$taxiCapacity&info_Q._Taxi_Plating_Authority=$platingAuthority&info_R._Type_Of_Cover=$typeOfCover&info_S._No_Claims_Bonus=$noClaimsBonus&info_T._Full_UK_Licence=$periodOfLicence&info_U._Taxi_Badge=$taxiBadge&info_V._Claims_Last_5_Years=$claims_5_years&info_W._Insurance_Start_date=$disDDMMYYYY";

         $leadCallUrl = "https://secure.optilead.co.uk/followup/$params";

         $ch  = curl_init();
         $url = "http://www.leadcall.co.uk/followup";

         // set URL and other appropriate options
         curl_setopt($ch, CURLOPT_URL, $leadCallUrl);
         curl_setopt($ch, CURLOPT_HEADER, 0);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

         $result 	= curl_exec($ch);

         curl_close($ch);

         print_r($result);

         if(! preg_match("/ERROR/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 1);
         else
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 2);

         $printParams = str_replace("?&login=$loginUsername&password=$loginPassword","",$params);
         $printParams = str_replace("&","\r\n&",$printParams);

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi One Answer 3 LeadCall-Optilead Request";
         $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi One Answer 3 LeadCall-Optilead Request";
         $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);

         // Ads sent and received data to filestore
         $this->DoOutboundFiles("url", $leadCallUrl, $result);
      } // end  One Anwer 3 lead call
   }//end LEAD CALL method
   
   
   if($this->outbounds['XML'])
   {
      if(($this->session["_COMPANY_DETAILS_"]['1888']["REQUEST_XML"] == 1) AND ($this->siteID == 1888)  AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {


         $xmlData =GetEmailTemplate($this->session,"INSURANCECHOICEXML2");

         //test
         //$ch = curl_init('https://feeder-staging.insurancechoice.co.uk/upload');

         //live
         $ch = curl_init('https://feeder.insurancechoice.co.uk/upload');

         $username = "seopa";
         $password = "0a5ff64%elx_w5h6";

         curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
         curl_setopt($ch, CURLOPT_POST,true);
         curl_setopt($ch, CURLOPT_POSTFIELDS,$xmlData );
         curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: text/xml'));

         $result = curl_exec($ch);

         curl_close ($ch);

         $xmlMessageBody  = "<table width='500' cellspacing='2' cellpadding='2' border='0' align='left'>";
         $xmlMessageBody .= "<tr>
                           <td  style=\"font:bold 18px Calibri;color:#9C0D0D;\" align=\"left\">QUOTEZONE</td>
                           <td style=\"font:bold 12px Calibri;color:black;\" align=\"right\">$dateAndTime</td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML REQUEST:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$xmlData</td>
                        </tr>";
         $xmlMessageBody .= "<tr height=\"10\">
                           <td colspan=\"2\">
                              &nbsp;
                           </td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML RESPONSE:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$result</td>
                        </tr>";
         $xmlMessageBody .= "</table>";

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi Insurance Choice 2 - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);  

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."Taxi Insurance Choice 2 - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);  

         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if( preg_match("/\<uploadResults received\=\"1\"/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
         else
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

         $sentContent =  "\nURL: ".$url."\n<br>PARAMS:".$xmlData;

         $this->DoOutboundFiles("xml", $sentContent,$result);

      }
   }//end XML -- INSURANCE CHOICE 2

   // start LEADCALL method
   if($this->outbounds['URL'])
   {
      if(($this->session["_COMPANY_DETAILS_"]['1888']["REQUEST_URL"] == 1) AND ($this->siteID == 1888)  AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {
         $daytime_telephone    = $this->session['_YourDetails_']['daytime_telephone'];
         $mobile_telephone     = $this->session['_YourDetails_']['mobile_telephone'];

         $dest = $daytime_telephone?$daytime_telephone:$mobile_telephone;

         $title                = $this->session['_YourDetails_']['title'];
         $firstName            = $this->session['_YourDetails_']['first_name'];
         $lastName             = $this->session['_YourDetails_']['surname'];
         $dateOfBirth          = $this->session['_YourDetails_']['date_of_birth_dd'].$this->session['_YourDetails_']['date_of_birth_mm'].$this->session['_YourDetails_']['date_of_birth_yyyy'];
         $emailAddress         = $this->session['_YourDetails_']['email_address'];
         $postCode             = $this->session['_YourDetails_']['postcode_prefix'].$this->session['_YourDetails_']['postcode_number'];
         $dateOfInsuranceStart = $this->session['_YourDetails_']['date_of_insurance_start_mm'].$this->session['_YourDetails_']['date_of_insurance_start_dd'].$this->session['_YourDetails_']['date_of_insurance_start_yyyy'];
         $taxiRegistration     = $this->session['_YourDetails_']['vehicle_registration_number'];
         $taxiMake             = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel            = $this->session['_YourDetails_']['taxi_model'];
         $estimatedValue       = $this->session['_YourDetails_']['estimated_value'];
         $yearOfManufactures   = $this->session['_YourDetails_']['year_of_manufacture'];
         $vehicleMileage       = $this->session['_YourDetails_']['vehicle_mileage'];
         $taxiType             = $this->session['_YourDetails_']['taxi_type'];
         $taxiuse              = $this->session['_YourDetails_']['taxi_used_for'];
         $numberOfPasegers     = $this->session['_YourDetails_']['taxi_capacity'];
         $platingAuthority     = $this->session['_YourDetails_']['plating_authority'];
         $typeOfCover          = $this->session['_YourDetails_']['type_of_cover'];
         $noClaimBonus         = $this->session['_YourDetails_']['taxi_ncb'];
         $fullLicence          = $this->session['_YourDetails_']['period_of_licence'];
         $taxiBadge            = $this->session['_YourDetails_']['taxi_badge'];
         $claimsLastYears      = $this->session['_YourDetails_']['claims_5_years'];
         $convictionsLastYears = $this->session['_YourDetails_']['convictions_5_years'];
         $houseNrOrName        = $this->session['_YourDetails_']['house_number_or_name'];
         $engineSize           = $this->session['_YourDetails_']['engine_size'];

         $dateAndTime          = date("Y/m/d H:i:s");

         //new params
         $params  = "?login=insctaxi";
         $params .= "&password=insctaxi";
         $params .= "&destday=".urlencode($daytime_telephone);
         $params .= "&desteve=".urlencode($mobile_telephone);
         $params .= "&info_a._title=".urlencode($title);
         $params .= "&info_b._first_name=".urlencode($firstName);
         $params .= "&info_c._surname=".urlencode($lastName);
         $params .= "&info_d._DOB=".urlencode($dateOfBirth);
         $params .= "&info_e._email_address=".urlencode($emailAddress);
   //      $params .= "&info_x._address=".urlencode($houseNrOrName);
         $params .= "&info_f._postcode=".urlencode($postCode);
         $params .= "&info_g._insurance_start_date=".urlencode($dateOfInsuranceStart);
         $params .= "&info_h._taxi_registration=".urlencode($taxiRegistration);
         $params .= "&info_i._taxi_make=".urlencode($taxiMake);
         $params .= "&info_j._taxi_model=".urlencode($taxiModel);
         $params .= "&info_k._estimated_taxi_value=".urlencode($estimatedValue);
         $params .= "&info_l._year_of_manufacture=".urlencode($yearOfManufactures);
         $params .= "&info_m._vehicle_mileage=".urlencode($vehicleMileage);
         $params .= "&info_n._taxi_type=".urlencode($taxiType);
         $params .= "&info_o._taxi_use".urlencode($taxiuse);
         $params .= "&info_p._max_number_of_passengers=".urlencode($numberOfPasegers);
         $params .= "&info_q._taxi_plating_authority=".urlencode($platingAuthority);
         $params .= "&info_r._type_of_cover=".urlencode($typeOfCover);
         $params .= "&info_s._taxi_no_claims_bonus=".urlencode($noClaimBonus);
         $params .= "&info_t._full_uk_licence=".urlencode($fullLicence);
         $params .= "&info_u._taxi_badge".urlencode($taxiBadge);
         $params .= "&info_v._claims_last_5_years=".urlencode($claimsLastYears);
         $params .= "&info_w._convictions_last_5_years=".urlencode($convictionsLastYears);
         $params .= "&info_x._engine_size=".urlencode($engineSize);

         $url      = "https://secure.optilead.co.uk/followup/";
         $leadcall = $url.$params;

         //Get
         $ch = curl_init();

         curl_setopt($ch, CURLOPT_URL, $leadcall);
         curl_setopt($ch, CURLOPT_TIMEOUT, 30);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         $result = curl_exec($ch);
         curl_close($ch);

				$printParams = str_replace("?login=insctaxi&password=insctaxi","",$params);
				$printParams = str_replace("&","\r\n&",$printParams);
				
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
            //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
            ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!
            ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Insurance Choice 2 LeadCall-Optilead Request";
            $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);
            
            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Insurance Choice 2 LeadCall-Optilead Request";
            $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"URL",$this->siteID);  
		

         $outboundObj->UpdateOutboundField($this->outbounds['URL'],'sent_status', 1);

         if(! strlen($result))
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 3); //NO RESPONSE

         if(preg_match("/OK/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 1); //OK RESPONSE
         else
            $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 2);//BAD RESPONSE

         $this->DoOutboundFiles("url", $leadcall, $result);
      }
   }//end LEAD CALL method  -- INSURANCE CHOICE 2   
   
   
   // XML method
   if($this->outbounds['XML'])
   {
      
      if(($this->session["_COMPANY_DETAILS_"]['10148']["REQUEST_XML"] == 1) AND ($this->siteID == 10148)  AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {           
         $subject = "Taxi query : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];
      
         $guid         = $this->rand_str(8)."-".$this->rand_str(4)."-".$this->rand_str(4)."-".$this->rand_str(4)."-".$this->rand_str(12);
         $leadTypeRef  = "QTZTAXI";
         $quoteRef     = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];
         $firstName    = $this->session['_YourDetails_']['first_name'];
         $lastName     = $this->session['_YourDetails_']['surname'];
         $email        = $this->session['_YourDetails_']['email_address'];
         $renewalDate  = $this->session['_YourDetails_']['date_of_insurance_start_yyyy']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_dd']."T00:00:00";
         $postcode     = $this->session['_YourDetails_']['postcode_prefix'].$this->session['_YourDetails_']['postcode_number'];
         $dateOfBirth  = $this->session['_YourDetails_']['date_of_birth'];

         $telephone    = $this->session['_YourDetails_']['daytime_telephone'];
         $mobilePhone  = $this->session['_YourDetails_']['mobile_telephone'];
         
             if($this->session['_YourDetails_']['daytime_telephone'] == "")
               $telephone = $this->session['_YourDetails_']['mobile_telephone'];
            
             if($this->session['_YourDetails_']['mobile_telephone'] == "")
               $mobilePhone = $this->session['_YourDetails_']['daytime_telephone'];
               
        
         $companyName = $this->session['_YourDetails_']['business_name'];

     

         //spare details for taxi
         $estimatedValue   = $this->session['_YourDetails_']['estimated_value'];
         $taxyType         = $_YourDetails['taxi_type'][$this->session['_YourDetails_']['taxi_type']];
         if($taxyType == "Black Cab")
            $taxyType = "Scotland";
         
         $taxiUse          = $_YourDetails['taxi_used_for'][$this->session['_YourDetails_']['taxi_used_for']];

         $platingAuthoriry = $this->session['_YourDetails_']['plating_authority'];
         $platingAuthoriry = urlencode($platingAuthoriry);

         $taxiBadge        = $_YourDetails['taxi_badge'][$this->session['_YourDetails_']['taxi_badge']];

         $SPARE_2 = substr('Estimated Taxi Value: '.$estimatedValue,0,50);
         $SPARE_3 = substr('Taxi Type: Scotland ',0,50);
         $SPARE_4 = substr('Taxi Use: '.$taxiUse,0,50);
         $SPARE_5 = str_replace("+"," ",substr('Taxi Plating Authority: '.$platingAuthoriry,0,50));
         $SPARE_6 = substr('Taxi Badge: '.$dateOfBirth,0,50);

      $houseNrName = $this->session['_YourDetails_']['house_number_or_name'];

//          source code/name:
//          QTZTAXI
//
//          Spare Fields:
//          2.Estimated Taxi Value
//          3.Taxi Type
//          4.Taxi Use
//          5.Taxi Plating Authority
//          6.Taxi Badge

         $xmlData    = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
            <soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">
               <soap12:Body>
                  <SubmitMicroVehicleLead xmlns=\"https://wsleads.brightsidegroup.co.uk/\">
                     <Username>QTZLIVE</Username>
                     <Password>90TE20N3</Password>
                     <TestMode>false</TestMode>
                     <LeadTypeReference>$leadTypeRef</LeadTypeReference><XMLMessage>";

         $xmlMessage = htmlspecialchars("<SubmitMicroVehicleLead>
         <LEADS>
            <LEAD_REFERENCE>$guid</LEAD_REFERENCE>
            <FIRST_NAME>$firstName</FIRST_NAME>
            <LAST_NAME>$lastName</LAST_NAME>
            <EMAIL>$email</EMAIL>
            <TELEPHONE_1>$telephone</TELEPHONE_1>
            <TELEPHONE_2>$mobilePhone</TELEPHONE_2>
            <MARKETING_OPT_IN>false</MARKETING_OPT_IN>
            <EXTERNAL_MARKETING_OPT_IN>false</EXTERNAL_MARKETING_OPT_IN>
            <RENEWAL_DATE>$renewalDate</RENEWAL_DATE>
            <COMPANY_NAME>Quotezone</COMPANY_NAME>
            <POSTCODE>$postcode</POSTCODE>
            <LEAD_PROVIDER_REFERENCE>$quoteRef</LEAD_PROVIDER_REFERENCE>
            <NOTES>
               <SPARE_1>Taxi</SPARE_1>
               <SPARE_2>$SPARE_2</SPARE_2>
               <SPARE_3>$SPARE_3</SPARE_3>
               <SPARE_4>$SPARE_4</SPARE_4>
               <SPARE_5>$SPARE_5</SPARE_5>
               <SPARE_6>$houseNrName</SPARE_6>
            </NOTES>
         </LEADS>
      </SubmitMicroVehicleLead>");
         $xmlData   .= $xmlMessage;
         $xmlData   .= "</XMLMessage></SubmitMicroVehicleLead></soap12:Body></soap12:Envelope>";

         $url = "https://wsleads.brightsidegroup.co.uk/WSLEADS.asmx";

         $ch = curl_init();

         $header[] = "POST /WSLEADS.asmx HTTP/1.1";
         $header[] = "Host: wsleads.brightsidegroup.co.uk";
         $header[] = "Content-Type: application/soap+xml; charset=utf-8";
         $header[] = "Content-length: ".strlen($xmlData);
         $header[] = "Connection: close \r\n";
         $header[] = $xmlData;

         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
         curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 180);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_HEADER, 1);

         $result = curl_exec($ch);

         curl_close ($ch);

         $xmlMessageBody  = "<table width='500' cellspacing='2' cellpadding='2' border='0' align='left'>";
         $xmlMessageBody .= "<tr>
                           <td  style=\"font:bold 18px Calibri;color:#9C0D0D;\" align=\"left\">QUOTEZONE</td>
                           <td style=\"font:bold 12px Calibri;color:black;\" align=\"right\">$dateAndTime</td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML REQUEST:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$xmlData</td>
                        </tr>";
         $xmlMessageBody .= "<tr height=\"10\">
                           <td colspan=\"2\">
                              &nbsp;
                           </td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML RESPONSE:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$result</td>
                        </tr>";
         $xmlMessageBody .= "</table>";

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi Brightside Scotland - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi Brightside Scotland - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if( preg_match("/<RESPONSE_CODE\>OK\<\/RESPONSE_CODE\>/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
         else
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

         $sentContent =  "\nURL: ".$url."\n<br>PARAMS:".$xmlData;

         $this->DoOutboundFiles("xml", $sentContent,$result);
      }
      
      if(($this->session["_COMPANY_DETAILS_"]['10149']["REQUEST_XML"] == 1) AND ($this->siteID == 10149)  AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {           
         $subject = "Taxi query : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];
      
         $guid         = $this->rand_str(8)."-".$this->rand_str(4)."-".$this->rand_str(4)."-".$this->rand_str(4)."-".$this->rand_str(12);
         $leadTypeRef  = "QTZTAXI";
         $quoteRef     = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];
         $firstName    = $this->session['_YourDetails_']['first_name'];
         $lastName     = $this->session['_YourDetails_']['surname'];
         $email        = $this->session['_YourDetails_']['email_address'];
         $renewalDate  = $this->session['_YourDetails_']['date_of_insurance_start_yyyy']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_dd']."T00:00:00";
         $postcode     = $this->session['_YourDetails_']['postcode_prefix'].$this->session['_YourDetails_']['postcode_number'];
         $dateOfBirth  = $this->session['_YourDetails_']['date_of_birth'];

         $telephone    = $this->session['_YourDetails_']['daytime_telephone'];
         $mobilePhone  = $this->session['_YourDetails_']['mobile_telephone'];
         
             if($this->session['_YourDetails_']['daytime_telephone'] == "")
               $telephone = $this->session['_YourDetails_']['mobile_telephone'];
            
             if($this->session['_YourDetails_']['mobile_telephone'] == "")
               $mobilePhone = $this->session['_YourDetails_']['daytime_telephone'];
         
        
         $companyName = $this->session['_YourDetails_']['business_name'];

     
         //spare details for taxi
         $estimatedValue   = $this->session['_YourDetails_']['estimated_value'];
         $taxyType         = $_YourDetails['taxi_type'][$this->session['_YourDetails_']['taxi_type']];
         if($taxyType == "Black Cab")
            $taxyType = "Walsingham";
         
         $taxiUse          = $_YourDetails['taxi_used_for'][$this->session['_YourDetails_']['taxi_used_for']];

         $platingAuthoriry = $this->session['_YourDetails_']['plating_authority'];
         $platingAuthoriry = urlencode($platingAuthoriry);

         $taxiBadge        = $_YourDetails['taxi_badge'][$this->session['_YourDetails_']['taxi_badge']];

         $SPARE_2 = substr('Estimated Taxi Value: '.$estimatedValue,0,50);
         $SPARE_3 = substr('Taxi Type: Walsingham ',0,50);
         $SPARE_4 = substr('Taxi Use: '.$taxiUse,0,50);
         $SPARE_5 = str_replace("+"," ",substr('Taxi Plating Authority: '.$platingAuthoriry,0,50));
         $SPARE_6 = substr('Taxi Badge: '.$dateOfBirth,0,50);

      $houseNrName = $this->session['_YourDetails_']['house_number_or_name'];

//          source code/name:
//          QTZTAXI
//
//          Spare Fields:
//          2.Estimated Taxi Value
//          3.Taxi Type
//          4.Taxi Use
//          5.Taxi Plating Authority
//          6.Taxi Badge

         $xmlData    = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
            <soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">
               <soap12:Body>
                  <SubmitMicroVehicleLead xmlns=\"https://wsleads.brightsidegroup.co.uk/\">
                     <Username>QTZLIVE</Username>
                     <Password>90TE20N3</Password>
                     <TestMode>false</TestMode>
                     <LeadTypeReference>$leadTypeRef</LeadTypeReference><XMLMessage>";

         $xmlMessage = htmlspecialchars("<SubmitMicroVehicleLead>
         <LEADS>
            <LEAD_REFERENCE>$guid</LEAD_REFERENCE>
            <FIRST_NAME>$firstName</FIRST_NAME>
            <LAST_NAME>$lastName</LAST_NAME>
            <EMAIL>$email</EMAIL>
            <TELEPHONE_1>$telephone</TELEPHONE_1>
            <TELEPHONE_2>$mobilePhone</TELEPHONE_2>
            <MARKETING_OPT_IN>false</MARKETING_OPT_IN>
            <EXTERNAL_MARKETING_OPT_IN>false</EXTERNAL_MARKETING_OPT_IN>
            <RENEWAL_DATE>$renewalDate</RENEWAL_DATE>
            <COMPANY_NAME>Quotezone</COMPANY_NAME>
            <POSTCODE>$postcode</POSTCODE>
            <LEAD_PROVIDER_REFERENCE>$quoteRef</LEAD_PROVIDER_REFERENCE>
            <NOTES>
               <SPARE_1>Taxi</SPARE_1>
               <SPARE_2>$SPARE_2</SPARE_2>
               <SPARE_3>$SPARE_3</SPARE_3>
               <SPARE_4>$SPARE_4</SPARE_4>
               <SPARE_5>$SPARE_5</SPARE_5>
               <SPARE_6>$houseNrName</SPARE_6>
            </NOTES>
         </LEADS>
      </SubmitMicroVehicleLead>");
         $xmlData   .= $xmlMessage;
         $xmlData   .= "</XMLMessage></SubmitMicroVehicleLead></soap12:Body></soap12:Envelope>";

         $url = "https://wsleads.brightsidegroup.co.uk/WSLEADS.asmx";

         $ch = curl_init();

         $header[] = "POST /WSLEADS.asmx HTTP/1.1";
         $header[] = "Host: wsleads.brightsidegroup.co.uk";
         $header[] = "Content-Type: application/soap+xml; charset=utf-8";
         $header[] = "Content-length: ".strlen($xmlData);
         $header[] = "Connection: close \r\n";
         $header[] = $xmlData;

         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
         curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 180);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_HEADER, 1);

         $result = curl_exec($ch);

         curl_close ($ch);

         $xmlMessageBody  = "<table width='500' cellspacing='2' cellpadding='2' border='0' align='left'>";
         $xmlMessageBody .= "<tr>
                           <td  style=\"font:bold 18px Calibri;color:#9C0D0D;\" align=\"left\">QUOTEZONE</td>
                           <td style=\"font:bold 12px Calibri;color:black;\" align=\"right\">$dateAndTime</td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML REQUEST:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$xmlData</td>
                        </tr>";
         $xmlMessageBody .= "<tr height=\"10\">
                           <td colspan=\"2\">
                              &nbsp;
                           </td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML RESPONSE:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$result</td>
                        </tr>";
         $xmlMessageBody .= "</table>";

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi Brightside Walsingham - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi Brightside Walsingham - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if( preg_match("/<RESPONSE_CODE\>OK\<\/RESPONSE_CODE\>/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
         else
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

         $sentContent =  "\nURL: ".$url."\n<br>PARAMS:".$xmlData;

         $this->DoOutboundFiles("xml", $sentContent,$result);
      }
 if(($this->session["_COMPANY_DETAILS_"]['10443']["REQUEST_XML"] == 1) AND ($this->siteID == 10443) AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {
         $subject = "Taxi query : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];

         $quoteRef      = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];
         $firstName     = $this->session['_YourDetails_']['first_name'];
         $lastName      = $this->session['_YourDetails_']['surname'];
         $email         = $this->session['_YourDetails_']['email_address'];
         $houseNrName   = $this->session['_YourDetails_']['house_number_or_name'];
         $houseNrName   = str_replace("&","and",$houseNrName);


         $DOB  = $this->session['_YourDetails_']['date_of_birth_dd']."-".$this->session['_YourDetails_']['date_of_birth_mm']."-".$this->session['_YourDetails_']['date_of_birth_yyyy'];
         $DIS  = $this->session['_YourDetails_']['date_of_insurance_start_dd']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_yyyy']." ".date("H:m");
         $inceptionDate = $this->session['_YourDetails_']['date_of_insurance_start_dd']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_yyyy'];
         $postcode      = $this->session['_YourDetails_']['postcode_prefix'].$this->session['_YourDetails_']['postcode_number'];
         $telephone     = $this->session['_YourDetails_']['daytime_telephone'];
         $mobilePhone   = $this->session['_YourDetails_']['mobile_telephone'];
         $taxiMake      = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel     = $this->session['_YourDetails_']['taxi_model'];

         if($this->session['_YourDetails_']['daytime_telephone'] == "")
            $telephone  = $this->session['_YourDetails_']['mobile_telephone'];

         if($this->session['_YourDetails_']['mobile_telephone'] == "")
            $mobilePhone = $this->session['_YourDetails_']['daytime_telephone'];

         $estimatedValue = $this->session['_YourDetails_']['estimated_value'];
         $quoteDateTime  = date("d-m-Y H:i");


         $xmlData    = '<?xml version="1.0" encoding="utf-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:brig="http://www.brightsideinsurance.co.uk/">
            <soapenv:Header/>
            <soapenv:Body>
               <brig:UploadLead>
               <!--Optional:-->
               <brig:RecordXML>
                  &lt;TopXLeadRequest&gt;
                     &lt;QuoteReference&gt;'.$quoteRef.'&lt;/QuoteReference&gt;
                     &lt;Forename&gt;'.$firstName.'&lt;/Forename&gt;
                     &lt;Surname&gt;'.$lastName.'&lt;/Surname&gt;
                     &lt;DateOfBirth&gt;'.$DOB.'&lt;/DateOfBirth&gt;
                     &lt;PhoneNumber1&gt;'.$telephone.'&lt;/PhoneNumber1&gt;
                     &lt;PhoneNumber2&gt;'.$mobilePhone.'&lt;/PhoneNumber2&gt;
                     &lt;EmailAddress1&gt;'.$email.'&lt;/EmailAddress1&gt;
                     &lt;AddressLine1&gt;'.$houseNrName.'&lt;/AddressLine1&gt;
                     &lt;Postcode&gt;'.$postcode.'&lt;/Postcode&gt;
                     &lt;Make&gt;'.$taxiMake.'&lt;/Make&gt;
                     &lt;Model&gt;'.$taxiModel.'&lt;/Model&gt;
                     &lt;Premium&gt;&lt;/Premium&gt;
                     &lt;TopXPosition&gt;1&lt;/TopXPosition&gt;
                     &lt;InceptionDate&gt;'.$inceptionDate.'&lt;/InceptionDate&gt;
                     &lt;QuoteProviderID&gt;14&lt;/QuoteProviderID&gt;
                     &lt;QuoteTypeID&gt;171&lt;/QuoteTypeID&gt;
                     &lt;QuoteDateTime&gt;'.$quoteDateTime.'&lt;/QuoteDateTime&gt;
                  &lt;/TopXLeadRequest&gt;
               </brig:RecordXML>
               </brig:UploadLead>
            </soapenv:Body>
         </soapenv:Envelope>';

         $url = "https://leadsource.brightsidegroup.co.uk/insert.asmx";

         $ch = curl_init();

         $header[] = "POST /insert.asmx HTTP/1.1";
         $header[] = "Host: leadsource.brightsidegroup.co.uk";
         // $header[] = "Content-Type: application/soap+xml; charset=utf-8";
         $header[] = "Content-Type: text/xml; charset=utf-8";
         $header[] = "Content-length: ".strlen($xmlData);
         $header[] = "SOAPAction: \"http://www.brightsideinsurance.co.uk/UploadLead\"";
         $header[] = "Connection: close \r\n";
         $header[] = $xmlData;

         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
         curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 180);

         curl_setopt($ch, CURLOPT_VERBOSE, 1);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_HEADER, 1);

         $result  = curl_exec($ch);
         $erro    = curl_error($ch);
         $errNo   = curl_errno($ch);


         curl_close ($ch);

         $xmlMessageBody  = "<table width='500' cellspacing='2' cellpadding='2' border='0' align='left'>";
         $xmlMessageBody .= "<tr>
                           <td  style=\"font:bold 18px Calibri;color:#9C0D0D;\" align=\"left\">QUOTEZONE</td>
                           <td style=\"font:bold 12px Calibri;color:black;\" align=\"right\">$dateAndTime</td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML REQUEST:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$xmlData</td>
                        </tr>";
         $xmlMessageBody .= "<tr height=\"10\">
                           <td colspan=\"2\">
                              &nbsp;
                           </td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML RESPONSE:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$result [$erro [$errNo]]</td>
                        </tr>";
         $xmlMessageBody .= "</table>";

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi Brightside Uber - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi Brightside Uber - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if( preg_match("/Record Accepted/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
         else
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

         $sentContent =  "\nURL: ".$url."\n<br>PARAMS:".$xmlData;

         $this->DoOutboundFiles("xml", $sentContent,$result);
      }

      if(($this->session["_COMPANY_DETAILS_"]['10304']["REQUEST_XML"] == 1) AND ($this->siteID == 10304)  AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      // if(($this->session["_COMPANY_DETAILS_"]['10304']["REQUEST_XML"] == 1) AND ($this->siteID == 10304))
      {
         $subject = "Taxi query : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];
              
         $quoteRef     = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];
         $firstName    = $this->session['_YourDetails_']['first_name'];
         $lastName     = $this->session['_YourDetails_']['surname'];


         $email        = $this->session['_YourDetails_']['email_address'];
         $houseNrName  = $this->session['_YourDetails_']['house_number_or_name'];


         $DOB  = $this->session['_YourDetails_']['date_of_birth_dd']."-".$this->session['_YourDetails_']['date_of_birth_mm']."-".$this->session['_YourDetails_']['date_of_birth_yyyy'];
         $DIS  = $this->session['_YourDetails_']['date_of_insurance_start_dd']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_yyyy']." ".date("H:m");
         $inceptionDate  = $this->session['_YourDetails_']['date_of_insurance_start_dd']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_yyyy'];

         $postcode     = $this->session['_YourDetails_']['postcode_prefix'].$this->session['_YourDetails_']['postcode_number'];
         $telephone    = $this->session['_YourDetails_']['daytime_telephone'];
         $mobilePhone  = $this->session['_YourDetails_']['mobile_telephone'];
         $taxiMake             = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel            = $this->session['_YourDetails_']['taxi_model'];
         
             if($this->session['_YourDetails_']['daytime_telephone'] == "")
               $telephone = $this->session['_YourDetails_']['mobile_telephone'];
            
             if($this->session['_YourDetails_']['mobile_telephone'] == "")
               $mobilePhone = $this->session['_YourDetails_']['daytime_telephone'];
    
         $estimatedValue   = $this->session['_YourDetails_']['estimated_value'];


         $xmlData    = '<?xml version="1.0" encoding="utf-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:brig="http://www.brightsideinsurance.co.uk/">
            <soapenv:Header/>
            <soapenv:Body>
               <brig:UploadLead>
               <!--Optional:-->
               <brig:RecordXML>
                  &lt;TopXLeadRequest&gt;
                     &lt;QuoteReference&gt;'.$quoteRef.'&lt;/QuoteReference&gt;
                     &lt;Forename&gt;'.$firstName.'&lt;/Forename&gt;
                     &lt;Surname&gt;'.$lastName.'&lt;/Surname&gt;
                     &lt;DateOfBirth&gt;'.$DOB.'&lt;/DateOfBirth&gt;
                     &lt;PhoneNumber1&gt;'.$telephone.'&lt;/PhoneNumber1&gt;
                     &lt;PhoneNumber2&gt;'.$mobilePhone.'&lt;/PhoneNumber2&gt;
                     &lt;EmailAddress1&gt;'.$email.'&lt;/EmailAddress1&gt;
                     &lt;AddressLine1&gt;'.$houseNrName.'&lt;/AddressLine1&gt;
                     &lt;Postcode&gt;'.$postcode.'&lt;/Postcode&gt;
                     &lt;Make&gt;'.$taxiMake.'&lt;/Make&gt;
                     &lt;Model&gt;'.$taxiModel.'&lt;/Model&gt;
                     &lt;Premium&gt;&lt;/Premium&gt;
                     &lt;TopXPosition&gt;1&lt;/TopXPosition&gt;
                     &lt;InceptionDate&gt;'.$inceptionDate.'&lt;/InceptionDate&gt;
                     &lt;QuoteProviderID&gt;14&lt;/QuoteProviderID&gt;                            
                     &lt;QuoteTypeID&gt;171&lt;/QuoteTypeID&gt;
                     &lt;QuoteDateTime&gt;'.$DIS.'&lt;/QuoteDateTime&gt;
                  &lt;/TopXLeadRequest&gt;
               </brig:RecordXML>
               </brig:UploadLead>
            </soapenv:Body>
         </soapenv:Envelope>';

         $url = "https://leadsource.brightsidegroup.co.uk/insert.asmx";

         $ch = curl_init();

         $header[] = "POST /insert.asmx HTTP/1.1";
         $header[] = "Host: leadsource.brightsidegroup.co.uk";
         // $header[] = "Content-Type: application/soap+xml; charset=utf-8";
         $header[] = "Content-Type: text/xml; charset=utf-8";
         $header[] = "Content-length: ".strlen($xmlData);
         $header[] = "SOAPAction: \"http://www.brightsideinsurance.co.uk/UploadLead\"";
         $header[] = "Connection: close \r\n";
         $header[] = $xmlData;

         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
         curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 180);

         curl_setopt($ch, CURLOPT_VERBOSE, 1);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_HEADER, 1);

         $result  = curl_exec($ch);
         $erro    = curl_error($ch);
         $errNo   = curl_errno($ch);

         if(! preg_match("/Record Accepted/i",$result))
         {
            //send email with the failed response
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
            ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!
            ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Brightside (OIS TQ) - Taxi (PCO) (taxi) 10304 NEW - FAILED 1st REQUEST";
            $mailBody    = "Response is a failure :\n $result($erro) \n";
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

            $mailTo      = "alexandru.furtuna@gmail.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Brightside (OIS TQ) - Taxi (PCO) (taxi) 10304 NEW - FAILED 1st REQUEST";
            $mailBody    = "Response is a failure :\n $result($erro) \n";
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);


            //sleep 10 seconds then retry to send thed deta
            sleep(10);

            //rerun the request
            $result  = curl_exec($ch);
            $erro    = curl_error($ch);
            $errNo   = curl_errno($ch);

         }

         //close the connection
         curl_close ($ch);

         $xmlMessageBody  = "<table width='500' cellspacing='2' cellpadding='2' border='0' align='left'>";
         $xmlMessageBody .= "<tr>
                           <td  style=\"font:bold 18px Calibri;color:#9C0D0D;\" align=\"left\">QUOTEZONE</td>
                           <td style=\"font:bold 12px Calibri;color:black;\" align=\"right\">$dateAndTime</td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML REQUEST:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$xmlData</td>
                        </tr>";
         $xmlMessageBody .= "<tr height=\"10\">
                           <td colspan=\"2\">
                              &nbsp;
                           </td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML RESPONSE:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$result [$erro [$errNo]]</td>
                        </tr>";
         $xmlMessageBody .= "</table>";

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Brightside (OIS TQ) - Taxi (PCO) (taxi) 10304 NEW";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Brightside (OIS TQ) - Taxi (PCO) (taxi) 10304 NEW";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if( preg_match("/Record Accepted/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
         else
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

         $sentContent =  "\nURL: ".$url."\n<br>PARAMS:".$xmlData;

         $this->DoOutboundFiles("xml", $sentContent,$result);
         //END NEW REQUEST
      }

      if(($this->session["_COMPANY_DETAILS_"]['10150']["REQUEST_XML"] == 1) AND ($this->siteID == 10150)  AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {           
         $subject = "Taxi query : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];
      
         $guid         = $this->rand_str(8)."-".$this->rand_str(4)."-".$this->rand_str(4)."-".$this->rand_str(4)."-".$this->rand_str(12);
         $leadTypeRef  = "QTZTAXI";
         $quoteRef     = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];
         $firstName    = $this->session['_YourDetails_']['first_name'];
         $lastName     = $this->session['_YourDetails_']['surname'];
         $email        = $this->session['_YourDetails_']['email_address'];
         $renewalDate  = $this->session['_YourDetails_']['date_of_insurance_start_yyyy']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_dd']."T00:00:00";
         $postcode     = $this->session['_YourDetails_']['postcode_prefix'].$this->session['_YourDetails_']['postcode_number'];
         $dateOfBirth  = $this->session['_YourDetails_']['date_of_birth'];

         $telephone    = $this->session['_YourDetails_']['daytime_telephone'];
         $mobilePhone  = $this->session['_YourDetails_']['mobile_telephone'];
         
             if($this->session['_YourDetails_']['daytime_telephone'] == "")
               $telephone = $this->session['_YourDetails_']['mobile_telephone'];
            
             if($this->session['_YourDetails_']['mobile_telephone'] == "")
               $mobilePhone = $this->session['_YourDetails_']['daytime_telephone'];
         
         
         $companyName = $this->session['_YourDetails_']['business_name'];

       


         //spare details for taxi
         $estimatedValue   = $this->session['_YourDetails_']['estimated_value'];
         $taxyType         = $_YourDetails['taxi_type'][$this->session['_YourDetails_']['taxi_type']];
         if($taxyType == "Black Cab")
            $taxyType = "Orbit";
         
         $taxiUse          = $_YourDetails['taxi_used_for'][$this->session['_YourDetails_']['taxi_used_for']];

         $platingAuthoriry = $this->session['_YourDetails_']['plating_authority'];
         $platingAuthoriry = urlencode($platingAuthoriry);

         $taxiBadge        = $_YourDetails['taxi_badge'][$this->session['_YourDetails_']['taxi_badge']];

         $SPARE_2 = substr('Estimated Taxi Value: '.$estimatedValue,0,50);
         $SPARE_3 = substr('Taxi Type: Orbit ',0,50);
         $SPARE_4 = substr('Taxi Use: '.$taxiUse,0,50);
         $SPARE_5 = str_replace("+"," ",substr('Taxi Plating Authority: '.$platingAuthoriry,0,50));
         $SPARE_6 = substr('Taxi Badge: '.$dateOfBirth,0,50);

      $houseNrName = $this->session['_YourDetails_']['house_number_or_name'];

//          source code/name:
//          QTZTAXI
//
//          Spare Fields:
//          2.Estimated Taxi Value
//          3.Taxi Type
//          4.Taxi Use
//          5.Taxi Plating Authority
//          6.Taxi Badge

         $xmlData    = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
            <soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">
               <soap12:Body>
                  <SubmitMicroVehicleLead xmlns=\"https://wsleads.brightsidegroup.co.uk/\">
                     <Username>QTZLIVE</Username>
                     <Password>90TE20N3</Password>
                     <TestMode>false</TestMode>
                     <LeadTypeReference>$leadTypeRef</LeadTypeReference><XMLMessage>";

         $xmlMessage = htmlspecialchars("<SubmitMicroVehicleLead>
         <LEADS>
            <LEAD_REFERENCE>$guid</LEAD_REFERENCE>
            <FIRST_NAME>$firstName</FIRST_NAME>
            <LAST_NAME>$lastName</LAST_NAME>
            <EMAIL>$email</EMAIL>
            <TELEPHONE_1>$telephone</TELEPHONE_1>
            <TELEPHONE_2>$mobilePhone</TELEPHONE_2>
            <MARKETING_OPT_IN>false</MARKETING_OPT_IN>
            <EXTERNAL_MARKETING_OPT_IN>false</EXTERNAL_MARKETING_OPT_IN>
            <RENEWAL_DATE>$renewalDate</RENEWAL_DATE>
            <COMPANY_NAME>Quotezone</COMPANY_NAME>
            <POSTCODE>$postcode</POSTCODE>
            <LEAD_PROVIDER_REFERENCE>$quoteRef</LEAD_PROVIDER_REFERENCE>
            <NOTES>
               <SPARE_1>Taxi</SPARE_1>
               <SPARE_2>$SPARE_2</SPARE_2>
               <SPARE_3>$SPARE_3</SPARE_3>
               <SPARE_4>$SPARE_4</SPARE_4>
               <SPARE_5>$SPARE_5</SPARE_5>
               <SPARE_6>$houseNrName</SPARE_6>
            </NOTES>
         </LEADS>
      </SubmitMicroVehicleLead>");
         $xmlData   .= $xmlMessage;
         $xmlData   .= "</XMLMessage></SubmitMicroVehicleLead></soap12:Body></soap12:Envelope>";

         $url = "https://wsleads.brightsidegroup.co.uk/WSLEADS.asmx";

         $ch = curl_init();

         $header[] = "POST /WSLEADS.asmx HTTP/1.1";
         $header[] = "Host: wsleads.brightsidegroup.co.uk";
         $header[] = "Content-Type: application/soap+xml; charset=utf-8";
         $header[] = "Content-length: ".strlen($xmlData);
         $header[] = "Connection: close \r\n";
         $header[] = $xmlData;

         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
         curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 180);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_HEADER, 1);

         $result = curl_exec($ch);

         if(! preg_match("/<RESPONSE_CODE\>OK\<\/RESPONSE_CODE\>/i",$result))
         {

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!

            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Brightside Orbit - XML Method RESENDING";
            $mailBody    = "Error response on 1st request , resending request";
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Brightside Orbit - XML Method RESENDING";
            $mailBody    = "Error response on 1st request , resending request";
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

            //sleep 10 seconds then retry to send thed deta
            sleep(10);

            //rerun the request
            $result  = curl_exec($ch);
         }

         curl_close ($ch);

         $xmlMessageBody  = "<table width='500' cellspacing='2' cellpadding='2' border='0' align='left'>";
         $xmlMessageBody .= "<tr>
                           <td  style=\"font:bold 18px Calibri;color:#9C0D0D;\" align=\"left\">QUOTEZONE</td>
                           <td style=\"font:bold 12px Calibri;color:black;\" align=\"right\">$dateAndTime</td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML REQUEST:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$xmlData</td>
                        </tr>";
         $xmlMessageBody .= "<tr height=\"10\">
                           <td colspan=\"2\">
                              &nbsp;
                           </td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML RESPONSE:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$result</td>
                        </tr>";
         $xmlMessageBody .= "</table>";

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi Brightside Orbit - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi Brightside Orbit - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if( preg_match("/<RESPONSE_CODE\>OK\<\/RESPONSE_CODE\>/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
         else
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

         $sentContent =  "\nURL: ".$url."\n<br>PARAMS:".$xmlData;

         $this->DoOutboundFiles("xml", $sentContent,$result);
      }
      
      if(($this->session["_COMPANY_DETAILS_"]['2283']["REQUEST_XML"] == 1) AND ($this->siteID == 2283)  AND (! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk"))
      {           
         $subject = "Taxi query : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];
              
         $quoteRef      = $this->session['_QZ_QUOTE_DETAILS_']['quote_reference'];
         $firstName     = $this->session['_YourDetails_']['first_name'];
         $lastName      = $this->session['_YourDetails_']['surname'];
         $email         = $this->session['_YourDetails_']['email_address'];
         $houseNrName   = $this->session['_YourDetails_']['house_number_or_name'];
         $houseNrName   = str_replace("&","and",$houseNrName);

         
         $DOB  = $this->session['_YourDetails_']['date_of_birth_dd']."-".$this->session['_YourDetails_']['date_of_birth_mm']."-".$this->session['_YourDetails_']['date_of_birth_yyyy'];
         $DIS  = $this->session['_YourDetails_']['date_of_insurance_start_dd']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_yyyy']." ".date("H:m");
         $inceptionDate = $this->session['_YourDetails_']['date_of_insurance_start_dd']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_yyyy'];
         $postcode      = $this->session['_YourDetails_']['postcode_prefix'].$this->session['_YourDetails_']['postcode_number'];
         $telephone     = $this->session['_YourDetails_']['daytime_telephone'];
         $mobilePhone   = $this->session['_YourDetails_']['mobile_telephone'];
         $taxiMake      = $this->session['_YourDetails_']['taxi_make'];
         $taxiModel     = $this->session['_YourDetails_']['taxi_model'];
         
         if($this->session['_YourDetails_']['daytime_telephone'] == "")
            $telephone  = $this->session['_YourDetails_']['mobile_telephone'];

         if($this->session['_YourDetails_']['mobile_telephone'] == "")
            $mobilePhone = $this->session['_YourDetails_']['daytime_telephone'];
             
         $estimatedValue = $this->session['_YourDetails_']['estimated_value'];
         $quoteDateTime  = date("d-m-Y H:i");


         $xmlData    = '<?xml version="1.0" encoding="utf-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:brig="http://www.brightsideinsurance.co.uk/">
            <soapenv:Header/>
            <soapenv:Body>
               <brig:UploadLead>
               <!--Optional:-->
               <brig:RecordXML>
                  &lt;TopXLeadRequest&gt;
                     &lt;QuoteReference&gt;'.$quoteRef.'&lt;/QuoteReference&gt;
                     &lt;Forename&gt;'.$firstName.'&lt;/Forename&gt;
                     &lt;Surname&gt;'.$lastName.'&lt;/Surname&gt;
                     &lt;DateOfBirth&gt;'.$DOB.'&lt;/DateOfBirth&gt;
                     &lt;PhoneNumber1&gt;'.$telephone.'&lt;/PhoneNumber1&gt;
                     &lt;PhoneNumber2&gt;'.$mobilePhone.'&lt;/PhoneNumber2&gt;
                     &lt;EmailAddress1&gt;'.$email.'&lt;/EmailAddress1&gt;
                     &lt;AddressLine1&gt;'.$houseNrName.'&lt;/AddressLine1&gt;
                     &lt;Postcode&gt;'.$postcode.'&lt;/Postcode&gt;
                     &lt;Make&gt;'.$taxiMake.'&lt;/Make&gt;
                     &lt;Model&gt;'.$taxiModel.'&lt;/Model&gt;
                     &lt;Premium&gt;&lt;/Premium&gt;
                     &lt;TopXPosition&gt;1&lt;/TopXPosition&gt;
                     &lt;InceptionDate&gt;'.$inceptionDate.'&lt;/InceptionDate&gt;
                     &lt;QuoteProviderID&gt;14&lt;/QuoteProviderID&gt;                            
                     &lt;QuoteTypeID&gt;170&lt;/QuoteTypeID&gt;
                     &lt;QuoteDateTime&gt;'.$quoteDateTime.'&lt;/QuoteDateTime&gt;
                  &lt;/TopXLeadRequest&gt;
               </brig:RecordXML>
               </brig:UploadLead>
            </soapenv:Body>
         </soapenv:Envelope>';

         $url = "https://leadsource.brightsidegroup.co.uk/insert.asmx";

         $ch = curl_init();

         $header[] = "POST /insert.asmx HTTP/1.1";
         $header[] = "Host: leadsource.brightsidegroup.co.uk";
         // $header[] = "Content-Type: application/soap+xml; charset=utf-8";
         $header[] = "Content-Type: text/xml; charset=utf-8";
         $header[] = "Content-length: ".strlen($xmlData);
         $header[] = "SOAPAction: \"http://www.brightsideinsurance.co.uk/UploadLead\"";
         $header[] = "Connection: close \r\n";
         $header[] = $xmlData;

         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
         curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 180);

         curl_setopt($ch, CURLOPT_VERBOSE, 1);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_HEADER, 1);

         $result  = curl_exec($ch);
         $erro    = curl_error($ch);
         $errNo   = curl_errno($ch);

         if(! preg_match("/Record Accepted/i",$result))
         {
            //send email with the failed response
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!

            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Brightside 2 - XML Method - RETRY";
            $mailBody    = $xmlMessageBody;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Brightside 2 - XML Method - RETRY";
            $mailBody    = $xmlMessageBody;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

            //sleep 10 seconds then retry to send thed deta
            sleep(10);

            //rerun the request
            $result  = curl_exec($ch);
            $erro    = curl_error($ch);
            $errNo   = curl_errno($ch);
         }
        
         curl_close ($ch);

         $xmlMessageBody  = "<table width='500' cellspacing='2' cellpadding='2' border='0' align='left'>";
         $xmlMessageBody .= "<tr>
                           <td  style=\"font:bold 18px Calibri;color:#9C0D0D;\" align=\"left\">QUOTEZONE</td>
                           <td style=\"font:bold 12px Calibri;color:black;\" align=\"right\">$dateAndTime</td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML REQUEST:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$xmlData</td>
                        </tr>";
         $xmlMessageBody .= "<tr height=\"10\">
                           <td colspan=\"2\">
                              &nbsp;
                           </td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML RESPONSE:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$result [$erro [$errNo]]</td>
                        </tr>";
         $xmlMessageBody .= "</table>";

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi Brightside 2 - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." Taxi Brightside 2 - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers,$this->session,"XML",$this->siteID);

         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if( preg_match("/Record Accepted/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
         else
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

         $sentContent =  "\nURL: ".$url."\n<br>PARAMS:".$xmlData;

         $this->DoOutboundFiles("xml", $sentContent,$result);
      }
   }
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: PrepareUrl
//
// [DESCRIPTION]:   Prepare a remote site config
//
// [PARAMETERS]:    $url = ""
//
// [RETURN VALUE]:  string, prepared url
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function PrepareUrl($url = "")
{
   $url = $this->siteConfig['nextUrl'];
   $this->httpReferer = $this->siteConfig['refererUrl'];
   return $url;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Get form name of current page
//
// [PARAMETERS]:    array
//
// [RETURN VALUE]:  the form name|_body_form
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetFormName($formParams)
{

   if(count($formParams) == 1)
      return "_body_form";

   foreach ($formParams as $formName => $value)
      if($formName != '_body_form')
         return $formName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessResponse()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessLastPage
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessLastPage()
{
   print "QUOTE SUCCESS";
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckResponseErrors
//
// [DESCRIPTION]:   Check an html response page for errors. Extract errors.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckResponseErrors()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetResultElements
//
// [DESCRIPTION]:   Get the results from the last page
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetResultElements()
{
   return;
} // end function GetResultElements(&$elementArray)

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ParseXmlResponse
//
// [DESCRIPTION]:   Get the results from the last page only for a broker
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  false | array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ParseXmlResponse()
{
   return;
}
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetXmlTemplate()
   //
   // [DESCRIPTION]:   -
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetXmlTemplate($type='',$d='', $sd='')
   {
      return;
   }


   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareXml
   //
   // [DESCRIPTION]:   Prepare the xml to send to PD
   //
   // [PARAMETERS]:    $SESSION
   //
   // [RETURN VALUE]:  xml data to send
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareXml($SESSION)
   {

   return;

   }// end function PrepareXml($SESSION)
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareQuote
   //
   // [DESCRIPTION]:   Prepare the array of params to get a quote
   //
   // [PARAMETERS]:    $sessionParams ($_SESSION);
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-11-11
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareQuote()
   {
      $this->DoOutbound();

      return;
   }// end function PrepareQuote()

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetUrl
   //
   // [DESCRIPTION]:   Download/Open a remote web page
   //
   // [PARAMETERS]:    $url="", [method="GET"], [$params=""] [$referer=""]
   //
   // [RETURN VALUE]:  downloaded page text if success, "" otherwise
   //
   // [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetUrl($url="", $method="GET", $params="", $referer="")
   {

      $this->htmlContent = "";

      if(empty($url))
      {
         $this->strERR = GetErrorString("INVALID_URL");
         return false;
      }

      $this->proto = "https://";

      // add protocol
      if(! preg_match("/:\/\//", $url))
         $url = $this->proto.$url;

      if(is_array($params))
      {
         $paramString = "";

         foreach($params as $key => $value)
            $paramString .= $key . '=' . $value . '&';

         $paramString = preg_replace("/\&$/","", $paramString);
         $params = $paramString;
      }

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      curl_setopt($ch, CURLOPT_URL,$url);

      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);

      //curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HEADER, 1);

      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; X11; Linux i686) Opera 7.54  [en]');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      $result = curl_exec($ch);

      curl_close ($ch);

      if(empty($result))
      {
         $this->strERR  = "CANNOT_GET_REMOTE_URL";
         $this->strERR .= curl_error($ch)."\n";
         return false;
      }

      preg_match("/^(.*)\n(.*)$/is", $result,$matches);

      $this->httpHeader  = $matches[1];
      $this->htmlContent = $matches[2];

      //print "\n======== result ===========\n".$this->htmlContent."\n=========================\n";die;
      return $result;
   }

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: rand_str
   //
   // [DESCRIPTION]:   Generate a random character string
   //
   // [PARAMETERS]:    $length = 0, $chars = 'ABCDEFabcdef0123456789'
   //
   // [RETURN VALUE]:  string
   //
   // [CREATED BY]:    Ciprian Sturza (ciprian.sturza@seopa.com) 2010-05-28
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function rand_str($length = 0, $chars = 'ABCDEFabcdef0123456789')
   {
      // Length of character list
      $chars_length = (strlen($chars) - 1);

      // Start our string
      $string = $chars{rand(0, $chars_length)};

      // Generate random string
      for ($i = 1; $i < $length; $i = strlen($string))
      {
         // Grab a random character from our list
         $r = $chars{rand(0, $chars_length)};

         $string .=  $r;
      }

      return $string;
   }


} // end of CTaxiMasterEngine class

?>

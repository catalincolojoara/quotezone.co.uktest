<?php

/*****************************************************************************/
/*                                                                           */
/*   CTaxiPlan7Engine class interface                                        */
/*                                                                           */
/*  (C) 2008 Furtuna Alexandru (alexandru.furtuna@seopa.com)                              */
/*                                                                           */
/*****************************************************************************/
include_once "TAXIEngine.php";
include_once "Url.php";
include_once "Site.php";
include_once "Session.php";
include_once "ArList.php";
include_once "Navigation.php";
include_once "Postcode.php";
include_once "functions.php";
include_once "YourDetailsElements.php";
include_once "EmailContent.php";

//outbound nedded
include_once "LeadsCompanyEmails.php";
include_once "SMTP.php";
include_once "File.php";
include_once "Outbound.php";

error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CTaxiPlan7Engine
//
// [DESCRIPTION]:  CTaxiPlan7Engine class interface, remote site data exchange support,
//                 PHP version
//
// [FUNCTIONS]:   bool   InitSite()
//                string PrepareUrl($url = "")
//                bool   ProcessResponse()
//                bool   ProcessLastPage()
//                bool   CheckResponseErrors()
//                array  GetResultElements()
//                void   PrepareQuote($SESSION)
//
// [CREATED BY]:   Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CTaxiPlan7Engine extends CTAXIEngine
{

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CTaxiPlan7Engine
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CTaxiPlan7Engine($fileName, $scanningFlag, $debugMode, $testingMode)
{
   CTAXIEngine::CTAXIEngine("taxi-insurance-plan.quotezone.co.uk", $fileName, $scanningFlag, $debugMode, $testingMode);

   $this->SetFailureResponse("There are errors on this page, please correct");

   $this->SetUnableQuoteOnlineServerMessage("Unfortunately we are unable to provide you with an on-line aplication");
   $this->SetOfflineServerMessage("The online aplication system is currently offline");

   $this->SetProtocol("https");

   $this->siteID = "3430";
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InitSite
//
// [DESCRIPTION]:   Initialise a remote site session
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InitSite()
{
   return true;
}

/////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DoOutbound
//
// [DESCRIPTION]:   send outbound
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Moraru Valeriu (null@seopa.com) 2011-04-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DoOutbound()
{
   //error_reporting(E_ALL);

   global $ACRUX_IP_ARRAY;
   global $_YourDetails;

   $objLeadsCompanyEmails = new CLeadsCompanyEmailsModule();

   $outboundObj         = new COutbound();
   $mailObj             = new CSMTP();
   $objFile             = new CFile();

   CTAXIEngine::DoOutbound();

   $logID  = $this->session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];

   if($this->outbounds['MAIL'])
   {
      // get emails to send from db
      $sendEmailsArray = array();
      $sendEmailsArray = $objLeadsCompanyEmails->GetAllEmailsToSendForCompanyBySiteID($this->siteID);

      print_r($sendEmailsArray);

      $message_body = GetEmailTemplate($this->session);

      $subject = "Taxi query : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];

      $headers  = 'MIME-Version: 1.0' . "\r\n";
      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
      ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
      $headers .= "X-Mailer: PHP\r\n"; // mailer
      $headers .= "X-Priority: 1\r\n"; // Urgent message!
      ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

      $mailFrom    = "taxi@quotezone.co.uk";
      $mailSubject = $subject;
      $mailBody    = $message_body;
      $emailId     = "";
      $mailToAll   = "";

      foreach($sendEmailsArray as $emailType=>$emailsArray)
      {
         $countEmails = count($emailsArray);

         switch($emailType)
         {
            case "COMPANY":
               if( ! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")
               {
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject;

                     $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }
               }
               break;

            case "MARKETING":

               if( ! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")
               {
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject." - Plan Insurance 7";

                     $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }
               }
               break;

            case "TECHNICAL":

               //send emails to TECHNICAL department
               for($k=0;$k<$countEmails;$k++)
               {
                  $mailTo      = $emailsArray[$k];
                  print "send to:".$mailTo."\n";

                  $mailSubject = $subject." - Plan Insurance 7";

                  $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                  $emailId   .= $mailObj->GetMessageID().",";
                  $mailToAll .= $mailTo.",";
               }
               break;
         }// switch
      }// foreach

      // remove last coma
      $emailId   = substr($emailId,0,-1);
      $mailToAll = substr($mailToAll,0,-1);

      $outboundObj->UpdateOutboundField($this->outbounds['MAIL'],'sent_status', 1);
      $outboundObj->UpdateOutboundField($this->outbounds['MAIL'],'extra_params', $emailId);

      $sentContent =  "\nHEADERS: ".$headers."\nFROM:".$mailFrom."\nTO:".$mailToAll."\nSUBJECT: ".$mailSubject."\nBODY: ".$mailBody;

      $this->DoOutboundFiles("mail", $sentContent);

   } //end if MAIL


   if($this->outbounds['XML'])
   {
     if(! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")
        {
					 $dateAndTime         = date("Y/m/d H:i:s");
                $now = date("H:i:s");
                $daytimeTelephone = $this->session['_YourDetails_']['daytime_telephone'];
                $mobileTelephone = $this->session['_YourDetails_']['mobile_telephone'];

                $phone = $mobileTelephone;
                if(! $phone)
                   $phone = $daytimeTelephone;

                $firstName = $this->session['_YourDetails_']['first_name'];
                $surname = $this->session['_YourDetails_']['surname'];
                $name = $firstName." ".$surname;
                $dateOfBirth = $this->session['_YourDetails_']['date_of_birth'];
                $dateOfInsuranceStart = $this->session['_YourDetails_']['date_of_insurance_start_dd']."/".$this->session['_YourDetails_']['date_of_insurance_start_mm']."/".$this->session['_YourDetails_']['date_of_insurance_start_yyyy'];
            $postcode = $this->session['_YourDetails_']['postcode'];
            $emailAddress = $this->session['_YourDetails_']['email_address'];
            $taxiUsedFor = $_YourDetails["taxi_used_for"][$this->session['_YourDetails_']['taxi_used_for']];
            $taxiType = $_YourDetails["taxi_type"][$this->session['_YourDetails_']['taxi_type']];
            $typeOfCover = $_YourDetails["type_of_cover"][$this->session['_YourDetails_']['type_of_cover']];
            $taxiMake = $this->session['_YourDetails_']['taxi_make'];
            $taxiModel = $this->session['_YourDetails_']['taxi_model'];
            $manufactureYear = $this->session['_YourDetails_']['year_of_manufacture'];
            $vehicleMileage = $_YourDetails['vehicle_mileage'][$this->session['_YourDetails_']['vehicle_mileage']];
            $estimatedValue = $this->session['_YourDetails_']['estimated_value'];
            $taxiCapacity = $_YourDetails["taxi_capacity"][$this->session['_YourDetails_']['taxi_capacity']];
            $taxiNcb = $_YourDetails["taxi_ncb"][$this->session['_YourDetails_']['taxi_ncb']];
            $platingAuth = $this->session['_YourDetails_']['plating_authority'];
            $vehicleRegistration = $this->session['_YourDetails_']['vehicle_registration_number'];
            $licencePeriod = $_YourDetails["period_of_licence"][$this->session['_YourDetails_']['period_of_licence']];
            $taxiBadge = $_YourDetails["taxi_badge"][$this->session['_YourDetails_']['taxi_badge']];
            $claims5years = $this->session['_YourDetails_']['claims_5_years'];
            $convictions5years = $this->session['_YourDetails_']['convictions_5_years'];

            $stringToSend = "";
                //$stringToSend .= "FIRST NAME/SURNAME  ".$firstName."/".$surname."\n";
                $stringToSend .= "POSTCODE  ".$postcode."\n";
                $stringToSend .= "DATE OF BIRTH  ".$dateOfBirth."\n";
                $stringToSend .= "DATE OF INSURANCE START  ".$dateOfInsuranceStart."\n";
                $stringToSend .= "TAXI USED FOR  ".$taxiUsedFor."\n";
                $stringToSend .= "TAXI TYPE  ".$taxiType."\n";
                $stringToSend .= "TYPE OF COVER  ".$typeOfCover."\n";
                $stringToSend .= "TAXI MAKE  ".$taxiMake."\n";
                $stringToSend .= "TAXI MODEL  ".$taxiModel."\n";
                $stringToSend .= "YEAR OF MANUFACTURE  ".$manufactureYear."\n";
                $stringToSend .= "VEHICLE MILEAGE  ".$vehicleMileage."\n";
                $stringToSend .= "ESTIMATED VALUE  ".$estimatedValue."\n";
                $stringToSend .= "TAXI CAPACITY  ".$taxiCapacity."\n";
                $stringToSend .= "TAXI NO CLAIMS BONUS  ".$taxiNcb."\n";
                $stringToSend .= "TAXI PLATING AUTHORITY  ".$platingAuth."\n";
                $stringToSend .= "TAXI REGISTRATION  ".$vehicleRegistration."\n";
                $stringToSend .= "FULL UK LICENCE  ".$licencePeriod."\n";
                $stringToSend .= "TAXI BADGE  ".$taxiBadge."\n";
                $stringToSend .= "CLAIMS LAST 5 YEARS  ".$claims5years."\n";
                $stringToSend .= "CONVICTIONS LAST 5 YEARS  ".$convictions5years."\n";
                $stringToSend .= "Mobile  ".$mobileTelephone."\n";
                $stringToSend .= "TELEPHONE  ".$daytimeTelephone."\n";
                $stringToSend .= "Email  ".$emailAddress;

                $stringToSend = str_replace("&", "&amp;", $stringToSend);



                    $xmlString = '<?xml version="1.0" encoding="utf-8"?>
                                                        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                                                          <soap:Body>
                                                            <CallMePlease xmlns="http://planinsurance.co.uk/webservices">
                                                              <callTime>'.$now.'</callTime>
                                                              <source>[TAXI NB QUOTEZONE C3 OFFICE HOURS]</source>
                                                              <phone>'.$phone.'</phone>
                                                              <name>'.$name.'</name>
                                                              <company></company>
                                                              <notes>'.$stringToSend.'</notes>
                                                            </CallMePlease>
                                                          </soap:Body>
                                                        </soap:Envelope>';

                    $url = "http://call.office.planinsurance.co.uk/CallMeNow.asmx";
                        //print $xmlString."\n\n";
                        $ch = curl_init();

                        $header[] = "POST /CallMeNow.asmx HTTP/1.1";
                        $header[] = "Host: call.office.planinsurance.co.uk";
                        $header[] = "Content-Type: text/xml; charset=utf-8";
                        $header[] = "Content-length: ".strlen($xmlString);
                        $header[] = "SOAPAction: \"http://planinsurance.co.uk/webservices/CallMePlease\"";
                        $header[] = "Cache-Control: no-cache";
                        $header[] = "Connection: close \r\n";
                        $header[] = $xmlString;

                        curl_setopt($ch, CURLOPT_URL,$url);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 180);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        //curl_setopt($ch, CURLOPT_VERBOSE, true);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        $result = curl_exec($ch);

                        curl_close ($ch);

                                 $xmlMessageBody = "<table width='500' cellspacing='2' cellpadding='2' border='0' align='left'>";
         $xmlMessageBody .= "<tr>
                           <td  style=\"font:bold 18px Calibri;color:#9C0D0D;\" align=\"left\">QUOTEZONE</td>
                           <td style=\"font:bold 12px Calibri;color:black;\" align=\"right\">$dateAndTime</td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML REQUEST:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$xmlString</td>
                        </tr>";
         $xmlMessageBody .= "<tr height=\"10\">
                           <td colspan=\"2\">
                              &nbsp;
                           </td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML RESPONSE:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$result</td>
                        </tr>";
         $xmlMessageBody .= "</table>";

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject. " Taxi Plan 7 - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);

         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject. " Taxi Plan 7 - XML Method ";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
         
                        //print $result;

                        $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

                        if(! strlen($result))
                  $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 3); //NO RESPONSE

                if( preg_match("/<CallMePleaseResult>true<\/CallMePleaseResult>/i",$result))
                  $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1); //OK RESPONSE
                else
                  $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);//BAD RESPONSE

                $this->DoOutboundFiles("xml", $xmlString, $result);

//          $Name      = "Plan Insurance 4 XML";
//          $email     = "taxi@quotezone.co.uk";
//          $recipient = "bogdan.turi@seopa.com";
//          $mail_body = $xmlString." \n\n ".$result."\n\n";
//          $subject   = "Plan Insurance 4 XML REQUEST : $now";
//          $header    = "From: ". $Name . " <" . $email . ">\r\n";
//          mail($recipient, $subject, $mail_body, $header);

        }

   }
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: PrepareUrl
//
// [DESCRIPTION]:   Prepare a remote site config
//
// [PARAMETERS]:    $url = ""
//
// [RETURN VALUE]:  string, prepared url
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function PrepareUrl($url = "")
{
   $url = $this->siteConfig['nextUrl'];
   $this->httpReferer = $this->siteConfig['refererUrl'];
   return $url;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Get form name of current page
//
// [PARAMETERS]:    array
//
// [RETURN VALUE]:  the form name|_body_form
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetFormName($formParams)
{

   if(count($formParams) == 1)
      return "_body_form";

   foreach ($formParams as $formName => $value)
      if($formName != '_body_form')
         return $formName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessResponse()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessLastPage
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessLastPage()
{
   print "QUOTE SUCCESS";
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckResponseErrors
//
// [DESCRIPTION]:   Check an html response page for errors. Extract errors.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckResponseErrors()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetResultElements
//
// [DESCRIPTION]:   Get the results from the last page
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetResultElements()
{
   return;
} // end function GetResultElements(&$elementArray)

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ParseXmlResponse
//
// [DESCRIPTION]:   Get the results from the last page only for a broker
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  false | array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ParseXmlResponse()
{
   return;
}
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetXmlTemplate()
   //
   // [DESCRIPTION]:   -
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetXmlTemplate($type='',$d='', $sd='')
   {
      return;
   }


   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareXml
   //
   // [DESCRIPTION]:   Prepare the xml to send to PD
   //
   // [PARAMETERS]:    $SESSION
   //
   // [RETURN VALUE]:  xml data to send
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareXml($SESSION)
   {

   return;

   }// end function PrepareXml($SESSION)
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareQuote
   //
   // [DESCRIPTION]:   Prepare the array of params to get a quote
   //
   // [PARAMETERS]:    $sessionParams ($_SESSION);
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-11-11
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareQuote()
   {
      $this->DoOutbound();

      return;
   }// end function PrepareQuote()


   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetUrl
   //
   // [DESCRIPTION]:   Download/Open a remote web page
   //
   // [PARAMETERS]:    $url="", [method="GET"], [$params=""] [$referer=""]
   //
   // [RETURN VALUE]:  downloaded page text if success, "" otherwise
   //
   // [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetUrl($url="", $method="GET", $params="", $referer="")
   {

      $this->htmlContent = "";

      if(empty($url))
      {
         $this->strERR = GetErrorString("INVALID_URL");
         return false;
      }

      $this->proto = "https://";

      // add protocol
      if(! preg_match("/:\/\//", $url))
         $url = $this->proto.$url;

      if(is_array($params))
      {
         $paramString = "";

         foreach($params as $key => $value)
            $paramString .= $key . '=' . $value . '&';

         $paramString = preg_replace("/\&$/","", $paramString);
         $params = $paramString;
      }

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      curl_setopt($ch, CURLOPT_URL,$url);

      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);

      //curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HEADER, 1);

      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; X11; Linux i686) Opera 7.54  [en]');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      $result = curl_exec($ch);

      curl_close ($ch);

      if(empty($result))
      {
         $this->strERR  = "CANNOT_GET_REMOTE_URL";
         $this->strERR .= curl_error($ch)."\n";
         return false;
      }

      preg_match("/^(.*)\n(.*)$/is", $result,$matches);

      $this->httpHeader  = $matches[1];
      $this->htmlContent = $matches[2];

      //print "\n======== result ===========\n".$this->htmlContent."\n=========================\n";die;
      return $result;
   }


} // end of CTaxiPlan7Engine class

?>



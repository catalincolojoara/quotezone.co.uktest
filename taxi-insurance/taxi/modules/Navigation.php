<?php
error_reporting(0);
session_start();

class CNavigation
{
   var $navigationList;

   var $activeNavigationStep;

   var $session;
   var $post;
   var $get;

   function CNavigation($SESSION)
   {
      $this->navigationList            = array(); // define all navigation steps

      if(! empty($SESSION))
         $_SESSION = $SESSION;
 
      if(empty($_SESSION['ACTIVE STEP']))
         $_SESSION['ACTIVE STEP'] = 1;

      $this->SetDefaultNavigation();

      $this->activeNavigationStep      = $_SESSION['ACTIVE STEP'];       // active step
      $this->post = $_POST;
      $this->get  = $_GET;

     if($this->IsBackStep())
        $this->SetBackwardStep();

      $keys = $this->GetNavigationListKeys($_SESSION['ACTIVE STEP']);

      $primaryKey   = $keys['primary_key'];
      $secondaryKey = $keys['secondary_key'];
      $thirdKey     = $keys['third_key'];

      $this->SetMenuLink($_SESSION['ACTIVE STEP'], $primaryKey." ".$secondaryKey." ".$thirdKey);

   }

   function SetActiveNavigationStep($step = '')
   {
      if(empty($step))
         return false;

      if($step < 1)
         return false;

      $this->activeNavigationStep = $step;

      $_SESSION['ACTIVE STEP'] = $step;

      return true;
   }

   function IsNextStep()
   {
      if($this->post['step'] == 'next')
         return true;

      return false;
   }

   function IsBackStep()
   {
      if($this->post['step'] == 'back')
         return true;

      return false;
   }

   function GetActiveNavigationStep()
   {
      return $this->activeNavigationStep;
   }

   function GetIncludeFilename()
   {
      $activeStep = $this->GetActiveNavigationStep();

      $category = $this->navigationList[$activeStep]['primary_key'];

      preg_match("/^_(.+)_$/", $category, $match);

      $filename = $match[1].".php";

      return $filename;
   }

   function SetDefaultNavigation()
   {

      $this->navigationList['1']['primary_key']          = '_YourDetails_';
      $this->navigationList['1']['secondary_key']        = '';
      $this->navigationList['1']['third_key']            = '';

      $this->navigationList['2']['primary_key']          = '_QUOTE_RESULTS_';
      $this->navigationList['2']['secondary_key']        = '';
      $this->navigationList['2']['third_key']            = '';


//        print "<pre>";
//         print_r($this->navigationList);
//        print "</pre>";

   }

   function GetNavigationListKeys($step)
   {
      return $this->navigationList[$step];
   }

   function SetNextStep()
   {
      $this->activeNavigationStep++;
   }

   function SetBackStep()
   {
      if($this->activeNavigationStep == 1)
         return;

      $this->activeNavigationStep--;
       $_SESSION['ACTIVE STEP'] = $this->activeNavigationStep;

   }

   function SetForwardStep()
   {
      // here we have to check the particular situation of each parameters steps.
      $activeStep = $this->GetActiveNavigationStep();

      //print($activeStep);die;
      $activeStep++;

      if($activeStep > count($this->navigationList))
         return;

      $this->SetActiveNavigationStep($activeStep);

      // we have to check of the current step is active
      // for this we need all step conditions
      if(! $this->CheckForwardStep())
      {
         if($activeStep == count($this->navigationList))
            return;

         unset($_SESSION['_MENU_'][$activeStep]);

         $this->SetForwardStep();
      }

      if($activeStep <= count($this->navigationList))
         $this->redirect();
   }

   function CheckForwardStep()
   {
      $activeStep = $this->GetActiveNavigationStep();

      $keys = $this->GetNavigationListKeys($activeStep);

      $primaryKey   = $keys['primary_key'];
      $secondaryKey = $keys['secondary_key'];
      $thirdKey     = $keys['third_key'];

      if(! $this->CheckStepConditions($primaryKey, $secondaryKey, $thirdKey))
      {
         return false;
      }
 
      return true;
   }

   function CheckStepConditions($category, $idDriver, $idDriverHistory)
   {

      switch($category)
      {
         case '_YourDetails_':
            return true;
            break;

         case '_YourCover_':
            return true;
            break;

         case '_YourQuotes_':
            return true;
            break;

         case '_QUOTE_SUMMARY_':
            return true;
            break;

         case '_QUOTE_RESULTS_':
            return true;
            break;
      }
   }


   function SetForwardStepOld()
   {
      // here we have to check the particular situation of each parameters steps.
      $activeStep = $this->GetActiveNavigationStep();

      $activeStep++;

      if($activeStep > count($this->navigationList))
         return;

      $this->SetActiveNavigationStep($activeStep);

      if($activeStep <= count($this->navigationList))
         $this->redirect();
   }

   function SetBackwardStep()
   {
      // here we have to find the last step from active step wich have an entry into session
      $activeStep = $this->GetActiveNavigationStep();

      if($activeStep == 1)
         return;

      while(true)
      {
         $activeStep--;

         $keys = $this->GetNavigationListKeys($activeStep);

         if( $this->ExistSessionEntry($keys['primary_key'], $keys['secondary_key'], $keys['third_key']))
         {
            $this->SetActiveNavigationStep($activeStep);
            break;
         }

         if($activeStep == 1)
            break;
      }

      $this->redirect();
   }

   function ExistSessionEntry($primaryKey='', $secondaryKey='', $thirdKey='')
   {
      if($thirdKey === '' && $secondaryKey === '')
      {
         if(! empty($_SESSION[$primaryKey]))
            return true;
      }

      if($thirdKey === '')
      {
         if(! empty($_SESSION[$primaryKey][$secondaryKey]))
            return true;
      }

      if(! empty($_SESSION[$primaryKey][$secondaryKey][$thirdKey]))
         return true;

      return false;
   }

   function GetSessionContent($primaryKey='', $secondaryKey='', $thirdKey='')
   {
      if($thirdKey !== '')
      {
         if($primaryKey === '' || $secondaryKey === '')
            return false;

         return $_SESSION[$primaryKey][$secondaryKey][$thirdKey];
      }

      if($secondaryKey !== '')
      {
         if($primaryKey === '')
            return false;

         return $_SESSION[$primaryKey][$secondaryKey];
      }
      
      if($primaryKey !== '')
      {
         return $_SESSION[$primaryKey];
      }

      return false;
   }

   function SetSessionContent($key='', $value='', $primaryKey='', $secondaryKey='', $thirdKey='')
   {
      if($key === '')
         return false;

      if($thirdKey !== '')
      {
         if($primaryKey === '' || $secondaryKey === '')
            return false;

         $_SESSION[$primaryKey][$secondaryKey][$thirdKey][$key] = $value;

         return true;
      }

      if($secondaryKey !== '')
      {
         if($primaryKey === '')
            return false;

         $_SESSION[$primaryKey][$secondaryKey][$key] = $value;

         return true;
      }
      
      if($primaryKey !== '')
      {
         $_SESSION[$primaryKey][$key] = $value;

         return true;
      }

      return false;
   }

   function DeleteSessionContent($primaryKey='', $secondaryKey='', $thirdKey='')
   {
      if($thirdKey !== '')
      {
         if($primaryKey === '' || $secondaryKey === '')
            return false;

         unset($_SESSION[$primaryKey][$secondaryKey][$thirdKey]);

         if(empty($_SESSION[$primaryKey][$secondaryKey]))
            unset($_SESSION[$primaryKey][$secondaryKey]);

         return true;
      }

      if($secondaryKey !== '')
      {
         if($primaryKey === '')
            return false;

         unset($_SESSION[$primaryKey][$secondaryKey]);

         if(empty($_SESSION[$primaryKey]))
            unset($_SESSION[$primaryKey]);

         return true;
      }

      
      if($primaryKey !== '')
      {
         unset($_SESSION[$primaryKey]);

         return true;
      }

      return false;
   }

   function GetNavigationList()
   {
      return $this->navigationList;
   }

   function IsLastStepNavigation()
   {
      if($_SESSION['ACTIVE STEP'] == count($this->navigationList))
         return true;

      return false;
   }

   function GetSessionCount($primaryKey='', $secondaryKey='', $thirdKey='')
   {
      if($thirdKey !== '')
      {
         if($primaryKey === '' || $secondaryKey === '')
            return 0;

         if(empty($_SESSION[$primaryKey][$secondaryKey][$thirdKey]))
            return 0;

         return count($_SESSION[$primaryKey][$secondaryKey][$thirdKey]);
      }

      if($secondaryKey !== '')
      {
         if($primaryKey === '')
            return 0;

         if(empty($_SESSION[$primaryKey][$secondaryKey]))
            return 0;


         return count($_SESSION[$primaryKey][$secondaryKey]);
      }
      
      if($primaryKey !== '')
      {
         if(empty($_SESSION[$primaryKey]))
            return 0;

         if(! is_array($_SESSION[$primaryKey]))
            return 0;

         $counter = 0;
         
         foreach($_SESSION[$primaryKey] as $secondaryKey => $secondaryArray)
         {   
            foreach($_SESSION[$primaryKey][$secondaryKey] as $thirdKey => $thirdArray)
            {
               if(! preg_match("/\d/",$thirdKey))                
                  return count($_SESSION[$primaryKey]);

               $counter++;
            }
         }
         
         return $counter;
         
      }

      return 0;

   }

   function SetMenuLink($step, $section)
   {
      $_SESSION[_MENU_][$step] = $section;
      // sorting by step
      ksort($_SESSION[_MENU_]);
   }

   function GenerateMenuLinks()
   {
      if(empty($_SESSION[_MENU_]))
         return false;
  
      //print_r($_SESSION);

      foreach($_SESSION[_MENU_] as $step => $sectionName)
      {
         $contentsLinks .= "<a href=\"index.php?step=$step\">$sectionName</a><br>";
      }

      return $contentsLinks;
   }

   function redirect($url="index.php")
   {
      if(! preg_match("/cs=/i", $url))
      {
         if(! empty($_GET['cs']))
            $cookieSession = $_GET['cs'];

         if(! empty($_POST['cs']))
            $cookieSession = $_POST['cs'];

         if( defined('COOKIE_SESSION_ID'))
           $cookieSession = COOKIE_SESSION_ID;

         if(! empty($cookieSession))
         {
            $prepUrl = $url."?cs=$cookieSession";
            if(preg_match("/\?/i", $url))
               $prepUrl = $url."&cs=$cookieSession";

            $url = $prepUrl;
         }
      }

      if(headers_sent())
      {
      
              
                  print <<<END_TAG
   
                  <script language="javascript">
                  <!--
                   location.href="$url";
                  //-->
                  </script>
   
END_TAG;
   
      }
      else
      {
         $today = date("D M j G:i:s Y");
         header("HTTP/1.1 200 OK");
         header("Date: $today");
         header("Location: $url");
         header("Content-Type: text/html");
      }
   
      exit(0); //if it doesn't work die (Javascript disabled?)
   }
}

?>

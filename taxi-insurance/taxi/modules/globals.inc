<?php

////////////////////////////////////////////////////////////////////////////////
///                       Global definitions area                            ///
////////////////////////////////////////////////////////////////////////////////

define("SEND_ONLY_FOR_ACRUX_MAIL", "0"); // used in SMTP.php in order to send to *@acrux.biz if is set "1"

define("DEBUG_MODE",   "0"); // debug mode used by some modules
define("TESTING_MODE", "0"); // testing mode used by application
define("LOGGING_MODE", "1"); // logging mode used by application
define("LANGUAGE",     "");  // language mode used by the temnplate class for multilanguage sites

define("ROOT_PATH", "/home/www/quotezone.co.uk/taxi-insurance/");

// set init path
ini_set("include_path", ROOT_PATH."modules/newdb:".ROOT_PATH.":".ROOT_PATH."taxi/modules:".ROOT_PATH."modules:".ROOT_PATH."taxi/steps:".ROOT_PATH."taxi/funcs");

include_once "common_globals.inc";
include_once "logo_information.inc";
include_once "fs.inc";
include_once "db.inc";
include_once "sql.inc";
include_once "cookie.php";

//DNA Insurance 3 - this will be removed after they finish the testing
$QZ_IP_ARRAY[] = "212.110.171.171";
$QZ_IP_ARRAY[] = "212.110.171.170";
$QZ_IP_ARRAY[] = "212.110.171.180";
$QZ_IP_ARRAY[] = "212.110.171.181";
$QZ_IP_ARRAY[] = "81.134.19.241";
$ACRUX_IP_ARRAY[] = "212.110.171.171";
$ACRUX_IP_ARRAY[] = "212.110.171.170";
$ACRUX_IP_ARRAY[] = "212.110.171.180";
$ACRUX_IP_ARRAY[] = "212.110.171.181";
$ACRUX_IP_ARRAY[] = "81.134.19.241";

// web site settings
define("WEB_SITE_ROOT", "https://taxi-insurance.quotezone.co.uk/taxi");
define("TEMPLATES_DIR", ROOT_PATH."taxi/templates");
define("PRELOAD_TEMPLATES", "Site.tmpl Header.tmpl Footer.tmpl");
define("WEB_SITE_FIRST_TMPL", "Site.tmpl");

// menu types
define("COMMON", 1);
define("SIMPLE_USER", 2);
define("ADVANCED_USER", 3);

// html section
define("HTML_BEGIN_ERROR", '<tr><td colspan="4" class="ErrTxt">');
define("HTML_END_ERROR", '</td></tr>');

define("HTML_BORDER_ERROR",'class="ErrBor"');
define("HTML_BACKGROUND_ERROR",'Err');
define("HTML_BACKGROUND_ERROR_C",'Errc');

/**
 * In this array we keep the sites that used gapOptIn system. The key is the siteID.
 */
$optInSitesArray = array(
  "671", // Fake
  "672", // Brightside - GapOptIn
  "673", // 2Gether - BreakdownOptIn
  "703", // Click4Gap - GapOptIn
);


?>

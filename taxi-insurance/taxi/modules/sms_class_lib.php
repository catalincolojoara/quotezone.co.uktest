<?php

include_once("db.inc");


include_once "../modules/globals.inc";
include_once "../steps/File.php";
include_once "../steps/FileStoreClient.php";
include_once "../steps/functions.php";
include_once "../steps/RerunSms.php";

// base class, shared by both sms_in and sms_out
class sms {

    private $dbHost = MASTER_DBHOST;
    private $dbUsername = MASTER_DBUSER;
    private $dbPassword = MASTER_DBPASS;
    private $dbName = MASTER_DBNAME;
    private $dbcon;
    private $logFileName = "logfile.txt";

    public function getDbcon() {
        return $this->dbcon;
    }

    public function getDbName() {
        return $this->dbName;
    }

    private function setDbcon($dbcon) {
        $this->dbcon = $dbcon;
    }

    public function getLogFileName() {
        return $this->logFileName;
    }

    public function setLogFileName($logFileName) {
        $this->logFileName = $logFileName;
    }

    public function mysqlConnect() {
        if (!$this->getDbcon()) {
// Connect to DB, select eligible users, create user object for each (including personalised message), then store in recipientArray (basic numeric array of user objects).
            $dbcon = mysql_connect($this->dbHost, $this->dbUsername, $this->dbPassword);
            if (!$dbcon) {
                $msg = "MySQL could not connect: " . mysql_error();
                $this->logToFile($msg);
                die($msg);
            }
            $db_selected = mysql_select_db($this->getDbName(), $dbcon);
            if (!$db_selected) {
                $msg = "MySql could not select DB: " . mysql_error();
                $this->logToFile($msg);
                die($msg);
            }
            $this->setDbcon($dbcon);
        }
    }

    public function mysqlDisconnect() {
        if ($this->getDbcon()) {
            mysql_close($this->getDbcon());
        }
    }

    public function mysqlQuery($query, $errorString = 'MySQL could not query: ') {
        if (!$this->getDbcon()) {
            $this->mysqlConnect();
        }
        $result = mysql_query($query);
        if (!$result) {
            $msg = $errorString . mysql_error();
            $this->logToFile($msg);
            die($msg);
        }
        return $result;
    }

    public function sentenceCase($s) {
        $str = strtolower($s);
        $cap = true;
        $ret = "";

        for ($x = 0; $x < strlen($str); $x++) {
            $letter = substr($str, $x, 1);
            if ($letter == "." || $letter == "!" || $letter == "?") {
                $cap = true;
            } elseif ($letter != " " && $cap == true) {
                $letter = strtoupper($letter);
                $cap = false;
            }

            $ret .= $letter;
        }

        return $ret;
    }

    public function formatMobileNumber($number) {
        if (!empty($number)) {
            $number = trim($number);
            $number = preg_replace('/\D/', '', $number);
            if (substr($number, 0, 2) == '44') {
                $number = "0" . substr($number, 2);
            }
            $number = substr($number, 0, 5) . "-" . substr($number, 5);
            if (substr($number, 0, 2) == '07') {
                return $number;
            } else {
                $msg = "ERROR - Ending as Mobile number does not appear to be valid.\n";
                $this->logToFile($msg);
                die($msg);
            }
        } else
            return $number;
    }

    public function logToFile($msg) {
// open file
        $fd = fopen($this->getLogFileName(), "a");
// append date/time to message
        $str = "[" . date("Y-m-d H:i:s", mktime()) . "] " . $msg;
// write string
        fwrite($fd, $str . "\n");
// close file
        fclose($fd);
    }

    public function replaceSystemNameHolder($searchString, $systemName) {
// first check if the string has the holder; if not we dont want to overwrite it
        if (stripos($searchString, "[SYSTEM_NAME]")) {
            $newString = str_replace("[SYSTEM_NAME]", $systemName, $searchString); // replace the holder text with the actual system name      
            /*
             * CURRENTLY REPLACING THE WHOLE STRING WITH JUST THE SYSTEM NAME - NEED TO UPDATE THE STRING INSTEAD AND ONLY REPLACE THE HOLDER, KEEPING THE OTHER TEXT
             */
            return $newString;
        }
        else
            return $searchString;
    }

    public function replaceFirstNameHolder($searchString, $firstName) {
        if (!stripos($searchString, "[FIRST_NAME]")) {
            $newString = str_replace("[FIRST_NAME]", $firstName, $searchString); // replace the holder text with the actual system name       
            return $newString;
        }
    }

    /*
     * TODO: Functions for interacting with our sms data, e.g. rebuilding message from ID and user details etc. Might be useful for reporting/interfaces etc
     */
}

// for sending sms messages
class sms_out extends sms {

    private static $validSystems = array(18 => "TAXI"); // an array of valid system names with their system IDs
    private static $validMessageKeys = array(1 => "RENEWAL_DEFAULT_1", 2 => "RENEWAL_FNAME_1"); // an array of valid message keys and their db IDs
    private $maxMessageLength = 160;
    private $defaultMessageKey = "RENEWAL_DEFAULT_1";
    private $daysToCheckForPrevSMS = 30; // how many days to check for a previous sent SMS, i.e. user hasnt already received an SMS within the last x days. 
    private $systemName;
    private $systemId;
    private $messageKey;
    private $messageID;
    private $message;
    private $daysFromInsuranceStartToSMS;
    private $from;
    private $recipientArray;
    private $messageCount;
    private $skipCount;
// SMS authentication details
    /*
     * TODO - MOVE TO inc 
     */
    private $uname = "lee@seopa.com";
    private $pword = "quotezone1";
// SMS configuration variables
    private $returnInfo = "1";
    private $testMode = "1";

    public function __construct($system, $daysFromInsuranceStartToSMS="335", $from="xreplyx", $messageKey = 'default') {
        try {
            $this->setLogFileName("sms_out_script.log");
            $this->logToFile("===============================");
            $this->logToFile("Script started...");
            $this->setSystem($system);
            $this->setDaysFromInsuranceStartToSMS($daysFromInsuranceStartToSMS);
            $this->setFrom($from);
            $this->setMessageKey($messageKey);
// Initialise the message and skip counts as 0
            $this->setMessageCount(0);
            $this->setSkipCount(0);
        } catch (Exception $e) {
            $msg = "ERROR - Problem constructing: " . $e->getMessage() . ".\n";
            $this->logToFile($msg);
            die($msg);
        }
    }

    public function getSystemName() {
        return $this->systemName;
    }

    public function getSystemId() {
        return $this->systemId;
    }

    public function getDaysFromInsuranceStartToSMS() {
        return $this->daysFromInsuranceStartToSMS;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getMessageID() {
        return $this->messageID;
    }

    public function getFrom() {
        return $this->from;
    }

    public function getRecipientArray() {
        return $this->recipientArray;
    }

    public function getTestMode() {
        return $this->testMode;
    }

    public function getReturnInfo() {
        return $this->returnInfo;
    }

    public function getMessageCount() {
        return $this->messageCount;
    }

    public function getSkipCount() {
        return $this->skipCount;
    }

    public function getDaysToCheckForPrevSMS() {
        return $this->daysToCheckForPrevSMS;
    }

    public function getMaxMessageLength() {
        return $this->maxMessageLength;
    }

    public function getDefaultMessageKey() {
        return $this->defaultMessageKey;
    }

    public function getMessageKey() {
        return $this->messageKey;
    }

    public function setMessageKey($messageKey) {
        try {
            $messageKey = strtoupper($messageKey);
            $messageKey = trim($messageKey);
            if ($messageKey = 'DEFAULT') {
                $messageKey = $this->getDefaultMessageKey();
            }
            if (in_array($messageKey, sms_out::$validMessageKeys)) {
                $this->messageKey = $messageKey;
                $message_id = array_search($messageKey, sms_out::$validMessageKeys);
                $this->setMessageID($message_id);
                $query = "SELECT sm.message FROM sms_messages sm WHERE sm.id = '" . $message_id . "'";
                $result = $this->mysqlQuery($query);
                while ($db_field = mysql_fetch_assoc($result)) {
                    $this->setMessage($db_field['message']);
                }
            } else {
                $msg = "ERROR - Message Key not recognised - is it in sms_messages DB table?\n";
                $this->logToFile($msg);
                die($msg);
            }
        } catch (Exception $e) {
            $msg = "WARNING - There was a problem setting the message key. Details: " . $e->getMessage() . ".\n";
            $this->logToFile($msg);
            echo $msg;
        }
    }

    private function setSystem($system) {
// Convert system name to caps and trim (for consistency)
        $system = strtoupper($system);
        $system = trim($system);
// Try to set the system and return an error if not found/valid
        if (in_array($system, sms_out::$validSystems)) {
            $this->systemName = $system;
            $systemId = array_search($system, sms_out::$validSystems);
            $this->systemId = $systemId;
//echo "System: " . $system . " SystemID: " . $systemId . "<br>\n"; // for debugging
        } else {
            $msg = "ERROR: There was a problem setting the system - cannot find '" . $system . "' in the validSystems array. If this is a new system then add it to the validSystems array specified at top of class.\n";
            $this->logToFile($msg);
            die($msg);
        }
    }

    public function setDaysFromInsuranceStartToSMS($daysFromInsuranceStartToSMS) {
// Make sure insurance start date is a number before setting
        if (is_numeric($daysFromInsuranceStartToSMS)) {
            $this->daysFromInsuranceStartToSMS = $daysFromInsuranceStartToSMS;
        } else {
            $msg = "ERROR - daysFromInsuranceStartToSMS must be numeric. Received: " . $daysFromInsuranceStartToSMS;
            $this->logToFile($msg);
            die($msg);
        }
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    private function setMessageID($id) {
        $this->messageID = $id;
    }

// validate remaining setters
    public function setFrom($from) {
        $this->from = $from;
    }

    private function setRecipientArray($recipientArray) {
        $this->recipientArray = $recipientArray;
    }

    public function setReturnInfo($returnInfo) {
        $this->info = $returnInfo;
    }

    public function setTestMode($testMode) {
        $this->testMode = $testMode;
    }

    private function setMessageCount($count) {
        $this->messageCount = $count;
    }

    private function setSkipCount($skip) {
        $this->skipCount = $skip;
    }

    public function setdaysToCheckForPrevSMS($daysToCheckForPrevSMS) {
        $this->daysToCheckForPrevSMS = $daysToCheckForPrevSMS;
    }

    public function setMaxMessageLength($maxMessageLength) {
        $this->maxMessageLength = $maxMessageLength;
    }

    public function setDefaultMessageKey($defaultMessageKey) {
        $this->defaultMessageKey = $defaultMessageKey;
    }

// Create the personalised message with the user's first name at start
    private function createFinalMessage($fname) {
        try {
            $message = $this->getMessage();
            $message = $this->replaceFirstNameHolder($message, $this->sentenceCase($fname));
            $message = $this->replaceSystemNameHolder($message, strtolower($this->getSystemName()));
// if the length is ok return the message, else we'll have to use the default message
            if (strlen($message) <= $this->getMaxMessageLength()) {
                return $message;
            } else {
                $msg = "WARNING - Message for " . $fname . " too long - trying default instead. Length: " . strlen($message) . ", Allowed: " . $this->getMaxMessageLength();
                $this->logToFile($msg);
                $this->setMessageKey('default');
                $message = $this->getMessage();
                $message = $this->replaceFirstNameHolder($message, $this->sentenceCase($fname)); // default shouldnt include a fname, as its intended to be used when the fname makes the message too long. Including the check anyway just in case the default is ever changed
                $message = $this->replaceSystemNameHolder($message, strtolower($this->getSystemName()));
// if still too long then die
                if (strlen($message) > $this->getMaxMessageLength()) {
                    $msg = "ERROR - Ending as default message still too long. Length: " . strlen($message) . ", Allowed: " . $this->getMaxMessageLength();
                    $this->logToFile($msg);
                    die($msg);
                }
                else
                    return $message;
            }
        } catch (Exception $e) {
            $msg = "WARNING - There was a problem creating the final message. Details: " . $e->getMessage() . ".\n";
            $this->logToFile($msg);
            echo $msg;
        }
    }

// Create an array of eligible recipients (id, fname, lname, mobile number, message)
    private function createRecipientArray() {
        /*
         * Select all users who have had a quote on this system x days ago (335), and who are opted in to receive SMS,
         * and who have entered a mobile number, and who havent already received a text message within the last x days (30)
         * If the user had multiple quotes, we are currently selecting the first quote
         * NB: This result can contain duplicate mobile numbers, as 2 different users can enter the same number. To fix this, we dedupe the result after our SQL select.
         *
         * Store all users as user objects in the recipientArray.
         */
        try {
            $this->logToFile("Creating recipient array...");
            $query = "
                    SELECT
                      l.filename,
                      l.id,
                      qzq.quote_type_id AS qzquote_type_id,
                      sstat.id AS sms_status_id,
                      qu.first_name,
                      qu.last_name,
                      qu.birth_date,
                      qu.password,
                      squd.mobile_phone_number,
                      squd.date_of_insurance_start,
                      sstat.sms_opt_in
                    FROM
                      logs l
                      LEFT JOIN qzquotes qzq ON (qzq.log_id = l.id)
                      LEFT JOIN qzquote_types qzqt ON (qzqt.id = qzq.quote_type_id)
                      LEFT JOIN " . strtolower($this->getSystemName()) . "_quote_user_details squd ON (squd.id = qzq.quote_user_details_id)
                      LEFT JOIN quote_users qu ON (qu.id = squd.quote_user_id)
                      LEFT JOIN sms_status sstat ON (sstat.quote_user_id = qu.id)
                    WHERE
                      qzq.quote_type_id = " . $this->getSystemId() . "
                      AND
                       TIMESTAMPDIFF(DAY,squd.date_of_insurance_start," . date("Y-m-d", mktime()) . ") = " . $this->getDaysFromInsuranceStartToSMS() . "
                      AND
                      l.id = (SELECT MIN(id) FROM logs WHERE quote_user_id = squd.quote_user_id)
                      AND
                      squd.mobile_phone_number != ''
                      AND
                      squd.mobile_phone_number != '-'
                      AND
                      squd.mobile_phone_number IS NOT NULL
                      AND
                      sstat.sms_opt_in = '1'
                    GROUP BY l.quote_user_id
                    ORDER BY l.id ASC
                ";
            $result = $this->mysqlQuery($query);
// TODO: ADD CHECKS TO ENSURE WE GET FULL RESULT?

            $i = 0;
//probably shouldn't use recipientArray[$i] like this?
            while ($db_field = mysql_fetch_assoc($result)) {
                $this->recipientArray[$i] = new user();
                $this->recipientArray[$i]->setLogId($db_field['id']);
                $this->recipientArray[$i]->setSmsStatusId($db_field['sms_status_id']);
                $this->recipientArray[$i]->setFname($db_field['first_name']);
                $this->recipientArray[$i]->setlname($db_field['last_name']);
                $this->recipientArray[$i]->setMobile($this->formatMobileNumber($db_field['mobile_phone_number']));
                $this->recipientArray[$i]->setDateOfInsuranceStart($db_field['date_of_insurance_start']);
                $this->recipientArray[$i]->setDob($db_field['birth_date']);
                $this->recipientArray[$i]->setPassword($db_field['password']);
                $this->recipientArray[$i]->setQzquote_type_id($db_field['qzquote_type_id']);
                $this->recipientArray[$i]->setFileName($db_field['filename']);

                $i++;
            }
// End now if the list is empty
            $this->logToFile("Found " . count($this->getRecipientArray()) . " total users matching criteria");
            if (count($this->getRecipientArray()) == 0) {
                $msg = "Ending as there is nothing to send - no eligeable users have been found.\n";
                $this->logToFile($msg);
                die($msg);
            }

// Takes each user in turn and checks if their mobile number is present anywhere else in the array. If so, remove the other record. Since the SQL is ordered by log id, the users in the array are in the order in which they got a quote, so the first/earliest user will be the one we keep when duplicates are found. NB: Only need to do this check if we have more than 1 user in our array
            $this->logToFile("Checking for duplicates...");
            $removedCount = 0;
            if (count($this->getRecipientArray()) > 1) {
                for ($i = 0; $i < count($this->getRecipientArray()); $i++) {
                    for ($j = $i + 1; $j < count($this->getRecipientArray()); $j++) {
                        if ($this->recipientArray[$i]->getMobile() == $this->recipientArray[$j]->getMobile()) {
//unset the duplicate we found (array will be reindexed after all are unset)
                            unset($this->recipientArray[$j]);
                            $removedCount++;
                        }
                    }
                }
            }
            if ($removedCount > 0) {
// Reindex the array since we have removed some users, log the removedCount
                $this->setRecipientArray(array_values($this->getRecipientArray()));
                $this->logToFile("Removed " . $removedCount . " duplicates.");
            }
// Now check if any of the mobile numbers for our users have already received a message in the last x days, regardless of system/user/message. If so, remove them from our array, as we never want to send 2 messages to the same number within $daysToCheckForPrevSMS days
            $this->logToFile("Checking if any have already received a message in the last " . $this->getDaysToCheckForPrevSMS() . " days...");
            $invalidCount = 0;
// kept separate since the count may change during the loop
            $startingCount = count($this->getRecipientArray());
            for ($i = 0; $i < $startingCount; $i++) {
                $query2 = "SELECT count(*) AS count FROM sms_sent ssnt 
                        WHERE
                            mobile_phone = '" . $this->recipientArray[$i]->getMobile() . "' 
                        AND ( 
                                (TIMESTAMPDIFF ( DAY , ssnt.date , " . date("Y-m-d H:i:s", mktime()) . " ) <= " . $this->getDaysToCheckForPrevSMS() . " )
                                OR
                                (ssnt.date IS NULL )
                            )";
                $result2 = $this->mysqlQuery($query2);
                while ($db_field = mysql_fetch_assoc($result2)) {
                    if ($db_field['count'] != 0) {
                        unset($this->recipientArray[$i]);
                        $invalidCount++;
                    }
                }
            }
            if ($invalidCount > 0) {
// Reindex the array since we have removed some invalid users we cant send to, log the invalidCount
                $this->setRecipientArray(array_values($this->getRecipientArray()));
                $this->logToFile("Removed " . $invalidCount . " invalid users (already received a text in last " . $this->getDaysToCheckForPrevSMS() . " days).");
            }
// potentially could have an empty array now, so double check if we can end early
            if (count($this->getRecipientArray()) == 0) {
                $msg = "Ending as there is nothing to send - no eligeable users have been left after validity checks.\n";
                $this->logToFile($msg);
                die($msg);
            }
// set the personal message for each remaining recipient. If their fname length causes message to go over max length it will be replaced with default message
            for ($i = 0; $i < count($this->getRecipientArray()); $i++) {
                $this->recipientArray[$i]->setMessage($this->createFinalMessage($this->recipientArray[$i]->getFname()));
            }
// combine the counts for reporting/results purposes. There is also one final check later for each user (validForSending) - any skipped there will also increment this count.
            $this->setSkipCount($this->getSkipCount() + $invalidCount + $removedCount);
        } catch (Exception $e) {
            $msg = "WARNING - There was a problem creating the recipient array. Details: " . $e->getMessage() . ".\n";
            $this->logToFile($msg);
            echo $msg;
        }
    }

    private function prepareData() {
// Prepare data for POST request
        $this->logToFile("Preparing the data...");
        try {
            for ($i = 0; $i < count($this->getRecipientArray()); $i++) {
// only add valid users - throw an exception if any are not valid
                if ($this->recipientArray[$i]->validForSending()) {
                    $this->recipientArray[$i]->setMessage(urlencode($this->recipientArray[$i]->getMessage()));
//prepare each SMS data string to be sent
                    $this->recipientArray[$i]->setDataString("uname=" . $this->uname . "&pword=" . $this->pword . "&message=" . $this->recipientArray[$i]->getMessage() . "&from=" . $this->from . "&selectednums=" . $this->recipientArray[$i]->getMobile() . "&info=" . $this->returnInfo . "&test=" . $this->testMode);
                }
            }
        } catch (Exception $e) {
            $msg = "WARNING - There was a problem preparing the SMS data to send. Details: " . $e->getMessage() . ".\n";
            $this->logToFile($msg);
            echo $msg;
        }
    }

    private function sendAndLogData() {
// Send the POST request with cURL
        $this->logToFile("Sending the data...");
        try {
            $ch = curl_init('http://www.txtlocal.com/sendsmspost.php');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Send all the valid ones and skip any remaining invalids
            for ($i = 0; $i < count($this->getRecipientArray()); $i++) {
                if ($this->recipientArray[$i]->validForSending()) {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $this->recipientArray[$i]->getDataString());
                    $result = curl_exec($ch); //This is the result from Textlocal
//Link result to user
                    $this->recipientArray[$i]->setResult($result);
                    $this->recipientArray[$i]->setSentStatus(true);
                    $this->logMessageSent($i);
// increment the messageCount
                    $this->setMessageCount($this->getMessageCount() + 1);
                } else {
// increment the skip count (skip count will include any that got removed earlier as well as skipped now                    
                    $this->setSkipCount($this->getSkipCount() + 1);
                }
            }

            curl_close($ch);
        } catch (Exception $e) {
            $msg = "ERROR - There was a problem sending the SMS message/s. Details: " . $e->getMessage() . ".\n";
            $this->logToFile($msg);
            echo $msg;
        }
    }

    private function logMessageSent($i) {
        try {
            $query = "INSERT INTO sms_sent (log_id,qzquote_type_id,sms_status_id,sms_message_id,mobile_phone,date,time) VALUES ("
                    . $this->recipientArray[$i]->getLogId() . "," . $this->recipientArray[$i]->getQzquote_type_id() . "," . $this->recipientArray[$i]->getSmsStatusId() . "," . $this->getMessageID() . ",'" . $this->recipientArray[$i]->getMobile() . "','" . date("Y-m-d", mktime()) . "','" . date("H:i:s", mktime()) . "')";
            $result = $this->mysqlQuery($query, 'Could not log history in DB: ');
        } catch (Exception $e) {
            $msg = "Error - There was a problem logging message history in DB. Details: " . $e->getMessage() . ".\n";
            $this->logToFile($msg);
            echo $msg;
        }
    }

    public function checkRecipients() {
        $this->mysqlConnect();
        $this->createRecipientArray();
        for ($i = 0; $i < count($this->getRecipientArray()); $i++) {
            $msg = $this->recipientArray[$i]->detailsForLog();
            echo $msg;
            $this->logToFile($msg);
        }
        echo "Count: " . count($this->getRecipientArray()) . "\n";
    }

    public function sendSMS() {
        try {

            $this->mysqlConnect();
            $this->createRecipientArray();
            $this->prepareData();
            $this->sendAndLogData();
            $this->mysqlDisconnect();
//Output the SMS count and array

            $msg = "Script complete. Sent: " . $this->getMessageCount() . ". Skipped: " . $this->getSkipCount() . ".";
            $this->logToFile($msg);
            echo $msg . " See sms_sent table for sent details and sms_out_script.log for skipped details.\n";
        } catch (Exception $e) {
            $msg = "Error - There was a problem sending the SMS. Details: " . $e->getMessage() . ".\n";
            $this->logToFile($msg);
            echo $msg;
        }
    }

}

// for receiving sms messages
class sms_in extends sms {

    private static $availableActions = array(sms_in_actions::opted_in, sms_in_actions::opted_out, sms_in_actions::unsure); // If adding new action, first add to sms_in_action enum class then add to this array
    private $fromNumber;
    private $content;
    private $toNumber;
    private $email;
    private $credits;
    private $sms_sent_id;
    private $rerun_log_id; // the new log id for the quote we reran
    private $quote_user_id;
    private $action; // enum: opted in, opted out, unsure.
    private $sms_opt_in;
    private $newInsuranceStartDate;
    private $daysFromOldInsuranceStartToNew = 365;
    private $systemName;

    /*
     * OutURL can also be set statically if required. The params are added automatically during the getUserData method. 
     * Once we know the system name we attempt to dynamically set the outURL - if it has been set statically instead, then it wont find [SYSTEM_NAME] holder so static version set will be unaffected
     */
    private $outUrl = "https://[SYSTEM_NAME]-insurance.quotezone.co.uk/[SYSTEM_NAME]/steps/RerunSms.php";
// we then try to dynamically set the following 2 URLs as well, but again these can be manually set if required
    private $systemURL = "https://[SYSTEM_NAME]-insurance.quotezone.co.uk";
    private $sessionURL = "https://[SYSTEM_NAME]-insurance.quotezone.co.uk/[SYSTEM_NAME]/ses.php";
// Tracking params
    private $tracker_url = "http://www.quotezone.co.uk/emailTracker_sms.php";
    private $linkID = "menu_home";
    private $recipient; // mobile number, set in getUserData
    private $utm_medium = "SMS";
    private $utm_source = "SMS+Renewals";
    private $utm_campaign = "SMS+[SYSTEM_NAME]+Renewals+1year"; // This is the starting campaign message. When setting this we pass the system name as a parameter, convert it to sentence case (e.g. Taxi), then replace "[SYSTEM_NAME]" in this string with the system name. If changing the message, double check the setUtm_campaign($system_name) method
    private $utm_content = "1";
    private $utm_date; // set in getUserData

    /*
     * Example complete out URL: "https://taxi-insurance.quotezone.co.uk/taxi/steps/RerunSms.php?sid=1117801041000001&start_date=2012-02-01"
     * Example email tracked URL: "http://www.quotezone.co.uk/emailTracker.php?linkID=menu_home&recipient=07411078930&utm_medium=SMS&utm_source=SMS+Renewals&utm_campaign=SMS+Taxi+Renewals+1year&utm_content=1&utm_date=2012-02-24&outUrl=https://taxi-insurance.quotezone.co.uk/taxi/steps/RerunSms.php?sid=1117801041000001&start_date=2012-02-01"
     */

    public function __construct($sender, $content, $inNumber, $email, $credits) {
        try {
            $this->setLogFileName("sms_in_script.log");
            $this->logToFile("===============================");
            $this->logToFile("Script started...");
            $this->setFromNumber($sender);
            $this->setContent($content);
            $this->setToNumber($inNumber);
            $this->setEmail($email);
            $this->setCredits($credits);
        } catch (Exception $e) {
            $msg = "Unable to construct: " . $e->getMessage() . ".\n";
            $this->logToFile($msg);
            echo $msg;
        }
    }

    public function getFromNumber() {
        return $this->fromNumber;
    }

    public function getContent() {
        return $this->content;
    }

    public function getToNumber() {
        return $this->toNumber;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getCredits() {
        return $this->credits;
    }

    public function getSms_sent_id() {
        return $this->sms_sent_id;
    }

    public function getRerun_log_id() {
        return $this->rerun_log_id;
    }

    public function getAction() {
        return $this->action;
    }

    public function getTracker_url() {
        return $this->tracker_url;
    }

    public function getLinkID() {
        return $this->linkID;
    }

    public function getRecipient() {
        return $this->recipient;
    }

    public function getUtm_medium() {
        return $this->utm_medium;
    }

    public function getUtm_source() {
        return $this->utm_source;
    }

    public function getUtm_campaign() {
        return $this->utm_campaign;
    }

    public function getUtm_content() {
        return $this->utm_content;
    }

    public function getUtm_date() {
        return $this->utm_date;
    }

    public function getOutUrl() {
        return $this->outUrl;
    }

    public function getSms_opt_in() {
        return $this->sms_opt_in;
    }

    public function getNewInsuranceStartDate() {
        return $this->newInsuranceStartDate;
    }

    public function getDaysFromOldInsuranceStartToNew() {
        return $this->daysFromOldInsuranceStartToNew;
    }

    public function getSystemName() {
        return $this->systemName;
    }

    public function getSystemURL() {
        return $this->systemURL;
    }

    public function getSessionURL() {
        return $this->sessionURL;
    }

    public function getQuote_user_id() {
        return $this->quote_user_id;
    }

    public function setQuote_user_id($quote_user_id) {
        $this->quote_user_id = $quote_user_id;
    }

    public function setSessionURL($sessionURL) {
        $this->sessionURL = $sessionURL;
    }

    public function setSystemURL($systemURL) {
        $this->systemURL = $systemURL;
    }

    public function setSystemName($systemName) {
        if (empty($systemName)) {
            $msg = "ERROR - Received an empty system name\n";
            $this->logToFile($msg);
            throw new Exception($msg);
        } else {
            $systemName = trim($systemName);
            $systemName = strtolower($systemName);
            $this->systemName = $systemName;
        }
    }

    public function setDaysFromOldInsuranceStartToNew($daysFromOldInsuranceStartToNew) {
        $this->daysFromOldInsuranceStartToNew = $daysFromOldInsuranceStartToNew;
    }

    public function setNewInsuranceStartDate($oldInsuranceStartDate) {
        $newdate = strtotime('+' . $this->getDaysFromOldInsuranceStartToNew() . " day", strtotime($oldInsuranceStartDate)); // adds x days to old insurance start date to give us the new insurance start date
        $this->newInsuranceStartDate = date('Y-m-d', $newdate);
    }

    private function setFromNumber($fromNumber) {
        $number = $this->formatMobileNumber($fromNumber);
        if (empty($number)) {
            $msg = "ERROR - Ending as from number empty";
            $this->logToFile($msg);
            die($msg);
        }
        else
            $this->fromNumber = $number;
    }

    private function setToNumber($toNumber) {
        $this->toNumber = $this->formatMobileNumber($toNumber); // will warn later on if $toNumber is null
    }

    public function setSms_opt_in($sms_opt_in) {
        if (is_numeric($sms_opt_in)) { // numeric in case we use additional settings in the future for opt in statuses
            $this->sms_opt_in = $sms_opt_in;
        } else {
            $msg = "ERROR - SMS Opt In must be numeric\n";
            $this->logToFile($msg);
            throw new Exception($msg);
        }
    }

    private function setContent($content) {
        $content = trim($content);
        if ($content == null) {
            $content = ''; // for consistency, blank messages should be treated as ''. DB also doesnt allow null for this column.
        }
        $this->content = $content;
    }

    private function setEmail($email) { //unimportant - no need to validate yet
        $this->email = $email;
    }

    private function setCredits($credits) { //unimportant - no need to validate yet
        $this->credits = $credits;
    }

    private function setSms_sent_id($sms_sent_id) {
        if (isset($sms_sent_id)) {
            $this->sms_sent_id = $sms_sent_id;
        } else {
            $msg = "WARNING - SMS sent ID is empty - we may not have found this user in our sent log. Continuing anyway.\n";
            $this->logToFile($msg);
            throw new Exception($msg);
        }
    }

    private function setRerun_log_id($rerun_log_id) {
        $this->rerun_log_id = $rerun_log_id; // allowed to be null
    }

    private function setAction($action) {
        if (in_array($action, sms_in::$availableActions))
            $this->action = $action;
        else {
            $msg = "ERROR - ending as action not in list of available actions\n";
            $this->logToFile($msg);
            die($msg);
        }
    }

    /*
     * TODO - Could do with better validation
     */

    public function setTracker_url($tracker_url) {
        $this->tracker_url = $tracker_url;
    }

    public function setLinkID($linkID) {
        $this->linkID = $linkID;
    }

    public function setRecipient($recipient) {
        $this->recipient = $recipient;
    }

    public function setUtm_medium($utm_medium) {
        $this->utm_medium = $utm_medium;
    }

    public function setUtm_source($utm_source) {
        $this->utm_source = $utm_source;
    }

    public function setUtm_campaign($utm_campaign) {
        $this->utm_campaign = $utm_campaign;
    }

    public function setUtm_content($utm_content) {
        $this->utm_content = $utm_content;
    }

    public function setUtm_date($utm_date) {
        $this->utm_date = $utm_date;
    }

    public function setOutUrl($outUrl) {
        $this->outUrl = $outUrl;
    }

    private function setDynamicSystemVariables() {

// Check for and replace the system name holders for each
        try {
            $this->logToFile("Setting system type: " . $this->getSystemName() . "...");
            $system_name = strtolower($this->getSystemName());
            $this->setOutUrl($this->replaceSystemNameHolder($this->getOutUrl(), $system_name));
            $this->setSystemURL($this->replaceSystemNameHolder($this->getSystemURL(), $system_name));
            $this->setSessionURL($this->replaceSystemNameHolder($this->getSessionURL(), $system_name));
            $system_name_sc = $this->sentenceCase($this->getSystemName()); // campaign param needs it in sentence case
            $this->setUtm_campaign($this->replaceSystemNameHolder($this->getUtm_campaign(), $system_name_sc));
        } catch (Exception $e) {
            $msg = "ERROR - There was a problem dynamically setting the system variables. Details: " . $e->getMessage() . ".\n";
            $this->logToFile($msg);
            echo $msg;
        }
    }

    private function addOutUrlParams($sid, $newInsuranceStartDate) {
        $finalOutURL = $this->getOutUrl() . "?sid=" . $sid . "&start_date=" . $newInsuranceStartDate;
        $this->setOutUrl($finalOutURL);
//$this->logToFile("Final out URL: " . $finalOutURL . "...");
    }

// Attemps to find the users details by looking up the last text we sent this number, then retrieving the system and quote we text them about. Done in 2 stages - first to find out the system, then once we know where to look we can get the other details
    private function getUserData() {
        try {
            $this->logToFile("Getting user data for " . $this->getFromNumber() . "...");
// find out the system first and the date of the most recent SMS we sent them
            $query1 = "SELECT l.quote_user_id,l.id AS log_id, qzqt.name AS system_name, MAX(CONCAT(ssnt.date,' ',ssnt.time)) AS datetime_sent, MAX(ssnt.date) AS date_sent
                            FROM logs l
                            LEFT JOIN sms_sent ssnt ON (ssnt.log_id = l.id)
                            LEFT JOIN qzquotes qzq ON (qzq.log_id = l.id)
                            LEFT JOIN qzquote_types qzqt ON (qzqt.id = qzq.quote_type_id)
                        WHERE ssnt.mobile_phone = '" . $this->getFromNumber() . "'\n";
            $result1 = $this->mysqlQuery($query1, 'Error getting the user details: ');

            if (mysql_num_rows($result1) == 0) {
                $msg = "ERROR - Can't find user.\n";
                $this->logToFile($msg);
                die($msg);
            } else {
                while ($db_field = mysql_fetch_assoc($result1)) {
                    $logid = $db_field['log_id'];
                    if (!is_numeric($logid)) {
                        $msg = "ERROR - Can't find user.\n";
                        $this->logToFile($msg);
                        die($msg);
                    }
                    $this->setSystemName($db_field['system_name']);
                    $this->setUtm_date($db_field['date_sent']);
                    $this->setDynamicSystemVariables(); // set the out URL,  system link, & utm campaign message dynamically using the system name
                }
            }
// Now that we know the system we can get the other details 
            $query2 = "SELECT l.quote_user_id,squd.date_of_insurance_start,sstat.sms_opt_in, qzqt.name AS system_name,ssnt.qzquote_type_id,ssnt.id AS sms_sent_id, ssnt.log_id, l.filename, ssnt.mobile_phone, MAX(CONCAT(ssnt.date,' ',ssnt.time)) AS date_sent, REPLACE(l.filename,'_','') AS sid
                        FROM 
                          logs l
                          LEFT JOIN qzquotes qzq ON (qzq.log_id = l.id)
                          LEFT JOIN qzquote_types qzqt ON (qzqt.id = qzq.quote_type_id)
                          LEFT JOIN " . $this->getSystemName() . "_quote_user_details squd ON (squd.id = qzq.quote_user_details_id)
                          LEFT JOIN quote_users qu ON (qu.id = squd.quote_user_id)
                          LEFT JOIN sms_status sstat ON (sstat.quote_user_id = qu.id)
                          LEFT JOIN sms_sent ssnt ON (ssnt.log_id = l.id)
                            WHERE ssnt.mobile_phone = '" . $this->getFromNumber() . "'";
            $result = $this->mysqlQuery($query2, 'Error getting the user details: ');

            if (mysql_num_rows($result) == 0) {
                $msg = "ERROR - Can't find user details.\n";
                $this->logToFile($msg);
                die($msg);
            } else {
                while ($db_field = mysql_fetch_assoc($result)) {
                    $this->setNewInsuranceStartDate($db_field['date_of_insurance_start']);
                    $this->addOutUrlParams($db_field['sid'], $this->getNewInsuranceStartDate()); // add the params to our out URL for rerunning the quote
                    $this->setSms_sent_id($db_field['sms_sent_id']);
                    $this->setRecipient($db_field['mobile_phone']); // set the number we sent the text too
                    $this->SetSms_opt_in($db_field['sms_opt_in']);
                    $this->setQuote_user_id($db_field['quote_user_id']);
                }
            }
        } catch (Exception $e) {
            $msg = "There was a problem getting/setting the user's data. Details: " . $e->getMessage() . ".\n";
            $this->logToFile($msg);
            echo $msg;
        }
    }

// identify opt-in status; if yes { update rerun flag, rerun the quote,confirm? } else if no {update db to opt that user out of everything, confirm?}
    public function processReply() {
        try {

// Open DB Connection for this object
            $this->mysqlConnect();
// Get the user data and store against this object
            $this->getUserData();

            $contentInUpperCase = strtoupper($this->getContent());

// If they've opted in do this
            if ($contentInUpperCase == "YES") {
                $this->processOptedIn();
            }
// else if they've opted out do this
            else if ($contentInUpperCase == "STOP") { // consider adding == "NO" as well?
                $this->processOptedOut();
            }
// if they've replied with something different do this
            else {
                $this->processUnsure();
            }
            $this->mysqlDisconnect();
            $msg = "Action: " . $this->getAction() . ".";
            $this->logToFile($msg);
            echo $msg;
            $msg = "Script finished.\n";
            $this->logToFile($msg);
            echo $msg;
        } catch (Exception $e) {
            $msg = "ERROR - There was a problem processing the reply. Details: " . $e->getMessage() . ".\n";
            $this->logToFile($msg);
            echo $msg;
        }
    }

    private function logReply() {
        try {
            $this->logToFile("Logging reply in DB...");
// if we reran the quote and have a rerun id, insert it as well. Otherwise insert without the rerun id.
// insert to use if we couldnt find an SMS sent to this number...
            if (!$this->getSms_sent_id() || !$this->getToNumber()) {
                $query = "INSERT INTO sms_received (from_number, content, action, date, time) VALUES ('"
                        . $this->getFromNumber() . "','" . $this->getContent() . "','" . $this->getAction() . "','" . date("Y-m-d", mktime()) . "','" . date("H:i:s", mktime()) . "')";
                $result = $this->mysqlQuery($query, 'ERROR - Could not log reply in DB: ');
            }
// insert to use if we didnt rerun a quote for this user
            else if (!$this->getRerun_log_id()) {
// insert a record in the history table to log that we received the message
                $query = "INSERT INTO sms_received (sms_sent_id, from_number, to_number, content, action, date, time) VALUES ("
                        . $this->getSms_sent_id() . ",'" . $this->getFromNumber() . "','" . $this->getToNumber() . "','" . $this->getContent() . "','" . $this->getAction() . "','" . date("Y-m-d", mktime()) . "','" . date("H:i:s", mktime()) . "')";
                $result = $this->mysqlQuery($query, 'ERROR - Could not log reply in DB: ');
            }
// insert to use if we have all info including a new quote rerun id
            else {
                $query = "INSERT INTO sms_received (sms_sent_id, rerun_log_id, from_number, to_number, content, action, date, time) VALUES ("
                        . $this->getSms_sent_id() . "," . $this->getRerun_log_id() . ",'" . $this->getFromNumber() . "','" . $this->getToNumber() . "','" . $this->getContent() . "','" . $this->getAction() . "','" . date("Y-m-d", mktime()) . "','" . date("H:i:s", mktime()) . "')";
                $result = $this->mysqlQuery($query, 'ERROR - Could not log reply in DB: ');
            }
        } catch (Exception $e) {
            $msg = "ERROR - Could not log reply in DB: " . $e->getMessage() . ".\n";
            $this->logToFile($msg);
            echo $msg;
        }
    }

    /*
     * TODO: Update to set actions from an enumerated array
     */

    private function processOptedIn() {
        $this->logToFile("Processing opted in...");
        $this->setAction(sms_in_actions::opted_in);
// Call ryan's tracking URL and rerun the quote
        $this->runQuote();
// log the reply
        $this->logReply();
// Consider replying to confirm
    }

    private function processOptedOut() {
        $this->logToFile("Processing opted out...");
        $this->setAction(sms_in_actions::opted_out);
// Update DB to opt this mobile number out of everything (i.e. they should never receive another SMS from us on this number)
        $this->unsubscribeUser();
// log the reply in DB
        $this->logReply();
// consider replying to confirm
    }

    private function processUnsure() {
        $this->logToFile("Processing unsure...");
        $this->setAction(sms_in_actions::unsure);
// we can't confirm they've opted in or out, so simply log the reply for now and take no further action
// consider replying to confirm what they want to do
        $this->logReply();
    }

    private function formTrackedUrl() {
        $finalTrackedURL = $this->getTracker_url() . "?linkID=" . $this->getLinkID() . "&recipient=" . $this->getRecipient() . "&utm_medium=" . $this->getUtm_medium() . "&utm_source=" . $this->getUtm_source() . "&utm_campaign=" . $this->getUtm_campaign() . "&utm_content=" . $this->getUtm_content() . "&utm_date=" . $this->getUtm_date() . "&outUrl=" . $this->getOutUrl();
// double check no spaces in URL by replacing any with + before returning the final URL string
        return str_replace(' ', '+', $finalTrackedURL);
    }

    private function runQuote() {
// Send the POST request with cURL
        try {
            $this->logToFile("Starting quote rerun...");
// This user has now opted in again by requesting a rerun - in the off chance that they are opted out in the db, update all matches now to indicate this mobile number is opted in again
            $query = "SELECT id,sms_opt_in FROM sms_status WHERE mobile_phone = '" . $this->getFromNumber() . "'";
            $result = $this->mysqlQuery($query);
            while ($db_field = mysql_fetch_assoc($result)) {
                if ($db_field['sms_opt_in'] == 0) {
                    $query = "UPDATE sms_status SET sms_opt_in = 1 WHERE id = '" . $db_field['id'] . "'";
                    $result = $this->mysqlQuery($query);
                    $msg = "Opted " . $this->getFromNumber() . " back in.";
                    $this->logToFile($msg);
                }
            }
// Form and call our final URL that will send the params to the email tracker script. This will then log the sms in the email_tracker table and redirect to our RerunSms.php script to trigger the quote rerun
            $trackedUrl = $this->formTrackedUrl();
            $this->logToFile("Full quote rerun URL: " . $trackedUrl);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $trackedUrl);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_NOBODY, 1);
            $result = curl_exec($ch); //This is the result
            curl_close($ch);

// Lookup the log id of the quote we just ran for this user (should be ok to use the latest log id for this user since we have only just ran the quote)
            $query = "SELECT MAX(l.id) AS rerun_id FROM logs l WHERE l.quote_user_id = '" . $this->getQuote_user_id() . "'";

            $result = $this->mysqlQuery($query, "Problem selecting most recent log id for user");
            while ($db_field = mysql_fetch_assoc($result)) {
                $this->setRerun_log_id($db_field['rerun_id']);
            }
            /* Is the above reliable enough? If the user gets a different quote at same time we rerun, we may not select the correct log id. Consider either improving this method or using a different method (such as getting from session instead)
              sleep(5); //temp
              $data = file_get_contents($this->getSessionURL());

              eval("\$Session = $data;");
              $filename = $Session["TEMPORARY FILE NAME"]; //add check to make sure user matches before setting? Just in case we receive 2 replies at same time
             * 

              sleep(5); //temp
              $query = "SELECT max(id) AS id FROM logs WHERE quote_user_id = (select quote_user_id from logs where id in (select log_id from sms_sent where id = '" . $this->getSms_sent_id() . "'))";
              $result = $this->mysqlQuery($query, "Could not find the log ID of the new quote");
              while ($db_field = mysql_fetch_assoc($result)) {
              $this->setRerun_log_id($db_field['id']);
              }
             */
        } catch (Exception $e) {
            $msg = "ERROR - There was a problem rerunning the quote. Details: " . $e->getMessage() . ". Data string: " . $trackedUrl . ".\n";
            $this->logToFile($msg);
            echo $msg;
        }
    }

    private function unsubscribeUser() {
//Update the opt-in status to false for all sms_status records with this mobile number. No need to check user etc as in this situation we want to ensure we never send this mobile number another SMS.
        try {

// update the sms_opt_in to false for ALL matches of this phone number, regardless of system etc (i.e. if they opt out, we should never send them an sms again for any system)
            $query = "UPDATE sms_status SET sms_opt_in = 0 WHERE mobile_phone = '" . $this->getFromNumber() . "'";
            $result = $this->mysqlQuery($query, "Could not unsubscribe user in DB: ");
            $this->logToFile("Opted " . $this->getFromNumber() . " out.");
        } catch (Exception $e) {
            $msg = "ERROR - Problem unsubscribing user: " . $e->getMessage() . ".\n";
            $this->logToFile($msg);
            echo $msg;
        }
    }

    public function deleteUser() {
// Add a way to fully remove a user? And/or integrate with existing customer delete scripts/procedures (new admin)
    }

}

// if adding an action, also add it to the availableActions array at the start of sms_in
abstract class sms_in_actions {
    const opted_in = "opted_in";
    const opted_out = "opted_out";
    const unsure = "unsure";
}

// Used to store neccesary user details/result in the same object. We then create arrays of users
class user {

    private $logId;
    private $smsStatusId;
    private $fname;
    private $lname;
    private $mobile;
    private $message;
    private $messageID;
    private $result;
    private $sentStatus = false;
    private $dataString;
    private $dob;
    private $password;
    private $dateOfInsuranceStart;
    private $qzquote_type_id;
    private $duplicateFlag;
    private $fileName;

    public function getDuplicateFlag() {
        return $this->duplicateFlag;
    }

    public function setDuplicateFlag($duplicateFlag) {
        $this->duplicateFlag = $duplicateFlag;
    }

    public function getLogId() {
        return $this->logId;
    }

    public function getSmsStatusId() {
        return $this->smsStatusId;
    }

    public function getFname() {
        return $this->fname;
    }

    public function getLname() {
        return $this->lname;
    }

    public function getMobile() {
        return $this->mobile;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getResult() {
        return $this->result;
    }

    public function getSentStatus() {
        return $this->sentStatus;
    }

    public function getDob() {
        return $this->dob;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getDateOfInsuranceStart() {
        return $this->dateOfInsuranceStart;
    }

    public function getDataString() {
        return $this->dataString;
    }

    public function getQzquote_type_id() {
        return $this->qzquote_type_id;
    }

    public function getMessageID() {
        return $this->messageID;
    }

    public function getFileName() {
        return $this->fileName;
    }

    public function setFileName($fileName) {
        $this->fileName = $fileName;
    }

    public function setMessageID($messageID) {
        $this->messageID = $messageID;
    }

    public function setQzquote_type_id($qzquote_type_id) {
        $this->qzquote_type_id = $qzquote_type_id;
    }

// Add validation to setters
    public function setLogID($logid) {
        $this->logId = $logid;
    }

    public function setSmsStatusId($smsStatusId) {
        $this->smsStatusId = $smsStatusId;
    }

    public function setFname($fname) {
        $this->fname = $fname;
    }

    public function setLname($lname) {
        $this->lname = $lname;
    }

    public function setMobile($mobile) {
        $this->mobile = $mobile;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function setResult($res) {
        $this->result = $res;
    }

    public function setSentStatus($status) {
        $this->sentStatus = $status;
    }

    public function setDob($dob) {
        $this->dob = $dob;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function setDateOfInsuranceStart($dateOfInsuranceStart) {
        $this->dateOfInsuranceStart = $dateOfInsuranceStart;
    }

    public function setDataString($dataString) {
        $this->dataString = $dataString;
    }

    public function printUser() {
        print "----START USER PRINT----\n";
        print "LogID: " . $this->getLogId() . ", \n";
        print "First Name: " . $this->getFname() . ", \n";
        print "Surname: " . $this->getLname() . ", \n";
        print "Mobile: " . $this->getMobile() . ", \n";
        print "Message: '" . $this->getMessage() . "'\n";
        print "Message Decoded: '" . urldecode($this->getMessage()) . "'\n";
        print "Sent status: " . $this->getSentStatus() . "\n";
        print "Valid for sending?: " . $this->validForSending() . "\n";
        print "Result: " . $this->getResult() . "\n";
        print "----END USER PRINT----\n";
    }

    public function detailsForLog() {
        return $this->getMobile() . " (LogID: " . $this->getLogId() . ", " . $this->getFname() . " " . $this->getLname() . ")\n\n";
    }

    public function debugPrint() {
        print "----START DEBUG PRINT----<BR>\n";
        print "LogID: " . $this->getLogId() . ", \n";
        print "Name: " . $this->getFname() . " \n";
        print $this->getLname() . ", \n";
        print "Mobile: " . $this->getMobile() . ", <br>\n";
        print "Message: '" . $this->getMessage() . "'<br>\n";
        print "Sent status: " . $this->getSentStatus() . "<br>\n";
        print "----END DEBUG PRINT----<BR>\n";
    }

// Check if this user is valid. If not they will be skipped.
    public function validForSending() {
        $msg = $this->getMessage();
        if ((!empty($msg)) && (strlen($this->getMobile()) > 10)) {
// make sure we have the file for this user - otherwise no point sending them an sms as we wont be able to rerun their quote
            /*
              $fileNameIn = FS_ROOT_PATH . "in/$this->getFileName())";
              $file = new CFile();
              $objFSClient = new CFileStoreClient(FS_SYSTEM_TYPE, 'in');
              $objFSClient->GetFile($this->getFileName());
              if (!$file->Open($fileNameIn, "r"))
              return false;
              else {
              return true;
              }
             */
            return true;
        } else {
            return false;
        }
    }

}

?>
<?php

/*****************************************************************************/
/*                                                                           */
/*  CRSEngine class interface                                                */
/*                                                                           */
/*  (C) 2008 ACRUX SOFTWARE (null@seopa.com)                                 */
/*                                                                           */
/*****************************************************************************/
define("RSENGINE_INCLUDED", "1");
include_once "globals.inc";
include_once "errors.inc";
include_once "Site.php";
include_once "Url.php";
include_once "Log.php";
include_once "QuoteStatus.php";
include_once "Quote.php";
include_once "QuoteDetails.php";
include_once "QuoteError.php";
include_once "Session.php";
include_once "QuoteScanning.php";
include_once "File.php";
include_once "MailInfo.php";
include_once "FileStoreClient.php";
include_once "Outbound.php";

//include_once "EmailOutbounds.php";
//include_once "EmailOutboundsEngine.php";
//include_once "EmailOutboundsEngineTaxi.php";

include_once "RSEngine.php";

define("OPERATOR_MODE", false);

// debug mode
if(DEBUG_MODE)
   error_reporting(1);
else
   error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CRSEngine
//
// [DESCRIPTION]:  CRSEngine class interface, remote site data exchange support,
//                 PHP version
//
// [FUNCTIONS]:    true | false    InitSite()
//                 true | false    FetchProcessInit()
//                 true | false    FetchProcessResponse()
//                 true | false    FetchProcessResult()
//                 integer | false SetQuoteUserID($quoteUserID='')
//                 integer | false SetQzQuoteID($qzQuoteID='')
//                 void            LogMsg($logMsg = "")
//                 void            WriteErrorFile($fileName="", $fileContent)
//                 void            WriteDataFile($fileName="", $step="", $params=array())
//                 void            WriteLogFile($fileName="", $fileContent)
//                 void            WriteErrorMsg($fileName="", $fileContent)
//                 true | false    CheckServerResponse()
//                 void            SetOfflineServerMessage($serverMessage="")
//                 void            SetUnableQuoteOnlineServerMessage($serverMessage="")
//                 void            SetSession($session=array())
//                 void            SetQuoteError($quoteError)
//                 integer | false AddLogToDB()
//                 integer | false AddQuoteStatusToDB()
//                 integer | false UpdateQuoteStatusToDB($status)
//                 integer | false AddQuoteErrorToDB($quoteError)
//                 integer | false AddQuotesToDB()
//                 integer | false AddSiteScanningToDB()
//                 void            WritePersonalizedSiteError()
//                 void            WriteAllFileLogs($category,$type='')
// 
//              bool Close();
//              bool GetError();
//
//              void SetSession($session=array());
//
// [CREATED BY]:   Eugen SAVIN (seugen@abc.ro) 2004-08-02
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CTAXIEngine extends CRSEngine
{

    var $offlineServerMessage;    // offline server quote message - eg. The online quotation system is currently offline
    var $unableQuoteOnlineMessage;// unable to quote online you
    var $fileName;                // filename in case of errors
    var $quoteUserID;             // quote user id allready saved into session
    var $qzQuoteID;               // qz quote id allready saved into session
    var $scanningFlag;            // scanning flag: true scanning : false not scanning (normal mode - web)
    var $testingMode;             // set site mode running for siteName and init steps

    var $closeDB;                 // close database flag
    var $dbh;                     // database handle

    var $objFSClient;              // file store server

    var $session;                 // keep the session data into this array

    //used not to send post to emailvision
    var $unsubscribedUser;          //users that are unsubscribed form our system

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CRSEngine
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    $siteName = "", $dbh = 0
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-02
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CTAXIEngine($siteName = "", $fileName = "", $scanningFlag = 0, $debugMode = 0, $testingMode = 0)
{
   CRSEngine::CRSEngine($siteName);

   $this->fileName             = $fileName;
   $this->scanningFlag         = $scanningFlag;
   $this->debugMode            = $debugMode;
   $this->testingMode          = $testingMode;
   $this->quoteUserID          = 0;
   $this->qzQuoteID            = 0;
   $this->logID                = 0;
   $this->quoteStatusID        = 0;

   $this->objFSClient          = new CFileStoreClient("taxi");


   $this->session              = array();
   $this->unsubscribedUser     = 0;

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
}

// temp here, debug purposes
function MyLogMsg($logMsg = "")
{
   if(empty($logMsg))
      return false;

   $file = new CFile();

   if(! $file->Open("debug-quote.log", "a+"))
      return false;

   $today = date("D M j G:i:s Y");

   $file->Write("$today  $logMsg\n");

   $file->Close();

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InitSite
//
// [DESCRIPTION]:   Initialise a remote site session
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InitSite()
{
   return false;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: FetchProcessInit
//
// [DESCRIPTION]:   Initialiase the remote site sesion and add some info into database
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function FetchProcessInit()
{
   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );

   if(! $this->SetQuoteUserID($this->session['_QZ_QUOTE_DETAILS_']['quote_user_id']))
      return false;

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   if(! $this->SetQzQuoteID($this->session['_QZ_QUOTE_DETAILS_']['qz_quote_id']))
      return false;

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   if(! $this->SetLogID($this->session['_QZ_QUOTE_DETAILS_']['qz_log_id']))
      return false;

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   if(! $this->quoteStatusID = $this->AddQuoteStatusToDB())
      return false;

   // site initialisation
   if(! $this->InitSite())
   {
      $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
      // write the error messages into file to be showed to the customers
      $quoteError = $this->WritePersonalizedSiteError();

      // add quote status into quote_status table
      $this->UpdateQuoteStatusToDB('FAILURE');

      // we have to log this  ?????????????????????????????
      $this->lastUrl = $this->initUrl;

      // add quote error into quote_errors table
      $this->AddQuoteErrorToDB($quoteError);

      //validate and adding the site to the scanning in case of site server errors
      $this->AddSiteScanningToDB('ON');

      // write all file logs
      $this->WriteAllFileLogs("INIT","FAILURE");

      return false;
   }// end if(! $this->InitSite())

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: FetchProcessResponse
//
// [DESCRIPTION]:   Check errors response and process response and add some info into database
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function FetchProcessResponse()
{
   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );

   // write all file logs
   $this->WriteAllFileLogs("STEP","LOG");

   $errorResponse = false;

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   $this->ProcessResponse();

   $status = 'FAILURE';

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   if(! $this->CheckServerResponse())
   {
      $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
      $errorResponse = true;

      if($this->quoteError == 5)
         $status = 'NO QUOTE';
   }

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );

   if(! $this->CheckResponseErrors())
   {
      $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
      // in case of failure we have to set quote error to 3
      if(empty($this->quoteError))
         $this->quoteError = 3;

      $errorResponse = true;
   }

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );

   if($errorResponse)
   {
      $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
      // write the error messages into file to be showed to the customers
      $quoteError = $this->WritePersonalizedSiteError();

      $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
      // add quote error into quote_errors table
      $this->AddQuoteErrorToDB($quoteError);

      $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
      $this->AddSiteScanningToDB('ON');

      $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
      // write all file logs
      $quoteError = $this->WriteAllFileLogs("STEP",'FAILURE');

      $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
      // update quote status into quote_status table
      $this->UpdateQuoteStatusToDB($status);

      return false;
   }

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: FetchProcessResult
//
// [DESCRIPTION]:   Check errors response and process response and add some info into database
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-23
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function FetchProcessResult()
{
   // last page processing FAILURE

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   if(! $this->ProcessLastPage())
   {
      $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
      // write the error messages into file to be showed to the customers
      $quoteError = $this->WritePersonalizedSiteError();

      $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
      // add quote error into quote_errors table
      $this->AddQuoteErrorToDB($quoteError);

      $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
      $this->AddSiteScanningToDB('ON');

      $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
      // write all file logs
      $this->WriteAllFileLogs("QUOTE","FAILURE");

      $status = 'FAILURE';

      $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
      if($this->quoteError == 5)
         $status = 'NO QUOTE';

      $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
      // update quote status into quote_status table
      $this->UpdateQuoteStatusToDB($status);

      return false;
   }

   // last page processing SUCCESS

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   $this->AddQuotesToDB();

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   $this->AddSiteScanningToDB('OFF');

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   $this->SetMailTopQuotes();
   
   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   // write all file logs
   $this->WriteAllFileLogs("QUOTE","SUCCESS");

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   $this->WriteAllFileLogs("LOG");

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   // add quote status into quote_status table
   $this->UpdateQuoteStatusToDB('SUCCESS');

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetLogID
//
// [DESCRIPTION]:   Set quote user ID
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  false | true
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetLogID($logID='')
{
   if(OPERATOR_MODE)
      return true;

   if(! preg_match("/\d+/",$logID))
   {
      $this->AddError('INVALID_RSENGINE_LOG_ID');
      return false;
   }

   $this->logID = $logID;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetQuoteUserID
//
// [DESCRIPTION]:   Set quote user ID
//
// [PARAMETERS]:    $quoteUserID
//
// [RETURN VALUE]:  false | true
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetQuoteUserID($quoteUserID='')
{
   if(OPERATOR_MODE)
      return true;

   if(! preg_match("/\d+/",$quoteUserID))
   {
      $this->AddError('INVALID_RSENGINE_QUOTE_USER_ID');
      return false;
   }

   $this->quoteUserID = $quoteUserID;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetQzQuoteID
//
// [DESCRIPTION]:   Set quotezone quote ID
//
// [PARAMETERS]:    $qzQuoteID
//
// [RETURN VALUE]:  false | true
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetQzQuoteID($qzQuoteID='')
{
   if(OPERATOR_MODE)
      return true;

   if(! preg_match("/\d+/",$qzQuoteID))
   {
      $this->AddError('INVALID_RSENGINE_QZ_QUOTE_ID');
      return false;
   }

   $this->qzQuoteID = $qzQuoteID;

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: LogMsg
//
// [DESCRIPTION]:   Logs an error message
//
// [PARAMETERS]:    $logMsg = ""
//
// [RETURN VALUE]:  tru|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-05-28
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function LogMsg($logMsg = "")
{
   if(empty($logMsg))
   {
      $this->AddError(GetErrorString('EMPTY_LOG_MESSAGE'));
      return false;
   }

   $file = new CFile();

   if(! $file->Open("quote.log", "a+"))
      return false;

   $today = date("D M j G:i:s Y");

   $file->Write("$today  $logMsg\n");

   $file->Close();

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: LogMsg
//
// [DESCRIPTION]:   Logs an error message
//
// [PARAMETERS]:    $logMsg = ""
//
// [RETURN VALUE]:  tru|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-05-28
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function WriteErrorFile($fileName="", $fileContent)
{
   if(OPERATOR_MODE)
      return true;

   if(empty($fileName))
   {
      $this->AddError(GetErrorString('EMPTY_ERROR_FILE_NAME'));
      return false;
   }

   $file = new CFile();

   if(! $file->Open("../errors/".$fileName."_".$this->siteID.".htm", "w+"))
   {
//      $this->AddError(GetErrorString('CANNOT_OPEN_ERROR_FILE'));
      return false;
   }

   if(preg_match("/http-equiv\=\"refresh\"/i",$fileContent))
      $fileContent = preg_replace("http-equiv=\"refresh\"","",$fileContent);

   $file->Write($fileContent);

   $file->Close();

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: LogMsg
//
// [DESCRIPTION]:   Logs an error message
//
// [PARAMETERS]:    $logMsg = ""
//
// [RETURN VALUE]:  tru|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-05-28
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function WriteDataFile($fileName="", $step="", $params=array())
{
   if(OPERATOR_MODE)
      return true;

   if(empty($fileName))
   {
      $this->AddError(GetErrorString('EMPTY_DATA_FILE_NAME'));
      return false;
   }

   $file = new CFile();


   if(! $file->Exists("data/".$fileName."_".$this->siteID.".htm"))
   {
      $fileContent = <<< EOT

      <center>
      <div>
         <table width="600" border="1" cellpading="0" cellspacing="0">
            <tr>
               <td colspan="2" align="center"><b><font size="5">$this->siteName</font></b></td>
            </tr>

EOT;
   }

   if(! $file->Open("../data/".$fileName."_".$this->siteID.".htm", "a+"))
   {
//      $this->AddError(GetErrorString('CANNOT_OPEN_DATA_FILE'));
      return false;
   }

   $url = $this->lastUrl;

   $fileContent .= <<< EOT

      <center>
      <div>
         <table width="600" border="1" cellpading="0" cellspacing="0">
         <tr>
            <td colspan="2" align="center"><b>$step</b></td>
         </tr>
EOT;

   if(! empty($params))
      $fileContent .= <<< EOT
         <tr>
            <td colspan="2" align="center"><b>Url: <a href="$url" target="_blank">$url</a></b></td>
         </tr>
         <tr>
            <td width="400" align="left">Param name</td>
            <td align="center">Param value</td>
         </tr>

EOT;

   foreach($params as $key => $value)
   {
      if($value === '')
         $value = '&nbsp';

      $fileContent .= <<< EOT

         <tr>
            <td width="400" align="left">$key</td>
            <td align="center">$value</td>
         </tr>

EOT;
   }


      $fileContent .= <<< EOT

         </table>
      </div>
      <center>

EOT;

   $file->Write($fileContent);

   $file->Close();

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: LogMsg
//
// [DESCRIPTION]:   Logs an error message
//
// [PARAMETERS]:    $logMsg = ""
//
// [RETURN VALUE]:  tru|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-05-28
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function WriteLogFile($fileName="", $fileContent)
{

   if(OPERATOR_MODE)
      return true;

   if(empty($fileName))
   {
      $this->AddError(GetErrorString('EMPTY_LOG_FILE_NAME'));
      return false;
   }

   $file = new CFile();

   if(! $file->Open("../log/".$fileName."_".$this->siteID.".htm", "a+"))
   {
//      $this->AddError(GetErrorString('CANNOT_OPEN_LOG_FILE'));
      return false;
   }

   if(preg_match("/http-equiv\=\"refresh\"/i",$fileContent))
      $fileContent = preg_replace("http-equiv=\"refresh\"","",$fileContent);

   $file->Write($fileContent);

   $file->Close();

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: LogMsg
//
// [DESCRIPTION]:   Logs an error message
//
// [PARAMETERS]:    $logMsg = ""
//
// [RETURN VALUE]:  tru|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-05-28
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function WriteQuotesFile($fileName="", $fileContent)
{

   if(OPERATOR_MODE)
      return true;

   if(empty($fileName))
   {
      $this->AddError(GetErrorString('EMPTY_LOG_FILE_NAME'));
      return false;
   }

   $file = new CFile();

   if(! $file->Open("../out/".$fileName."_".$this->siteID.".tmp", "w+"))
   {
      $this->AddError(GetErrorString('CANNOT_OPEN_QUOTE_FILE'));
      return false;
   }

   $file->Write($fileContent);

   $file->Close();

   $file->Rename("../out/".$fileName."_".$this->siteID.".tmp","../out/".$fileName."_".$this->siteID.".html",true);

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: WriteErrorMsg
//
// [DESCRIPTION]:   Logs a personalized error message
//
// [PARAMETERS]:    $fileName="", $fileContent
//
// [RETURN VALUE]:  tru|false
//
// [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-05-28
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function WriteErrorMsg($fileName="", $fileContent)
{

   if(OPERATOR_MODE)
      return true;

   if(empty($fileName))
   {
      $this->AddError(GetErrorString('EMPTY_ERROR_MESSAGE_FILE_NAME'));
      return false;
   }

   $file = new CFile();

   if(! $file->Open("../out/".$fileName."_".$this->siteID.".err", "w+"))
   {
//      $this->AddError(GetErrorString('CANNOT_OPEN_ERROR_MESSAGE_FILE'));
      return false;
   }

   $file->Write($fileContent);

   $file->Close();

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckServerResponse
//
// [DESCRIPTION]:   check if the server quote ar online
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true  |  false
//
// [CREATED BY]:    Gabriel ISTVANCSEK (null@seopa.com) 2004-07-01
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckServerResponse()
{
   // unable to create dynamic next url
   if(! empty($this->siteConfig['nextUrl']))
      if(! preg_match("/^\//", $this->siteConfig['nextUrl']))
      {
         $this->AddError(GetErrorString("CANNOT_FIND_NEXT_URL"));
         $this->SetQuoteError(4);
         return false;
      }

   // empty checking
   if(empty($this->htmlContent))
   {
      $this->AddError(GetErrorString("CANNOT_GET_REMOTE_URL"));
      $this->SetQuoteError(1);
      return false;
   }

   // offline checking
   if(! empty($this->offlineServerMessage))
   {
      if(preg_match("/".$this->offlineServerMessage."/is", $this->htmlContent))
      {
         $this->AddError(GetErrorString("SERVER_QUOTING_IS_OFFLINE"));
         $this->SetQuoteError(2);
         return false;
      }
   }

   // unable to qoute you online
   if(! empty($this->unableQuoteOnlineMessage))
   {
      if(preg_match("/".$this->unableQuoteOnlineMessage."/is", $this->htmlContent))
      {
         $this->AddError(GetErrorString("SERVER_CANNOT_PROVIDE_ONLINE_QUOTE_BIKE"));
         $this->SetQuoteError(5);

         return false;
      }
   }

   return true;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetOfflineServerMessage
//
// [DESCRIPTION]:   set the message offline server
//
// [PARAMETERS]:    $serverMessage
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel ISTVANCSEK (null@seopa.com) 2004-07-01
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetOfflineServerMessage($serverMessage="")
{
   $this->offlineServerMessage = $serverMessage;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetOfflineServerMessage
//
// [DESCRIPTION]:   set the message offline server
//
// [PARAMETERS]:    $serverMessage
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Gabriel ISTVANCSEK (null@seopa.com) 2004-07-01
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetUnableQuoteOnlineServerMessage($serverMessage="")
{
   $this->unableQuoteOnlineMessage = $serverMessage;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetSession
//
// [DESCRIPTION]:   set the session for this module. It is a simulated session array
//
// [PARAMETERS]:    $session=array()
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Eugen SAVIN (null@seopa.com) 2005-07-19
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetSession($session=array())
{
   $this->session = $session;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetQuoteError
//
// [DESCRIPTION]:   set type of quote error
//
// [PARAMETERS]:    $quoteError
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-21
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetQuoteError($quoteError)
{
   $this->quoteError = $quoteError;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteStatusToDB
//
// [DESCRIPTION]:   insert the entry into logs table
//
// [PARAMETERS]:    
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-21
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteStatusToDB()
{
   if(OPERATOR_MODE)
      return true;

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   $objQuoteStatus = new CQuoteStatus($this->dbh);

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   // we dont logs anything if the is in the scanning mode
   if($this->scanningFlag)
   {
      $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
      if(! $resQuoteStatus = $objQuoteStatus->GetQuoteStatusByLogIdAndSiteId($this->logID, $this->siteID))
         $this->AddError($objQuoteStatus->GetError());

      return $resQuoteStatus['id'];
   }

   $type = 'NORMAL';
   if($this->debugMode)
      $type = 'DEBUG';

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );

   if(! $quoteStatusID = $objQuoteStatus->AddQuoteStatus($this->logID, $this->siteID,'WAITING',$type))
      $this->AddError($objQuoteStatus->GetError());

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   return $quoteStatusID;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: UpdateQuoteStatusToDB
//
// [DESCRIPTION]:   update quote status 
//
// [PARAMETERS]:    $status
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-21
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function UpdateQuoteStatusToDB($status)
{
   if(OPERATOR_MODE)
      return true;

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   $this->MyLogMsg("status is: $status");

   // we dont logs anything if the is in the scanning mode
   $type = 'NORMAL';
   if($this->scanningFlag)
      $type = 'SCAN';
   else if($this->debugMode)
      $type = 'DEBUG';

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
    $objQuoteStatus = new CQuoteStatus($this->dbh);

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   if(! $objQuoteStatus->UpdateQuoteStatus($this->quoteStatusID, $this->logID, $this->siteID, $status, $type))
      $this->AddError($objQuoteStatus->GetError());

   $this->MyLogMsg($this->siteName . " " . $this->fileName . " " . __CLASS__ . " " . __METHOD__ . " at line " . __LINE__ );
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteErrorToDB
//
// [DESCRIPTION]:   insert the entry into logs table
//
// [PARAMETERS]:    $quoteStatusID
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-21
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuoteErrorToDB($quoteError)
{
   // we dont logs anything if the is in the scanning mode
   //if($this->scanningFlag)
   //   return false;

   if(OPERATOR_MODE)
      return true;

    $objQuoteError = new CQuoteError($this->dbh);

   if(! $quoteErrorID = $objQuoteError->AddQuoteError($this->quoteStatusID, $this->lastUrlID, $this->lastUrl, $this->GetError(), $quoteError))
      $this->AddError($objQuoteError->GetError());

   return $quoteErrorID;
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddQuoteStatusToDB
//
// [DESCRIPTION]:   insert the entry into logs table
//
// [PARAMETERS]:    $quoteStatusID
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-21
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddQuotesToDB()
{
   
   if(OPERATOR_MODE)
      return true;

   $quoteReference = $this->siteConfig['quote_details'][0]['quote reference'];
   $quotePassword  = $this->siteConfig['quote_details'][0]['password'];

   $objQuote        = new CQuote($this->dbh);
   $objQuoteDetails = new CQuoteDetails($this->dbh);

   if(! $quoteID = $objQuote->AddQuote($this->quoteStatusID, $quoteReference, $quotePassword))
      $this->AddError($objQuote->GetError());
/*
   foreach($this->siteConfig['quote_details'] as $qResultsIndex => $quoteResultsArray)
   {
      if(! $objQuoteDetails->AddQuoteDetails($quoteID, $quoteResultsArray['insurer'], $quoteResultsArray['annual premium'], $quoteResultsArray['monthly premium'], $quoteResultsArray['voluntary excess']))
         $this->AddError($objQuoteDetails->GetError());
   }
*/
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: AddSiteScanningToDB
//
// [DESCRIPTION]:   add the site to be relanched in case of quoting server offline
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-21
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function AddSiteScanningToDB($status)
{
   if(OPERATOR_MODE)
      return true;

   if($this->debugMode)
      return;

   $objScanning = new CQuoteScanning($this->dbh);

   switch($this->quoteError)
   {
      case '1':
      case '2':

      if(! $this->scanningFlag)
      {
         if(! $objScanning->AddQuoteScanning($this->quoteStatusID))
            $this->AddError($objScanning->GetError());
      }
      else
      {
         if(! $objScanning->UpdateQuoteScanning($this->quoteStatusID,$status))
            $this->AddError($objScanning->GetError());
      }

      break;

      case '3':
      case '4':
      case '5':
         if(! $this->scanningFlag)
         {
            if(! $objScanning->AddQuoteScanning($this->quoteStatusID))
               $this->AddError($objScanning->GetError());
         }
         else // force status to "OFF" in case of no quote or no next url when scanning
         {
            if(! $objScanning->UpdateQuoteScanning($this->quoteStatusID, "OFF"))
               $this->AddError($objScanning->GetError());
         }

      break;

      default:

         // SUCCESS OR FAILURE
         if($this->scanningFlag)
         {
            if(! $objScanning->UpdateQuoteScanning($this->quoteStatusID,$status))
               $this->AddError($objScanning->GetError());
         }

      break;
   }
}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: SetMailTopQuotes()
//
// [DESCRIPTION]:   we personalized the error which is showed to the customers
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-10-25
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function SetMailTopQuotes()
{
   if(OPERATOR_MODE)
      return true;

   // if we dont running in scan mode return;
   if(! $this->scanningFlag)
      return;

   $topOne   = 'OFF';
   $topThree = 'OFF';

   $objQuoteDetails = new CQuoteDetails($this->dbh);

   if(! $resTopQuoteDetails = $objQuoteDetails->GetCheapestTopFiveSitesQuoteDetails($this->logID, 4))
      $this->AddError($objQuoteDetails->GetError());

   // top 1 
   // we have to send email to the user with new cheapest quote
   if($resTopQuoteDetails[1] == $this->siteID)
   {
      $topOne = 'ON';
   }

   // top 3
   // we have to send email to the insurers
   for($i=1; $i < 5; $i++)
   {
      if($i < 4)
      {
         if($resTopQuoteDetails[$i] == $this->siteID)
         {
            $topThree = 'ON';
            break;
         }

         continue;
      }
   }

   if($topOne == 'OFF' && $topThree == 'OFF')
      return;

   $objMailInfo = new CMailInfo($this->dbh);

   if(! $objMailInfo->SetSendMailInfo($this->qzQuoteID,$topOne,$topThree))
      $this->AddError($objMailInfo->GetError());
   
   return;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: WritePersonalizedSiteError()
//
// [DESCRIPTION]:   we personalized the error which is showed to the customers
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-21
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function WritePersonalizedSiteError()
{
   // we personalized the error which is showed to the customers
   switch($this->quoteError)
   {
      // cannot get remote url
      case '1':
            $quoteError = GetErrorString('PERSONALIZED_CANNOT_GET_REMOTE_URL');
         break;
      // server quoting system si offline
      case '2':
            $quoteError = GetErrorString('PERSONALIZED_SERVER_QUOTING_IS_OFFLINE');
         break;
      case '3':
            $quoteError = GetErrorString('PERSONALIZED_INTERNAL_SERVER_ERROR');
         break;
   // http://www.insure-systems.co.ukProposer Details INVALID CONSTRUCT URL !!!
      case '4':
            $quoteError = GetErrorString('PERSONALIZED_CANNOT_FIND_NEXT_URL');
         break;

      case '5':
            $quoteError = GetErrorString('PERSONALIZED_SERVER_CANNOT_PROVIDE_ONLINE_QUOTE_TRAVEL');
         break;

      // default server cannot provide online quote by your data
      default:
            $quoteError = GetErrorString('PERSONALIZED_SERVER_CANNOT_PROVIDE_ONLINE_QUOTE_TRAVEL');
         break;
   }

   $this->WriteErrorMsg($this->fileName,$quoteError);
   
   return $quoteError;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: WriteAllFileLogs($category,$type)
//
// [DESCRIPTION]:   write all log files like error,data,debug
//
// [PARAMETERS]:    $category,$type
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-21
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function WriteAllFileLogs($category,$type='')
{
 
   $htmlFile  = $this->fileName."_".$this->siteID.".html";
   $htmFile   = $this->fileName."_".$this->siteID.".htm";
   $errFile   = $this->fileName."_".$this->siteID.".err";

   $scanningLog = '';
   if($this->scanningFlag)
      $scanningLog = 'SCANNING';

   $debugLog = '';
   if($this->debugMode)
      $debugLog = '!!! DEBUG !!! ';


   switch($category)
   {
      case 'INIT':
         switch($type)
         {
            case 'FAILURE':
                  $this->LogMsg("$debugLog $scanningLog PID : ".posix_getpid()." -> ".$this->siteName." STATUS => ERROR : \n".$this->GetError());

                  $this->WriteErrorFile($this->fileName, $this->htmlContent);
                  
                  // send error file
                  $this->objFSClient->SetFileType("errors");
                  $this->objFSClient->PutFile($htmFile);
                   
                   // send error message file
                  $this->objFSClient->SetFileType("out");
                  $this->objFSClient->PutFile($errFile);
                  
               break;
         }

         break;
      case 'STEP':
         switch($type)
         {
            case 'SKIP':
               $this->LogMsg("$debugLog $scanningLog PID : ".posix_getpid()." -> ".$this->siteName." => ".$this->lastUrlID." Skipped");
               break;

            case 'LOG':
               // log all steps site with params  - debug mode - comment this in normal mode
               $this->WriteDataFile($this->fileName, "Step ".$urlID." -> ".$this->urls[$this->lastUrlID], $this->urlParams[$this->lastUrlID]);

               // log all steps site - debug mode - comment this in normal mode
               $this->WriteLogFile($this->fileName, $this->htmlContent);
         
               // log steps
               $this->LogMsg("$debugLog $scanningLog PID : ".posix_getpid()." -> ".$this->siteName." => ".$this->urls[$this->lastUrlID]);
            break;

            case 'FAILURE':
               // log error
               $this->LogMsg("$debugLog $scanningLog PID : ".posix_getpid()." -> ".$this->siteName." STATUS => ERROR : \n".$this->GetError());

               $this->WriteErrorFile($this->fileName, $this->htmlContent);

               
               // send error file
               $this->objFSClient->SetFileType("errors");
               $this->objFSClient->PutFile($htmFile);
                
                // send data file
               $this->objFSClient->SetFileType("data");
               $this->objFSClient->PutFile($htmFile);
             
                // send log file
               $this->objFSClient->SetFileType("log");
               $this->objFSClient->PutFile($htmFile);
                
                // send error message file
               $this->objFSClient->SetFileType("out");
               $this->objFSClient->PutFile($errFile);
               

            break;
         }

         break;
      case 'QUOTE':
         switch($type)
         {
            case 'SUCCESS':
               // succes quote
               $this->LogMsg("$debugLog $scanningLog PID : ".posix_getpid()." -> ".$this->siteName." STATUS => SUCCESS");

               // log all steps site - debug mode - comment this in normal mode
               $this->WriteLogFile($this->fileName, $this->htmlContent);

               // write the quote premiums into file
               $this->WriteQuotesFile($this->fileName, var_export($this->siteConfig['quote_details'],true));
               
               
               // send data file
               $this->objFSClient->SetFileType("data");
               $this->objFSClient->PutFile($htmFile);
             
                // send log file
               $this->objFSClient->SetFileType("log");
               $this->objFSClient->PutFile($htmFile);
                
                // send quotes file
               $this->objFSClient->SetFileType("out");
               $this->objFSClient->PutFile($htmlFile);
               
               
               break;
            case 'FAILURE':
               // log error
               $this->LogMsg("$debugLog $scanningLog PID : ".posix_getpid()." -> ".$this->siteName." STATUS => ERROR : \n".$this->GetError());
         
               $this->WriteErrorFile($this->fileName, $this->htmlContent);
               
               
               // send error file
               $this->objFSClient->SetFileType("errors");
               $this->objFSClient->PutFile($htmFile);
                
                // send data file
                $this->objFSClient->SetFileType("data");
                $this->objFSClient->PutFile($htmFile);
             
                // send log file
                $this->objFSClient->SetFileType("log");
                $this->objFSClient->PutFile($htmFile);
                
                // send error message file
                $this->objFSClient->SetFileType("out");
                $this->objFSClient->PutFile($errFile);
                

               break;
         }

         break;
         
      case 'LOG':
         if(! empty($this->strERR))
            $this->LogMsg("\n !!! WARNINGS !!! \n".$this->GetError());
       break;
   }
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DoOutbound
//
// [DESCRIPTION]:   Ads entry to outbounds table
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Moraru Valeriu 2011-04-25
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DoOutbound()
{
   global $ACRUX_IP_ARRAY;

   $outboundObj       = new COutbound();
   $logID             = $this->session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];

   $sentStatus        = 0;
   $receivedStatus    = 0;
   $xtraParams        = "";

   $serverIp =  gethostbyname("taxi-quote-server");

   if($serverIp == "taxi-quote-server")
      $serverIp = $this->session["_QZ_QUOTE_DETAILS_"]["SERVER_IP"];

   $date              = date("Y-m-d");
   $time              = date("H:i:s");


   /* ================== INFO ===================
    If we have the scanner flag we do not have to add default values of this outbound
    Instead we need to get this outbound id and increment his 'tries' field and return it so the process can rerun it
   */

   if($this->outboundScanningFlag)
   { 
      $scannerOutboundsId = array();
      $scannerArray       = $outboundObj->GetQuoteOutbounds($logID,$this->siteID);

      foreach($scannerArray as $key=>$outboundsArray)
      {
         //update tries for every outbound in here
         $outboundObj->UpdateOutboundTries($outboundsArray["outbound_id"]);
         $this->outbounds[$outboundsArray["type"]] = $outboundsArray["outbound_id"];
      }

      return true;
   }

   if(! $outboundTypes = $outboundObj->GetOutboundConfigTypes($this->siteID)) 
      return false;

   foreach($outboundTypes as $key => $outboundTypeArray)
   {
      if($outboundObj->CheckOutboundExists($logID, $outboundTypeArray['id']))
         continue;

      if($outboundTypeArray['status'] == "ON")
      {
         if($outboundID = $outboundObj->AddOutbound($logID, $outboundTypeArray["id"], $sentStatus, $receivedStatus, $serverIp, $extraParams, $date, $time))
         {
            $this->outbounds[$outboundTypeArray["type"]] = $outboundID;
         }
      }
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DoOutboundFiles
//
// [DESCRIPTION]:   Adds the outbound files to filestore
//
// [PARAMETERS]:    $outboundType,$fileExtension
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Racu Daniel 2008-05-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DoOutboundFiles($fileExtension,$sentContent = "",$receivedContent = "")
{
   $objFileStoreClient = new CFileStoreClient();
   $file               = new CFile();

   $objFileStoreClient->SetSystemType('outbounds');

   $fileName = $this->session['TEMPORARY FILE NAME']."_".$this->siteID.".".$fileExtension;

   if($sentContent != "")
   {
      //create the SENT file on local hard
      $file->Create(FS_ROOT_PATH."sent/".$fileName);
      $file->Open(FS_ROOT_PATH."sent/".$fileName,"w+");
      $file->Write($sentContent);
      $file->Close();

      //ads sent file to fileStore
      $objFileStoreClient->SetFileType('sent');
      $objFileStoreClient->PutFile($fileName);
   }

   if($receivedContent != "")
   {
      //create the RECEIVED file on local hard
      $file->Create(FS_ROOT_PATH."received/".$fileName);
      $file->Open(FS_ROOT_PATH."received/".$fileName,"w+");
      $file->Write($receivedContent);
      $file->Close();

      //ads received file to fileStore
      $objFileStoreClient->SetFileType('received');
      $objFileStoreClient->PutFile($fileName);
   }
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: Close
//
// [DESCRIPTION]:   Close the object and also close the connection with the
//                  database server if necessary
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Istvancsek Gabriel (null@seopa.com) 2005-09-12
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function Close()
{
    if($this->closeDB)
       $this->dbh->Close();

    return;
}

} // end of CBIKEEngine class

?>

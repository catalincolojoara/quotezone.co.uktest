<?php

/*****************************************************************************/
/*                                                                           */
/*   CTaxiQuotaxEngine class interface                                       */
/*                                                                           */
/*  (C) 2008 Furtuna Alexandru (alexandru.furtuna@seopa.com)                              */
/*                                                                           */
/*****************************************************************************/
include_once "TAXIEngine.php";
include_once "Url.php";
include_once "Site.php";
include_once "Session.php";
include_once "ArList.php";
include_once "Navigation.php";
include_once "Postcode.php";
include_once "functions.php";
include_once "YourDetailsElements.php";
include_once "EmailContent.php";

//outbound nedded
include_once "LeadsCompanyEmails.php";
include_once "SMTP.php";
include_once "File.php";
include_once "Outbound.php";

error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CTaxiQuotaxEngine
//
// [DESCRIPTION]:  CTaxiQuotaxEngine class interface, remote site data exchange support,
//                 PHP version
//
// [FUNCTIONS]:   bool   InitSite()
//                string PrepareUrl($url = "")
//                bool   ProcessResponse()
//                bool   ProcessLastPage()
//                bool   CheckResponseErrors()
//                array  GetResultElements()
//                void   PrepareQuote($SESSION)
//
// [CREATED BY]:   Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CTaxiQuotaxEngine extends CTAXIEngine
{

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CTaxiQuotaxEngine
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CTaxiQuotaxEngine($fileName, $scanningFlag, $debugMode, $testingMode)
{
   CTAXIEngine::CTAXIEngine("taxi-insurance.quotezone.co.uk", $fileName, $scanningFlag, $debugMode, $testingMode);

   $this->SetFailureResponse("There are errors on this page, please correct");

   $this->SetUnableQuoteOnlineServerMessage("Unfortunately we are unable to provide you with an on-line aplication");
   $this->SetOfflineServerMessage("The online aplication system is currently offline");

   $this->SetProtocol("https");

   $this->siteID = "798";


}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InitSite
//
// [DESCRIPTION]:   Initialise a remote site session
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InitSite()
{
   return true;
}

/////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DoOutbound
//
// [DESCRIPTION]:   send outbound
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Moraru Valeriu (null@seopa.com) 2011-04-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DoOutbound()
{
   //error_reporting(E_ALL);

   global $ACRUX_IP_ARRAY;
   global $_YourDetails;

   $objLeadsCompanyEmails = new CLeadsCompanyEmailsModule();

   $outboundObj         = new COutbound();
   $mailObj             = new CSMTP();
   $objFile             = new CFile();

   CTAXIEngine::DoOutbound();

   $logID  = $this->session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];

   if($this->outbounds['MAIL'])
   {
      // get emails to send from db
      $sendEmailsArray = array();
      $sendEmailsArray = $objLeadsCompanyEmails->GetAllEmailsToSendForCompanyBySiteID($this->siteID);

      print_r($sendEmailsArray);

      $message_body = GetEmailTemplate($this->session);

      $subject = "Taxi query : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];

      $headers  = 'MIME-Version: 1.0' . "\r\n";
      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
      //$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
      $headers .= "X-Mailer: PHP\r\n"; // mailer
      $headers .= "X-Priority: 1\r\n"; // Urgent message!
      //$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

      $mailFrom    = "taxi@quotezone.co.uk";
      $mailSubject = $subject;
      $mailBody    = $message_body;
      $emailId     = "";
      $mailToAll   = "";

      foreach($sendEmailsArray as $emailType=>$emailsArray)
      {
         $countEmails = count($emailsArray);

         switch($emailType)
         {
            case "COMPANY":
               if( ! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")
               {
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject;

                     $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }
               }
               break;

            case "MARKETING":

               if( ! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")
               {
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject." Quotax";

                     $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }
               }
               break;

            case "TECHNICAL":

               //send emails to TECHNICAL department
               for($k=0;$k<$countEmails;$k++)
               {
                  $mailTo      = $emailsArray[$k];
                  print "send to:".$mailTo."\n";

                  $mailSubject = $subject." Quotax";

                  $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                  $emailId   .= $mailObj->GetMessageID().",";
                  $mailToAll .= $mailTo.",";
               }
               break;
         }// switch
      }// foreach

      // remove last coma
      $emailId   = substr($emailId,0,-1);
      $mailToAll = substr($mailToAll,0,-1);

      $outboundObj->UpdateOutboundField($this->outbounds['MAIL'],'sent_status', 1);
      $outboundObj->UpdateOutboundField($this->outbounds['MAIL'],'extra_params', $emailId);

      $sentContent =  "\nHEADERS: ".$headers."\nFROM:".$mailFrom."\nTO:".$mailToAll."\nSUBJECT: ".$mailSubject."\nBODY: ".$mailBody;

      $this->DoOutboundFiles("mail", $sentContent);

   } //end if MAIL
if($this->outbounds['XML'])
   {
      if( in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) OR $this->session['_YourDetails_']['email_address'] == "test@quotezone.co.uk")
         return;

      $taxiUsedFor        = $this->session['_YourDetails_']['taxi_used_for'];
      $taxiType           = $this->session['_YourDetails_']['taxi_type'];
      $taxiMake           = $this->session['_YourDetails_']['taxi_make'];
      $vehicleYear        = $this->session['_YourDetails_']['year_of_manufacture'];
      $typeOfVehicle      = "Taxi";
      $taxiTypeOfCover    = $this->session['_YourDetails_']['type_of_cover'];
      $taxiVehicleMileage = $this->session['_YourDetails_']['vehicle_mileage'];
      $taxiModel          = $this->session['_YourDetails_']['taxi_model'];
      $estimatedValue     = $this->session['_YourDetails_']['estimated_value'];
      $taxiCapacity       = $this->session['_YourDetails_']['taxi_capacity'];
      
      $platingAuth        = $this->session['_YourDetails_']['plating_authority'];
      $gapInsurance       = $this->session['_YourDetails_']['gap_insurance'];
      $breakdownCover     = $this->session['_YourDetails_']['breakdown_cover'];
      $bestTime           = $this->session['_YourDetails_']['best_time_call'];
      $bestDay            = '';
      $dateAndTime        = date("Y/m/d H:i:s");

      $title              = $this->session['_YourDetails_']['title'];
      $firstName          = $this->session['_YourDetails_']['first_name'];
      $surname            = $this->session['_YourDetails_']['surname'];
      $birthDate          = $this->session['_YourDetails_']['date_of_birth'];
      $dayTimePhone       = $this->session['_YourDetails_']['daytime_telephone'];
      $mobilePhone        = $this->session['_YourDetails_']['mobile_telephone'];
      $postcode           = $this->session['_YourDetails_']['postcode'];
      $houseNumber        = $this->session['_YourDetails_']['house_number_or_name'];
      $email              = $this->session['_YourDetails_']['email_address'];
      $dob                = $this->session['_YourDetails_']['date_of_birth_mm']."".$this->session['_YourDetails_']['date_of_birth_dd']."".$this->session['_YourDetails_']['date_of_birth_yyyy'];
      $engineSize         = "";
      $annualMileage      = "";
      $title              = $this->session['_YourDetails_']['title'];
      $firstName          = $this->session['_YourDetails_']['first_name'];
      $lastName           = $this->session['_YourDetails_']['surname'];
      $daytime_telephone  = $this->session['_YourDetails_']['daytime_telephone'];
      $mobile_telephone   = $this->session['_YourDetails_']['mobile_telephone'];

      $dest               = $daytime_telephone?$daytime_telephone:$mobile_telephone;

      $make               = $taxiMake;
      $model              = $taxiModel;
      $systemType         = "Taxi";

      $params  = "?login=qztaxiqx";
      $params .= "&password=qztaxiqx";
      $params .= "&destday=".urlencode($daytime_telephone);
      $params .= "&desteve=".urlencode($mobile_telephone);
      $params .= "&info_A._Title=".urlencode($title);
      $params .= "&info_B._First_Name=".urlencode($firstName);
      $params .= "&info_C._Last_Name=".urlencode($lastName);
      $params .= "&Info_D._Home_Address=".urlencode($this->session['_YourDetails_']['house_number_or_name']);
      $params .= "&info_E._Home_Postcode=".urlencode($this->session['_YourDetails_']['postcode']);
      $params .= "&info_F._Email=".urlencode($this->session['_YourDetails_']['email_address']);
      $params .= "&info_G._date_of_Birth=".urlencode($dob);
      $params .= "&info_H._Reg_Number=".urlencode($this->session['_YourDetails_']['vehicle_registration_number']);
      $params .= "&info_I._Vehicle_Make=".urlencode($make);
      $params .= "&info_J._Vehicle_Model=".urlencode($model);
      $params .= "&info_K._Engine_size=".urlencode($engineSize);
      $params .= "&info_L._approx_annual_Mileage=".urlencode($annualMileage);
      $params .= "&info_N._Vehicle_Value=".urlencode($estimatedValue);

      $taxiType   = $this->session['_YourDetails_']['taxi_type'];
      $params .= "&info_P._Taxi_Type=".urlencode($_YourDetails["taxi_type"][$taxiType]);

      $params .= "&info_Q._Year_Of_Manufacture=".urlencode($vehicleYear);

      $taxiBadge = $this->session['_YourDetails_']['taxi_badge'];
      $params .= "&info_R._Taxi_PCV_badge_held_for=".urlencode($_YourDetails["taxi_badge"][$taxiBadge]);
      $params .= "&info_S._Name_Of_Licensing_Authority=".urlencode($platingAuth);
      $params .= "&info_T._Number_Of_Passengers=".urlencode($taxiCapacity);
      $params .= "&info_U._Claims_last_5_years=".urlencode($this->session['_YourDetails_']['claims_5_years']);
      $params .= "&info_V._Convictions_last_5_years=".urlencode($this->session['_YourDetails_']['convictions_5_years']);

      $taxiNcb            = $this->session['_YourDetails_']['taxi_ncb'];
      $params .= "&info_W._Taxi_NCB=".urlencode($_YourDetails["taxi_ncb"][$taxiNcb]);
      
// login=            qztaxiqx
// password=         qztaxiqx
// destday=
// desteve=
// info_A._Title=
// info_B._First_Name=
// info_C._Last_Name=
// Info_D._Home_Address=
// info_E._Home_Postcode=
// info_F._Email=
// info_G._date_of_Birth=
// info_H._Reg_Number=
// info_I._Vehicle_Make =
// info_J._Vehicle_Model=
// info_K._Engine_size=
// info_L._approx_annual_Mileage =
// info_N._Vehicle_Value=
// info_P._Taxi_Type=
// info_Q._Year_Of_Manufacture=
// info_R._Taxi_PCV_badge_held_for=
// info_S._Name_Of_Licensing_Authority=
// info_T._Number_Of_Passengers=
// info_U._Claims_last_5_years=
// info_V._Convictions_last_5_years=
// info_W._Taxi_NCB=


      $ch = curl_init();

      $url    = "http://www.leadcall.co.uk/followup/";

      $leadcall = $url.$params;

      print "\n$leadcall\n\n";

      //Get
      curl_setopt($ch, CURLOPT_URL, $leadcall);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $result = curl_exec($ch);
      curl_close($ch);

				$printParams = str_replace("?login=qztaxiqx&password=qztaxiqx","",$params);
				$printParams = str_replace("&","\r\n&",$printParams);
				
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
            //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
            ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!
            ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Quotax LeadCall-Optilead Request";
            $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
            
            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi Quotax LeadCall-Optilead Request";
            $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);  


      print "\n========== result ===============\n";
      print $result;
      print "\n========== result ===============\n";

      $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

      if( ! preg_match("/ERROR/i",$result))
         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
      else
         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

      $sentContent =  "\nURL: ".$url."\nPARAMS:".$params;

      $this->DoOutboundFiles("xml", $sentContent,$result);

   } //end if XML

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: PrepareUrl
//
// [DESCRIPTION]:   Prepare a remote site config
//
// [PARAMETERS]:    $url = ""
//
// [RETURN VALUE]:  string, prepared url
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function PrepareUrl($url = "")
{
   $url = $this->siteConfig['nextUrl'];
   $this->httpReferer = $this->siteConfig['refererUrl'];
   return $url;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Get form name of current page
//
// [PARAMETERS]:    array
//
// [RETURN VALUE]:  the form name|_body_form
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetFormName($formParams)
{

   if(count($formParams) == 1)
      return "_body_form";

   foreach ($formParams as $formName => $value)
      if($formName != '_body_form')
         return $formName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessResponse()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessLastPage
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessLastPage()
{
   print "QUOTE SUCCESS";
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckResponseErrors
//
// [DESCRIPTION]:   Check an html response page for errors. Extract errors.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckResponseErrors()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetResultElements
//
// [DESCRIPTION]:   Get the results from the last page
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetResultElements()
{
   return;
} // end function GetResultElements(&$elementArray)

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ParseXmlResponse
//
// [DESCRIPTION]:   Get the results from the last page only for a broker
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  false | array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ParseXmlResponse()
{
   return;
}
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetXmlTemplate()
   //
   // [DESCRIPTION]:   -
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetXmlTemplate($type='',$d='', $sd='')
   {
      return;
   }


   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareXml
   //
   // [DESCRIPTION]:   Prepare the xml to send to PD
   //
   // [PARAMETERS]:    $SESSION
   //
   // [RETURN VALUE]:  xml data to send
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareXml($SESSION)
   {

   return;

   }// end function PrepareXml($SESSION)
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareQuote
   //
   // [DESCRIPTION]:   Prepare the array of params to get a quote
   //
   // [PARAMETERS]:    $sessionParams ($_SESSION);
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-11-11
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareQuote
   //
   // [DESCRIPTION]:   Prepare the array of params to get a quote
   //
   // [PARAMETERS]:    $sessionParams ($_SESSION);
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-11-11
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareQuote()
   {
      $this->DoOutbound();

      return;
   }// end function PrepareQuote()

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetUrl
   //
   // [DESCRIPTION]:   Download/Open a remote web page
   //
   // [PARAMETERS]:    $url="", [method="GET"], [$params=""] [$referer=""]
   //
   // [RETURN VALUE]:  downloaded page text if success, "" otherwise
   //
   // [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetUrl($url="", $method="GET", $params="", $referer="")
   {

      $this->htmlContent = "";

      if(empty($url))
      {
         $this->strERR = GetErrorString("INVALID_URL");
         return false;
      }

      $this->proto = "https://";

      // add protocol
      if(! preg_match("/:\/\//", $url))
         $url = $this->proto.$url;

      if(is_array($params))
      {
         $paramString = "";

         foreach($params as $key => $value)
            $paramString .= $key . '=' . $value . '&';

         $paramString = preg_replace("/\&$/","", $paramString);
         $params = $paramString;
      }

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      curl_setopt($ch, CURLOPT_URL,$url);

      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);

      //curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HEADER, 1);

      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; X11; Linux i686) Opera 7.54  [en]');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      $result = curl_exec($ch);

      curl_close ($ch);

      if(empty($result))
      {
         $this->strERR  = "CANNOT_GET_REMOTE_URL";
         $this->strERR .= curl_error($ch)."\n";
         return false;
      }

      preg_match("/^(.*)\n(.*)$/is", $result,$matches);

      $this->httpHeader  = $matches[1];
      $this->htmlContent = $matches[2];

      //print "\n======== result ===========\n".$this->htmlContent."\n=========================\n";die;
      return $result;
   }


} // end of CTaxiQuotaxEngine class

?>



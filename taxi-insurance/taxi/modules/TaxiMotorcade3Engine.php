<?php

/*****************************************************************************/
/*                                               
 *   This is Motorcade (Palace)                             */
/*   CTaxiMotorcade3Engine class interface                                       */
/*                                                                           */
/*  (C) 2010 Sturza Ciprian (ciprian.sturza@seopa.com)                                 */
/*                                                                           */
/*****************************************************************************/
include_once "TAXIEngine.php";
include_once "Url.php";
include_once "Site.php";
include_once "Session.php";
include_once "ArList.php";
include_once "Navigation.php";
include_once "Postcode.php";
include_once "functions.php";
include_once "YourDetailsElements.php";
include_once "EmailContent.php";

//outbound nedded
include_once "LeadsCompanyEmails.php";
include_once "SMTP.php";
include_once "File.php";
include_once "Outbound.php";

error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CTaxiMotorcade3Engine
//
// [DESCRIPTION]:  CTaxiMotorcade3Engine class interface, remote site data exchange support,
//                 PHP version
//
// [FUNCTIONS]:   bool   InitSite()
//                string PrepareUrl($url = "")
//                bool   ProcessResponse()
//                bool   ProcessLastPage()
//                bool   CheckResponseErrors()
//                array  GetResultElements()
//                void   PrepareQuote($SESSION)
//
// [CREATED BY]:   Sturza Ciprian (ciprian.sturza@seopa.com) 2010-05-07
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CTaxiMotorcade3Engine extends CTAXIEngine
{

   //var $mailObj; //SMTP object

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CTaxiMotorcade3Engine
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Sturza Ciprian (ciprian.sturza@seopa.com) 2010-05-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CTaxiMotorcade3Engine($fileName, $scanningFlag, $debugMode, $testingMode)
{
   CTAXIEngine::CTAXIEngine("taxi-insurance.quotezone.co.uk", $fileName, $scanningFlag, $debugMode, $testingMode);

   $this->SetFailureResponse("There are errors on this page, please correct");

   $this->SetUnableQuoteOnlineServerMessage("Unfortunately we are unable to provide you with an on-line aplication");
   $this->SetOfflineServerMessage("The online aplication system is currently offline");

   $this->SetProtocol("https");

   $this->siteID = "2462";

}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InitSite
//
// [DESCRIPTION]:   Initialise a remote site session
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InitSite()
{
   return true;
}

/////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DoOutbound
//
// [DESCRIPTION]:   send outbound
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Moraru Valeriu (null@seopa.com) 2011-04-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DoOutbound()
{
   //error_reporting(E_ALL);

   global $ACRUX_IP_ARRAY;

   $objLeadsCompanyEmails = new CLeadsCompanyEmailsModule();

   $outboundObj         = new COutbound();
   $mailObj             = new CSMTP();
   $objFile             = new CFile();

   CTAXIEngine::DoOutbound();

   $logID  = $this->session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];

   if($this->outbounds['MAIL'])
   {
      // get emails to send from db
      $sendEmailsArray = array();
      $sendEmailsArray = $objLeadsCompanyEmails->GetAllEmailsToSendForCompanyBySiteID($this->siteID);

      print_r($sendEmailsArray);

      $message_body = GetEmailTemplate($this->session);

      $subject = "Taxi query : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];

      $headers  = 'MIME-Version: 1.0' . "\r\n";
      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
      //$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
      $headers .= "X-Mailer: PHP\r\n"; // mailer
      $headers .= "X-Priority: 1\r\n"; // Urgent message!
      //$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

      $mailFrom    = "taxi@quotezone.co.uk";
      $mailSubject = $subject;
      $mailBody    = $message_body;
      $emailId     = "";
      $mailToAll   = "";

      foreach($sendEmailsArray as $emailType=>$emailsArray)
      {
         $countEmails = count($emailsArray);

         switch($emailType)
         {
            case "COMPANY":
               if( ! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")
               {
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject;

                     $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }
               }
               break;

            case "MARKETING":

               if( ! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")
               {
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject." Motorcade 3";

                     $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }
               }
               break;

            case "TECHNICAL":

               //send emails to TECHNICAL department
               for($k=0;$k<$countEmails;$k++)
               {
                  $mailTo      = $emailsArray[$k];
                  print "send to:".$mailTo."\n";

                  $mailSubject = $subject." Motorcade 3";

                  $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                  $emailId   .= $mailObj->GetMessageID().",";
                  $mailToAll .= $mailTo.",";
               }
               break;
         }// switch
      }// foreach

      // remove last coma
      $emailId   = substr($emailId,0,-1);
      $mailToAll = substr($mailToAll,0,-1);

      $outboundObj->UpdateOutboundField($this->outbounds['MAIL'],'sent_status', 1);
      $outboundObj->UpdateOutboundField($this->outbounds['MAIL'],'extra_params', $emailId);

      $sentContent =  "\nHEADERS: ".$headers."\nFROM:".$mailFrom."\nTO:".$mailToAll."\nSUBJECT: ".$mailSubject."\nBODY: ".$mailBody;

      $this->DoOutboundFiles("mail", $sentContent);

   } //end if MAIL
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: PrepareUrl
//
// [DESCRIPTION]:   Prepare a remote site config
//
// [PARAMETERS]:    $url = ""
//
// [RETURN VALUE]:  string, prepared url
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function PrepareUrl($url = "")
{
   $url = $this->siteConfig['nextUrl'];
   $this->httpReferer = $this->siteConfig['refererUrl'];
   return $url;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Get form name of current page
//
// [PARAMETERS]:    array
//
// [RETURN VALUE]:  the form name|_body_form
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetFormName($formParams)
{

   if(count($formParams) == 1)
      return "_body_form";

   foreach ($formParams as $formName => $value)
      if($formName != '_body_form')
         return $formName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessResponse()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessLastPage
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessLastPage()
{
   print "QUOTE SUCCESS";
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckResponseErrors
//
// [DESCRIPTION]:   Check an html response page for errors. Extract errors.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckResponseErrors()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetResultElements
//
// [DESCRIPTION]:   Get the results from the last page
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetResultElements()
{
   return;
} // end function GetResultElements(&$elementArray)

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ParseXmlResponse
//
// [DESCRIPTION]:   Get the results from the last page only for a broker
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  false | array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ParseXmlResponse()
{
   return;
}
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetXmlTemplate()
   //
   // [DESCRIPTION]:   -
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetXmlTemplate($type='',$d='', $sd='')
   {
      return;
   }


   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareXml
   //
   // [DESCRIPTION]:   Prepare the xml to send to PD
   //
   // [PARAMETERS]:    $SESSION
   //
   // [RETURN VALUE]:  xml data to send
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareXml($SESSION)
   {

   return;

   }// end function PrepareXml($SESSION)
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareQuote
   //
   // [DESCRIPTION]:   Prepare the array of params to get a quote
   //
   // [PARAMETERS]:    $sessionParams ($_SESSION);
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-11-11
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareQuote()
   {
      $this->DoOutbound();

      return;
   }// end function PrepareQuote()

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetUrl
   //
   // [DESCRIPTION]:   Download/Open a remote web page
   //
   // [PARAMETERS]:    $url="", [method="GET"], [$params=""] [$referer=""]
   //
   // [RETURN VALUE]:  downloaded page text if success, "" otherwise
   //
   // [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetUrl($url="", $method="GET", $params="", $referer="")
   {

      $this->htmlContent = "";

      if(empty($url))
      {
         $this->strERR = GetErrorString("INVALID_URL");
         return false;
      }

      $this->proto = "https://";

      // add protocol
      if(! preg_match("/:\/\//", $url))
         $url = $this->proto.$url;

      if(is_array($params))
      {
         $paramString = "";

         foreach($params as $key => $value)
            $paramString .= $key . '=' . $value . '&';

         $paramString = preg_replace("/\&$/","", $paramString);
         $params = $paramString;
      }

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      curl_setopt($ch, CURLOPT_URL,$url);

      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);

      //curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HEADER, 1);

      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; X11; Linux i686) Opera 7.54  [en]');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      $result = curl_exec($ch);

      curl_close ($ch);

      if(empty($result))
      {
         $this->strERR  = "CANNOT_GET_REMOTE_URL";
         $this->strERR .= curl_error($ch)."\n";
         return false;
      }

      preg_match("/^(.*)\n(.*)$/is", $result,$matches);

      $this->httpHeader  = $matches[1];
      $this->htmlContent = $matches[2];

      //print "\n======== result ===========\n".$this->htmlContent."\n=========================\n";die;
      return $result;
   }


} // end of CTaxiMotorcade3Engine class

?>



<?php

/*****************************************************************************/
/*                                                                           */
/*   CTaxiOneAnswerEngine class interface                                    */
/*                                                                           */
/*  (C) 2008 Furtuna Alexandru (alexandru.furtuna@seopa.com)                              */
/*                                                                           */
/*****************************************************************************/
include_once "TAXIEngine.php";
include_once "Url.php";
include_once "Site.php";
include_once "Session.php";
include_once "ArList.php";
include_once "Navigation.php";
include_once "Postcode.php";
include_once "functions.php";
include_once "YourDetailsElements.php";
include_once "EmailContent.php";

//outbound nedded
include_once "LeadsCompanyEmails.php";
include_once "SMTP.php";
include_once "File.php";
include_once "Outbound.php";

error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CTaxiOneAnswerEngine
//
// [DESCRIPTION]:  CTaxiOneAnswerEngine class interface, remote site data exchange support,
//                 PHP version
//
// [FUNCTIONS]:   bool   InitSite()
//                string PrepareUrl($url = "")
//                bool   ProcessResponse()
//                bool   ProcessLastPage()
//                bool   CheckResponseErrors()
//                array  GetResultElements()
//                void   PrepareQuote($SESSION)
//
// [CREATED BY]:   Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CTaxiOneAnswerEngine extends CTAXIEngine
{

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CTaxiOneAnswerEngine
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CTaxiOneAnswerEngine($fileName, $scanningFlag, $debugMode, $testingMode)
{
   CTAXIEngine::CTAXIEngine("taxi-insurance.quotezone.co.uk", $fileName, $scanningFlag, $debugMode, $testingMode);

   $this->SetFailureResponse("There are errors on this page, please correct");

   $this->SetUnableQuoteOnlineServerMessage("Unfortunately we are unable to provide you with an on-line aplication");
   $this->SetOfflineServerMessage("The online aplication system is currently offline");

   $this->SetProtocol("https");

   $this->siteID = "858";


}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InitSite
//
// [DESCRIPTION]:   Initialise a remote site session
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InitSite()
{
   return true;
}

/////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DoOutbound
//
// [DESCRIPTION]:   send outbound
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Moraru Valeriu (null@seopa.com) 2011-04-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DoOutbound()
{
   //error_reporting(E_ALL);

   global $ACRUX_IP_ARRAY;
   global $_YourDetails;

   $objLeadsCompanyEmails = new CLeadsCompanyEmailsModule();

   $outboundObj         = new COutbound();
   $mailObj             = new CSMTP();
   $objFile             = new CFile();

   CTAXIEngine::DoOutbound();

   $logID  = $this->session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];

   if($this->outbounds['MAIL'])
   {
      // get emails to send from db
      $sendEmailsArray = array();
      $sendEmailsArray = $objLeadsCompanyEmails->GetAllEmailsToSendForCompanyBySiteID($this->siteID);

      print_r($sendEmailsArray);

      $message_body = GetEmailTemplate($this->session);

      $subject = "TAXI INSURANCE QUERY DETAILS : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];

      $headers  = 'MIME-Version: 1.0' . "\r\n";
      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
      //$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
      $headers .= "X-Mailer: PHP\r\n"; // mailer
      $headers .= "X-Priority: 1\r\n"; // Urgent message!
      //$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

      $mailFrom    = "taxi@quotezone.co.uk";
      $mailSubject = $subject;
      $mailBody    = $message_body;
      $emailId     = "";
      $mailToAll   = "";

      foreach($sendEmailsArray as $emailType=>$emailsArray)
      {
         $countEmails = count($emailsArray);

         switch($emailType)
         {
            case "COMPANY":
               if( ! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")
               {
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject;

                     $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }
               }
               break;

            case "MARKETING":

               if( ! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")
               {
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject." One Answer";

                     $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }
               }
               break;

            case "TECHNICAL":

               //send emails to TECHNICAL department
               for($k=0;$k<$countEmails;$k++)
               {
                  $mailTo      = $emailsArray[$k];
                  print "send to:".$mailTo."\n";

                  $mailSubject = $subject." One Answer";

                  $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                  $emailId   .= $mailObj->GetMessageID().",";
                  $mailToAll .= $mailTo.",";
               }
               break;
         }// switch
      }// foreach

      // remove last coma
      $emailId   = substr($emailId,0,-1);
      $mailToAll = substr($mailToAll,0,-1);

      $outboundObj->UpdateOutboundField($this->outbounds['MAIL'],'sent_status', 1);
      $outboundObj->UpdateOutboundField($this->outbounds['MAIL'],'extra_params', $emailId);

      $sentContent =  "\nHEADERS: ".$headers."\nFROM:".$mailFrom."\nTO:".$mailToAll."\nSUBJECT: ".$mailSubject."\nBODY: ".$mailBody;

      $this->DoOutboundFiles("mail", $sentContent);

   } //end if MAIL

   if($this->outbounds['URL'])
   {
      if( in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) OR $this->session['_YourDetails_']['email_address'] == "test@quotezone.co.uk" )
         return;

      $outboundObj->UpdateOutboundField($this->outbounds['URL'],'sent_status', 1);

      $loginUsername = '1ansqztx';
      $loginPassword = '1ansqztx';

      $dayPnone      = trim($this->session["_YourDetails_"]["daytime_telephone"]);
      $mobilePnone   = trim($this->session["_YourDetails_"]["mobile_telephone"]);

      $dayPnone      = str_replace(" ","",$dayPnone);
      $mobilePnone   = str_replace(" ","",$mobilePnone);

      if(trim($mobilePnone) != "")
      {
         $destPnone = $mobilePnone;
      }
      elseif(trim($dayPnone) != "")
      {
         $destPnone = $dayPnone;
      }

      $title       = $this->session["_YourDetails_"]["title"];
      $firstName   = $this->session["_YourDetails_"]["first_name"];
      $surname     = $this->session["_YourDetails_"]["surname"];
      $email       = urlencode($this->session["_YourDetails_"]["email_address"]);

      $dobDD       = $this->session["_YourDetails_"]["date_of_birth_dd"];
      $dobMM       = $this->session["_YourDetails_"]["date_of_birth_mm"];
      $dobYYYY     = $this->session["_YourDetails_"]["date_of_birth_yyyy"];
      $dobDDMMYYYY = $dobDD."/".$dobMM."/".$dobYYYY;

      if(trim($this->session["_YourDetails_"]["address_line1"]) == "")
      {
         $homeAddress = urlencode($this->session["_YourDetails_"]["house_number_or_name"]." ".$this->session["_YourDetails_"]["address_line4"]);
      }
      else
      {
         $homeAddress = urlencode($this->session["_YourDetails_"]["address_line1"]." ".$this->session["_YourDetails_"]["address_line2"]." ".$this->session["_YourDetails_"]["address_line3"]." ".$this->session["_YourDetails_"]["address_line4"]);
      }

      $postcode = $this->session["_YourDetails_"]["postcode_prefix"].$this->session["_YourDetails_"]["postcode_number"];

      $vehicleReg       = urlencode($this->session["_YourDetails_"]["vehicle_registration_number"]);
      $vehicleMake      = $this->session["_YourDetails_"]["taxi_make"];
      $vehicleModel     = $this->session["_YourDetails_"]["taxi_model"];
      $estimatedValue   = $this->session["_YourDetails_"]["estimated_value"];
      $annualMileage    = urlencode($_YourDetails["vehicle_mileage"][$this->session["_YourDetails_"]["vehicle_mileage"]]);
      $yearOfManuf      = $this->session["_YourDetails_"]["year_of_manufacture"];
      $vehicleType      = urlencode($_YourDetails["taxi_type"][$this->session["_YourDetails_"]["taxi_type"]]);
      $vehicleUse       = urlencode($_YourDetails["taxi_used_for"][$this->session["_YourDetails_"]["taxi_used_for"]]);
      $taxiCapacity     = urlencode($_YourDetails["taxi_capacity"][$this->session["_YourDetails_"]["taxi_capacity"]]);
      $platingAuthority = $this->session["_YourDetails_"]["plating_authority"];
      $typeOfCover      = urlencode($_YourDetails["type_of_cover"][$this->session["_YourDetails_"]["type_of_cover"]]);
      $noClaimsBonus    = urlencode($_YourDetails["taxi_ncb"][$this->session["_YourDetails_"]["taxi_ncb"]]);
      $periodOfLicence  = urlencode($_YourDetails["period_of_licence"][$this->session["_YourDetails_"]["period_of_licence"]]);
      $taxiBadge        = urlencode($_YourDetails["taxi_badge"][$this->session["_YourDetails_"]["taxi_badge"]]);
      $claims_5_years   = urlencode($_YourDetails["claims_5_years"][$this->session["_YourDetails_"]["claims_5_years"]]);

      $disDD       = $this->session["_YourDetails_"]["date_of_insurance_start_dd"];
      $disMM       = $this->session["_YourDetails_"]["date_of_insurance_start_mm"];
      $disYYYY     = $this->session["_YourDetails_"]["date_of_insurance_start_yyyy"];
      $disDDMMYYYY = $disDD.$disMM.$disYYYY;

      $params = "?&login=$loginUsername&password=$loginPassword&dest=$destPnone&destday=$dayPnone&desteve=$mobilePnone&info_A._Title=$title&info_B._First_Name=$firstName&info_C._Last_Name=$surname&Info_D._Home_Address=$homeAddress&info_E._Home_Postcode=$postcode&info_F._Email=$email&info_G._date_of_Birth=$dobDDMMYYYY&info_H._Reg_Number=$vehicleReg&info_I._Vehicle_Make=$vehicleMake&info_J._Vehicle_Model=$vehicleModel&info_K._Taxi_Value=$estimatedValue&info_L._Taxi_Mileage=$annualMileage&info_M._Year_Of_Manufacture=$yearOfManuf&info_N._Taxi_Type=$vehicleType&info_O._Taxi_use=$vehicleUse&info_P._Maximum_number_of_Passengers=$taxiCapacity&info_Q._Taxi_Plating_Authority=$platingAuthority&info_R._Type_Of_Cover=$typeOfCover&info_S._No_Claims_Bonus=$noClaimsBonus&info_T._Full_UK_Licence=$periodOfLicence&info_U._Taxi_Badge=$taxiBadge&info_V._Claims_Last_5_Years=$claims_5_years&info_W._Insurance_Start_date=$disDDMMYYYY";

      $leadCallUrl = "http://www.leadcall.co.uk/followup/$params";

      $ch = curl_init();

      // set URL and other appropriate options
      curl_setopt($ch, CURLOPT_URL, $leadCallUrl);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      $result = curl_exec($ch);

      curl_close($ch);

      print_r($result);

		
		$printParams = str_replace("?&login=$loginUsername&password=$loginPassword","",$params);
				$printParams = str_replace("&","\r\n&",$printParams);
				
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
            //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
            ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
            $headers .= "X-Mailer: PHP\r\n"; // mailer
            $headers .= "X-Priority: 1\r\n"; // Urgent message!
            ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

            $mailTo      = "eb2-technical@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi One Answer LeadCall-Optilead Request";
            $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
            
            $mailTo      = "leads@seopa.com";
            $mailFrom    = "taxi@quotezone.co.uk";
            $mailSubject = $subject." Taxi One Answer LeadCall-Optilead Request";
            $mailBody    = "\nURL: ".$url."\nPARAMS:".$printParams."\nRESULT:".$result;
            $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);  
		
		
      if(! preg_match("/ERROR/i",$result))
         $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 1);
      else
         $outboundObj->UpdateOutboundField($this->outbounds['URL'],'received_status', 2);

       // Ads sent and received data to filestore
       $this->DoOutboundFiles("url", $leadCallUrl, $result);

   }// end if URL

}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: PrepareUrl
//
// [DESCRIPTION]:   Prepare a remote site config
//
// [PARAMETERS]:    $url = ""
//
// [RETURN VALUE]:  string, prepared url
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function PrepareUrl($url = "")
{
   $url = $this->siteConfig['nextUrl'];
   $this->httpReferer = $this->siteConfig['refererUrl'];
   return $url;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Get form name of current page
//
// [PARAMETERS]:    array
//
// [RETURN VALUE]:  the form name|_body_form
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetFormName($formParams)
{

   if(count($formParams) == 1)
      return "_body_form";

   foreach ($formParams as $formName => $value)
      if($formName != '_body_form')
         return $formName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessResponse()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessLastPage
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessLastPage()
{
   print "QUOTE SUCCESS";
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckResponseErrors
//
// [DESCRIPTION]:   Check an html response page for errors. Extract errors.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckResponseErrors()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetResultElements
//
// [DESCRIPTION]:   Get the results from the last page
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetResultElements()
{
   return;
} // end function GetResultElements(&$elementArray)

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ParseXmlResponse
//
// [DESCRIPTION]:   Get the results from the last page only for a broker
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  false | array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ParseXmlResponse()
{
   return;
}
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetXmlTemplate()
   //
   // [DESCRIPTION]:   -
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetXmlTemplate($type='',$d='', $sd='')
   {
      return;
   }


   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareXml
   //
   // [DESCRIPTION]:   Prepare the xml to send to PD
   //
   // [PARAMETERS]:    $SESSION
   //
   // [RETURN VALUE]:  xml data to send
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareXml($SESSION)
   {

   return;

   }// end function PrepareXml($SESSION)
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareQuote
   //
   // [DESCRIPTION]:   Prepare the array of params to get a quote
   //
   // [PARAMETERS]:    $sessionParams ($_SESSION);
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-11-11
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareQuote()
   {
      $this->DoOutbound();

      return;
   }// end function PrepareQuote()

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetUrl
   //
   // [DESCRIPTION]:   Download/Open a remote web page
   //
   // [PARAMETERS]:    $url="", [method="GET"], [$params=""] [$referer=""]
   //
   // [RETURN VALUE]:  downloaded page text if success, "" otherwise
   //
   // [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetUrl($url="", $method="GET", $params="", $referer="")
   {

      $this->htmlContent = "";

      if(empty($url))
      {
         $this->strERR = GetErrorString("INVALID_URL");
         return false;
      }

      $this->proto = "https://";

      // add protocol
      if(! preg_match("/:\/\//", $url))
         $url = $this->proto.$url;

      if(is_array($params))
      {
         $paramString = "";

         foreach($params as $key => $value)
            $paramString .= $key . '=' . $value . '&';

         $paramString = preg_replace("/\&$/","", $paramString);
         $params = $paramString;
      }

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      curl_setopt($ch, CURLOPT_URL,$url);

      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);

      //curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HEADER, 1);

      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; X11; Linux i686) Opera 7.54  [en]');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      $result = curl_exec($ch);

      curl_close ($ch);

      if(empty($result))
      {
         $this->strERR  = "CANNOT_GET_REMOTE_URL";
         $this->strERR .= curl_error($ch)."\n";
         return false;
      }

      preg_match("/^(.*)\n(.*)$/is", $result,$matches);

      $this->httpHeader  = $matches[1];
      $this->htmlContent = $matches[2];

      //print "\n======== result ===========\n".$this->htmlContent."\n=========================\n";die;
      return $result;
   }


} // end of CTaxiOneAnswerEngine class

?>



<?php

/*****************************************************************************/
/*                                                                           */
/*   CTaxiGapOptInEngine class interface                                     */
/*                                                                           */
/*  (C) 2008 Furtuna Alexandru (alexandru.furtuna@seopa.com)                              */
/*                                                                           */
/*****************************************************************************/
include_once "TAXIEngine.php";
include_once "Url.php";
include_once "Site.php";
include_once "Session.php";
include_once "ArList.php";
include_once "Navigation.php";
include_once "Postcode.php";
include_once "functions.php";
//include_once "SMTP.php";
include_once "YourDetailsElements.php";

error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CTaxiGapOptInEngine
//
// [DESCRIPTION]:  CTaxiGapOptInEngine class interface, remote site data exchange support,
//                 PHP version
//
// [FUNCTIONS]:   bool   InitSite()
//                string PrepareUrl($url = "")
//                bool   ProcessResponse()
//                bool   ProcessLastPage()
//                bool   CheckResponseErrors()
//                array  GetResultElements()
//                void   PrepareQuote($SESSION)
//
// [CREATED BY]:   Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CTaxiGapOptInEngine extends CTAXIEngine
{

   //var $mailObj; //SMTP object

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CTaxiGapOptInEngine
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CTaxiGapOptInEngine($fileName, $scanningFlag, $debugMode, $testingMode)
{
   CTAXIEngine::CTAXIEngine("taxi-insurance-test.quotezone.co.uk/barrygrainger", $fileName, $scanningFlag, $debugMode, $testingMode);

   $this->SetFailureResponse("There are errors on this page, please correct");

   $this->SetUnableQuoteOnlineServerMessage("Unfortunately we are unable to provide you with an on-line aplication");
   $this->SetOfflineServerMessage("The online aplication system is currently offline");

   $this->SetProtocol("https");

   //$this->mailObj = new CSMTP();

   $this->siteID = "671";

}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InitSite
//
// [DESCRIPTION]:   Initialise a remote site session
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InitSite()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: PrepareUrl
//
// [DESCRIPTION]:   Prepare a remote site config
//
// [PARAMETERS]:    $url = ""
//
// [RETURN VALUE]:  string, prepared url
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function PrepareUrl($url = "")
{
   $url = $this->siteConfig['nextUrl'];
   $this->httpReferer = $this->siteConfig['refererUrl'];
   return $url;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Get form name of current page
//
// [PARAMETERS]:    array
//
// [RETURN VALUE]:  the form name|_body_form
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetFormName($formParams)
{

   if(count($formParams) == 1)
      return "_body_form";

   foreach ($formParams as $formName => $value)
      if($formName != '_body_form')
         return $formName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessResponse()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessLastPage
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessLastPage()
{
   print "QUOTE SUCCESS";
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckResponseErrors
//
// [DESCRIPTION]:   Check an html response page for errors. Extract errors.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckResponseErrors()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetResultElements
//
// [DESCRIPTION]:   Get the results from the last page
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetResultElements()
{
   return;
} // end function GetResultElements(&$elementArray)

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ParseXmlResponse
//
// [DESCRIPTION]:   Get the results from the last page only for a broker
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  false | array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ParseXmlResponse()
{
   return;
}
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetXmlTemplate()
   //
   // [DESCRIPTION]:   -
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetXmlTemplate($type='',$d='', $sd='')
   {
      return;
   }


   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareXml
   //
   // [DESCRIPTION]:   Prepare the xml to send to PD
   //
   // [PARAMETERS]:    $SESSION
   //
   // [RETURN VALUE]:  xml data to send
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareXml($SESSION)
   {

   return;

   }// end function PrepareXml($SESSION)
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareQuote
   //
   // [DESCRIPTION]:   Prepare the array of params to get a quote
   //
   // [PARAMETERS]:    $sessionParams ($_SESSION);
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-11-11
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareQuote($SESSION)
   {

   global $_YourDetails;

   $taxiUsedFor    = $SESSION['_YourDetails_']['taxi_used_for'];
   $taxiType       = $SESSION['_YourDetails_']['taxi_type'];
   $taxiMake       = $SESSION['_YourDetails_']['taxi_make'];
   $vehicleAge     = date("Y") - $SESSION['_YourDetails_']['year_of_manufacture'];
   if($vehicleAge == 0)
      $vehicleAge = "Less than one year";

   $typeOfVehicle  = "Taxi";

   $taxiTypeOfCover    = $SESSION['_YourDetails_']['type_of_cover'];
   $taxiVehicleMileage = $SESSION['_YourDetails_']['vehicle_mileage'];
   $taxiModel      = $SESSION['_YourDetails_']['taxi_model'];
   $estimatedValue = $SESSION['_YourDetails_']['estimated_value'];
   $taxiCapacity   = $SESSION['_YourDetails_']['taxi_capacity'];
   $taxiNcb        = $SESSION['_YourDetails_']['taxi_ncb'];
   $platingAuth    = $SESSION['_YourDetails_']['plating_authority'];
   $gapInsurance       = $SESSION['_YourDetails_']['gap_insurance'];
   $breakdownCover     = $SESSION['_YourDetails_']['breakdown_cover'];

   $bestTime          = $SESSION['_YourDetails_']['best_time_call'];
   $bestDay          = '';
   //$bestDayToCallValue  = $SESSION['_YourDetails_']['best_day_call'];
   //$bestTimeToCallValue = $SESSION['_YourDetails_']['best_time_call'];
   $dateAndTime         = date("Y/m/d H:i:s");

   $title        = $SESSION['_YourDetails_']['title'];
   $firstName    = $SESSION['_YourDetails_']['first_name'];
   $surname      = $SESSION['_YourDetails_']['surname'];
   $birthDate    = $SESSION['_YourDetails_']['date_of_birth'];
   $dayTimePhone = $SESSION['_YourDetails_']['daytime_telephone'];
   $mobilePhone  = $SESSION['_YourDetails_']['mobile_telephone'];
   $postcode     = $SESSION['_YourDetails_']['postcode'];
   $houseNumber  = $SESSION['_YourDetails_']['house_number_or_name'];
   $addr1        = $SESSION['_YourDetails_']['address_line1'];
   $addr2        = $SESSION['_YourDetails_']['address_line2'];
   $addr3        = $SESSION['_YourDetails_']['address_line3'];
   $addr4        = $SESSION['_YourDetails_']['address_line4'];
   $email        = $SESSION['_YourDetails_']['email_address'];

   $dateOfInsuranceStart = $SESSION['_YourDetails_']['date_of_insurance_start_dd']."/".$SESSION['_YourDetails_']['date_of_insurance_start_mm']."/".$SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $message_body .= "<table width='500' cellspacing='2' cellpadding='2' border='0' align='left'>";
   $message_body .= "<tr>
                        <td style=\"font:bold 18px Calibri;color:#9C0D0D;\" align=\"left\">QUOTEZONE</td>
                        <td style=\"font:bold 12px Calibri;color:black;\" align=\"right\">$dateAndTime</td>
                     </tr>";
   $message_body .= "<tr>
                        <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">GAP OPT IN INSURANCE QUERY DETAILS</td>
                     </tr>";

   $oddColor  = "#EDF4FF";
   $evenColor = "#F7FAFF";
   $color = 1;
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                       <td width='200'>VEHICLE MAKE</td>
                       <td width='300'>".$taxiMake."</td>
                    </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                       <td width='200'>VEHICLE MODEL</td>
                       <td width='300'>".$taxiModel."</td>
                    </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>VEHICLE AGE</td>
                        <td width='300'>".$vehicleAge."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>TYPE OF VEHICLE</td>
                        <td width='300'>".$typeOfVehicle."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>VEHICLE USE</td>
                        <td width='300'>".$_YourDetails["taxi_used_for"][$taxiUsedFor]."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>ESTIMATED VALUE</td>
                        <td width='300'>".$estimatedValue."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;

   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>CURRENT MILEAGE</td>
                        <td width='300'>".$_YourDetails['vehicle_mileage'][$taxiVehicleMileage]."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;

   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>CURRENT INSURANCE</td>
                        <td width='300'>".$_YourDetails["type_of_cover"][$taxiTypeOfCover]."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;

   $message_body .= "<tr height=\"10\">
                        <td colspan=\"2\">
                           &nbsp;
                        </td>
                     </tr>";

   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>FIRST NAME/SURNAME</td>
                        <td width='300'>".$title." ".$firstName." ".$surname."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>TELEPHONE</td>
                        <td width='300'>".$dayTimePhone."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>MOBILE PHONE</td>
                        <td width='300'>".$mobilePhone."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>BEST TIME TO CALL</td>
                        <td width='300'>".$_YourDetails["best_time_call"][$bestTime]."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>EMAIL ADDRESS</td>
                        <td width='300'>".$email."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>POSTCODE</td>
                        <td width='300'>".$postcode."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>HOUSE NUMBER/NAME</td>
                        <td width='300'>".$houseNumber."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>ADDRESS LINE 1</td>
                        <td width='300'>".$addr1."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>STREET NAME</td>
                        <td width='300'>".$addr2."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>TOWN/CITY</td>
                        <td width='300'>".$addr3."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>COUNTY</td>
                        <td width='300'>".$addr4."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>DATE OF BIRTH</td>
                        <td width='300'>".$birthDate."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>DATE OF INSURANCE START</td>
                        <td width='300'>".$startDate."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>QUOTE REFERENCE</td>
                        <td width='300'>".$SESSION['_QZ_QUOTE_DETAILS_']['quote_reference']."</td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr height=\"10\">
                        <td colspan=\"2\">
                           &nbsp;
                        </td>
                     </tr>";
   $rowColor = ($color++%2) ? $oddColor : $evenColor ;
   $message_body .= "<tr>
                        <td colspan=\"2\" align=\"left\" style=\"background-color:#EDF4FF;font:10px Calibri;color:#333333;\">
                           This user has provided their contact information for the sole purpose of providing a taxi quotation.
                           No further contact can be made with the user by you or third parties for other sales or marketing purposes.<br><br>

                           Quotezone is not responsible for verifying the accuracy and completeness of any information provided by a user as
                           part of this quotation request. Please verify this information with the user.<br><br>

                           Seopa Ltd T/A Quotezone.co.uk and CompareNI.com.  Registered office: Seopa Ltd, Blackstaff Studios, Floor 2, 8-10 Amelia
                           Street, Belfast, Co. Antrim, BT2 7GS. Registered in Northern Ireland NI46322. Seopa Ltd is authorised and regulated by the
                           Financial Conduct Authority (FCA). Our register number is 313860. Our permitted business is insurance mediation.
                        </td>
                     </tr>";
   $message_body .= "</table>";

   $subject = "Gap Opt In Query : ".$SESSION['_YourDetails_']['title']." ".$SESSION['_YourDetails_']['first_name']." ".$SESSION['_YourDetails_']['surname'];

   $headers  = 'MIME-Version: 1.0' . "\r\n";
   $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
   //$headers .= "From: <gapoptin@quotezone.co.uk>\r\n";
   //$headers .= "X-Sender: <gapoptin@quotezone.co.uk>\r\n";
   $headers .= "X-Mailer: PHP\r\n"; // mailer
   $headers .= "X-Priority: 1\r\n"; // Urgent message!
   //$headers .= "Return-Path: <gapoptin@quotezone.co.uk>\r\n";

   //george
   $mailTo      = "george.vulcan@seopa.com";
   $mailFrom    = "gapoptin@quotezone.co.uk";
   $mailSubject = $subject." Fake";
   $mailBody    = $message_body;
   mail($mailTo,$mailSubject,$mailBody,$headers);

  //BarryGrainger
  if($SESSION['USER_IP'] != "86.125.114.56" AND $SESSION['_YourDetails_']['email_address'] != "test@quotezone.co.uk")
  {
      //$mailTo      = "sales@barrygraingerinsurance.co.uk";
      //$mailFrom    = "taxi@quotezone.co.uk";
      //$mailSubject = $subject ." - " .$SESSION['USER_IP'];
      //$mailBody    = $message_body;
      //mail($mailTo,$mailSubject,$mailBody,$headers);

      //lee
      $mailTo      = "lee@seopa.com";
      $mailFrom    = "gapoptin@quotezone.co.uk";
      $mailSubject = $subject." Fake";
      $mailBody    = $message_body;
      mail($mailTo,$mailSubject,$mailBody,$headers);
  }

   return;
   }// end function PrepareQuote($SESSION)


   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetUrl
   //
   // [DESCRIPTION]:   Download/Open a remote web page
   //
   // [PARAMETERS]:    $url="", [method="GET"], [$params=""] [$referer=""]
   //
   // [RETURN VALUE]:  downloaded page text if success, "" otherwise
   //
   // [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetUrl($url="", $method="GET", $params="", $referer="")
   {

      $this->htmlContent = "";

      if(empty($url))
      {
         $this->strERR = GetErrorString("INVALID_URL");
         return false;
      }

      $this->proto = "https://";

      // add protocol
      if(! preg_match("/:\/\//", $url))
         $url = $this->proto.$url;

      if(is_array($params))
      {
         $paramString = "";

         foreach($params as $key => $value)
            $paramString .= $key . '=' . $value . '&';

         $paramString = preg_replace("/\&$/","", $paramString);
         $params = $paramString;
      }

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      curl_setopt($ch, CURLOPT_URL,$url);

      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);

      //curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HEADER, 1);

      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; X11; Linux i686) Opera 7.54  [en]');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      $result = curl_exec($ch);

      curl_close ($ch);

      if(empty($result))
      {
         $this->strERR  = "CANNOT_GET_REMOTE_URL";
         $this->strERR .= curl_error($ch)."\n";
         return false;
      }

      preg_match("/^(.*)\n(.*)$/is", $result,$matches);

      $this->httpHeader  = $matches[1];
      $this->htmlContent = $matches[2];

      //print "\n======== result ===========\n".$this->htmlContent."\n=========================\n";die;
      return $result;
   }


} // end of CTaxiGapOptInEngine class

?>



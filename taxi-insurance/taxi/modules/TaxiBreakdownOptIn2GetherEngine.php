<?php

/*****************************************************************************/
/*                                                                           */
/*   CTaxiBreakdownOptIn2GetherEngine class interface                        */
/*                                                                           */
/*  (C) 2008 Furtuna Alexandru (alexandru.furtuna@seopa.com)                              */
/*                                                                           */
/*****************************************************************************/

include_once "TAXIEngine.php";
include_once "Url.php";
include_once "Site.php";
include_once "Session.php";
include_once "ArList.php";
include_once "Navigation.php";
include_once "Postcode.php";
include_once "functions.php";
include_once "YourDetailsElements.php";
include_once "EmailContent.php";

//outbound nedded
include_once "LeadsCompanyEmails.php";
include_once "SMTP.php";
include_once "File.php";
include_once "Outbound.php";

error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CTaxiBreakdownOptIn2GetherEngine
//
// [DESCRIPTION]:  CTaxiBreakdownOptIn2GetherEngine class interface, remote site data exchange support,
//                 PHP version
//
// [FUNCTIONS]:   bool   InitSite()
//                string PrepareUrl($url = "")
//                bool   ProcessResponse()
//                bool   ProcessLastPage()
//                bool   CheckResponseErrors()
//                array  GetResultElements()
//                void   PrepareQuote($SESSION)
//
// [CREATED BY]:   Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CTaxiBreakdownOptIn2GetherEngine extends CTAXIEngine
{

   //var $mailObj; //SMTP object

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CTaxiBreakdownOptIn2GetherEngine
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CTaxiBreakdownOptIn2GetherEngine($fileName, $scanningFlag, $debugMode, $testingMode)
{
   CTAXIEngine::CTAXIEngine("taxi-insurance.quotezone.co.uk", $fileName, $scanningFlag, $debugMode, $testingMode);

   $this->SetFailureResponse("There are errors on this page, please correct");

   $this->SetUnableQuoteOnlineServerMessage("Unfortunately we are unable to provide you with an on-line aplication");
   $this->SetOfflineServerMessage("The online aplication system is currently offline");

   $this->SetProtocol("https");

   //$this->mailObj = new CSMTP();

   $this->siteID = "673";

}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InitSite
//
// [DESCRIPTION]:   Initialise a remote site session
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InitSite()
{
   return true;
}

/////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DoOutbound
//
// [DESCRIPTION]:   send outbound
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Moraru Valeriu (null@seopa.com) 2011-04-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DoOutbound()
{
   //error_reporting(E_ALL);

   global $ACRUX_IP_ARRAY;

   $objLeadsCompanyEmails = new CLeadsCompanyEmailsModule();

   $outboundObj         = new COutbound();
   $mailObj             = new CSMTP();
   $objFile             = new CFile();

   CTAXIEngine::DoOutbound();

   $logID  = $this->session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];

   if($this->outbounds['MAIL'])
   {
      // get emails to send from db
      $sendEmailsArray = array();
      $sendEmailsArray = $objLeadsCompanyEmails->GetAllEmailsToSendForCompanyBySiteID($this->siteID);

      print_r($sendEmailsArray);

      //$message_body = GetEmailTemplate($this->session);

      // This has custom email content
      global $_YourDetails;

      $taxiUsedFor    = $this->session['_YourDetails_']['taxi_used_for'];
      $taxiType       = $this->session['_YourDetails_']['taxi_type'];
      $taxiMake       = $this->session['_YourDetails_']['taxi_make'];
      $vehicleYear    = $this->session['_YourDetails_']['year_of_manufacture'];

      $typeOfVehicle  = "Taxi";

      $taxiTypeOfCover    = $this->session['_YourDetails_']['type_of_cover'];
      $taxiVehicleMileage = $this->session['_YourDetails_']['vehicle_mileage'];
      $taxiModel      = $this->session['_YourDetails_']['taxi_model'];
      $estimatedValue = $this->session['_YourDetails_']['estimated_value'];
      $taxiCapacity   = $this->session['_YourDetails_']['taxi_capacity'];
      $taxiNcb        = $this->session['_YourDetails_']['taxi_ncb'];
      $platingAuth    = $this->session['_YourDetails_']['plating_authority'];
      $gapInsurance       = $this->session['_YourDetails_']['gap_insurance'];
      $breakdownCover     = $this->session['_YourDetails_']['breakdown_cover'];

      $bestTime          = $this->session['_YourDetails_']['best_time_call'];
      $bestDay          = '';
      //$bestDayToCallValue  = $this->session['_YourDetails_']['best_day_call'];
      //$bestTimeToCallValue = $this->session['_YourDetails_']['best_time_call'];
      $dateAndTime         = date("Y/m/d H:i:s");

      $title        = $this->session['_YourDetails_']['title'];
      $firstName    = $this->session['_YourDetails_']['first_name'];
      $surname      = $this->session['_YourDetails_']['surname'];
      $birthDate    = $this->session['_YourDetails_']['date_of_birth'];
      $dayTimePhone = $this->session['_YourDetails_']['daytime_telephone'];
      $mobilePhone  = $this->session['_YourDetails_']['mobile_telephone'];
      $postcode     = $this->session['_YourDetails_']['postcode'];
      $houseNumber  = $this->session['_YourDetails_']['house_number_or_name'];
      $addr1        = $this->session['_YourDetails_']['address_line1'];
      $addr2        = $this->session['_YourDetails_']['address_line2'];
      $addr3        = $this->session['_YourDetails_']['address_line3'];
      $addr4        = $this->session['_YourDetails_']['address_line4'];
      $email        = $this->session['_YourDetails_']['email_address'];

      $dateOfInsuranceStart = $this->session['_YourDetails_']['date_of_insurance_start_dd']."/".$this->session['_YourDetails_']['date_of_insurance_start_mm']."/".$this->session['_YourDetails_']['date_of_insurance_start_yyyy'];

      $message_body .= "<table width='500' cellspacing='2' cellpadding='2' border='0' align='left'>";
      $message_body .= "<tr>
                           <td style=\"font:bold 18px Calibri;color:#9C0D0D;\" align=\"left\">QUOTEZONE</td>
                           <td style=\"font:bold 12px Calibri;color:black;\" align=\"right\">$dateAndTime</td>
                        </tr>";
      $message_body .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">BREAKDOWN OPT IN INSURANCE QUERY DETAILS</td>
                        </tr>";

      $oddColor  = "#EDF4FF";
      $evenColor = "#F7FAFF";
      $color = 1;
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>VEHICLE MAKE</td>
                        <td width='300'>".$taxiMake."</td>
                     </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                        <td width='200'>VEHICLE MODEL</td>
                        <td width='300'>".$taxiModel."</td>
                     </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>VEHICLE YEAR</td>
                           <td width='300'>".$vehicleYear."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>TYPE OF VEHICLE</td>
                           <td width='300'>".$typeOfVehicle."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>VEHICLE USE</td>
                           <td width='300'>".$_YourDetails["taxi_used_for"][$taxiUsedFor]."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>ESTIMATED VALUE</td>
                           <td width='300'>".$estimatedValue."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>CURRENT MILEAGE</td>
                           <td width='300'>".$_YourDetails['vehicle_mileage'][$taxiVehicleMileage]."</td>
                        </tr>";

      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr height=\"10\">
                           <td colspan=\"2\">
                              &nbsp;
                           </td>
                        </tr>";

      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>FIRST NAME/SURNAME</td>
                           <td width='300'>".$title." ".$firstName." ".$surname."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>TELEPHONE</td>
                           <td width='300'>".$dayTimePhone."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>MOBILE PHONE</td>
                           <td width='300'>".$mobilePhone."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>BEST TIME TO CALL</td>
                           <td width='300'>".$_YourDetails["best_time_call"][$bestTime]."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>EMAIL ADDRESS</td>
                           <td width='300'>".$email."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>POSTCODE</td>
                           <td width='300'>".$postcode."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>HOUSE NUMBER/NAME</td>
                           <td width='300'>".$houseNumber."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>ADDRESS LINE 1</td>
                           <td width='300'>".$addr1."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>STREET NAME</td>
                           <td width='300'>".$addr2."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>TOWN/CITY</td>
                           <td width='300'>".$addr3."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>COUNTY</td>
                           <td width='300'>".$addr4."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>DATE OF BIRTH</td>
                           <td width='300'>".$birthDate."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>DATE OF INSURANCE START</td>
                           <td width='300'>".$dateOfInsuranceStart."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr style=\"font:bold 12px Calibri;background-color:$rowColor;\">
                           <td width='200'>QUOTE REFERENCE</td>
                           <td width='300'>".$this->session['_QZ_QUOTE_DETAILS_']['quote_reference']."</td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr height=\"10\">
                           <td colspan=\"2\">
                              &nbsp;
                           </td>
                        </tr>";
      $rowColor = ($color++%2) ? $oddColor : $evenColor ;
      $message_body .= "<tr>
                           <td colspan=\"2\" align=\"left\" style=\"background-color:#EDF4FF;font:10px Calibri;color:#333333;\">

                              This user has provided their contact information for the sole purpose of providing a taxi quotation.
                              No further contact can be made with the user by you or third parties for other sales or marketing purposes.<br><br>

                              Quotezone is not responsible for verifying the accuracy and completeness of any information provided by a user as
                              part of this quotation request. Please verify this information with the user.<br><br>

                              Seopa Ltd T/A Quotezone.co.uk and CompareNI.com.  Registered office: Seopa Ltd, Blackstaff Studios, Floor 2, 8-10 Amelia
                              Street, Belfast, Co. Antrim, BT2 7GS. Registered in Northern Ireland NI46322. Seopa Ltd is authorised and regulated by the
                              Financial Conduct Authority (FCA). Our register number is 313860. Our permitted business is insurance mediation.

                           </td>
                        </tr>";
      $message_body .= "</table>";

      $subject = "Breakdown Opt In Query : ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];

      $headers  = 'MIME-Version: 1.0' . "\r\n";
      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      //$headers .= "From: <breakdownoptin@quotezone.co.uk>\r\n";
      //$headers .= "X-Sender: <breakdownoptin@quotezone.co.uk>\r\n";
      $headers .= "X-Mailer: PHP\r\n"; // mailer
      $headers .= "X-Priority: 1\r\n"; // Urgent message!
      //$headers .= "Return-Path: <breakdownoptin@quotezone.co.uk>\r\n";

      $mailFrom    = "breakdownoptin@quotezone.co.uk";
      $mailSubject = $subject;
      $mailBody    = $message_body;
      $emailId     = "";
      $mailToAll   = "";

      foreach($sendEmailsArray as $emailType=>$emailsArray)
      {
         $countEmails = count($emailsArray);

         switch($emailType)
         {
            case "COMPANY":
               if( ! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")
               {
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject;

                     $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }
               }
               break;

            case "MARKETING":

               if( ! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")
               {
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject." 2Gether";

                     $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }
               }
               break;

            case "TECHNICAL":

               //send emails to TECHNICAL department
               for($k=0;$k<$countEmails;$k++)
               {
                  $mailTo      = $emailsArray[$k];
                  print "send to:".$mailTo."\n";

                  $mailSubject = $subject." 2Gether";

                  $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                  $emailId   .= $mailObj->GetMessageID().",";
                  $mailToAll .= $mailTo.",";
               }
               break;
         }// switch
      }// foreach

      // remove last coma
      $emailId   = substr($emailId,0,-1);
      $mailToAll = substr($mailToAll,0,-1);

      $outboundObj->UpdateOutboundField($this->outbounds['MAIL'],'sent_status', 1);
      $outboundObj->UpdateOutboundField($this->outbounds['MAIL'],'extra_params', $emailId);

      $sentContent =  "\nHEADERS: ".$headers."\nFROM:".$mailFrom."\nTO:".$mailToAll."\nSUBJECT: ".$mailSubject."\nBODY: ".$mailBody;

      $this->DoOutboundFiles("mail", $sentContent);

   } //end if MAIL

   if($this->outbounds['XML'])
   {
      if( in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) OR $this->session['_YourDetails_']['email_address'] == "test@quotezone.co.uk")
         return;

      $taxiUsedFor        = $this->session['_YourDetails_']['taxi_used_for'];
      $taxiType           = $this->session['_YourDetails_']['taxi_type'];
      $taxiMake           = $this->session['_YourDetails_']['taxi_make'];
      $vehicleYear        = $this->session['_YourDetails_']['year_of_manufacture'];
      $typeOfVehicle      = "Taxi";
      $taxiTypeOfCover    = $this->session['_YourDetails_']['type_of_cover'];
      $taxiVehicleMileage = $this->session['_YourDetails_']['vehicle_mileage'];
      $taxiModel          = $this->session['_YourDetails_']['taxi_model'];
      $estimatedValue     = $this->session['_YourDetails_']['estimated_value'];
      $taxiCapacity       = $this->session['_YourDetails_']['taxi_capacity'];
      $taxiNcb            = $this->session['_YourDetails_']['taxi_ncb'];
      $platingAuth        = $this->session['_YourDetails_']['plating_authority'];
      $gapInsurance       = $this->session['_YourDetails_']['gap_insurance'];
      $breakdownCover     = $this->session['_YourDetails_']['breakdown_cover'];
      $bestTime           = $this->session['_YourDetails_']['best_time_call'];
      $bestDay            = $this->session['_YourDetails_']['best_day_call'];
      $dateAndTime        = date("Y/m/d H:i:s");

      $title              = $this->session['_YourDetails_']['title'];
      $firstName          = $this->session['_YourDetails_']['first_name'];
      $surname            = $this->session['_YourDetails_']['surname'];
      $birthDate          = $this->session['_YourDetails_']['date_of_birth'];
      $dayTimePhone       = $this->session['_YourDetails_']['daytime_telephone'];
      $mobilePhone        = $this->session['_YourDetails_']['mobile_telephone'];
      $postcode           = $this->session['_YourDetails_']['postcode'];
      $houseNumber        = $this->session['_YourDetails_']['house_number_or_name'];
      $addr1              = $this->session['_YourDetails_']['address_line1'];
      $addr2              = $this->session['_YourDetails_']['address_line2'];
      $addr3              = $this->session['_YourDetails_']['address_line3'];
      $addr4              = $this->session['_YourDetails_']['address_line4'];
      $email              = $this->session['_YourDetails_']['email_address'];

      $title              = $this->session['_YourDetails_']['title'];
      $firstName          = $this->session['_YourDetails_']['first_name'];
      $lastName           = $this->session['_YourDetails_']['surname'];
      $daytime_telephone  = $this->session['_YourDetails_']['daytime_telephone'];
      $mobile_telephone   = $this->session['_YourDetails_']['mobile_telephone'];

      $dest               = $daytime_telephone?$daytime_telephone:$mobile_telephone;

      $make               = $taxiMake;
      $model              = $taxiModel;
      $systemType         = "Taxi";

      $params  = "?login=qztx2geth";
      $params .= "&password=qztx2geth";
      $params .= "&destday=".urlencode($daytime_telephone);
      $params .= "&desteve=".urlencode($mobile_telephone);
      $params .= "&info_a._title=".urlencode($title);
      $params .= "&info_b._forename=".urlencode($firstName);
      $params .= "&info_c._middle_name=";
      $params .= "&info_d._surname=".urlencode($lastName);
      $params .= "&info_e._Date_Of_Birth=".urlencode($SESSION['_YourDetails_']['date_of_birth']);
      $params .= "&info_f._Taxi_registration_number=".urlencode($SESSION['_YourDetails_']['vehicle_registration_number']);
      $params .= "&info_g._Taxi_Make=".urlencode($make);
      $params .= "&info_h._Taxi_Model=".urlencode($model);
      $params .= "&info_i._Estimated_Taxi_Value=".urlencode($estimatedValue);
      $params .= "&info_j._Year_Of_Manufacture=".urlencode($vehicleYear);
      $params .= "&info_l._Vehicle_Mileage=".urlencode($taxiVehicleMileage);
      $params .= "&info_m._Taxi_Type=".urlencode($taxiType);
      $params .= "&info_n._Taxi_Use=".urlencode($taxiUsedFor);
      $params .= "&info_o._Max_Number_of_Passengers=".urlencode($taxiCapacity);
      $params .= "&info_p._Taxi_Plating_Authority=".urlencode($platingAuth);
      $params .= "&info_q._Type_of_Cover=".urlencode($taxiTypeOfCover);
      $params .= "&info_r._Taxi_No_Claims_Bonus=".urlencode($taxiNcb);
      $params .= "&info_s._Full_UK_Licence=".urlencode($_YourDetails["period_of_licence"][$SESSION['_YourDetails_']['period_of_licence']]);
      $params .= "&info_t._Taxi_badge=".urlencode($_YourDetails["taxi_badge"][$SESSION['_YourDetails_']['taxi_badge']]);
      $params .= "&info_u._Claims_Last_5_Years=".urlencode($SESSION['_YourDetails_']['claims_5_years']);
      $params .= "&info_v._Postcode=".urlencode($SESSION['_YourDetails_']['postcode']);
      $params .= "&info_w._Insurance_Start_Date=".urlencode($dateOfInsuranceStart);
      $params .= "&info_x._Email_Address=".urlencode($SESSION['_YourDetails_']['email_address']);
      $params .= "&info_z._House_Name_Or_Number=".urlencode($SESSION['_YourDetails_']['house_number_or_name']);

      //$params = "?&login=qz2geth&password=qz2geth&dest=$dest&tag1=$title&tag2=$firstName&tag3=$lastName&tag4=$make&tag5=$model&tag6=$systemType";

      $ch = curl_init();

      $url    = "http://www.leadcall.co.uk/followup/";

      $leadcall = $url.$params;

      print "\n$leadcall\n\n";

      //Get
      curl_setopt($ch, CURLOPT_URL, $leadcall);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $result = curl_exec($ch);
      curl_close($ch);

      print "\n========== result ===============\n";
      print $result;
      print "\n========== result ===============\n";

      $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

      if( ! preg_match("/ERROR/i",$result))
         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
      else
         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

      $sentContent =  "\nURL: ".$url."\nPARAMS:".$params;

      $this->DoOutboundFiles("xml", $sentContent,$result);

   } //end if XML


}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: PrepareUrl
//
// [DESCRIPTION]:   Prepare a remote site config
//
// [PARAMETERS]:    $url = ""
//
// [RETURN VALUE]:  string, prepared url
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function PrepareUrl($url = "")
{
   $url = $this->siteConfig['nextUrl'];
   $this->httpReferer = $this->siteConfig['refererUrl'];
   return $url;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Get form name of current page
//
// [PARAMETERS]:    array
//
// [RETURN VALUE]:  the form name|_body_form
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetFormName($formParams)
{

   if(count($formParams) == 1)
      return "_body_form";

   foreach ($formParams as $formName => $value)
      if($formName != '_body_form')
         return $formName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessResponse()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessLastPage
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessLastPage()
{
   print "QUOTE SUCCESS";
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckResponseErrors
//
// [DESCRIPTION]:   Check an html response page for errors. Extract errors.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckResponseErrors()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetResultElements
//
// [DESCRIPTION]:   Get the results from the last page
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetResultElements()
{
   return;
} // end function GetResultElements(&$elementArray)

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ParseXmlResponse
//
// [DESCRIPTION]:   Get the results from the last page only for a broker
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  false | array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ParseXmlResponse()
{
   return;
}
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetXmlTemplate()
   //
   // [DESCRIPTION]:   -
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetXmlTemplate($type='',$d='', $sd='')
   {
      return;
   }


   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareXml
   //
   // [DESCRIPTION]:   Prepare the xml to send to PD
   //
   // [PARAMETERS]:    $SESSION
   //
   // [RETURN VALUE]:  xml data to send
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-11-20
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareXml($SESSION)
   {

   return;

   }// end function PrepareXml($SESSION)
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareQuote
   //
   // [DESCRIPTION]:   Prepare the array of params to get a quote
   //
   // [PARAMETERS]:    $sessionParams ($_SESSION);
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-11-11
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareQuote()
   {
      $this->DoOutbound();

      return;
   }// end function PrepareQuote()

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetUrl
   //
   // [DESCRIPTION]:   Download/Open a remote web page
   //
   // [PARAMETERS]:    $url="", [method="GET"], [$params=""] [$referer=""]
   //
   // [RETURN VALUE]:  downloaded page text if success, "" otherwise
   //
   // [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetUrl($url="", $method="GET", $params="", $referer="")
   {

      $this->htmlContent = "";

      if(empty($url))
      {
         $this->strERR = GetErrorString("INVALID_URL");
         return false;
      }

      $this->proto = "https://";

      // add protocol
      if(! preg_match("/:\/\//", $url))
         $url = $this->proto.$url;

      if(is_array($params))
      {
         $paramString = "";

         foreach($params as $key => $value)
            $paramString .= $key . '=' . $value . '&';

         $paramString = preg_replace("/\&$/","", $paramString);
         $params = $paramString;
      }

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      curl_setopt($ch, CURLOPT_URL,$url);

      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);

      //curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HEADER, 1);

      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; X11; Linux i686) Opera 7.54  [en]');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      $result = curl_exec($ch);

      curl_close ($ch);

      if(empty($result))
      {
         $this->strERR  = "CANNOT_GET_REMOTE_URL";
         $this->strERR .= curl_error($ch)."\n";
         return false;
      }

      preg_match("/^(.*)\n(.*)$/is", $result,$matches);

      $this->httpHeader  = $matches[1];
      $this->htmlContent = $matches[2];

      //print "\n======== result ===========\n".$this->htmlContent."\n=========================\n";die;
      return $result;
   }


} // end of CTaxiBreakdownOptIn2GetherEngine class

?>



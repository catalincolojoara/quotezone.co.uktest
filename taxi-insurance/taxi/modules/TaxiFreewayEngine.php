<?php

/*****************************************************************************/
/*                                                                           */
/*   CTaxiAFreewayEngine class interface                                     */
/*                                                                           */
/*  (C) 2008 Furtuna Alexandru (alexandru.furtuna@seopa.com)                              */
/*                                                                           */
/*****************************************************************************/
include_once "TAXIEngine.php";
include_once "Url.php";
include_once "Site.php";
include_once "Session.php";
include_once "ArList.php";
include_once "Navigation.php";
include_once "Postcode.php";
include_once "functions.php";
include_once "YourDetailsElements.php";
include_once "EmailContent.php";

//outbound nedded
include_once "LeadsCompanyEmails.php";
include_once "SMTP.php";
include_once "File.php";
include_once "Outbound.php";

include_once "Vehicle.php";

error_reporting(0);

//////////////////////////////////////////////////////////////////////////////PB
//
// [CLASS NAME]:   CTaxiAFreewayEngine
//
// [DESCRIPTION]:  CTaxiAFreewayEngine class interface, remote site data exchange support,
//                 PHP version
//
// [FUNCTIONS]:   bool   InitSite()
//                string PrepareUrl($url = "")
//                bool   ProcessResponse()
//                bool   ProcessLastPage()
//                bool   CheckResponseErrors()
//                array  GetResultElements()
//                void   PrepareQuote($SESSION)
//
// [CREATED BY]:   Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

class CTaxiAFreewayEngine extends CTAXIEngine
{

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CTaxiAFreewayEngine
//
// [DESCRIPTION]:   Default class constructor. Initialization goes here.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CTaxiAFreewayEngine($fileName, $scanningFlag, $debugMode, $testingMode)
{
   CTAXIEngine::CTAXIEngine("taxi-insurance.quotezone.co.uk", $fileName, $scanningFlag, $debugMode, $testingMode);

   $this->SetFailureResponse("There are errors on this page, please correct");

   $this->SetUnableQuoteOnlineServerMessage("Unfortunately we are unable to provide you with an on-line aplication");
   $this->SetOfflineServerMessage("The online aplication system is currently offline");

   $this->SetProtocol("https");

   $this->siteID = "1152";


}
//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: InitSite
//
// [DESCRIPTION]:   Initialise a remote site session
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function InitSite()
{
   return true;
}

/////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: DoOutbound
//
// [DESCRIPTION]:   send outbound
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Moraru Valeriu (null@seopa.com) 2011-04-20
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function DoOutbound()
{
   //error_reporting(E_ALL);

   global $ACRUX_IP_ARRAY;
   global $_YourDetails;

   $objLeadsCompanyEmails = new CLeadsCompanyEmailsModule();

   $outboundObj         = new COutbound();
   $mailObj             = new CSMTP();
   $objFile             = new CFile();

   CTAXIEngine::DoOutbound();

   $logID  = $this->session["_QZ_QUOTE_DETAILS_"]["qz_log_id"];

   if($this->outbounds['MAIL'])
   {
      // get emails to send from db
      $sendEmailsArray = array();
      $sendEmailsArray = $objLeadsCompanyEmails->GetAllEmailsToSendForCompanyBySiteID($this->siteID);

      print_r($sendEmailsArray);

      $message_body = GetEmailTemplate($this->session);

      $subject = "Taxi query (Clean Risk): ".$this->session['_YourDetails_']['title']." ".$this->session['_YourDetails_']['first_name']." ".$this->session['_YourDetails_']['surname'];

      $headers  = 'MIME-Version: 1.0' . "\r\n";
      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
      ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
      $headers .= "X-Mailer: PHP\r\n"; // mailer
      $headers .= "X-Priority: 1\r\n"; // Urgent message!
      ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

      $mailFrom    = "taxi@quotezone.co.uk";
      $mailSubject = $subject;
      $mailBody    = $message_body;
      $emailId     = "";
      $mailToAll   = "";

      foreach($sendEmailsArray as $emailType=>$emailsArray)
      {
         $countEmails = count($emailsArray);

         switch($emailType)
         {
            case "COMPANY":
               if( ! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")
               {
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject;

                     $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }
               }
               break;

            case "MARKETING":

               if( ! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")
               {
                  for($k=0;$k<$countEmails;$k++)
                  {
                     $mailTo = $emailsArray[$k];
                     print "send to:".$mailTo."\n";

                     $mailSubject = $subject." Freeway Insurance";

                     $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                     $emailId   .= $mailObj->GetMessageID().",";
                     $mailToAll .= $mailTo.",";
                  }
               }
               break;

            case "TECHNICAL":

               //send emails to TECHNICAL department
               for($k=0;$k<$countEmails;$k++)
               {
                  $mailTo      = $emailsArray[$k];
                  print "send to:".$mailTo."\n";

                  $mailSubject = $subject." Freeway Insurance";

                  $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
                  $emailId   .= $mailObj->GetMessageID().",";
                  $mailToAll .= $mailTo.",";
               }
               break;
         }// switch
      }// foreach

      // remove last coma
      $emailId   = substr($emailId,0,-1);
      $mailToAll = substr($mailToAll,0,-1);

      $outboundObj->UpdateOutboundField($this->outbounds['MAIL'],'sent_status', 1);
      $outboundObj->UpdateOutboundField($this->outbounds['MAIL'],'extra_params', $emailId);

      $sentContent =  "\nHEADERS: ".$headers."\nFROM:".$mailFrom."\nTO:".$mailToAll."\nSUBJECT: ".$mailSubject."\nBODY: ".$mailBody;

      $this->DoOutboundFiles("mail", $sentContent);

   } //end if MAIL


   //XML
   if($this->outbounds['XML'])
   {
      if(! in_array($this->session["USER_IP"],$ACRUX_IP_ARRAY) AND $this->session['_YourDetails_']['email_address'] != "test@quotezone.co.uk")
      {

         //filters here 
         $claimMade = $this->session['_YourDetails_']['claims_5_years'];
         $convMade  = $this->session['_YourDetails_']['convictions_5_years'];

         if(($claimMade != "No") OR ($convMade != "No"))
            return;

         //end filters


         //fileds
         //Taxi Registration         <registrationNumber> <vehicle>      Must be upper case and formatted as registration number.
         $registrationNumber = $this->session['_YourDetails_']['vehicle_registration_number'];
         $registrationNumber = strtoupper($registrationNumber);

         //Taxi Make                 <vehicleMake>        <vehicle>      Must be upper case.
         $vehicleMake = $_YourDetails['vehicle_make'][$this->session['_YourDetails_']['vehicle_make']];
         $vehicleMake = strtoupper($vehicleMake);

         //Taxi Model                <vehicleModel>       <vehicle>      Must be upper case.
         $vehicleModel = $this->session['_YourDetails_']['taxi_model'];
         $vehicleModel = strtoupper($vehicleModel);

         //Estimated Taxi Value      <value>              <vehicle>      Money values no decimal places.
         $estimatedValue = $this->session['_YourDetails_']['estimated_value'];

         //Year of Manufacture       <yearOfManufacture>  <vehicle>      Four numeric values only, no spaces.
         $yearOfManufacture = $this->session['_YourDetails_']['year_of_manufacture'];

         //Vehicle Mileage           <annualMileage>      <vehicle>      Should be a numeric value no decimal places.
         $annualMileage      = $_YourDetails["vehicle_mileage"][$this->session['_YourDetails_']['vehicle_mileage']];

         if($annualMileage == "Less than 10000")
         {
            $annualMileage      = str_replace("Less than ","",$annualMileage);
         }
         elseif($annualMileage == "Greater than 150,000")
         {
            $annualMileage      = str_replace("Greater than ","",$annualMileage);
         }
         else
         {
            $annualMileageArray = explode("-",$annualMileage);
            $annualMileage      = $annualMileageArray[1];
         }

         $annualMileage      = str_replace(",","",$annualMileage);


         //Taxi Type                 <bodyTypeID>         <vehicle>      Translate List Values: 'Black Cab' = 22, 'Saloon' = 01 and 'MPV' = 62
         //"1" => "Black Cab", 
         //"2" => "Saloon", 
         //"3" => "MPV", 
         //If <bodyTypeID> = 22 THEN                                                     
         //<hackneyCarriage> = true ELSE   <hackneyCarriage> = false

         $hackneyCarriage = 'false';
         $bodyType = $this->session['_YourDetails_']['taxi_type'];
         switch($bodyType)
         {
            case '1':
               $bodyTypeID = "22";
               $hackneyCarriage = 'true';
            break;

            case '2':
               $bodyTypeID = "01";
            break;

            case '3':
               $bodyTypeID = "62";
            break;
         }


         // fuelTypeID  Petrol (P) = 002 , Diesel (D) = 001
         $engineSize = $this->session['_YourDetails_']['engine_size'];
         if(preg_match("/P/isU", $engineSize, $matches))
            $fuelTypeID = $matches[0];

         if(preg_match("/D/isU", $engineSize, $matches))
            $fuelTypeID = $matches[0];

         if($fuelTypeID == "P")
            $fuelTypeID = "002";

         if($fuelTypeID == "D")
            $fuelTypeID = "001";

        // gearboxTypeID	Automatic (A) = 001  Manual (M) = 002

         $gearboxTypeID = $this->session['_YourDetails_']['transmission_type'];

         if($gearboxTypeID == "A")
            $gearboxTypeID = "001";

         if($gearboxTypeID == "M")
            $gearboxTypeID = "002";

         // <vehicleMark>Series</vehicleMark>
         $series = $this->session['_YourDetails_']['series'];
         if($registrationNumber == "")
            $series = "";

         //<vehicleCC>Engine_CC="2499"</vehicleCC>
         $engineSize = $this->session['_YourDetails_']['engine_size'];
         if(preg_match("/^([0-9]+)/", $engineSize, $matches))
            $engineCC = $matches[1];


         //Taxi Use                  <vehicleUseID>       <insuredParty> Translate List Values: 'Private Hire' = 15 and 'Public Hire' = 17
         //"1" => "Private Hire", 
         //"2" => "Public Hire",
         //Apply Logic: If <vehicleUseID> = 15 THEN
         //<qualificationID> = 614 ELSE
         //<qualificationID> = 613
         
         $qualificationID = "";
         $taxiUse = $this->session['_YourDetails_']['taxi_used_for'];
         switch($taxiUse)
         {
            case '1':
               $taxiUseID       = "15";
               $qualificationID =  "6I4";
               //$qualificationID =  "614";
            break;

            case '2':
               $taxiUseID = "17";
               $qualificationID =  "6I3";
               //$qualificationID =  "613";
            break;
         }

         //Max. Number of Passengers <numberOfSeats>      <vehicle>      Numeric fields only no decimal places.
         $numberOfSeats = $this->session['_YourDetails_']['taxi_capacity'];

         //Taxi Plating Authority   <taxiAreaID>          <vehicle>       Awaiting Mike

         //Type of Cover            <coverRequiredID>     <cover>         Translate List Values: 
         //Fully comprehensive = 01, 
         //Third Party Fire And Theft = 02
         //Third Party = 03
         //"1" => "Fully comprehensive", 
         //"2" => "Third party fire and theft", 
         //"3" => "Third party", 

         $typeOfCover = $this->session['_YourDetails_']['type_of_cover'];

         switch($typeOfCover)
         {
            case '1':
               $coverRequiredID = "01";
            break;

            case '2':
               $coverRequiredID = "02";
            break;

            case '3':
               $coverRequiredID = "03";
            break;
         }


         //Taxi No Claim Bonus      <ncdYears>            <ncd>           Numeric values only. Translate List Value: 'No NCB' = 0 and '5 Years +' = 5.
         $ncdYears = $this->session['_YourDetails_']['taxi_ncb'];

         //Full UK Licence          <dateLicenceObtained> <insuredParty>  See Below Full UK Logic, this should be returned as a date/time.1996-03-18T00:00:00
//          "1"  => "Under 6 Months",
//          "2"  => "6 Months To 1 Year",
//          "3"  => "1 - 2 Years",
//          "4"  => "2 - 3 Years",
//          "5"  => "3 - 4 Years",
//          "6"  => "4 - 5 Years",
//          "7"  => "5 - 6 Years",
//          "8"  => "6 - 7 Years",
//          "9"  => "7 - 8 Years",
//          "10" => "8 - 9 Years",
//          "11" => "9 - 10 Years",
//          "12" => "Over 10 Years",

         $fullUkLicObtained = $this->session['_YourDetails_']['period_of_licence'];

         switch($fullUkLicObtained)
         {

            case '1':
               $licDate = date("Y-m-d",mktime("0","0","0",date("m")-5,date("d"),date("Y")));  
            break;

            case '2':
               $licDate = date("Y-m-d",mktime("0","0","0",date("m")-11,date("d"),date("Y")));  
            break;

            case '3':
               $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-1));  
            break;

            case '4':
               $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-2));  
            break;

            case '5':
               $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-3));  
            break;

            case '6':
               $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-4));  
            break;

            case '7':
               $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-5));  
            break;

            case '8':
               $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-6));  
            break;

            case '9':
               $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-7));  
            break;

            case '10':
               $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-8));  
            break;

            case '11':
               $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-9));  
            break;

            case '12':
               $licDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-10));  
            break;
         }

         $licDateTime = $licDate."T00:00:00";
  
         //NOT FOUND IN THE EXAMPLE XML FILE!!!
         //Taxi badge               <datePassed>          <qualification> See Below Taxi Badge Logic, this should be returned as a date/time
         $taxiBadgeDate = $this->session['_YourDetails_']['taxi_badge'];

         switch($taxiBadgeDate)
         {
            case '1':
               $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")));  
            break;

            case '2':
               $badgeDate = date("Y-m-d",mktime("0","0","0",date("m")-5,date("d"),date("Y")));  
            break;

            case '3':
               $badgeDate = date("Y-m-d",mktime("0","0","0",date("m")-11,date("d"),date("Y")));  
            break;

            case '4':
               $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-1));  
            break;

            case '5':
               $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-2));  
            break;

            case '6':
               $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-3));  
            break;

            case '7':
               $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-4));  
            break;

            case '8':
               $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-5));  
            break;

            case '9':
               $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-6));  
            break;

            case '10':
               $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-7));  
            break;

            case '11':
               $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-8));  
            break;

            case '12':
               $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-9));  
            break;

            case '13':
               $badgeDate = date("Y-m-d",mktime("0","0","0",date("m"),date("d"),date("Y")-10));  
            break;
         }

         $datePassed = $badgeDate."T00:00:00";

         //Claims Last 5 years                            <claim>         Quotezone do not capture claims information

         //Convictions Last 5 years                       <conviction>    Quotezone do not capture conviction information

         //Title                    <titleID>         <insuredParty>          Translate List Values: 'Mr' = 003, 'Mrs' = 004, 'Ms' = 005 and 'Miss' = 002
         $title = $this->session['_YourDetails_']['title'];

         switch($title)
         {
            case 'Mr':
               $titleID = "003";
               $sex     = "Male";
            break;

            case 'Mrs':
               $titleID = "004";
               $sex     = "Female";
            break;

            case 'Ms':
               $titleID = "005";
               $sex     = "Female";
            break;

            case 'Miss':
               $titleID = "002";
               $sex     = "Female";
            break;
         }

         //First Name(s)            <forename>        <insuredParty>          Non numeric field
         $firstName = $this->session['_YourDetails_']['first_name'];

         //Surname                  <surname>         <insuredParty>          Non numeric field
         $surname = $this->session['_YourDetails_']['surname'];
   
         //Date Of Birth            <dob>             <insuredParty>          Date/Time field
         $dob = $this->session['_YourDetails_']['date_of_birth_yyyy']."-".$this->session['_YourDetails_']['date_of_birth_mm']."-".$this->session['_YourDetails_']['date_of_birth_dd']."T00:00:00";

         //Telephone                <telephoneNumber> <telephone>             Numeric field formatted as telephone number, <telephoneTypeID>should be set to '3AJPQ7C4'.
         $telephoneNumber = $this->session['_YourDetails_']['daytime_telephone'];

         if($telephoneNumber )
         {
            $telNumberXML = "<telephone instance=\"1\">
               <telephoneNumber>$telephoneNumber</telephoneNumber>
               <telephoneTypeID>3AJPQ7C4</telephoneTypeID>
            </telephone>";
         }



         //Mobile Phone             <telephoneNumber> <telephone>             Numeric field formatted as telephone number, <telephoneTypeID>should be set to '3AJPQ7C5'.
         $mobileNumber = $this->session['_YourDetails_']['mobile_telephone'];
         if($mobileNumber)
         {
            $mobileTelXML = "<telephone instance=\"1\">
               <telephoneNumber>$mobileNumber</telephoneNumber>
               <telephoneTypeID>3AJPQ7C6</telephoneTypeID>
            </telephone>";
         }


         //Email Address            <email>           <insuredParty>          Formatted as email address.
         $email = $this->session['_YourDetails_']['email_address'];

         //Post Code                <postcode>        <correspondanceAddress>
         $postCode = $this->session['_YourDetails_']['postcode'];

         //Post Code Lookup Results <house>           <correspondanceAddress>
         $house = $this->session['_YourDetails_']['house_number_or_name'];

         //Post Code Lookup Results <street>          <correspondanceAddress>
         $street = $this->session['_YourDetails_']['address_line2'];

         //Post Code Lookup Results <locality>        <correspondanceAddress>
         $locality = $this->session['_YourDetails_']['address_line3']; 

         //Post Code Lookup Results <county>          <correspondanceAddress>
         $county = $this->session['_YourDetails_']['address_line4']; 

         //Post Code Lookup Results <country>         <correspondanceAddress>
         //defaulted

         //Post Code Lookup Results <city>            <correspondanceAddress>
         $city = $locality;
         
         //<qualificationID>             <qualification> Apply Logic: If <vehicleUseID> = 15 THEN                                                      <qualificationID> = 614 ELSE <qualificationID> = 613
         //tag not in example

         //<yearsWithCurrentInsurer>     <cover>         Apply Logic: <yearsNcdPriorToClaim> - 1
         //we dont ask ncd prior to claim
         $yearsWithCurrentInsurer = $ncdYears - 1;
         if($yearsWithCurrentInsurer < 0)
            $yearsWithCurrentInsurer = 0;

         //<previousInsuranceExpiryDate> <cover>         Apply Logic: <coverStartDate> - 1 Minute i.e.                                                      14/02/2012  00:00 = 13/02/2012 23:59

         $DISMkTime = mktime("0","0","0",$this->session['_YourDetails_']['date_of_insurance_start_mm'],$this->session['_YourDetails_']['date_of_insurance_start_dd'],$this->session['_YourDetails_']['date_of_insurance_start_yyyy']);

         //2011-09-29 00:00
         $DIS = $this->session['_YourDetails_']['date_of_insurance_start_yyyy']."-".$this->session['_YourDetails_']['date_of_insurance_start_mm']."-".$this->session['_YourDetails_']['date_of_insurance_start_dd']."T00:01:00";

         //$previousInsuranceExpiryDate = date("Y-m-d",(mktime("0","0","0",$this->session['_YourDetails_']['date_of_insurance_start_mm'],$this->session['_YourDetails_']['date_of_insurance_start_dd'],$this->session['_YourDetails_']['date_of_insurance_start_yyyy']) - 1));
         //$previousInsuranceExpiryDate = $previousInsuranceExpiryDate."T23:59:59";

         $previousInsuranceExpiryDate = date("Y-m-d");
         $previousInsuranceExpiryDate = $previousInsuranceExpiryDate."T23:59:59";

         //ukResidenceYears
         $ukResidenceYears = $this->session['_YourDetails_']['date_of_insurance_start_yyyy'] - $this->session['_YourDetails_']['date_of_birth_yyyy'];


         //<previousInsurance>           <cover>         Apply Logic: If <yearsNcdPriorToClaim> = 0                                                      THEN <previousInsurance> = false ELSE  <previousInsurance> = true
         //we dont ask ncd prior to claim


         if($ncdYears > 0)
            $previousInsurance = "true";
			else
				$previousInsurance = "false";


         //<hackneyCarriage>             <vehicle>       Apply Logic: If <bodyTypeID> = 22 THEN                                                      <hackneyCarriage> = true ELSE   <hackneyCarriage> = false

         $dateOfPurchaseDate = date("Y-m-d");
         $dateOfPurchase     = $dateOfPurchaseDate."T00:00:00";

         $taxiPlatingAuthority = strtoupper($this->session["_YourDetails_"]["plating_authority"]);
         $taxiPlatingAuthorityArray = array(
            "ABERDEEN"=>"1",
            "ABERDEENSHIRE"=>"2",
            "ADUR"=>"3",
            "ALLERDALE"=>"4",
            "ALNWICK"=>"5",
            "AMBER VALLEY"=>"6",
            "ANGUS"=>"7",
            "ARGYLE & BUTE"=>"8",
            "ARUN"=>"9",
            "ASHFIELD"=>"10",
            "ASHFORD"=>"11",
            "AYLESBURY VALE"=>"12",
            "BABERGH"=>"13",
            "BADENOCH & STRATHSPEY"=>"14",
            "BARKING"=>"187",
            "BARNET"=>"187",
            "BARNSLEY"=>"15",
            "BARROW IN FURNESS"=>"16",
            "BASILDON"=>"17",
            "BASINGSTOKE & DEANE"=>"18",
            "BASSETLAW"=>"19",
            "BATH & N.E SOMERSET"=>"20",
            "BEDFORD"=>"21",
            "BERWICK ON TWEED"=>"22",
            "BEXLEY"=>"187",
            "BIRMINGHAM"=>"23",
            "BLABY"=>"24",
            "BLACKBURN WITH DARWEN"=>"25",
            "BLACKPOOL"=>"26",
            "BLAENAU GWENT"=>"27",
            "BLYTH VALLEY"=>"28",
            "BOLSOVER"=>"29",
            "BOLTON"=>"30",
            "BOSTON"=>"31",
            "BOURNEMOUTH"=>"32",
            "BRACKNELL FOREST"=>"33",
            "BRADFORD"=>"34",
            "BRAINTREE"=>"35",
            "BRECKLAND"=>"36",
            "BRENT"=>"187",
            "BRENTWOOD"=>"37",
            "BRIDGEND"=>"38",
            "BRIDGENORTH"=>"39",
            "BRIGHTON & HOVE"=>"40",
            "BRISTOL CITY OF UA"=>"41",
            "BROADLAND"=>"42",
            "BROMLEY"=>"187",
            "BROMSGROVE"=>"43",
            "BROXBOURNE"=>"44",
            "BROXTOWE"=>"45",
            "BURNLEY"=>"46",
            "BURY"=>"47",
            "BURY ST EDMUNDS"=>"48",
            "CAERPHILLY"=>"49",
            "CAITHNESS (HIGHLANDS)"=>"50",
            "CALDERDALE"=>"51",
            "CAMBRIDGE"=>"52",
            "CAMDEN"=>"187",
            "CANNOCK CHASE"=>"53",
            "CANTERBURY"=>"54",
            "CARADON"=>"55",
            "CARDIFF"=>"56",
            "CARLISLE"=>"57",
            "CARMARTHENSHIRE"=>"58",
            "CARRICK"=>"59",
            "CASTLE MORPETH"=>"60",
            "CASTLE POINT"=>"61",
            "CENTRAL BEDFORDSHIRE"=>"297",
            "CEREDIGION"=>"62",
            "CHANNEL ISLANDS"=>"408",
            "CHARNWOOD"=>"63",
            "CHELMSFORD"=>"64",
            "CHELTENHAM"=>"65",
            "CHERWELL"=>"66",
            "CHESTER"=>"67",
            "CHESTERFIELD"=>"68",
            "CHESTER-LE-STREET"=>"69",
            "CHICHESTER"=>"70",
            "CHILTERN"=>"71",
            "CHORLEY"=>"72",
            "CHRISTCHURCH"=>"73",
            "CLACKMANNANSHIRE"=>"74",
            "CLYDEBANK"=>"75",
            "CLYDESDALE"=>"76",
            "COLCHESTER"=>"77",
            "CONGLETON"=>"78",
            "CONWY"=>"79",
            "COPELAND"=>"80",
            "CORBY"=>"81",
            "COTSWOLD"=>"82",
            "COUNTY OF HEREFORD"=>"83",
            "COVENTRY"=>"84",
            "CRAVEN"=>"85",
            "CRAWLEY"=>"86",
            "CREWE & NANTWICH"=>"87",
            "CROYDEN"=>"187",
            "DACORUM"=>"88",
            "DARLINGTON"=>"89",
            "DARTFORD"=>"90",
            "DAVENTRY"=>"91",
            "DENBIGSHIRE"=>"92",
            "DERBY"=>"93",
            "DERBYSHIRE DALES"=>"94",
            "DERWENTSIDE"=>"95",
            "DEVON COUNTY"=>"413",
            "DONCASTER"=>"96",
            "DOVER"=>"97",
            "DUDLEY"=>"98",
            "DUMFRIES & GALLOWAY"=>"99",
            "DUNBARTON"=>"100",
            "DUNDEE"=>"101",
            "DURHAM"=>"102",
            "EALING"=>"187",
            "EASINGTON"=>"103",
            "EAST AYRSHIRE (KILMARNOCK)"=>"104",
            "EAST CAMBRIDGESHIRE"=>"105",
            "EAST DEVON"=>"106",
            "EAST DORSET"=>"107",
            "EAST DUNBARTONSHIRE (KIRKINTILLOCH)"=>"108",
            "EAST HAMPSHIRE"=>"109",
            "EAST HERTS"=>"110",
            "EAST KILBRIDE"=>"111",
            "EAST LINDSEY"=>"112",
            "EAST LOTHIAN (HADDINGTON)"=>"113",
            "EAST NORTHANTS"=>"114",
            "EAST RENFREWSHIRE (GIFFNOCH)"=>"115",
            "EAST RIDING"=>"116",
            "EAST STAFFORDSHIRE"=>"117",
            "EASTBOURNE"=>"118",
            "EASTLEIGH"=>"119",
            "EDEN"=>"120",
            "EDINBURGH"=>"121",
            "ELLESMERE PORT"=>"122",
            "ELMBRIDGE"=>"123",
            "ENFIELD"=>"187",
            "EPPING FOREST"=>"124",
            "EPSOM & EWELL"=>"125",
            "EREWASH"=>"126",
            "EXETER"=>"127",
            "FALKIRK"=>"128",
            "FAREHAM"=>"129",
            "FENLAND"=>"130",
            "FIFE"=>"131",
            "FLINTSHIRE"=>"132",
            "FOREST HEATH"=>"133",
            "FOREST OF DEAN"=>"134",
            "FYLDE"=>"135",
            "GATESHEAD"=>"136",
            "GEDLING"=>"137",
            "GILLINGHAM"=>"138",
            "GLASGOW"=>"139",
            "GLOUCESTER"=>"140",
            "GLOUCESTERSHIRE COUNTY"=>"414",
            "GOSPORT"=>"141",
            "GRAVESHAM"=>"142",
            "GREAT YARMOUTH"=>"143",
            "GREENWICH"=>"187",
            "GUERNSEY"=>"410",
            "GUILDFORD"=>"144",
            "GWYNEDD"=>"145",
            "HACKNEY"=>"187",
            "HALTON"=>"146",
            "HAMBLETON"=>"147",
            "HAMILTON"=>"148",
            "HAMMERSMITH"=>"187",
            "HARBOROUGH"=>"149",
            "HARINGEY"=>"187",
            "HARLOW"=>"150",
            "HARROGATE"=>"151",
            "HART"=>"152",
            "HARTLEPOOL"=>"153",
            "HASTINGS"=>"154",
            "HAVANT"=>"155",
            "HAVERING"=>"187",
            "HEREFORD"=>"156",
            "HERTSMERE"=>"157",
            "HIGH PEAK"=>"158",
            "HILLINGDON"=>"187",
            "HINCKLEY & BOSWORTH"=>"160",
            "HORSHAM"=>"161",
            "HOUNSLOW"=>"187",
            "HUNTINGDONSHIRE"=>"162",
            "HYNDBURN"=>"163",
            "INVERCLYDE (GREENOCK)"=>"164",
            "INVERNESS (HIGHLANDS)"=>"165",
            "IPSWICH"=>"166",
            "ISLE OF ANGLESEY"=>"167",
            "ISLE OF MAN"=>"168",
            "ISLE OF SCILLY"=>"169",
            "ISLE OF WIGHT"=>"170",
            "ISLINGTON"=>"187",
            "JERSEY"=>"409",
            "KEIGHLEY"=>"421",
            "KENNET"=>"171",
            "KENSINGTON"=>"187",
            "KENT"=>"412",
            "KERRIER"=>"172",
            "KETTERING"=>"173",
            "KINGS LYNN & W NORFOLK"=>"174",
            "KINGSTON UPON THAMES ROYAL"=>"187",
            "KINGSTON-UPON-HULL"=>"175",
            "KIRKLEES"=>"176",
            "KNOWSLEY"=>"177",
            "LAMBETH"=>"187",
            "LANCASTER"=>"178",
            "LEEDS"=>"179",
            "LEICESTER"=>"180",
            "LEOMINSTER"=>"181",
            "LEWES"=>"182",
            "LEWISHAM"=>"187",
            "LICHFIELD"=>"183",
            "LINCOLN"=>"184",
            "LIVERPOOL"=>"185",
            "LOCHABER (HIGHLAND - FORT WILLIAM)"=>"159",
            "LONDON PCO"=>"187",
            "LUTON"=>"188",
            "MACCLESFIELD"=>"189",
            "MAIDSTONE"=>"190",
            "MALDON"=>"191",
            "MALVERN HILLS"=>"192",
            "MANCHESTER"=>"193",
            "MANSFIELD"=>"194",
            "MEDWAY"=>"195",
            "MELTON"=>"196",
            "MENDIP"=>"197",
            "MERTHYR TYDFIL"=>"198",
            "MERTON"=>"187",
            "MID BEDFORDSHIRE"=>"199",
            "MID DEVON"=>"200",
            "MID SUFFOLK"=>"201",
            "MID SUSSEX"=>"202",
            "MIDDLESBOROUGH"=>"203",
            "MIDLOTHIAN"=>"204",
            "MILTON KEYNES"=>"205",
            "MOLE VALLEY"=>"206",
            "MONMOUTHSHIRE"=>"207",
            "MORAY"=>"208",
            "MOTHERWELL DISTRICT COUNCIL"=>"",
            "NAIRN (HIGHLANDS)"=>"209",
            "NEATH PORT TALBOT"=>"210",
            "NEW FOREST"=>"211",
            "NEWARK & SHERWOOD"=>"212",
            "NEWBURY"=>"213",
            "NEWCASTLE UNDER LYME"=>"214",
            "NEWCASTLE UPON TYNE"=>"215",
            "NEWHAM"=>"187",
            "NEWPORT"=>"216",
            "NORTH AYRSHIRE"=>"217",
            "NORTH CORNWALL"=>"218",
            "NORTH DEVON"=>"219",
            "NORTH DORSET"=>"220",
            "NORTH EAST DERBYSHIRE"=>"221",
            "NORTH EAST LINCOLNSHIRE"=>"222",
            "NORTH HERTS"=>"223",
            "NORTH KESTEVEN"=>"224",
            "NORTH LANARKSHIRE (NORTH)"=>"225",
            "NORTH LANARKSHIRE(SOUTH/CENTRAL)"=>"226",
            "NORTH LICOLNSHIRE"=>"227",
            "NORTH NORFOLK"=>"228",
            "NORTH SHROPSHIRE"=>"229",
            "NORTH SOMERSET"=>"230",
            "NORTH TYNESIDE"=>"231",
            "NORTH WARWICKS"=>"232",
            "NORTH WEST LEICESTER"=>"233",
            "NORTH WILTSHIRE"=>"234",
            "NORTH YORKSHIRE COUNTY"=>"415",
            "NORTHAMPTON"=>"235",
            "NORTHUMBERLAND"=>"411",
            "NORWICH"=>"236",
            "NOTTINGHAM"=>"237",
            "NUNEATON & BEDWORTH"=>"238",
            "OADBY & WIGSTON"=>"239",
            "OLDHAM"=>"240",
            "ORKNEY ISLANDS (KIRKWALL)"=>"241",
            "OSWESTRY"=>"242",
            "OXFORD"=>"243",
            "OXFORDSHIRE COUNTY"=>"416",
            "PEMBROKE"=>"244",
            "PENDLE"=>"245",
            "PENWITH"=>"246",
            "PERTH & KINROSS"=>"247",
            "PETERBOROUGH"=>"248",
            "PLYMOUTH"=>"249",
            "POOLE"=>"250",
            "PORTSMOUTH"=>"251",
            "POWYS"=>"252",
            "PRESTON"=>"253",
            "PURBECK"=>"254",
            "READING"=>"255",
            "REDBRIDGE"=>"187",
            "REDCAR & CLEVELAND"=>"256",
            "REDDITCH"=>"257",
            "REIGATE AND BANSTEAD"=>"258",
            "RENFREWSHIRE (PAISLEY)"=>"259",
            "RESTORMEL"=>"260",
            "RHONDDA CYNON TAFF"=>"261",
            "RIBBLE VALLEY"=>"262",
            "RICHMOND"=>"187",
            "RICHMONDSHIRE"=>"263",
            "ROCHDALE"=>"264",
            "ROCHESTER"=>"265",
            "ROCHFORD"=>"266",
            "ROSS & CROMARTY (HIGHLAND - DINGWALL)"=>"267",
            "ROSSENDALE"=>"268",
            "ROTHER"=>"269",
            "ROTHERHAM"=>"270",
            "RUGBY"=>"272",
            "RUNNYMEDE"=>"273",
            "RUSHCLIFFE"=>"274",
            "RUSHMOOR"=>"275",
            "RUTLAND"=>"277",
            "RYEDALE"=>"278",
            "SALFORD"=>"279",
            "SALISBURY"=>"280",
            "SANDWELL"=>"281",
            "SCARBOROUGH"=>"282",
            "SCOTTISH BORDERS"=>"283",
            "SEDGEFIELD"=>"284",
            "SEDGEMOOR"=>"285",
            "SEFTON"=>"286",
            "SELBY"=>"287",
            "SEVENOAKS"=>"288",
            "SHEFFIELD"=>"289",
            "SHEPWAY"=>"290",
            "SHETLAND ISLANDS (LERWICK)"=>"291",
            "SHREWSBURY"=>"292",
            "SHROPSHIRE COUNTY"=>"417",
            "SKYE & LOCHAISH (HIGHLANDS)"=>"293",
            "SLOUGH"=>"294",
            "SOLIHULL"=>"295",
            "SOUTH AYRSHIRE (AYR)"=>"296",
            "SOUTH BEDFORDSHIRE"=>"297",
            "SOUTH BUCKINGHAM"=>"298",
            "SOUTH CAMBRIDGE"=>"299",
            "SOUTH DERBYSHIRE"=>"300",
            "SOUTH EAST & METROPOLITAN"=>"187",
            "SOUTH GLOUCESTER"=>"301",
            "SOUTH HAMS"=>"302",
            "SOUTH HEREFORDSHIRE"=>"303",
            "SOUTH HOLLAND"=>"304",
            "SOUTH KESTEVEN"=>"305",
            "SOUTH LAKELAND"=>"306",
            "SOUTH LANARKSHIRE"=>"307",
            "SOUTH NORFOLK"=>"309",
            "SOUTH NORTHANTS"=>"310",
            "SOUTH OXFORDSHIRE"=>"311",
            "SOUTH RIBBLE"=>"312",
            "SOUTH SHROPSHIRE"=>"313",
            "SOUTH SOMERSET"=>"314",
            "SOUTH STAFFORDSHIRE"=>"315",
            "SOUTH TYNESIDE"=>"316",
            "SOUTHAMPTON"=>"317",
            "SOUTHEND-ON-SEA"=>"318",
            "SOUTHWARK"=>"187",
            "SPELTHORNE"=>"319",
            "ST ALBANS"=>"320",
            "ST EDMUNDSBURY"=>"321",
            "ST HELENS"=>"322",
            "ST HELIER - JERSEY"=>"409",
            "STAFFORD"=>"323",
            "STAFFORDSHIRE COUNTY"=>"420",
            "STAFFS MOORLANDS"=>"324",
            "STEVENAGE"=>"325",
            "STIRLING"=>"326",
            "STOCKPORT"=>"327",
            "STOCKTON ON TEES"=>"328",
            "STOKE ON TRENT"=>"329",
            "STRATFORD ON AVON"=>"330",
            "STROUD"=>"331",
            "SUFFOLK COASTAL"=>"332",
            "SUNDERLAND"=>"333",
            "SURREY HEATH"=>"334",
            "SUTHERLAND (HIGHLANDS)"=>"335",
            "SUTTON"=>"187",
            "SWALE"=>"336",
            "SWANSEA"=>"337",
            "SWINDON"=>"338",
            "TAMESIDE"=>"339",
            "TAMWORTH"=>"340",
            "TANDRIDGE"=>"341",
            "TAUNTON DEANE"=>"342",
            "TEESDALE"=>"343",
            "TEIGNBRIDGE"=>"344",
            "TELFORD & WREKIN"=>"345",
            "TENDRING"=>"346",
            "TEST VALLEY"=>"347",
            "TEWKESBURY"=>"348",
            "THAMESDOWN"=>"349",
            "THANET BROADSTAIRS"=>"350",
            "THANET DISTRICT"=>"351",
            "THANET MARGATE"=>"352",
            "THANET RAMSGATE"=>"353",
            "THREE RIVERS"=>"354",
            "THURROCK"=>"355",
            "TONBRIDGE & MALLING"=>"356",
            "TORBAY"=>"357",
            "TORFAEN"=>"358",
            "TORRIDGE"=>"359",
            "TOWER HAMLETS"=>"187",
            "TRAFFORD"=>"360",
            "TUNBRIDGE WELLS"=>"361",
            "TYNEDALE"=>"362",
            "UTTLESFORD"=>"363",
            "VALE OF GLAMORGAN"=>"364",
            "VALE OF WHITE HORSE"=>"365",
            "VALE ROYAL"=>"366",
            "WAKEFIELD"=>"367",
            "WALSALL"=>"368",
            "WALTHAM FOREST"=>"187",
            "WANDSWORTH"=>"187",
            "WANSBECK"=>"369",
            "WARRINGTON"=>"370",
            "WARWICK"=>"371",
            "WATFORD"=>"372",
            "WAVENEY"=>"373",
            "WAVERLEY"=>"374",
            "WEALDEN"=>"375",
            "WEAR VALLEY"=>"376",
            "WELLINGBOROUGH"=>"377",
            "WELWYN HATFIELD"=>"378",
            "WEST BERKSHIRE"=>"379",
            "WEST DEVON"=>"380",
            "WEST DORSET"=>"381",
            "WEST DUNBARTONSHIRE (CLYDEBANK)"=>"382",
            "WEST LANCASHIRE"=>"383",
            "WEST LINDSEY"=>"384",
            "WEST LOTHIAN (LIVINGSTON)"=>"385",
            "WEST OXFORD"=>"386",
            "WEST SOMERSET"=>"387",
            "WEST SUSSEX COUNTY"=>"418",
            "WEST WILTSHIRE"=>"388",
            "WESTERN ISLES (STORNOWAY)"=>"389",
            "WESTMINSTER"=>"187",
            "WEYMOUTH & PORTLAND"=>"390",
            "WIGAN"=>"391",
            "WILTSHIRE COUNTY"=>"419",
            "WINCHESTER"=>"392",
            "WINDSOR & MAIDENHEAD"=>"393",
            "WIRRALL"=>"394",
            "WOKING"=>"395",
            "WOKINGHAM"=>"396",
            "WOLVERHAMPTON"=>"397",
            "WORCESTER"=>"398",
            "WORTHING"=>"399",
            "WREKIN"=>"400",
            "WREXHAM"=>"401",
            "WYCHAVON"=>"402",
            "WYCOMBE"=>"403",
            "WYRE"=>"404",
            "WYRE FORREST"=>"405",
            "YNYS MON"=>"406",
            "YORK"=>"407",
            "DVA (NI)"=>"0",
            "KILMARNOCK & LOUDOUN DISTRICT COUNCIL"=>"0",
            "KINCARDINE & DEESIDE DISTRICT COUNCIL"=>"0",
            "KIRKALDY DISTRICT COUNCIL"=>"0",
            "LOTHIAN REGIONAL COUNCIL"=>"0",
            "PSV EASTERN"=>"0",
            "PSV JERSEY"=>"0",
            "PSV NORTH EAST"=>"0",
            "PSV NORTH WEST"=>"0",
            "PSV SCOTLAND"=>"0",
            "PSV WALES"=>"0",
            "PSV WEST MIDLANDS"=>"0",
            "PSV WESTERN"=>"0",
            "SOUTH LANARKSHIRE (HIGHLANDS"=>"308",
				"CHESHIRE EAST COUNCIL" => "189",
				"CHESHIRE WEST & CHESTER (RED) ELLESMERE PORT" => "122",
				"CHESHIRE WEST & CHESTER (GREEN) VALE ROYAL" => "366",
				"CHESHIRE WEST & CHESTER (BLUE) CHESTER" => "67",
				"CORNWALL COUNCIL" => "218",

         );
         
         $taxiPlatingAuthorityID = $taxiPlatingAuthorityArray[$taxiPlatingAuthority];

         $privateCarNCB          = $this->session['_YourDetails_']['private_car_ncb'];


	// <yearsNcdPriorToClaim> = 0 
	// THEN
	// <previousInsurance> = false
	// ELSE
	// <previousInsurance> = true

         $xmlTemplate = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
									<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
									<soap:Body>
										<Quote xmlns=\"http://TGSL/TES_Aggregator\">
											<lxmlTGSLXML>
											<tgsl xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://localhost/TES_Aggregator/schemas/tgsl.xsd\" xmlns=\"http://www.transactorgsl.com/schemas\">
												<aggregator>fwquotezone</aggregator>
												<domain>http://localhost</domain>
												<partner>
													<partnerName>freeway</partnerName>
													<broker>freeway</broker>
												</partner>
												<product>cv</product>
											</tgsl>
											</lxmlTGSLXML>
											<lstrRiskXML>
											<cvRisk xmlns=\"http://www.transactorgsl.com/schemas\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://localhost/TES_Aggregator/Schemas/TGSL/TGSLCVSchema.xsd\">
												<insuredParty instance=\"1\">
													<accessToOtherVehicles>true</accessToOtherVehicles>
													<client>true</client>
													<clubMember>false</clubMember>
													<dateLicenceObtained>$licDateTime</dateLicenceObtained>
													<dob>$dob</dob>
													<email>$email</email>
													<forename>$firstName</forename>
													<homeOwner>true</homeOwner>
													<insurancePreviouslyRefused>false</insurancePreviouslyRefused>
													<licenceRestrictionsID>8</licenceRestrictionsID>
													<licenceTypeID>F</licenceTypeID>
													<mainDriver>true</mainDriver>
													<maritalStatusID></maritalStatusID>
													<mothersMaidenName></mothersMaidenName>
													<numberOfVehiclesAccessTo>0</numberOfVehiclesAccessTo>
													<password></password>
													<sex>$sex</sex>
													<surname>$surname</surname>
													<titleID>$titleID</titleID>
													<ukResidenceYears>$ukResidenceYears</ukResidenceYears>
													<vehicleUseID>$taxiUseID</vehicleUseID>
												</insuredParty>
												<correspondanceAddress instance=\"1\">
													<city>$city</city>
													<country>United Kingdom</country>
													<county>$county</county>
													<house>$house</house>
													<locality>$locality</locality>
													<postcode>$postCode</postcode>
													<street>$street</street>
												</correspondanceAddress>
												<occupation instance=\"1\" insuredPartyInstance=\"1\">
													<employersBusinessID>703</employersBusinessID>
													<employmentStatusID>S</employmentStatusID>
													<occupationID>D34</occupationID>
													<parttime>false</parttime>
													<primaryOccupation>true</primaryOccupation>
												</occupation>
												$telNumberXML $mobileTelXML
												<qualification instance=\"1\" insuredPartyInstance=\"1\">
													<datePassed>$datePassed</datePassed>
													<qualificationID>$qualificationID</qualificationID>
												</qualification>
												<cover instance=\"1\">
													<coverRequiredID>$coverRequiredID</coverRequiredID>
													<coverStartDate>$DIS</coverStartDate>
													<previousInsurance>$previousInsurance</previousInsurance>
													<previousInsuranceExpiryDate>$previousInsuranceExpiryDate</previousInsuranceExpiryDate>
													<previousInsurancePolicyNumber>LEADIMPORT</previousInsurancePolicyNumber>
													<previousInsurerID>997</previousInsurerID>
													<yearsWithCurrentInsurer>$yearsWithCurrentInsurer</yearsWithCurrentInsurer>
													<yearsNcdPriorToClaim>$ncdYears</yearsNcdPriorToClaim>
													<voluntaryExcess>150</voluntaryExcess>
												</cover>
												<vehicle instance=\"1\">
													<personalisedPlate>false</personalisedPlate>
													<abiCode>0</abiCode>
													<annualMileage>$annualMileage</annualMileage>
													<bodyTypeID>$bodyTypeID</bodyTypeID>
													<businessMileage>0</businessMileage>
													<countryOfFirstRegistrationID>GB</countryOfFirstRegistrationID>
													<currentMileage>0</currentMileage>
													<dateOfPurchase>$dateOfPurchase</dateOfPurchase>
													<fuelTypeID>$fuelTypeID</fuelTypeID>
													<gearboxTypeID>$gearboxTypeID</gearboxTypeID>
													<imported>false</imported>
													<keeperID>1</keeperID>
													<kitCar>false</kitCar>
													<leftHandDrive>false</leftHandDrive>
													<numberOfSeats>$numberOfSeats</numberOfSeats>
													<ownerID>1</ownerID>
													<overnightParkingID>1</overnightParkingID>
													<pricePaid>0</pricePaid>
													<qPlated>false</qPlated>
													<qPlatedReasonID>0</qPlatedReasonID>
													<registrationNumber>$registrationNumber</registrationNumber>
													<value>$estimatedValue</value>
													<vehicleCC>$engineCC</vehicleCC>
													<vehicleMake>$vehicleMake</vehicleMake>
													<vehicleMark>$series</vehicleMark>
													<vehicleModel>$vehicleModel</vehicleModel>
													<yearOfManufacture>$yearOfManufacture</yearOfManufacture>
													<trailer>false</trailer>
													<carriesGoods>false</carriesGoods>
													<goodsCarriedID>0</goodsCarriedID>
													<grossVehicleWeight>0</grossVehicleWeight>
													<carryingCapacity>0</carryingCapacity>
													<cctv>false</cctv>
													<interiorCamera>false</interiorCamera>
													<exteriorCameraID>0</exteriorCameraID>
													<hackneyCarriage>$hackneyCarriage</hackneyCarriage>
													<contractWork>false</contractWork>
													<contractDetails></contractDetails>
													<licensePlate></licensePlate>
													<taxiAreaID>$taxiPlatingAuthorityID</taxiAreaID>
												</vehicle>
												<ncd instance=\"1\">
													<ncdYears>$ncdYears</ncdYears>
													<otherVehicleNCDYears>$privateCarNCB</otherVehicleNCDYears>
													<protectedNCD>false</protectedNCD>
												</ncd>
												<garageAddress instance=\"1\">
													<city>$city</city>
													<country>United Kingdom</country>
													<county>$county</county>
													<house>$house</house>
													<locality>$locality</locality>
													<postcode>$postCode</postcode>
													<street>$street</street>
												</garageAddress>
											</cvRisk>
											</lstrRiskXML>
										</Quote>
									</soap:Body>
									</soap:Envelope>";


         //http://94.127.98.171/TES_Aggregator/Aggregator.asmx
         $url = "http://94.127.98.171/TES_Aggregator/Aggregator.asmx";

         $ch = curl_init();

         $header[] = "POST /TES_Aggregator/Aggregator.asmx HTTP/1.1";
         $header[] = "Host:94.127.98.171";
         //$header[] = "Content-Type: application/soap+xml; charset=utf-8";
         $header[] = "Content-Type: text/xml; charset=utf-8";
         $header[] = "Content-length: ".strlen($xmlTemplate);
         $header[] = "Connection: close \r\n";
         $header[] = $xmlTemplate;

         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
         curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 180);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_HEADER, 1);

         $result = curl_exec($ch);

         curl_close ($ch);

         $xmlMessageBody  = "<table width='500' cellspacing='2' cellpadding='2' border='0' align='left'>";
         $xmlMessageBody .= "<tr>
                           <td  style=\"font:bold 18px Calibri;color:#9C0D0D;\" align=\"left\">QUOTEZONE</td>
                           <td style=\"font:bold 12px Calibri;color:black;\" align=\"right\">$dateAndTime</td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML REQUEST:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$xmlTemplate</td>
                        </tr>";
         $xmlMessageBody .= "<tr height=\"10\">
                           <td colspan=\"2\">
                              &nbsp;
                           </td>
                        </tr>";
         $xmlMessageBody .= "<tr>
                           <td style=\"font:bold 16px Calibri;color:#3D7FB9;\"  colspan=\"2\" align=\"left\">XML RESPONSE:</td>
                        </tr>";
         $xmlMessageBody .= "<tr style=\"font:bold 12px Calibri;\">
                           <td colspan=\"2\" align=\"left\">$result</td>
                        </tr>";
         $xmlMessageBody .= "</table>"; 

         $headers  = 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
         //$headers .= "From: <taxi@quotezone.co.uk>\r\n";
         ////$headers .= "X-Sender: <taxi@quotezone.co.uk>\r\n";
         $headers .= "X-Mailer: PHP\r\n"; // mailer
         $headers .= "X-Priority: 1\r\n"; // Urgent message!
         ////$headers .= "Return-Path: <taxi@quotezone.co.uk>\r\n";

         //eb2
         $mailTo      = "eb2-technical@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." -Taxi Freeway - XML Method";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);

         //seopa
         $mailTo      = "leads@seopa.com";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject."-Taxi Freeway - XML Method";
         $mailBody    = $xmlMessageBody;
			$mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);
         
         //company
         $mailTo      = "Peter.Bann@freewayinsurance.co.uk";
         $mailFrom    = "taxi@quotezone.co.uk";
         $mailSubject = $subject." - XML Method (Clean Risk)";
         $mailBody    = $xmlMessageBody;
         $mailObj->SendMail($mailFrom, $mailTo, $mailSubject, $mailBody, $headers);

         $outboundObj->UpdateOutboundField($this->outbounds['XML'],'sent_status', 1);

         if(preg_match("/\<Status\>QUOTE\<\/Status\>/i",$result))
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 1);
         else
            $outboundObj->UpdateOutboundField($this->outbounds['XML'],'received_status', 2);

         $sentContent =  "\nURL: ".$url."\n<br>PARAMS:".$xmlTemplate;

         $this->DoOutboundFiles("xml", $sentContent,$result);
      }
   }
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: PrepareUrl
//
// [DESCRIPTION]:   Prepare a remote site config
//
// [PARAMETERS]:    $url = ""
//
// [RETURN VALUE]:  string, prepared url
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function PrepareUrl($url = "")
{
   $url = $this->siteConfig['nextUrl'];
   $this->httpReferer = $this->siteConfig['refererUrl'];
   return $url;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Get form name of current page
//
// [PARAMETERS]:    array
//
// [RETURN VALUE]:  the form name|_body_form
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE

function GetFormName($formParams)
{

   if(count($formParams) == 1)
      return "_body_form";

   foreach ($formParams as $formName => $value)
      if($formName != '_body_form')
         return $formName;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessResponse
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessResponse()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ProcessLastPage
//
// [DESCRIPTION]:   Process and parse html contents after url download
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  none
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ProcessLastPage()
{
   print "QUOTE SUCCESS";
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: CheckResponseErrors
//
// [DESCRIPTION]:   Check an html response page for errors. Extract errors.
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  true|false
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function CheckResponseErrors()
{
   return true;
}

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: GetResultElements
//
// [DESCRIPTION]:   Get the results from the last page
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function GetResultElements()
{
   return;
} // end function GetResultElements(&$elementArray)

//////////////////////////////////////////////////////////////////////////////FB
//
// [FUNCTION NAME]: ParseXmlResponse
//
// [DESCRIPTION]:   Get the results from the last page only for a broker
//
// [PARAMETERS]:    none
//
// [RETURN VALUE]:  false | array
//
// [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
//
// [MODIFIED]:      - [programmer (email) date]
//                    [short description]
//////////////////////////////////////////////////////////////////////////////FE
function ParseXmlResponse()
{
   return;
}
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetXmlTemplate()
   //
   // [DESCRIPTION]:   -
   //
   // [PARAMETERS]:    none
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2008-10-07
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetXmlTemplate($type='',$d='', $sd='')
   {
      return;
   }


   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareXml
   //
   // [DESCRIPTION]:   Prepare the xml to send to PD
   //
   // [PARAMETERS]:    $SESSION
   //
   // [RETURN VALUE]:  xml data to send
   //
   // [CREATED BY]:    Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-11-06
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareXml($SESSION)
   {

   return;

   }// end function PrepareXml($SESSION)
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: PrepareQuote
   //
   // [DESCRIPTION]:   Prepare the array of params to get a quote
   //
   // [PARAMETERS]:    $sessionParams ($_SESSION);
   //
   // [RETURN VALUE]:  none
   //
   // [CREATED BY]:    Gabi ISTVANCSEK (null@seopa.com) 2004-11-11
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function PrepareQuote()
   {
      $this->DoOutbound();

      return;
   }// end function PrepareQuote()

   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetUrl
   //
   // [DESCRIPTION]:   Download/Open a remote web page
   //
   // [PARAMETERS]:    $url="", [method="GET"], [$params=""] [$referer=""]
   //
   // [RETURN VALUE]:  downloaded page text if success, "" otherwise
   //
   // [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetUrl($url="", $method="GET", $params="", $referer="")
   {

      $this->htmlContent = "";

      if(empty($url))
      {
         $this->strERR = GetErrorString("INVALID_URL");
         return false;
      }

      $this->proto = "https://";

      // add protocol
      if(! preg_match("/:\/\//", $url))
         $url = $this->proto.$url;

      if(is_array($params))
      {
         $paramString = "";

         foreach($params as $key => $value)
            $paramString .= $key . '=' . $value . '&';

         $paramString = preg_replace("/\&$/","", $paramString);
         $params = $paramString;
      }

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      curl_setopt($ch, CURLOPT_URL,$url);

      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);

      //curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_HEADER, 1);

      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; X11; Linux i686) Opera 7.54  [en]');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      $result = curl_exec($ch);

      curl_close ($ch);

      if(empty($result))
      {
         $this->strERR  = "CANNOT_GET_REMOTE_URL";
         $this->strERR .= curl_error($ch)."\n";
         return false;
      }

      preg_match("/^(.*)\n(.*)$/is", $result,$matches);

      $this->httpHeader  = $matches[1];
      $this->htmlContent = $matches[2];

      //print "\n======== result ===========\n".$this->htmlContent."\n=========================\n";die;
      return $result;
   }


} // end of CTaxiAFreewayEngine class

?>



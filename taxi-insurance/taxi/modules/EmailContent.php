<?php

include_once "globals.inc";
include_once "Template.php";
include_once "YourDetailsElements.php";
include_once "Vehicle.php";
include_once "Postcode.php";
require_once "/home/www/wl-config.quotezone.co.uk/insurance-new/system/core/modules/EmailTemplateHelper.php";


// error_reporting(E_ALL);

$objTmpl       = new CTemplate();
$objPostcode   = new CPostcode();

function GetEmailTemplate($SESSION, $mailFormat="HTML")
{
   global $objTmpl;
   global $objPostcode;
   global $_YourDetails;

   $objVehicle    = new CVehicle();
   $vehMakeArray  = $objVehicle->GetVehicleMake();  


   $tmplPrepare = array();
   $wlName                = trim($SESSION['_QZ_QUOTE_DETAILS_']['settingsWlName']);
   $system                = trim($SESSION['_QZ_QUOTE_DETAILS_']['system']);

   if(isset($SESSION['consent_statements_values']['companyMarketing']))
   {
      $tmplPrepare["header"]                  = prepareEmailTemplateHeader("", $mailFormat, $wlName);
      $tmplPrepare["userContactTitle"]        = prepareEmailTemplateTitle("USER CONTACT PREFERENCES", $mailFormat, $wlName);
      $tmplPrepare["yourDetailsTitle"]        = prepareEmailTemplateTitle("TAXI INSURANCE QUERY DETAILS", $mailFormat, $wlName);
      $tmplPrepare["personalDetailsTitle"]    = prepareEmailTemplateTitle("PERSONAL DETAILS", $mailFormat, $wlName);
      $tmplPrepare["consentStatementsValues"] = prepareContactPreferences($mailFormat, $SESSION['consent_statements_values'], $wlName);
   }
   else
   {
      $tmplPrepare["header"]                  = prepareHeader("TAXI INSURANCE QUERY DETAILS", $mailFormat, $wlName);
      $tmplPrepare["userContactTitle"]        = "";
      $tmplPrepare["yourDetailsTitle"]        = "";
      $tmplPrepare["personalDetailsTitle"]    = "";
      $tmplPrepare["consentStatementsValues"] = ""; 
   }

   $tmplPrepare["address"] = prepareAddress( $SESSION['_YourDetails_']['postcode'], $SESSION['_YourDetails_']['house_number_or_name'], $mailFormat);
   $tmplPrepare["footer_text"] = prepareFooterSection($system, $mailFormat, $wlName);
   $tmplPrepare["email_details"] = getTaxiTemplate($SESSION,$mailFormat);

   //new postcode lookup template update
   $newPostcodeDetails = $objPostcode->GeoPostCodeLookup($SESSION['_YourDetails_']['postcode'], $SESSION['_YourDetails_']['house_number_or_name']);
   $newAddressLine1                              = $newPostcodeDetails['line1']." ".$newPostcodeDetails['line2'];
   $newAddressLine2                              = $newPostcodeDetails['line3'];
   $newAddressLine3                              = $newPostcodeDetails['line4'];

   $businessNamePostcode                           = $newPostcodeDetails ['organisation_name'];
   $tmplPrepare["line1"]                           = $newPostcodeDetails['line1']." ".$newPostcodeDetails['line2'];
   $tmplPrepare ["house_number"]                   = $newPostcodeDetails ['building_name_and_number'];
   $tmplPrepare ["road"]                           = $newPostcodeDetails ['road'];
   $tmplPrepare ["town"]                           = $newPostcodeDetails ['post_town'];
   $tmplPrepare ["county"]                         = $newPostcodeDetails ['county'];

   //standard variable names accross systems
   $tmplPrepare ["houseNumberOrName"] = $houseNumberOrName         = $newPostcodeDetails['building_name_and_number'];
   $tmplPrepare ["addressLine1"]      = $addressLine1              = $newPostcodeDetails['line1'];
   $tmplPrepare ["streetName"]        = $streetName                = $newPostcodeDetails['road'];
   $tmplPrepare ["town"]              = $town                      = $newPostcodeDetails['post_town'];
   $tmplPrepare ["county"]            = $county                    = $newPostcodeDetails['county'];
   $tmplPrepare ["postcode"]          = $postcode                  = $SESSION['_YourDetails_']['postcode'];
   
   $tmplPrepare ["dob"]               = $dob                       = $SESSION['_YourDetails_']['date_of_birth'];
   $tmplPrepare ["dobDD"]             = $dobDD                     = $SESSION['_YourDetails_']['date_of_birth_dd'];
   $tmplPrepare ["dobMM"]             = $dobMM                     = $SESSION['_YourDetails_']['date_of_birth_mm'];
   $tmplPrepare ["dobYYYY"]           = $dobYYYY                   = $SESSION['_YourDetails_']['date_of_birth_yyyy'];
   
   $tmplPrepare ["doq"]               = $doq                       = $SESSION['_YourDetails_']['start_date'];
   $tmplPrepare ["doqDD"]             = $doqDD                     = $SESSION['_YourDetails_']['start_date_dd'];
   $tmplPrepare ["doqMM"]             = $doqMM                     = $SESSION['_YourDetails_']['start_date_mm'];
   $tmplPrepare ["doqYYYY"]           = $doqYYYY                   = $SESSION['_YourDetails_']['start_date_yyyy'];
   
   $tmplPrepare ["dis"]               = $dis                       = $SESSION['_YourDetails_']['date_of_insurance_start'];
   $tmplPrepare ["disDD"]             = $disDD                     = $SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $tmplPrepare ["disMM"]             = $disMM                     = $SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $tmplPrepare ["disYYYY"]           = $disYYYY                   = $SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];
   
   $tmplPrepare ["title"]             = $title                     = $SESSION['_YourDetails_']['title'];
   $tmplPrepare ["firstName"]         = $firstName                 = $SESSION['_YourDetails_']['first_name'];
   $tmplPrepare ["lastName"]          = $lastName                  = $SESSION['_YourDetails_']['surname'];
   $tmplPrepare ["daytimeTelephone"]  = $daytimeTelephone          = $SESSION['_YourDetails_']['daytime_telephone'];
   $tmplPrepare ["mobileTelephone"]   = $mobileTelephone           = $SESSION['_YourDetails_']['mobile_telephone'];
   $tmplPrepare ["email"]             = $email                     = $SESSION['_YourDetails_']['email_address'];   
   $tmplPrepare ["quoteRef"]          = $quoteRef                  = $SESSION['_QZ_QUOTE_DETAILS_']['quote_reference'];
   // end of standard variable names accross systems

   $consentEmail = 'No';
   if(isset($SESSION['consent_statements_values']['companyMarketing']['Email']))
   {
      if($SESSION['consent_statements_values']['companyMarketing']['Email'] == 1)
         $consentEmail = 'Yes';
   }

   $consentSMS = 'No';
   if(isset($SESSION['consent_statements_values']['companyMarketing']['SMS']))
   {
      if($SESSION['consent_statements_values']['companyMarketing']['SMS'] == 1)
         $consentSMS = 'Yes';
   }

   $tmplPrepare["email_consent"] = $consentEmail;
   $tmplPrepare["sms_consent"]   = $consentSMS;

   //prepare the SMS renewal change
   if($SESSION['USER_IP'] == '80.94.200.13')
   {
      $tmplPrepare["sms_taxi_details"] = '<tr>
                                             <td colspan="2" style="font:12px Calibri;background-color:#99ff99;" align="center" valign="middle">
                                                This is a Quotezone SMS lead. Please be sure to verify all details with the user.
                                             </td>
                                          </tr>';
   }
   $tmplPrepare["uber_driver"]          = $_YourDetails["uber_driver"][$SESSION['_YourDetails_']['uber_driver']];
   $tmplPrepare["source_dna"]           = "Quotezone Cabman";
   $tmplPrepare["source_dna_text"]      = "Quotezone Cabman";

   $tmplPrepare["dna_account_id"]       = "7802";

   $tmplPrepare["dob_day_dna"]          = $SESSION['_YourDetails_']['date_of_birth_dd'];
   $tmplPrepare["dob_month_dna"]        = $SESSION['_YourDetails_']['date_of_birth_mm'];
   $tmplPrepare["dob_year_dna"]         = $SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $tmplPrepare["taxi_use"]             = $_YourDetails["taxi_used_for"][$SESSION['_YourDetails_']['taxi_used_for']];
   $tmplPrepare["taxi_use_dna"]         = $SESSION['_YourDetails_']['taxi_used_for'];

   $tmplPrepare["taxi_type"]            = $_YourDetails["taxi_type"][$SESSION['_YourDetails_']['taxi_type']];
   $tmplPrepare["taxi_type_dna"]        = $SESSION['_YourDetails_']['taxi_type'];

   $tmplPrepare["cover_type"]           = $_YourDetails["type_of_cover"][$SESSION['_YourDetails_']['type_of_cover']];
   $tmplPrepare["cover_type_dna"]       = $SESSION['_YourDetails_']['type_of_cover'];

   $tmplPrepare["taxi_make"]            = $vehMakeArray[$SESSION['_YourDetails_']['taxi_make']] ? $vehMakeArray[$SESSION['_YourDetails_']['taxi_make']] : $SESSION['_YourDetails_']['taxi_make'];
   $tmplPrepare["taxi_make_dna"]        = substr(rawurlencode($tmplPrepare["taxi_make"]),0,20);
   $tmplPrepare["taxi_make_dna"]        = str_replace("%20"," ",$tmplPrepare["taxi_make_dna"]);

   $tmplPrepare["taxi_model"]           = $SESSION['_YourDetails_']['taxi_model'];
   $tmplPrepare["taxi_model_dna"]       = substr(rawurlencode($SESSION['_YourDetails_']['taxi_model']),0,20);
   $tmplPrepare["taxi_model_dna"]       = str_replace("%20"," ",$tmplPrepare["taxi_model_dna"]);

   $tmplPrepare["manufacture_year"]     = $SESSION['_YourDetails_']['year_of_manufacture'];
   $tmplPrepare["engine_size"]          = $SESSION['_YourDetails_']['engine_size'];


   $tmplPrepare["estimated_value"]      = $SESSION['_YourDetails_']['estimated_value'];

   $tmplPrepare["taxi_capacity"]        = $_YourDetails["taxi_capacity"][$SESSION['_YourDetails_']['taxi_capacity']];
   $tmplPrepare["taxi_capacity_dna"]    = $SESSION['_YourDetails_']['taxi_capacity'];

   $tmplPrepare["taxi_ncb"]             = $_YourDetails["taxi_ncb"][$SESSION['_YourDetails_']['taxi_ncb']];
   $tmplPrepare["taxi_ncb_dna"]         = $SESSION['_YourDetails_']['taxi_ncb'];

   $tmplPrepare["private_car_ncb"]      = $_YourDetails["private_car_ncb"][$SESSION['_YourDetails_']['private_car_ncb']];


   $tmplPrepare["plating_auth"]         = $SESSION['_YourDetails_']['plating_authority'];
   $platingAuthDna                      = rawurlencode($SESSION['_YourDetails_']['plating_authority']);
   $platingAuthDna                      = str_replace("%20"," ",$platingAuthDna);
   $platingAuthDna                      = str_replace("%28"," ",$platingAuthDna);
   $platingAuthDna                      = str_replace("%2"," ",$platingAuthDna);
   $platingAuthDna                      = str_replace("9"," ",$platingAuthDna);
   $tmplPrepare["plating_auth_dna"]     = substr($platingAuthDna,0,29);


   $tmplPrepare["vehicle_registration"]     = $SESSION['_YourDetails_']['vehicle_registration_number'];
   $tmplPrepare["vehicle_registration_dna"] = str_replace(" ","",$SESSION['_YourDetails_']['vehicle_registration_number']);

   $tmplPrepare["licence_period"]       = $_YourDetails["period_of_licence"][$SESSION['_YourDetails_']['period_of_licence']];
   $tmplPrepare["licence_period_dna"]   = $SESSION['_YourDetails_']['period_of_licence'];

   $tmplPrepare["taxi_badge"]           = $_YourDetails["taxi_badge"][$SESSION['_YourDetails_']['taxi_badge']];
   $tmplPrepare["taxi_badge_dna"]       = $SESSION['_YourDetails_']['taxi_badge'];

   $tmplPrepare["taxi_driver"]          = $_YourDetails["taxi_driver"][$SESSION['_YourDetails_']['taxi_driver']];


   $tmplPrepare["claims_5_years"]          = $_YourDetails["claims_5_years"][$SESSION['_YourDetails_']['claims_5_years']];
   $tmplPrepare["claims_5_years_dna_text"] = str_replace(",","",$_YourDetails["claims_5_years"][$SESSION['_YourDetails_']['claims_5_years']]);


   $tmplPrepare["convictions_5_years"]  = $_YourDetails["convictions_5_years"][$SESSION['_YourDetails_']['convictions_5_years']];

   $claimsLast5Years   = $SESSION['_YourDetails_']['claims_5_years'];
   $tmplPrepare["claims_5_years_dna"]      = "no";
   $tmplPrepare["claims_5_years_dna_mail"] = "No";

   if($claimsLast5Years == "Yes")
   {
      $tmplPrepare["claims_5_years_dna"]      = "yes";
      $tmplPrepare["claims_5_years_dna_mail"] = "Yes";
   }

   $convictionsLast5Years   = $SESSION['_YourDetails_']['convictions_5_years'];
   $tmplPrepare["convictions_5_years_dna"]      = "No";
   $tmplPrepare["convictions_5_years_dna_mail"] = "No";
   if($convictionsLast5Years == "Yes")
   {
      $tmplPrepare["convictions_5_years_dna"]      = "Yes";
      $tmplPrepare["convictions_5_years_dna_mail"] = "Yes";

   }

   $tmplPrepare["gap_insurance"]        = $_YourDetails["gap_insurance"][$SESSION['_YourDetails_']['gap_insurance']];
   $tmplPrepare["gap_insurance_dna"]    = $SESSION['_YourDetails_']['gap_insurance'];


   $tmplPrepare["title"]                = $SESSION["_YourDetails_"]["title"];
   $tmplPrepare["title_dna"]            = $SESSION['_YourDetails_']['title']." ".$SESSION['_YourDetails_']['surname'];

   $tmplPrepare["first_name"]           = $SESSION["_YourDetails_"]["first_name"];
   $tmplPrepare["surname"]              = $SESSION["_YourDetails_"]["surname"];

   $tmplPrepare["firstNameSurename"]    = $SESSION["_YourDetails_"]["title"]." ".$SESSION["_YourDetails_"]["first_name"]." ".$SESSION["_YourDetails_"]["surname"];
   $tmplPrepare["firstName_dna"]        = $SESSION["_YourDetails_"]["first_name"];

   $tmplPrepare["sex"]                         = "Male";
   if($SESSION['_YourDetails_']['title'] != "Mr")
      $tmplPrepare["sex"]                      = "Female";

   $tmplPrepare["daytime_telephone"]    = $SESSION["_YourDetails_"]["daytime_telephone"];
   $tmplPrepare["mobile_telephone"]     = $SESSION["_YourDetails_"]["mobile_telephone"];


   //prepare tel for DNA - if one missing - put in the other one
   $tmplPrepare["daytime_telephone_dna"]    = $SESSION["_YourDetails_"]["daytime_telephone"];
   if($SESSION["_YourDetails_"]["daytime_telephone"] == "")
      $tmplPrepare["daytime_telephone_dna"] = $SESSION["_YourDetails_"]["mobile_telephone"];

   $tmplPrepare["mobile_telephone_dna"]     = $SESSION["_YourDetails_"]["mobile_telephone"];
   if($SESSION["_YourDetails_"]["mobile_telephone"] == "")
      $tmplPrepare["mobile_telephone_dna"]  = $SESSION["_YourDetails_"]["daytime_telephone"];



   $tmplPrepare["best_day_call"]        = '';
   $tmplPrepare["best_day_call_dna"]    = '';

   $tmplPrepare["best_time_call"]       = $_YourDetails["best_time_call"][$SESSION['_YourDetails_']['best_time_call']];
   $tmplPrepare["best_time_call_dna"]   = $SESSION['_YourDetails_']['best_time_call'];

   $tmplPrepare["email_address"]        = $SESSION["_YourDetails_"]["email_address"];
   $tmplPrepare["postcode"]             = $SESSION["_YourDetails_"]["postcode"];
   $tmplPrepare["house_number_or_name"] = $SESSION["_YourDetails_"]["house_number_or_name"];

   $tmplPrepare["email_address_dna"]          = substr($SESSION['_YourDetails_']['email_address'],0,49);
   //$tmplPrepare["email_address_dna"]          = str_replace("%20"," ",$tmplPrepare["email_address_dna"]);

   $houseNumberOrName                         = $SESSION['_YourDetails_']['house_number_or_name'];
   $addressLine1                              = $SESSION['_YourDetails_']['address_line1'];
   $addressLine2                              = $SESSION['_YourDetails_']['address_line2'];
   $addressLine3                              = $SESSION['_YourDetails_']['address_line3'];
   $addressLine4                              = $SESSION['_YourDetails_']['address_line4'];

   $houseNumberOrName                         = str_replace("$addressLine1","",$houseNumberOrName);
   $houseNumberOrName                         = str_replace("$addressLine2","",$houseNumberOrName);
   $houseNumberOrName                         = str_replace("$addressLine3","",$houseNumberOrName);
   $houseNumberOrName                         = str_replace("$addressLine4","",$houseNumberOrName);

   $houseNumberOrNameExploded                 = explode(" ", $houseNumberOrName);
   foreach($houseNumberOrNameExploded as $key => $value)
   {
      if(trim($value) == "")
      {
         unset($houseNumberOrNameExploded[$key]);
      }
   }
   $houseNumberOrNameExploded = array_values($houseNumberOrNameExploded);
   $houseNumberOrName                         = $houseNumberOrNameExploded[0];

   $tmplPrepare["house_number_or_name_dna"]   = substr(rawurlencode($houseNumberOrName),0,29);
   $tmplPrepare["house_number_or_name_dna"]   = str_replace("%20"," ",$tmplPrepare["house_number_or_name_dna"]);

   $tmplPrepare["address_line1_dna"]          = substr(rawurlencode($newPostcodeDetails['line1']),0,29);
   $tmplPrepare["address_line1_dna"]          = str_replace("%20"," ",$tmplPrepare["address_line1_dna"]);

   $tmplPrepare["address_line2_dna"]          = substr(rawurlencode($newPostcodeDetails['line2']),0,29);
   $tmplPrepare["address_line2_dna"]          = str_replace("%20"," ",$tmplPrepare["address_line2_dna"]);

   $tmplPrepare["address_line3_dna"]          = substr(rawurlencode($newPostcodeDetails['line3']),0,29);
   $tmplPrepare["address_line3_dna"]          = str_replace("%20"," ",$tmplPrepare["address_line3_dna"]);

   $tmplPrepare["address_line4_dna"]          = substr(rawurlencode($SESSION['_YourDetails_']['address_line4']),0,19);
   $tmplPrepare["address_line4_dna"]          = str_replace("%20"," ",$tmplPrepare["address_line4_dna"]);


   $tmplPrepare["address_line1"]        = $newAddressLine1;
   $tmplPrepare["address_line2"]        = $newAddressLine2;
   $tmplPrepare["address_line3"]        = $newAddressLine3;
   $tmplPrepare["address_line4"]        = $SESSION["_YourDetails_"]["address_line4"];
   $tmplPrepare["date_of_birth"]        = $SESSION["_YourDetails_"]["date_of_birth"];
   $tmplPrepare["date_time"]            = date("Y/m/d H:i:s");
   $tmplPrepare["date_of_insurance"]    = $SESSION['_YourDetails_']['date_of_insurance_start_dd']."/".$SESSION['_YourDetails_']['date_of_insurance_start_mm']."/".$SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];
   $tmplPrepare["date_of_insurance_dd_DNA"]    = $SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $tmplPrepare["date_of_insurance_mm_DNA"]    = $SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $tmplPrepare["date_of_insurance_yyyy_DNA"]    = $SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $tmplPrepare["quote_reference"]      = $SESSION["_QZ_QUOTE_DETAILS_"]["quote_reference"];

   if($mailFormat == "INSURANCECHOICEXML" OR $mailFormat == "INSURANCECHOICEXML2")
   {
       $tmplPrepare["plating_auth"]         = $SESSION['_YourDetails_']['plating_authority'];
       $tmplPrepare["plating_auth"]         = str_replace("&","AND",$tmplPrepare["plating_auth"]);

      $tmplPrepare["house_number_or_name"] = $SESSION["_YourDetails_"]["house_number_or_name"];
      $tmplPrepare["house_number_or_name"] = str_replace("&","AND",$tmplPrepare["house_number_or_name"]);
   }

   if($mailFormat == "DNA2")
   {
      $tmplPrepare["source_dna"]             = "Quotezone Taxi";
      $tmplPrepare["source_dna_text"]        = "Quotezone Taxi";
   }

   if($mailFormat == "DNA_TEXT4")
   {
      $tmplPrepare["source_dna"]             = "QZ1";
      $tmplPrepare["source_dna_text"]        = "QZ1";
   }

    if($mailFormat == "DNA3" OR $mailFormat == "DNA_TEXT3")
   {
      $tmplPrepare["source_dna"]            = "Quotezone Cwood";
      $tmplPrepare["source_dna_text"]    = "Quotezone Cwood";
   }

    if($mailFormat == "DNASEO" OR $mailFormat == "TEXTDNASEO" OR $mailFormat == "DNA_TEXT_SEO")
   {
      $tmplPrepare["source_dna"]            = "SEO";
      $tmplPrepare["source_dna_text"]    = "SEO";
   }

    if($mailFormat == "DNASEO_SYQ" OR $mailFormat == "DNA_XML_LYQ" OR $mailFormat == "TEXTDNASEO_LYQ" OR $mailFormat == "DNA_TEXT_SEO_LYQ")
   {
      $tmplPrepare["source_dna"]            = "LYQ";
      $tmplPrepare["source_dna_text"]    = "LYQ";
   }


   if($mailFormat == "DNA_TEXT_SEO" OR  $mailFormat == "DNA_TEXT_SEO_LYQ")
   {
      $tmplPrepare["cost"] = "0";
   }
   elseif($mailFormat == "DNA_TEXT" OR $mailFormat == "DNA_TEXT3")
   {
      $tmplPrepare["cost"] = "4.50";
   }

   
   if($mailFormat == "DNA_TEXT2")
   {
      $tmplPrepare["source_dna"]         = "Quotezone Taxi";
      $tmplPrepare["source_dna_text"]    = "Quotezone Taxi";
   }
   
   $tmplPrepare["cost"] = "5.00";

   if($mailFormat == "DNA_NEW_BADGE")
   {
      $tmplPrepare["source_dna"]         = "QZ2";
      $tmplPrepare["source_dna_text"]    = "QZ2";
      $tmplPrepare["cost"] = "4.00";
   }


   $businessNamePostcode = $newPostcodeDetails['organisation_name'];
   $tmplPrepare["town"] = $newPostcodeDetails['post_town'];
   $tmplPrepare["county"] = $newPostcodeDetails['county'];
   //set dynamic postcode fields
   if(!empty($businessNamePostcode))
   {
      if($mailFormat == 'HTML' )
      {
         $tmplPrepare['business_name_postcode'] = '
          <tr style="[# row_style #]">
            <td width="300">BUSINESS NAME</td>
            <td width="300">'.$businessNamePostcode.'</td>
         </tr>
         ';
      }elseif($mailFormat == 'TEXT')
      {
         $tmplPrepare['business_name_postcode'] = 'BUSINESS NAME : '.$businessNamePostcode;
      }
   }

   if(!empty($newAddressLine2))
   {
      if($mailFormat == 'HTML')
      {
         $tmplPrepare['new_address_line3'] = '
          <tr style="[# row_style #]">
            <td width="300">ADDRESS LINE 2</td>
            <td width="300">'.$newAddressLine2.'</td>
         </tr>
         ';
      }elseif($mailFormat == 'TEXT')
      {
         $tmplPrepare['new_address_line3'] = 'ADDRESS LINE 2 : '.$newAddressLine2;
      }
   }

   if(!empty($newAddressLine3))
   {
      if($mailFormat == 'HTML')
      {
         $tmplPrepare['new_address_line4'] = '
          <tr style="[# row_style #]">
            <td width="300">ADDRESS LINE 3</td>
            <td width="300">'.$newAddressLine3.'</td>
         </tr>
         ';
      }elseif($mailFormat == 'TEXT' || $mailFormat == 'RAINBOW_CUSTOM' || $mailFormat == 'TEXTTHINK')
      {
         $tmplPrepare['new_address_line4'] = 'ADDRESS LINE 3 : '.$newAddressLine3;
      }
   }

   // 347 => "PlanInsurance-TaxiAAABQuotezoneTaxiNBCB",
   // 1085 => "PlanInsurance-TaxiNBQuotezoneC2OfficeHours",
   // 2062 => "PlanInsurance-TaxiNBQuotezoneC1OutOfHours",
   // 2063 => "PlanInsurance-TaxiNBQuotezoneC2OutOfHours",
   // 2952 => "PlanInsurance-TaxiNBQuotezoneBCOfficeHours",
   // 3293 => "PlanInsurance-TaxiNBQuotezoneBCOutOfOfficeHours",
   // 3430 => "PlanInsurance-TaxiNBQuotezoneC3OfficeHours",
   // 3431 => "PlanInsurance-TaxiNBQuotezoneC3OutOfHours",
   // Plan Insurance - Taxi (CHAUFFEUR 15K VEHICLE C4) OH (taxi)  12644 
   // Plan Insurance - Taxi (CHAUFFEUR 15K VEHICLE C4) OOH (taxi) 12645 
   // Plan Insurance - Taxi (PRIVH 12 VEHICLE C4) OH (taxi) 12642 
   // Plan Insurance - Taxi (PRIVH 12 VEHICLE C4) OOH (taxi)   12647 
   // Plan Insurance - Taxi (PUBH 7K VEHICLE C4) OH (taxi)  12643 
   // Plan Insurance - Taxi (PUBH 7K VEHICLE C4) OOH (taxi) 12646
   
   $chkPlanMailFormat = strpos($mailFormat, "PLAN_XML_");

   if($chkPlanMailFormat !== false)
   {
      preg_match('/\d(.*)/', $mailFormat, $matchId);
      $planInsuranceArr = array(                      
                           347 => "AAAB - Quotezone Taxi NB CB",
                           1085 => "TAXI NB QUOTEZONE C2 OFFICE HOURS",
                           2062 => "TAXI NB QUOTEZONE C1 OUT OF HOURS",
                           2063 => "TAXI NB QUOTEZONE C2 OUT OF HOURS",
                           2952 => "TAXI NB QUOTEZONE BC OFFICE HOURS",
                           3293 => "TAXI NB QUOTEZONE BC OUT OF OFFICE HOURS",
                           3430 => "TAXI NB QUOTEZONE C3 OFFICE HOURS",
                           3431 => "TAXI NB QUOTEZONE C3 OUT OF HOURS",
                           11307 => "TAXI NB QUOTEZONE BC WEEKEND",
                           11309 => "TAXI NB QUOTEZONE C1 WEEKEND",
                           11305 => "TAXI NB QUOTEZONE C2 WEEKEND",
                           11306 => "TAXI NB QUOTEZONE C3 WEEKEND",
                           12644 => "TAXI NB QUOTEZONE C4",
                           12645 => "TAXI NB QUOTEZONE C4",
                           12642 => "TAXI NB QUOTEZONE C4",
                           12647 => "TAXI NB QUOTEZONE C4",
                           12643 => "TAXI NB QUOTEZONE C4",
                           12646 => "TAXI NB QUOTEZONE C4",
                        );

      $tmplPrepare["DOB"]            = $SESSION['_YourDetails_']['date_of_birth_yyyy']."-".$SESSION['_YourDetails_']['date_of_birth_mm']."-".$SESSION['_YourDetails_']['date_of_birth_dd'];
      $tmplPrepare["DIS"]            = $SESSION['_YourDetails_']["date_of_insurance_start_yyyy"]."-".$SESSION['_YourDetails_']["date_of_insurance_start_mm"]."-".$SESSION['_YourDetails_']["date_of_insurance_start_dd"]; 
      $tmplPrepare["house_number_or_name"] = $SESSION["_YourDetails_"]["house_number_or_name"];
      $tmplPrepare["house_number_or_name"] = str_replace("&"," and ",$tmplPrepare["house_number_or_name"]);
      $tmplPrepare["address"]        = prepareAddress( $SESSION['_YourDetails_']['postcode'], $tmplPrepare["house_number_or_name"], 'TEXT');
      $tmplPrepare["plating_auth"]   = $SESSION['_YourDetails_']['plating_authority'];
      $tmplPrepare["plating_auth"]   = str_replace("&"," and ",$tmplPrepare["plating_auth"]);
      $tmplPrepare["taxi_type"]      = str_replace("&"," and ",$_YourDetails["taxi_type"][$SESSION['_YourDetails_']['taxi_type']]);
      $tmplPrepare["cover_type"]     = str_replace("&"," and ",$_YourDetails["type_of_cover"][$SESSION['_YourDetails_']['type_of_cover']]);
      $tmplPrepare["licence_period"] = str_replace("&"," and ",$_YourDetails["period_of_licence"][$SESSION['_YourDetails_']['period_of_licence']]);
      $tmplPrepare["taxi_badge"]     = str_replace("&"," and ",$_YourDetails["taxi_badge"][$SESSION['_YourDetails_']['taxi_badge']]);
      $tmplPrepare["taxi_driver"]    = str_replace("&"," and ",$_YourDetails["taxi_driver"][$SESSION['_YourDetails_']['taxi_driver']]);
      $tmplPrepare["uber_driver"]    = $_YourDetails["uber_driver"][$SESSION['_YourDetails_']['uber_driver']];

      $consentEmail = '0';
      if($SESSION['consent_statements_values']['companyMarketing']['Email'] == 1)
         $consentEmail = '1';

      $consentSMS = '0';
      if($SESSION['consent_statements_values']['companyMarketing']['SMS'] == 1)
         $consentSMS = '1';

      $tmplPrepare["email_consent"] = $consentEmail;
      $tmplPrepare["sms_consent"]   = $consentSMS;
      
      foreach($planInsuranceArr as $keyId => $planCampaignSource)
         {
            

            if($matchId[0] == $keyId)
               $tmplPrepare["campaignSource"]   = $planCampaignSource;         
         }

         $mailFormat = "PLAN_XML";
      }
  
      $tmplPrepare["date_complete"]          =  date("Y/m/d h:i:s");  

   switch($mailFormat)
   {
      case "DNA":
      case "DNA2":
      case "DNA3":
      case "DNASEO":
      case "DNASEO_SYQ":
      case "HTML":
      case "RAINBOW_CUSTOM"://moved to standard html
         $templateName = "EmailTemplate.tmpl";
      break;

      case "TEXT":
      case "TEXTDNASEO":
      case "TEXTDNASEO_LYQ":
         $templateName = "EmailTemplateText.tmpl";
      break;

      case "ALLEN_TXT_1671":
         $templateName = "EmailTemplateAllenText.tmpl";
      break;

      case "COMPLETE_COVER_TXT_11826":
         $templateName = "EmailTemplateAllenText.tmpl";
      break;


      case "DNA_TEXT":
      case "DNA_NEW_BADGE":
      case "DNA_TEXT2":
      case "DNA_TEXT3":
      case "DNA_TEXT4":
      case "DNA_TEXT_SEO":
      case "DNA_TEXT_SEO_LYQ":
         $templateName = "EmailTemplateDNAText.tmpl";
      break;

      case "DNA_XML":
      case "DNA_XML2":
      case "DNA_XML3":
      case "DNA_XML_LYQ":
            $templateName = "EmailTemplateDNAXml.tmpl";
      break;

      case "INSURANCECHOICEXML":
         $templateName = "EmailTemplateInsuranceChoiceCustomXml.tmpl";
      break;

      case "INSURANCECHOICEXML2":
         $templateName = "EmailTemplateInsuranceChoice2CustomXml.tmpl";
      break;

      case "PLAN_XML":
         $templateName = "EmailTemplatePlanXML.tmpl";
      break;

      case "JMB_TXT_11257":
      case "JMB_TXT_12382":
         $templateName = "EmailTemplateJMBText.tmpl";
      break;

      default:
         $templateName = "EmailTemplate.tmpl";
      break;
   }

   if(! $objTmpl->Load($templateName))
      print $objTmpl->GetError();

   $objTmpl->Prepare($tmplPrepare);

   $message_body = trim($objTmpl->GetContent());

   $message_body = setEmailStyles($message_body);


   return $message_body;
}


function getTaxiTemplate($SESSION,$mailFormat)
{       
   global $objTmpl;
   global $objPostcode;
   global $_YourDetails;

   $objVehicle = new CVehicle();
   $vehMakeArray = $objVehicle->GetVehicleMake();

   $TaxiDetailsHtml = "";
   $TaxiDetailsTxt  = "";  

   $registrationNumberQuestion = $SESSION['_YourDetails_']['registration_number_question'] == "Y"?"Yes":"No";

   // DO YOU KNOW THE REGISTRATION NUMBER?
   $TaxiDetailsHtml .= getHtmlTableRow("DO YOU KNOW THE REGISTRATION NUMBER?", $SESSION['_YourDetails_']['registration_number_question'] == "Y"?"Yes":"No");
   $TaxiDetailsTxt .= "\n DO YOU KNOW THE REGISTRATION NUMBER? : ".$registrationNumberQuestion;

   //REGISTRATION NUMBER
   $TaxiDetailsHtml .= getHtmlTableRow("REGISTRATION NUMBER", $SESSION['_YourDetails_']['vehicle_registration_number']);
   $TaxiDetailsTxt .= "\n REGISTRATION NUMBER : ".$SESSION['_YourDetails_']['vehicle_registration_number'];

    //TAXI MAKE
   $TaxiDetailsHtml .= getHtmlTableRow("TAXI MAKE", $vehMakeArray[$SESSION['_YourDetails_']['taxi_make']] ? $vehMakeArray[$SESSION['_YourDetails_']['taxi_make']] : $SESSION['_YourDetails_']['taxi_make']);
   $TaxiDetailsTxt .= "\n TAXI MAKE : ".$vehMakeArray[$SESSION['_YourDetails_']['taxi_make']] ? $vehMakeArray[$SESSION['_YourDetails_']['taxi_make']] : $SESSION['_YourDetails_']['taxi_make'];

   //TAXI MODEL
   $TaxiDetailsHtml .= getHtmlTableRow("TAXI MODEL", $SESSION['_YourDetails_']['taxi_model']);
   $TaxiDetailsTxt .= "\n TAXI MODEL : ".$SESSION['_YourDetails_']['taxi_model'];

   //YEAR OF MANUFACTURE
   $TaxiDetailsHtml .= getHtmlTableRow("YEAR OF MANUFACTURE", $SESSION['_YourDetails_']['year_of_manufacture']);
   $TaxiDetailsTxt .= "\n YEAR OF MANUFACTURE : ".$SESSION['_YourDetails_']['year_of_manufacture'];

   //ENGINE SIZE
   $TaxiDetailsHtml .= getHtmlTableRow("ENGINE SIZE", $SESSION['_YourDetails_']['engine_size']);
   $TaxiDetailsTxt .= "\n ENGINE SIZE : ".$SESSION['_YourDetails_']['engine_size'];

   // ESTIMATED TAXI VALUE (£)
   $TaxiDetailsHtml .= getHtmlTableRow("ESTIMATED TAXI VALUE (&pound;)", $SESSION['_YourDetails_']['estimated_value']);
   $TaxiDetailsTxt .= "\n ESTIMATED TAXI VALUE (£) : ".$SESSION['_YourDetails_']['estimated_value'];

   // TAXI TYPE
   $TaxiDetailsHtml .= getHtmlTableRow("TAXI TYPE", $_YourDetails["taxi_type"][$SESSION['_YourDetails_']['taxi_type']]);
   $TaxiDetailsTxt .= "\n TAXI TYPE : ".$_YourDetails["taxi_type"][$SESSION['_YourDetails_']['taxi_type']];

   // MAIN TAXI USE
   $TaxiDetailsHtml .= getHtmlTableRow("MAIN TAXI USE", $_YourDetails["taxi_used_for"][$SESSION['_YourDetails_']['taxi_used_for']]);
   $TaxiDetailsTxt .= "\n MAIN TAXI USE : ".$_YourDetails["taxi_used_for"][$SESSION['_YourDetails_']['taxi_used_for']]; 

   // ARE YOU AN UBER DRIVER?
      // Display if  'taxi use' = Private Hire, Chauffeur
   if($SESSION['_YourDetails_']['taxi_used_for'] == "1" || $SESSION['_YourDetails_']['taxi_used_for'] == "3")
   {
      $TaxiDetailsHtml .= getHtmlTableRow("ARE YOU AN UBER DRIVER?", $_YourDetails['uber_driver'][$SESSION['_YourDetails_']['uber_driver']]);
      $TaxiDetailsTxt .= "\n ARE YOU AN UBER DRIVER? : ".$_YourDetails['uber_driver'][$SESSION['_YourDetails_']['uber_driver']];
   }

   // MAXIMUM NUMBER OF PASSENGERS
   $TaxiDetailsHtml .= getHtmlTableRow("MAXIMUM NUMBER OF PASSENGERS", $_YourDetails["taxi_capacity"][$SESSION['_YourDetails_']['taxi_capacity']]);
   $TaxiDetailsTxt .= "\n MAXIMUM NUMBER OF PASSENGERS : ".$_YourDetails["taxi_capacity"][$SESSION['_YourDetails_']['taxi_capacity']];

   // TAXI LICENSING AUTHORITY
   $TaxiDetailsHtml .= getHtmlTableRow("TAXI LICENSING AUTHORITY", $SESSION['_YourDetails_']['plating_authority']);
   $TaxiDetailsTxt .= "\n TAXI LICENSING AUTHORITY : ".$SESSION['_YourDetails_']['plating_authority'];

   //TYPE OF COVER
   $TaxiDetailsHtml .= getHtmlTableRow("TYPE OF COVER", $_YourDetails["type_of_cover"][$SESSION['_YourDetails_']['type_of_cover']]);
   $TaxiDetailsTxt .= "\n TYPE OF COVER : ".$_YourDetails["type_of_cover"][$SESSION['_YourDetails_']['type_of_cover']];

   //TAXI NO CLAIMS BONUS
   $TaxiDetailsHtml .= getHtmlTableRow("TAXI NO CLAIMS BONUS", $_YourDetails["taxi_ncb"][$SESSION['_YourDetails_']['taxi_ncb']]);
   $TaxiDetailsTxt .= "\n TAXI NO CLAIMS BONUS : ".$_YourDetails["taxi_ncb"][$SESSION['_YourDetails_']['taxi_ncb']];

   //PRIVATE CAR NO CLAIMS BONUS
   $TaxiDetailsHtml .= getHtmlTableRow("PRIVATE CAR NO CLAIMS BONUS", $_YourDetails["private_car_ncb"][$SESSION['_YourDetails_']['private_car_ncb']]);
   $TaxiDetailsTxt .= "\n PRIVATE CAR NO CLAIMS BONUS : ".$_YourDetails["private_car_ncb"][$SESSION['_YourDetails_']['private_car_ncb']];
 
   //FULL UK LICENCE
   $TaxiDetailsHtml .= getHtmlTableRow("FULL UK LICENCE", $_YourDetails["period_of_licence"][$SESSION['_YourDetails_']['period_of_licence']]);
   $TaxiDetailsTxt .= "\n FULL UK LICENCE : ".$_YourDetails["period_of_licence"][$SESSION['_YourDetails_']['period_of_licence']];

   //TAXI BADGE
   $TaxiDetailsHtml .= getHtmlTableRow("TAXI BADGE", $_YourDetails["taxi_badge"][$SESSION['_YourDetails_']['taxi_badge']]);
   $TaxiDetailsTxt .= "\n TAXI BADGE : ".$_YourDetails["taxi_badge"][$SESSION['_YourDetails_']['taxi_badge']];

   //TAXI DRIVER(S)
   $TaxiDetailsHtml .= getHtmlTableRow("TAXI DRIVER(S)", $_YourDetails["taxi_driver"][$SESSION['_YourDetails_']['taxi_driver']]);
   $TaxiDetailsTxt .= "\n TAXI DRIVER(S) : ".$_YourDetails["taxi_driver"][$SESSION['_YourDetails_']['taxi_driver']];

   //CLAIMS LAST 5 YEARS
   $TaxiDetailsHtml .= getHtmlTableRow("CLAIMS LAST 5 YEARS", $_YourDetails["claims_5_years"][$SESSION['_YourDetails_']['claims_5_years']]);
   $TaxiDetailsTxt .= "\n CLAIMS LAST 5 YEARS : ".$_YourDetails["claims_5_years"][$SESSION['_YourDetails_']['claims_5_years']];

   //MOTORING CONVICTIONS LAST 5 YEARS  
   $TaxiDetailsHtml .= getHtmlTableRow("MOTORING CONVICTIONS LAST 5 YEARS", $_YourDetails["convictions_5_years"][$SESSION['_YourDetails_']['convictions_5_years']]);
   $TaxiDetailsTxt .= "\n MOTORING CONVICTIONS LAST 5 YEARS : ".$_YourDetails["convictions_5_years"][$SESSION['_YourDetails_']['convictions_5_years']];

   

   if($mailFormat == "HTML")
      return $TaxiDetailsHtml;
   else if($mailFormat == "TEXT")
      return $TaxiDetailsTxt;

}

?>

<?php


#########################
#                       #
#     TARVEL SECTION      #
#                       #
#########################

// SQL TABLE DEFINITIONS

//AFFILIATES SYSTEM
define("SQL_COOKIE_STATS", "cookie_stats"); // Cookie stats table
define("SQL_COOKIES", "cookies"); // Cookies table
define("SQL_AFFILIATES", "affiliates"); // Affiliate table
define("SQL_USERS", "users"); // User table
define("SQL_QZQUOTE_AFFILIATES", "qzquote_affiliates"); // Quotezone affiliates table

// QUOTE SYSTEM
define("SQL_QZQUOTE_TYPES", "ci_new.qzquote_types"); // QzQuotes types table
define("SQL_QZQUOTES", "qzquotes"); // QzQuotes table
define("SQL_LOGS", "logs"); // Logs table
define("SQL_QUOTE_STATUS", "quote_status"); // Quote status table
define("SQL_QUOTE_ERRORS", "quote_errors"); // Quote errors table
define("SQL_QUOTES", "quotes"); //  Quotes table
define("SQL_QUOTE_DETAILS", "quote_details");// Quote Details table

// QUOTE MAIL SYSTEM
define("SQL_SKIP_QUOTE_MAIL_INFO", "skip_quote_mail_info");// Skip quote mail info table
define("SQL_MAIL_INFO", "mail_info"); // Mail info table
define("SQL_EMAIL_STORE", "email_store"); // Email store table
define("SQL_NEWS_EMAIL_CAMPAIGNS", "news_email_campaigns"); // News email campaings table

// QUOTE USER SYSTEM
define("SQL_QUOTE_USER_DETAILS", "breakdown_quote_user_details"); // Quote user details table
define("SQL_QUOTE_USERS", "quote_users"); //  Quote users table

// QUOTE SCANNINGS SYSTEM
define("SQL_QUOTE_SCANNINGS", "quote_scannings"); // Quote scannings table

//ESCAPE QUOTE MAIL
define("SQL_EMAIL_STORE", "email_store"); // Email Store
define("SQL_QUOTE_EMAIL_INFO", "skip_quote_mail_info"); // Skip quote email info

//SITE SYSTEM
define("SQL_SESSION_PARAMS", "ci_new.session_params"); // Session params table
define("SQL_SITES", "ci_new.sites"); // Sites table
define("SQL_URL_PARAMS", "ci_new.url_params");// Url params table
define("SQL_URLS", "ci_new.urls"); // Urls table
define("SQL_BUSINESS_CODES", "ci_new.EMP_CODES"); // Business codes table
define("SQL_OCCUPATION_CODES", "ci_new.OCC_CODES"); // Occupation codes table
define("SQL_PCODE_DETAILS", "ci_new.pcode_details"); // Postcode details table
define("SQL_PCODE_STREETS", "ci_new.u_pcode_streets"); // Postcode streets table
define("SQL_POST_CODES", "ci_new.post_codes"); // Postcode table
define("SQL_QUIT", "quit"); // Quit messages table
define("SQL_HELP", "ci_new.travel_help"); // travel help table
define("SQL_TELL_A_FRIEND", "tell_a_friend"); // Tell a friend
define("SQL_NEWS_LETTER", "news_letter");// News letter table

// QUOTE TRACKING SYSTEM
define("SQL_TRACK_URLS", "track_urls"); // Track urls table
define("SQL_TRACKER", "tracker"); // Tracker table
define("SQL_TRACKER_DETAILS", "tracker_details"); // Tracker details table
define("SQL_TRACKING_HOSTS", "tracking_hosts"); // Tracking hosts table
define("SQL_TRACKING_PATHS", "tracking_paths"); // Tracking paths table
define("SQL_TRACKING_QUERIES", "tracking_queries"); // Tracking queries table
define("SQL_TRACKING_SCHEMES", "tracking_schemes"); // Tracking schemes table
define("SQL_TRACKING_SOURCE", "tracking_source"); // Tracking source table
define("SQL_TRACKING_SOURCE_NAMES", "tracking_source_names"); // Tracking source names table
define("SQL_TRACKINGS", "trackings"); // Trackings table
define("SQL_TRACKINGS_INOUT", "trackings_inout"); // Trackings inout table


// QUOTE STEP TRANCKINGS
define("SQL_STEP_TRACKINGS", "step_trackings"); // Trackings inout table
define("SQL_OVERTURE_TRACKINGS", "overture_tracker"); // OvertureTrackings inout table

// USERS SITES LOGIN
define("SQL_SITES_USERS_LOGIN", "sites_users_login"); // Trackings inout table

// QUOTE STATS
define("SQL_QUOTE_STATS", "quote_stats"); // Quote Stats table
define("SQL_QUOTE_STATS_DETAILS", "quote_stats_details"); // Quote Stats Details table

// TRAVEL EXCEL TABLES
define("SQL_TRAVEL_PARTIES_TABLE", "ci_new.travel_parties");
define("SQL_TRAVEL_TRIPS_TABLE","ci_new.travel_trips");
define("SQL_TRAVEL_DESTINATIONS_TABLE","ci_new.travel_destinations");
define("SQL_TRAVEL_PRICES_TABLE","ci_new.travel_excel_prices");
define("SQL_TRAVEL_AGE_INTERVALS_TABLE","ci_new.travel_age_intervals");
define("SQL_TRAVEL_RULES_AGE_INTERVAL_TABLE","ci_new.travel_rules_age_int");
define("SQL_TRAVEL_RULES_TABLE","ci_new.travel_rules");


define("SQL_BREAKDOWN_COVER_DETAILS_TABLE", "ci_new.breakdown_cover_details");
define("SQL_BREAKDOWN_PRICES_TABLE", "ci_new.breakdown_prices");


?>

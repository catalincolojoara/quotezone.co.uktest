<?php
   // Ignore user aborts and allow the script to run
   ignore_user_abort(true);

   //force redirecting to https
   if($_SERVER['SERVER_PORT'] == '80')
   {
      $redirectUrl = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      redirect($redirectUrl);
   }

   session_start();

   $_SESSION["USER_IP"] = $_SERVER['REMOTE_ADDR'];

   if(empty($_GET['sid']) AND $_COOKIE['AFFID'] == "J7E6G4-0018" AND $_GET['taxi_make'] != "" AND $_GET['taxi_model'] != "" AND $_GET['estimated_value'] != "")
   {
      $_SESSION['_YourDetails_']['taxi_make']       = $_GET['taxi_make'];
      $_SESSION['_YourDetails_']['taxi_model']      = $_GET['taxi_model'];
      $_SESSION['_YourDetails_']['estimated_value'] = $_GET['estimated_value'];

      redirect('index.php');
   }

   // Add server ip to session to be used in Outbound class
   $_SESSION["_QZ_QUOTE_DETAILS_"]["SERVER_IP"] = $_SERVER["SERVER_ADDR"];

   //testing sites
    if(! empty($_GET['site']))
    {

        $_SESSION['TESTING SITE ID'] = $_GET['site'];

        $_SESSION['TESTING SITE FAKE'] = '1';

        if(! empty($_GET['live']))
        {
            $_SESSION['TESTING SITE FAKE'] = 0;
        }

        if(! defined('TESTING_MODE'))
           define("TESTING_MODE", "1");

    }

   if($_SESSION['TESTING SITE ID'])
       if(! defined('TESTING_MODE'))
          define("TESTING_MODE", "1");

   // keep user agent into session file
   $_SESSION['HTTP_USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'];


   include_once "modules/globals.inc";
   include_once "Offline.php";

   $objOffline = new COffline();

   if($objOffline->IsOffline("taxi"))
   {
      $offlineMessage = $objOffline->GetOfflineMessage();

      $objOffline->SetOfflineWebsite($offlineMessage);
   }


   include_once "Navigation.php";
   include_once "WebSite.php";

   include_once "Template.php";
   include_once "QzProtection.php";
   include_once "QzQuoteType.php";
   include_once "StepTrackings.php";

   error_reporting(0);


   $objQzQuoteType     = new CQzQuoteType();
   $objStepTrackings   = new CStepTrackings();
   $objMenu            = new CNavigation();

   //$quoteTypeID = $objQzQuoteType->GetQuoteTypeIDByPath();
   $quoteTypeID = "18";

   if(! empty($_GET['step']))
   {
      $_SESSION['ACTIVE STEP'] = $_GET['step'];
      redirect("index.php");
   }

   if(! empty($_GET['sid']))
   {
      redirect("steps/Refill.php?sid=".$_GET['sid']);
      exit(0);
   }

   if(! empty($_GET['crsid']))
   {
      redirect("steps/Crossfill.php?crsid=".$_GET['crsid']);
      exit(0);
   }

   // redirect to static html page
   if(! empty($_GET['staticLink']))
   {
      $_SESSION['STATIC LINK'] = $_GET['staticLink'];
      redirect("../");
   }

   $objTmpl = new CTemplate();
   $objWs   = new CWebSite();

   $objWs->SetMenu($objMenu->GenerateMenuLinks());

   if(! $activeStep = $objMenu->GetActiveNavigationStep())
   {
      $objMenu->SetActiveNavigationStep($_SESSION['ACTIVE STEP']);
      $activeStep = $_SESSION['ACTIVE STEP'];
   }

   $resKeys = $objMenu->GetNavigationListKeys($activeStep);
   
   $category        = $resKeys['primary_key'];

   // online quotes - NORMAL MODE
   if($_POST['request'] == 'GET QUOTES')
   {
      if($_SESSION['CONFIRMATION_CODE'] == $_SESSION['ENTERED_CODE'])
      {
         unset($_SESSION['RETRIEVE_QUOTES']['request']);
         $objMenu->SetForwardStep();
      }

   }// RETRIEVE QUOTE BY SID
   else if ($_SESSION['RETRIEVE_QUOTES']['request'] == 'GET QUOTES')
   {
      if(! $_SESSION['LOCKED'])
      {
      
        $protectedSesssion = false;
        $protectedCookie  = false;
        if($_SESSION['CONFIRMATION_CODE'] == $_SESSION['ENTERED_CODE'])
           $protectedSesssion = true;

        $objQzProtection = new CQzProtection($quoteTypeID,$_COOKIE,$_SESSION);

        if($objQzProtection->ValidateRequest())
           $protectedCookie = true;

        //if($category == '_QUOTE_SUMMARY_')
        //   if($protectedSesssion && $protectedCookie)
        //      $objMenu->SetForwardStep();

        //if($category == '_QUOTE_RESULTS_')
        //   $objMenu->SetBackStep();
      }
   }

   // DO NOT set STEP_TRACKINGS_ID for QZ_IP_ARRAY IPs
   if( ! in_array($_SERVER['REMOTE_ADDR'],$QZ_IP_ARRAY))
   {
      //steps trackings 
      if(! $_SESSION['LOCKED'])
      {
         $category = trim(str_replace("_", " ",trim($category)));
         $category = trim(str_replace(" ", "_",trim($category)));

         if(! isset($_SESSION['STEP_TRACKINGS_ID']))
         {
            if(! $_SESSION['STEP_TRACKINGS_ID'] = $objStepTrackings->AddStepTrackings($category,$quoteTypeID,$_SERVER['REMOTE_ADDR']))
               $objStepTrackings->GetError();         
         }
         else 
         {
            if(! $objStepTrackings->UpdateStepTrackings($_SESSION['STEP_TRACKINGS_ID'],$category))
               $objStepTrackings->GetError();
         }

         if($category=="QUOTE_RESULTS")
         {
            $objStepTrackings->RemoveSessionFile($_SESSION['STEP_TRACKINGS_ID']);
         }          
         else 
         {
            if(! $objStepTrackings->SaveSessionFile($_SESSION['STEP_TRACKINGS_ID']))
               $objStepTrackings->GetError();
         }      
      }
      else // log all steps trackings 
      {
         unset($_SESSION['STEP_TRACKINGS_ID']);
      }
   }                                            

   // set tracker page
   $_refererInfo = parse_url($_SERVER['HTTP_REFERER']);
   $_refererInfo['host'] = strtolower($_refererInfo['host']);
   $_hostInfo['host']    = strtolower($_SERVER['HTTP_HOST']);

//print "<!-- ".$_refererInfo['host'] ." == ".$_hostInfo['host']." -->";

   if(empty($category))
     $category = 'YourDetails';

   $categoryTracked = trim(str_replace("_", " ",trim($category)));
   $categoryTracked = trim(str_replace(" ", "_",trim($categoryTracked)));

//print "<!-- 123 $categoryTracked -->";

  // if($category != '' && $_refererInfo['host'] == $_hostInfo['host']) //TODO WHY THIS 
      $objWs->SetTrackerPage($categoryTracked.'.php');

//print "<!-- 123 $categoryTracked -->";
  

//   print "File to include = $includeFilename <br>";

   $includeFilename = $objMenu->GetIncludeFilename();

   if($_POST['step'] == 'quit')
      $includeFilename = 'QUIT.php';

   include $includeFilename;

   if($objMenu->IsNextStep())
   {

      if(function_exists('Process'))
         Process();
   }
   else
   {
      if(function_exists('Show'))
         Show();
   }


   function redirect($url="index.php")
   {
      if(! preg_match("/cs=/i", $url))
      {
         if(! empty($_GET['cs']))
            $cookieSession = $_GET['cs'];

         if(! empty($_POST['cs']))
            $cookieSession = $_POST['cs'];

         if( defined('COOKIE_SESSION_ID'))
           $cookieSession = COOKIE_SESSION_ID;

         if(! empty($cookieSession))
         {
            $prepUrl = $url."?cs=$cookieSession";
            if(preg_match("/\?/i", $url))
               $prepUrl = $url."&cs=$cookieSession";

            $url = $prepUrl;
         }
      }

      if(headers_sent())
      {
                  print <<<END_TAG

                  <script language="javascript">
                  <!--
                  location.href="$url";
                  //-->
                  </script>

END_TAG;

      }
      else
      {
         $today = date("D M j G:i:s Y");
         header("HTTP/1.1 200 OK");
         header("Date: $today");
         header("Location: $url");
         header("Content-Type: text/html");
      
      }

      exit(0); //if it doesn't work die (Javascript disabled?)
   }



?>

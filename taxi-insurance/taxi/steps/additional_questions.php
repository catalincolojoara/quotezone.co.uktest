<?php

// this question was only added Nov 2011, therefore we wont have it for a lot of renewals. Here we check if we have it and if not we default it to no.
if (!$Session["_YourDetails_"]["convictions_5_years"]) {
    $Session["_YourDetails_"]["convictions_5_years"] = "No";
}

// some 2 or 3 year renewals do not have this question, so default to no
if (!$Session["_YourDetails_"]["claims_5_years"]) {
    $Session["_YourDetails_"]["claims_5_years"] = "No";
}

?>
<?php
/*****************************************************************************/
/*                                                                           */
/*   AutoFill.php -  retrieve common details from quotes on other systems    */
/*                                                                           */
/*  (C) 2010 Moraru Valeriu (null@seopa.com)                                 */
/*                                                                           */
/*****************************************************************************/
session_start();

$_SESSION = array();

error_reporting(0);

include_once "../modules/globals.inc";
include_once "File.php";
include_once "FileStoreClient.php";
include_once "MySQL.php";

$crsid  = $_GET['crsid'];

if(empty($crsid))
   redirect("../index.php");

$fileName = substr($crsid, 0, 10)."_".substr($crsid, 10, 6);

if(! $quoteType = GetQuoteType($fileName))
   redirect("../index.php");

//print $quoteType;die;

//patch
if($quoteType == "trailer-tent")
   $quoteType = "trailer_tent";

if($quoteType == "motor-fleet")
   $quoteType = "motor_fleet";

if($quoteType == "taxi-fleet")
   $quoteType = "taxi_fleet";

if($quoteType == "privatemedicalinsurance")
   $quoteType = "pmi";

$objFSClient = new CFileStoreClient($quoteType,'in');

// get file
$data =  $objFSClient->GetFile($fileName);
eval("\$Session = $data;");

$_SESSION["CRSID"] = $quoteType;

//START file is from the same system - we do similar to sid bu redirect to 1`st page and reset the DIS field
$localSystemQuoteType = "taxi";

if($quoteType == $localSystemQuoteType)
{

   //set the initial session details (as they are in the file)
   $_SESSION = array();
   $_SESSION = $Session;

   //reset some session parameters :
   
   //DIS must be empty
   $_SESSION["_YourDetails_"]["date_of_insurance_start_dd"]   = "";
   $_SESSION["_YourDetails_"]["date_of_insurance_start_mm"]   = "";
   $_SESSION["_YourDetails_"]["date_of_insurance_start_yyyy"] = "";
   $_SESSION["_YourDetails_"]["start_date_dd"]                = "";
   $_SESSION["_YourDetails_"]["start_date_mm"]                = "";
   $_SESSION["_YourDetails_"]["start_date_yyyy"]              = "";
   $_SESSION["_YourDetails_"]["start_date"]                   = "";

   //We must be at step 1
   $_SESSION["ACTIVE STEP"] = 1;
   unset($_SESSION["_MENU_"]["2"]);


   //redirect to index
   redirect("../index.php");
   
}
//END file is from the same system - we do similar to sid bu redirect to 1`st page and reset the DIS field




switch($quoteType)
{
   case 'car'  :
   case 'van'  :
   case 'bike' :
      // prepare session data
      $_SESSION["_YourDetails_"]['title']                    = $Session["_DRIVERS_"][0]['title'];
      $_SESSION["_YourDetails_"]['first_name']               = $Session["_DRIVERS_"][0]['first_name'];
      $_SESSION["_YourDetails_"]['surname']                  = $Session["_DRIVERS_"][0]['surname'];
      $_SESSION["_YourDetails_"]['date_of_birth_dd']         = $Session["_DRIVERS_"][0]['date_of_birth_dd'];
      $_SESSION["_YourDetails_"]['date_of_birth_mm']         = $Session["_DRIVERS_"][0]['date_of_birth_mm'];
      $_SESSION["_YourDetails_"]['date_of_birth_yyyy']       = $Session["_DRIVERS_"][0]['date_of_birth_yyyy'];
      $_SESSION["_YourDetails_"]['date_of_birth']            = $Session["_DRIVERS_"][0]['date_of_birth'];

      if($Session["_DRIVERS_"][0]['daytime_telephone_prefix'] AND $Session["_DRIVERS_"][0]['daytime_telephone_number'])
         $_SESSION["_YourDetails_"]['daytime_telephone'] = $Session["_DRIVERS_"][0]['daytime_telephone_prefix'].$Session["_DRIVERS_"][0]['daytime_telephone_number'];
      else
         $_SESSION["_YourDetails_"]['daytime_telephone'] = $Session["_DRIVERS_"][0]['home_telephone_prefix'].$Session["_DRIVERS_"][0]['home_telephone_number'];

      $_SESSION["_YourDetails_"]['mobile_telephone'] = $Session["_DRIVERS_"][0]['mobile_telephone_prefix'].$Session["_DRIVERS_"][0]['mobile_telephone_number'];

      $_SESSION["_YourDetails_"]['address_line1']            = $Session["_DRIVERS_"][0]['address_line1'];
      $_SESSION["_YourDetails_"]['address_line2']            = $Session["_DRIVERS_"][0]['address_line2'];
      $_SESSION["_YourDetails_"]['address_line3']            = $Session["_DRIVERS_"][0]['address_line3'];
      $_SESSION["_YourDetails_"]['address_line4']            = $Session["_DRIVERS_"][0]['address_line4'];
      $_SESSION["_YourDetails_"]['house_number_or_name']     = $Session["_DRIVERS_"][0]['house_number_or_name'];
      $_SESSION["_YourDetails_"]['postcode_prefix']          = $Session["_DRIVERS_"][0]['postcode_prefix'];
      $_SESSION["_YourDetails_"]['postcode']                 = $Session["_DRIVERS_"][0]['postcode'];
      $_SESSION["_YourDetails_"]['postcode_number']          = $Session["_DRIVERS_"][0]['postcode_number'];
      $_SESSION["_YourDetails_"]['email_address']            = $Session["_DRIVERS_"][0]['email_address'];

      break;

   case 'home' :
      // prepare session data
      $_SESSION["_YourDetails_"]['title']                    = $Session["_INSURED_"][0]['title'];
      $_SESSION["_YourDetails_"]['first_name']               = $Session["_INSURED_"][0]['first_name'];
      $_SESSION["_YourDetails_"]['surname']                  = $Session["_INSURED_"][0]['surname'];
      $_SESSION["_YourDetails_"]['date_of_birth_dd']         = $Session["_INSURED_"][0]['date_of_birth_dd'];
      $_SESSION["_YourDetails_"]['date_of_birth_mm']         = $Session["_INSURED_"][0]['date_of_birth_mm'];
      $_SESSION["_YourDetails_"]['date_of_birth_yyyy']       = $Session["_INSURED_"][0]['date_of_birth_yyyy'];
      $_SESSION["_YourDetails_"]['date_of_birth']            = $Session["_INSURED_"][0]['date_of_birth'];

      if($Session["_INSURED_"][0]['daytime_telephone_prefix'] AND $Session["_INSURED_"][0]['daytime_telephone_number'])
         $_SESSION["_YourDetails_"]['daytime_telephone'] = $Session["_INSURED_"][0]['daytime_telephone_prefix'].$Session["_INSURED_"][0]['daytime_telephone_number'];
      else
         $_SESSION["_YourDetails_"]['daytime_telephone'] = $Session["_INSURED_"][0]['home_telephone_prefix'].$Session["_INSURED_"][0]['home_telephone_number'];

      $_SESSION["_YourDetails_"]['mobile_telephone'] = $Session["_INSURED_"][0]['mobile_telephone_prefix'].$Session["_INSURED_"][0]['mobile_telephone_number'];
            
      $_SESSION["_YourDetails_"]['address_line1']            = $Session["_INSURED_"][0]['address_line1'];
      $_SESSION["_YourDetails_"]['address_line2']            = $Session["_INSURED_"][0]['address_line2'];
      $_SESSION["_YourDetails_"]['address_line3']            = $Session["_INSURED_"][0]['address_line3'];
      $_SESSION["_YourDetails_"]['address_line4']            = $Session["_INSURED_"][0]['address_line4'];
      $_SESSION["_YourDetails_"]['house_number_or_name']     = $Session["_INSURED_"][0]['house_number_or_name'];
      $_SESSION["_YourDetails_"]['postcode_prefix']          = $Session["_INSURED_"][0]['postcode_prefix'];
      $_SESSION["_YourDetails_"]['postcode']                 = $Session["_INSURED_"][0]['postcode'];
      $_SESSION["_YourDetails_"]['postcode_number']          = $Session["_INSURED_"][0]['postcode_number'];
      $_SESSION["_YourDetails_"]['email_address']            = $Session["_INSURED_"][0]['email_address'];

   break;

   case 'travel' :
      // prepare session data
      $_SESSION["_YourDetails_"]['title']                    = $Session["YourDetails"]['title'];
      $_SESSION["_YourDetails_"]['first_name']               = $Session["YourDetails"]['first_name'];
      $_SESSION["_YourDetails_"]['surname']                  = $Session["YourDetails"]['surname'];
      $_SESSION["_YourDetails_"]['date_of_birth_dd']         = $Session["YourDetails"]['date_of_birth_dd'];
      $_SESSION["_YourDetails_"]['date_of_birth']            = $Session["YourDetails"]['date_of_birth'];
      $_SESSION["_YourDetails_"]['date_of_birth_mm']         = $Session["YourDetails"]['date_of_birth_mm'];
      $_SESSION["_YourDetails_"]['date_of_birth_yyyy']       = $Session["YourDetails"]['date_of_birth_yyyy'];
      $_SESSION["_YourDetails_"]['postcode_prefix']          = $Session["YourDetails"]['postcode_prefix'];
      $_SESSION["_YourDetails_"]['postcode']                 = $Session["YourDetails"]['postcode_prefix']." ".$Session["YourDetails"]['postcode_number'] ;
      $_SESSION["_YourDetails_"]['postcode_number']          = $Session["YourDetails"]['postcode_number'];
      $_SESSION["_YourDetails_"]['email_address']            = $Session["YourDetails"]['email_address'];

   break;

   case 'breakdown' :
      // prepare session data
      $_SESSION["_YourDetails_"]['title']                    = $Session["_YourDetails_"]['title'];
      $_SESSION["_YourDetails_"]['first_name']               = $Session["_YourDetails_"]['first_name'];
      $_SESSION["_YourDetails_"]['surname']                  = $Session["_YourDetails_"]['surname'];
      $_SESSION["_YourDetails_"]['date_of_birth_dd']         = $Session["_YourDetails_"]['date_of_birth_dd'];
      $_SESSION["_YourDetails_"]['date_of_birth']            = $Session["_YourDetails_"]['date_of_birth'];
      $_SESSION["_YourDetails_"]['date_of_birth_mm']         = $Session["_YourDetails_"]['date_of_birth_mm'];
      $_SESSION["_YourDetails_"]['date_of_birth_yyyy']       = $Session["_YourDetails_"]['date_of_birth_yyyy'];
      $_SESSION["_YourDetails_"]['postcode_prefix']          = $Session["_YourDetails_"]['postcode_prefix'];
      $_SESSION["_YourDetails_"]['postcode']                 = $Session["_YourDetails_"]['postcode_prefix']." ".$Session["_YourDetails_"]['postcode_number'] ;
      $_SESSION["_YourDetails_"]['postcode_number']          = $Session["_YourDetails_"]['postcode_number'];
      $_SESSION["_YourDetails_"]['email_address']            = $Session["_YourDetails_"]['email_address'];

   break;

   case 'pet' :
      // prepare session data
      $_SESSION["_YourDetails_"]['title']                    = $Session["_YourDetails_"]['title'];
      $_SESSION["_YourDetails_"]['first_name']               = $Session["_YourDetails_"]['first_name'];
      $_SESSION["_YourDetails_"]['surname']                  = $Session["_YourDetails_"]['surname'];
      $_SESSION["_YourDetails_"]['date_of_birth_dd']         = $Session["_YourDetails_"]['date_of_birth_dd'];
      $_SESSION["_YourDetails_"]['date_of_birth']            = $Session["_YourDetails_"]['date_of_birth'];
      $_SESSION["_YourDetails_"]['date_of_birth_mm']         = $Session["_YourDetails_"]['date_of_birth_mm'];
      $_SESSION["_YourDetails_"]['date_of_birth_yyyy']       = $Session["_YourDetails_"]['date_of_birth_yyyy'];
      $_SESSION["_YourDetails_"]['daytime_telephone']        = $Session["_YourDetails_"]['phone_number_prefix'].$Session["_YourDetails_"]['phone_number_suffix'];
      $_SESSION["_YourDetails_"]['email_address']            = $Session["_YourDetails_"]['email_address'];
      $_SESSION["_YourDetails_"]['postcode_prefix']          = $Session["_YourDetails_"]['postcode_prefix'];
      $_SESSION["_YourDetails_"]['postcode_number']          = $Session["_YourDetails_"]['postcode_number'];
      $_SESSION["_YourDetails_"]['postcode']                 = $Session["_YourDetails_"]['postcode_prefix']." ".$Session["_YourDetails_"]['postcode_number'] ;
      $_SESSION["_YourDetails_"]['house_number_or_name']     = $Session["_YourDetails_"]['house_number_or_name'];
      $_SESSION["_YourDetails_"]['address_line1']            = $Session["_YourDetails_"]['address_line1'];
      $_SESSION["_YourDetails_"]['address_line2']            = $Session["_YourDetails_"]['address_line2'];
      $_SESSION["_YourDetails_"]['address_line3']            = $Session["_YourDetails_"]['address_line3'];
      $_SESSION["_YourDetails_"]['address_line4']            = $Session["_YourDetails_"]['address_line4'];

   break;

   default:
      // prepare session data
      $_SESSION["_YourDetails_"]['title']                    = ucfirst(strtolower($Session["_YourDetails_"]['title']));
      $_SESSION["_YourDetails_"]['first_name']               = $Session["_YourDetails_"]['first_name'];
      $_SESSION["_YourDetails_"]['surname']                  = $Session["_YourDetails_"]['surname'];
      $_SESSION["_YourDetails_"]['date_of_birth_dd']         = $Session["_YourDetails_"]['date_of_birth_dd'];
      $_SESSION["_YourDetails_"]['date_of_birth']            = $Session["_YourDetails_"]['date_of_birth'];
      $_SESSION["_YourDetails_"]['date_of_birth_mm']         = $Session["_YourDetails_"]['date_of_birth_mm'];
      $_SESSION["_YourDetails_"]['date_of_birth_yyyy']       = $Session["_YourDetails_"]['date_of_birth_yyyy'];
 
      if($Session["_YourDetails_"]['daytime_telephone_prefix'] AND $Session["_YourDetails_"]['daytime_telephone_number'])
         $_SESSION["_YourDetails_"]['daytime_telephone']     = $Session["_YourDetails_"]['daytime_telephone_prefix'].$Session["_YourDetails_"]['daytime_telephone_number'];
      else
         $_SESSION["_YourDetails_"]['daytime_telephone']     = $Session["_YourDetails_"]['daytime_telephone'];

      if($Session["_YourDetails_"]['mobile_telephone_prefix'] AND $Session["_YourDetails_"]['mobile_telephone_number'])
         $_SESSION["_YourDetails_"]['mobile_telephone']     = $Session["_YourDetails_"]['mobile_telephone_prefix'].$Session["_YourDetails_"]['mobile_telephone_number'];
      else
         $_SESSION["_YourDetails_"]['mobile_telephone']     = $Session["_YourDetails_"]['mobile_telephone'];
      
      $_SESSION["_YourDetails_"]['email_address']            = $Session["_YourDetails_"]['email_address'];
      $_SESSION["_YourDetails_"]['postcode_prefix']          = $Session["_YourDetails_"]['postcode_prefix'];
      $_SESSION["_YourDetails_"]['postcode_number']          = $Session["_YourDetails_"]['postcode_number'];
      $_SESSION["_YourDetails_"]['postcode']                 = $Session["_YourDetails_"]['postcode'];
      $_SESSION["_YourDetails_"]['house_number_or_name']     = $Session["_YourDetails_"]['house_number_or_name'];
      $_SESSION["_YourDetails_"]['address_line1']            = $Session["_YourDetails_"]['address_line1'];
      $_SESSION["_YourDetails_"]['address_line2']            = $Session["_YourDetails_"]['address_line2'];
      $_SESSION["_YourDetails_"]['address_line3']            = $Session["_YourDetails_"]['address_line3'];
      $_SESSION["_YourDetails_"]['address_line4']            = $Session["_YourDetails_"]['address_line4'];
  
}

// lightbox
$_SESSION["_YourDetails_"]['daytime_telephone_lightbox'] = $_SESSION["_YourDetails_"]['daytime_telephone'];
$_SESSION["_YourDetails_"]['mobile_telephone_lightbox']  = $_SESSION["_YourDetails_"]['mobile_telephone'];
$_SESSION["_YourDetails_"]['email_address_lightbox']     = $_SESSION["_YourDetails_"]['email_address'];

redirect("../index.php");


/************************************************************************************************/
/*                                                                                              */
/*                                         FUNCTIONS                                            */
/*                                                                                              */ 
/************************************************************************************************/

function GetQuoteType($filename="")
{
   if(empty($filename))
      return false;

   // get the quote type of specified filename in
   $mysqlObj = new CMySQL();

   if(! $mysqlObj->Open(DBNAME, DBHOST, DBUSER, DBPASS))
   {
      $strERR = $this->dbh->GetError();
      return false;
   }

   $lastSqlCmd = " SELECT qzqt.name FROM logs AS l, qzquotes AS qzq, qzquote_types AS qzqt WHERE l.id = qzq.log_id AND qzq.quote_type_id = qzqt.id AND l.filename = '$filename' ";

   if(! $mysqlObj->Exec($lastSqlCmd))
   {
      $strERR = $mysqlObj->GetError();
      return false;
   }

   if(! $mysqlObj->GetRows())
   {
      $strERR = "FILENAME_NOT_FOUND";
      return false;
   }

   $result    = $mysqlObj->FetchRows();
   $quoteType = $result->name;

   return $quoteType;
}

function redirect($url="")
{
      if(! preg_match("/cs=/i", $url))
      {
         if(! empty($_GET['cs']))
            $cookieSession = $_GET['cs'];

         if(! empty($_POST['cs']))
            $cookieSession = $_POST['cs'];

         if( defined('COOKIE_SESSION_ID'))
           $cookieSession = COOKIE_SESSION_ID;

         if(! empty($cookieSession))
         {
            $prepUrl = $url."?cs=$cookieSession";
            if(preg_match("/\?/i", $url))
               $prepUrl = $url."&cs=$cookieSession";

            $url = $prepUrl;
         }
      }

   if(headers_sent())
   {
               print <<<  END_TAG

               <SCRIPT LANGUAGE="JavaScript">
               <!--
               location.href="$url";
               //-->
               </SCRIPT>

END_TAG;

   }
   else
   {
      $today = date("D M j G:i:s Y");
      header("HTTP/1.1 200 OK");
      header("Date: $today");
      header("Location: $url");
      header("Content-Type: text/html");
   }

   exit(0); //if it doesn't work die (Javascript disabled?)
}


?>

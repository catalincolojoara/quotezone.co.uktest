<?php
/*****************************************************************************/
/*                                                                           */
/*  TAXI web interface                                                       */
/*                                                                           */
/*  (C) 2006 ACRUX SOFTWARE                                                  */
/*                                                                           */
/*****************************************************************************/

include_once "modules/globals.inc";
include_once "YourDetailsElements.php";
include_once "errors.inc";
include_once "functions.php";
include_once "Business.php";
include_once "Postcode.php";


//////////////////////////////////////////////////////////////////////////////PB
//
// [SCRIPT NAME]:   YourDetails.php
//
// [DESCRIPTION]:  script web interface for bike vehicle section
//
// [FUNCTIONS]:
//                Show()
//                Process()
//                TemplateElements(&$resParameters)
//                ValidateExtraErrors(&$resParameters)
//
// [CREATED BY]:   Adi Axente (adrian.axente@seopa.com) 2008-03-24
//
// [MODIFIED]:     - [programmer (email) date]
//                   [short description]
//
//////////////////////////////////////////////////////////////////////////////PE

   $objPostCode = new CPostcode();

   $resParameters = array();

   // set post formular variables
   $postParameters = array(
      'vehicle_registration_number',
      'taxi_make',
      'taxi_model',
      'estimated_value',
      'year_of_manufacture',
      'vehicle_mileage',
      'taxi_type',
      'taxi_used_for',
      'taxi_capacity',
      'plating_authority',
      'type_of_cover',
      'taxi_ncb',
      'private_car_ncb',
      'period_of_licence',
      'taxi_badge',
      'taxi_driver',
      'claims_5_years',
      'convictions_5_years',
      //'gap_insurance',
      //'breakdown_cover',
      'title',
      'first_name',
      'surname',
      'daytime_telephone',
      'daytime_telephone_lightbox',
      'mobile_telephone',
      'mobile_telephone_lightbox',
      'best_day_call',
      'best_time_call',
      'date_of_birth',
      'date_of_birth_dd',
      'date_of_birth_mm',
      'date_of_birth_yyyy',
      'date_of_insurance_start_dd',
      'date_of_insurance_start_mm',
      'date_of_insurance_start_yyyy',
      'postcode_prefix',
      'postcode_number',
      'postcode',
      'house_number_or_name',
      'address_line1',
      'address_line2',
      'address_line3',
      'address_line4',
      'email_address',
      'email_address_lightbox',
      'showLightbox',
   );

   foreach($postParameters as $key => $value)
      $resParameters["$value"] = trim($_POST["$value"]);

   // set image level

   $imageLevel = 1;

function Show()
{
   global $imageLevel;
   global $objTmpl;
   global $objWs;
   global $objMenu;
   global $resParameters;
   global $postParameters;

   $resParameters['select_view'] = "none";

   if(! $objTmpl->Load("YourDetails.tmpl"))
      print $objTmpl->GetError()."\n";

   if((! $resParameters['showLightbox']) || ($resParameters['showLightbox'] == ""))
   {
      if($resSessionParameters = $objMenu->GetSessionContent('_YourDetails_'))
      {
         foreach($postParameters as $key => $value)
         {
      	    if ($value == 'showLightbox' and $resParameters['showLightbox'] == 1)
      	    {
      		$resParameters['showLightbox'] = '1';
      		continue;
      	    }
         
            $resParameters["$value"] = $resSessionParameters[$value];
         }

         $resParameters['select_view'] = "block";
      }
   }

   if(! $objMenu->ExistSessionEntry('_YourDetails_'))
   {
      $objTmpl->SetRunMode(1);
      $resYourDetailsPrepare = array();
      $objTmpl->Prepare($resYourDetailsPrepare);

      $objTmpl->SetRunMode(2);
   } // end set default radiobox elements

   YourDetailsElements($resParameters);

   $resParameters["house_number_or_name"] = urlencode($resParameters["house_number_or_name"]);

   if($resParameters['showLightbox'] != '1')
   {
      $adkey = $_GET['adkey'];
      $userTracking = "var user = {'userid' : '$adkey'};piwikTracker.setCustomData(user);";
      $objWs->SetPiwikTracker("/virtual/taxi-insurance/page/1",$userTracking);
      //SET NEW GOOGLE TRACKER
      $objWs->SetGoogleTracker('/taxi-quote/step1/quote-form/');
   }

   $objTmpl->Prepare($resParameters);
   $objWs->SetImageHeaderLevel($imageLevel);
   $objWs->SetWorkArea($objTmpl->GetContent());
   $objWs->Run();

   $_SESSION['LOCKED'] = false;
}

function Process()
{
   global $imageLevel;

   global $objTmpl;
   global $objWs;
   global $objMenu;
   global $resParameters;
   global $postParameters;

   $validator = true;

   if(! $objTmpl->Load("YourDetails.tmpl"))
       print $objTmpl->GetError();

   $resParameters['select_view'] = 'none';

   YourDetailsElements($resParameters);

   if(! $objTmpl->Validate($resParameters))
      $validator = false;

   if(! ValidateExtraErrors($resParameters))
      $validator = false;

   $resParameters["house_number_or_name"] = urldecode($resParameters["house_number_or_name"]);

   if(! $validator)
   {
      $resParameters['showLightbox'] = '';
      Show();
      return;
   }

   if ($resParameters['showLightbox'] == '')
   {
   	$resParameters['showLightbox'] = '1';
   	Show();
   	return;
   }

   $resParameters['showLightbox'] = '';

   // set forces params
   foreach($postParameters as $k => $v)
   {
      $objMenu->SetSessionContent($v, trim($resParameters[$v]), '_YourDetails_');

      // prepare date of birth
      if($v == "date_of_birth_dd" || $v == "date_of_birth_mm" || $v == "date_of_birth_yyyy")
      {
         $resParameters["date_of_birth"] = $resParameters["date_of_birth_dd"]."/".$resParameters["date_of_birth_mm"]."/".$resParameters["date_of_birth_yyyy"];

         $objMenu->SetSessionContent('date_of_birth', $resParameters["date_of_birth"], '_YourDetails_');
      }

      // prepare postcode
      if($v == "postcode_prefix" || $v == "postcode_number")
      {
         $resParameters[$selected."postcode"] = $resParameters["postcode_prefix"]." ".$resParameters["postcode_number"];
         $objMenu->SetSessionContent('postcode', $resParameters["postcode_prefix"]." ".$resParameters["postcode_number"], '_YourDetails_');
      }
   }
   // save data to the session
   foreach($postParameters as $k => $v)
      $objMenu->SetSessionContent($v, $resParameters[$v], '_YourDetails_');

   $objMenu->SetForwardStep();

}

function YourDetailsElements(&$resParameters)
{

   global $objTmpl;
   global $objMenu;
   global $postParameters;
   global $_YourDetails;

   //force date of insorance start to 1 day in future: - SJC-542762
   $tommorow   = date("Y-m-d",(mktime("0","0","0",date('m'),date('d'),date('Y')) + 86400));
   $tomorowArr = explode("-",$tommorow);

   if(! $resParameters["date_of_insurance_start_dd"])
      $resParameters["date_of_insurance_start_dd"]   = $tomorowArr[2];

   if(! $resParameters["date_of_insurance_start_mm"])
      $resParameters["date_of_insurance_start_mm"]   = $tomorowArr[1];

   if(! $resParameters["date_of_insurance_start_yyyy"])
      $resParameters["date_of_insurance_start_yyyy"] = $tomorowArr[0];

   // prepare select box elements
   foreach($_YourDetails as $key => $resElement)
   {
      $selectedKey = $resParameters[$key];

      $elements = GetAllElements($resElement);

      $resParameters["$key.value"] = $objTmpl->MakeSelBoxOptions($elements,$selectedKey,false);

   }

   $resYourDetails = $objMenu->GetSessionContent('_YourDetails_');

   if(! $resParameters["plating_authority"] || $resParameters["plating_authority"] == "Start typing here to search")
   {
      $resParameters["plating_authority"] = "Start typing here to search";
      $resParameters["plating_authority.class"] = "autoSearch";
   }
   else
      $resParameters["plating_authority.class"] = "cssTTSize";

   SetEditBox("vehicle_registration_number", $resParameters);
   SetEditBox("plating_authority", $resParameters);
   SetEditBox("taxi_make", $resParameters);
   SetEditBox("taxi_model", $resParameters);
   SetEditBox("estimated_value", $resParameters);
   SetEditBox("first_name", $resParameters);
   SetEditBox("surname", $resParameters);
   SetEditBox("daytime_telephone", $resParameters);
   SetEditBox("daytime_telephone_lightbox", $resParameters);
   SetEditBox("mobile_telephone", $resParameters);
   SetEditBox("mobile_telephone_lightbox", $resParameters);
   SetEditBox("address_line1", $resParameters);
   SetEditBox("address_line2", $resParameters);
   SetEditBox("address_line3", $resParameters);
   SetEditBox("address_line4", $resParameters);
   SetEditBox("house_number_or_name", $resParameters);
   SetEditBox("email_address", $resParameters);
   SetEditBox("postcode_prefix", $resParameters);
   SetEditBox("postcode_number", $resParameters);
   SetEditBox("postcode", $resParameters);
   SetEditBox("email_address_lightbox", $resParameters);
   SetEditBox("showLightbox", $resParameters);
}

function ValidateExtraErrors(&$resParameters)
{

   global $objPostCode;

   $objBus = new CBusiness();

   $error = false;

   $day   = date("d");
   $month = date("m");
   $year  = date("Y");

   $dobDay   = $resParameters["date_of_birth_dd"];
   $dobMonth = $resParameters["date_of_birth_mm"];
   $dobYear  = $resParameters["date_of_birth_yyyy"];

   $insDay  = $resParameters["date_of_insurance_start_dd"];
   $insMon  = $resParameters["date_of_insurance_start_mm"];
   $insYear = $resParameters["date_of_insurance_start_yyyy"];

   $tsToday = _mktime(0,0,0,$month,$day,$year);
   $tsDob   = _mktime(0,0,0,$dobMonth,$dobDay,$dobYear);
   $tsIns   = _mktime(0,0,0,$insMon,$insDay,$insYear);

   $insuranceStartDate = $insYear."-".$insMon."-".$insDay;
   $proposerBirthDate  = $dobYear."-".$dobMonth."-".$dobDay;

   $ProposerYearAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($ProposerYearAge < 18)
   {
      SetExtraError("date_of_birth", "INVALID_AGE_OF_DRIVER", $resParameters);
      $error=true;
   }

   $taxiNCB       = $resParameters["taxi_ncb"];
   $taxiBadge     = $resParameters["taxi_badge"];
   $taxiUse       = $resParameters["taxi_used_for"];
   $licencePeriod = $resParameters["period_of_licence"];

   if($taxiUse == "2")
   {
      if($ProposerYearAge < 21)
      {
         SetExtraError("date_of_birth", "INVALID_PUBLIC_HIRE_AGE_OF_DRIVER", $resParameters);
         $error=true;
      }

      if($licencePeriod < 3)
      {
         SetExtraError("period_of_licence", "INVALID_PUBLIC_HIRE_LICENCE", $resParameters);
         $error=true;
      }

      $exactAge = floor($ProposerYearAge);

      if($exactAge >= 21)
      {
         $ageDiff = $exactAge - 21;

         if($licencePeriod > (5 + $ageDiff))
         {
            SetExtraError("period_of_licence", "INVALID_21_PUBLIC_HIRE_LICENCE", $resParameters);
            $error=true;
         }  

         if($taxiNCB > (0 + $ageDiff))
         {
            SetExtraError("taxi_ncb", "INVALID_21_PUBLIC_HIRE_NCB", $resParameters);
            $error=true;
         }

         if($taxiBadge > (3 + $ageDiff))
         {
            SetExtraError("taxi_badge", "INVALID_21_PUBLIC_HIRE_BADGE", $resParameters);
            $error=true; 
         } 
      }
   }

   if($taxiUse == "1")
   {
      if($ProposerYearAge < 18)
      {
         SetExtraError("date_of_birth", "INVALID_PRIVATE_HIRE_AGE_OF_DRIVER", $resParameters);
         $error=true;
      }

      if($licencePeriod < 3)
      {
         SetExtraError("period_of_licence", "INVALID_PRIVATE_HIRE_LICENCE", $resParameters);
         $error=true;
      }

      $exactAge = floor($ProposerYearAge);

      if($exactAge >= 18)
      {
         $ageDiff = $exactAge - 18;

         if($licencePeriod > (3 + $ageDiff))
         {
            SetExtraError("period_of_licence", "INVALID_18_PRIVATE_HIRE_LICENCE", $resParameters);
            $error=true;
         }  

         if($taxiNCB > (0 + $ageDiff))
         {
            SetExtraError("taxi_ncb", "INVALID_18_PRIVATE_HIRE_NCB", $resParameters);
            $error=true;
         }

         if($taxiBadge > (3 + $ageDiff))
         {
            SetExtraError("taxi_badge", "INVALID_18_PRIVATE_HIRE_BADGE", $resParameters);
            $error=true; 
         } 
      }
   }

   if($tsIns < $tsToday)
   {
      SetExtraError("date_of_insurance_start", "INSURANCE_CANNOT_START_IN_PAST", $resParameters);
      $error=true;
   }

   //insurance start date
   $days         = (($tsIns - $tsToday) / 86400);

   if($days < 0)
   {
      SetExtraError("date_of_insurance_start", "INVALID_INSURANCE_CANNOT_START_IN_THE_PAST", $resParameters);
      $error = true;
   }

   if($days > 30)
   {
      SetExtraError("date_of_insurance_start", "INVALID_INSURANCE_CANNOT_BE_MORE_THAN_30_DAYS", $resParameters);
      $error = true;
   }



   if(empty($resParameters["date_of_birth_dd.error"]) && empty($resParameters["date_of_birth_mm.error"]) && empty($resParameters["date_of_birth_yyyy.error"]))
   {
      if(! ValidateDate($dobDay,$dobMonth,$dobYear))
      {
         SetExtraError("date_of_birth", "INVALID_DAYS_OF_MONTH", $resParameters);
         $error=true;
      }
   }

   $daytimePhone = $resParameters['daytime_telephone'];
   $mobilePhone  = $resParameters['mobile_telephone'];

   // one of two numbers are mandatory. We must have at least one field completed
   if(empty($daytimePhone) && empty($mobilePhone))
   {
      SetExtraError("daytime_telephone", "INVALID_DAYTIME_PHONE_NUMBER_MANDATORY", $resParameters);
      SetExtraError("mobile_telephone", "INVALID_MOBILE_TELEPHONE_MANDATORY", $resParameters);
      $error=true;
   }

   $taxiMake       = $resParameters['taxi_make'];
   $taxiModel      = $resParameters['taxi_model'];
   $estimatedValue = $resParameters['estimated_value'];

   $platingAuthority = $resParameters['plating_authority'];
   if($platingAuthority == "" || $platingAuthority == "Start typing here to search")
   {
      SetExtraError("plating_authority", "INVALID_PLATING_AUTHORITY", $resParameters);
      $error=true;
   }

   if($platingAuthority != "" && $platingAuthority != "Start typing here to search")
   {
      if(! $objBus->GetBusinessByName($platingAuthority))
      {
         SetExtraError("plating_authority", "INVALID_PLATING_AUTHORITY", $resParameters);
         $error=true;
      }
   }

   if($taxiMake == "")
   {
      SetExtraError("taxi_make", "INVALID_TAXI_MAKE", $resParameters);
      $error=true;
   }

   if($taxiModel == "")
   {
      SetExtraError("taxi_model", "INVALID_TAXI_MODEL", $resParameters);
      $error=true;
   }

   if($estimatedValue == "")
   {
      SetExtraError("estimated_value", "INVALID_ESTIMATED_VALUE", $resParameters);
      $error=true;
   }

   if(! empty($daytimePhone))
   {
      if(strlen($daytimePhone) != 12 && strlen($daytimePhone) != 11 && strlen($daytimePhone) != 10)
      {
         if(empty($resParameters['daytime_telephone_prefix.error']) && empty($resParameters['daytime_telephone_number.error']))
         {
            SetExtraError($selected."daytime_telephone", "INVALID_DAYTIME_PHONE_NUMBER_LENGTH", $resParameters);
            $error = true;
         }
      }
      else if(! preg_match("/^((01)|(02)|(03)|(07))\d+/i",$daytimePhone))
      { //validate daytime dialing code
         SetExtraError($selected."daytime_telephone", "INVALID_DAYTIME_DIAL_CODE", $resParameters);
         $error = true;
      }
      elseif(preg_match("/^(03)\d+/i",$daytimePhone) AND strlen($daytimePhone) != 11)
      {
         SetExtraError($selected."daytime_telephone", "INVALID_DAYTIME_PHONE_NUMBER_LENGTH", $resParameters);
         $error = true;
      }
   }

   if(empty($resParameters["mobile_telephone_prefix.error"]) && empty($resParameters["mobile_telephone_number.error"]))
   {
      if(! empty($mobilePhone))
      {
         //validate mobile dialing code
         if(! preg_match("/^(07)\d+/i",$mobilePhone) || ((strlen($mobilePhone)!=11) && (strlen($mobilePhone)!=10)))
         {
            SetExtraError($selected."mobile_telephone", "INVALID_MOBILE_TELEPHONE", $resParameters);
            $error = true;
         }
      }
   }

   // check postcode
   $postCode = strtoupper(trim($resParameters['postcode_prefix'])." ".trim($resParameters['postcode_number']));
   $size = strlen($postCode);

   list($firstPart,$secondPart) = split(" ",$postCode);

   if($size < 6 || $size > 8)
      if(empty($resParameters[$selected."postcode_prefix.error"]) && empty($resParameters[$selected."postcode_number.error"]))
      {
         SetExtraError($selected."postcode", "INVALID_POSTCODE_SIZE", $resParameters);
         $error=true;
      }

   if(! preg_match("/^[A-Z]/i",$firstPart[0]))
      if(empty($resParameters[$selected."postcode_prefix.error"]) && empty($resParameters[$selected."postcode_number.error"]))
      {
         SetExtraError($selected."postcode", "INVALID_POSTCODE_FIRST_FP_CHAR", $resParameters);
         $error=true;
      }

   if(! $errorPC)
      if(! preg_match("/^[0-9]/",$secondPart[0]))
         if(empty($resParameters[$selected."postcode_prefix.error"]) && empty($resParameters[$selected."postcode_number.error"]))
         {
            SetExtraError($selected."postcode", "INVALID_POSTCODE_FIRST_SP_CHAR", $resParameters);
            $error=true;
         }

   if(! $errorPC)
      if(! preg_match("/^[A-Z]/i",$secondPart[1]))
         if(empty($resParameters[$selected."postcode_prefix.error"]) && empty($resParameters[$selected."postcode_number.error"]))
         {
            SetExtraError($selected."postcode", "INVALID_POSTCODE_SECOND_SP_CHAR", $resParameters);
            $error=true;
         }

   if(! $errorPC)
      if(! preg_match("/^[A-Z]/i",$secondPart[2]))
         if(empty($resParameters[$selected."postcode_prefix.error"]) && empty($resParameters[$selected."postcode_number.error"]))
         {
            SetExtraError($selected."postcode", "INVALID_POSTCODE_LAST_SP_CHAR", $resParameters);
            $error=true;
         }

   $resPostcode = $objPostCode->PostCodeLookup($postCode);

   if(! is_array($resPostcode))
   {
      SetExtraError($selected."postcode", "INVALID_POSTCODE_NOT_FOUND", $resParameters);
      $error=true;
   }


   if($errorPC)
      $error=true;

   if($error)
      return false;

   return true;
}



function SetSelectBoxElement(&$resParameters,$name='', $value='')
{
   global $objTmpl;

   $resElement = $_YourDetails[$name];
   $elements = GetAllElements($resElement);

   $resParameters[$name.".value"] = $objTmpl->MakeSelBoxOptions($elements,$value,false);
}

function SetRadioBoxElement(&$resParameters,$name='',$value)
{
   $resParameters[$name."_".$value.".checked"]  = "checked";
}

?>

<?php

   include_once "../modules/globals.inc";
   include_once "Postcode.php";
   include_once "Template.php";

   session_start();

   $objPostCode = new CPostcode();

   $postcode    = trim($_GET['pc']);
   $houseNumber = urldecode(trim($_GET['hn']));
   $control     = trim($_GET['c']);

   // check postcode
   $size = strlen($postcode);

   $postcode = preg_replace("/\s+/"," ", $postcode);

   list($firstPart,$secondPart) = split(" ",$postcode);

   $errorPC = false;

   if($size < 6 || $size > 8)
   {
      $errMesg = GetErrorString("INVALID_POSTCODE_SIZE");
      $errorPC=true;
   }

   if(! $errorPC)
      if(! preg_match("/^[A-Z]/i",$firstPart[0]))
      {
         $errMesg = GetErrorString("INVALID_POSTCODE_FIRST_FP_CHAR");
         $errorPC=true;
      }

   if(! $errorPC)
      if(! preg_match("/^[0-9]/",$secondPart[0]))
      {
         $errMesg = GetErrorString("INVALID_POSTCODE_FIRST_SP_CHAR");
         $errorPC=true;
      }

   if(! $errorPC)
      if(! preg_match("/^[A-Z]/i",$secondPart[1]))
      {
         $errMesg = GetErrorString("INVALID_POSTCODE_SECOND_SP_CHAR");
         $errorPC=true;
      }

   if(! $errorPC)
      if(! preg_match("/^[A-Z]/i",$secondPart[2]))
      {
         $errMesg = GetErrorString("INVALID_POSTCODE_LAST_SP_CHAR");
         $errorPC=true;
      }

print <<<EOT

   <script language="JavaScript">

   if('$control' == '')
   {
      var pcErrBg  = parent.document.getElementById('postcode_error_bg');
      var pcErrTbl = parent.document.getElementById('postcode_error_tbl');
      var pcErrTxt = parent.document.getElementById('postcode_error_txt');

      // we have to check xml errors to hide
      var elTd = pcErrTbl.getElementsByTagName('td');

      for(var i = 0; i < elTd.length; i++)
      {

         if(elTd[i] == pcErrTxt)
            continue;

         if(elTd[i].className == 'ErrTxt')
         {
            elTd[i].innerHTML = '';
            elTd[i].className = 'q';
         }
      }

      var notFoundDiv = parent.document.getElementById('div_not_found_address');
      var foundDiv    = parent.document.getElementById('div_found_address');

      var elNfTbl = notFoundDiv.getElementsByTagName('table');

      elNfTbl[0].className = 'tblCnt';
      elNfTbl[1].className = 'tblCnt';
      elNfTbl[2].className = 'tblCnt';
      elNfTbl[3].className = 'tblCnt';
      elNfTbl[4].className = 'tblCnt';

      var elNfTd = elNfTbl[0].getElementsByTagName('td');

      for(var i = 0; i < elNfTd.length; i++)
      {
         if(elNfTd[i].className == 'ErrTxt')
         {
            elNfTd[i].innerHTML = '';
            elNfTd[i].className = 'q';
         }
      }


      var elFTbl = foundDiv.getElementsByTagName('table');

      elFTbl[0].className = 'tblCnt';

      var elFTd = elFTbl[0].getElementsByTagName('td');

      for(var i = 0; i < elFTd.length; i++)
      {
         if(elFTd[i].className == 'ErrTxt')
         {
            elFTd[i].innerHTML = '';
            elFTd[i].className = 'q';
         }
      }

      var pcodeAddDet = parent.document.getElementById('postcode_address_details');

      pcErrTbl.className             = 'tblCnt';
      pcErrTxt.style.visibility      = 'hidden';
      pcErrBg.className              = '';
      pcErrBg.style.borderWidth      = '0px';
      pcodeAddDet.style.borderWidth  = '0px';
      pcErrTxt.innerHTML             = '';
   }

   </script>

EOT;

   if($errorPC)
   {
      ShowNotFoundPostcode($errMesg);
      exit(0);
   }


   $resPostcode = $objPostCode->PostCodeLookup($postcode);

   if(! empty($houseNumber))
      if(! array_key_exists($houseNumber, $resPostcode))
      {
         ShowNotFoundAddress();
         exit(0);
      }

   if(! is_array($resPostcode))
   {
      $errMesg = GetErrorString("INVALID_POSTCODE_NOT_FOUND");
      ShowNotFoundPostcode($errMesg);
      exit(0);
   }

   ShowPostcodeAddress();


function ShowNotFoundPostcode($errMesg)
{
  global $control;

   // show error in case we did not find any entries
   print <<<EOT

   <script language="JavaScript">

      var notFoundDiv = parent.document.getElementById('div_not_found_address');
      var foundDiv    = parent.document.getElementById('div_found_address');

      foundDiv.style.display    = 'none';
      notFoundDiv.style.display = 'none';

      var pcErrBg  = parent.document.getElementById('postcode_error_bg');
      var pcErrTbl = parent.document.getElementById('postcode_error_tbl');
      var pcErrTxt = parent.document.getElementById('postcode_error_txt');

      if('$control' == '')
      {
         pcErrTbl.className        = 'tblCntErr';
         pcErrBg.className         = 'ErrBor';
         pcErrTxt.innerHTML        = '$errMesg';
         pcErrTxt.style.visibility = 'visible';
         pcErrBg.style.borderWidth = '1px';
         parent.document.YourDetails.postcodeID.focus();
      }

   </script>

EOT;

}


function ShowNotFoundAddress()
{
   global $control;
   global $resPostcode;

   print<<<EOT

      <script language="JavaScript">

         var notFoundDiv = parent.document.getElementById('div_not_found_address');
         var foundDiv    = parent.document.getElementById('div_found_address');

         foundDiv.style.display    = 'none';
         notFoundDiv.style.display = 'block';

         if('$control' == '')
         {
            parent.document.YourDetails.address_line1.focus();

         }
         else
            if(parent.document.YourDetails.address_line1.value == '')
            {
               parent.document.YourDetails.address_line2.value = '';
               parent.document.YourDetails.address_line3.value = '';
               parent.document.YourDetails.address_line4.value = '';
            }


      </script>
EOT;

}

function ShowPostcodeAddress()
{
   global $resPostcode;
   global $houseNumber;
   global $control;

   if(! is_array($resPostcode))
   {
      ShowNotFoundAddress();
      return;
   }

   print<<<EOT

      <script language="JavaScript">

         var notFoundDiv = parent.document.getElementById('div_not_found_address');
         var foundDiv    = parent.document.getElementById('div_found_address');

         foundDiv.style.display    = 'block';
         notFoundDiv.style.display = 'none';

         parent.document.getElementById('postcode_id').visibility = "visible";

         var PostcodeDivElement   = parent.document.getElementById("postcode_id");
         var postcodeFrameElement = parent.document.getElementById("postcode");

         parent.document.YourDetails.pcode.options.length         = 0;
         parent.document.YourDetails.found_address.options.length = 0;

         parent.addOptionSelectBox(parent.document.YourDetails.pcode,0,'          -- Select --','');
         parent.addOptionSelectBox(parent.document.YourDetails.pcode,1,'   Not shown in the list','not_shown');

         parent.addOptionSelectBox(parent.document.YourDetails.found_address,0,'          -- Select --','');
         parent.addOptionSelectBox(parent.document.YourDetails.found_address,1,'   Not shown in the list','not_shown');

         //alert('Show the postcode');

EOT;

      $countPcodeElements = count($resPostcode);
      $countPcodeElements +=3 ;;

      $i=1;
      foreach($resPostcode as $idPC => $namePC)
      {
         $i++;

         print "parent.addOptionSelectBox(parent.document.YourDetails.pcode,$i,'$idPC','$namePC');\n";
         print "parent.addOptionSelectBox(parent.document.YourDetails.found_address,$i,'$idPC','$namePC');\n";
      }

      $i++;

      print <<<EOT

      parent.addOptionSelectBox(parent.document.YourDetails.pcode,$i,'   Not shown in the list','not_shown');
      parent.addOptionSelectBox(parent.document.YourDetails.found_address,$i,'   Not shown in the list','not_shown');

EOT;

      if($control)
      {
         print "PostcodeDivElement.style.visibility = 'hidden'\n";
      }

      if(empty($houseNumber))
      {
         if(! $control)
         {
            if($countPcodeElements == 3)
            {
               print "PostcodeDivElement.style.visibility = 'hidden'\n";
            }
            elseif($countPcodeElements < 11)
            {
               print <<<EOT

               parent.document.YourDetails.pcode.size = $countPcodeElements;
               PostcodeDivElement.style.top  = parent.getPosY(postcodeFrameElement) + 'px';
               PostcodeDivElement.style.left  = parent.getPosX(postcodeFrameElement) + 'px';

               PostcodeDivElement.style.visibility = 'visible';
               parent.document.YourDetails.pcode.options[0].selected = true;
               parent.document.YourDetails.pcode.focus();

EOT;
            }
            else
            {
               print <<<EOT

                  parent.document.YourDetails.pcode.size = 11;
                  PostcodeDivElement.style.top  = parent.getPosY(postcodeFrameElement) + 'px';
                  PostcodeDivElement.style.left  = parent.getPosX(postcodeFrameElement)  + 'px';

                  PostcodeDivElement.style.visibility = 'visible';
                  parent.document.YourDetails.pcode.options[0].selected = true;
                  parent.document.YourDetails.pcode.focus();
EOT;
            }
         }
      }
      else
      {
         print "parent.selectOptionSelectBox(parent.document.YourDetails.found_address,'$houseNumber')";
      }

      global $objPostCode;
      global $postcode;

      // prepare not show in the list
      foreach($resPostcode as $key => $value)
         break;

      $resGeoPostcode = $objPostCode->GeoPostCodeLookup($postcode, $key);

      preg_match("/[a-z0-9]+\s(.+)/is", $resGeoPostcode['line1'], $matches);

      $addressLine2 = $matches[1];
      $addressLine3 = $resGeoPostcode['post_town'];
      $addressLine4 = $resGeoPostcode['county'];

      print <<<EOT

         parent.document.YourDetails.address_line2.value = '$addressLine2';
         parent.document.YourDetails.address_line3.value = '$addressLine3';
         parent.document.YourDetails.address_line4.value = '$addressLine4';

         </script>

EOT;
}
?>

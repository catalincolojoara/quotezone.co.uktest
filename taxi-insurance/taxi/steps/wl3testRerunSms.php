<?php

error_reporting(0);

session_start();

$system = $_GET['system'];
$sid = $_GET['sid'];
$start_date = $_GET['start_date'];

//Testing
//$system = "campervan";
//$sid = "1382440149760599";
//$start_date = "2013-10-25";


if (!empty($system))
{
$formatted_system = trim(preg_replace('/-/', '', $system));
}





//Using the system name passed over to file the correct include files.
include("../modules/globals.inc");

include_once "File.php";
include_once "TrackUrls.php";
include_once "Tracker.php";
include_once "FileStoreClient.php";
include_once "functions.php";


if (!empty($sid))
    $fileName = substr($sid, 0, 10) . "_" . substr($sid, 10, 6);


$fileNameIn = FS_ROOT_PATH . "in/$fileName";

$file = new CFile();
$objFSClient = new CFileStoreClient(FS_SYSTEM_TYPE, 'in');

// get file
$objFSClient->GetFile($fileName);

if (!$file->Open($fileNameIn, "r"))
    $file->GetError();

if (!$size = $file->GetSize($fileNameIn))
    $file->GetError();

if (!$data = $file->Read($size))
    $file->GetError();

if (!$file->Close())
    $file->GetError();

eval("\$Session = $data;");

// if the new start date we got passed is in the future we can use it, otherwise if its in the past then we need to default to todays date for the insurance start
/*
 * Currently if passing a date far in advance its still accepted, but a user shouldnt get a date this far in advance so dont think its a problem? might be worth adding something for it anyway.
 */

$todays_date = date("Y-m-d");
if (strtotime($start_date) >= strtotime($todays_date)) {
    $explodedate = explode("-", $start_date);
    $year = $explodedate[0];
    $month = $explodedate[1];
    $day = $explodedate[2];
} else {
    $explodedate = explode("-", $todays_date);
    $year = $explodedate[0];
    $month = $explodedate[1];
    $day = $explodedate[2];
}
$Session["_YourDetails_"]["date_of_insurance_start_dd"] = $day;
$Session["_YourDetails_"]["date_of_insurance_start_mm"] = $month;
$Session["_YourDetails_"]["date_of_insurance_start_yyyy"] = $year;


//Check for additional file changes and if they exist include the file.  
$filename = "additional_questions.php";

if (file_exists($filename))
{
include ($filename);
}


$_SESSION = array();
$_SESSION = $Session;



unset($_SESSION['TEMPORARY FILE NAME']);
unset($_SESSION['LOCKED']);

redirect("../");

function redirect($url="") {
    if (!preg_match("/cs=/i", $url)) {
        if (!empty($_GET['cs']))
            $cookieSession = $_GET['cs'];

        if (!empty($_POST['cs']))
            $cookieSession = $_POST['cs'];

        if (defined('COOKIE_SESSION_ID'))
            $cookieSession = COOKIE_SESSION_ID;

        if (!empty($cookieSession)) {
            $prepUrl = $url . "?cs=$cookieSession";
            if (preg_match("/\?/i", $url))
                $prepUrl = $url . "&cs=$cookieSession";




            $url = $prepUrl;
        }
    }

    if (headers_sent()) {
        print <<<  END_TAG

               <SCRIPT LANGUAGE="JavaScript">
               <!--
               location.href="$url";
               //-->
               </SCRIPT>

END_TAG;
    } else {
        $today = date("D M j G:i:s Y");
        header("HTTP/1.1 200 OK");
        header("Date: $today");
        header("Location: $url");
        header("Content-Type: text/html");
    }

    exit(0); //if it doesn't work die (Javascript disabled?)
}
?>


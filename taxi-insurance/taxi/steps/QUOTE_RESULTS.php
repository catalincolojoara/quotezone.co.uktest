<?php
   session_start();

   error_reporting(0);

   // check the system quotes running
   $SYSTEM_LOCKED = true;

   if(! $_SESSION['LOCKED'])
   {
      $SYSTEM_LOCKED      = false;
      $_SESSION['LOCKED'] = true;
      $_SESSION['TIME']   = mktime();
   }

   $retrieveQuoteMode = false;
   if(mktime() - $_SESSION['TIME'] > 120)
      $retrieveQuoteMode = true;

   error_reporting(0);

   include_once "../modules/globals.inc";
   include_once "QzQuote.php";
   include_once "QuoteUser.php";
   include_once "QuoteUserDetails.php";
   include_once "Log.php";
   include_once "QuoteStatus.php";
   include_once "QuoteError.php";
   include_once "File.php";
   include_once "QzQuoteType.php";
   include_once "QzCookieAffiliate.php";
   include_once "Affiliates.php";
   include_once "AffiliateKeywords.php";
   include_once "Cookie.php";
   include_once "Tracking.php";
   include_once "QzProtection.php";
   include_once "functions.php";
   include_once "SiteTesting.php";
   include_once "EmailStore.php";
   include_once "EmailStoreCodes.php";
   include_once "errors.inc";
   include_once "QuoteServerLogs.php";
   include_once "FileStoreClient.php";
   include_once "SMTP.php";
   include_once "YourDetailsElements.php";
   include_once "TrackDetailsNoJavascript.php";
   include_once "TrackDetailsNoCookies.php";
   include_once "OvertureTracker.php";
   include_once "visitorsTracking.php";
   include_once "Site.php";
   include_once "/home/www/quotezone.co.uk/common/modules/Logger.php";
   include_once "/home/www/quotezone.co.uk/common/modules/LeadValidityRates.php";

   include_once "EmailOutbounds.php";
   include_once "EmailOutboundsEngine.php";
   include_once "EmailOutboundsEngineTaxi.php";

   include_once "/home/www/quotezone.co.uk/common/modules/CompanyConfig.php";

   //esvtracker
   include_once "/home/www/quotezone.co.uk/insurance-new/modules/EsvTracker.php";

   //Insurers Rotation
   include_once "/home/www/quotezone.co.uk/common/modules/InsurersRotation.php";

   //Success leads limit - this is used in showsites
   include_once "/home/www/quotezone.co.uk/common/modules/SiteLeadsLimit.php";

   //new filters messages
   include_once "/home/www/quotezone.co.uk/common/modules/LeadQuoteFilters.php";

   //new time log (last page load time)
   include_once "/home/www/quotezone.co.uk/common/modules/LogLeadQuoteTimes.php";

   // set image level
   $imageLevel = 1;

   setQzProtection();

   $objFile                     = new CFile();
   $dbHandle                    = $objWs->GetDbHandle();
   $objQuoteStatus              = new CQuoteStatus($dbHandle);
   $objQuoteError               = new CQuoteError($dbHandle);
   $objQzCookieAffiliates       = new CQzCookieAffiliate($dbHandle);
   $objAffiliate                = new CAffiliates($dbHandle);
   $objAffiliateKeywords        = new CAffiliateKeywords();
   $objCookie                   = new CCookie($dbHandle);
   $objQzQuoteType              = new CQzQuoteType($dbHandle);
   $objLog                      = new CLog($dbHandle);
   $objQzQuote                  = new CQzQuote($dbHandle);
   $objQuoteUser                = new CQuoteUser($dbHandle);
   $objQuoteUserDetails         = new CQuoteUserDetails($dbHandle);
   $objOvertureTracker          = new COvertureTracker($dbHandle);
   $objEmailStore               = new CEmailStore();
   $objEmailStoreCodes          = new CEmailStoreCodes();
   $objFSClient                 = new CFileStoreClient(FS_SYSTEM_TYPE,"in");
   $objQuoteServerLogs          = new CQuoteServerLogs();
   $mailObj                     = new CSMTP();
   $objTrackDetailsNoJavascript = new CTrackDetailsNoJavascript();
   $objTrackDetailsNoCookies    = new CTrackDetailsNoCookies();
   $objEsvTracker               = new CEsvTracker($dbHandle);
   $objSite                     = new CSite($dbHandle);
   $objValidityRate             = new CLeadValidityRate($dbHandle);
   $objLeadQuoteFilters         = new CLeadQuoteFilters($dbHandle);

   $objLogLeadQuoteTimes        = new CLogLeadsQuoteTimes($dbHandle);

   $objCompanyConfig            = new CCompanyConfig($dbHandle);


   //Logger for quote results lag
   $objLogger                   = new CLogger(ROOT_PATH."QuoteResultsResponse.log");

function Show()
{
   global $resSite;
   global $imageLevel;
   global $objTmpl;
   global $objWs;
   global $objMenu;
   global $resParameters;
   global $postParameters;
   global $objFile;
   global $dbHandle;
   global $objQuoteStatus;
   global $objQuoteError;
   global $objQzCookieAffiliates;
   global $objAffiliate;
   global $objCookie;
   global $objQzQuoteType;
   global $objLog;
   global $objQzQuote;
   global $objQuoteUser;
   global $objQuoteUserDetails;
   global $objEmailStore;
   global $testingSiteID;
   global $objQuoteServerLogs;
   global $mailObj;
   global $SYSTEM_LOCKED;
   global $getQuote;
   global $objEsvTracker;
   global $optInSitesArray;
   global $objSite;
   global $objValidityRate;
   global $objLogger;
   global $objLogLeadQuoteTimes;
   global $objCompanyConfig;
   global $arrIncSiteIds;

   $startTimeLoger = time();

   //taxi
   $quoteTypeID = '18';

   if(LOGGING_MODE)
   {
      $objLogger->Info("Start QR");

      if($_SERVER['REMOTE_ADDR'] == '86.125.114.56')
         $objLogger->Info("ACRUX test");

      $startTimeStamp = time();
   }


//INC FILES ======================================================================
   if (!$dirHandle1 = opendir("sites/config"))
      $errCannotOpenSites = "Cant open sites/config directory";

   $allSitesIncluded1 = array();
   while ($fileName1 = readdir($dirHandle1))
   {
      if (is_dir("sites/config/" . $fileName1))
         continue;

      // only php files
      if (!preg_match("/\.inc$/", $fileName1))
         continue;

      array_push($allSitesIncluded1, $fileName1);
   }

   $_SESSION['SHOW_SITES'] = array();

   if(! $SYSTEM_LOCKED)
      $_SESSION['REQUEST_SITES'] = array();

   $currentDirectory = getcwd();
   $getQuote         = array();
   $getQuoteErr      = array();

   unset($_SESSION['NOT_SKIPPED_SITES']);

   $_SESSION['SHOW_SITES'] = array();

   if(! $SYSTEM_LOCKED)
      $_SESSION['REQUEST_SITES'] = array();
   $currentDirectory = getcwd();

   // array for config files for companies
   $arrIncSiteIds = array();

   //here we see which site is on or off , has filter condifion, reached its limits and any other restrictions that it may have
   //so it is eligible to receive leads
   foreach($allSitesIncluded1 as $index => $siteFileName)
   {
      //set the site online as default
      $_resSite['offline'] = false;
      $siteID              = "";
      $_companyDetails     = array();

      //include the files in the config directory
      include_once "$currentDirectory/sites/config/$siteFileName";

      //set the site_id in a variable
      $siteID = $_companyDetails['plugin_details']['siteID'];

      $_SESSION['REQUEST_SITES'][$siteID] = 1;
      
      //print "<br>siteID:".$siteID;

      $_SESSION["_COMPANY_DETAILS_"][$siteID]["LOGO_ID"]              = $_companyDetails['plugin_details']['logoID'];
      $_SESSION["_COMPANY_DETAILS_"][$siteID]["COMPANY_NAME"]         = $_companyDetails['plugin_details']['companyName'];
      
      $_SESSION["_COMPANY_DETAILS_"][$siteID]["PROMO_TEXT"]           = $_companyDetails['plugin_details']['promoText'];
      $_SESSION["_COMPANY_DETAILS_"][$siteID]["COMPANY_PHONE_NUMBER"] = $_companyDetails['plugin_details']['companyPhoneNumber'];
      $_SESSION["_COMPANY_DETAILS_"][$siteID]["CALL_OPENING_HOURS"]   = $_companyDetails['plugin_details']['callOpeningHours'];
      $_SESSION["_COMPANY_DETAILS_"][$siteID]["EMAILSUBJECT"]         = ($_companyDetails['plugin_details']['emailSubject']) ? $_companyDetails['plugin_details']['emailSubject'] : "";
      
      if($objCompanyConfig->CheckCompanyQuoteDetails($siteID, $_companyDetails,$_SESSION))
      { 
         $_SESSION['NOT_SKIPPED_SITES'][$siteID] = 1;

         //add details to session
         $_SESSION["_COMPANY_DETAILS_"][$siteID]["EMAILS"]               = $_companyDetails['emails']['MAIL'];
         
         $_SESSION["_COMPANY_DETAILS_"][$siteID]["TXT_EMAILS"]           = $_companyDetails['emails']['TXT_MAIL'];      // txt emails
         $_SESSION["_COMPANY_DETAILS_"][$siteID]["TXT"]                  = $_companyDetails['emails']['TXT'];      // txt emails

         if(isset($_companyDetails['request']['zip_email']))
            $_SESSION["_COMPANY_DETAILS_"][$siteID]["REQUEST"]              = $_companyDetails['request']['zip_email']; //this treats different requests
         elseif(isset($_companyDetails['request']['leadcall']))
            $_SESSION["_COMPANY_DETAILS_"][$siteID]["REQUEST"]              = $_companyDetails['request']['leadcall']; //we set the special request here LEADCALL

         //we set the special request here XML
         $_SESSION["_COMPANY_DETAILS_"][$siteID]["REQUEST_XML"]          = $_companyDetails['request']['xml'];

         //we set the special request here .CSV files
         $_SESSION["_COMPANY_DETAILS_"][$siteID]["REQUEST_CSV"]          = $_companyDetails['request']['csv'];
         
         $_SESSION["_COMPANY_DETAILS_"][$siteID]["EMAILS_TEXT"]          = $_companyDetails['emails']['MAIL_TEXT'];
         

         //set in session testing informations
         $_SESSION["_COMPANY_DETAILS_"][$siteID]['testing_company']   = $_companyDetails['plugin_details']['testing_company'];
         $_SESSION["_COMPANY_DETAILS_"][$siteID]['testing_marketing'] = $_companyDetails['plugin_details']['testing_marketing'];
         $_SESSION["_COMPANY_DETAILS_"][$siteID]['send_QA_team']      = $_companyDetails['plugin_details']['send_QA_team'];

         $getQuote[$siteID] = true;

      }//end if($objCompanyConfig->CheckCompanyQuoteDetails($siteID, $_companyDetails))   
      
      //add details to session
      $_SESSION['SHOW_SITES'][$siteID] = $siteFileName;

      $noOfSitesNotSkipped = count($_SESSION['NOT_SKIPPED_SITES']);   
      
      $arrIncSiteIds[] = $siteID;
      
   }//foreach($allSitesIncluded as $index => $siteFileName)

   //end inc files ===================================================================




   if(! $dirHandle = opendir("sites/showsites"))
      print "Cant open sites directory";

   $allSitesIncluded = array();

   while($fileName = readdir($dirHandle))
   {
      if(is_dir("sites/showsites/".$fileName))
         continue;

      // only php files
      if(! preg_match("/\.php$/", $fileName))
        continue;

      array_push($allSitesIncluded, $fileName);
   }

//    $_SESSION['SHOW_SITES'] = array();
// 
//    if(! $SYSTEM_LOCKED)
//       $_SESSION['REQUEST_SITES'] = array();
// 
//    $currentDirectory = getcwd();
//    $getQuote         = array();
//    $getQuoteErr      = array();

   if(LOGGING_MODE)
      $objLogger->Info("Starting showsites/skipsites processing");

//    unset($_SESSION['NOT_SKIPPED_SITES']);

   foreach($allSitesIncluded as $index => $siteFileName)
   {
      include_once "$currentDirectory/sites/showsites/$siteFileName";

      if(in_array($_resSite['siteID'], $arrIncSiteIds))
         continue;

      $_SESSION['SHOW_SITES'][$_resSite['siteID']] = $siteFileName;

      // testing sites
      if(! empty($testingSiteID))
      {
         $_resSite['offline'] = false;
         if($testingSiteID != $_resSite['siteID'])
            $_resSite['offline'] = true;

         if(! empty($_resSite['siteOrigID']))
           if($testingSiteID == $_resSite['siteOrigID'])
               $_resSite['offline'] = false;
      }

      // escape offline sites
      if($_resSite['offline'])
      {
         //remove OptIn offline sites from $optInSitesArray  
         $optInSitesArray = array_diff($optInSitesArray,array($_resSite['siteID'])); 
         continue;
      }
      if(! empty($_resSite['siteOrigID']))
      {
         $siteID = $_resSite['siteID'];
         $_resSite['siteID'] = $_resSite['siteOrigID'];
      }

      // in case of retrieve by SID
      if(empty($testingSiteID))
         if($SYSTEM_LOCKED && $retrieveQuoteMode)
            if(! $objQuoteStatus->ExistQuoteStatusSite($_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id'], $_resSite['siteID']))
               continue;

      if(! empty($_resSite['siteOrigID']))
      {
         $_resSite['siteID'] = $siteID;
      }

      $getQuote[$_resSite['siteID']] = true;

      // we have a fake site
      if(! empty($_resSite['siteOrigID']))
         $getQuote[$_resSite['siteID']] = false;

      if(! empty($_SESSION['DEBUG_SITES']))
         if(empty($_SESSION['DEBUG_SITES'][$_resSite['siteID']]))
            $getQuote[$_resSite['siteID']] = false;

      $fileHande = $objFile->Open("$currentDirectory/sites/skipsites/$siteFileName", "r");

      $skipFileContent = $objFile->Read(filesize("$currentDirectory/sites/skipsites/$siteFileName"));

      $objFile->Close();

      $skipFileContent = preg_replace("/\<\?php/","", $skipFileContent);
      $skipFileContent = preg_replace("/\?\>/","", $skipFileContent);

      if(empty($skipFileContent))
         $skipFileContent = "return true;";

      $skipSites = create_function('&$personalizedError', $skipFileContent);

      // DEBUG MODE
      $personalizedError = array();
      if($skipSites($personalizedError))
      {
         $getQuote[$_resSite['siteID']] = false;
         $getQuoteErr[$_resSite['siteID']] = $personalizedError;
      }
      else
      {
         if(! in_array($_resSite['siteID'],$optInSitesArray))
            $_SESSION['NOT_SKIPPED_SITES'][$_resSite['siteID']] = 1;
      }

      //to get no quote
      //if($_SERVER['REMOTE_ADDR'] == "86.125.114.56")
      //    $getQuote[$_resSite['siteID']] = false;

      //if we have a OptIn sites this must be excluded from rotation
      if(! in_array($_resSite['siteID'],$optInSitesArray))
         $_SESSION['REQUEST_SITES'][$_resSite['siteID']] = 1;

      $noOfSitesNotSkipped = count($_SESSION['NOT_SKIPPED_SITES']);
      
      $quoteFrameContent = "QuoteFrameContent.tmpl";

      if(! $objTmpl->Load($quoteFrameContent))
         print $objTmpl->GetError();

      $resFrameSite['IMAGE_SITE_REMOTE'] = $_resSite['imageSiteRemote'];
      $resFrameSite['SITE_ID']           = $_resSite['siteID'];
      $resFrameSite['IMAGE_PROCESSING']  = $_resSite['imageProcessing'];
      $resFrameSite['SITE_LINK']         = $_resSite['link'];
      $resFrameSite['SITE_QUOTE']        = $_resSite['siteQuoteMessage'];
      $resFrameSite['SITE_NAME']         = $_resSite['siteName'];

      unset($_resSite);
      $objTmpl->Prepare($resFrameSite);

      $frameContent .= $objTmpl->GetContent();

   }//foreach($allSitesIncluded as $index => $siteFileName)

   if(LOGGING_MODE)
      $objLogger->Info("End showsites/skipsites processing");

   ///////////////////////////////////////
   // INSURERS ROTATION
   //////////////////////////////////////
   // rotation exclude Acrux IP 
   if($_SERVER['REMOTE_ADDR'] != '86.125.114.56')
   {
      BrightsideRotation();
      MarkerStudyRotation();
      CoversureRotation();
      ThinkRotation();
      DNARotation();
      BarryGraingerRotation();
      BroadsureDirect();
      PatonsInsuranceRotation();
      OneAnswerInsuranceRotation();

      //rebuild request_sites from database for sid only
      if($SYSTEM_LOCKED)
      {
         unset($_SESSION['REQUEST_SITES']);

         $activeSites = $objQuoteStatus->GetAllQuoteStatusByLogID($_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id']);

         foreach($activeSites as $site)
         {
            if ($site['status'] == 'SKIPPED')
               continue;

            if ($site['status'] == 'WAITING')
               continue;

            $_SESSION['REQUEST_SITES'][$site['site_id']] = 1;
            $getQuote[$site['site_id']] = 1;
         }
      }
      else
      {

         $insurersLimit = 5;

         $requestSites = array();
         if($requestSites = RotateInsurers($quoteTypeID,$getQuote,$_SESSION['REQUEST_SITES'],$insurersLimit,$_SESSION['_YourDetails_']['first_name'],$_SESSION['_YourDetails_']['surname'], $_SESSION['_YourDetails_']['date_of_birth']))
         {
            if(!empty($requestSites))
            {
               unset($_SESSION['REQUEST_SITES']);
               $_SESSION['REQUEST_SITES'] = $requestSites;
            }
         }
       
         if(LOGGING_MODE)
            $objLogger->Info("End system Rotation");
      }
   }

   //after rotation include OptIn sites that were not skipped
   foreach($optInSitesArray as $OptInSiteID)
   {
       $_SESSION['REQUEST_SITES'][$OptInSiteID] = 1;
   }

   //calculate ESV amount
   if(! $SYSTEM_LOCKED)
   {
      $leadAmount = 0;

      foreach ($getQuote as $key=>$value)
      {
         if(! $_SESSION['REQUEST_SITES'][$key])
            continue;

         if ($value != 1)
            continue;

         //updated esv_price to lead_price as instructed in ticket : WRC5-LA6
         if($siteDetails = $objSite->GetSite($key))
            $leadAmount += $siteDetails["lead_price"];
      }

      if($rateDetails = $objValidityRate->GetValidityRatesByDate(date("Y-m-d"),$quoteTypeID))
      {
        $validityRate = $rateDetails[$quoteTypeID]['validity_rate'];

        $esvAmount = number_format($leadAmount * ($validityRate),2);

        $_SESSION['_QZ_QUOTE_DETAILS_']['esv_amount'] = $esvAmount;
      }
   }

   if(LOGGING_MODE)
      $objLogger->Info("Start processquotedetails");

   // quote searching
   processQuoteDetails();

   if(LOGGING_MODE)
      $objLogger->Info("End processquotedetails");

   $resSite['QUOTE_FRAME_CONTENT'] = $frameContent;
   
   //to get no quote
   //if($_SERVER['REMOTE_ADDR'] == "86.125.114.56")
   //   $noOfSitesNotSkipped = 0;

   //optIn sites were not counted!!!!!
   if($noOfSitesNotSkipped == 0)
      showAdvertise($resParameters);
   else
      showSystemsAdvertise($resParameters);

    //showAdvertise($resParameters);
   ////////////////////////////////////////////////////////////////////////////////
   // Here we have the inside information. We show the insurers logos
   ////////////////////////////////////////////////////////////////////////////////
   global $logosInformationArray;
   global $logoInformationSystem;
   $insideContent = "";

//   if ($_SERVER['REMOTE_ADDR'] == '86.125.114.56')
   {
   foreach ($getQuote as $key=>$value)
   {
      //do not show OptIn sites on the last page !!!!
      if(in_array($key,$optInSitesArray))
         continue;

      if(! $_SESSION['REQUEST_SITES'][$key])
         continue;

      if ($value == 1)
      {
         $arrayData = array();
         if(in_array($key, $arrIncSiteIds))
         {
            $arrayData['IMAGE_LOGO_PATH'] = WEB_SITE_ROOT . "/images/logos/" . $logosInformationArray["brokers"][$_SESSION["_COMPANY_DETAILS_"][$key]["LOGO_ID"]]["logo"];
            $arrayData['INSURER_NAME']    = $logosInformationArray["brokers"][$_SESSION["_COMPANY_DETAILS_"][$key]["LOGO_ID"]]["name"];
         }
         else
         {
            $arrayData['IMAGE_LOGO_PATH'] = WEB_SITE_ROOT . "/images/logos/" . $logosInformationArray['brokers'][$logoInformationSystem[$key]]['logo'];
            $arrayData['INSURER_NAME'] = $logosInformationArray['brokers'][$logoInformationSystem[$key]]['name'];
         }
         $objTmpl->Load("YourQuotesContent.tmpl");
         $objTmpl->Prepare($arrayData);
         $insideContent .= $objTmpl->GetContent();
      }
   }
   }

   // Don not show telephone div if: E, EC, N, NW, SE, SW, W, WC, BT postcodes, PCO plating authority and driver under 25 years
   $resParameters['telephone_div_display'] = "display: block;";

   $postCodePref = $_SESSION['_YourDetails_']['postcode_prefix'];

   $skipPostCodePrefixArray = array(
      "B1",
      "B10",
      "B11",
      "B12",
      "B13",
      "B14",
      "B15",
      "B16",
      "B17",
      "B18",
      "B19",
      "B2",
      "B20",
      "B21",
      "B22",
      "B23",
      "B24",
      "B25",
      "B26",
      "B27",
      "B28",
      "B29",
      "B3",
      "B30",
      "B31",
      "B32",
      "B33",
      "B34",
      "B35",
      "B36",
      "B37",
      "B38",
      "B39",
      "B4",
      "B40",
      "B41",
      "B42",
      "B43",
      "B44",
      "B45",
      "B46",
      "B47",
      "B48",
      "B5",
      "B6",
      "B7",
      "B8",
      "B9",
      "B90",
      "B91",
      "B92",
      "B93",
      "B94",
      "B95",
      "B99",
      "BD1",
      "BD10",
      "BD11",
      "BD12",
      "BD13",
      "BD14",
      "BD15",
      "BD2",
      "BD3",
      "BD4",
      "BD5",
      "BD6",
      "BD7",
      "BD8",
      "BD9",
      "BD99",
      "FY0",
      "FY1",
      "FY2",
      "FY3",
      "FY4",
      "G1",
      "G10",
      "G11",
      "G12",
      "G13",
      "G14",
      "G15",
      "G16",
      "G17",
      "G18",
      "G19",
      "G2",
      "G20",
      "G21",
      "G22",
      "G23",
      "G24",
      "G25",
      "G26",
      "G27",
      "G28",
      "G29",
      "G3",
      "G30",
      "G31",
      "G32",
      "G33",
      "G34",
      "G35",
      "G36",
      "G37",
      "G38",
      "G39",
      "G4",
      "G40",
      "G41",
      "G42",
      "G43",
      "G44",
      "G45",
      "G47",
      "G48",
      "G49",
      "G5",
      "G50",
      "G51",
      "G52",
      "G53",
      "G54",
      "G55",
      "G56",
      "G57",
      "G58",
      "G59",
      "G6",
      "G7",
      "G70",
      "G79",
      "G8",
      "G9",
      "G90",
      "LS1",
      "LS10",
      "LS11",
      "LS12",
      "LS13",
      "LS14",
      "LS15",
      "LS16",
      "LS17",
      "LS18",
      "LS19",
      "LS2",
      "LS3",
      "LS4",
      "LS5",
      "LS6",
      "LS7",
      "LS8",
      "LS88",
      "LS9",
      "LS98",
      "LS99",
      "LU1",
      "LU2",
      "LU3",
      "LU4",
      "LU5",
      "SL1",
      "SL2",
      "SL3",
      "SL60",
      "SL95",
      "WF1",
      "WF2",
      "WF3",
      "WF4",
      "SE1",
      "SE2",
      "SE3",
      "SE4",
      "SE5",
      "SE6",
      "SE7",
      "SE8",
      "SE9",
      "SE10",
      "SE11",
      "SE12",
      "SE13",
      "SE14",
      "SE15",
      "SE16",
      "SE17",
      "SE18",
      "SE19",
      "SE20",
      "SE21",
      "SE22",
      "SE23",
      "SE24",
      "SE25",
      "SE26",
      "SE27",
      "SE28",
      "SW1",
      "SW2",
      "SW3",
      "SW4",
      "SW5",
      "SW6",
      "SW7",
      "SW8",
      "SW9",
      "SW10",
      "SW11",
      "SW12",
      "SW13",
      "SW14",
      "SW15",
      "SW16",
      "SW17",
      "SW18",
      "SW19",
      "SW20",
      "W1",
      "W2",
      "W3",
      "W4",
      "W5",
      "W6",
      "W7",
      "W8",
      "W9",
      "W10",
      "W11",
      "W12",
      "W13",
      "W14",
      "E1",
      "E2",
      "E3",
      "E4",
      "E5",
      "E6",
      "E7",
      "E8",
      "E9",
      "E10",
      "E11",
      "E12",
      "E13",
      "E14",
      "E15",
      "E16",
      "E17",
      "E18",
      "WC1",
      "WC2",
      "EC1",
      "EC2",
      "EC3",
      "EC4",
      "N1",
      "N2",
      "N3",
      "N4",
      "N5",
      "N6",
      "N7",
      "N8",
      "N9",
      "N10",
      "N11",
      "N12",
      "N13",
      "N14",
      "N15",
      "N16",
      "N17",
      "N18",
      "N19",
      "N20",
      "N21",
      "N22",
      "NW1",
      "NW2",
      "NW3",
      "NW4",
      "NW5",
      "NW6",
      "NW7",
      "NW8",
      "NW9",
      "NW10",
      "NW11",
   );

   if(in_array($postCodePref,$skipPostCodePrefixArray))
   {
      $resParameters['telephone_div_display'] = "display: none;";
   }

   $platingAuthority = $_SESSION['_YourDetails_']['plating_authority'];
   if($platingAuthority == "LONDON PCO")
   {
      $resParameters['telephone_div_display'] = "display: none;";
   }

   $DISdd   = $_SESSION['_YourDetails_']['date_of_insurance_start_dd'];
   $DISmm   = $_SESSION['_YourDetails_']['date_of_insurance_start_mm'];
   $DISyyyy = $_SESSION['_YourDetails_']['date_of_insurance_start_yyyy'];

   $dobDD   = $_SESSION['_YourDetails_']['date_of_birth_dd'];
   $dobMM   = $_SESSION['_YourDetails_']['date_of_birth_mm'];
   $dobYYYY = $_SESSION['_YourDetails_']['date_of_birth_yyyy'];

   $proposerBirthDate  = $dobYYYY."-".$dobMM."-".$dobDD;
   $insuranceStartDate = $DISyyyy."-".$DISmm."-".$DISdd;

   $propAge = GetAge($proposerBirthDate,$insuranceStartDate);

   if($propAge < 25)
   {
      $resParameters['telephone_div_display'] = "display: none;";
   }


   if($noOfSitesNotSkipped != 0)
   {
      $resParameters['RESULT_HEADER_TITLE']  = "Thank you for using Quotezone.co.uk";
      $resParameters['RESULT_HEADER_TEXT']   = "Your information has been passed onto the following panel of Taxi Insurance specialists who should be in contact shortly:";
      $resParameters['RESULT_CONTENT_IMAGES']= $insideContent;
      
      $resParameters['telephone_message'] = "Not happy waiting for companies to call you?";
   }
   else
   {
      $resParameters['RESULT_HEADER_TITLE']  = "Thank you for using Quotezone.co.uk";
      $resParameters['RESULT_HEADER_TEXT']   = "Due to the nature of your risk, online quotes are not available at this time. There are other specialist insurers who may be able to provide online quotes.<br>";
      $resParameters['RESULT_CONTENT_IMAGES'] = "<tr><td style='padding-left:29px; padding-top:29px;'><b><NOBR>Please select Visit Site on any of the options below:</NOBR></b></td></tr>";

      $resParameters['telephone_message'] = "Online quotes are not available at this time.";
   }


   ////////////////////////////////////////////////////////////////////////////////

   if(! $objTmpl->Load("YourQuotes.tmpl"))
         print $objTmpl->GetError()."\n";


   if(! $objMenu->ExistSessionEntry('_QUOTE_RESULTS_'))
   {
      $objTmpl->SetRunMode(1);

      $resYourDetailsPrepare = array();

      $resYourDetailsPrepare['QUOTE_FRAME_CONTENT'] = $resSite['QUOTE_FRAME_CONTENT'];

        if(LOGGING_MODE)
         $objLogger->Info("Start EMV");

        //send emailvision data
        sendEmailOutboundsEmailVision();

        if(LOGGING_MODE)
         $objLogger->Info("End EMV"); 

        //set search vision ads
        setSearchVisionAdds();


        // check if this user is unique for the last 30 days, on this system 
        if(! $SYSTEM_LOCKED)
        { 
           if($objValidityRate->GetUnicity($_SESSION["_QZ_QUOTE_DETAILS_"]["quote_user_id"],$quoteTypeID,30))
              setGoogleAdds();
        }

        $dateOfBirthMonth = $_SESSION["_YourDetails_"]["date_of_birth_mm"];
        $dateOfBirthDay   = $_SESSION["_YourDetails_"]["date_of_birth_dd"];
        $dateOfBirthYear  = $_SESSION["_YourDetails_"]["date_of_birth_yyyy"];

        $userIDPiwik = $_SESSION["_QZ_QUOTE_DETAILS_"]["quote_user_id"];
        $title       = $_SESSION["_YourDetails_"]["title"];
        $gender = "F";
        if($title == "Mr")
           $gender = "M";
        $genderPiwik = $gender;
        $dobPiwik    = $dateOfBirthYear."-".$dateOfBirthMonth."-".$dateOfBirthDay;

        $userTracking = "var user = {
                             'userid' : '$userIDPiwik',
                             'gender' : '$genderPiwik',
                             'birthdate': '$dobPiwik'
                             };
                             piwikTracker.setCustomData(user);";

        $objWs->SetPiwikTracker("/virtual/taxi-insurance/quote_generated",$userTracking);

        //esv tracker
        if(! $SYSTEM_LOCKED)
        {
           if(isset($_COOKIE['adkey']) AND trim($_COOKIE['adkey']) != "")
           {
              $qzQuoteID = $_SESSION["_QZ_QUOTE_DETAILS_"]["qz_quote_id"];

              $adKeyData = $objEsvTracker->GetAdKeyByKey($_COOKIE['adkey']);
              $adKeyID = $adKeyData['id'];

              if (!$adKeyID)
                 $adKeyID = $objEsvTracker->AddAdKey($_COOKIE['adkey']);
           }

           if($qzQuoteID AND $adKeyID)
              $objEsvTracker->AddAdKeyQzQuote($qzQuoteID, $adKeyID);
        }

      $objTmpl->Prepare($resYourDetailsPrepare);
      $objTmpl->SetRunMode(2);

      $objWs->SetMetaTag("robots", "noindex,nofollow");

      //SET NEW GOOGLE TRACKER
      $objWs->SetGoogleTracker('/taxi-quote/step2/quote-results/');

      $objTmpl->Prepare($resParameters);

      //$objWs->SetGoogleTracker("urchinTracker('/funnel_B1/BikeQuote-StepBike1.htm');");

      // log the start of the quote by inserting a row in the quote_web_server table with the confirmed field as N
      // after the last page is displayed, the table will be updated with the confirmed field as Y
      $webServerIP = $_SERVER["SERVER_ADDR"];
      $logID       = $_SESSION["_QZ_QUOTE_DETAILS_"]["qz_log_id"];
      $quoteTypeID = 18;

      if(! $SYSTEM_LOCKED)
         $objQuoteServerLogs->AddQuoteServerLog($logID,$quoteTypeID,$webServerIP,'N');

      // Disabled by ticket ID : OBI-230414
      //setUniqueQuotesTracker();

      if(LOGGING_MODE)
      {
         $objLogger->Info("End QR");
         $endTimeStamp = time();

         $totalTime = date("i:s", $endTimeStamp-$startTimeStamp);

         $objLogger->Info("TOTAL QR TIME(min:sec): ".$totalTime);
      }

      $objWs->SetImageHeaderLevel($imageLevel);

      $objWs->SetWorkArea($objTmpl->GetContent());

      $objWs->Run();

      lanchQuotes($getQuote, $getQuoteErr);

      showQuoteReference($resSite);

   } // end set default radiobox elements

   if(!$SYSTEM_LOCKED)
   {
      //end process logger
      $endProcessLoger = time();
      
      $timePassedLogger = $endProcessLoger - $startTimeLoger;

      $serverIP  = $_SERVER["SERVER_ADDR"];
      $logedDate = date("Y-m-d");

      $objLogLeadQuoteTimes->AddLeadQuoteTimesToDb($quoteTypeID,$logID,$timePassedLogger,$serverIP,$logedDate);

   }



}

function ValidateExtraErrors(&$resParameters)
{

   $error = false;


   if($error)
      return false;

   return true;
}

function lanchQuotes($getQuote, $getQuoteErr)
{
   global $SYSTEM_LOCKED;
   global $objQuoteStatus;
   global $arrIncSiteIds;

   if($SYSTEM_LOCKED)
      return;

   foreach($_SESSION['REQUEST_SITES'] as $siteID => $flag)
   {
      // skip site
      if(! $getQuote[$siteID])
      {
         // add to db quote status skipped
         skipSite($siteID, $getQuoteErr[$siteID]);
         continue;
      }

      $debugMode = 0;
      $scanningFlag = 0;
      $quoteType = 'NORMAL';

      if($_SESSION['DEBUG_SITES'][$siteID])
      {
         $quoteType = 'DEBUG';
         $debugMode = 1;
      }

      $testingMode = 0;// real url init
      if(! empty($_SESSION['TESTING SITE ID']))
      {
         if(! empty($_SESSION['TESTING SITE FAKE']))
            $testingMode = 1;// test url init
      }

      $incVersion = 0;
      if(in_array($siteID, $arrIncSiteIds))
         $incVersion = 1;
      //aici exec
      exec('/usr/bin/php -q steps/GetQuote.php '.$siteID.' '.$_SESSION['TEMPORARY FILE NAME'].' '.$scanningFlag.' '.$debugMode.' '.$testingMode.' 0 '.$incVersion.'  > quote.log 2>&1 &');

   }
}

function BrightsideRotation()
{
   global $getQuote;

   $mainSiteID   = 672; // Brightside
   $secondSiteID = 703; // Click4Gap

   //(BCF-222659)
   //Click4Gap will skip if Brightside doesn't skip
   if($getQuote[$mainSiteID] && $getQuote[$secondSiteID])
   {
      $getQuote[$secondSiteID] = false;
      $getQuoteErr[$secondSiteID] = "Click4Gap will not receive leads if Brightside is not skipped.";
      unset($_SESSION['NOT_SKIPPED_SITES'][$secondSiteID]);
   }

}

function MarkerStudyRotation()
{
   global $getQuote;

   $mainSiteID   = 361; // Marker Study 1
   $secondSiteID = 775; // Marker Study 2

   //MS2 will get leads only if MS1 skip
   if($getQuote[$mainSiteID] && $getQuote[$secondSiteID])
   {
      $getQuote[$secondSiteID] = false;
      $getQuoteErr[$secondSiteID] = "Marker Study 2 will not receive leads if Marker Study 1 is not skipped.";
      unset($_SESSION['NOT_SKIPPED_SITES'][$secondSiteID]);
   }

}

function CoversureRotation()
{
   global $quoteTypeID;
   global $SYSTEM_LOCKED;
   global $objQuoteStatus;
   global $getQuote;
   global $objQuoteUser;

   //rebiuld request_sites from database for sid only
   if ($SYSTEM_LOCKED)
   {
      $activeSites = $objQuoteStatus->GetAllQuoteStatusByLogID($_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id']);
      unset($_SESSION['REQUEST_SITES']);
      foreach($activeSites as $site)
      {
         if ($site['status'] == 'SKIPPED')
            continue;

         if ($site['status'] == 'WAITING')
            continue;

         $_SESSION['REQUEST_SITES'][$site['site_id']] = 1;
         $getQuote[$site['site_id']] = 1;
      }

      return;
   }

   $objSite  = new CSite();

   $brokerID = 19; // Coversure

   // getting the list of Coversure insurers
   $coversureArray = $objSite->GetBrokerSites($brokerID, $quoteTypeID);

  //rebuild request_sites from database with the company the user had quote before (in case if he had one)
   if( $userDetails = $objQuoteUser->GetUserByFNameSNameDateOfBirth($_SESSION['_YourDetails_']['first_name'], $_SESSION['_YourDetails_']['surname'], $_SESSION['_YourDetails_']['date_of_birth']))
   {
      $quoteUserID = $userDetails['id'];
      $lastCompaniesSent = $objQuoteStatus->GetLastQuoteStatusByQuoteUserID($quoteUserID, $brokerID);
   }

   if (is_array($lastCompaniesSent) AND !empty($lastCompaniesSent))
   {
      foreach ($lastCompaniesSent as $companySiteId)
      {
        // check if company belong to the searched group
        if(array_key_exists($companySiteId, $coversureArray))
        {
           if(array_key_exists($companySiteId,$_SESSION['REQUEST_SITES']) && $getQuote[$companySiteId])
           {
              foreach($coversureArray as $id => $name)
              {
                 if($id != $companySiteId)
                    unset($_SESSION['REQUEST_SITES'][$id]);
              }

              return;
           }
        }
      }
   }

    // removing the coversure insurers that are not included
   foreach($coversureArray as $id => $name)
   {
      if(! $_SESSION['REQUEST_SITES'][$id])
         unset($coversureArray[$id]);
   }

   // getting the last entry from quote_status table for each coversure insurer; the biggest id means the last insurer with success
   // init the priority list
   $lastSuccessSiteID = 0;
   $lastQuoteStatusID = 0;
   $coversurePriorityList = array();
   foreach($coversureArray as $id => $name)
   {
      array_push($coversurePriorityList, $id);

      // removing all coversure insurers from the request sites list
      unset($_SESSION['REQUEST_SITES'][$id]);

      if(! $qsID = $objQuoteStatus->GetLastSiteSuccess($id))
         continue;

      if($qsID > $lastQuoteStatusID)
      {
         $lastQuoteStatusID = $qsID;
         $lastSuccessSiteID = $id;
      }
   }

   // ordering the priority list
   foreach($coversureArray as $id => $name)
   {
      array_shift($coversurePriorityList);
      array_push($coversurePriorityList, $id);

      if($id == $lastSuccessSiteID)
         break;
   }

   foreach($coversurePriorityList as $index => $siteID)
   {
      $_SESSION['REQUEST_SITES'][$siteID] = 1;

      if($getQuote[$siteID])
         break;
   }

}

function ThinkRotation()
{
   global $quoteTypeID;
   global $SYSTEM_LOCKED;
   global $objQuoteStatus;
   global $getQuote;
   global $objQuoteUser;

   //rebiuld request_sites from database for sid only
   if ($SYSTEM_LOCKED)
   {
      $activeSites = $objQuoteStatus->GetAllQuoteStatusByLogID($_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id']);
      unset($_SESSION['REQUEST_SITES']);
      foreach($activeSites as $site)
      {
         if ($site['status'] == 'SKIPPED')
            continue;

         if ($site['status'] == 'WAITING')
            continue;

         $_SESSION['REQUEST_SITES'][$site['site_id']] = 1;
         $getQuote[$site['site_id']] = 1;
      }

      return;
   }

   $objSite  = new CSite();

   $brokerID = 36; // Think Insurance

   // getting the list of Coversure insurers
   $coversureArray = $objSite->GetBrokerSites($brokerID, $quoteTypeID);

  //rebuild request_sites from database with the company the user had quote before (in case if he had one)
   if( $userDetails = $objQuoteUser->GetUserByFNameSNameDateOfBirth($_SESSION['_YourDetails_']['first_name'], $_SESSION['_YourDetails_']['surname'], $_SESSION['_YourDetails_']['date_of_birth']))
   {
      $quoteUserID = $userDetails['id'];
      $lastCompaniesSent = $objQuoteStatus->GetLastQuoteStatusByQuoteUserID($quoteUserID, $brokerID);
   }

   if (is_array($lastCompaniesSent) AND !empty($lastCompaniesSent))
   {
      foreach ($lastCompaniesSent as $companySiteId)
      {
        // check if company belong to the searched group
        if(array_key_exists($companySiteId, $coversureArray))
        {
           if(array_key_exists($companySiteId,$_SESSION['REQUEST_SITES']) && $getQuote[$companySiteId])
           {
              foreach($coversureArray as $id => $name)
              {
                 if($id != $companySiteId)
                    unset($_SESSION['REQUEST_SITES'][$id]);
              }

              return;
           }
        }
      }
   }

    // removing the coversure insurers that are not included
   foreach($coversureArray as $id => $name)
   {
      if(! $_SESSION['REQUEST_SITES'][$id])
         unset($coversureArray[$id]);
   }

   // getting the last entry from quote_status table for each coversure insurer; the biggest id means the last insurer with success
   // init the priority list
   $lastSuccessSiteID = 0;
   $lastQuoteStatusID = 0;
   $coversurePriorityList = array();
   foreach($coversureArray as $id => $name)
   {
      array_push($coversurePriorityList, $id);

      // removing all coversure insurers from the request sites list
      unset($_SESSION['REQUEST_SITES'][$id]);

      if(! $qsID = $objQuoteStatus->GetLastSiteSuccess($id))
         continue;

      if($qsID > $lastQuoteStatusID)
      {
         $lastQuoteStatusID = $qsID;
         $lastSuccessSiteID = $id;
      }
   }

   // ordering the priority list
   foreach($coversureArray as $id => $name)
   {
      array_shift($coversurePriorityList);
      array_push($coversurePriorityList, $id);

      if($id == $lastSuccessSiteID)
         break;
   }

   foreach($coversurePriorityList as $index => $siteID)
   {
      $_SESSION['REQUEST_SITES'][$siteID] = 1;

      if($getQuote[$siteID])
         break;
   }

}



function DNARotation()
{
   global $quoteTypeID;
   global $SYSTEM_LOCKED;
   global $objQuoteStatus;
   global $getQuote;
   global $objQuoteUser;

   //rebiuld request_sites from database for sid only
   if ($SYSTEM_LOCKED)
   {
      $activeSites = $objQuoteStatus->GetAllQuoteStatusByLogID($_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id']);
      unset($_SESSION['REQUEST_SITES']);
      foreach($activeSites as $site)
      {
         if ($site['status'] == 'SKIPPED')
            continue;

         if ($site['status'] == 'WAITING')
            continue;

         $_SESSION['REQUEST_SITES'][$site['site_id']] = 1;
         $getQuote[$site['site_id']] = 1;
      }

      return;
   }

   $objSite  = new CSite();

   $brokerID = 35; // DNA

   // getting the list of Coversure insurers
   $DNAArray = $objSite->GetBrokerSites($brokerID, $quoteTypeID);

  //rebuild request_sites from database with the company the user had quote before (in case if he had one)
   if( $userDetails = $objQuoteUser->GetUserByFNameSNameDateOfBirth($_SESSION['_YourDetails_']['first_name'], $_SESSION['_YourDetails_']['surname'], $_SESSION['_YourDetails_']['date_of_birth']))
   {
      $quoteUserID = $userDetails['id'];
      $lastCompaniesSent = $objQuoteStatus->GetLastQuoteStatusByQuoteUserID($quoteUserID, $brokerID);
   }

   if (is_array($lastCompaniesSent) AND !empty($lastCompaniesSent))
   {
      foreach ($lastCompaniesSent as $companySiteId)
      {
        // check if company belong to the searched group
        if(array_key_exists($companySiteId, $DNAArray))
        {
           if(array_key_exists($companySiteId,$_SESSION['REQUEST_SITES']) && $getQuote[$companySiteId])
           {
              foreach($DNAArray as $id => $name)
              {
                 if($id != $companySiteId)
                    unset($_SESSION['REQUEST_SITES'][$id]);
              }

              return;
           }
        }
      }
   }

    // removing the DNA insurers that are not included
   foreach($DNAArray as $id => $name)
   {
      if(! $_SESSION['REQUEST_SITES'][$id])
         unset($DNAArray[$id]);
   }

   // getting the last entry from quote_status table for each DNA insurer; the biggest id means the last insurer with success
   // init the priority list
   $lastSuccessSiteID = 0;
   $lastQuoteStatusID = 0;
   $DNAPriorityList = array();
   foreach($DNAArray as $id => $name)
   {
      array_push($DNAPriorityList, $id);

      // removing all DNA insurers from the request sites list
      unset($_SESSION['REQUEST_SITES'][$id]);

      if(! $qsID = $objQuoteStatus->GetLastSiteSuccess($id))
         continue;

      if($qsID > $lastQuoteStatusID)
      {
         $lastQuoteStatusID = $qsID;
         $lastSuccessSiteID = $id;
      }
   }

   // ordering the priority list
   foreach($DNAArray as $id => $name)
   {
      array_shift($DNAPriorityList);
      array_push($DNAPriorityList, $id);

      if($id == $lastSuccessSiteID)
         break;
   }

   foreach($DNAPriorityList as $index => $siteID)
   {
      $_SESSION['REQUEST_SITES'][$siteID] = 1;

      if($getQuote[$siteID])
         break;
   }

}


function BarryGraingerRotation()
{
   global $quoteTypeID;
   global $SYSTEM_LOCKED;
   global $objQuoteStatus;
   global $getQuote;
   global $objQuoteUser;

   //rebiuld request_sites from database for sid only
   if ($SYSTEM_LOCKED)
   {
      $activeSites = $objQuoteStatus->GetAllQuoteStatusByLogID($_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id']);
      unset($_SESSION['REQUEST_SITES']);
      foreach($activeSites as $site)
      {
         if ($site['status'] == 'SKIPPED')
            continue;

         if ($site['status'] == 'WAITING')
            continue;

         $_SESSION['REQUEST_SITES'][$site['site_id']] = 1;
         $getQuote[$site['site_id']] = 1;
      }

      return;
   }

   $objSite  = new CSite();

   $brokerID = 37; // Barry Grainger

   // getting the list of Coversure insurers
   $coversureArray = $objSite->GetBrokerSites($brokerID, $quoteTypeID);

  //rebuild request_sites from database with the company the user had quote before (in case if he had one)
   if( $userDetails = $objQuoteUser->GetUserByFNameSNameDateOfBirth($_SESSION['_YourDetails_']['first_name'], $_SESSION['_YourDetails_']['surname'], $_SESSION['_YourDetails_']['date_of_birth']))
   {
      $quoteUserID = $userDetails['id'];
      $lastCompaniesSent = $objQuoteStatus->GetLastQuoteStatusByQuoteUserID($quoteUserID, $brokerID);
   }

   if (is_array($lastCompaniesSent) AND !empty($lastCompaniesSent))
   {
      foreach ($lastCompaniesSent as $companySiteId)
      {
        // check if company belong to the searched group
        if(array_key_exists($companySiteId, $coversureArray))
        {
           if(array_key_exists($companySiteId,$_SESSION['REQUEST_SITES']) && $getQuote[$companySiteId])
           {
              foreach($coversureArray as $id => $name)
              {
                 if($id != $companySiteId)
                    unset($_SESSION['REQUEST_SITES'][$id]);
              }

              return;
           }
        }
      }
   }

    // removing the coversure insurers that are not included
   foreach($coversureArray as $id => $name)
   {
      if(! $_SESSION['REQUEST_SITES'][$id])
         unset($coversureArray[$id]);
   }

   // getting the last entry from quote_status table for each coversure insurer; the biggest id means the last insurer with success
   // init the priority list
   $lastSuccessSiteID = 0;
   $lastQuoteStatusID = 0;
   $coversurePriorityList = array();
   foreach($coversureArray as $id => $name)
   {
      array_push($coversurePriorityList, $id);

      // removing all coversure insurers from the request sites list
      unset($_SESSION['REQUEST_SITES'][$id]);

      if(! $qsID = $objQuoteStatus->GetLastSiteSuccess($id))
         continue;

      if($qsID > $lastQuoteStatusID)
      {
         $lastQuoteStatusID = $qsID;
         $lastSuccessSiteID = $id;
      }
   }

   // ordering the priority list
   foreach($coversureArray as $id => $name)
   {
      array_shift($coversurePriorityList);
      array_push($coversurePriorityList, $id);

      if($id == $lastSuccessSiteID)
         break;
   }

   foreach($coversurePriorityList as $index => $siteID)
   {
      $_SESSION['REQUEST_SITES'][$siteID] = 1;

      if($getQuote[$siteID])
         break;
   }

}


function BroadsureDirect()
{
   global $quoteTypeID;
   global $SYSTEM_LOCKED;
   global $objQuoteStatus;
   global $getQuote;
   global $objQuoteUser;

   //rebiuld request_sites from database for sid only
   if ($SYSTEM_LOCKED)
   {
      $activeSites = $objQuoteStatus->GetAllQuoteStatusByLogID($_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id']);
      unset($_SESSION['REQUEST_SITES']);
      foreach($activeSites as $site)
      {
         if ($site['status'] == 'SKIPPED')
            continue;

         if ($site['status'] == 'WAITING')
            continue;

         $_SESSION['REQUEST_SITES'][$site['site_id']] = 1;
         $getQuote[$site['site_id']] = 1;
      }

      return;
   }

   $objSite  = new CSite();

   $brokerID = 40; // Coversure

   // getting the list of Coversure insurers
   $coversureArray = $objSite->GetBrokerSites($brokerID, $quoteTypeID);

  //rebuild request_sites from database with the company the user had quote before (in case if he had one)
   if( $userDetails = $objQuoteUser->GetUserByFNameSNameDateOfBirth($_SESSION['_YourDetails_']['first_name'], $_SESSION['_YourDetails_']['surname'], $_SESSION['_YourDetails_']['date_of_birth']))
   {
      $quoteUserID = $userDetails['id'];
      $lastCompaniesSent = $objQuoteStatus->GetLastQuoteStatusByQuoteUserID($quoteUserID, $brokerID);
   }

   if (is_array($lastCompaniesSent) AND !empty($lastCompaniesSent))
   {
      foreach ($lastCompaniesSent as $companySiteId)
      {
        // check if company belong to the searched group
        if(array_key_exists($companySiteId, $coversureArray))
        {
           if(array_key_exists($companySiteId,$_SESSION['REQUEST_SITES']) && $getQuote[$companySiteId])
           {
              foreach($coversureArray as $id => $name)
              {
                 if($id != $companySiteId)
                    unset($_SESSION['REQUEST_SITES'][$id]);
              }

              return;
           }
        }
      }
   }

    // removing the coversure insurers that are not included
   foreach($coversureArray as $id => $name)
   {
      if(! $_SESSION['REQUEST_SITES'][$id])
         unset($coversureArray[$id]);
   }

   // getting the last entry from quote_status table for each coversure insurer; the biggest id means the last insurer with success
   // init the priority list
   $lastSuccessSiteID = 0;
   $lastQuoteStatusID = 0;
   $coversurePriorityList = array();
   foreach($coversureArray as $id => $name)
   {
      array_push($coversurePriorityList, $id);

      // removing all coversure insurers from the request sites list
      unset($_SESSION['REQUEST_SITES'][$id]);

      if(! $qsID = $objQuoteStatus->GetLastSiteSuccess($id))
         continue;

      if($qsID > $lastQuoteStatusID)
      {
         $lastQuoteStatusID = $qsID;
         $lastSuccessSiteID = $id;
      }
   }

   // ordering the priority list
   foreach($coversureArray as $id => $name)
   {
      array_shift($coversurePriorityList);
      array_push($coversurePriorityList, $id);

      if($id == $lastSuccessSiteID)
         break;
   }

   foreach($coversurePriorityList as $index => $siteID)
   {
      $_SESSION['REQUEST_SITES'][$siteID] = 1;

      if($getQuote[$siteID])
         break;
   }

}

function PatonsInsuranceRotation()
{
   global $quoteTypeID;
   global $SYSTEM_LOCKED;
   global $objQuoteStatus;
   global $getQuote;
   global $objQuoteUser;

   //rebiuld request_sites from database for sid only
   if ($SYSTEM_LOCKED)
   {
      $activeSites = $objQuoteStatus->GetAllQuoteStatusByLogID($_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id']);
      unset($_SESSION['REQUEST_SITES']);
      foreach($activeSites as $site)
      {
         if ($site['status'] == 'SKIPPED')
            continue;

         if ($site['status'] == 'WAITING')
            continue;

         $_SESSION['REQUEST_SITES'][$site['site_id']] = 1;
         $getQuote[$site['site_id']] = 1;
      }

      return;
   }

   $objSite  = new CSite();

   $brokerID = 45; // Patons Insurance

   // getting the list of Coversure insurers
   $coversureArray = $objSite->GetBrokerSites($brokerID, $quoteTypeID);

  //rebuild request_sites from database with the company the user had quote before (in case if he had one)
   if( $userDetails = $objQuoteUser->GetUserByFNameSNameDateOfBirth($_SESSION['_YourDetails_']['first_name'], $_SESSION['_YourDetails_']['surname'], $_SESSION['_YourDetails_']['date_of_birth']))
   {
      $quoteUserID = $userDetails['id'];
      $lastCompaniesSent = $objQuoteStatus->GetLastQuoteStatusByQuoteUserID($quoteUserID, $brokerID);
   }

   if (is_array($lastCompaniesSent) AND !empty($lastCompaniesSent))
   {
      foreach ($lastCompaniesSent as $companySiteId)
      {
        // check if company belong to the searched group
        if(array_key_exists($companySiteId, $coversureArray))
        {
           if(array_key_exists($companySiteId,$_SESSION['REQUEST_SITES']) && $getQuote[$companySiteId])
           {
              foreach($coversureArray as $id => $name)
              {
                 if($id != $companySiteId)
                    unset($_SESSION['REQUEST_SITES'][$id]);
              }

              return;
           }
        }
      }
   }

    // removing the coversure insurers that are not included
   foreach($coversureArray as $id => $name)
   {
      if(! $_SESSION['REQUEST_SITES'][$id])
         unset($coversureArray[$id]);
   }

   // getting the last entry from quote_status table for each coversure insurer; the biggest id means the last insurer with success
   // init the priority list
   $lastSuccessSiteID = 0;
   $lastQuoteStatusID = 0;
   $coversurePriorityList = array();
   foreach($coversureArray as $id => $name)
   {
      array_push($coversurePriorityList, $id);

      // removing all coversure insurers from the request sites list
      unset($_SESSION['REQUEST_SITES'][$id]);

      if(! $qsID = $objQuoteStatus->GetLastSiteSuccess($id))
         continue;

      if($qsID > $lastQuoteStatusID)
      {
         $lastQuoteStatusID = $qsID;
         $lastSuccessSiteID = $id;
      }
   }

   // ordering the priority list
   foreach($coversureArray as $id => $name)
   {
      array_shift($coversurePriorityList);
      array_push($coversurePriorityList, $id);

      if($id == $lastSuccessSiteID)
         break;
   }

   foreach($coversurePriorityList as $index => $siteID)
   {
      $_SESSION['REQUEST_SITES'][$siteID] = 1;

      if($getQuote[$siteID])
         break;
   }

}

function OneAnswerInsuranceRotation()
{
   global $quoteTypeID;
   global $SYSTEM_LOCKED;
   global $objQuoteStatus;
   global $getQuote;
   global $objQuoteUser;

   //rebiuld request_sites from database for sid only
   if ($SYSTEM_LOCKED)
   {
      $activeSites = $objQuoteStatus->GetAllQuoteStatusByLogID($_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id']);
      unset($_SESSION['REQUEST_SITES']);
      foreach($activeSites as $site)
      {
         if ($site['status'] == 'SKIPPED')
            continue;

         if ($site['status'] == 'WAITING')
            continue;

         $_SESSION['REQUEST_SITES'][$site['site_id']] = 1;
         $getQuote[$site['site_id']] = 1;
      }

      return;
   }

   $objSite  = new CSite();

   $brokerID = 33; // One Answer

   // getting the list of Coversure insurers
   $coversureArray = $objSite->GetBrokerSites($brokerID, $quoteTypeID);

  //rebuild request_sites from database with the company the user had quote before (in case if he had one)
   if( $userDetails = $objQuoteUser->GetUserByFNameSNameDateOfBirth($_SESSION['_YourDetails_']['first_name'], $_SESSION['_YourDetails_']['surname'], $_SESSION['_YourDetails_']['date_of_birth']))
   {
      $quoteUserID = $userDetails['id'];
      $lastCompaniesSent = $objQuoteStatus->GetLastQuoteStatusByQuoteUserID($quoteUserID, $brokerID);
   }

   if (is_array($lastCompaniesSent) AND !empty($lastCompaniesSent))
   {
      foreach ($lastCompaniesSent as $companySiteId)
      {
        // check if company belong to the searched group
        if(array_key_exists($companySiteId, $coversureArray))
        {
           if(array_key_exists($companySiteId,$_SESSION['REQUEST_SITES']) && $getQuote[$companySiteId])
           {
              foreach($coversureArray as $id => $name)
              {
                 if($id != $companySiteId)
                    unset($_SESSION['REQUEST_SITES'][$id]);
              }

              return;
           }
        }
      }
   }

    // removing the coversure insurers that are not included
   foreach($coversureArray as $id => $name)
   {
      if(! $_SESSION['REQUEST_SITES'][$id])
         unset($coversureArray[$id]);
   }

   // getting the last entry from quote_status table for each coversure insurer; the biggest id means the last insurer with success
   // init the priority list
   $lastSuccessSiteID = 0;
   $lastQuoteStatusID = 0;
   $coversurePriorityList = array();
   foreach($coversureArray as $id => $name)
   {
      array_push($coversurePriorityList, $id);

      // removing all coversure insurers from the request sites list
      unset($_SESSION['REQUEST_SITES'][$id]);

      if(! $qsID = $objQuoteStatus->GetLastSiteSuccess($id))
         continue;

      if($qsID > $lastQuoteStatusID)
      {
         $lastQuoteStatusID = $qsID;
         $lastSuccessSiteID = $id;
      }
   }

   // ordering the priority list
   foreach($coversureArray as $id => $name)
   {
      array_shift($coversurePriorityList);
      array_push($coversurePriorityList, $id);

      if($id == $lastSuccessSiteID)
         break;
   }

   foreach($coversurePriorityList as $index => $siteID)
   {
      $_SESSION['REQUEST_SITES'][$siteID] = 1;

      if($getQuote[$siteID])
         break;
   }

}



function setLookSmartTracker()
{
   global $objWs;
   $objWs->SetLookSmartTracker();

}


function setSearchVisionAdds()
{
   // DEBUG MODE
   global $objWs;

   $quoteUserID = $_SESSION['_QZ_QUOTE_DETAILS_']['quote_user_id'];
   $esvAmount   = $_SESSION['_QZ_QUOTE_DETAILS_']['esv_amount'];

   $objWs->SetSearchVisionAds($quoteUserID,$esvAmount);
}


function setGoogleAdds()
{
  global $objWs;

  $quoteUserID = $_SESSION['_QZ_QUOTE_DETAILS_']['quote_user_id'];
  $esvAmount   = $_SESSION['_QZ_QUOTE_DETAILS_']['esv_amount'];

  $objWs->SetGoogleAds($quoteUserID,$esvAmount);
}


function sendEmailOutboundsEmailVision()
{
   global $SYSTEM_LOCKED;

   if($SYSTEM_LOCKED)
      return true;

   exec('/usr/bin/php -q steps/OutboundsEmailVision.php '.$_SESSION['TEMPORARY FILE NAME'].' '.$_SESSION["_QZ_QUOTE_DETAILS_"]["qz_log_id"].' > /dev/null 2>&1 &');

   return true;
}

function sendEmailOutboundsEmailVisionOld()
{

   global $SYSTEM_LOCKED;
   global $mailObj;
   global $_YourDetails;


   $fileNameOutbounds = $_SESSION['TEMPORARY FILE NAME'];

   $objEmailOutbounds           = new CEmailOutbounds($dbHandle);
   $objEmailOutboundsEngineTaxi = new CEmailOutboundsEngineTaxi($fileNameOutbounds, $dbHandle);

   if($SYSTEM_LOCKED)
      return true;

   //email vision send data
   $fh = fopen("/home/www/quotezone.co.uk/taxi-insurance/taxi/TaxiEmailOutbounds.log","a+");

   $logIDOutbounds = $_SESSION["_QZ_QUOTE_DETAILS_"]["qz_log_id"];

//   if(! $objEmailOutbounds->AddEmailOutbounds($logIDOutbounds))
//   {
//      $fh = fopen("/home/www/quotezone.co.uk/taxi-insurance/taxi/TaxiEmailOutbounds.log","a+");
//      fwrite($fh, "FAILURE add emaioutbound details to table aa: $fileNameOutbounds / $logIDOutbounds ...\n");
//      fwrite($fh, "Failure \n");
//      print $objEmailOutbounds->GetError();
//   }

   if($objEmailOutboundsEngineTaxi->SendUserDetails())
   {
      if(!$objEmailOutbounds->SetEmailOutboundsResponse($logIDOutbounds,1))
      {
         $fh = fopen("/home/www/quotezone.co.uk/taxi-insurance/taxi/TaxiEmailOutbounds.log","a+");
         fwrite($fh, "\n\n\n Response was failure for aaa: $fileNameOutbounds / $logIDOutbounds \n\n\n");
         fclose($fh);
      }
      else
      {
         $fh = fopen("/home/www/quotezone.co.uk/taxi-insurance/taxi/TaxiEmailOutbounds.log","a+");
         fwrite($fh, "\n\n\n Response was SUCCESS for : $fileNameOutbounds / $logIDOutbounds \n STOP \n\n");
         fclose($fh);
      }
   }

   return true;
}

function skipSite($siteID, $personalizedError)
{
   global $objQuoteStatus;
   global $objLeadQuoteFilters;
   global $SYSTEM_LOCKED;

   $quoteType    = 'NORMAL';

   if(! empty($_SESSION['DEBUG_SITES']))
      $quoteType    = 'DEBUG';

   if(! $SYSTEM_LOCKED)
   {
      if(! $quoteStatusID = $objQuoteStatus->AddQuoteStatus($_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id'], $siteID,'SKIPPED',$quoteType))
         $objQuoteStatus->ShowError();

      foreach($personalizedError as $index => $filter)
      {
         list($skipRule, $details) = explode("--",$filter);

         if(! $leadQuoteFilterID = $objLeadQuoteFilters->AddLeadQuoteFilters($siteID, $_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id'], $skipRule, $details))
            print $objLeadQuoteFilters->GetError();
      }
   }

}


function showQuoteReference(&$resSite)
{
   $resSite['QZ_QUOTE_REFERENCE'] = $_SESSION['_QZ_QUOTE_DETAILS_']['quote_reference'];
}


function processQuoteDetails()
{
   global $objWs;
   global $objFile;
   global $objQzCookieAffiliates;
   global $objAffiliate;
   global $objCookie;
   global $quoteTypeID;
   global $SYSTEM_LOCKED;
   global $objFSClient;

   //echo "<script language=javascript>alert('$SYSTEM_LOCKED');</script>";

  if(! $SYSTEM_LOCKED)
  {

         // add quote user and user details into db and ses ids to session
         addQuoteUserDetailsToDb(); // DONE

         // add email store
         addEmailStore();

         // add email store code
         addEmailStoreCodes();

         // generate unique file name and save into session
         generateUniqueFileName(); // done

         addQuoteLogToDB(); // done

         // generate quoteref  and save to the session
         generateQuoteReference();// done

         // add qzquote to db
         addQzQuoteToDB(); // done

         // update status if user has js disabled and the enabled
         noJSConversion();

         // update status if user has cookies disabled
         noCookiesConversion();

         // check affiliate cookie and add to db
         setAffiliateCookie(); // check !!!!!

         // save session content into file already generated
         saveSessionToFile(); // done

         // insert quotes into email_tracking_quotes
         if($_COOKIE['EmailTracking'])
            insertEmailTrackerQuotes();

         // send file to be stored
         $objFSClient->PutFile($_SESSION['TEMPORARY FILE NAME']);

   }

   setTrackingCookie();

}

function addQuoteUserDetailsToDb()
{
   global $objQzQuote;
   global $objQuoteUser;
   global $objQuoteUserDetails;
   global $quoteTypeID;
   global $_SESSION;

   $_SESSION['_YourDetails_']["start_date_dd"]   = date("d");
   $_SESSION['_YourDetails_']["start_date_mm"]   = date("m");
   $_SESSION['_YourDetails_']["start_date_yyyy"] = date("Y");
   $_SESSION['_YourDetails_']["start_date"]      = date("d")."/".date("m")."/".date("Y");

   $firstName   = $_SESSION['_YourDetails_']['first_name'];
   $lastName    = $_SESSION['_YourDetails_']['surname'];
   $dateOfBirth = $_SESSION['_YourDetails_']['date_of_birth'];
//    $password    = $_SESSION['_QZ_QUOTE_DETAILS_']['quote_password'];
   $password    = 'quotezone';

   // have a new user, else an old one :) about 93 years
   //if(! $quoteUserID = $_SESSION['_QZ_QUOTE_DETAILS_']['quote_user_id'])
      if(! $quoteUserID = $objQuoteUser->AddQuoteUser($firstName,$lastName,$dateOfBirth, $password))
         $objQuoteUser->ShowError();

   if(! $quoteUserDetailsID = $objQuoteUserDetails->AddQuoteUserDetails($quoteUserID,$quoteTypeID,$_SESSION))
   {
      $objQuoteUserDetails->ShowError();
      die("Cannot add quote user details");
   }

   $_SESSION['_QZ_QUOTE_DETAILS_']['quote_user_id']         = $quoteUserID;
   $_SESSION['_QZ_QUOTE_DETAILS_']['quote_user_details_id'] = $quoteUserDetailsID;
}

function addEmailStore()
{
   global $objEmailStore;
   global $emailStoreId;

   $realName  = $_SESSION["_YourDetails_"]["first_name"]." ".$_SESSION["_YourDetails_"]["surname"];
   $homeOwner = "0";

   $userEmailAddress = $_SESSION["_YourDetails_"]["email_address"];

   $emailStoreId = $objEmailStore->AddEmailStore($homeOwner,$realName,$userEmailAddress,$_SESSION['_QZ_QUOTE_DETAILS_']['quote_user_id']);

}

function addEmailStoreCodes()
{
   global $objEmailStoreCodes;
   global $emailStoreId;

   $userEmailAddress = $_SESSION["_YourDetails_"]["email_address"];

   while (1==1)
   {
            $randomCode = $objEmailStoreCodes->GenerateRandomCode();

            if ($objEmailStoreCodes->CheckIfRandomCodeExists($randomCode))
                  break;
   }

   $verificationCode = $objEmailStoreCodes->GenerateSecurityCode($userEmailAddress);

   $objEmailStoreCodes->AddEmailStoreCodes($randomCode,$verificationCode,$emailStoreId);
}


function generateUniqueFileName()
{
   global $objFile;

   $dirName = ROOT_PATH."in";

   // generate unique file name in out directory
   if(! $fileName = $objFile->GenerateFileName($dirName, true))
      print $objFile->GetError();

   //print($fileName);//die;

   //echo "<script language=javascript>alert('$fileName');</script>";

   $_SESSION['TEMPORARY FILE NAME'] = $fileName;
}

function addQuoteLogToDB()
{
   global $objLog;

   if(! $logID = $objLog->AddLog($_SESSION['_QZ_QUOTE_DETAILS_']['quote_user_id'], $_SERVER['REMOTE_ADDR'],$_SESSION['TEMPORARY FILE NAME']))
      $objLog->ShowError();

   $_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id'] = $logID;
}

function generateQuoteReference()
{
   global $objQzQuote;
   global $quoteTypeID;

   while(true)
   {
      $quoteReference = $objQzQuote->GenerateQuoteRef($quoteTypeID);

      if(! $objQzQuote->GetQzQuoteByQRef($quoteReference))
         break;
   }

   $_SESSION['_QZ_QUOTE_DETAILS_']['quote_reference'] = $quoteReference;
}

function addQzQuoteToDB()
{
   global $objQzQuote;
   global $quoteTypeID;

   if(! $qzQuoteID = $objQzQuote->AddQzQuote($_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id'],$_SESSION['_QZ_QUOTE_DETAILS_']['quote_user_details_id'],$quoteTypeID, $_SESSION['_QZ_QUOTE_DETAILS_']['quote_reference']))
      $objQzQuote->ShowError();

   $_SESSION['_QZ_QUOTE_DETAILS_']['qz_quote_id']     = $qzQuoteID;
	// if we have a mobile number, add user to SMS status table
    if (!empty($_SESSION['_YourDetails_']['mobile_telephone'])) {
        if (!$smsID = $objQzQuote->AddSmsStatus($_SESSION['_QZ_QUOTE_DETAILS_']['quote_user_id'], $_SESSION['_YourDetails_']['mobile_telephone']))
            $objQzQuote->ShowError();
    }
}

function setAffiliateCookie()
{
   global $objAffiliate;
   global $objCookie;
   global $objQzCookieAffiliates;
   global $objAffiliateKeywords;

   if(! ($_COOKIE["AFFID"] && $_COOKIE["CREF"]))
      return;

   $_SESSION['_AFFILIATES_']['id']   = $_COOKIE["AFFID"];
   $_SESSION['_AFFILIATES_']['cRef'] = $_COOKIE["CREF"];

   list($affiliatesPrefix, $affiliatesSufix) = split("-",$_COOKIE["AFFID"]);

   $affiliateID = $objAffiliate->GetAffiliatesIDBySufixAndPrefix($affiliatesPrefix, $affiliatesSufix);

   $cookieID    = $objCookie->GetCookieByName($_COOKIE["CREF"]);

   $qzQuoteID   = $_SESSION['_QZ_QUOTE_DETAILS_']['qz_quote_id'];

   if(preg_match('/^\d+$/',$affiliateID) && preg_match('/^\d+$/',$cookieID["id"]))
      if(! $qzCookieAffiliateID = $objQzCookieAffiliates->AddQzCookieAffiliate($qzQuoteID, $affiliateID, $cookieID["id"]))
         print $objQzCookieAffiliates->GetError();

   $_SESSION['_AFFILIATES_']['qzquote_affiliate_id'] = $qzCookieAffiliateID;

	 if ($_COOKIE['AFFKWID'])
	 {
		if (!$objAffiliateKeywords->AddQzQuoteAffiliateKeywords($_COOKIE['AFFKWID'],$qzCookieAffiliateID))
			print $objAffiliateKeywords->GetError();
	 }
}

function saveSessionToFile()
{
   global $objFile;

   $dirName = ROOT_PATH."in/";

   $session = var_export($_SESSION, true);

   if(! $objFile->Open($dirName.$_SESSION['TEMPORARY FILE NAME'], "w+"))
      print $objFile->GetError();

   if(! $objFile->Write($session))
      print $objFile->GetError();

   if(! $objFile->Close())
      print $objFile->GetError();
}

function setTrackingCookie()
{
   $objTracking = new CTracking("", "", base64_decode($_COOKIE['curef']), base64_decode($_COOKIE['cqref']), $_SERVER['REMOTE_ADDR'], $_SESSION['_QZ_QUOTE_DETAILS_']['qz_quote_id']);

   $objTracking->_updateTracking();

   setcookie("cqref","",time(),"",".quotezone.co.uk");
}

function setQzProtection()
{
   return ;
   global $quoteTypeID;
   global $SYSTEM_LOCKED;

   // set qz protection details
   if(! $SYSTEM_LOCKED)
   {
      // we have to set the cookies user quote number
      $objQzProtection = new CQzProtection($quoteTypeID,$_COOKIE,$_SESSION);

      $_SESSION = $objQzProtection->SetRequest();
   }
}

function showAdvertise(&$resSite)
{

   $text = getOvertureAds($overtureHtml);
   $text = preg_replace('/£/', '&pound;', $text);

   $resSite['OVERTURE_CONTENT'] = $text;

}

function showSystemsAdvertise(&$resSite)
{
   global $objTmpl;
 
   if(! $objTmpl->Load("OvertureSystems.tmpl"))
      print $objTmpl->GetError()."\n";

   $resSite['OVERTURE_SYSTEMS_CONTENT'] = $objTmpl->GetContent();
}



function getError($personalizedError)
{
   $counter = 0;
   $errors  = explode("<br>", $personalizedError);

   $personalizedError = "";

   foreach($errors as $error)
   {
      if(empty($error))
         continue;

      $counter++;
      $personalizedError .= "$counter. $error<br>";
   }

   return $personalizedError;
}


function getOvertureAds()
{
   global $objTmpl;
   global $objOvertureTracker;
   global $quoteTypeID;

   if(TESTING_MODE)
     return;

   $USER_AGENT     = $_SERVER['HTTP_USER_AGENT'];
   $USER_IP_ADDR   = $_SERVER['REMOTE_ADDR'];
   $X_FORWARD_IP   = $_SERVER['HTTP_X_FORWARDED_FOR'];

   $affilDataValue = "ip=$USER_IP_ADDR";

   if($X_FORWARD_IP)
      $affilDataValue .= "&xfip=$X_FORWARD_IP";

   $affilDataValue .= "&ua=".rawurlencode($USER_AGENT);


   // Specify URL to XML and XSL documents
   $xmlDoc = 'http://xml.uk.overture.com/d/search/p/standard/eu/xml/rlb/?';

   $data = rawurlencode("taxi insurance");

   $xmlDoc .= "catMaps=seopa_uk_multi&mkt=uk&maxCount=20&Partner=seopa_xml_uk_searchbox_quotezone&adultFilter=clean&Keywords=$data&keywordCharEnc=utf-8&outputCharEnc=utf-8&type=c";
   $xmlDoc .= "&affilData=".rawurlencode($affilDataValue);
   $xmlDoc .= "&serveUrl=" . rawurlencode(($_SERVER['HTTPS']== 'on'?'https://':'http://').$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']);

   $xmlConn = fopen ($xmlDoc, "r");

   if(!$xmlConn)
      return "";

   do
   {
       $data = fread($xmlConn,2048);

      if(strlen($data) == 0)
         break;

      $xmlFeed .= $data;

   } while(true);

   fclose($xmlConn);

   //print $xmlFeed;
   // Parse XML results into two arrays
   $parser = xml_parser_create();
   xml_parse_into_struct($parser,$xmlFeed,$values,$tags);
   xml_parser_free($parser);

   //If the types are open they are put into array $listings
   $listings = array();
   $listingIndex = 0;

   if( $tags['LISTING'] )
   {
      foreach ($tags['LISTING'] as $key=>$val)
      {
         if( 'open' == $values[$val]['type'] )
         {
            $listings[$listingIndex] = $values[$val]['attributes'];
            $listingIndex++;
         }
      }
   }


   // If no listings, then we are done
   $totalListings = sizeof( $listings );

   if( 0 == $totalListings )
   {
       return false;
   }

   // Get one clickUrl per listing
   $listingIndex   = 0;
   $totalUrls      = sizeof( $tags['CLICKURL'] );
   $urlsPerListing = $totalUrls / $totalListings;

   for($urlIndex = 0; $urlIndex < $totalUrls; $urlIndex += $urlsPerListing )
   {
      $url = $values[$tags['CLICKURL'][$urlIndex]]['value'];
      $listings[$listingIndex]['CLICKURL'] = $url;
      $listingIndex++;
   }
   // Read NextArgs/PrevArgs for navigation
   $nextargs = $values[$tags['NEXTARGS'][0]]['value'];
   $prevargs = $values[$tags['PREVARGS'][0]]['value'];

   $counter = 0;
   $index = 0;
   foreach ($listings as $key=>$val)
   {

      $index++;
      $resOvertureLinks = array();

     if(preg_match("/(comparethemarket)|(gocompare)|(confused)|(tescocompare)|(moneysupermarket)|(beatthatquote)|(quotezone)/i",$val['SITEHOST']))
         continue;

     $counter++;

     $siteHostLen = strlen($val['SITEHOST']);

      // show horizontal line
      if($counter != 5)
      {
         if(! $objTmpl->Load("OvertureLinksTaxi.tmpl"))
            print $objTmpl->GetError()."\n";

         $resOvertureLinks['overture_links_taxi.value'] = $objTmpl->GetContent();
      }

      if(! $objTmpl->Load("OvertureLinks.tmpl"))
         print $objTmpl->GetError()."\n";

      $resOvertureLinks['overture_count_image.value'] =  "<a href=\"https://taxi-insurance.quotezone.co.uk/taxi/steps/OvertureClickIn.php?urlName=".$val['SITEHOST']."&top=".$val['RANK']."&qzTop=".$counter."&logId=".$_SESSION["_QZ_QUOTE_DETAILS_"]["qz_log_id"]."&quoteRef=".$_SESSION["_QZ_QUOTE_DETAILS_"]["quote_reference"]."&urlAddr=".urlencode($val["CLICKURL"])."\" target=\"_blank\"><img src=\"images/dot_nr".$counter.".gif\" border=\"0\" width=\"32\" height=\"32\"></a>";
      $resOvertureLinks['overture_title.value']        =  $val['TITLE'];
      $resOvertureLinks['overture_descripion.value']   =  $val['DESCRIPTION'];
      $resOvertureLinks['overture_link.value']         =  'https://taxi-insurance.quotezone.co.uk/taxi/steps/OvertureClickIn.php?urlName='.$val['SITEHOST'].'&top='.$val['RANK'].'&qzTop='.$counter.'&logId='.$_SESSION["_QZ_QUOTE_DETAILS_"]["qz_log_id"].'&quoteRef='.$_SESSION['_QZ_QUOTE_DETAILS_']['quote_reference'].'&urlAddr='.urlencode($val['CLICKURL']);
      $resOvertureLinks['overture_host.value']         = $val["SITEHOST"];

      //$resOvertureLinks['overture_title.value']      =  $val['TITLE'];
      //$resOvertureLinks['overture_descripion.value'] =  $val['DESCRIPTION'];
      //$resOvertureLinks['overture_link.value']       =  'https://taxi-insurance.quotezone.co.uk/taxi/steps/OvertureClickIn.php?urlName='.$val['SITEHOST'].'&top='.$val['RANK'].'&qzTop='.$counter.'&logId='.$_SESSION["_QZ_QUOTE_DETAILS_"]["qz_log_id"].'&quoteRef='.$_SESSION['_QZ_QUOTE_DETAILS_']['quote_reference'].'&urlAddr='.urlencode($val['CLICKURL']);
      //$resOvertureLinks['overture_host.value']       = $val["SITEHOST"];

      // track impressions
      $urlName = $val['SITEHOST'];
      $urlName    = preg_replace("/\<br\>/","", $urlName);
      $urlName    = preg_replace("/\s+/","", $urlName);

      if(! preg_match("/^http/i", $urlName))
        $urlName = "http://".$urlName;

      $urlName    = preg_replace("/www\./","", $urlName);
      $urlName    = strtolower($urlName);

      $urlDetails = parse_url($urlName);
      $advertiser = $urlDetails['host'];

      $objOvertureTracker->AddOvertureTracker($_SESSION["_QZ_QUOTE_DETAILS_"]["qz_log_id"],$quoteTypeID,$val['RANK'],$counter,$advertiser,$_SERVER['REMOTE_ADDR'],time(),'IMPRESSION','RIGHT');


      $objTmpl->Prepare($resOvertureLinks);

      $overtureHtml .= $objTmpl->GetContent();

      if($counter == 5)
         break;
   }

   return $overtureHtml;
}

   function setUniqueQuotesTracker()
   {
      global $objWs;
      global $SYSTEM_LOCKED;
      global $dbHandle;

      if($_SESSION['RETRIEVE_QUOTES']['SID'])
         return;

      if($SYSTEM_LOCKED)
         return;

      $currentDateBlowsearch = date("Y-m-d");
      $blowsearchUserId        = $_SESSION["_QZ_QUOTE_DETAILS_"]["quote_user_id"];
      $systemType              = 7;


      $sql = "SELECT count(*) AS number from logs l, qzquotes qzq WHERE l.id=qzq.log_id AND  qzq.quote_type_id='".$systemType."' AND l.quote_user_id='".$blowsearchUserId."'";

      if(! $dbHandle->Exec($sql))
         return;

      if(! $dbHandle->FetchRows())
         return;

      if($dbHandle->GetFieldValue("number") != 1)
         return;

      //$objWs->SetAdvivaTracker();

   }

   function noJSConversion()
   {
      global $objTrackDetailsNoJavascript;

      // if previosly the user has javascript disabled
      if(empty($_SESSION["_NO_JAVASCRIPT_"]["ID"]))
         return;

      $logID               = $_SESSION["_QZ_QUOTE_DETAILS_"]["qz_log_id"];
      $trackNoJavascriptID = $_SESSION["_NO_JAVASCRIPT_"]["ID"];
      $status              = 2;    // activated javascript and then obtained a quote

      if(! $objTrackDetailsNoJavascript->UpdateTrackDetailsStatusNoJavascript($trackNoJavascriptID,$status,$logID))
         print $objTrackDetailsNoJavascript->strERR;

      unset($_SESSION["_NO_JAVASCRIPT_"]);
   }

   function noCookiesConversion()
   {
      global $objTrackDetailsNoCookies;

      // if previosly the user has javascript disabled
      if(empty($_SESSION["_NO_COOKIES_"]["ID"]))
         return;

      $logID            = $_SESSION["_QZ_QUOTE_DETAILS_"]["qz_log_id"];
      $trackNoCookiesID = $_SESSION["_NO_COOKIES_"]["ID"];
      $status           = 1;    // obtained a quote

      if(! $objTrackDetailsNoCookies->UpdateTrackDetailsStatusNoCookies($trackNoCookiesID,$status,$logID))
         print $objTrackDetailsNoCookies->strERR;

      unset($_SESSION["_NO_COOKIES_"]);
   }
   //////////////////////////////////////////////////////////////////////////////FB
   //
   // [FUNCTION NAME]: GetUrl
   //
   // [DESCRIPTION]:   Download/Open a remote web page
   //
   // [PARAMETERS]:    $url="", [method="GET"], [$params=""] [$referer=""]
   //
   // [RETURN VALUE]:  downloaded page text if success, "" otherwise
   //
   // [CREATED BY]:    Eugen SAVIN (seugen@abc.ro) 2004-08-16
   //
   // [MODIFIED]:      - [programmer (email) date]
   //                    [short description]
   //////////////////////////////////////////////////////////////////////////////FE
   function GetUrl($url="", $method="GET", $params="", $referer="")
   {
      $this->maxUrlsCounter--;

      if($this->maxUrlsCounter <= 0)
         return false;

      $this->htmlContent = "";

      if(empty($url))
      {
         $this->strERR = GetErrorString("INVALID_URL");
         return false;
      }

      // add protocol
      if(! preg_match("/:\/\//", $url))
         $url = $this->proto.$url;

      if(is_array($params))
      {
         $paramString = "";

         foreach($params as $key => $value)
            $paramString .= $key . '=' . $value . '&';

         $paramString = preg_replace("/\&$/","", $paramString);
         $params = $paramString;
      }

      // override referer
      if(! empty($referer))
         $this->httpReferer =  $referer;

      $ch = curl_init();

      if(! empty($this->httpReferer))
         curl_setopt($ch, CURLOPT_REFERER, $this->httpReferer);

      if($method == "GET")
      {
         if(! empty($params))
         {
            if(preg_match("/\?/", $url))
               $url .= "&$params";
            else
               $url .= "/?".$params;
         }
      }
      elseif($method == "POST")
      {
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      }
      elseif($method == "POST_XML")
      {
         $header[] = "Host: 195.26.59.68";
         $header[] = "Content-Type: text/xml; charset=utf-8";
         $header[] = "SOAPAction: http://TGSL/TES_Aggregator/Quote";
         $header[] = "Content-length: ".strlen($params)." \r\n";
         $header[] = $params;

         curl_setopt($ch, CURLOPT_URL,$url);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
         curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

         curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 30);

         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_VERBOSE, true);
         curl_setopt($ch, CURLOPT_HEADER, 1);
      }

      $url = preg_replace("/\&$/","", $url);

      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);

      $parsedUrl = parse_url($url);

      if(empty($this->cookieFileName))
      {
         if(! $this->cookieFileName = $this->GenerateCookieFile())
         {
            $this->AddError(GetErrorString("CANNOT_GENERATE_COOKIE_FILENAME"));
            return false;
         }
      }

      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      // try to get the url more times in case of failure
      $retryDelay = $this->retryDelay;
      $try = 1;

      while($try <= $this->retryNumber)
      {
         $result = curl_exec($ch);

         if(! empty($result))
            break;

         $this->LogMsg($this->siteName." => ".$url." ( retry $try )");

         sleep($retryDelay);

         $retryDelay    += 30;

         $this->SetTimeout($this->timeout + 60);

         $try++;

      } // end while($try < $this->retryNumber)

      curl_close ($ch);

      if(empty($result))
      {
         $this->strERR  = "CANNOT_GET_REMOTE_URL";
         $this->strERR .= curl_error($ch)."\n";
         return false;
      }

      preg_match("/.*(\<\?xml version=\"1\.0\".*)/is", $result, $extractXmlResult);
      $result = $extractXmlResult[1];

      $this->lastUrl     = $url;
      $this->httpReferer = $url;
      $this->htmlContent = $result;

      if(preg_match("/^(.+)\<html\>/isU", $result, $matches))
         $this->httpHeader = $matches[1];

      return $result;
   }

   function insertEmailTrackerQuotes()
   {
      global $SYSTEM_LOCKED;
      global $dbHandle;

//      if($_SESSION['RETRIEVE_QUOTES']['SID'])
//         return;

      if($SYSTEM_LOCKED)
         return;

      $logId            = $_SESSION["_QZ_QUOTE_DETAILS_"]["qz_log_id"];
      $email_tracker_id = $_COOKIE['EmailTracking'];

      //print_r($dbHandle);die;

      $sql = "insert into email_tracker_quotes(`email_tracker_id`,`log_id`) values('$email_tracker_id','$logId')";

      //print($sql);die;

      if(! $dbHandle->Exec($sql))
         return;

   }

?>

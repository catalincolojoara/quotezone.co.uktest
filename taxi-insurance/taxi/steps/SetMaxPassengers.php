<?php
session_start();

$cabType = $_GET['cab'];
$selPass = $_SESSION["temp_capacity"];

$taxiCapacityArray = array();

$taxiCapacityArray = array(
      ""   => "Please Select", 
      "4"  => "4", 
      "5"  => "5", 
      "6"  => "6", 
      "7"  => "7",
      "8"  => "8", 
      "9"  => "9", 
      "10" => "10", 
      "11" => "11",
      "12" => "12",
      "13" => "13",
      "14" => "14+",
      );

if($cabType == "1" || $cabType == "2")
{
   $taxiCapacityArray = array(
      ""   => "Please Select", 
      "4"  => "4", 
      "5"  => "5", 
      "6"  => "6", 
      "7"  => "7",
      "8"  => "8+", 
      );
}

$sel ="";

$sel ="<select  class=\"cssTTSizeSelect\" name=\"taxi_capacity\" tabindex=\"9\" onChange=\"setTempSes(this.value)\">";

foreach($taxiCapacityArray as $key=>$val)
{
   $selected = "";
   if($selPass == $key)
      $selected = "selected";

   $sel .="<option value=\"$key\" $selected>$val</option>";
}

$sel .="</select>";

print $sel;

?>

<?php
session_start();

include_once "../modules/globals.inc";
include_once "modules/TrackUrls.php";
include_once "OvertureTracker.php";

$hostIP  = $_SERVER['REMOTE_ADDR'];
$urlName = $_GET['urlName'];
$urlAddr = $_GET['urlAddr'];
$top     = $_GET['top'];
$qzTop   = $_GET['qzTop'];

if (isset($_GET["popupPosition"]))
        $popupPos = $_GET["popupPosition"];
else
        $popupPos = "RIGHT";

$quoteRef    = $_GET['quoteRef'];
$logID       = $_GET['logId'];
$quoteTypeID = '10';

if($popupPos == 'NEWS')
   $quoteTypeID = '0';

//prepare advertiser
$urlName    = preg_replace("/\<br\>/","", $urlName);
$urlName    = preg_replace("/\s+/","", $urlName);

if(! preg_match("/^http/i", $urlName))
   $urlName = "http://".$urlName;

$urlName    = preg_replace("/www\./","", $urlName);
$urlName    = strtolower($urlName);

$urlDetails = parse_url($urlName);
$advertiser = $urlDetails['host'];

$info    = $advertiser;

if(empty($logID)) $logID = 0;
if(empty($top))   $top   = 0;
if(empty($qzTop)) $qzTop = 0;

$overtureTracker   = new COvertureTracker();

LogMsgDB($logID, $quoteTypeID, $top, $qzTop, $info, $hostIP, $popupPos);

if($urlAddr)
   Redirect($urlAddr);
else
   Redirect($trackUrlInfo["url"]);

function Redirect($url = "")
{
   if (headers_sent())
   {
      print <<<  END_TAG

      <SCRIPT LANGUAGE="JavaScript">
      //<!--
         location.href="$url";
      //-->
      </SCRIPT>

END_TAG;
   }
   else
   {
      $today = date("D M j G:i:s Y");
      header("HTTP/1.1 200 OK");
      header("Date: $today");
      header("Location: $url");
      header("Content-Type: text/html");
   }

   print <<< END_TAG

   <HTML><HEAD><TITLE>302 Moved</TITLE></HEAD><BODY>
   <H1>302 Moved</H1>
   The document has moved <A HREF="$url">here</A>.
   </BODY></HTML>

END_TAG;

   exit(0);
}

 function LogMsg($msg="")
 {
   $fd = fopen("OvertureTracker.log", "a+");

   if(! $fd)
      return;

   $today = date("D M j G:i:s Y");

   fwrite($fd, "$today $msg\n");

   fclose($fd);
 }

 function LogMsgDB($logID=0, $quoteTypeID=0, $top=0, $qzTop=0, $additionalInfo="", $hostIP="", $popupPos="RIGHT")
 {
   global $overtureTracker;
   
   $overtureTracker->AddOvertureTracker($logID, $quoteTypeID, $top, $qzTop, $additionalInfo, $hostIP, $time, $popupPos);
 }

?>

<?php
   $startDate     = $argv[1];
   $endDate       = $argv[2];
   $quoteTypeId   = $argv[3]; 
   $quoteTypeName = $argv[4]; 
   $quoteTypeNameU = ucfirst($quoteTypeName);

   include_once "modules/globals.inc";
   include_once "EmailOutbounds.php";
   include_once "EmailOutboundsEngine.php";
   include_once "EmailOutboundsEngineTaxi.php";   
   include_once "MySQL.php";
   include_once "FileStoreClient.php";

   $sqlObj = new CMySQL();
   $sqlObj->Open();

   $objFileStore = new CFileStoreClient($quoteTypeName, 'in');

   $sqlCmd = "select 
                 l.id as LogId,l.filename 
              from 
                 logs l, qzquotes qzq 
              where 
                 l.id=qzq.log_id and 
                 qzq.quote_type_id=$quoteTypeId and 
                 l.date>='$startDate' and 
                 l.date<='$endDate' and 
                 l.host_ip <> '81.196.65.167' and l.host_ip <> '86.125.114.56' and l.host_ip <> '37.153.72.90';";

   if(! $sqlObj->Exec($sqlCmd))
      print "exec : ".$sqlObj->GetError();

   if(! $sqlObj->GetRows())
      print "get rows : ".$sqlObj->GetError();

  $detailsArr = array();

  while($sqlObj->MoveNext())
  {
    $logID              = $sqlObj->GetFieldValue("LogId");
    $detailsArr[$logID] = $sqlObj->GetFieldValue("filename");
  }

  $objEmailOutbounds             = new CEmailOutbounds();

  $i = 0;
  foreach($detailsArr as $logId => $filename)
  {
     $fileNameOutbounds  = $filename;
     $logIDOutbounds     = $logId;

     $className = 'CEmailOutboundsEngineTaxi';
     $objEmailOutboundsEngine = new $className($fileNameOutbounds, $dbHandle);

     if(! $data = $objFileStore->GetFile($filename))
     { continue;}

     eval("\$Session = $data;");

     $dataString = '';
     $header = '';

     if($result = $objEmailOutboundsEngine->FormatUserDetails($Session))
     {
        $i++;
        $resultArray = explode('&',$result);

        foreach($resultArray as $index => $details)
        {
           $detailsArray = explode('=',$details);
           $header     .= '"'.$detailsArray[0].'",'; 

           if($detailsArray[0] == 'QUOTE_DATE_FIELD')
              $dataString .= '"'.date('m/d/Y',$Session['TIME']).'",';
           else
           $dataString .= '"'.urldecode($detailsArray[1]).'",'; 
        }
        $dataString .= "\n";

        if($i <= 1)
          print($header."\n");
 
        print($dataString);
     }
   }

?>

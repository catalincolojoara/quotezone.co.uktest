<?php
session_start();

header("Last-Modified: " . gmdate( "D, j M Y H:i:s") . " GMT" );
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", FALSE);
header("Pragma: no-cache"); // HTTP/1.0
header("Content-type: image/png");

error_reporting(0);

include_once "../modules/globals.inc";
include_once "TrackDetailsNoJavascript.php";

$objTrackDetailsNoJavascript = new CTrackDetailsNoJavascript();

$quoteTypeID = 8;

$userAgent = mysql_escape_string($_SERVER['HTTP_USER_AGENT']);
$hostIP    = $_SERVER['REMOTE_ADDR'];

//change status = 1 if previous the user has javascript disabled
if(isset($_SESSION["_NO_JAVASCRIPT_"]["status"]) AND $_SESSION["_NO_JAVASCRIPT_"]["status"] == 0 )
{
   //set in session new status
   $_SESSION["_NO_JAVASCRIPT_"]["status"] = 1;

   $trackNoJavascriptID = $_SESSION["_NO_JAVASCRIPT_"]["ID"];
   $status              = 1;     // activated javascript

   if(! $objTrackDetailsNoJavascript->UpdateTrackDetailsStatusNoJavascript($trackNoJavascriptID,$status))
      print $objTrackDetailsNoJavascript->strERR;
}

?>

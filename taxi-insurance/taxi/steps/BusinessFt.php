<?php
   include_once "../modules/globals.inc";
   include_once "YourDetailsElements.php";
   include_once "Template.php";
   include_once "Business.php";

   $objBus = new CBusiness();

   $key     = trim($_GET['key']);

   if($key == '???')
   {
      if(! $objBus->GetAllBusiness($resList))
         exit(0);
   }
   else
   {
      if(! $objBus->GetAllBusinessByName($key,$resList))
      {
         if(! $objBus->GetAllBusinessByName(substr($key, 0, 2),$resList))
        {
            FindList();
            exit(0);
        }
     }
   }

ShowAjxList($resList);


function FindList()
{
   global $key;
   global $_YourDetails;

   if(! strlen($key))
   {
      $resList = $_YourDetails['business_list'];

   print <<<EOT

   <script language="JavaScript">
EOT;

      print "parent.document.YourDetails.business_trade.value = '';\n";
   print <<<EOT

   </script>

EOT;

      return;
   }

   //$errMesg = "No matches found for \\'$key\\'. Please try a different description.";
   $errMesg = "ERR_NO_MATCH";

   if(strlen($key) < 2)
      //$errMesg = GetErrorString('INVALID_OCCUPATION_BUSINESS_SEARCH_WORD_LENGTH');
      //$errMesg = 'INVALID_OCCUPATION_BUSINESS_SEARCH_WORD_LENGTH';
      $errMesg = 'ERR_WORD_LENGTH';

   // show error in case we did not find any entries
   print <<<EOT
   <script language="JavaScript">
      var busErrBg  = document.getElementById('business_error_bg');
      var busErrTbl = document.getElementById('business_trade_error_tbl');
      var busErrTxt = document.getElementById('business_error_txt');

      busErrTbl.className        = 'frmTblExtCntserror';
      busErrBg.className         = 'frmErrBg';
      busErrBg.style.borderWidth = '1px';
      busErrTxt.className        = 'frmTxtErr';
      busErrTxt.innerHTML        = '$errMesg';
      busErrTxt.style.visibility = 'visible';
  </script>

EOT;

   if(strlen($key) >= 2)
      ShowAjxList($resList);
}


function ShowAjxList($resList)
{
   global $control;
   global $code;
   global $selected;
   global $key;

   print "<div id=\"postcodeContainer\" style=\"\">";  
   print "<iframe id=\"postcodeIEiframe\" src=\"javascript:'';\" marginwidth=\"0\" marginheight=\"0\" align=\"bottom\" scrolling=\"no\" frameborder=\"0\" style=\"position:absolute; left:0; top:0px; display:block; filter:alpha(opacity=0); height: 160px;\"></iframe>";

   print "<select onBlur=\"BusinessFtControl();\" name=\"fake_business_list\" class=\"frmSelBoxMedFake\" style=\"width: 215px;z-index:1; position:absolute;\" size=\"11\" tabindex=\"10\" onClick=\"selectBusFT(this.options[this.selectedIndex].text);\" onKeyUp=\"selectBusFtFromFake();\">"."\n";

   $counter = 0;
   foreach($resList as $k => $v)
   {
      if(! ($counter++ % 2))
         $bgColor = "#EEEEEE";
      else
         $bgColor = "#FFFFFF";

      print "  <option value=\"$k\" style=\"background-color: $bgColor\">$v</option>\n";
   }

   if($key != '???')
   {
      if(! ($counter++ % 2))
         $bgColor = "#EEEEEE";
      else
         $bgColor = "#FFFFFF";

      print "  <option value=\"other\" style=\"color: blue; background-color: $bgColor\">Not Listed? Click Here!</option>\n";
   }

   print "</select>\n";
   print "</div>";


print <<< EOT
   <script language=\"javascript\">

     // hide error when found  business

      var busErrBg  = document.getElementById('business_error_bg');
      var busErrTbl = document.getElementById('business_error_tbl');
      var busErrTxt = document.getElementById('business_error_txt');

      busErrTbl.className        = 'frmTblExtCnts';
      busErrBg.className         = '';
      busErrBg.style.borderWidth = '0px';
      busErrTxt.innerHTML        = '';
      busErrTxt.style.visibility = 'hidden';

      // we have to check xml errors to hide
      var elTd = busErrTbl.getElementsByTagName('td');

      for(var i = 0; i < elTd.length; i++)
      {
         if(elTd[i] == busErrTxt)
            continue;

         if(elTd[i].className == 'frmTxtErr')
         {
            elTd[i].innerHTML = '';
            elTd[i].className = '';
         }
      }


      var busListDiv       = document.getElementById('div_ft_business');
      var fakeBusListDiv   = document.getElementById('bus_id');
      var realBusSelBox    = document.Drivers.business_list;
      var fakeBusSelBox    = document.Drivers.fake_business_list;
      var targetElement    = document.getElementById('div_ft_business');
      var sourceElement    = document.getElementById('div_ft_business_selbox');

       //alert('here');
       if(fakeBusSelBox.length > 11)
          fakeBusSelBox.size = 11;
       else
          fakeBusSelBox.size = fakeBusSelBox.length;

      //Position.absolutize('bus_id');
      Element.extend(busListDiv);
      Element.extend(fakeBusListDiv);
      Element.extend(fakeBusSelBox);

		var objDriver =  document.Drivers.business_key;

      if(typeof(objDriver) == 'undefined')
   	{
	      objDriver =  document.Drivers.additional_business_key;
   	}


       //moveIframeToElement(fakeBusListDiv, objDriver, 'bli');
      fakeBusListDiv.show();
      //fakeBusSelBox.focus();

   </script>
EOT;


}

function ShowList($resList)
{
   global $control;
   global $code;
   global $selected;

   print<<<EOT

      <script language="JavaScript">

         var listDiv     = document.getElementById('div_ft_business');
         var keyDiv      = document.getElementById('div_ft_business_search');
         var fakeListDiv = document.getElementById('bus_id');
EOT;
         print "var listElm     = document.Drivers.$selected"."business_list;\n";

print <<<EOT

         var fakeListElm = document.Drivers.fake_business_list;

         listDiv.style.display = 'block';
        // keyDiv.style.display  = 'none';

         var busErrBg  = document.getElementById('business_error_bg');
         var busErrTbl = document.getElementById('business_error_tbl');
         var busErrTxt = document.getElementById('business_error_txt');

         busErrTbl.className        = 'frmTblExtCnts';
         busErrTxt.style.visibility = 'hidden';
         busErrBg.className         = '';
         busErrTxt.innerHTML        = '';

         if('$control' == '')
            fakeListDiv.style.visibility = 'visible';

         listElm.options.length     = 1;
         fakeListElm.options.length = 1;

         parent.addOptionSelectBox(listElm,1,'   Other...','other');

         parent.addOptionSelectBox(fakeListElm,1,'   Other...','other');

EOT;

      $countElements = count($resList) + 2;

      $i=1;
      foreach($resList as $k => $v)
      {
         $i++;

         print "parent.addOptionSelectBox(listElm,$i,'$v','$k');\n";
         print "parent.addOptionSelectBox(fakeListElm,$i,'$v','$k');\n";
      }

      $i++;

      print <<<EOT

      parent.addOptionSelectBox(listElm,$i,'   Other...','other');
      parent.addOptionSelectBox(fakeListElm,$i,'   Other...','other');
EOT;

      if($countElements < 11)
      {
         print "fakeListElm.size = '$countElements'; \n";

      }
      else
      {
         print "fakeListElm.size = 11; \n";
      }

      print <<<EOT

      if('$control' == '')
      {
         fakeListDiv.style.top   = parent.getPosY(listElm) - fakeListElm.offsetHeight + listElm.offsetHeight + 'px';
         fakeListDiv.style.left  = parent.getPosX(listElm)  + 'px';
         fakeListElm.focus();
      }
      else
         parent.selectOptionSelectBox(listElm, '$code');


EOT;

print <<<EOT
      </script>
EOT;
}

?>

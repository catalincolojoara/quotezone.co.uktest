<?php


$_YourDetails = array(

   "taxi_used_for" => array(
      ""  => "Please Select", 
      "1" => "Private Hire", 
      "2" => "Public Hire",
      ),

   "taxi_type" => array(
      ""  => "Please Select", 
      "1" => "Black Cab", 
      "2" => "Saloon", 
      "3" => "MPV", 
      ),
  
   "type_of_cover" => array(
     // ""  => "Please Select", 
      "1" => "Fully comprehensive", 
      "2" => "Third party fire and theft", 
      "3" => "Third party", 
      ),

   "period_of_licence" => array(
      ""   => "Please Select", 
      "1"  => "Under 6 Months",
      "2"  => "6 Months To 1 Year",
      "3"  => "1 - 2 Years",
      "4"  => "2 - 3 Years",
      "5"  => "3 - 4 Years",
      "6"  => "4 - 5 Years",
      "7"  => "5 - 6 Years",
      "8"  => "6 - 7 Years",
      "9"  => "7 - 8 Years",
      "10" => "8 - 9 Years",
      "11" => "9 - 10 Years",
      "12" => "Over 10 Years",
      ),

   "taxi_badge" => array(
      ""   => "Please Select", 
      "1"  => "No Badge",
      "2"  => "Under 6 Months",
      "3"  => "6 Months To 1 Year",
      "4"  => "1 - 2 Years",
      "5"  => "2 - 3 Years",
      "6"  => "3 - 4 Years",
      "7"  => "4 - 5 Years",
      "8"  => "5 - 6 Years",
      "9"  => "6 - 7 Years",
      "10" => "7 - 8 Years",
      "11" => "8 - 9 Years",
      "12" => "9 - 10 Years",
      "13" => "Over 10 Years",
      ),

   "taxi_driver" => array(
      "1" => "Insured Only",
      "2" => "Insured +1 Named Driver",
      "3" => "Insured and 2+ Named Drivers",
      "4" => "Insured and Spouse",
      ),

   "claims_5_years" => array(
      "No"   => "No Claims",
      "Yes"  => "Yes, Claims Made",
      ),

   "convictions_5_years" => array(
      "No"   => "No",
      "Yes"  => "Yes",
      ),

   "year_of_manufacture" => array(
         " => " => "Please Select",
      ),

   "vehicle_mileage" => array(
         "" => "Please Select",
         "1" => "Less than 10000",
         "2" => "10,000 - 20,000",
         "3" => "20,000 - 30,000",
         "4" => "30,000 - 40,000",
         "5" => "40,000 - 50,000",
         "6" => "50,000 - 60,000",
         "7" => "60,000 - 70,000",
         "8" => "70,000 - 80,000",
         "9" => "80,000 - 90,000",
         "10" => "90,000 - 100,000",
         "11" => "100,000 - 110,000",
         "12" => "110,000 - 120,000",
         "13" => "120,000 - 130,000",
         "14" => "130,000 - 140,000",
         "15" => "140,000 - 150,000",
         "16" => "Greater than 150,000",
      ),

   "taxi_capacity" => array(
      ""   => "Please Select", 
      "4"  => "4", 
      "5"  => "5", 
      "6"  => "6", 
      "7"  => "7",
      "8"  => "8", 
      "9"  => "9", 
      "10" => "10", 
      "11" => "11",
      "12" => "12",
      "13" => "13",
      "14" => "14+",
      ),

   "taxi_ncb" => array(
      ""  => "Please Select", 
      "0" => "No NCB", 
      "1" => "1 Year", 
      "2" => "2 Years", 
      "3" => "3 Years",
      "4" => "4 Years", 
      "5" => "5+ Years",
   ),

   "private_car_ncb" => array(
      ""  => "Please Select", 
      "0" => "No NCB", 
      "1" => "1 Year", 
      "2" => "2 Years", 
      "3" => "3 Years",
      "4" => "4 Years", 
      "5" => "5+ Years",
   ),


   "gap_insurance" => array(
      ""  => "Please Select", 
      "0" => "Yes", 
      "1" => "No", 
   ),

   "breakdown_cover" => array(
      ""  => "Please Select", 
      "0" => "Yes", 
      "1" => "No", 
   ),

   "title"  => array(
      " => "    =>"Please Select",
      "Mr"  =>"Mr",
      "Mrs" =>"Mrs",
      "Ms"  =>"Ms",
      "Miss"=>"Miss",
      ),

      "best_time_call" => array(
         "0" =>"Now (ASAP)",
         "1" =>"Anytime",
         "2" => "Morning (9am - 12pm)",
         "3" => "Afternoon (12pm - 5.30pm)",
         "4" => "Evening (5.30pm - 8pm)",
      ),

      "best_day_call" => array(
         "0" => "Any day",
         "1" => "Monday",
         "2" => "Tuesday",
         "3" => "Wednesday",
         "4" => "Thursday",
         "5" => "Friday",
         "6" => "Saturday",
         "7" => "Sunday",
      ),

   "date_of_birth_dd" => array(
         " => "=>"Day",
      ),
      
      "date_of_birth_mm" => array(
                  " => "=>"Month",
                 "01"=>"January",
                 "02"=>"February",
                 "03"=>"March",
                 "04"=>"April",
                 "05"=>"May",
                 "06"=>"June",
                 "07"=>"July",
                 "08"=>"August",
                 "09"=>"September",
                 "10"=>"October",
                 "11"=>"November",
                 "12"=>"December",
                 ),
      
      "date_of_birth_yyyy" => array(
         " => "=>"Year",
      ),
      "date_of_insurance_start_dd" => array(
         " => "=>"Day",
      ),
      
      "date_of_insurance_start_mm" => array(
                  " => "=>"Month",
                 "01"=>"January",
                 "02"=>"February",
                 "03"=>"March",
                 "04"=>"April",
                 "05"=>"May",
                 "06"=>"June",
                 "07"=>"July",
                 "08"=>"August",
                 "09"=>"September",
                 "10"=>"October",
                 "11"=>"November",
                 "12"=>"December",
                 ),
      
      "date_of_insurance_start_yyyy" => array(
         " => "=>"Year",
      ),

      "age_of_vehicle"  => array(
         " => " => "&nbsp;&nbsp;&nbsp;&nbsp;--Select--",
         "0" => "Under 1 year",
         "1" => "1 year",
         "2" => "2 years",
         "3" => "3 years",
         "4" => "4 years",
         "5" => "5 years",
         "6" => "6 years",
         "7" => "7 years",
         "8" => "8 years",
         "9" => "9 years",
         "10" => "10 years",
         "11" => "11 years",
         "12" => "12 years",
         "13" => "13 years",
         "14" => "14 years",
         "15" => "15 years",
         "16" => "Over 15 years",
      ),

);
//print_r($_YourDetails);
   $today = date("d/m/Y");
      
   list($todDay,$todMonth,$todYear) = explode("/",$today);
   
   $curYear = date("Y") - 18;
   for($i=$curYear;$i>=1901;$i--)
   {
     $_YourDetails ["date_of_birth_yyyy"][$i] = $i;
   }

   for($i=1;$i<=31;$i++)
   {
      if ($i<10) $i = "0".$i;
      $_YourDetails["date_of_birth_dd"][$i] = $i;
   }

   $curYear = date("Y");
   for($i=$curYear;$i>=1901;$i--)
   {
     $_YourDetails ["year_of_manufacture"][$i] = $i;
   }
   
   $curYear  = date("Y");
   $nextYear = $curYear + 1;

   $_YourDetails["date_of_insurance_start_yyyy"][$curYear]  = $curYear;
   $_YourDetails["date_of_insurance_start_yyyy"][$nextYear] = $nextYear;

   for($i=1;$i<=31;$i++)
   {
      if ($i<10) $i = "0".$i;
      $_YourDetails["date_of_insurance_start_dd"][$i] = $i;
   }







?>

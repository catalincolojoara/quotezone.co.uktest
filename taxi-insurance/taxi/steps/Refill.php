<?php
//   session_start();
//   session_destroy();
 
  error_reporting(1);
  session_start();

  include_once "../modules/globals.inc";
  include_once "File.php";
  include_once "TrackUrls.php";
  include_once "Tracker.php";
  include_once "FileStoreClient.php";
  include_once "functions.php";

   $fileName  = $_GET['fileName'];
   $debug     = $_GET['debug'];
   $skipSites = $_GET['sites'];
   $rQuotes   = $_GET['retQuotes'];
   $realUser  = $_GET['real'];
   $sid       = $_GET['sid'];

   // retrieve quotes by user from email
   if(! empty($sid))
      $fileName = substr($sid, 0, 10)."_".substr($sid, 10, 6);

   $fileNameIn = FS_ROOT_PATH."in/$fileName";

   $file = new CFile();
   $objFSClient = new CFileStoreClient(FS_SYSTEM_TYPE,'in');

   // get file
   $objFSClient->GetFile($fileName);

  if(! $file->Open($fileNameIn, "r"))
    $file->GetError();

  if(! $size = $file->GetSize($fileNameIn))
    $file->GetError();

  if(! $data = $file->Read($size))
    $file->GetError();

  if(! $file->Close())
    $file->GetError();

  eval("\$Session = $data;");

   // retrieve quotes by quote ref or email from RetrieveQuote page
   if($debug)
   {
      $password = $Session['_QZ_QUOTE_DETAILS_']['quote_password'];

      unset($Session['RETRIEVE_QUOTES']);
      unset($Session['_QZ_QUOTE_DETAILS_']);

      $Session['_QZ_QUOTE_DETAILS_']['quote_password'] = $password;

      $Session['TEMPORARY FILE NAME'] = $fileName;
      $Session['LOCKED'] = false;

      // email change testing@test.quotezone.co.uk
      // change insurance start
      // change policy run out

   }  
   
   // retrieve quotes by quote ref or email from RetrieveQuote page
   if($rQuotes)
   {
      unset($Session['RETRIEVE_QUOTES']);
      $Session['RETRIEVE_QUOTES']['request'] = 'GET QUOTES';
      $Session['RETRIEVE_QUOTES']['QUOTE_USER_ID'] = $Session['_QZ_QUOTE_DETAILS_']['quote_user_id'];

      $Session['RETRIEVE_QUOTES']['SID']     = false;
      $Session['RETRIEVE_QUOTES']['STATUS']  = true;

      $Session['TEMPORARY FILE NAME'] = $fileName;
      $Session['LOCKED'] = true;
   }

   // retrieve quotes by user from email
   if(! empty($sid))
   {
      if(! $file->Exists(FS_ROOT_PATH."in/".$fileName))
         redirect("../../");

      if(isset($_GET["step"]) AND $_GET["step"] == 1)
      {
         // first step with session loaded
         $Session['LOCKED']      = false;
         $Session["ACTIVE STEP"] = 1;
      }
      else
      {
         $Session['TEMPORARY FILE NAME']        = $fileName;
         $Session['RETRIEVE_QUOTES']            = $_SESSION['RETRIEVE_QUOTES'];
         $Session['RETRIEVE_QUOTES']['request'] = 'GET QUOTES';

         $Session['RETRIEVE_QUOTES']['QZ_ID']           = $Session['_QZ_QUOTE_DETAILS_']['qz_quote_id'];
         $Session['RETRIEVE_QUOTES']['QUOTE_USER_ID']   = $Session['_QZ_QUOTE_DETAILS_']['quote_user_id'];
         $Session['RETRIEVE_QUOTES']['QUOTE_REFERENCE'] = $Session['_QZ_QUOTE_DETAILS_']['quote_reference'];
         $Session['RETRIEVE_QUOTES']['QUOTE_PASSWORD']  = $Session['_QZ_QUOTE_DETAILS_']['quote_password'];

         $Session['RETRIEVE_QUOTES']['STATUS']  = true;
         $Session['RETRIEVE_QUOTES']['SID']     = true;
         $Session['LOCKED']                     = true;

         $trackUrls = new CTrackUrls();
         $tracker   = new CTracker();

         $trackUrlInfo = $trackUrls->GetTrackUrlByName("Retrieve Quote SID");

         if($trackUrlInfo !== false)
         {
            $hostIP         = $_SERVER['REMOTE_ADDR'];
            $additionalInfo = "QuoteRef: ".$Session['_QZ_QUOTE_DETAILS_']['quote_reference'];
            $time           = time();

            $tracker->AddTracker(0,0,$trackUrlInfo['id'], $additionalInfo, $hostIP, $time);
         }
      }
   }

  $_SESSION = $Session;

   if(! empty($skipSites))
   {
      $_SESSION['DEBUG_SITES'] = array();

      $skipSitesSlice = explode(":",$skipSites);

      foreach($skipSitesSlice as $index => $siteID)
         $_SESSION['DEBUG_SITES'][$siteID] = true;
   }

//   if($realUser AND $debug)
//      redirect("../index.php?step=1");

   redirect("../");

function redirect($url="")
{
      if(! preg_match("/cs=/i", $url))
      {
         if(! empty($_GET['cs']))
            $cookieSession = $_GET['cs'];

         if(! empty($_POST['cs']))
            $cookieSession = $_POST['cs'];

         if( defined('COOKIE_SESSION_ID'))
           $cookieSession = COOKIE_SESSION_ID;

         if(! empty($cookieSession))
         {
            $prepUrl = $url."?cs=$cookieSession";
            if(preg_match("/\?/i", $url))
               $prepUrl = $url."&cs=$cookieSession";

            $url = $prepUrl;
         }
      }

   if(headers_sent())
   {
               print <<<  END_TAG

               <SCRIPT LANGUAGE="JavaScript">
               <!--
               location.href="$url";
               //-->
               </SCRIPT>

END_TAG;

   }
   else
   {
      $today = date("D M j G:i:s Y");
      header("HTTP/1.1 200 OK");
      header("Date: $today");
      header("Location: $url");
      header("Content-Type: text/html");
   }

   exit(0); //if it doesn't work die (Javascript disabled?)
}



?>


<?php

   //force session_start
   session_start();

   include_once "../modules/globals.inc";
   include_once "TrackDetailsNoCookies.php";

   if(! empty($_SERVER['REMOTE_ADDR']))
   {
      // check if we have cookie enabled and define it
      if(empty($_COOKIE[session_name()]))
      {
         if(preg_match("/loans.quotezone.co.uk/i", $_SERVER['HTTP_REFERER']))
         {
            if(! defined('COOKIE_SESSION_ID'))
            {
               $urlInfo    = parse_url($_SERVER['HTTP_REFERER']);
               parse_str($urlInfo['query'], $getParams);

               if(! empty($getParams['cs']))
                 define('COOKIE_SESSION_ID',$getParams['cs']);
            }
         }

         if(! defined('COOKIE_SESSION_ID'))
         {
            define("COOKIE_SESSION_ID",session_id());

            //track no cookies
            $objTrackDetailsNoCookies = new CTrackDetailsNoCookies();

            $quoteTypeID = 8;
            $userAgent   = mysql_escape_string($_SERVER['HTTP_USER_AGENT']);
            $hostIP      = $_SERVER['REMOTE_ADDR'];

            if(empty($_SESSION["_NO_COOKIES_"]["ID"]))
            {
               if($trackNoCookiesID = $objTrackDetailsNoCookies->AddTrackDetailsNoCookies($quoteTypeID,$userAgent,$hostIP))
               {
                  $_SESSION["_NO_COOKIES_"]["ID"]     = $trackNoCookiesID;
                  $_SESSION["_NO_COOKIES_"]["status"] = 0;
               }
            }

            $urlInfo    = parse_url($_SERVER['HTTP_REFERER']);
            parse_str($urlInfo['query'], $getParams);

             $url = $_SERVER['HTTP_REFERER']."?cs=".COOKIE_SESSION_ID;
            if(! empty($getParams))
               $url = $_SERVER['HTTP_REFERER']."&cs=".COOKIE_SESSION_ID;

            //redirect first time to get coookie session id from referer
             print <<<END_TAG
                  <script language="javascript">
                  <!--
                  parent.Redirect("$url");
                  //-->
                  </script>

END_TAG;

         }
      }
   }

?>

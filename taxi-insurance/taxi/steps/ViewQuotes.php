<?php

   session_start();

   if($_SESSION['TESTING SITE ID'])
       if(! defined('TESTING_MODE'))
          define("TESTING_MODE", "1");

   $_SESSION['FOUND_SITE_ERROR'] = false;

   include_once "../modules/globals.inc";

   include_once "File.php";
   include_once "Template.php";

   include_once "QuoteStatus.php";
   include_once "QuoteError.php";
   include_once "QuoteDetails.php";
   include_once "InsurersCall.php";
   include_once "functions.php";
   include_once "FileStoreClient.php";

   $logID = $_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id'];

   $trackerMsg = '';

   if($_SESSION['RETRIEVE_QUOTES']['STATUS'])
      $trackerMsg = 'Login';

   if($_SESSION['RETRIEVE_QUOTES']['SID'])
      $trackerMsg = 'Sid';

     print <<<EOT
 <script language="javascript">
     parent.SetTrackerMsg('$trackerMsg');
 </script>

print <<<EOT
<script language="javascript">
   parent.SetLogID('$logID');
</script>
EOT;

   flush();

   $objTmpl         = new CTemplate();
   $objQuoteStatus  = new CQuoteStatus();
   $objQuoteError   = new CQuoteError();
   $objQuoteDetails = new CQuoteDetails();
   $objFile         = new CFile();
   $objFSClient     = new CFileStoreClient(FS_SYSTEM_TYPE,"out");
     
   $currentDirectory = getcwd();

   foreach($_SESSION['REQUEST_SITES'] as $siteID => $siteStatus)
   {

      // escape all the sites already gived us a quote
      if(! $resQuoteStatus = $objQuoteStatus->GetQuoteStatusByLogIdAndSiteId($_SESSION['_QZ_QUOTE_DETAILS_']['qz_log_id'], $siteID))
         continue;

     if($resQuoteStatus['status'] == 'WAITING')
        continue;


      // get files from  store server
      switch($resQuoteStatus['status'])
      {
         // get error file
         case 'NO QUOTE':
         case 'FAILURE':
            $objFSClient->GetFile($_SESSION['TEMPORARY FILE NAME']."_".$siteID.".err");
            break;

         case 'SUCCESS':
            $objFSClient->GetFile($_SESSION['TEMPORARY FILE NAME']."_".$siteID.".html");
            break;
      }

      $siteFileName = $_SESSION['SHOW_SITES'][$siteID];
      unset($_resSite);
         
      include "$currentDirectory/../sites/showsites/$siteFileName";
         
      if($_resSite['siteID'] != $siteID)
         continue;

      $continue = false;
      switch($resQuoteStatus['status'])
      {
         // WAITING
         case 'WAITING':
            break;

         // FAILURE
         case 'FAILURE':
         case 'NO QUOTE':
         case 'SKIPPED':

            $_SESSION['FOUND_SITE_ERROR'] = true;
           
            $resQuoteError = $objQuoteError->GetErrorDetailsByLogID($resQuoteStatus['id']);

            $errMessage = preg_replace("/\"/",'\"', $resQuoteError['quote_message']);
            $errMessage = str_replace("'", "\\'", $errMessage);

            print <<<EOT
               <script language="JavaScript">
                  parent.ShowFrame('$siteID', true, '$errMessage');
                  parent.LogSiteDetails('$siteID','showError');
               </script>

EOT;
            flush();

            unset($_SESSION['REQUEST_SITES'][$siteID]);
        break;

         // SUCCESS
         case 'SUCCESS':

         // get quote details!!!
         $resPremiumQuoteDetails = $objQuoteDetails->GetQuoteDetailsByQuoteStatusID($resQuoteStatus['id']);

//          $fd = fopen("/tmp/kkt1.txt","a+");
// 
//          $s =  var_export($resPremiumQuoteDetails,true);
//       
//          fwrite($fd,$s);

         // TO DO get quote details from out directory !!!

        print <<<EOT
               <script language="JavaScript">
                  //parent.ShowFrame($siteID, false, "");
                  //parent.document.getElementById('QuoteResults').style.display = 'block';
                  //parent.document.getElementById('QuoteResultsHeader').style.display = 'block'; 
               </script>

EOT;
            flush();

            prepareJsTxt($_resSite);

            $resultsFile = OpenResultFile($siteID);

            $i = 0;
            $fakeQuote = false;
            $index = -1;
            $personalizedAnnualPremiumFound = false;


            foreach($resPremiumQuoteDetails as  $elements)
            {

//             $a = var_export($elements,true);
//  
//             $a = rawurlencode($a);
// 
//             echo "<script language=JavaScript>alert('$a');</script>";

               $index++;
               $i++;

               $insurer = $elements['insurer'];
               if($_resSite['insurer'])
                  $insurer = $_resSite['insurer'];

               $premiumLevel = $elements['annual_premium'];
               if($_resSite['premiumLevel'])
                  $premiumLevel = $_resSite['premiumLevel'];

               //$logoLink  = $resultsFile[$index]['link'];

               $logo = $_resSite['imageSiteRemote'];

               //$LogoTable = "<img style=\"cursor:pointer;\" src=".$logo." width=\"100\" height=\"20\" onclick=\"javascript:window.open(\'$link\');\">";

               //echo "<script language=JavaScript>alert('$logo');</script>";

               if($elements['quote_ref'] == '-')
                  $elements['quote_ref'] = "not used";

               if($elements['password'] == '-')
                  $elements['password'] = "not used";

               if($elements['monthly_premium'] == 0)
                  $elements['monthly_premium'] = "Call Now";
               else
                  $elements['monthly_premium'] = "&pound;&nbsp;".$elements['monthly_premium'];

               $voluntaryExcess = $elements['voluntary_excess'];
               $elements['voluntary_excess'] = "&pound;&nbsp;".$elements['voluntary_excess'];

//                if($voluntaryExcess == -1)
//                   $elements['voluntary_excess'] = 'Unknown';

               $applyDetails = $_resSite['quoteDetails'];


               // set personalised premiums for multiple fake site
               $personalizedAnnualPremiumContinue = false;
               if(! empty($_resSite['personalizedAnnualPremiums']))
               {
                  $personalizedAnnualPremiumSiteId = $_resSite["siteID"];
                  if(! empty($_resSite['siteOrigID']))
                       $personalizedAnnualPremiumSiteId = $_resSite['siteOrigID'];

                  foreach($_resSite["personalizedAnnualPremiums"] as $resultKey => $resultValue)
                  {
                     $openResultFile = OpenResultFile($personalizedAnnualPremiumSiteId);

                     if(! preg_match("/".$resultValue."/i",$openResultFile[intval($i-1)][$resultKey]))
                     {
                        $personalizedAnnualPremiumContinue = true;
                        continue;
                     }
                     else
                      $personalizedAnnualPremiumFound = true;
                  }

                  if($i == count($resPremiumQuoteDetails))
                  {
                       unset($_SESSION['REQUEST_SITES'][$siteID]);

                       if(! $personalizedAnnualPremiumFound)
                       {
                           continue;
                       print <<<EOT
                       <script language="JavaScript">
                            ShowFrame($siteID, true, 'Unfortunately, the insurer is unable to quote for people with your circumstances.');
                       </script>
EOT;
                       }
                  }
               }

               if($personalizedAnnualPremiumContinue)
                  continue;



               // QUOTE DETAILS
               $resQuoteDetails = array();
               // in case of success load templates premiums, if not exists load defaults templates
               $templateDirectory = "../sites/templates/$siteID";

               $objTmpl->SetTemplateDirectory($templateDirectory);

               if(! $objTmpl->Load("quoteDetails.tmpl"))
               {
                  $templateDirectory = "../sites/templates/default";

                  $objTmpl->SetTemplateDirectory($templateDirectory);
                  $objTmpl->Load("quoteDetails.tmpl");
               }

               $resQuoteDetails['brokerFullName']   = $_resSite['brokerFullName'];
               $resQuoteDetails['annual_premium']   = $elements['annual_premium'];
               $resQuoteDetails['monthly_premium']  = $elements['monthly_premium'];

               $resQuoteContent["broker"]           = $_resSite['brokerFullName'];
               $resQuoteDetails["voluntary_excess"] = $elements['voluntary_excess'];
               $resQuoteContent["quote_reference"]  = $elements['quote_ref'];
               $resQuoteContent["quote_password"]   = $elements['password'];
               $resQuoteContent["telephone"]        = $_INSURERS_CALL[$siteID];

               $resQuoteDetails['BUY_OVER_THE_PHONE'] = $_resSite['buyOverThePhone'];
               $resQuoteDetails['BUY_ONLINE']         = $_resSite['buyOnline'];
               $resQuoteDetails['assumptionPage']     = $_resSite['assumptionPage'];

               // prepare extra params template
               foreach($_resSite['_EXTRA_PARAMS_']['quoteDetails'] as $_key => $_value)
               {
                  if(is_array($_value))
                  {
                     $resQuoteDetails[$_key] = $_value[$index];
                     continue;
                  }

                  if(! array_key_exists($resQuoteDetails, $_key))
                     $resQuoteDetails[$_key] = $_value;
               }

               $objTmpl->Prepare($resQuoteDetails);

               $quoteDetails = $objTmpl->GetContent();

               $quoteDetails = preg_replace("/\\\n/i","",$quoteDetails);
               // end QUOTE DETAILS

               // QUOTE APPLY
               $resQuoteApply = array();
               // in case of success load templates premiums, if not exists load defaults templates
               $templateDirectory = "../sites/templates/$siteID";

               $objTmpl->SetTemplateDirectory($templateDirectory);

               if(! $objTmpl->Load("quoteApply.tmpl"))
               {
                  $templateDirectory = "../sites/templates/default";

                  $objTmpl->SetTemplateDirectory($templateDirectory);
                  $objTmpl->Load("quoteApply.tmpl");
               }

               $resQuoteApply['brokerFullName']     = $_resSite['brokerFullName'];
               $resQuoteApply['annual_premium']     = $elements['annual_premium'];
               $resQuoteApply['monthly_premium']    = $elements['monthly_premium'];
               $resQuoteApply["voluntary_excess"]   = $elements['voluntary_excess'];

               $resQuoteContent["broker"]           = $_resSite['brokerFullName'];
               $resQuoteDetails["voluntary_excess"] = $elements['voluntary_excess'];
               $resQuoteContent["quote_reference"]  = $elements['quote_ref'];
               $resQuoteContent["quote_password"]   = $elements['password'];
               $resQuoteContent["telephone"]        = $_INSURERS_CALL[$siteID];

               $resQuoteApply['BUY_OVER_THE_PHONE'] = $_resSite['buyOverThePhone'];
               $resQuoteApply['BUY_ONLINE']         = $_resSite['buyOnline'];
               $resQuoteApply['assumptionPage']     = $_resSite['assumptionPage'];

               // prepare extra params template
               foreach($_resSite['_EXTRA_PARAMS_']['quoteApply'] as $_key => $_value)
               {
                  if(is_array($_value))
                  {
                     $resQuoteApply[$_key] = $_value[$index];
                     continue;
                  }

                  if(! array_key_exists($resQuoteApply, $_key))
                     $resQuoteApply[$_key] = $_value;
               }


               $objTmpl->Prepare($resQuoteApply);

               $quoteApply = $objTmpl->GetContent();

               $quoteApply = preg_replace("/\\\n/i","",$quoteApply);
               // end QUOTE APPLY

               // QUOTE BUY OVER THE PHONE
               $resQuoteBuyOverThePhone = array();

               $templateDirectory = "../sites/templates/$siteID";

               $objTmpl->SetTemplateDirectory($templateDirectory);

               if(! $objTmpl->Load("quoteBuyOverThePhone.tmpl"))
               {
                  $templateDirectory = "../sites/templates/default";

                  $objTmpl->SetTemplateDirectory($templateDirectory);
                  $objTmpl->Load("quoteBuyOverThePhone.tmpl");
               }

               $resQuoteBuyOverThePhone['brokerFullName']        = $_resSite['brokerFullName'];
               $resQuoteBuyOverThePhone['annual_premium']        = $elements['annual_premium'];
               $resQuoteBuyOverThePhone['monthly_premium']       = $elements['monthly_premium'];

               $resQuoteContent["broker"]           = $_resSite['brokerFullName'];
               $resQuoteDetails["voluntary_excess"] = $elements['voluntary_excess'];
               $resQuoteContent["quote_reference"]  = $elements['quote_ref'];
               $resQuoteContent["quote_password"]   = $elements['password'];
               $resQuoteContent["telephone"]        = $_INSURERS_CALL[$siteID];

               $resQuoteBuyOverThePhone['telephone']             = $_resSite['telephone'];
               $resQuoteBuyOverThePhone['quotingSite']           = $_resSite['quotingSite'];
               $resQuoteBuyOverThePhone['preQuotingSite']        = $_resSite['preQuotingSite'];
               $resQuoteBuyOverThePhone['buyOverThePhoneTop']    = $_resSite['buyOverThePhoneTop'];
               $resQuoteBuyOverThePhone['buyOverThePhoneBottom'] = $_resSite['buyOverThePhoneBottom'];
               $resQuoteBuyOverThePhone['assumptionPage']        = $_resSite['assumptionPage'];
               $resQuoteBuyOverThePhone['quote_reference']       = $elements['quote_ref'];
               $resQuoteBuyOverThePhone['quote_password']        = $elements['password'];
               $resQuoteBuyOverThePhone["voluntary_excess"]      = $elements['voluntary_excess'];
               $resQuoteBuyOverThePhone["Email address"]         = $_SESSION["_DRIVERS_"][0]["email_address"];

               // prepare extra params template
               foreach($_resSite['_EXTRA_PARAMS_']['buyOverThePhone'] as $_key => $_value)
               {
                  if(is_array($_value))
                  {
                     $resQuoteBuyOverThePhone[$_key] = $_value[$index];
                     continue;
                  }

                  if(! array_key_exists($resQuoteBuyOverThePhone, $_key))
                     $resQuoteBuyOverThePhone[$_key] = $_value;
               }

               $objTmpl->Prepare($resQuoteBuyOverThePhone);

               $quoteBuyOverThePhone = $objTmpl->GetContent();

               $quoteBuyOverThePhone = preg_replace("/\\\n/i","",$quoteBuyOverThePhone);
               // end QUOTE BUY OVER THE PHONE


               // QUOTE BUY ONLINE
               $resQuoteBuyOnline = array();

               $templateDirectory = "../sites/templates/$siteID";

               $objTmpl->SetTemplateDirectory($templateDirectory);

               if(! $objTmpl->Load("quoteBuyOnline.tmpl"))
               {
                  $templateDirectory = "../sites/templates/default";

                  $objTmpl->SetTemplateDirectory($templateDirectory);
                  $objTmpl->Load("quoteBuyOnline.tmpl");
               }

               $resQuoteBuyOnline['brokerFullName']        = $_resSite['brokerFullName'];
               $resQuoteBuyOnline['annual_premium']        = $elements['annual_premium'];
               $resQuoteBuyOnline['monthly_premium']       = $elements['monthly_premium'];

               $resQuoteContent["broker"]           = $_resSite['brokerFullName'];
               $resQuoteDetails["voluntary_excess"] = $elements['voluntary_excess'];
               $resQuoteContent["quote_reference"]  = $elements['quote_ref'];
               $resQuoteContent["quote_password"]   = $elements['password'];
               $resQuoteContent["telephone"]        = $_INSURERS_CALL[$siteID];

               $resQuoteBuyOnline['buyOnlineTop']          = $_resSite['buyOnlineTop'];
               $resQuoteBuyOnline['buyOnlineBottom']       = $_resSite['buyOnlineBottom'];
               $resQuoteBuyOnline['proceedBuyOnlineLink']  = $_resSite['proceedBuyOnlineLink'];
               $resQuoteBuyOnline["voluntary_excess"]      = $elements['voluntary_excess'];

               $resQuoteBuyOnline['buyOnlineTopText']     = $_resSite['buyOnlineTopText'];
               $resQuoteBuyOnline['assumptionPage']       = $_resSite['assumptionPage'];


               foreach($_resSite['buyOnlineDetails'] as $keyName => $valueName)
               {
                  if(empty($valueName))
                     continue;

                  if(preg_match("/quote_ref\.value/is", $valueName))
                     $valueName = $elements['quote_ref'];

                   if(preg_match("/link\.value/is", $valueName))
                     $valueName = $elements['link'];

                  $resQuoteBuyOnline[$keyName]            = $keyName." :";
                  $resQuoteBuyOnline[$keyName.".value"]   = $valueName;
               }

               // prepare proceed buy online link
               if(preg_match("/quote_ref\.value/is", $resQuoteBuyOnline['proceedBuyOnlineLink']))
                  $resQuoteBuyOnline['proceedBuyOnlineLink'] = preg_replace("/quote_ref\.value/", $elements['quote_ref'], $resQuoteBuyOnline['proceedBuyOnlineLink']);

               // prepare proceed buy online link
               if(preg_match("/annual_premium\.value/is", $resQuoteBuyOnline['proceedBuyOnlineLink']))
                  $resQuoteBuyOnline['proceedBuyOnlineLink'] = preg_replace("/annual_premium\.value/", $elements['annual_premium'], $resQuoteBuyOnline['proceedBuyOnlineLink']);

               $objTmpl->SetRunMode(1);
               $objTmpl->Prepare($resQuoteBuyOnline);
               $objTmpl->SetRunMode(2);


               // prepare extra params template
               foreach($_resSite['_EXTRA_PARAMS_']['buyOnline'] as $_key => $_value)
               {

                  if($_value == '_premiums_file_')
                  {
                     $resQuoteBuyOnline[$_key.".value"] = $resultsFile[$index][$_key];
                     continue;
                  }

                  if(is_array($_value))
                  {
                     $resQuoteBuyOnline[$_key] = $_value[$index];
                     continue;
                  }

                  if(! array_key_exists($resQuoteBuyOnline, $_key))
                     $resQuoteBuyOnline[$_key] = $_value;
               }

               $objTmpl->Prepare($resQuoteBuyOnline);

               $quoteBuyOnline = $objTmpl->GetContent();

               $quoteBuyOnline = preg_replace("/\\\n/i","",$quoteBuyOnline);


               // if we don't have quoteApply and quoteDetails
               if(empty($quoteApply))
                  $quoteApply = $quoteBuyOnline;

               if(empty($quoteDetails))
                  $quoteDetails = $quoteBuyOnline;

               // end QUOTE BUY ONLINE
         
         $coverType                          = $resultsFile[$index]['cover type'];
         
         $DetailsQuote['roadsideAssistance'] = $resultsFile[$index]['roadside assistance'];
         $DetailsQuote['recoveryLocal']      = $resultsFile[$index]['recovery local'];
         $DetailsQuote['recoveryUk']         = $resultsFile[$index]['recovery uk'];
         $DetailsQuote['homeStart']          = $resultsFile[$index]['home start'];
         $DetailsQuote['carHire']            = $resultsFile[$index]['car hire'];
         $DetailsQuote['altTravel']          = $resultsFile[$index]['alt travel'];
         $DetailsQuote['hotel']              = $resultsFile[$index]['hotel'];
         $DetailsQuote['reliefDriver']       = $resultsFile[$index]['relief driver'];
         $DetailsQuote['medical']            = $resultsFile[$index]['medical'];
         $DetailsQuote['european']           = $resultsFile[$index]['european'];

         $policy                             = $resultsFile[$index]['policy'];

         $link                               = $resultsFile[$index]['link'];
         $companyId                          = $resultsFile[$index]['companyId'];
         $specialOffer                       = $resultsFile[$index]['special_offer'];    

         $specialOffer = preg_replace('/£/', '&pound;', $specialOffer);


         //$logoLink = 'sites/clickIn.php?urlName=Buy%20Online%20'.$companyId.'%20Breakdown&urlAddr='.$link;
         $logoLink = 'sites/clickIn.php?urlName=Buy%20Online%20'.$companyId.'%20Breakdown&siteID='.$_resSite['siteID'].'&urlAddr='.$link;

         $LogoTable = "<img style=\"cursor:pointer;\" src=".$logo." width=\"100\" height=\"20\" onclick=\"javascript:window.open(\'$logoLink\');\">";

         $nextLink = "<img style=\"cursor:pointer;\" src=\"images/next_blue_01.gif\" onMouseOver=\"javascript:this.src=\'images/next_blue_02.gif\'\"  onclick=\"javascript:window.open(\'$logoLink\');\">";

         //$nextLink =  "<img style=\"cursor:pointer;\" src=\"images/next_blue_01.gif\" onMouseOver=\"javascript:this.src=\'images/next_blue_02.gif\'\" onMouseOut=\"javascript:this.src=\'images/next_blue_01.gif\'\" title=\'\' onClick=\"javascript:window.open(\'$logoLink\');\" />'; 


         //echo "<script language=JavaScript>alert('$companyId')</script>";
         

         foreach($DetailsQuote as $DetailsQuoteKey => $DetailsQuoteValue)
         {
 
             if($DetailsQuoteValue == 'YES')
                $DetailsQuoteParam[$DetailsQuoteKey] = "<img src=\"images/tick.gif\">";
             elseif($DetailsQuoteValue == 'EXTRA')
                $DetailsQuoteParam[$DetailsQuoteKey] = "<img src=\"images/plus.gif\">";
             else
                $DetailsQuoteParam[$DetailsQuoteKey] = "<img src=\"images/not_included.gif\">";
          }
         
         $roadsideAssistance = $DetailsQuoteParam['roadsideAssistance'];
         $recoveryLocal      = $DetailsQuoteParam['recoveryLocal'];
         $recoveryUk         = $DetailsQuoteParam['recoveryUk'];
         $homeStart          = $DetailsQuoteParam['homeStart'];
         $carHire            = $DetailsQuoteParam['carHire'];
         $altTravel          = $DetailsQuoteParam['altTravel'];
         $hotel              = $DetailsQuoteParam['hotel'];
         $reliefDriver       = $DetailsQuoteParam['reliefDriver'];
         $medical            = $DetailsQuoteParam['medical'];
         $european           = $DetailsQuoteParam['european'];
         


         // echo "<script language=JavaScript>alert('$coverType')</script>";
         //$LogoTable =  "<img src=".$logo." width=\"100\" height=\"20\">";
         
         $LogoTable = $LogoTable."<br />".$policy;

         print <<<EOT

         <script language="JavaScript">
            var quoteID = parent.AddQuoteResult("$premiumLevel",'$LogoTable',"<center><span class=\"t5new\">$coverType</span></center>","<center>&pound;&nbsp;$premiumLevel</center>", '<center>$roadsideAssistance</center>','<center>$recoveryLocal</center>','<center>$recoveryUk</center>','<center>$homeStart</center>','<center>$carHire</center>','<center>$altTravel</center>','<center>$hotel</center>','<center>$reliefDriver</center>','<center>$medical</center>','<center>$european</center>','$specialOffer','$nextLink');

            parent.AddSiteID(quoteID, '$siteID');
            parent.AddQuoteApply(quoteID,'$quoteApply');
            parent.AddQuoteDetails(quoteID,'$quoteDetails');
            parent.AddQuoteBuyOverPhone(quoteID,'$quoteBuyOverThePhone');
            parent.AddQuoteBuyOnline(quoteID,'$quoteBuyOnline');

         </script>

EOT;

               flush();

            }

            print <<<EOT
               <script language="JavaScript">
                  parent.LogSiteDetails('$siteID','showPremium');
               </script>

EOT;
            flush();


            unset($_SESSION['REQUEST_SITES'][$siteID]);

            break;

         default:
          break;
      }

   }// end foreach($_SESSION['REQUEST_SITES'] as $siteID => $siteStatus)


 print <<<EOT
<script language="JavaScript">
   //parent.InsertPremiumLastRowTable();
</script>
EOT;


flush();


   if(! count($_SESSION['REQUEST_SITES']))
   {

      if(! $_SESSION['FOUND_SITE_ERROR'])
         print <<<EOT
<script language="JavaScript">
            //parent.document.getElementById('QuoteSearchingHeader').style.display = 'none';
            //parent.document.getElementById('QuoteSearching').style.display       = 'none';
</script>
EOT;

      print <<<EOT
<script language="JavaScript">
            parent.Stop();
</script>
EOT;
      flush();
   }


function preCompile($fileName)
{
   return true;

   $errors = "Fatal error, Parse Error"; // check if we match  some words in compiling output
   $emails = "adrian.axente@seopa.com"; //emails where to be send the email errors]

   $outputCompile = shell_exec("/usr/bin/php -q $fileName 2>&1");
/*
   $outputCompile = '';
   $ph = popen("/usr/bin/php -l $fileName 2>&1");

   while(! feof($ph))
      $outputCompile .= fread($ph, 128);

   pclose($ph);
*/

   $allErrors = explode(",", $errors);
   $allEmails = explode(",", $emails);

   foreach($allErrors as $index => $error)
   {
     $error = trim($error);

     if(preg_match("/$error/im", $outputCompile))
     {
        $mailBody = "CAR INSURANCE FILE ERROR: >> $fileName <<\n";
        $mailBody .= $outputCompile;

        foreach($allEmails as $index => $toEmail)
        {
           $toEmail = trim($toEmail);
           mail($toEmail, "CAR INSURANCE QUOTEZONE INCLUDE FILE ERROR", $mailBody,"From: errors@quotezone.co.uk");
        }

        return false;
     }
   }

   return true;
}


function prepareJsTxt(&$arrayContent)
{
   if(! is_array($arrayContent))
      return;

   foreach($arrayContent as $key => $value)
   {
      if(is_array($value))
      {

         prepareJsTxt($value);

         $arrayContent[$key] = $value;
         continue;
      }

      $arrayContent[$key] = preg_replace("/'/","\\\'", $value);
   }
}// end function prepareJsTxt(&$arrayContent)

?>

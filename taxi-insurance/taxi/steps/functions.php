<?php

error_reporting(1);

function IsLeapYear($year = 0)
{
   if(! ereg("[0-9]+", $year))
      return 0;

   if($year %4 == 0)
   {
      if($year % 100 == 0)
      {
         if($year % 400 == 0)
            return 1;
         else
            return 0;
      }

      return 1;
   }

   return 0;
}

function GetDaysInMonth($month = 0, $year = 0)
{
   $month = intval($month);

   if(! ereg("[0-9]+", $month))
      return -1;

   if(! ereg("[0-9]+", $year))
      return -2;

   if($month > 12 || $month < 1)
      return -3;

   $daysInMonth = array(0,31,28,31,30,31,30,31,31,30,31,30,31);

   $days = $daysInMonth[$month];

   if($month == 2)
      $days += IsLeapYear($year);

   return $days;
}

function ValidateDate($day,$month,$year)
{
   $daysInMonth = GetDaysInMonth($month,$year);

   if($day > $daysInMonth)
      return false;
   else
      return true;
}

function _mktime ( $hour="0", $minute="0", $second="0", $month="0", $day="0", $year="0" )
{
   if(empty($day))
      $day = date('d');

   if(empty($month))
      $month = date('m');

   if(empty($year))
      $year = date('Y');

   if ( $year > 1969 )
   {
      return ( gmmktime ( $hour, $minute, $second, $month, $day, $year ) );
   }

   $t  = 0;
   $ds = 86400;
   $hs = 3600;
   $dy = 365;
   $ms = 60;

   $months    = array ( 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 );
   $leap_year = $year % 4 == 0 && ( $year % 100 > 0 || $year % 400 == 0 ) ? true : false;

   if ( $year < 1969 )
   {
      $y = 1969 - $year;
      $t -= ( $y * $dy ) * $ds;
      $x = ceil ( $y / 4 );

      if ( $leap_year && $month > 2 )
      {
         $x -= 1;
      }

      $t -= $x * $ds;
   }

   if ( $month != 12 )
   {
      $tm = $months;
      $tm = array_slice ( $tm, $month );
      $t -= array_sum ( $tm ) * $ds;
      unset ( $tm );
   }

   $nh = ( ( $month == 2 && $leap_year ? 29 : $months[$month-1] ) - $day );
   $t -= $nh != 0 ? $nh * $ds : 0;
   $nh = 23 - $hour;
   $t -= $nh != 0 ? $nh * $hs : 0;
   $nh = 59 - $minute;
   $t -= $nh != 0 ? $nh * $ms : 0;
   $nh = 59 - $second;
   $t -= $nh != 0 ? $nh + 1 : 0;

   return ( $t );
}

function GetWebPage($url="")
{
   if(! $url)
      return;

   $fd = fopen($url,"r");

   while(! feof($fd))
      $pageData .= fread($fd, 1024);
   fclose($fd);

  return $pageData;
}

function GetAllElements($element,$limitNameChars=0)
{
   if(! $limitNameChars)
      return $element;

   $newArray = array();
   foreach($element as $code => $name)
   {
      if(strlen($name) > $limitNameChars)
         $newArray[$code] = substr($name, 0, $limitNameChars)."...";
      else
         $newArray[$code] = $name;
   }

   return $newArray;
}

function SetEditBox($elementName='', &$prepArray)
{
   $prepArray[$elementName.".value"]  = $prepArray[$elementName];
}

function SetRadioButton($elementName='', &$prepArray)
{
   $prepArray[$elementName."_".$prepArray[$elementName].".checked"]  = "checked";
}

function SetCheckBox($elementName='', &$prepArray)
{
   $prepArray[$elementName."_".$prepArray[$elementName].".checked"]  = "checked";
}

function SetExtraError($elementName, $errorKey, &$prepArray, $extraErrorKey)
{
   if(! empty($prepArray["$elementName.error"]))
      return;

   $prepArray["$elementName.extraError"] = HTML_BEGIN_ERROR.GetErrorString($errorKey).$extraErrorKey.HTML_END_ERROR;
   $prepArray["$elementName.error_b"]    = HTML_BORDER_ERROR;
   $prepArray["$elementName.error_bg"]   = HTML_BACKGROUND_ERROR;
}

function OpenResultFile($siteID)
{
   include_once "FileStoreClient.php";
   $objFileFs = new  CFileStoreClient('loans','out');

   $outName = $_SESSION['TEMPORARY FILE NAME']."_".$siteID.".html";	

   if(! $data = $objFileFs->GetCacheFile($outName))
      return false;

   eval("\$resultFile = $data;");
   
   return $resultFile;
}

function GetAge($birthDate, $curDate)
{
    list($birthYear,$birthMonth,$birthDay) = explode("-", $birthDate);
    list($curYear,$curMonth,$curDay)       = explode("-", $curDate);

    $yearDiff  = $curYear  - $birthYear;
    $monthDiff = $curMonth - $birthMonth;
    $dayDiff   = $curDay   - $birthDay;

    if(($monthDiff < 0) || ($monthDiff == 0 && $dayDiff < 0))
      $yearDiff--;

    //echo "<--! $birthDate $curDate [$birthYear-$birthMonth-$birthDay] [$curYear-$curMonth-$curDay] -->\n";
    return $yearDiff;
}

?>

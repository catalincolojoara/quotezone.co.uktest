<?php

/*****************************************************************************/
/*                                                                           */
/*  BREAKDOWN web interface                                                       */
/*                                                                           */
/*  (C) 2006 ACRUX SOFTWARE                                                  */
/*                                                                           */
/*****************************************************************************/


   include_once "modules/globals.inc";
   include_once "YourCoverElements.php";
   include_once "errors.inc";
   include_once "functions.php";
   include_once "js/YourQuotes.js";

//////////////////////////////////////////////////////////////////////////////PB
//
// [SCRIPT NAME]:   YourDetails.php
//
// [DESCRIPTION]:  script web interface for bike vehicle section
//
// [FUNCTIONS]:   
//                Show()
//                Process()
//                TemplateElements(&$resParameters)
//                ValidateExtraErrors(&$resParameters)
//
// [CREATED BY]:  Furtuna Alexandru (alexandru.furtuna@seopa.com) 2009-10-14
//
// [MODIFIED]:   - [programmer (email) date]
//                 [short description]
//
//////////////////////////////////////////////////////////////////////////////PE
   $resParameters = array();

   // set post formular variables
   $postParameters = array(
   );
   
   foreach($postParameters as $key => $value)
      $resParameters["$value"] = trim($_POST["$value"]);

   // set image level
   $imageLevel = 1;


   //print_r($postParameters); 

function Show()
{

   global $imageLevel;

   global $objTmpl;
   global $objWs;
   global $objMenu;

   global $resParameters;
   global $postParameters;

   $resParameters['select_view'] = "none";

   if(! $objTmpl->Load("YourQuotes.tmpl"))
         print $objTmpl->GetError()."\n";

   if($resSessionParameters = $objMenu->GetSessionContent('_YourCover_'))
   {
      foreach($postParameters as $key => $value)
         $resParameters["$value"] = $resSessionParameters[$value];

      $resParameters['select_view'] = "block";
    }

    if(! $objMenu->ExistSessionEntry('_YourQuotes_'))
    {

       $objTmpl->SetRunMode(1);
    
       $resYourDetailsPrepare = array();
 
       $objTmpl->Prepare($resYourDetailsPrepare);
    
       $objTmpl->SetRunMode(2);
    } // end set default radiobox elements

   YourCoverElements($resParameters);

   $objTmpl->Prepare($resParameters);

   //$objWs->SetGoogleTracker("urchinTracker('/funnel_B1/BikeQuote-StepBike1.htm');");

   $objWs->SetImageHeaderLevel($imageLevel);
   $objWs->SetWorkArea($objTmpl->GetContent());
   $objWs->Run();
}

function Process()
{
   global $imageLevel;

   global $objTmpl;
   global $objWs;
   global $objMenu;

   global $resParameters;
   global $postParameters;
 

   if(! $objTmpl->Load("YourQuotes.tmpl"))
       print $objTmpl->GetError();

   $resParameters['select_view'] = 'none';

   YourCoverElements($resParameters);

   print_r($resParameters);

   $validator = true;

   if(! $objTmpl->Validate($resParameters))
      $validator = false;
   
   if(! ValidateExtraErrors($resParameters))
      $validator = false;
    
   if(! $validator)
   {
      $objTmpl->Prepare($resParameters);
      $objWs->SetImageHeaderLevel($imageLevel);
      $objWs->SetWorkArea($objTmpl->GetContent());
      $objWs->Run();
      
      exit(0);
   }

   // save data to the session
   foreach($postParameters as $k => $v)
      $objMenu->SetSessionContent($v, $resParameters[$v], '_YourCover_');

   $objMenu->SetForwardStep();

}

function YourCoverElements(&$resParameters)
{

   global $objTmpl;
   global $objMenu;
   global $postParameters;
   global $_YourCover;

   //print("!!!!!!!!!!!!!!!!!!!!!!!!");
   //print_r($_YourCover);die;

   // prepare select box elements
   foreach($_YourCover as $key => $resElement)
   {
      $selectedKey = $resParameters[$key];

      $elements = GetAllElements($resElement);

      $resParameters["$key.value"] = $objTmpl->MakeSelBoxOptions($elements,$selectedKey,false);
      
   }
}

function ValidateExtraErrors(&$resParameters)
{

   $error = false;


   if($error)
      return false;

   return true;
}



function SetSelectBoxElement(&$resParameters,$name='', $value='')
{
   global $objTmpl;

   $resElement = $_YourCover[$name];
   $elements = GetAllElements($resElement);
   
   $resParameters[$name.".value"] = $objTmpl->MakeSelBoxOptions($elements,$value,false);
}

function SetRadioBoxElement(&$resParameters,$name='',$value)
{
   $resParameters[$name."_".$value.".checked"]  = "checked";
}

?>

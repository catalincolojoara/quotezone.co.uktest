<div class="rightMenu">
    <div class="rightMenuBg">
        <div class="rightMenuTitle">
            <h4><a href="http://www.quotezone.co.uk/insurance.htm">Insurance</a></h4>
            <p>
                <a href="http://www.quotezone.co.uk/car-insurance.htm">Car</a>, <a href="http://www.quotezone.co.uk/van-insurance.htm">Van</a>, <a href="http://www.quotezone.co.uk/bike-insurance.htm">Bike</a>, <a href="http://www.quotezone.co.uk/home-insurance.htm">Home</a>,<a href="http://www.quotezone.co.uk/travel-insurance.htm">Travel</a>, <a href="http://www.quotezone.co.uk/pet-insurance.htm">Pet</a>...
            </p>
        </div>
        <div class="rightMenuInsurance" onclick="location.href='insurance.htm';" style="cursor:pointer;">
        </div>
    </div>
    <div class="rightMenuBg">
        <div class="rightMenuTitle">
            <h4><a href="http://www.quotezone.co.uk/money.htm">Money</a></h4>
            <p>
                <a href="http://www.quotezone.co.uk/credit-cards.htm">Credit Cards</a>, <a href="https://loans.quotezone.co.uk/loans/">Loans</a>, <a href="http://www.quotezone.co.uk/mortgages.htm">Mortgages</a>, <a href="http://www.quotezone.co.uk/money/savings-accounts.htm">Savings</a>
                ...
            </p>
        </div>
        <div class="rightMenuMoney" onclick="location.href='money.htm';" style="cursor:pointer;">
        </div>
    </div>
    <div class="rightMenuBg">
        <div class="rightMenuTitle">
            <h4><a href="http://www.quotezone.co.uk/motoring.htm">Motoring</a></h4>
            <p>
                <a href="http://www.quotezone.co.uk/warranties.htm">Warranties</a>, <a href="http://www.quotezone.co.uk/used-cars.htm">Used Cars</a>, <a href="http://www.quotezone.co.uk/breakdown-cover.htm">Breakdown</a>
                ...
            </p>
        </div>
        <div class="rightMenuMotoring" onclick="location.href='motoring.htm';" style="cursor:pointer;">
        </div>
    </div>
    <div class="rightMenuBg">
        <div class="rightMenuTitle">
            <h4><a href="http://www.quotezone.co.uk/utilities.htm">Utilities</a></h4>
            <p>
                <a href="http://www.quotezone.co.uk/utilities/gas.htm">Gas</a>, <a href="http://www.quotezone.co.uk/utilities/electricity.htm">Electricity</a>, <a href="http://www.quotezone.co.uk/utilities/broadband.htm">Broadband</a>, <a href="http://www.quotezone.co.uk/utilities/mobile-phones.htm">Mobiles</a>
                ...
            </p>
        </div>
        <div class="rightMenuUtilities" onclick="location.href='utilities.htm';" style="cursor:pointer;">
        </div>
    </div>
    <div class="rightMenuBg">
        <div class="rightMenuTitle">
            <h4><a href="http://www.quotezone.co.uk/travel.htm">Travel</a></h4>
            <p>
                <a href="http://www.quotezone.co.uk/travel-insurance.htm">Insurance</a>, <a href="http://www.quotezone.co.uk/travel/holidays.htm">Holidays</a>, <a href="http://www.quotezone.co.uk/travel/hotels.htm">Hotels</a>, <a href="http://www.quotezone.co.uk/travel/car-hire.htm">Car Hire</a>
                ...
            </p>
        </div>
        <div class="rightMenuTravel" onclick="location.href='travel.htm';" style="cursor:pointer;">
        </div>
    </div>
    <div class="rightMenuBg">
        <div class="rightMenuTitle">
            <h4><a href="http://www.quotezone.co.uk/shopping.htm">Shopping</a></h4>
            <p>
                <a href="http://www.quotezone.co.uk/shopping/electronics.htm">Electronics</a>, <a href="http://www.quotezone.co.uk/shopping/home-garden.htm">Home</a>, <a href="http://www.quotezone.co.uk/shopping/entertainment.htm">Entertainment</a>
                ...
            </p>
        </div>
        <div class="rightMenuShopping" onclick="location.href='shopping.htm';" style="cursor:pointer;">
        </div>
    </div>
    <div class="rightMenuBg">
        <div class="rightMenuTitle">
            <h4><a href="http://www.quotezone.co.uk/business.htm">Business</a></h4>
            <p>
                <a href="http://www.quotezone.co.uk/money/business-finance.htm">Finance</a>, <a href="http://www.quotezone.co.uk/business/tool-cover.htm">Tool Cover</a>, <a href="http://www.quotezone.co.uk/business-insurance.htm">Commercial Insurance</a>
            </p>
        </div>
        <div class="rightMenuBusiness" onclick="location.href='business.htm';" style="cursor:pointer;">
        </div>
    </div>
</div>

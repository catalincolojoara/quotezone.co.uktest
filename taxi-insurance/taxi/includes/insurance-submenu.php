<?php

  //calls the submenu for the insurance section pages
  //kev 22/10/09

  function ShowSubMenu($menuItem="")
   {
     //testing
     //echo $menuItem; 

     //store all insurance section submenu items
     $menuItemArray = array( array( Name => "Car", 
                                    Url => "http://www.quotezone.co.uk/car-insurance.htm",
                                    Sep => "|",
                             ),
                             array( Name => "Van", 
                                    Url => "http://www.quotezone.co.uk/van-insurance.htm", 
					 Sep => "|",
                              ),
                             array( Name => "Bike", 
                                    Url => "http://www.quotezone.co.uk/bike-insurance.htm",
					 Sep => "|",
                             ),
				 array( Name => "Home", 
                                    Url => "http://www.quotezone.co.uk/home-insurance.htm",
                                    Sep => "|",
                             ),
				 array( Name => "Travel", 
                                    Url => "http://www.quotezone.co.uk/travel-insurance.htm",
                                    Sep => "|",                    
                             ),
				 array( Name => "Breakdown", 
                                    Url => "http://www.quotezone.co.uk/breakdown-cover.htm",
					 Sep => "|",                    
                             ),
				 array( Name => "Life", 
                                    Url => "http://www.quotezone.co.uk/life-insurance.htm",
					 Sep => "|",                    
                             ),
				 array( Name => "Business", 
                                    Url => "http://www.quotezone.co.uk/business-insurance.htm",
					 Sep => "|",                    
                             ),
				 array( Name => "Pet", 
                                    Url => "http://www.quotezone.co.uk/pet-insurance.htm",
					 Sep => "|",                    
                             ),
				 array( Name => "Caravan", 
                                    Url => "http://www.quotezone.co.uk/caravan-insurance.htm",
					 Sep => "|",                    
                             ),
				 array( Name => "Motorhome", 
                                    Url => "http://www.quotezone.co.uk/motorhome-insurance.htm",
					 Sep => "|",                    
                             ),
				 array( Name => "Campervan", 
                                    Url => "http://www.quotezone.co.uk/campervan-insurance.htm",
					 Sep => "|",                    
                             ),
				 array( Name => "Boat", 
                                    Url => "http://www.quotezone.co.uk/boat-insurance.htm",
					 Sep => "|",                    
                             ),
				 array( Name => "Health", 
                                    Url => "http://www.quotezone.co.uk/health-insurance.htm",
					 Sep => "|",                    
                             ),
				 array( Name => "Landlords", 
                                    Url => "http://www.quotezone.co.uk/landlords.htm",
					 Sep => "|",                    
                             ),
				 array( Name => "Income Protection", 
                                    Url => "http://www.quotezone.co.uk/income-protection.htm",
					 Sep => "|",                    
                             ),
				 array( Name => "Mortgage Protection", 
                                    Url => "http://www.quotezone.co.uk/mortgage-protection.htm",
					 Sep => "|",                    
                             ),
				 array( Name => "Heating", 
                                    Url => "http://www.quotezone.co.uk/heating.htm",
					 Sep => "|",                    
                             ),
				 array( Name => "Dental", 
                                    Url => "http://www.quotezone.co.uk/dental.htm",
					 Sep => "|",                    
                             ),
                 array( Name => "Taxi", 
                                    Url => "http://www.quotezone.co.uk/taxi-insurance.htm",
					 Sep => "|",                    
                             ),
                 array( Name => "Minibus", 
                                    Url => "http://www.quotezone.co.uk/minibus-insurance.htm",
					 Sep => "|",                    
                             ),
                 array( Name => "Limousine", 
                                    Url => "http://www.quotezone.co.uk/limo-insurance.htm",
					 Sep => "|",                    
                             ),
                 array( Name => "Coach", 
                                    Url => "http://www.quotezone.co.uk/coach-insurance.htm",
					 Sep => "|",                    
                             ),
                 array( Name => "Bus", 
                                    Url => "http://www.quotezone.co.uk/bus-insurance.htm",
					 Sep => "",                    
                             )                             
                            );


     //keep record of the number of menu items	
     $count = count($menuItemArray);

     //contruct the submenu
     echo "<ul class='menu_submenu'>";

	for ($row = 0; $row < $count; $row++)
	{
		//locate active menu item	
		if ($menuItem == $menuItemArray[$row]["Name"]) 
		    $activeMenuItem = "<li class='active'>"; 
 		else
		    $activeMenuItem = "<li>";
	
		echo $activeMenuItem."<a href='".$menuItemArray[$row]["Url"]. "'>".$menuItemArray[$row]["Name"]."</a></li><li class='separator'>".$menuItemArray[$row]["Sep"]."</li>";
	}

     echo "</ul>";
 
   }       
?>


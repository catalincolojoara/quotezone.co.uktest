<?php

  function ShowOvertureAds($keyword="")
   {
      //validate keyword
      if(empty($keyword))
         return;

      // Specify URL to XML and XSL documents
        $xmlDoc = 'http://xml.uk.overture.com/d/search/p/standard/eu/xml/rlb/?';
        $data = rawurlencode($keyword);
      
        // Enable Globals may be turned off
        $QUERY_STRING = $_SERVER['QUERY_STRING'] ;
        $REQUEST_METHOD = $_SERVER['REQUEST_METHOD'] ;
        $USER_AGENT     = $_SERVER['HTTP_USER_AGENT'];
        $USER_IP_ADDR   = $_SERVER['REMOTE_ADDR'];
        $X_FORWARD_IP   = $_SERVER['HTTP_X_FORWARDED_FOR'];
        
        $affilDataValue = "ip=$USER_IP_ADDR";

        if($X_FORWARD_IP)
          $affilDataValue .= "&xfip=$X_FORWARD_IP";
      
        $affilDataValue .= "&ua=".rawurlencode($USER_AGENT);
              
        //$xmlDoc .= "&mkt=uk&maxCount=15&Partner=seopa_xml_uk_searchbox_quotezone&adultFilter=clean&Keywords=$data";
        $xmlDoc .= "catMaps=seopa_uk_multi&mkt=uk&maxCount=7&Partner=seopa_xml_uk_searchbox_quotezone&adultFilter=clean&Keywords=$data&keywordCharEnc=utf-8&outputCharEnc=utf-8";
        $xmlDoc .= "&affilData=".rawurlencode($affilDataValue);
        $xmlDoc .= "&serveUrl=" . rawurlencode(($_SERVER['HTTPS']== 'on'?'https://':'http://').$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']);
      
        $xmlConn = fopen ($xmlDoc, "r");
      
        if (!$xmlConn) {
          return;
//          echo "<b>Connection to XML feed could not be established</b>"; exit;
        } else {
          do {
            $data = fread($xmlConn,2048);
            if( strlen( $data ) == 0 ) {
              break;
            }
            $xmlFeed .= $data;
          } while( true );
        }
        fclose($xmlConn);
        
        // Parse XML results into two arrays
        $parser = xml_parser_create();
        xml_parse_into_struct($parser,$xmlFeed,$values,$tags);
        xml_parser_free($parser);
      
        // Read listings
        $listings = array();
        $listingIndex = 0;
      
        if( $tags['LISTING'] ) {
          foreach ($tags['LISTING'] as $key=>$val) {
            if( 'open' == $values[$val]['type'] ) {
              $listings[$listingIndex] = $values[$val]['attributes'];
              $listingIndex++;
            }
          }
        }
      
        // If no listings, then we are done
        $totalListings = sizeof( $listings );
        if( 0 == $totalListings ) {
return;
//          echo '<!-- end Overture container-->';
          exit;
        }
      
        // Get one clickUrl per listing
        $listingIndex = 0;
        $totalUrls = sizeof( $tags['CLICKURL'] );
        $urlsPerListing = $totalUrls / $totalListings;
      
        for( $urlIndex = 0; $urlIndex < $totalUrls; $urlIndex += $urlsPerListing ) {
          $url = $values[$tags['CLICKURL'][$urlIndex]]['value'];
          $listings[$listingIndex]['CLICKURL'] = $url;
          $listingIndex++;
        }
      
        // Read NextArgs/PrevArgs for navigation
        $nextargs = $values[$tags['NEXTARGS'][0]]['value'];
        $prevargs = $values[$tags['PREVARGS'][0]]['value'];
      
         // Display the results   
        foreach ($listings as $key=>$val) {
         echo "<div class='ovContBox' onclick='window.status=\"".$val[TITLE]."\";return true;' onmouseover='window.status=\"".$val[TITLE]."\";return true;' onmouseout='window.status=\"\";return true;'>";
         
         print "     <div class='ovContBoxTop'></div>";
         print "          <div class='ovContBoxMiddle'>";
         print "           <div class='ovContBoxMiddleBox'>";
         print "                  <h4><a rel='nofollow' target='_blank' title='".$val[TITLE]."' href='".$val[CLICKURL]."' >".$val['TITLE']."</a></h4><br/>";
         print "                      <p style='float:left;font-family: Arial; font-size: 11px; width:100%;'>".$val['DESCRIPTION']."</p><br/>";
         print "                      <p class='siteDomain' align='left'><a rel='nofollow' href='".$val[CLICKURL]."' target='_blank'>".$val['SITEHOST']."</a></p><a style='float:right;cursor:hand; padding: 0 15px 0 0;' target='_blank' rel='nofollow' title='".$val[TITLE]."' href='".$val[CLICKURL]."'><img src='https://www.quotezone.co.uk/images/vtsbutt_img.gif' width='124' height='18' border='0' alt='".$val[TITLE]."' /></a>";                   
         print "              </div>";
         print "          </div>";
         print "       <div class='ovContBoxBottom'></div>";
         print "</div>";
            
                  
         }
         
        
         //print '<br/><br/><br/>';

     }       
?>


var helpText = Array();

helpText[0]  = "<b>Taxi Use</b><br>Private hire covers pre booked jobs whilst public hire grants on street pickup.<br>";
helpText[1]  = "<b>Taxi Type</b><br>Insurance prices will vary based on the vehicle type. Please select the appropriate type from the menu available.<br>";
helpText[2]  = "<b>Type of Cover</b><br>Please select the type of cover required.<br>";
helpText[3]  = "<b>Taxi Make</b><br>Please insert your vehicle make.<br>e.g. Volkswagen<br>";
helpText[4]  = "<b>Taxi Model</b><br>Please insert your vehicle model.<br>e.g. Passat TDI<br>";
helpText[5]  = "<b>Year Of Manufacture</b><br>Please select the year of manufacture for your taxi vehicle.<br>";
helpText[6]  = "<b>Vehicle Mileage</b><br>Please select your vehicle mileage .<br>";
helpText[7]  = "<b>Estimated Taxi Value</b><br>Please give an estimation of the vehicles value to the nearest &pound;<br>";
helpText[8]  = "<b>Max. Number of Passengers</b><br>Excluding the driver, how many passengers can the taxi legally carry per journey?<br>";
helpText[9]  = "<b>Taxi No Claims Bonus</b><br>Please indicate the number of years Taxi No Claims Bonus you have acquired.<br>";
helpText[10] = "<b>Plating Authority</b><br>Please select the local authority where your license was obtained.<br>";
helpText[11] = "<b>GAP insurance</b><br>GAP insurance can protect the value of your vehicle or any finance owed in the event of a total loss.<br>";
helpText[12] = "<b>Breakdown Cover</b><br>Please select if you are interested in breakdown cover<br>";
helpText[13] = "<b>Best Time To Call</b><br>In order to offer the best quotes available, some insurers may need to contact you via telephone. Please indicate the best time to call.<br>";
helpText[14] = "<b>Best Day To Call</b><br>In order to offer the best quotes available, some insurers may need to contact you via telephone. Please indicate the best day to call.<br>";
helpText[15] = "<b>Postcode</b><br>Insurance prices will vary by region. Please insert your Postcode.<br>";
helpText[16] = "<b>Insurance Start Date</b><br>Please indicate the date you wish for the policy to commence. Quotes can only be provided for up to 30 days in advance.<br>";
helpText[17] = "<b>Taxi Registration</b><br>Please insert your vehicle registration. If unknown, leave blank.<br>";
helpText[18] = "<b>Full UK Licence</b><br>Please select how long you have held a full UK licence.<br>";
helpText[19] = "<b>Taxi Badge</b><br>Please select how long you have held a taxi badge.<br>";
helpText[20] = "<b>Claims Last 5 Years</b><br>Please select if you have made any claims in the last 5 years.<br>";
helpText[21] = "<b>Convictions Last 5 Years</b><br>Please indicate if you've had any motoring convictions in the last 5 years.<br>";


helpText[22] = "<b>Private Car No Claims Bonus</b><br>Please indicate the number of years private car no claims bonus you have acquired.<br>";
helpText[23] = "<b>Taxi Driver(s)</b><br>Insurance prices will vary based on the taxi driver(s).  Please select the appropriate driver arrangement from the menu available.<br>";


var staticText = Array();
staticText[0] = "Please enter a valid phone number so our select panel can contact you to offer their best rates";
staticText[1] = "Start typing your licensing authority";
staticText[2] = "GAP insurance can protect the value of your vehicle or any finance owed in the event of a total loss.";
staticText[3] = "If unknown leave blank";

var blankPostcodeKeyPressed = false;
//**************************************************************************************************************************************
$().ready( function(){
   //$('#nextLnk').click(function(){
   showLightbox = $('input[name=showLightbox]').val();
   if (showLightbox == '1')
   {
      $('#lightbox')
         .css({backgroundColor:'white', position:'absolute'})
      $('#iframeLightbox')
         .css({backgroundColor:'white', position:'absolute'})
      $('#pageBackground')
         .height($(document).height()*1.3)
         .width($(document).width()-50)
         .css({backgroundColor:'white', opacity:'0.8', position:'absolute',left:'0', top:'0'})
         .fadeIn('def', function(){
            $('#lightbox').show();
            });
   }

$('#closeLightBox').hover(
         function(){$(this).css('cursor','pointer');},
         function(){$(this).css('cursor','default');}
   );

   $('#closeLightBox').click(function(){
      $('#lightbox').hide();
      $('#iframeLightbox').hide();
      $('#pageBackground').fadeOut('def', function(){
         $('input[name=showLightbox]').val("");
      });
      });
	
	$('input[name=daytime_telephone_lightbox]').blur(function(){
		var temp = $(this).val();
//		if (temp == "")
//			return;
/*		var res = temp.split("-");
		var number = "";
		var prefix = "";
		if(!res[1])
		{
			prefix = res[0];
		}
		else
		{
			prefix = res[0];
			number = res[1];
		}
		$('input[name=daytime_telephone_prefix]').val(prefix);
		$('input[name=daytime_telephone_number]').val(number);*/

                //check if daytime was altered
                var initial_daytime_phone = $('input[name=daytime_telephone]').val();

                if(temp != initial_daytime_phone)
                {
                   // track event
                   TrackElement('TAXI_LIGHTBOX','daytimephone_altered','IN');
                }

		$('input[name=daytime_telephone]').val(temp);
	});
	$('input[name=mobile_telephone_lightbox]').blur(function(){
		var temp = $(this).val();
//		if (temp == "")
//			return;
/*		var res = temp.split("-");
		var number = "";
		var prefix = "";
		if(!res[1])
		{
			prefix = res[0];
		}
		else
		{
			prefix = res[0];
			number = res[1];
		}*/
		//$('input[name=mobile_telephone_prefix]').val(prefix);
		//$('input[name=mobile_telephone_number]').val(number);

                //check if mobile was altered
                var initial_mobile_phone = $('input[name=mobile_telephone]').val();

                if(temp != initial_mobile_phone)
                {
                   // track event
                   TrackElement('TAXI_LIGHTBOX','mobilephone_altered','IN');
                }

		$('input[name=mobile_telephone]').val(temp);
	});
	$('input[name=email_address_lightbox]').blur(function(){
		var temp = $(this).val();

                //check if email was altered
                var initial_email_address = $('input[name=email_address]').val();

                if(temp != initial_email_address)
                {
                   // track event
                   TrackElement('TAXI_LIGHTBOX','email_address_altered','IN');
                }

		$('input[name=email_address]').val(temp);
	});
	$('input[name=email_address]').blur(function(){
		var temp = $(this).val();
		$('input[name=email_address_lightbox]').val(temp);
	});
	
	
	
	$('input[name=daytime_telephone]').blur(function(){
		var temp = $(this).val();
//		if (temp == "")
//			return;
/*		var res = temp.split("-");
		var number = "";
		var prefix = "";
		if(!res[1])
		{
			prefix = res[0];
		}
		else
		{
			prefix = res[0];
			number = res[1];
		}
		$('input[name=daytime_telephone_prefix]').val(prefix);
		$('input[name=daytime_telephone_number]').val(number);*/
		$('input[name=daytime_telephone_lightbox]').val(temp);
	});
	$('input[name=mobile_telephone]').blur(function(){
		var temp = $(this).val();
//		if (temp == "")
//			return;
/*		var res = temp.split("-");
		var number = "";
		var prefix = "";
		if(!res[1])
		{
			prefix = res[0];
		}
		else
		{
			prefix = res[0];
			number = res[1];
		}
		$('input[name=mobile_telephone_prefix]').val(prefix);
		$('input[name=mobile_telephone_number]').val(number);*/
		$('input[name=mobile_telephone_lightbox]').val(temp);
	});
	$('input[name=postcode]')
/*		.blur(function(){
			var temp = $(this).val();
			if (temp == "")
				return;
			var res = temp.split(" ");
			var number = "";
			var prefix = "";
			if(!res[1])
			{
				prefix = res[0];
			}
			else
			{
				prefix = res[0];
				number = res[1];
			}
			$('input[name=postcode_prefix]').val(prefix);
			$('input[name=postcode_number]').val(number);
			})*/
		.keypress(function(e){
				var code = e.charCode ? e.charCode : e.keyCode;
				if (code == 32)
				{
					if (blankPostcodeKeyPressed)
					{
						e.preventDefault();
						return;
					}
					blankPostcodeKeyPressed = true;
					return;
				}
			})
		.keyup(function(e){
				var text = $(this).val();
				if (text != "")
				{
					if (text.indexOf(' ') == -1)
						blankPostcodeKeyPressed = false;
				}
			});
});
	

   var timerID       = 0;
   var process       = false;
   var objContent       = new Array();
   var quotePrices = new Array();

   function SetProcessBegining()
   {
      process = true;
   }

   function CheckProcessStatus()
   {
      return process;
   }

   function UpdateTimer()
   {

      if(timerID)
         clearTimeout(timerID);

      timerID = setTimeout("UpdateTimer()", 10000);

      if(CheckProcessStatus())
         return false;
       
      //alert(frames['frame_quotes']);
      
      if(frames['frame_quotes'])
         frames['frame_quotes'].location.href = "steps/ViewQuotes.php";
   

   }

   function Start(mode)
   {

      //alert('Start');
 
      var timeusec = 10000;

      if(mode == 1)
         timeusec = 2000;

      timerID  = setTimeout("UpdateTimer()", timeusec);
   }

   function Stop()
   {
      if(timerID)
         clearTimeout(timerID);

      timerID  = 0;
      //alert('aaaaaaaaaaaaaaa');

   }

   Start(1);

   var quoteID;
   var quoteApply        = new Array();
   var quoteDetails      = new Array();
   var quoteBuyOnline    = new Array();
   var quoteBuyOverPhone = new Array();
   
   // track =============================================================
   var logID;
   var siteID           = new Array();
   var sid = '';

   function AddSiteID(quoteID, id)
   {
      siteID[quoteID] = id;
   }
   
   function SetTrackerMsg(msg)
   {
      if(typeof(msg) == 'undefined')
         msg = '';

      sid = msg;
   }

   var busy       = false;
   var hd         = null;
   
   function LogDetails(quoteID,detailsType)
   {
      return;

      if(busy)
      {
         tmplTimerID = setTimeout("LogDetails('" + quoteID + "','" + detailsType + "')", 100);
         return ;
      }
      
      tmplTimerID = 0;
      
      busy = true;
   
      if(hd == null)
      {
         // firefox, safari
         if (window.XMLHttpRequest)
             hd = new XMLHttpRequest();
         
         if (window.ActiveXObject)
            hd = new ActiveXObject("Microsoft.XMLHTTP");
            
      }
      
      hd.onreadystatechange  = function()
      {
         if(hd.readyState == '4')
         {      
            busy = false;
            hd=null;
         }
      }
      
      //hd.open("GET", 'http://bike-insurance.quotezone.co.uk/bike/sites/track.php?s=' + siteID[quoteID] + '&l='+logID+'&d=' + detailsType + sid,  true); 
//       hd.send(null);
   
   }
   
   function LogSiteDetails(sID,detailsType)
   {
      return;

      if(busy)
      {
         tmplTimerID = setTimeout("LogSiteDetails('" + sID + "','" + detailsType + "')", 100);
         return ;
      }
      
      tmplTimerID = 0;
      
      busy = true;
   
      if(hd == null)
      {
         // firefox, safari
         if (window.XMLHttpRequest)
             hd = new XMLHttpRequest();
         
         if (window.ActiveXObject)
            hd = new ActiveXObject("Microsoft.XMLHTTP");
            
      }
      
      hd.onreadystatechange  = function()
      {
         if(hd.readyState == '4')
         {      
            busy = false;
            hd=null;
         }
      }
      
      //hd.open("GET", 'http://bike-insurance.quotezone.co.uk/bike/sites/track.php?s=' + sID + '&l='+logID+'&d=' + detailsType + sid ,  true); 
      //hd.send(null);
   }
   
   // end track =============================================================
   
   function AddQuoteApply(quoteID, contents)
   {
      while (contents.indexOf('%quoteID%') != -1)
         contents = contents.replace('%quoteID%',quoteID);
      
      quoteApply[quoteID] = contents;
   }

   function AddQuoteDetails(quoteID, contents)
   {
      while (contents.indexOf('%quoteID%') != -1)
         contents = contents.replace('%quoteID%',quoteID);
      
      quoteDetails[quoteID] = contents;
   }

   function AddQuoteBuyOnline(quoteID, contents)
   {
      while (contents.indexOf('%quoteID%') != -1)
         contents = contents.replace('%quoteID%',quoteID);

      quoteBuyOnline[quoteID] = contents;
   }

   function AddQuoteBuyOverPhone(quoteID, contents)
   {
      while (contents.indexOf('%quoteID%') != -1)
         contents = contents.replace('%quoteID%',quoteID);

      quoteBuyOverPhone[quoteID] = contents;
   }


 function CloseApplyDetails(quoteID)
 {

   var premiumRow        = document.getElementById('premium_' + quoteID);
   var premiumTable      = document.getElementById('QuoteResults');

   // delete details
   for(var i = 1; i < premiumTable.rows.length ; i++)
   {
      if( premiumTable.rows[i].getAttribute('id') == 'details')
      {
         rowPosition = i;
         var ResultsTable = document.getElementById('QuoteResults').deleteRow(rowPosition);
      }
   }
 }

 function PopupWindow(page)
 {
    myWin = window.open('http://bike-insurance.quotezone.co.uk/bike/sites/assumptions/' + page,'Assumptions', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=450,height=400,left = 10,top = 10');
    myWin.focus();
 }

function MM_preloadimages()
{ //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadimages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d)
{ //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImgRestore()
{ //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_swapImage()
{ //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function LogPageClosedDetails()
{
   // firefox, safari
   if (window.XMLHttpRequest)
      hd = new XMLHttpRequest();
         
   if (window.ActiveXObject)
      hd = new ActiveXObject("Microsoft.XMLHTTP");
            
   //hd.open("GET", 'http://bike-insurance.quotezone.co.uk/bike/sites/track.php?s=0&l='+logID+'&d=pageClosed' + sid,  true); 
//    hd.send(null);
}

onBeforeUnloadEvent(LogPageClosedDetails);

function onBeforeUnloadEvent(func) {
   if(typeof window.addEventListener != 'undefined') {
      // moz, saf1.2, ow5b6.1
      window.addEventListener('beforeunload', func, false);
   } else if (typeof document.addEventListener != 'undefined') {
      // MSN/OSX, op7.50, saf1.2, ow5b6.1
      document.addEventListener('beforeunload', func, false);
   } else if (typeof window.attachEvent != 'undefined') {
      // ie5.0w, ie5.5w, ie6w
      window.attachEvent('onbeforeunload', func);
   } else {
      // all other browsers
      if (typeof window.onbeforeunload == 'function') {
         var oldonbeforeunload = onbeforeunload;
         window.onbeforeunload = function() {
            oldonbeforeunload();
            func();
         };
      } else {
            window.onbeforeunload = func;
      }
   }
}

   var quoteID;
   var quotePrices        = new Array();
   var quoteApply         = new Array();
   var quoteDetails       = new Array();
   var quoteBuyOnline     = new Array();
   var quoteBuyOverPhone  = new Array();
   var premiumColumn      = 1;

   // track =============================================================
   var logID;
   var siteID           = new Array();

  

   function SetLogID(id)
   {
      logID = id;
   }

   function LogDetails(quoteID,detailsType)
   {
      return;

      var im = new Image();

      im.src = 'http://travel-insurance.quotezone.co.uk/travel/sites/track.php?s=' + siteID[quoteID] + '&l='+logID+'&d=' + detailsType;
   }

   function LogSiteDetails(sID,detailsType)
   {
      return;

      var im = new Image();

      im.src = 'http://travel-insurance.quotezone.co.uk/travel/sites/track.php?s=' + sID + '&l='+logID+'&d=' + detailsType;
   }

   // end track =============================================================

   
   function InitGetQuotes()
   {
      quotePrices.length       = 0;
      quoteApply.length        = 0;
      quoteDetails.length      = 0;
      quoteBuyOnline.length    = 0;
      quoteBuyOverPhone.length = 0;
   }

   function AddQuoteApply(quoteID, contents)
   {
      while (contents.indexOf('%quoteID%') != -1)
         contents = contents.replace('%quoteID%',quoteID);
      
      quoteApply[quoteID] = contents;
   }

   function AddQuoteDetails(quoteID, contents)
   {
      while (contents.indexOf('%quoteID%') != -1)
         contents = contents.replace('%quoteID%',quoteID);
      
      quoteDetails[quoteID] = contents;
   }

   function AddQuoteBuyOnline(quoteID, contents)
   {
      while (contents.indexOf('%quoteID%') != -1)
         contents = contents.replace('%quoteID%',quoteID);

      quoteBuyOnline[quoteID] = contents;
   }

   function AddQuoteBuyOverPhone(quoteID, contents)
   {
      while (contents.indexOf('%quoteID%') != -1)
         contents = contents.replace('%quoteID%',quoteID);

      quoteBuyOverPhone[quoteID] = contents;
   }


   // Add quote result on table quote results
   function AddQuoteResult(quotePrice, cell1, cell2, cell3, cell4, cell5, cell6, cell7, cell8, cell9, cell10, cell11, cell12,cell13, specialOfferMessage, nextLink, tid,tid2, tid3, optionBuyId, insertRowAfter)
   {
      
      //alert(specialOfferMessage);

      var rowPosition = 1;
      var premiumTable = document.getElementById('QuoteResults');

      premiumTable.rows[0].cells[premiumColumn].className = 'hh';

      var numberRows = premiumTable.rows.length;

      // calculate row position 
      for(var i=1; i < quotePrices.length; i++)
      {

         if(parseFloat(quotePrice) == 0)
         {
            rowPosition += 1 ;
            continue;
         }

         if(parseFloat(quotePrices[i]) != 0)
            if(parseFloat(quotePrice) > parseFloat(quotePrices[i]))
               rowPosition += 1;
      }

      // move row one step down so that the brands oes below the original company for the same quote price

      if(rowPosition < quotePrices.length)
         if(insertRowAfter > 0)
            rowPosition += 1;

      //if(rowPosition > 1)
         rowPosition = rowPosition*2 - 1;

      //calulcate row position when quote details focused
      for(var j = 1; j <= premiumTable.rows.length; j++)
      {
         if(premiumTable.rows[j] == null)
            continue;

         if(typeof(premiumTable.rows[j]) == 'undefined')
            continue;
   
         if( premiumTable.rows[j].getAttribute('id') != 'details')
            continue;

         if(Number(rowPosition) < Number(j))
            break;

         rowPosition += 1;
 
         break;
         
      }

      quotePrices[i] = quotePrice;

      var quoteID = quotePrices.length - 1;
      
      //alert('a');
      //alert(rowPosition);

      var ResultsTable      = document.getElementById('QuoteResults').insertRow(rowPosition);
      var specialOffer      = document.getElementById('QuoteResults').insertRow(rowPosition + 1);
      
            
      //alert(quoteID);
      
      ResultsTable.setAttribute('id', 'premium_' + quoteID);
      
      var c0  = ResultsTable.insertCell(0); // LOGO
      var c1  = ResultsTable.insertCell(1); // STP
      var c2  = ResultsTable.insertCell(2); // AMP
      var c3  = ResultsTable.insertCell(3); // MEC
      var c4  = ResultsTable.insertCell(4); // PLC
      var c5  = ResultsTable.insertCell(5); // BC
      var c6  = ResultsTable.insertCell(6); // CC
      var c7  = ResultsTable.insertCell(7); // EXC
      var c8  = ResultsTable.insertCell(8); // LINK
      var c9  = ResultsTable.insertCell(9); // BC
      var c10 = ResultsTable.insertCell(10); // CC
      var c11 = ResultsTable.insertCell(11); // EXC
      var c12 = ResultsTable.insertCell(12); // LINK
      var c13 = ResultsTable.insertCell(13); // LINK
   
      //var c14 = specialOffer.insertCell(0); // logo
      //var c15 = specialOffer.insertCell(0); // logo
      var c16 = specialOffer.insertCell(0); // message
      //var c17 = specialOffer.insertCell(2); // LINK
           

      //super offer row    
      c0.colSpan = '2';
      c1.rowSpan = '2';
      c2.rowSpan = '2';
      c13.colSpan = '2';
      c0.rowSpan = '2';
      c13.rowSpan = '2';
     
      //c15.colSpan = '2';
      c16.colSpan = '10';
      //c17.colSpan = '2';

      c0.innerHTML  = cell1;
      c1.innerHTML  = cell3;  // single trip - monthly
      c2.innerHTML  = cell2;  // multi trip  - annual
      c3.innerHTML  = cell4;
      c4.innerHTML  = cell5;
      c5.innerHTML  = cell6;
      c6.innerHTML  = cell7;
      c7.innerHTML  = cell8;
      c8.innerHTML  = cell9;
      c9.innerHTML  = cell10;
      c10.innerHTML = cell11;
      c11.innerHTML = cell12;
      c12.innerHTML = cell13;
      // TO DO       
      // set class name accordingly with last premium or not !!!!
      
      
      c13.innerHTML = nextLink;
      c16.innerHTML = specialOfferMessage;
      //c16.innerHTML = specialOfferMessage;
     

      // set class name
      c0.className  = 'bc';
      c1.className  = 'bcpr';
      c2.className  = 'bc';
      c3.className  = 'bc';
      c4.className  = 'bc';
      c5.className  = 'bc';
      c6.className  = 'bc';
      c7.className  = 'bc';
      c8.className  = 'bc';
      c9.className  = 'bc';
      c10.className = 'bc';
      c11.className = 'bc';
      c12.className = 'bc';
      c13.className = 'bcl';

      //c15.className  = 'c';
      c16.className  = 'bco';
      //c17.className = 'cl';
           

      eval('c'+premiumColumn+'.className = \'bcp\'');
     
      if(rowPosition ==  '1')
      for(var i=2; i < quotePrices.length; i++)
      {
         j = 2*i -1 ;
 
         premiumTable.rows[j].cells[0].className  ='bc';
         premiumTable.rows[j].cells[1].className  ='bcpr';
         premiumTable.rows[j].cells[2].className  ='bcpr';
         premiumTable.rows[j].cells[3].className  ='bc';
         premiumTable.rows[j].cells[4].className  ='bc';
         premiumTable.rows[j].cells[5].className  ='bc';
         premiumTable.rows[j].cells[6].className  ='bc';
         premiumTable.rows[j].cells[7].className  ='bc';
         premiumTable.rows[j].cells[8].className  ='bc';
         premiumTable.rows[j].cells[9].className  ='bc';
         premiumTable.rows[j].cells[10].className ='bc';
         premiumTable.rows[j].cells[11].className ='bc';
         premiumTable.rows[j].cells[12].className ='bc';
         premiumTable.rows[j].cells[13].className ='bcl';

         premiumTable.rows[j].cells[premiumColumn].className = 'bcp';

         j = 2*i;

         //premiumTable.rows[j].cells[0].className ='c';
         premiumTable.rows[j].cells[0].className ='bco';
         //premiumTable.rows[j].cells[2].className ='cl';


      }

      // rewrite header class ( chepaest premium no top border)
      premiumTable.rows[1].cells[0].className  ='c';
      premiumTable.rows[1].cells[1].className  ='cpr';
      premiumTable.rows[1].cells[2].className  ='c';
      premiumTable.rows[1].cells[3].className  ='c';
      premiumTable.rows[1].cells[4].className  ='c';
      premiumTable.rows[1].cells[5].className  ='c';
      premiumTable.rows[1].cells[6].className  ='c';
      premiumTable.rows[1].cells[7].className  ='c';
      premiumTable.rows[1].cells[8].className  ='c';
      premiumTable.rows[1].cells[9].className  ='c';
      premiumTable.rows[1].cells[10].className ='c';
      premiumTable.rows[1].cells[11].className ='c';
      premiumTable.rows[1].cells[12].className ='c';
      premiumTable.rows[1].cells[13].className ='cl';
      
      premiumTable.rows[1].cells[premiumColumn].className ='cp';
      
      return quoteID;
   }

   function InsertPremiumLastRowTable()
   {
      var premiumTable = document.getElementById('QuoteResults');

      var rowPosition = premiumTable.rows.length;

      rowPosition--;

      document.getElementById('QuoteResults').deleteRow(rowPosition);

      var ResultsTable = document.getElementById('QuoteResults').insertRow(rowPosition);

      c1                       = ResultsTable.insertCell(0);
      c1.style.backgroundImage ='url(images/bl.gif)';
      c1.style.width           = '10px';
      c1.style.height          = '10px';
      c1.className             = '';

      c2                       = ResultsTable.insertCell(1);
      c2.style.backgroundImage ='url(images/bm.gif)';
      c2.className             = 'bi';
      c2.style.width           = '106px';
      c2.style.height          = '10px';
      c2.innerHTML             = '&nbsp;';

      c3                       = ResultsTable.insertCell(2);
      c3.style.backgroundImage ='url(images/gbm.gif)';
      c3.className             = 'bl';
      c3.style.height          = '10px';
      c3.innerHTML             = '&nbsp;';

      c4                       = ResultsTable.insertCell(3);
      c4.style.backgroundImage ='url(images/bm.gif)';
      c4.className             = 'bl';
      c4.style.height          = '10px';
      c4.innerHTML             = '&nbsp;';

      if(premiumColumn == 2)
      {
         c3.style.backgroundImage ='url(images/gbm.gif)';
         c4.style.backgroundImage ='url(images/bm.gif)';
      }

      c5                       = ResultsTable.insertCell(4);
      c5.style.backgroundImage ='url(images/gbm.gif)';
      c5.className             = 'bl';
      c5.style.height          = '10px';
      c5.innerHTML             = '&nbsp;';

      c6                       = ResultsTable.insertCell(5);
      c6.style.backgroundImage ='url(images/gbm.gif)';
      c6.className             = 'bi';
      c6.style.height          = '10px';
      c6.innerHTML             = '&nbsp;';

      c7                       = ResultsTable.insertCell(6);
      c7.style.backgroundImage ='url(images/gbm.gif)';
      c7.className             = 'bi';
      c7.style.height          = '10px';
      c7.innerHTML             = '&nbsp;';

      c8                       = ResultsTable.insertCell(7);
      c8.style.backgroundImage ='url(images/gbm.gif)';
      c8.className             = 'bi';
      c8.style.height          = '10px';
      c8.innerHTML             = '&nbsp;';

      c9                       = ResultsTable.insertCell(8);
      c9.style.backgroundImage ='url(images/gbm.gif)';
      c9.className             = 'bi';
      c9.style.height          = '10px';
      c9.innerHTML             = '&nbsp;';

      c10                       = ResultsTable.insertCell(9);
      c10.style.backgroundImage ='url(images/gbm.gif)';
      c10.className             = 'bi';
      c10.style.height          = '10px';
      c10.innerHTML             = '&nbsp;';

      c11                       = ResultsTable.insertCell(10);
      c11.style.backgroundImage ='url(images/gbm.gif)';
      c11.className             = 'bi';
      c11.style.height          = '10px';
      c11.innerHTML             = '&nbsp;';

      c12                       = ResultsTable.insertCell(11);
      c12.style.backgroundImage ='url(images/gbm.gif)';
      c12.className             = 'bi';
      c12.style.height          = '10px';
      c12.innerHTML             = '&nbsp;';

      c13                       = ResultsTable.insertCell(12);
      c13.style.backgroundImage ='url(images/gbm.gif)';
      c13.className             = 'bi';
      c13.style.height          = '10px';
      c13.innerHTML             = '&nbsp;';

      c14                       = ResultsTable.insertCell(13);
      c14.style.backgroundImage ='url(images/gbm.gif)';
      c14.className             = 'bi';
      c14.style.height          = '10px';
      c14.innerHTML             = '&nbsp;';

      c15                       = ResultsTable.insertCell(14);
      c15.style.backgroundImage ='url(images/bm.gif)';
      c15.className             = 'bl';
      c15.style.width           = '78px';
      c15.style.height          = '10px';
      c15.innerHTML             = '&nbsp;';

      c16                       = ResultsTable.insertCell(15);
      c16.style.backgroundImage ='url(images/br.gif)';
      c16.className             = '';
      c16.style.width              = '10px';
      c16.style.height             = '10px';
   }


   function PopupWindow(page)
   {
       myWin = window.open(page,'Policy', 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=450,height=400,left = 10,top = 10');
       myWin.focus();
   }

   function OpenPdf(url)
   {
       var ifrEl = document.getElementById('pdf');
       ifrEl.src = url;
   }

   function ShowFrame(id, failure, errorMessage)
   {
      return;
      if(typeof(errorMessage) == 'undefined' || errorMessage == '')
         errorMessage='Unable to quote for your circumstances.'

      NS4 = (document.layers) ? 1 : 0;
      IE4 = (document.all) ? 1 : 0;
      DOM = (document.getElementById) ? 1 : 0;

      var frame    = 'frame_quote_' + id;
      var quote    = 'quote_' + id;
      var wait     = 'wait_' + id;
      var noQuote  = 'noQuote_' + id;
      var error    = 'error_' + id;
      var table    = 'table_' + id;

      if(NS4)
      {
         if(failure)
         {
            document.layers[error].innerHTML = errorMessage;
            document.layers[noQuote].display   = "block";
            document.layers[table].display     = "block";
            document.layers[wait].display      = "none";
         }
         else
         {
            document.layers[table].display          = "none";
         }
      }
      else
      {
         if(DOM)
         {

            if(failure)
            {
               //alert(errorMessage);
               document.getElementById(error).innerHTML          = errorMessage;
               document.getElementById(noQuote).style.display    = "block";
               document.getElementById(table).style.display      = "block";
               document.getElementById(wait).style.display       = "none";
            }
            else
            {
               // hide table
               document.getElementById(table).style.display        = "none";
            }

         }
         else
         {
            if(failure)
            {
               document.all[error].innerHTML          = errorMessage;
               document.all[noQuote].style.display    = "block";
               document.all[table].style.display      = "block";
               document.all[wait].style.display       = "none";
            }
            else
            {
               document.all[table].style.display           = "none";
            }
         }
      }
   }// end ShowFrame(id,failure)


function ShowLoadingImage()
{
//   return;
   actDiv =  document.getElementById(objForm.GetElement('name'));

   if(! (actDiv == 'undefined' || actDiv == null))
      actDiv.style.display = 'none';

   document.getElementById('loading').style.display = 'block';
}

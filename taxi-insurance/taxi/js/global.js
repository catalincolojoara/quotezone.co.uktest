function ShowHelp(id)
{
   var activeElement = document.getElementById(id);
   var helpIframe    = document.getElementById('help_iframe');
   
   showElement(helpIframe, 'display');

   var title         = frames['help_iframe'].document.getElementById('help_title');
   var description   = frames['help_iframe'].document.getElementById('help_description');
   var example       = frames['help_iframe'].document.getElementById('help_example');
   
   title.innerHTML       = helpTitle[id];
   description.innerHTML = helpDescription[id];
   example.innerHTML     = helpExample[id];

   moveIframeToElement(helpIframe, activeElement, 'bri');
}

function HideHelp()
{
   var helpIframe    = document.getElementById('help_iframe');
   hideElement(helpIframe,'display');
}

function GetKeyUp(e)
{

   if(window.event)
   {
      if( typeof( window.event.keyCode ) == 'number'  )
         return  window.event.keyCode;
   }

   if(e)
   {
      if(typeof( e.which ) == 'number' )
         return e.which;

      if( typeof( e.charCode ) == 'number'  )
         return e.charCode;
   }


   return null;
}

function UrlEncode(string)
{
   var encodedString = escape(string);
   encodedString = encodedString.replace(/\*/g, "%2A");
   encodedString = encodedString.replace(/\@/g, "%40A");
   encodedString = encodedString.replace(/\+/g, "%2B");
   encodedString = encodedString.replace(/\//g, "%2F");    
   
   return encodedString;
}

function UrlDecode(string)
{
   var decodedString = unescape(string);
   decodedString = decodedString.replace(/%2A/g, "*");
   decodedString = decodedString.replace(/%40A/g, "@");
   decodedString = decodedString.replace(/%2B/g, "+");
   decodedString = decodedString.replace(/%2F/g, "/");    
   
   return decodedString;
}

// POSITION OF AN ELEMENT
function getPosX(obj)
{
	var p = $(".frmHouseNumSelBox").position();
	return p.left;
   if(typeof(obj) == 'undefined')
   {
      alert('undefined obj');
      return false;
   }

   var curleft = 0;

   if (obj.offsetParent)
   {
      while(obj.offsetParent)
      {
         curleft += obj.offsetLeft;
         obj = obj.offsetParent;
      }
   }
   else if (obj.x)
      curleft += obj.x;

   return curleft;
}

function getPosY(obj)
{
	var p = $(".frmHouseNumSelBox").position();
	return p.top;
   if(typeof(obj) == 'undefined')
   {
      alert('undefined obj');
      return false;
   }

   var curtop = 0;

   if (obj.offsetParent)
   {
      while(obj.offsetParent)
      {
         curtop += obj.offsetTop;
         obj = obj.offsetParent;
      }
   }
   else if (obj.y)
      curtop += obj.y;

   return curtop;
}
// end POSITION OF AN ELEMENT

// SELECT BOX FUNCTIONS
function resetSelectBox(obj)
{
   if(typeof(obj) == 'undefined')
   {
      alert('undefined obj');
      return false;
   }

   obj.options.length = 0;
   obj.options.value  = '';
}

function isSelectedOptionSelectBox(obj)
{
   if(typeof(obj) == 'undefined')
   {
      alert('undefined obj');
      return false;
   }

   if(obj.value == '')
      return false;

   return true;
}

function selectOptionSelectBox(obj, value)
{

   if(typeof(obj) == 'undefined')
   {
      alert('undefined obj');
      return false;
   }

   if(typeof(value) == 'undefined')
   {
      value = '';
   }
   
   for(i=0; i<obj.options.length; i++)
   {
      if(obj.options[i].value == value)
      {
         //obj.options[i].selected = true;
	      obj.options.selectedIndex = i;
         
         return true;
      }
   }
    
   return false;
}

function addOptionSelectBox(obj, index, key, value)
{
   if(typeof(obj) == 'undefined')
   {
      alert('undefined obj');
      return false;
   }

   if(typeof(key) == 'undefined')
   {
      alert('undefined key' + key);
      return false;
   }

   if(typeof(value) == 'undefined')
   {
      alert('undefined value' + value);
      return false;
   }

   if(typeof(index) == 'undefined' || index == '')
   {
      index = obj.options.length;
   }

   obj.options[index] = new Option(key, value);

}

function enableElement(obj)
{
   if(typeof(obj) == 'undefined')
   {
      alert('undefined obj');
      return false;
   }

   obj.disabled = false;

   return true;
}

function disableElement(obj)
{
   if(typeof(obj) == 'undefined')
   {
      alert('undefined obj');
      return false;
   }

   obj.disabled = true;

   return true;
}

function moveIframeToElement(iframeObj, obj, location)
{
   if(typeof(iframeObj) == 'undefined')
   {
      alert('undefined iframe');
      return false;
   }

   if(typeof(obj) == 'undefined')
   {
      alert('undefined obj');
      return false;
   }

   switch(location)
   {
      case 'blo':
         posHeight = obj.offsetHeight;
         posWidth  = -obj.offsetWidth;
         break;

      case 'bl':
         posHeight = obj.offsetHeight;
         posWidth  = 0 - iframeObj.offsetWidth/2;
         break;

      case 'bli':
         posHeight = obj.offsetHeight;
         posWidth  = 0;
         break;

      case 'bro':

         posHeight = obj.offsetHeight;
         posWidth  = obj.offsetWidth;

         break;

      case 'br':
         posHeight = obj.offsetHeight;
         posWidth  = 0 + obj.offsetWidth/2;
         break;

      case 'bri':
         posHeight = obj.offsetHeight;
         posWidth  = 0 + obj.offsetWidth - iframeObj.offsetWidth;
         break;

      case 'bc':
         posHeight = obj.offsetHeight;
         posWidth  = 0 + obj.offsetWidth/2 - iframeObj.offsetWidth/2;
         break;

      case 'tc':
         posHeight = - obj.offsetHeight;
         posWidth  = 0 + obj.offsetWidth/2 - iframeObj.offsetWidth/2;
         break;
   }

   var posX = getPosX(obj);
   var posY = getPosY(obj);

   iframeObj.style.top  = posY + posHeight + 'px';
   iframeObj.style.left = posX  + 'px';

   return  true;
}


function showElement(obj, style)
{
   if(typeof(obj) == 'undefined')
   {
      alert('undefined obj');
      return false;
   }

   if(typeof(style) == 'undefined')
   {
      style = 'visibility';
   }

   switch(style)
   {
      case 'visibility':
         obj.style.visibility = 'visible';
         break;

      case 'display':
         obj.style.display = 'block';
         break;
   }

   return true;
}

function hideElement(obj, style)
{
   if(typeof(obj) == 'undefined')
   {
      alert('undefined obj');
      return false;
   }

   if(typeof(style) == 'undefined')
   {
      style = 'visibility';
   }

   switch(style)
   {
      case 'visibility':
         obj.style.visibility = 'hidden';
         break;

      case 'display':
         obj.style.display = 'none';
         break;
   }

   return true;
}

function EscapeKeys(k)
{
   if(k==8 || k==9 || k==0 || k>63000)
      return true;

   return false;
}

//permit only digits entry on date fields
function PermitOnlyNumbers(e)
{
   var k = GetKeyUp(e);

   if(! EscapeKeys(k))
      if(k < 48 || k > 57)
         e.preventDefault ? e.preventDefault() : e.returnValue = false;
}

//permit only chars and line
function PermitOnlyCharsLine(e)
{
   var k = GetKeyUp(e);

   if(! EscapeKeys(k))
      if ((k > 32 && k<45) ||(k > 45 && k < 65) || (k > 90 && k < 97))
         e.preventDefault ? e.preventDefault() : e.returnValue = false;
}

//permit only chars
function PermitOnlyChars(e)
{
   var k = GetKeyUp(e);

   if(! EscapeKeys(k))
      if ((k > 32  && k < 65) || (k > 90 && k < 97))
         e.preventDefault ? e.preventDefault() : e.returnValue = false;
}

// dont permit blanks
function DontPermitBlanks(e)
{
   var k = GetKeyUp(e);

   if(! EscapeKeys(k))
      if(k == 32)
         e.preventDefault ? e.preventDefault() : e.returnValue = false;
}

//permit chars and digits
function PermitOnlyNumbersChars(e)
{
   var k = GetKeyUp(e);

   if(! EscapeKeys(k))
      if ((k > 32 && k < 48) || (k > 57 && k < 65) || (k > 90 && k < 97))
         e.preventDefault ? e.preventDefault() : e.returnValue = false;
}

// prepare date fields: day and month
function PrepareDate(name)
{
   var fieldName = eval('document.Insured.' + name);
   
   if(fieldName.value.length == '1')
   {
      if(fieldName.value == '0')
      {
         fieldName.value = '';
         return;
      }

      fieldName.value = '0' + fieldName.value;
   }
}

// testing row on focus
function ShowElementIn(el, more)
{
   var numRows = 0;

   if(typeof(more) == 'undefined')
   {
      more = false;
   }

   var subEle = el.parentNode;

   while(true)
   {
      if(subEle.getElementsByTagName('tr').length == numRows )
      {
         subEle = subEle.parentNode;
         continue;
      }

      if(more)
      {
         subEle = subEle.parentNode;
         numRows = subEle.getElementsByTagName('tr').length;
         more = false;
         continue;
      }

      table = subEle.parentNode;
      ShowElement(table);
      break;
   }
}

// testing row on focus
function ShowElementOut(el, more)
{
   var numRows = 0;

   if(typeof(more) == 'undefined')
   {
      more = false;
   }

   var subEle = el.parentNode;

   while(true)
   {

      if( subEle.getElementsByTagName('tr').length == numRows )
      {
         subEle = subEle.parentNode;
         continue;
      }

      if(more)
      {
         subEle = subEle.parentNode;
         numRows = subEle.getElementsByTagName('tr').length;
         more = false;
         continue;
      }

      table = subEle.parentNode;
      HideElement(table);
      break;
   }
}

// testing row on focus
function ShowElement(table)
{
//    return;

   var tableRows = table.getElementsByTagName('tr');

   for(var i=0; i < tableRows.length; i++ )
   {
      tableData = tableRows[i].getElementsByTagName('td');

      for(j=0; j< tableData.length ; j++)
      {
         if(tableData[j].className == 'i')
         {
            tableData[j].className = 'if';
            continue;
         }

         if(tableData[j].className == 'q')
         {
            tableData[j].className = 'qf';
            continue;
         }
      }
   }
}

// testing row on focus
function HideElement(table)
{
//    return;

   var tableRows = table.getElementsByTagName('tr');

   for(var i=0; i < tableRows.length; i++ )
   {
      tableData = tableRows[i].getElementsByTagName('td');

      for(j=0; j< tableData.length ; j++)
      {
         if(tableData[j].className == 'if')
         {
            tableData[j].className = 'i';
            continue;
         }

         if(tableData[j].className == 'qf')
         {
            tableData[j].className = 'q';
            continue;
         }
      }
   }
}

function PrepareDayMonth(obj)
{
   if(typeof(obj)=='undefined')
      return;
   
   if(obj.value.length != '1')
     return;

   if(obj.value == '0')
   {
      obj.value = '';
      return;
   }

   obj.value = '0' + obj.value;
}

function PrepareYear(obj)
{

   if(typeof(obj)=='undefined')
      return;

   if(obj.value.length != '2')
      return;
  

    if(Number(obj.value) <= '10')
    {
      obj.value = '20' + obj.value;
    }
    else
    {
      obj.value = '19' + obj.value;
    }
}


function UCWord(obj)
{
   if(typeof(obj)=='undefined')
      return;

   obj.value = obj.value.toUpperCase();
}

function UCWords(obj)
{
   if(typeof(obj)=='undefined')
      return;

   var string = new String();
   var newstring = new String();
   var found = false;
   
   string = obj.value.toLowerCase();

   for (var i = 0; i < string.length; i++)
   {

      if(i == '0')
      {
         newstring += string.charAt(0).toUpperCase();
         continue;
      }

      if(string.charAt(i) == ' ' || string.charAt(i) == '-')
      {
         newstring += string.charAt(i);

         found = true;
         continue;
      }

      if(found)
      {
         newstring += string.charAt(i).toUpperCase();
         found = false;
         continue;
      }

      newstring += string.charAt(i);
  }

  obj.value=newstring;
}

function Trim(value)
{
   if(value == null || value == 'undefined')
      return;

   if(value == '')
      return '';

   var valueLength = value.length
   var leftIndex   = 0;
   var rightIndex  = 0;

   // check from left
   for(var i=0; i < value.length; i++)
   {
      //check if begin with space
      if(value.charAt(i) != ' ')
      {
         leftIndex = i;
         break;
      }
   }

   //check from right
   for(var i=value.length - 1; i>= 0; i--)
   {
      //check if begin with space
      if(value.charAt(i) != ' ')
      {
         rightIndex = i + 1;
         break;
      }
   }

   // eliminate the tail
   value = value.substring(0, rightIndex);
   // eliminate the head
   value = value.substring(leftIndex, value.length);

   return value;
}

function monthDays(month, year)
{
   switch(month)
   {
      case '02':
         if(year/4 - parseInt(year/4))
         {
            return 28;
         }
         return 29;
      break;

      case '04':
      case '06':
      case '09':
      case '11':
         return 30;
      break;
      default:
         return 31;
      break;

   }
}

function ShowStepImage(step)
{

   //alert('step');
   var div1 = document.getElementById('header_img1');
   //var div2 = document.getElementById('header_img2');
   //var div3 = document.getElementById('header_img3');
   //var div4 = document.getElementById('text_right');

   div1.style.display = "none";
   //div2.style.display = "none";
   //div3.style.display = "none";
   //div4.style.display = "block";
}

function HideQuoteSearchingHeader()
{
 
    //alert('step');
    var quoteSearchingHeader = document.getElementById('QuoteSearchingHeader');
    var quoteSearching       = document.getElementById('QuoteSearching');
    var overtureHeader       = document.getElementById('overtureHeader');
    var overture             = document.getElementById('overture');


    quoteSearchingHeader.style.display = "none";
    quoteSearching.style.display = "none";
    overtureHeader.style.display = "none";
    overture.style.display = "none";
}

   function Next()
   {
      document.YourQuotes.step.value = "next";
      document.YourQuotes.submit();
   }

   function Back()
   {
      Stop();
      document.YourQuotes.step.value = "back";
      document.YourQuotes.submit();
   }

   function HideLoadingImage()
   {
      document.getElementById('loading').style.display = 'none';
   }

   function ShowSiteContent()
   {
      var site    = document.getElementById('siteContent');
      var loading = document.getElementById('siteLoading');

      loading.style.display = 'none';
      site.style.display    = 'block';
   }

   if(document.getElementById("mozopacity"))
   {
      var clientwidth = document.documentElement.clientWidth;
      var clientheight = document.documentElement.clientHeight;

      document.getElementById("mozopacity").style.width = $('siteContent').getWidth()+"px";
      document.getElementById("mozopacity").style.height = $('siteContent').getHeight()+"px";
   }

   function Redirect(url)
   {
      location.href = url;
   }

function Restart(wlId)
{
	if (!wlId)
		wlId = '';

	really = confirm('Are you sure you want to restart the quoting engine?');

	if (really)
		document.location = 'steps_wl/RestartQuote.php?id='+wlId;
}

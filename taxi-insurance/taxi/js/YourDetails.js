   var site               = document.getElementById('siteContent');
   var loading            = document.getElementById('siteLoading');
   var xmlhttp;
   
   loading.style.display = 'none';
   site.style.display    = 'block';

   function Next(obj)
   {
      if(document.YourDetails.step.value != "")
         return;

      obj.onclick = "";

      document.getElementById('siteContent').style.display = 'none';
      document.getElementById('siteLoading').style.display = 'block';
      document.getElementById('loading').style.display = 'block';

      var browserName=navigator.appName;

      if (browserName == "Microsoft Internet Explorer")
      {
         var imgSpan  = document.getElementById('img_span');
         var spanContent   = imgSpan.innerHTML;
         imgSpan.innerHTML = spanContent;
      }

      document.YourDetails.step.value = "next";
      document.YourDetails.submit();
   }

   // prepare date day and month
   function DataFill(name)
   {
      var fieldName = eval('document.YourDetails.' + name);

      if(fieldName.value.length == '1')
      {
         if(fieldName.value == '0')
         {
            fieldName.value = '';
            return;
         }

         fieldName.value = '0' + fieldName.value;
      }
   }

   function BussinesLookup(fullList)
  {
   var key            = document.getElementById('plating_authority').value;
   var fakeBusListDiv = document.getElementById('bus_id');
   var fakeBusError   = document.getElementById('business_error_txt');

   if(fullList)
      key = '%3F%3F%3F';

   if(key == '')
      return;

   // save the current lookup key
   //document.Drivers.business_key_bkp.value = key;

   fakeBusListDiv.innerHTML  = '<table cellspacing="0" cellpadding="2" bgcolor="#FFFFFF" style="width: 215px; border: 1px #FFFFFF inset;"><tr><td height="15" valign="middle"><img src="images/loading1.gif"></td><td valign="middle"><font size="-2">Please wait while searching...</font></td></tr></table>';

   fakeBusListDiv.style.display = 'block';
   fakeBusListDiv.style.left = fakeBusListDiv.offsetLeft + 0 + 'px';

   var paramString = 'key=' + key;

   try {
        // Firefox, Opera 8.0+, Safari
        xmlhttp=new XMLHttpRequest();
        } catch (e)
        {   // Internet Explorer
                try {
                        xmlhttp=new ActiveXObject("Msxml2.XMLHttp.3.0");
                        }
                catch (e)
                {
                try {
                        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                        }
                catch (e) {
                        alert("Your browser does not support ajax!");
                        return false;
                        }
                }
        }

         xmlhttp.onreadystatechange=function()
                    {
                       if(xmlhttp.readyState==4)
                       {
                         var response = xmlhttp.responseText;

                         document.getElementById('bus_id').innerHTML=response;
                       }
                    };

        xmlhttp.open("GET","steps/BusinessFt.php?key="+key,true);
        xmlhttp.send(null);


//   new Ajax.Updater('bus_id', 'steps/BusinessFt.php', { method: 'get', parameters: paramString, evalScripts: true });

   return;
}

   function selectBusFtFromFake()
   {
   var fakeListElm = document.YourDetails.fake_business_list;

   // find selected text
   for(i=0; i<fakeListElm.options.length; i++)
   {
      if(fakeListElm.options[i].selected)
      {
         selectBusFT(fakeListElm.options[i].text);
         break;
      }
   }

   return;
   }

   function BusinessFtControl()
   {
   var fakeListElm = document.YourDetails.fake_business_list;
   var reset       = "false";

   for(i=0; i<fakeListElm.options.length; i++)
   {
      if(fakeListElm.options[i].selected)
      {
         if(fakeListElm.options[i].text == document.forms.YourDetails.plating_authority.value)
            reset = "true";
      }
   }

   if(reset == "false")
   {
      document.forms.YourDetails.plating_authority.value='';
      document.getElementById('bus_id').style.display='none';
   }
   }

   function selectBusFT(SelectedText)
   {
   var listElm     = document.YourDetails.business_list;
   var fakeListElm = document.YourDetails.fake_business_list;
   var fakeListDiv = document.getElementById('bus_id');

   var selectedValue ;

//   Element.extend(fakeListDiv);
   fakeListDiv.style.display = "none";


   // rebuild the select box
   // while(listElm.options.length)
   // listElm.remove(0);
   listElm.options.length = 0;

   for(i=0; i<fakeListElm.options.length; i++)
   {
      //listElm.options[i] = new Option(fakeListElm.options[i].text, fakeListElm.options[i].value);

      if(fakeListElm.options[i].selected)
      {
         listElm.options[0] = new Option(fakeListElm.options[i].text, fakeListElm.options[i].value);
         listElm.options[0].selected = true;
         selectedValue = fakeListElm.options[i].value;
         break;
      }
   }

   if(selectedValue == 'other')
   {
      BussinesLookup(1);
      return;
   }

   document.YourDetails.plating_authority.value = SelectedText;

   //listElm.style.display = 'none';
   }


   //permit only chars
   function PermitOnlyChars()
   {
      if ((event.keyCode > 32 && event.keyCode < 65) || (event.keyCode > 90 && event.keyCode < 97))
         event.returnValue = false;
   }

   function DontPermitBlanks()
   {
      if(event.keyCode == 32)
         event.returnValue = false;
   }

   //permit chars and digits
   function PermitOnlyNumbersChars()
   {
      if ((event.keyCode > 33 && event.keyCode < 48) || (event.keyCode > 57 && event.keyCode < 65) || (event.keyCode > 90 && event.keyCode < 97))
         event.returnValue = false;
   }


  // prepare postcode entry
  function PreparePostcode()
  {
     var postcodeLeft        = document.YourDetails.postcode_prefix;
     var postcodeRight       = document.YourDetails.postcode_number;
     var postcodeLeftLength  = document.YourDetails.postcode_prefix.value.length;
     var postcodeRightLength = document.YourDetails.postcode_number.value.length;

     if(postcodeRightLength == 0 || postcodeRightLength == 3)
        return;


      while((postcodeRightLength < 3) && (postcodeLeftLength > 2))
      {
         postcodeRight.value = postcodeLeft.value.charAt(postcodeLeftLength - 1) + postcodeRight.value;
         postcodeRightLength++;
         postcodeLeftLength--;
      }

      postcodeLeft.value = postcodeLeft.value.substring( 0 , postcodeLeftLength);

  }

function UCWord(obj)
{
   if(typeof(obj)=='undefined')
      return;

   obj.value = obj.value.toUpperCase();
}

function UCWords(obj)
{
   if(typeof(obj)=='undefined')
      return;

   var string = new String();
   var newstring = new String();
   var found = false;
   
   string = obj.value.toLowerCase();

   for (var i = 0; i < string.length; i++)
   {

      if(i == '0')
      {
         newstring += string.charAt(0).toUpperCase();
         continue;
      }

      if(string.charAt(i) == ' ' || string.charAt(i) == '-')
      {
         newstring += string.charAt(i);

         found = true;
         continue;
      }

      if(found)
      {
         newstring += string.charAt(i).toUpperCase();
         found = false;
         continue;
      }

      newstring += string.charAt(i);
  }

  obj.value=newstring;
}
//permit only chars and line
function PermitOnlyCharsLine(e)
{
   var k = GetKeyUp(e);

   if(! EscapeKeys(k))
      if ((k > 32 && k<45) ||(k > 45 && k < 65) || (k > 90 && k < 97))
         e.preventDefault ? e.preventDefault() : e.returnValue = false;
}

//permit only chars
function PermitOnlyChars(e)
{
   var k = GetKeyUp(e);

   if(! EscapeKeys(k))
      if ((k > 32  && k < 65) || (k > 90 && k < 97))
         e.preventDefault ? e.preventDefault() : e.returnValue = false;
}

// dont permit blanks
function DontPermitBlanks(e)
{
   var k = GetKeyUp(e);

   if(! EscapeKeys(k))
      if(k == 32)
         e.preventDefault ? e.preventDefault() : e.returnValue = false;
}

//permit chars and digits
function PermitOnlyNumbersChars(e)
{
   var k = GetKeyUp(e);

   if(! EscapeKeys(k))
      if ((k > 32 && k < 48) || (k > 57 && k < 65) || (k > 90 && k < 97))
         e.preventDefault ? e.preventDefault() : e.returnValue = false;
}

function ShowLoadingImage()
{
//   return;
   actDiv =  document.getElementById(objForm.GetElement('name'));

   if(! (actDiv == 'undefined' || actDiv == null))
      actDiv.style.display = 'none';

   document.getElementById('loading').style.display = 'block';
}

function ShowStepImage(step)
{
   if (document.getElementById('img_step'))
   {
      var img = document.getElementById('img_step');
      img.src= 'images/' + step + '.gif';
   }
   else
      return;
}

function MoveCursorToNextDay()
{
   var daytimeTelPrefix  = document.getElementById('daytime_telephone_prefix');
   var daytimeTelNumber  = document.getElementById('daytime_telephone_number');

   var valOfPref    = daytimeTelPrefix.value;
   var noOfTelChars = valOfPref.length;

   if(noOfTelChars == '5')
   {
      daytimeTelNumber.focus();
      return;
   }
}

function MoveCursorToNextMobile()
{
   var mobileTelPrefix   = document.getElementById('mobile_telephone_prefix');
   var mobileTelNumber   = document.getElementById('mobile_telephone_number');

   var valOfPrefix  = mobileTelPrefix.value;
   var noOfMobChars = valOfPrefix.length;

   if(noOfMobChars == '5')
   {
      mobileTelNumber.focus();
      return;
   }

}

function PostcodeSelected()
{
   var childPostcodeSelect  = document.YourDetails.found_address;
   var parentPostcodeSelect = document.YourDetails.pcode;
   var PostcodeDivElement   = document.getElementById("postcode_id");

   document.YourDetails.found_address.value        = parentPostcodeSelect.value;
   document.YourDetails.house_number_or_name.value = parentPostcodeSelect.value;

   childPostcodeSelect.value = parentPostcodeSelect.value;

   if(PostcodeDivElement.style.visibility == 'visible')
   {
      PostcodeDivElement.style.visibility = 'hidden';
   }

   // check Not show in the list value
   if(document.YourDetails.found_address.value == '')
   {
      var notFoundDiv = document.getElementById('div_not_found_address');
      var foundDiv    = document.getElementById('div_found_address');

      foundDiv.style.display    = 'none';
      notFoundDiv.style.display = 'block';

      document.YourDetails.not_found_address.focus();

      return;
   }

   childPostcodeSelect.focus();
}

function SetPostcodePrefixNumber(postcode)
{
	postcode = new String(Trim(postcode));
	postcode = postcode.split(' ').join('');
	prefix   = '';
	number   = '';

	prefix = Trim(postcode.slice(0,postcode.length-3));
	number = Trim(postcode.slice(postcode.length-3, postcode.length));

	$('input[name=postcode_prefix]').val(prefix);
	$('input[name=postcode_number]').val(number);
//	document.forms.Drivers.postcode_prefix.value = prefix;
//	document.forms.Drivers.postcode_number.value = number;
}

function PostcodeSubmit()
{
   var postcodeFrame = document.getElementById('iframe_postcode');
   var postcodePref  = document.YourDetails.postcode_prefix;
   var postcodeSuf   = document.YourDetails.postcode_number;

   document.getElementById("postcode_id").style.visibility = 'hidden';

   postcodeFrame.src  = 'steps/Postcode.php?pc=' + postcodePref.value + ' ' + postcodeSuf.value;

   document.YourDetails.address_line1.value = '';

   return;
}

function ConfirmHouseNumberName()
{
   var notFoundDiv = document.getElementById('div_not_found_address');
   var foundDiv    = document.getElementById('div_found_address');

   // check Not show in the list value
   if(document.YourDetails.found_address.value == 'not_shown')
   {
      foundDiv.style.display    = 'none';
      notFoundDiv.style.display = 'block';

      document.YourDetails.found_address.value        = '';

      document.YourDetails.address_line1.value    = '';
      document.YourDetails.house_number_or_name.value = '';

      document.YourDetails.address_line1.focus();
   }

   if(notFoundDiv.style.display == 'none')
      if(document.YourDetails.found_address.value == 'not_shown')
      {
         document.YourDetails.address_line1.value    = '';
      }

   document.YourDetails.house_number_or_name.value = '';
   if(document.YourDetails.address_line1.value != '' && document.YourDetails.address_line2.value != '')
   {
      document.YourDetails.house_number_or_name.value = document.YourDetails.address_line1.value + ' ' + document.YourDetails.address_line2.value + ' ' + document.YourDetails.address_line3.value + ' ' + document.YourDetails.address_line4.value;
   }

   if(foundDiv.style.display == 'block')
      document.YourDetails.house_number_or_name.value = document.YourDetails.found_address.value;

}

function ResetHouseNumber()
{
   document.YourDetails.house_number_or_name.value = '';
}

//ViewPropertyDetails();

function ViewPropertyDetails()
{
   var homeOwnerY = document.YourDetails.home_owner[0];
   var homeOwnerN = document.YourDetails.home_owner[1];

   var pvDiv = document.getElementById('propertyValue');
   var moDiv = document.getElementById('mortageOutstanding');

   var pvTd = parent.document.getElementById('propertyValueTD');
   var moTd = parent.document.getElementById('mortageOutstandingTD');

   if(homeOwnerY.checked)
   {
      pvDiv.style.display = 'block';
      moDiv.style.display = 'block';

      pvTd.style.borderWidth      = '1px';
      moTd.style.borderWidth      = '1px';
   }
   else
   {
      // hide property value and mortage outstanding blocks
      pvDiv.style.display = 'none';
      moDiv.style.display = 'none';

      pvTd.style.borderWidth      = '0px';
      moTd.style.borderWidth      = '0px';

      // reset property value and mortage outstanding params
      document.YourDetails.property_value.value = '';
      document.YourDetails.mortage_outstanding.value  = '';
   }
}

function SetMaxPassengers(taxyType)
{
    var origSel         = document.getElementById('orig_capacity');
    var fakeSel        = document.getElementById('fake_capacity');

    origSel.style.display = 'none';
    fakeSel.style.display = 'block';

    try {
    	// Firefox, Opera 8.0+, Safari   
    	xmlhttp=new XMLHttpRequest(); 
    	} catch (e) 
    	{   // Internet Explorer   
    		try {     
    			xmlhttp=new ActiveXObject("Msxml2.XMLHttp.3.0");   
    			}   
    		catch (e) 
    		{     
                try {
    			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");     
    			}     
    		catch (e) {
    			alert("Your browser does not support ajax!");
    			return false;    
    			}   
    		} 
    	}

    	xmlhttp.onreadystatechange=function()
		    {
		       if(xmlhttp.readyState==4)
		       {
		    	 var response = xmlhttp.responseText;
		         document.getElementById('fake_capacity').innerHTML=response;
		       }
		    };
    
    	xmlhttp.open("GET","steps/SetMaxPassengers.php?cab="+taxyType,true);
    	xmlhttp.send(null);

}

function setTempSes(tempSelValue)
{
  var xmlhttp;
   if (window.XMLHttpRequest)
   {
      // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
   }
   else if (window.ActiveXObject)
   {
      // code for IE6, IE5
      //xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      xmlhttp=new ActiveXObject("MSXML2.XMLHTTP.3.0");
   }
   else
   {
      alert("Your browser does not support XMLHTTP!");
   }
   xmlhttp.onreadystatechange=function()
   {
      if(xmlhttp.readyState==4)
      {
         return;
      }
   }
   
   xmlhttp.open("GET","steps/SetTempSes.php?tempVal="+tempSelValue,true);
   xmlhttp.send(null);
}

   var helpTitle       = new Array();
   var helpDescription = new Array();
   var helpExample     = new Array();
   
   helpTitle[1]       = '<b>Taxi Use</b>';
   helpDescription[1] = 'Private hire covers pre booked jobs whilst public hire grants on street pickup.';
   helpExample[1]     = '';

   helpTitle[2]       = '<b>Taxi Type</b>';
   helpDescription[2] = 'Insurance prices will vary based on the vehicle type. Please select the appropriate type from the menu available.';
   helpExample[2]     = '';

   helpTitle[3]       = '<b>Taxi Make</b>';
   helpDescription[3] = 'Please insert your vehicle make';
   helpExample[3]     = 'e.g. Volkswagen';

   helpTitle[4]       = '<b>Taxi Model</b>';
   helpDescription[4] = 'Please insert your vehicle model';
   helpExample[4]     = 'e.g. Passat TDI';

   helpTitle[5]       = '<b>Estimated Taxi Value</b>';
   helpDescription[5] = 'Please give an estimation of the vehicles value to the nearest pound';
   helpExample[5]     = '';

   helpTitle[6]       = '<b>How many passengers is the taxi licensed to carry?</b>';
   helpDescription[6] = 'Excluding the driver, how many passengers can the taxi legally carry per journey?';
   helpExample[6]     = '';

   helpTitle[7]       = '<b>Best time to call</b>';
   helpDescription[7] = 'In order to offer the best quotes available, some insurers may need to contact you via telephone. Please indicate the best time to call.';
   helpExample[7]     = '';

   helpTitle[8]       = '<b>Best Day to call</b>';
   helpDescription[8] = 'In order to offer the best quotes available, some insurers may need to contact you via telephone. Please indicate the best day to call.';
   helpExample[8]     = '';

   helpTitle[9]       = '<b>Postcode</b>';
   helpDescription[9] = 'Insurance prices will vary by region. Please insert your Postcode';
   helpExample[9]     = 'e.g. PE27 6TX';

   helpTitle[10]       = '<b>Insurance Start Date</b>';
   helpDescription[10] = 'Please indicate the date you wish for the policy to commence. Quotes can only be provided for up to 30 days in advance.';
   helpExample[10]     = '';
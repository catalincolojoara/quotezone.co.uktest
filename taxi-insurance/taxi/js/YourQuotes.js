   var site    = document.getElementById('siteContent');
   var loading = document.getElementById('siteLoading');

   loading.style.display = 'none';
   site.style.display    = 'block';

   function Next()
   {
      if(document.YourQuotes.step.value != "")
         return;

      document.YourQuotes.step.value = "next";
      document.YourQuotes.submit();
   }

   function Back()
   {
      document.YourQuotes.step.value = "back";
      document.YourQuotes.submit();
   }

   // prepare date day and month
   function DataFill(name)
   {
      var fieldName = eval('document.YourQuotes.' + name);

      if(fieldName.value.length == '1')
      {
         if(fieldName.value == '0')
         {
            fieldName.value = '';
            return;
         }

         fieldName.value = '0' + fieldName.value;
      }
   }


<?php

function UnsubscribeUser($email,$unsubscribeOptions)
{

   if(!eregi("^[a-zA-Z0-9_./+-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$",$email))
   {
      print "<!--Email address format is not valid-->";
   }
/*
   YES to unsubscribe

   <form Action="http://as1.emv2.com/D" method="POST" target="_top">
   <input type="hidden" name="emv_pageok" value="Unsubscribe Success Page">
   <input type="hidden" name="emv_pageerror" value="Unsubscribe Fail Page">
   <input type="hidden" name="emv_webformtype" value="1">
   <input type="hidden" name="emv_bounceback" value="0">
   <input type="hidden" name="emv_clientid" value="35812">
   <input type="text" name="EMAIL_FIELD" value="email address">
*/
   $UnsubscribeArray = array();

   $unsDate = date("m/d/Y");

   $UnsubscribeArray["emv_pageok"]      = "http://www.quotezone.co.uk/unsubscribeemailpostsuccesshome.htm";
   $UnsubscribeArray["emv_pageerror"]   = "http://www.quotezone.co.uk/unsubscribeemailpostfailurehome.htm";
   $UnsubscribeArray["emv_webformtype"] = "3";
   $UnsubscribeArray["emv_bounceback"]  = "0";
   $UnsubscribeArray["emv_clientid"]    = "35812";
   $UnsubscribeArray["EMAIL_FIELD"]     = $email;
	$UnsubscribeArray["NEWSLETTER_UNSUBSCRIBE_FIELD"] = (array_key_exists("NEWSLETTER_UNSUBSCRIBE_FIELD",$unsubscribeOptions)?$unsDate:"");
	$UnsubscribeArray["RENEWAL_UNSUBSCRIBE_FIELD"] = (array_key_exists("RENEWAL_UNSUBSCRIBE_FIELD",$unsubscribeOptions)?$unsDate:"");
	$UnsubscribeArray["PROMOTION_UNSUBSCRIBE_FIELD"] = (array_key_exists("PROMOTION_UNSUBSCRIBE_FIELD",$unsubscribeOptions)?$unsDate:"");
	$UnsubscribeArray["THIRDPARTY_UNSUBSCRIBE_FIELD"] = (array_key_exists("THIRDPARTY_UNSUBSCRIBE_FIELD",$unsubscribeOptions)?$unsDate:"");
	$UnsubscribeArray["SITENEWS_UNSUBSCRIBE_FIELD"] = (array_key_exists("SITENEWS_UNSUBSCRIBE_FIELD",$unsubscribeOptions)?$unsDate:"");
	$UnsubscribeArray["SURVEY_UNSUBSCRIBE_FIELD"] = (array_key_exists("SURVEY_UNSUBSCRIBE_FIELD",$unsubscribeOptions)?$unsDate:"");
	$UnsubscribeArray["RESULT_UNSUBSCRIBE_FIELD"] = (array_key_exists("RESULT_UNSUBSCRIBE_FIELD",$unsubscribeOptions)?$unsDate:"");

//print "<pre>";
//print_r($UnsubscribeArray);
//die();

   foreach($UnsubscribeArray as $key=>$val)
   {
      $string .= $key."=".urlencode($val)."&";
   }
//print $string;
   $string = rtrim($string, "&");

   //print "$string \n\n\n\n";
   $host = 'as1.emv2.com';
   $port = 80;
   $path = "/D";

   $fp = fsockopen($host, $port, $errno, $errstr, $timeout = 30);
   if (!$fp)
   {
      //print out any errors
      echo "<!-- $errstr ($errno)\n -->";
   }
   else //if the connection was established successfully
   {
      //send the server request
      fputs($fp, "POST $path HTTP/1.1\r\n");
      fputs($fp, "Host: $host\r\n");
      fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
      fputs($fp, "Content-length: ".strlen($string)."\r\n");
      fputs($fp, "Connection: close\r\n\r\n");
      fputs($fp, $string . "\r\n\r\n");

      //store the response
      while (!feof($fp))
      {
         $buffer .= fread($fp,4096);
         //print $buffer."\r\n";
      }
      //print $buffer;

   }


   $logFile = fopen("/home/www/quotezone.co.uk/insurance-new/home/HomeEmails/unsubscribedDetails.log", "a+");
   $today = date("Y-m-d H:i:s");
   fwrite($logFile, "$today $logID $buffer \n $string \n");
   fclose($logFile);

   //return $buffer;

}//end function UnsubscribeUser($email)

function ResubscribeUser($email)
{
   if(!eregi("^[a-zA-Z0-9_./+-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$",$email))
   {
      print "<!--Email address format is not valid-->";
   }

/*
   If the user selects No to unsubscribe, then use this (this will resubscribe the user from the email database):

   <form Action="http://as1.emv2.com/D" method="POST" target="_top">
   <input type="hidden" name="emv_pageok" value="Resubscribe Success Page">
   <input type="hidden" name="emv_pageerror" value="Resubscribe  Fail Page">
   <input type="hidden" name="emv_webformtype" value="3">
   <input type="hidden" name="emv_bounceback" value="0">
   <input type="hidden" name="emv_clientid" value="35812">
   <input type="text" name="EMAIL_FIELD" value="email address">
   <input type="text" name="DATEUNJOIN_FIELD" value="">
*/

   $resubscribeArray = array();

   $resubscribeArray["emv_pageok"]       = "http://www.quotezone.co.uk/resubscribeemailpostsuccesscar.htm";
   $resubscribeArray["emv_pageerror"]    = "http://www.quotezone.co.uk/resubscribeemailpostfailurecar.htm";
   $resubscribeArray["emv_webformtype"]  = "3";
   $resubscribeArray["emv_bounceback"]   = "0";
   $resubscribeArray["emv_clientid"]     = "35812";
   $resubscribeArray["EMAIL_FIELD"]      = $email;
   $resubscribeArray["DATEUNJOIN_FIELD"] = "";


   foreach($resubscribeArray as $key=>$val)
   {
      $string .= $key."=".urlencode($val)."&";
   }

   $string = rtrim($string, "&");

   //print "$string \n\n\n\n";

   $host = 'as1.emv2.com';
   $port = 80;
   $path = "/D";

   $fp = fsockopen($host, $port, $errno, $errstr, $timeout = 30);
   if (!$fp)
   {
      //print out any errors
      echo "<!--$errstr ($errno)\n -->";
   }
   else //if the connection was established successfully
   {
      //send the server request
      fputs($fp, "POST $path HTTP/1.1\r\n");
      fputs($fp, "Host: $host\r\n");
      fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
      fputs($fp, "Content-length: ".strlen($string)."\r\n");
      fputs($fp, "Connection: close\r\n\r\n");
      fputs($fp, $string . "\r\n\r\n");

      //store the response
      while (!feof($fp))
      {
         $buffer .= fread($fp,4096);
         //print $buffer."\r\n";
      }
      //print $buffer;

   }


   $logFile = fopen("/home/www/quotezone.co.uk/insurance-new/home/HomeEmails/ResubscribeDetails.log", "a+");
   $today = date("Y-m-d H:i:s");
   fwrite($logFile, "$today $logID $buffer \n $string \n");
   fclose($logFile);

   //return $buffer;

}//end function ResubscribeUser($email)

?>

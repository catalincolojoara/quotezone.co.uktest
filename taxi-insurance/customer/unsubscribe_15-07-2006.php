<?php
error_reporting(E_ALL);
include_once "../modules/globals.adm.inc";
include_once "EmailStore.php";
include_once "QuoteUserDetails.php";
include_once "Log.php";
include_once "MySQL.php";

$emStore = new CEmailStore();
print_r($_GET);
$emails = $_GET["email"];//"top1;top3;campaign";
$quotes = $_GET["quote"];//"car;home;van;bike";
$emailAddress = $_GET["emailaddress"];//"test@test.com";


if (!$_POST["unsubscribe"])
{
	if ($_GET["authcode"] != $emStore->GenerateAuthenticityCode(str_replace(";",".",$_GET["email"])."@".str_replace(";",".",$_GET["quote"])))
	{
		ShowHtml("Authenticity problem! Try again or contact Quotezone. Thank you!");
		exit(0);
	}
	
	if ($_GET["emauthcode"] != $emStore->GenerateAuthenticityCode($_GET["emailaddress"]))
	{
		ShowHtml("Authenticity problem! Try again or contact Quotezone. Thank you!");
		exit(0);
	}
}	

if ($_POST["unsubscribe"] == "true")
{
	print_r($_POST);
	
	if (!$emStoreId = $emStore->GetEmailStoreIDByEmail($_POST["emailaddress"]))
	{
		$objQuoteUserDetails = new CQuoteUserDetails();
		$objLog              = new CLog();
		$objMysql            = new CMySQL();
		
		$fileName = substr($_POST["sid"], 0, 10)."_".substr($_POST["sid"], 10, 6);
		
		$logId = $objLog->GetLogIdByFilename($fileName);

		$objMysql->Open();
			
		$sql = "SELECT quote_users.first_name,quote_users.last_name,quote_user_details.home_owner FROM quote_users,quote_user_details,qzquotes,logs WHERE logs.id='$logId' AND qzquotes.log_id=logs.id AND qzquotes.quote_user_details_id=quote_user_details.id AND quote_user_details.quote_user_id=quote_users.id;";

		$objMysql->Exec($sql);
		
		$objMysql->FetchRows();
		
		$realName  = $objMysql->GetFieldValue("first_name")." ".$objMysql->GetFieldValue("last_name");
		if ($objMysql->GetFieldValue("home_owner") == N)
			$homeOwner = 0;
			else $homeOnwer = 1;
		
		print $realName.$homeOwner;	
		die();
		$emStoreID = $emStore->AddEmailStore();
	}
	
	$initialEmails = $emStore->GetAllEmailTypes();
	$initialQuotes = $emStore->GetAllQuoteTypes();
	
	$finalEmails = $initialEmails;
	$finalQuotes = $initialQuotes;
	
	$emailParams = explode(";",$_POST["email"]);
	$quoteParams = explode(";",$_POST["quote"]);

	foreach ($initialEmails as $initialEmailsName=>$initialEmailsValue)
	{
		foreach ($emailParams as $emailParamKey=>$emailParam)
			if ($initialEmailsName == $emailParam)
				$finalEmails[$initialEmailsName] = 0;
	}
	
	foreach ($initialQuotes as $initialQuotesName=>$initialQuotesValue)
	{
		foreach ($quoteParams as $quoteParamKey=>$quoteParam)
			if ($initialQuotesName == $quoteParam)
				$finalQuotes[$initialQuotesName] = 0;
	}
	
	$result = $emStore->CalculateRule($finalEmails,$finalQuotes);
	
	ShowHtml($result);
	exit(0);
}

$message = "IMPORTANT - If you choose to unsubscribe from our database, we will not be able to send you an email reminder when its time for your insurance renewal, nor will we be able to email you if any of our panel members return a cheaper quote in the next few days. Are you sure you wish to unsubcribe $_GET[emailaddress] from our mailist list?";
ShowHtml($message,true);

function ShowMessage($msg)
{
	print $msg;
}

function ShowHtml($message,$buttons)
{
	print <<<EOT
	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>QuoteZone</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>

<center>
	<table width="760" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="760" height="106" id="qzheader"></td>
		</tr>
		<tr>
			<td height="6"></td>
		</tr>
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="0">
  					<tr>
						<td valign="top">
							<table border="0" cellspacing="0" cellpadding="0">
              				<tr>
                				<td>
										<table border="0" cellspacing="0" cellpadding="0">
                  					<tr>
                    						<td><img src="images/wul.gif" width="10" height="22" /></td>
                    						<td width="740" background="images/wum.gif"></td>
                    						<td><img src="images/wur.gif" width="10" height="22" /></td>
                  					</tr>
                  					<tr>
                    						<td background="images/wml.gif"></td>
                    						<td align="left">
													<table border="0" cellpadding="0" cellspacing="0">
                           					<tr>
															<td height="20" class="btr">
																<table border="0" cellspacing="0" cellpadding="0">
                                 						<tr>
                                    						<td>Customer Unsubscribe</td>
                                 						</tr>
                              						</table>
															</td>
														</tr>
														<tr>
                              					<td width="740" height="1" bgcolor="#CCCCCC"></td>
                           					</tr>
														<tr>
                              					<td height="55"></td>
                           					</tr>
														<tr>
                              					<td align="center" valign="middle">
																<table border="0" cellspacing="0" cellpadding="0">
                                 						<tr>
                                    						<td>
																			<fieldset class="border">
																				<legend class="btr">Confirmation </legend>
																				<form name="login" action="unsubscribe.php" method="POST">

																					<table border="0" cellspacing="0" cellpadding="0">
                                             							<tr>
                                                							<td height="10"></td>
                                                							<td height="10"></td>
                                             							</tr>
                                             							<tr>
                                                							<td align="center" colspan="2"  class="btr">
                                                							    $message
																			</td>
																			
                                             							</tr>
EOT;

	if ($buttons)
		print <<< EOT
                                             							<tr>
                                                							<td class="padding" align="center" colspan="2"><input name="submit" type="submit" value="Yes"/>
                                                							<input type="button" name="button" value="No" onclick="window.close()"/></td>
		                                          							<input type="hidden" name="unsubscribe" value="true">
		                                          							<input type="hidden" name="emailaddress" value="$_GET[emailaddress]">
		                                          							<input type="hidden" name="emauthcode" value="$_GET[emauthcode]">
		                                          							<input type="hidden" name="email" value="$_GET[email]">
		                                          							<input type="hidden" name="quote" value="$_GET[quote]">
		                                          							<input type="hidden" name="authcode" value="$_GET[authcode]">
		                                          							<input type="hidden" name="sid" value="$_GET[sid]">
                                             							</tr>
EOT;
	
	print <<< EOT
                                             							<tr>
                                                							<td colspan="2" align="center" valign="middle" class="padding"></td>
                                             							</tr>
                                          							</table>
                                          							</form>
<!--                                          							<center><input type="button" name="submit" value="Submit" onclick="SubmitForm()" class="submit" /></center>-->
																				</fieldset>
																		</td>
                                 						</tr>
                              						</table>
															</td>
                           					</tr>
														<tr>
                              					<td height="75"></td>
                           					</tr>
                        					</table>
												</td>
												<td background="images/wmr.gif"></td>
											</tr>
											<tr>
												<td><img src="images/wdl.gif" width="10" height="10" /></td>
												<td background="images/wdm.gif"></td>
												<td><img src="images/wdr.gif" width="10" height="10" /></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td height="6"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td height="2"></td>
		</tr>
		<tr>
			<td height="35" bgcolor="#4882B4" align="center"><font color="#FFFFFF" face="Verdana" size="2">Copyright &copy; 2005 QuoteZone, All Rights Reserved</font></td>
		</tr>
	</table>
</center>
</body>
</html>	
EOT;
}

?>





